//
// AbstractRepositoryProxyManager.java
//
//     An adapter for a RepositoryProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.repository.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RepositoryProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRepositoryProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.repository.RepositoryProxyManager>
    implements org.osid.repository.RepositoryProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterRepositoryProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRepositoryProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRepositoryProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRepositoryProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if asset lookup is supported. 
     *
     *  @return <code> true </code> if asset lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetLookup() {
        return (getAdapteeManager().supportsAssetLookup());
    }


    /**
     *  Tests if asset query is supported. 
     *
     *  @return <code> true </code> if asset query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (getAdapteeManager().supportsAssetQuery());
    }


    /**
     *  Tests if asset search is supported. 
     *
     *  @return <code> true </code> if asset search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetSearch() {
        return (getAdapteeManager().supportsAssetSearch());
    }


    /**
     *  Tests if asset administration is supported. 
     *
     *  @return <code> true </code> if asset administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetAdmin() {
        return (getAdapteeManager().supportsAssetAdmin());
    }


    /**
     *  Tests if asset notification is supported. A repository may send 
     *  messages when assets are created, modified, or deleted. 
     *
     *  @return <code> true </code> if asset notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetNotification() {
        return (getAdapteeManager().supportsAssetNotification());
    }


    /**
     *  Tests if retrieving mappings of assets and repositories is supported. 
     *
     *  @return <code> true </code> if asset repository mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetRepository() {
        return (getAdapteeManager().supportsAssetRepository());
    }


    /**
     *  Tests if managing mappings of assets and repositories is supported. 
     *
     *  @return <code> true </code> if asset repository assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetRepositoryAssignment() {
        return (getAdapteeManager().supportsAssetRepositoryAssignment());
    }


    /**
     *  Tests if asset smart repository is supported. 
     *
     *  @return <code> true </code> if asset smart repository is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetSmartRepository() {
        return (getAdapteeManager().supportsAssetSmartRepository());
    }


    /**
     *  Tests if retrieving mappings of assets and time coverage is supported. 
     *
     *  @return <code> true </code> if asset temporal mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetTemporal() {
        return (getAdapteeManager().supportsAssetTemporal());
    }


    /**
     *  Tests if managing mappings of assets and time ocverage is supported. 
     *
     *  @return <code> true </code> if asset temporal assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetTemporalAssignment() {
        return (getAdapteeManager().supportsAssetTemporalAssignment());
    }


    /**
     *  Tests if retrieving mappings of assets and spatial coverage is 
     *  supported. 
     *
     *  @return <code> true </code> if asset spatial mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetSpatial() {
        return (getAdapteeManager().supportsAssetSpatial());
    }


    /**
     *  Tests if managing mappings of assets and spatial ocverage is 
     *  supported. 
     *
     *  @return <code> true </code> if asset spatial assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetSpatialAssignment() {
        return (getAdapteeManager().supportsAssetSpatialAssignment());
    }


    /**
     *  Tests if assets are included in compositions. 
     *
     *  @return <code> true </code> if asset composition supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetComposition() {
        return (getAdapteeManager().supportsAssetComposition());
    }


    /**
     *  Tests if mapping assets to compositions is supported. 
     *
     *  @return <code> true </code> if designing asset compositions is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetCompositionDesign() {
        return (getAdapteeManager().supportsAssetCompositionDesign());
    }


    /**
     *  Tests if composition lookup is supported. 
     *
     *  @return <code> true </code> if composition lookup is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionLookup() {
        return (getAdapteeManager().supportsCompositionLookup());
    }


    /**
     *  Tests if composition query is supported. 
     *
     *  @return <code> true </code> if composition query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionQuery() {
        return (getAdapteeManager().supportsCompositionQuery());
    }


    /**
     *  Tests if composition search is supported. 
     *
     *  @return <code> true </code> if composition search is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionSearch() {
        return (getAdapteeManager().supportsCompositionSearch());
    }


    /**
     *  Tests if composition administration is supported. 
     *
     *  @return <code> true </code> if composition administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionAdmin() {
        return (getAdapteeManager().supportsCompositionAdmin());
    }


    /**
     *  Tests if composition notification is supported. 
     *
     *  @return <code> true </code> if composition notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionNotification() {
        return (getAdapteeManager().supportsCompositionNotification());
    }


    /**
     *  Tests if retrieval of composition to repository mappings is supported. 
     *
     *  @return <code> true </code> if composition to repository mapping is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionRepository() {
        return (getAdapteeManager().supportsCompositionRepository());
    }


    /**
     *  Tests if assigning composition to repository mappings is supported. 
     *
     *  @return <code> true </code> if composition to repository assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionRepositoryAssignment() {
        return (getAdapteeManager().supportsCompositionRepositoryAssignment());
    }


    /**
     *  Tests if composition smart repository is supported. 
     *
     *  @return <code> true </code> if composition smart repository is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionSmartRepository() {
        return (getAdapteeManager().supportsCompositionSmartRepository());
    }


    /**
     *  Tests if repository lookup is supported. 
     *
     *  @return <code> true </code> if repository lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryLookup() {
        return (getAdapteeManager().supportsRepositoryLookup());
    }


    /**
     *  Tests if repository query is supported. 
     *
     *  @return <code> true </code> if repository query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryQuery() {
        return (getAdapteeManager().supportsRepositoryQuery());
    }


    /**
     *  Tests if repository search is supported. 
     *
     *  @return <code> true </code> if repository search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositorySearch() {
        return (getAdapteeManager().supportsRepositorySearch());
    }


    /**
     *  Tests if repository administration is supported. 
     *
     *  @return <code> true </code> if repository administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryAdmin() {
        return (getAdapteeManager().supportsRepositoryAdmin());
    }


    /**
     *  Tests if repository notification is supported. Messages may be sent 
     *  when <code> Repository </code> objects are created, deleted or 
     *  updated. Notifications for assets within repositories are sent via the 
     *  asset notification session. 
     *
     *  @return <code> true </code> if repository notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryNotification() {
        return (getAdapteeManager().supportsRepositoryNotification());
    }


    /**
     *  Tests if a repository hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a repository hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryHierarchy() {
        return (getAdapteeManager().supportsRepositoryHierarchy());
    }


    /**
     *  Tests if a repository hierarchy design is supported. 
     *
     *  @return <code> true </code> if a repository hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryHierarchyDesign() {
        return (getAdapteeManager().supportsRepositoryHierarchyDesign());
    }


    /**
     *  Tests if a repository batch service is supported. 
     *
     *  @return <code> true </code> if a repository batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryBatch() {
        return (getAdapteeManager().supportsRepositoryBatch());
    }


    /**
     *  Tests if a repository rules service is supported. 
     *
     *  @return <code> true </code> if a repository rules service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryRules() {
        return (getAdapteeManager().supportsRepositoryRules());
    }


    /**
     *  Gets all the asset record types supported. 
     *
     *  @return the list of supported asset record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssetRecordTypes() {
        return (getAdapteeManager().getAssetRecordTypes());
    }


    /**
     *  Tests if a given asset type is supported. 
     *
     *  @param  assetRecordType the asset record type 
     *  @return <code> true </code> if the asset record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> assetRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssetRecordType(org.osid.type.Type assetRecordType) {
        return (getAdapteeManager().supportsAssetRecordType(assetRecordType));
    }


    /**
     *  Gets all the asset search record types supported. 
     *
     *  @return the list of supported asset search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssetSearchRecordTypes() {
        return (getAdapteeManager().getAssetSearchRecordTypes());
    }


    /**
     *  Tests if a given asset search record type is supported. 
     *
     *  @param  assetSearchRecordType the asset search record type 
     *  @return <code> true </code> if the asset search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> assetSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssetSearchRecordType(org.osid.type.Type assetSearchRecordType) {
        return (getAdapteeManager().supportsAssetSearchRecordType(assetSearchRecordType));
    }


    /**
     *  Gets all the asset content record types supported. 
     *
     *  @return the list of supported asset content record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssetContentRecordTypes() {
        return (getAdapteeManager().getAssetContentRecordTypes());
    }


    /**
     *  Tests if a given asset content record type is supported. 
     *
     *  @param  assetContentRecordType the asset content record type 
     *  @return <code> true </code> if the asset content record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> assetContentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssetContentRecordType(org.osid.type.Type assetContentRecordType) {
        return (getAdapteeManager().supportsAssetContentRecordType(assetContentRecordType));
    }


    /**
     *  Gets all the composition record types supported. 
     *
     *  @return the list of supported composition record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompositionRecordTypes() {
        return (getAdapteeManager().getCompositionRecordTypes());
    }


    /**
     *  Tests if a given composition record type is supported. 
     *
     *  @param  compositionRecordType the composition record type 
     *  @return <code> true </code> if the composition record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> compositionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompositionRecordType(org.osid.type.Type compositionRecordType) {
        return (getAdapteeManager().supportsCompositionRecordType(compositionRecordType));
    }


    /**
     *  Gets all the composition search record types supported. 
     *
     *  @return the list of supported composition search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompositionSearchRecordTypes() {
        return (getAdapteeManager().getCompositionSearchRecordTypes());
    }


    /**
     *  Tests if a given composition search record type is supported. 
     *
     *  @param  compositionSearchRecordType the composition serach type 
     *  @return <code> true </code> if the composition search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompositionSearchRecordType(org.osid.type.Type compositionSearchRecordType) {
        return (getAdapteeManager().supportsCompositionSearchRecordType(compositionSearchRecordType));
    }


    /**
     *  Gets all the repository record types supported. 
     *
     *  @return the list of supported repository record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRepositoryRecordTypes() {
        return (getAdapteeManager().getRepositoryRecordTypes());
    }


    /**
     *  Tests if a given repository record type is supported. 
     *
     *  @param  repositoryRecordType the repository record type 
     *  @return <code> true </code> if the repository record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> repositoryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRepositoryRecordType(org.osid.type.Type repositoryRecordType) {
        return (getAdapteeManager().supportsRepositoryRecordType(repositoryRecordType));
    }


    /**
     *  Gets all the repository search record types supported. 
     *
     *  @return the list of supported repository search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRepositorySearchRecordTypes() {
        return (getAdapteeManager().getRepositorySearchRecordTypes());
    }


    /**
     *  Tests if a given repository search record type is supported. 
     *
     *  @param  repositorySearchRecordType the repository search type 
     *  @return <code> true </code> if the repository search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          repositorySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRepositorySearchRecordType(org.osid.type.Type repositorySearchRecordType) {
        return (getAdapteeManager().supportsRepositorySearchRecordType(repositorySearchRecordType));
    }


    /**
     *  Gets all the spatial unit record types supported. 
     *
     *  @return the list of supported spatial unit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpatialUnitRecordTypes() {
        return (getAdapteeManager().getSpatialUnitRecordTypes());
    }


    /**
     *  Tests if a given spatial unit record type is supported. 
     *
     *  @param  spatialUnitRecordType the spatial unit record type 
     *  @return <code> true </code> if the spatial unit record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        return (getAdapteeManager().supportsSpatialUnitRecordType(spatialUnitRecordType));
    }


    /**
     *  Gets all the coordinate types supported. 
     *
     *  @return the list of supported coordinate types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateTypes() {
        return (getAdapteeManager().getCoordinateTypes());
    }


    /**
     *  Tests if a given coordinate type is supported. 
     *
     *  @param  coordinateType the coordinate type 
     *  @return <code> true </code> if the coordinate type is supported <code> 
     *          , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateType(org.osid.type.Type coordinateType) {
        return (getAdapteeManager().supportsCoordinateType(coordinateType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetLookupSession getAssetLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset lookup 
     *  service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAssetLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetLookupSession getAssetLookupSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetLookupSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuerySession getAssetQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset query 
     *  service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuerySession getAssetQuerySessionForRepository(org.osid.id.Id repositoryId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetQuerySessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets an asset search session. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSearchSession getAssetSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetSearchSession(proxy));
    }


    /**
     *  Gets an asset search session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAssetSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSearchSession getAssetSearchSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetSearchSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets an asset administration session for creating, updating and 
     *  deleting assets. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetAdminSession getAssetAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetAdminSession(proxy));
    }


    /**
     *  Gets an asset administration session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetAdminSession getAssetAdminSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetAdminSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to asset 
     *  changes. 
     *
     *  @param  assetReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AssetNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assetReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetNotificationSession getAssetNotificationSession(org.osid.repository.AssetReceiver assetReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetNotificationSession(assetReceiver, proxy));
    }


    /**
     *  Gets the asset notification session for the given repository. 
     *
     *  @param  assetReceiver the notification callback 
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> assetReceiver, 
     *          repositoryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetNotificationSession getAssetNotificationSessionForRepository(org.osid.repository.AssetReceiver assetReceiver, 
                                                                                                 org.osid.id.Id repositoryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetNotificationSessionForRepository(assetReceiver, repositoryId, proxy));
    }


    /**
     *  Gets the session for retrieving asset to repository mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetRepositorySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetRepository() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetRepositorySession getAssetRepositorySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetRepositorySession(proxy));
    }


    /**
     *  Gets the session for assigning asset to repository mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetRepositoryAsignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetRepositoryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetRepositoryAssignmentSession getAssetRepositoryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetRepositoryAssignmentSession(proxy));
    }


    /**
     *  Gets an asset smart repository session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetSmartRepositorySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetSmartRepository() </code> <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSmartRepositorySession getAssetSmartRepositorySession(org.osid.id.Id repositoryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetSmartRepositorySession(repositoryId, proxy));
    }


    /**
     *  Gets the session for retrieving temporal coverage of an asset. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetTemporalSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetTemporal() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalSession getAssetTemporalSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetTemporalSession(proxy));
    }


    /**
     *  Gets the session for retrieving temporal coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetTemporalSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetTemporal() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalSession getAssetTemporalSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetTemporalSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets the session for assigning temporal coverage to an asset. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetTemporalAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetTemporalAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalAssignmentSession getAssetTemporalAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetTemporalAssignmentSession(proxy));
    }


    /**
     *  Gets the session for assigning temporal coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetTemporalAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetTemporalAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalAssignmentSession getAssetTemporalAssignmentSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetTemporalAssignmentSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets the session for retrieving spatial coverage of an asset. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetSpatialSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSpatialAssets() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialSession getAssetSpatialSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetSpatialSession(proxy));
    }


    /**
     *  Gets the session for retrieving spatial coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetSpatialSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetSpatial() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialSession getAssetSpatialSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetSpatialSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets the session for assigning spatial coverage to an asset. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetSpatialAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetSpatialAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialAssignmentSession getAssetSpatialAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetSpatialAssignmentSession(proxy));
    }


    /**
     *  Gets the session for assigning spatial coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetSpatialAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetSpatialAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialAssignmentSession getAssetSpatialAssignmentSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetSpatialAssignmentSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets the session for retrieving asset compositions. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetCompositionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetComposition() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetCompositionSession getAssetCompositionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetCompositionSession(proxy));
    }


    /**
     *  Gets the session for creating asset compositions. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetCompositionDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetCompositionDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetCompositionDesignSession getAssetCompositionDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssetCompositionDesignSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return the new <code> CompositionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionLookupSession getCompositionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  lookup service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return the new <code> CompositionLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionLookupSession getCompositionLookupSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionLookupSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets a composition query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSearchSession getCompositionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionQuerySession(proxy));
    }


    /**
     *  Gets a composition query session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuerySession getCompositionQuerySessionForRepository(org.osid.id.Id repositoryId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionQuerySessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets a composition search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSearchSession getCompositionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionSearchSession(proxy));
    }


    /**
     *  Gets a composition search session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSearchSession getCompositionSearchSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionSearchSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets a composition administration session for creating, updating and 
     *  deleting compositions. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionAdminSession getCompositionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionAdminSession(proxy));
    }


    /**
     *  Gets a composiiton administrative session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionAdminSession getCompositionAdminSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionAdminSessionForRepository(repositoryId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  composition changes. 
     *
     *  @param  compositionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> compositionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionNotificationSession getCompositionNotificationSession(org.osid.repository.CompositionReceiver compositionReceiver, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionNotificationSession(compositionReceiver, proxy));
    }


    /**
     *  Gets the composition notification session for the given repository. 
     *
     *  @param  compositionReceiver the notification callback 
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> compositionReceiver, 
     *          repositoryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionNotificationSession getCompositionNotificationSessionForRepository(org.osid.repository.CompositionReceiver compositionReceiver, 
                                                                                                             org.osid.id.Id repositoryId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionNotificationSessionForRepository(compositionReceiver, repositoryId, proxy));
    }


    /**
     *  Gets the session for retrieving composition to repository mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionRepositorySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionRepository() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionRepositorySession getCompositionRepositorySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionRepositorySession(proxy));
    }


    /**
     *  Gets the session for assigning composition to repository mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionRepositoryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionRepositoryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionRepositoryAssignmentSession getCompositionRepositoryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionRepositoryAssignmentSession(proxy));
    }


    /**
     *  Gets a composition smart repository session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionSmartRepositorySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionSmartRepository() </code> <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSmartRepositorySession getCompositionSmartRepositorySession(org.osid.id.Id repositoryId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionSmartRepositorySession(repositoryId, proxy));
    }


    /**
     *  Gets the repository lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepositoryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryLookupSession getRepositoryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryLookupSession(proxy));
    }


    /**
     *  Gets the repository query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepositoryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuerySession getRepositoryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryQuerySession(proxy));
    }


    /**
     *  Gets the repository search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepositorySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositorySearchSession getRepositorySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositorySearchSession(proxy));
    }


    /**
     *  Gets the repository administrative session for creating, updating and 
     *  deleteing repositories. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepositoryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryAdminSession getRepositoryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryAdminSession(proxy));
    }


    /**
     *  Gets the notification session for subscribing to changes to a 
     *  repository. 
     *
     *  @param  repositoryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RepositoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryNotificationSession getRepositoryNotificationSession(org.osid.repository.RepositoryReceiver repositoryReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryNotificationSession(repositoryReceiver, proxy));
    }


    /**
     *  Gets the repository hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a RepositoryHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryHierarchySession getRepositoryHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryHierarchySession(proxy));
    }


    /**
     *  Gets the repository hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepostoryHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryHierarchyDesignSession getRepositoryHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> RepositoryBatchProxyManager. </code> 
     *
     *  @return a <code> RepostoryBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.RepositoryBatchProxyManager getRepositoryBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryBatchProxyManager());
    }


    /**
     *  Gets a <code> RepositoryRulesProxyManager. </code> 
     *
     *  @return a <code> RepostoryRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.RepositoryRulesProxyManager getRepositoryRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
