//
// ResourceVelocityMiter.java
//
//     Defines a ResourceVelocity miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.resourcevelocity;


/**
 *  Defines a <code>ResourceVelocity</code> miter for use with the builders.
 */

public interface ResourceVelocityMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidCompendiumMiter,
            org.osid.mapping.path.ResourceVelocity {


    /**
     *  Sets the resource.
     *
     *  @param resource the resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the speed.
     *
     *  @param speed the speed
     *  @throws org.osid.NullArgumentException <code>speed</code>
     *          is <code>null</code>
     */

    public void setSpeed(org.osid.mapping.Speed speed);


    /**
     *  Sets the coordinate position.
     *
     *  @param coordinate the position
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public void setPosition(org.osid.mapping.Coordinate coordinate);


    /**
     *  Sets the heading.
     *
     *  @param heading the heading
     *  @throws org.osid.NullArgumentException <code>heading</code>
     *          is <code>null</code>
     */

    public void setHeading(org.osid.mapping.Heading heading);


    /**
     *  Sets the path.
     *
     *  @param path the path
     *  @throws org.osid.NullArgumentException <code>path</code>
     *          is <code>null</code>
     */

    public void setPath(org.osid.mapping.path.Path path);


    /**
     *  Adds a ResourceVelocity record.
     *
     *  @param record a resource velocity record
     *  @param recordType the type of resource velocity record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addResourceVelocityRecord(org.osid.mapping.path.records.ResourceVelocityRecord record, 
                                          org.osid.type.Type recordType);
}       


