//
// AbstractIndexedMapIntersectionLookupSession.java
//
//    A simple framework for providing an Intersection lookup service
//    backed by a fixed collection of intersections with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Intersection lookup service backed by a
 *  fixed collection of intersections. The intersections are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some intersections may be compatible
 *  with more types than are indicated through these intersection
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Intersections</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapIntersectionLookupSession
    extends AbstractMapIntersectionLookupSession
    implements org.osid.mapping.path.IntersectionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.Intersection> intersectionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.Intersection>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.Intersection> intersectionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.Intersection>());


    /**
     *  Makes an <code>Intersection</code> available in this session.
     *
     *  @param  intersection an intersection
     *  @throws org.osid.NullArgumentException <code>intersection<code> is
     *          <code>null</code>
     */

    @Override
    protected void putIntersection(org.osid.mapping.path.Intersection intersection) {
        super.putIntersection(intersection);

        this.intersectionsByGenus.put(intersection.getGenusType(), intersection);
        
        try (org.osid.type.TypeList types = intersection.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.intersectionsByRecord.put(types.getNextType(), intersection);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an intersection from this session.
     *
     *  @param intersectionId the <code>Id</code> of the intersection
     *  @throws org.osid.NullArgumentException <code>intersectionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeIntersection(org.osid.id.Id intersectionId) {
        org.osid.mapping.path.Intersection intersection;
        try {
            intersection = getIntersection(intersectionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.intersectionsByGenus.remove(intersection.getGenusType());

        try (org.osid.type.TypeList types = intersection.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.intersectionsByRecord.remove(types.getNextType(), intersection);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeIntersection(intersectionId);
        return;
    }


    /**
     *  Gets an <code>IntersectionList</code> corresponding to the given
     *  intersection genus <code>Type</code> which does not include
     *  intersections of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known intersections or an error results. Otherwise,
     *  the returned list may contain only those intersections that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  intersectionGenusType an intersection genus type 
     *  @return the returned <code>Intersection</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByGenusType(org.osid.type.Type intersectionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.intersection.ArrayIntersectionList(this.intersectionsByGenus.get(intersectionGenusType)));
    }


    /**
     *  Gets an <code>IntersectionList</code> containing the given
     *  intersection record <code>Type</code>. In plenary mode, the
     *  returned list contains all known intersections or an error
     *  results. Otherwise, the returned list may contain only those
     *  intersections that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  intersectionRecordType an intersection record type 
     *  @return the returned <code>intersection</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByRecordType(org.osid.type.Type intersectionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.intersection.ArrayIntersectionList(this.intersectionsByRecord.get(intersectionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.intersectionsByGenus.clear();
        this.intersectionsByRecord.clear();

        super.close();

        return;
    }
}
