//
// AbstractAssemblyRegistrationQuery.java
//
//     A RegistrationQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.registration.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RegistrationQuery that stores terms.
 */

public abstract class AbstractAssemblyRegistrationQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.registration.RegistrationQuery,
               org.osid.course.registration.RegistrationQueryInspector,
               org.osid.course.registration.RegistrationSearchOrder {

    private final java.util.Collection<org.osid.course.registration.records.RegistrationQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.registration.records.RegistrationQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.registration.records.RegistrationSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRegistrationQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRegistrationQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the activity bundle <code> Id </code> for this query to match 
     *  registrations that have a related course. 
     *
     *  @param  activityBundleId an activity bundle <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityBundleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityBundleId(org.osid.id.Id activityBundleId, 
                                      boolean match) {
        getAssembler().addIdTerm(getActivityBundleIdColumn(), activityBundleId, match);
        return;
    }


    /**
     *  Clears the activity bundle <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityBundleIdTerms() {
        getAssembler().clearTerms(getActivityBundleIdColumn());
        return;
    }


    /**
     *  Gets the activity bundle <code> Id </code> query terms. 
     *
     *  @return the activity bundle <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityBundleIdTerms() {
        return (getAssembler().getIdTerms(getActivityBundleIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by activity bundle. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivityBundle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActivityBundleColumn(), style);
        return;
    }


    /**
     *  Gets the ActivityBundleId column name.
     *
     * @return the column name
     */

    protected String getActivityBundleIdColumn() {
        return ("activity_bundle_id");
    }


    /**
     *  Tests if an <code> ActivityBundleQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity bundle query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity bundle. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the activity bundle query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQuery getActivityBundleQuery() {
        throw new org.osid.UnimplementedException("supportsActivityBundleQuery() is false");
    }


    /**
     *  Clears the activity bundle terms. 
     */

    @OSID @Override
    public void clearActivityBundleTerms() {
        getAssembler().clearTerms(getActivityBundleColumn());
        return;
    }


    /**
     *  Gets the activity bundle query terms. 
     *
     *  @return the activity bundle query terms 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQueryInspector[] getActivityBundleTerms() {
        return (new org.osid.course.registration.ActivityBundleQueryInspector[0]);
    }


    /**
     *  Tests if an activity bundle search order is available. 
     *
     *  @return <code> true </code> if an activity bundle search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the activity bundle search order. 
     *
     *  @return the activity bundle search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSearchOrder getActivityBundleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivityBundleSearchOrder() is false");
    }


    /**
     *  Gets the ActivityBundle column name.
     *
     * @return the column name
     */

    protected String getActivityBundleColumn() {
        return ("activity_bundle");
    }


    /**
     *  Sets the student resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getStudentIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the student resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        getAssembler().clearTerms(getStudentIdColumn());
        return;
    }


    /**
     *  Gets the student <code> Id </code> query terms. 
     *
     *  @return the student <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (getAssembler().getIdTerms(getStudentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by student. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStudentColumn(), style);
        return;
    }


    /**
     *  Gets the StudentId column name.
     *
     * @return the column name
     */

    protected String getStudentIdColumn() {
        return ("student_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student resource terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        getAssembler().clearTerms(getStudentColumn());
        return;
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the student query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a student search order is available. 
     *
     *  @return <code> true </code> if a student search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the student search order. 
     *
     *  @return the student search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Gets the Student column name.
     *
     * @return the column name
     */

    protected String getStudentColumn() {
        return ("student");
    }


    /**
     *  Matches registrations with credits between the given numbers 
     *  inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchCredits(java.math.BigDecimal min, 
                             java.math.BigDecimal max, boolean match) {
        getAssembler().addDecimalRangeTerm(getCreditsColumn(), min, max, match);
        return;
    }


    /**
     *  Matches a registration that has any credits assigned. 
     *
     *  @param  match <code> true </code> to match registrations with any 
     *          credits, <code> false </code> to match registrations with no 
     *          credits 
     */

    @OSID @Override
    public void matchAnyCredits(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getCreditsColumn(), match);
        return;
    }


    /**
     *  Clears the credit terms. 
     */

    @OSID @Override
    public void clearCreditsTerms() {
        getAssembler().clearTerms(getCreditsColumn());
        return;
    }


    /**
     *  Gets the credits query terms. 
     *
     *  @return the credits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getCreditsTerms() {
        return (getAssembler().getDecimalTerms(getCreditsColumn()));
    }


    /**
     *  Gets the Credits column name.
     *
     * @return the column name
     */

    protected String getCreditsColumn() {
        return ("credits");
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingOptionId(org.osid.id.Id gradeSystemId, 
                                     boolean match) {
        getAssembler().addIdTerm(getGradingOptionIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingOptionIdTerms() {
        getAssembler().clearTerms(getGradingOptionIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradingOptionIdTerms() {
        return (getAssembler().getIdTerms(getGradingOptionIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grading 
     *  option. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradingOption(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradingOptionColumn(), style);
        return;
    }


    /**
     *  Gets the GradingOptionId column name.
     *
     * @return the column name
     */

    protected String getGradingOptionIdColumn() {
        return ("grading_option_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradingOptionQuery() {
        throw new org.osid.UnimplementedException("supportsGradingOptionQuery() is false");
    }


    /**
     *  Matches registrations that have any grading option. 
     *
     *  @param  match <code> true </code> to match registrations with any 
     *          grading option, <code> false </code> to match registrations 
     *          with no grading options 
     */

    @OSID @Override
    public void matchAnyGradingOption(boolean match) {
        getAssembler().addIdWildcardTerm(getGradingOptionColumn(), match);
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearGradingOptionTerms() {
        getAssembler().clearTerms(getGradingOptionColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradingOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a grade system search order is available. 
     *
     *  @return <code> true </code> if a grade system order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingOptionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system search order. 
     *
     *  @return the grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingOptionSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getGradingOptionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradingOptionSearchOrder() is false");
    }


    /**
     *  Gets the GradingOption column name.
     *
     * @return the column name
     */

    protected String getGradingOptionColumn() {
        return ("grading_option");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  registrations assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this registration supports the given record
     *  <code>Type</code>.
     *
     *  @param  registrationRecordType a registration record type 
     *  @return <code>true</code> if the registrationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type registrationRecordType) {
        for (org.osid.course.registration.records.RegistrationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(registrationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  registrationRecordType the registration record type 
     *  @return the registration query record 
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(registrationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.RegistrationQueryRecord getRegistrationQueryRecord(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.RegistrationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(registrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(registrationRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  registrationRecordType the registration record type 
     *  @return the registration query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(registrationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.RegistrationQueryInspectorRecord getRegistrationQueryInspectorRecord(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.RegistrationQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(registrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(registrationRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param registrationRecordType the registration record type
     *  @return the registration search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(registrationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.RegistrationSearchOrderRecord getRegistrationSearchOrderRecord(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.RegistrationSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(registrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(registrationRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this registration. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param registrationQueryRecord the registration query record
     *  @param registrationQueryInspectorRecord the registration query inspector
     *         record
     *  @param registrationSearchOrderRecord the registration search order record
     *  @param registrationRecordType registration record type
     *  @throws org.osid.NullArgumentException
     *          <code>registrationQueryRecord</code>,
     *          <code>registrationQueryInspectorRecord</code>,
     *          <code>registrationSearchOrderRecord</code> or
     *          <code>registrationRecordTyperegistration</code> is
     *          <code>null</code>
     */
            
    protected void addRegistrationRecords(org.osid.course.registration.records.RegistrationQueryRecord registrationQueryRecord, 
                                      org.osid.course.registration.records.RegistrationQueryInspectorRecord registrationQueryInspectorRecord, 
                                      org.osid.course.registration.records.RegistrationSearchOrderRecord registrationSearchOrderRecord, 
                                      org.osid.type.Type registrationRecordType) {

        addRecordType(registrationRecordType);

        nullarg(registrationQueryRecord, "registration query record");
        nullarg(registrationQueryInspectorRecord, "registration query inspector record");
        nullarg(registrationSearchOrderRecord, "registration search odrer record");

        this.queryRecords.add(registrationQueryRecord);
        this.queryInspectorRecords.add(registrationQueryInspectorRecord);
        this.searchOrderRecords.add(registrationSearchOrderRecord);
        
        return;
    }
}
