//
// AbstractPoolSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.pool.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPoolSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.PoolSearchResults {

    private org.osid.provisioning.PoolList pools;
    private final org.osid.provisioning.PoolQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.records.PoolSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPoolSearchResults.
     *
     *  @param pools the result set
     *  @param poolQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>pools</code>
     *          or <code>poolQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPoolSearchResults(org.osid.provisioning.PoolList pools,
                                            org.osid.provisioning.PoolQueryInspector poolQueryInspector) {
        nullarg(pools, "pools");
        nullarg(poolQueryInspector, "pool query inspectpr");

        this.pools = pools;
        this.inspector = poolQueryInspector;

        return;
    }


    /**
     *  Gets the pool list resulting from a search.
     *
     *  @return a pool list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPools() {
        if (this.pools == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.PoolList pools = this.pools;
        this.pools = null;
	return (pools);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.PoolQueryInspector getPoolQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  pool search record <code> Type. </code> This method must
     *  be used to retrieve a pool implementing the requested
     *  record.
     *
     *  @param poolSearchRecordType a pool search 
     *         record type 
     *  @return the pool search
     *  @throws org.osid.NullArgumentException
     *          <code>poolSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(poolSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.PoolSearchResultsRecord getPoolSearchResultsRecord(org.osid.type.Type poolSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.records.PoolSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(poolSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(poolSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record pool search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPoolRecord(org.osid.provisioning.records.PoolSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "pool record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
