//
// AbstractMutablePath.java
//
//     Defines a mutable Path.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.topology.path.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Path</code>.
 */

public abstract class AbstractMutablePath
    extends net.okapia.osid.jamocha.topology.path.path.spi.AbstractPath
    implements org.osid.topology.path.Path,
               net.okapia.osid.jamocha.builder.topology.path.path.PathMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this path. 
     *
     *  @param record path record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addPathRecord(org.osid.topology.path.records.PathRecord record, org.osid.type.Type recordType) {
        super.addPathRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this path is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this path ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this path.
     *
     *  @param displayName the name for this path
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this path.
     *
     *  @param description the description of this path
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this path
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the starting node.
     *
     *  @param node a starting node
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    @Override
    public void setStartingNode(org.osid.topology.Node node) {
        super.setStartingNode(node);
        return;
    }


    /**
     *  Sets the ending node.
     *
     *  @param node an ending node
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndingNode(org.osid.topology.Node node) {
        super.setEndingNode(node);
        return;
    }


    /**
     *  Sets the hops.
     *
     *  @param hop a hops
     *  @throws org.osid.InvalidArgumentException <code>hop</code> is
     *          negative
     */

    @Override
    public void setHops(long hop) {
        super.setHops(hop);
        return;
    }


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    @Override
    public void setDistance(java.math.BigDecimal distance) {
        super.setDistance(distance);
        return;
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    @Override
    public void setCost(java.math.BigDecimal cost) {
        super.setCost(cost);
        return;
    }


    /**
     *  Adds an edge.
     *
     *  @param edge an edge
     *  @throws org.osid.NullArgumentException <code>edge</code> is
     *          <code>null</code>
     */

    @Override
    public void addEdge(org.osid.topology.Edge edge) {
        super.addEdge(edge);
        return;
    }


    /**
     *  Sets all the edges.
     *
     *  @param edges a collection of edges
     *  @throws org.osid.NullArgumentException <code>edges</code> is
     *          <code>null</code>
     */

    @Override
    public void setEdges(java.util.Collection<org.osid.topology.Edge> edges) {
        super.setEdges(edges);
        return;
    }
}

