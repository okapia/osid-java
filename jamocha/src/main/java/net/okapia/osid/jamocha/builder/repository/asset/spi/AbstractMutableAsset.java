//
// AbstractMutableAsset.java
//
//     Defines a mutable Asset.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.asset.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Asset</code>.
 */

public abstract class AbstractMutableAsset
    extends net.okapia.osid.jamocha.repository.asset.spi.AbstractAsset
    implements org.osid.repository.Asset,
               net.okapia.osid.jamocha.builder.repository.asset.AssetMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this asset. 
     *
     *  @param record asset record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAssetRecord(org.osid.repository.records.AssetRecord record, org.osid.type.Type recordType) {
        super.addAssetRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the provider for this asset.
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(net.okapia.osid.provider.Provider provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Sets the provider of this asset
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(org.osid.resource.Resource provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Adds an asset for the provider branding.
     *
     *  @param asset an asset to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    @Override
    public void addAssetToBranding(org.osid.repository.Asset asset) {
        super.addAssetToBranding(asset);
        return;
    }


    /**
     *  Adds assets for the provider branding.
     *
     *  @param assets an array of assets to add
     *  @throws org.osid.NullArgumentException <code>assets</code> is
     *          <code>null</code>
     */

    @Override
    public void setBranding(java.util.Collection<org.osid.repository.Asset> assets) {
        super.setBranding(assets);
        return;
    }


    /**
     *  Sets the license.
     *
     *  @param license the license
     *  @throws org.osid.NullArgumentException <code>license</code>
     *          is <code>null</code>
     */

    @Override
    public void setLicense(org.osid.locale.DisplayText license) {
        super.setLicense(license);
        return;
    }


    /**
     *  Sets the display name for this asset.
     *
     *  @param displayName the name for this asset
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this asset.
     *
     *  @param description the description of this asset
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    @Override
    public void setTitle(org.osid.locale.DisplayText title) {
        super.setTitle(title);
        return;
    }


    /**
     *  Sets the copyright status known flag.
     *
     *  @param status <code> true </code> if the copyright status of
     *         this asset is known, <code> false </code> otherwise. If
     *         <code> false, isPublicDomain(), </code> <code>
     *         canDistributeVerbatim(), canDistributeAlterations() and
     *         canDistributeCompositions() </code> may also be <code>
     *         false. </code>
     */

    @Override
    public void setCopyrightStatusKnown(boolean status) {
        super.setCopyrightStatusKnown(status);
        return;
    }


    /**
     *  Sets the public domain flag and enables all the distribution
     *  rights if <code>true</code>.
     *
     *  @param publicDomain <code> true </code> if this asset is in
     *         the public domain, <code> false </code> otherwise. If
     *         <code> true, </code> <code> canDistributeVerbatim(),
     *         canDistributeAlterations() and
     *         canDistributeCompositions() </code> must also be <code>
     *         true.  </code>
     */

    @Override
    public void setPublicDomain(boolean publicDomain) {
        this.setPublicDomain(publicDomain);
        return;
    }


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @throws org.osid.NullArgumentException <code>copyright</code>
     *          is <code>null</code>
     */

    public void setCopyright(org.osid.locale.DisplayText copyright) {
        super.setCopyright(copyright);
        return;
    }


    /**
     *  Sets the copyright registration.
     *
     *  @param registration a copyright registration
     *  @throws org.osid.NullArgumentException
     *          <code>registration</code> is <code>null</code>
     */

    public void setCopyrightRegistration(String registration) {
        super.setCopyrightRegistration(registration);
        return;
    }


    /**
     *  Sets the can distribute verbatim flag.
     *
     *  @param status <code> true </code> if the asset can be
     *         distributed verbatim, <code> false </code> otherwise.
     */

    @Override
    public void setCanDistributeVerbatim(boolean status) {
        super.setCanDistributeVerbatim(status);
        return;
    }


    /**
     *  Sets the can distribute alterations flag.
     *
     *  @param status <code> true </code> if the asset can be
     *         modified, <code> false </code> otherwise. If <code>
     *         true, </code> <code> canDistributeVerbatim() </code>
     *         must also be <code> true.  </code>
     */

    @Override
    public void setCanDistributeAlterations(boolean status) {
        super.setCanDistributeAlterations(status);
        return;
    }


    /**
     *  Sets the can distribute compositions flag.
     *
     *  @param status <code> true </code> if the asset can be part of
     *         a larger composition <code> false </code> otherwise. If
     *         <code> true, </code> <code> canDistributeVerbatim()
     *         </code> must also be <code> true. </code>
     */

    @Override
    public void setCanDistributeCompositions(boolean status) {
        super.setCanDistributeCompositions(status);
        return;
    }


    /**
     *  Sets all distribution rights.
     * 
     *  @param canDistributeVerbatim <code> true </code> if the asset
     *         can be distributed verbatim, <code> false </code>
     *         otherwise.
     *  @param canDistributeAlterations <code> true </code> if the
     *         asset can be modified, <code> false </code>
     *         otherwise. If <code> true, </code> <code>
     *         canDistributeVerbatim() </code> must also be <code>
     *         true.  </code>
     *  @param canDistributeCompositions status <code> true </code> if
     *         the asset can be part of a larger composition <code>
     *         false </code> otherwise. If <code> true, </code> <code>
     *         canDistributeVerbatim() </code> must also be <code>
     *         true. </code>
     *  @throws org.osid.BadLogicException 
     */

    @Override
    public void setDistributionRights(boolean canDistributeVerbatim,
                                      boolean canDistributeAlterations,
                                      boolean canDistributeCompositions) {
        super.setDistributionRights(canDistributeVerbatim, 
                                    canDistributeAlterations,
                                    canDistributeCompositions);
        return;
    }
        

    /**
     *  Sets the source.
     *
     *  @param source a source
     *  @throws org.osid.NullArgumentException <code>source</code> is
     *          <code>null</code>
     */

    @Override
    public void setSource(org.osid.resource.Resource source) {
        super.setSource(source);
        return;
    }


    /**
     *  Adds a provider link.
     *
     *  @param providerLink a provider link
     *  @throws org.osid.NullArgumentException
     *          <code>providerLink</code> is <code>null</code>
     */

    @Override
    public void addProviderLink(org.osid.resource.Resource providerLink) {
        super.addProviderLink(providerLink);
        return;
    }


    /**
     *  Sets all the provider links.
     *
     *  @param providerLinks a collection of provider links
     *  @throws org.osid.NullArgumentException
     *          <code>providerLinks</code> is <code>null</code>
     */

    @Override
    public void setProviderLinks(java.util.Collection<org.osid.resource.Resource> providerLinks) {
        super.setProviderLinks(providerLinks);
        return;
    }


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreatedDate(org.osid.calendaring.DateTime date) {
        super.setCreatedDate(date);
        return;
    }


    /**
     *  Sets the published date.
     *
     *  @param date a published date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setPublishedDate(org.osid.calendaring.DateTime date) {
        super.setPublishedDate(date);
        return;
    }


    /**
     *  Sets the principal credit string.
     *
     *  @param credit a principal credit string
     *  @throws org.osid.NullArgumentException <code>credit</code> is
     *          <code>null</code>
     */

    @Override
    public void setPrincipalCreditString(org.osid.locale.DisplayText credit) {
        super.setPrincipalCreditString(credit);
        return;
    }


    /**
     *  Adds an asset content.
     *
     *  @param assetContent an asset content
     *  @throws org.osid.NullArgumentException
     *          <code>assetContent</code> is <code>null</code>
     */

    @Override
    public void addAssetContent(org.osid.repository.AssetContent assetContent) {
        super.addAssetContent(assetContent);
        return;
    }


    /**
     *  Sets all the asset contents.
     *
     *  @param assetContents a collection of asset contents
     *  @throws org.osid.NullArgumentException
     *          <code>assetContents</code> is <code>null</code>
     */

    @Override
    public void setAssetContents(java.util.Collection<org.osid.repository.AssetContent> assetContents) {
        super.setAssetContents(assetContents);
        return;
    }


    /**
     *  Sets the composition.
     *
     *  @param composition a composition
     *  @throws org.osid.NullArgumentException
     *          <code>composition</code> is <code>null</code>
     */

    @Override
    public void setComposition(org.osid.repository.Composition composition) {
        super.setComposition(composition);
        return;
    }
}

