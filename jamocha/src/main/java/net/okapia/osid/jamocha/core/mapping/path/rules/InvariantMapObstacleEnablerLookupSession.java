//
// InvariantMapObstacleEnablerLookupSession
//
//    Implements an ObstacleEnabler lookup service backed by a fixed collection of
//    obstacleEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules;


/**
 *  Implements an ObstacleEnabler lookup service backed by a fixed
 *  collection of obstacle enablers. The obstacle enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapObstacleEnablerLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.rules.spi.AbstractMapObstacleEnablerLookupSession
    implements org.osid.mapping.path.rules.ObstacleEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapObstacleEnablerLookupSession</code> with no
     *  obstacle enablers.
     *  
     *  @param map the map
     *  @throws org.osid.NullArgumnetException {@code map} is
     *          {@code null}
     */

    public InvariantMapObstacleEnablerLookupSession(org.osid.mapping.Map map) {
        setMap(map);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapObstacleEnablerLookupSession</code> with a single
     *  obstacle enabler.
     *  
     *  @param map the map
     *  @param obstacleEnabler an single obstacle enabler
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code obstacleEnabler} is <code>null</code>
     */

      public InvariantMapObstacleEnablerLookupSession(org.osid.mapping.Map map,
                                               org.osid.mapping.path.rules.ObstacleEnabler obstacleEnabler) {
        this(map);
        putObstacleEnabler(obstacleEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapObstacleEnablerLookupSession</code> using an array
     *  of obstacle enablers.
     *  
     *  @param map the map
     *  @param obstacleEnablers an array of obstacle enablers
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code obstacleEnablers} is <code>null</code>
     */

      public InvariantMapObstacleEnablerLookupSession(org.osid.mapping.Map map,
                                               org.osid.mapping.path.rules.ObstacleEnabler[] obstacleEnablers) {
        this(map);
        putObstacleEnablers(obstacleEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapObstacleEnablerLookupSession</code> using a
     *  collection of obstacle enablers.
     *
     *  @param map the map
     *  @param obstacleEnablers a collection of obstacle enablers
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code obstacleEnablers} is <code>null</code>
     */

      public InvariantMapObstacleEnablerLookupSession(org.osid.mapping.Map map,
                                               java.util.Collection<? extends org.osid.mapping.path.rules.ObstacleEnabler> obstacleEnablers) {
        this(map);
        putObstacleEnablers(obstacleEnablers);
        return;
    }
}
