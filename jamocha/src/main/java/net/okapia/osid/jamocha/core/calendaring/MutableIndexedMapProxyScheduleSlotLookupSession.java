//
// MutableIndexedMapProxyScheduleSlotLookupSession
//
//    Implements a ScheduleSlot lookup service backed by a collection of
//    scheduleSlots indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a ScheduleSlot lookup service backed by a collection of
 *  scheduleSlots. The schedule slots are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some scheduleSlots may be compatible
 *  with more types than are indicated through these scheduleSlot
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of schedule slots can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyScheduleSlotLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractIndexedMapScheduleSlotLookupSession
    implements org.osid.calendaring.ScheduleSlotLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyScheduleSlotLookupSession} with
     *  no schedule slot.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyScheduleSlotLookupSession(org.osid.calendaring.Calendar calendar,
                                                       org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyScheduleSlotLookupSession} with
     *  a single schedule slot.
     *
     *  @param calendar the calendar
     *  @param  scheduleSlot an schedule slot
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code scheduleSlot}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyScheduleSlotLookupSession(org.osid.calendaring.Calendar calendar,
                                                       org.osid.calendaring.ScheduleSlot scheduleSlot, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putScheduleSlot(scheduleSlot);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyScheduleSlotLookupSession} using
     *  an array of schedule slots.
     *
     *  @param calendar the calendar
     *  @param  scheduleSlots an array of schedule slots
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code scheduleSlots}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyScheduleSlotLookupSession(org.osid.calendaring.Calendar calendar,
                                                       org.osid.calendaring.ScheduleSlot[] scheduleSlots, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putScheduleSlots(scheduleSlots);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyScheduleSlotLookupSession} using
     *  a collection of schedule slots.
     *
     *  @param calendar the calendar
     *  @param  scheduleSlots a collection of schedule slots
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code scheduleSlots}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyScheduleSlotLookupSession(org.osid.calendaring.Calendar calendar,
                                                       java.util.Collection<? extends org.osid.calendaring.ScheduleSlot> scheduleSlots,
                                                       org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putScheduleSlots(scheduleSlots);
        return;
    }

    
    /**
     *  Makes a {@code ScheduleSlot} available in this session.
     *
     *  @param  scheduleSlot a schedule slot
     *  @throws org.osid.NullArgumentException {@code scheduleSlot{@code 
     *          is {@code null}
     */

    @Override
    public void putScheduleSlot(org.osid.calendaring.ScheduleSlot scheduleSlot) {
        super.putScheduleSlot(scheduleSlot);
        return;
    }


    /**
     *  Makes an array of schedule slots available in this session.
     *
     *  @param  scheduleSlots an array of schedule slots
     *  @throws org.osid.NullArgumentException {@code scheduleSlots{@code 
     *          is {@code null}
     */

    @Override
    public void putScheduleSlots(org.osid.calendaring.ScheduleSlot[] scheduleSlots) {
        super.putScheduleSlots(scheduleSlots);
        return;
    }


    /**
     *  Makes collection of schedule slots available in this session.
     *
     *  @param  scheduleSlots a collection of schedule slots
     *  @throws org.osid.NullArgumentException {@code scheduleSlot{@code 
     *          is {@code null}
     */

    @Override
    public void putScheduleSlots(java.util.Collection<? extends org.osid.calendaring.ScheduleSlot> scheduleSlots) {
        super.putScheduleSlots(scheduleSlots);
        return;
    }


    /**
     *  Removes a ScheduleSlot from this session.
     *
     *  @param scheduleSlotId the {@code Id} of the schedule slot
     *  @throws org.osid.NullArgumentException {@code scheduleSlotId{@code  is
     *          {@code null}
     */

    @Override
    public void removeScheduleSlot(org.osid.id.Id scheduleSlotId) {
        super.removeScheduleSlot(scheduleSlotId);
        return;
    }    
}
