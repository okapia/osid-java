//
// AbstractAssemblyObjectiveQuery.java
//
//     An ObjectiveQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.learning.objective.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ObjectiveQuery that stores terms.
 */

public abstract class AbstractAssemblyObjectiveQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.learning.ObjectiveQuery,
               org.osid.learning.ObjectiveQueryInspector,
               org.osid.learning.ObjectiveSearchOrder {

    private final java.util.Collection<org.osid.learning.records.ObjectiveQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.learning.records.ObjectiveQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.learning.records.ObjectiveSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyObjectiveQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyObjectiveQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessment(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAssessmentColumn(), style);
        return;
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available for querying 
     *  activities. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches an objective that has any assessment assigned. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          assessment, <code> false </code> to match objectives with no 
     *          assessment 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentColumn(), match);
        return;
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the asset terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssessmentTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Tests if an <code> AssessmentSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an assessment search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an assessment. 
     *
     *  @return the assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentSearchOrder() is false");
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Sets the knowledge category <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchKnowledgeCategoryId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getKnowledgeCategoryIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the knowledge category <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearKnowledgeCategoryIdTerms() {
        getAssembler().clearTerms(getKnowledgeCategoryIdColumn());
        return;
    }


    /**
     *  Gets the knowledge category <code> Id </code> query terms. 
     *
     *  @return the knowledge category <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getKnowledgeCategoryIdTerms() {
        return (getAssembler().getIdTerms(getKnowledgeCategoryIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the knowledge 
     *  category. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByKnowledgeCategory(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getKnowledgeCategoryColumn(), style);
        return;
    }


    /**
     *  Gets the KnowledgeCategoryId column name.
     *
     * @return the column name
     */

    protected String getKnowledgeCategoryIdColumn() {
        return ("knowledge_category_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available for querying 
     *  knowledge categories. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKnowledgeCategoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a knowledge category. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKnowledgeCategoryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getKnowledgeCategoryQuery() {
        throw new org.osid.UnimplementedException("supportsKnowledgeCategoryQuery() is false");
    }


    /**
     *  Matches an objective that has any knowledge category. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          knowledge category, <code> false </code> to match objectives 
     *          with no knowledge category 
     */

    @OSID @Override
    public void matchAnyKnowledgeCategory(boolean match) {
        getAssembler().addIdWildcardTerm(getKnowledgeCategoryColumn(), match);
        return;
    }


    /**
     *  Clears the knowledge category terms. 
     */

    @OSID @Override
    public void clearKnowledgeCategoryTerms() {
        getAssembler().clearTerms(getKnowledgeCategoryColumn());
        return;
    }


    /**
     *  Gets the knowledge category query terms. 
     *
     *  @return the knowledge category terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getKnowledgeCategoryTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKnowledgeCategorySearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order to order on knolwgedge category. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKnowledgeCategorySearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getKnowledgeCategorySearchOrder() {
        throw new org.osid.UnimplementedException("supportsKnowledgeCategorySearchOrder() is false");
    }


    /**
     *  Gets the KnowledgeCategory column name.
     *
     * @return the column name
     */

    protected String getKnowledgeCategoryColumn() {
        return ("knowledge_category");
    }


    /**
     *  Sets the cognitive process <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCognitiveProcessId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getCognitiveProcessIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the cognitive process <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCognitiveProcessIdTerms() {
        getAssembler().clearTerms(getCognitiveProcessIdColumn());
        return;
    }


    /**
     *  Gets the cognitive process <code> Id </code> query terms. 
     *
     *  @return the cognitive process <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCognitiveProcessIdTerms() {
        return (getAssembler().getIdTerms(getCognitiveProcessIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the cognitive 
     *  process. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCognitiveProcess(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCognitiveProcessColumn(), style);
        return;
    }


    /**
     *  Gets the CognitiveProcessId column name.
     *
     * @return the column name
     */

    protected String getCognitiveProcessIdColumn() {
        return ("cognitive_process_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available for querying 
     *  cognitive processes. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCognitiveProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cognitive process. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCognitiveProcessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getCognitiveProcessQuery() {
        throw new org.osid.UnimplementedException("supportsCognitiveProcessQuery() is false");
    }


    /**
     *  Matches an objective that has any cognitive process. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          cognitive process, <code> false </code> to match objectives 
     *          with no cognitive process 
     */

    @OSID @Override
    public void matchAnyCognitiveProcess(boolean match) {
        getAssembler().addIdWildcardTerm(getCognitiveProcessColumn(), match);
        return;
    }


    /**
     *  Clears the cognitive process terms. 
     */

    @OSID @Override
    public void clearCognitiveProcessTerms() {
        getAssembler().clearTerms(getCognitiveProcessColumn());
        return;
    }


    /**
     *  Gets the cognitive process query terms. 
     *
     *  @return the cognitive process terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getCognitiveProcessTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCognitiveProcessSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order to order on cognitive process. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCognitiveProcessSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getCognitiveProcessSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCognitiveProcessSearchOrder() is false");
    }


    /**
     *  Gets the CognitiveProcess column name.
     *
     * @return the column name
     */

    protected String getCognitiveProcessColumn() {
        return ("cognitive_process");
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available for querying 
     *  activities. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches an objective that has any related activity. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          activity, <code> false </code> to match objectives with no 
     *          activity 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.learning.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.learning.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Sets the requisite objective <code> Id </code> for this query. 
     *
     *  @param  requisiteObjectiveId a requisite objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requisiteObjectiveId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRequisiteObjectiveId(org.osid.id.Id requisiteObjectiveId, 
                                          boolean match) {
        getAssembler().addIdTerm(getRequisiteObjectiveIdColumn(), requisiteObjectiveId, match);
        return;
    }


    /**
     *  Clears the requisite objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRequisiteObjectiveIdTerms() {
        getAssembler().clearTerms(getRequisiteObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the requisite objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequisiteObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getRequisiteObjectiveIdColumn()));
    }


    /**
     *  Gets the RequisiteObjectiveId column name.
     *
     * @return the column name
     */

    protected String getRequisiteObjectiveIdColumn() {
        return ("requisite_objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  requisite objectives. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite objective. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getRequisiteObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsRequisiteObjectiveQuery() is false");
    }


    /**
     *  Matches an objective that has any related requisite. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          requisite, <code> false </code> to match objectives with no 
     *          requisite 
     */

    @OSID @Override
    public void matchAnyRequisiteObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getRequisiteObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the requisite objective terms. 
     */

    @OSID @Override
    public void clearRequisiteObjectiveTerms() {
        getAssembler().clearTerms(getRequisiteObjectiveColumn());
        return;
    }


    /**
     *  Gets the requisite objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getRequisiteObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the RequisiteObjective column name.
     *
     * @return the column name
     */

    protected String getRequisiteObjectiveColumn() {
        return ("requisite_objective");
    }


    /**
     *  Sets the dependent objective <code> Id </code> to query objectives 
     *  dependent on the given objective. 
     *
     *  @param  dependentObjectiveId a dependent objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dependentObjectiveId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDependentObjectiveId(org.osid.id.Id dependentObjectiveId, 
                                          boolean match) {
        getAssembler().addIdTerm(getDependentObjectiveIdColumn(), dependentObjectiveId, match);
        return;
    }


    /**
     *  Clears the dependent objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDependentObjectiveIdTerms() {
        getAssembler().clearTerms(getDependentObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the requisite objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDependentObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getDependentObjectiveIdColumn()));
    }


    /**
     *  Gets the DependentObjectiveId column name.
     *
     * @return the column name
     */

    protected String getDependentObjectiveIdColumn() {
        return ("dependent_objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  dependent objectives. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepndentObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dependent objective. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDependentObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getDependentObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsDependentObjectiveQuery() is false");
    }


    /**
     *  Matches an objective that has any related dependents. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          dependent, <code> false </code> to match objectives with no 
     *          dependents 
     */

    @OSID @Override
    public void matchAnyDependentObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getDependentObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the dependent objective terms. 
     */

    @OSID @Override
    public void clearDependentObjectiveTerms() {
        getAssembler().clearTerms(getDependentObjectiveColumn());
        return;
    }


    /**
     *  Gets the requisite objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getDependentObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the DependentObjective column name.
     *
     * @return the column name
     */

    protected String getDependentObjectiveColumn() {
        return ("dependent_objective");
    }


    /**
     *  Sets the equivalent objective <code> Id </code> to query equivalents. 
     *
     *  @param  equivalentObjectiveId an equivalent objective <code> Id 
     *          </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> equivalentObjectiveId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEquivalentObjectiveId(org.osid.id.Id equivalentObjectiveId, 
                                           boolean match) {
        getAssembler().addIdTerm(getEquivalentObjectiveIdColumn(), equivalentObjectiveId, match);
        return;
    }


    /**
     *  Clears the equivalent objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEquivalentObjectiveIdTerms() {
        getAssembler().clearTerms(getEquivalentObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the equivalent objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEquivalentObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getEquivalentObjectiveIdColumn()));
    }


    /**
     *  Gets the EquivalentObjectiveId column name.
     *
     * @return the column name
     */

    protected String getEquivalentObjectiveIdColumn() {
        return ("equivalent_objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  equivalent objectives. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEquivalentObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an equivalent objective. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEquivalentObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getEquivalentObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsEquivalentObjectiveQuery() is false");
    }


    /**
     *  Matches an objective that has any related equivalents. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          equivalent, <code> false </code> to match objectives with no 
     *          equivalents 
     */

    @OSID @Override
    public void matchAnyEquivalentObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getEquivalentObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the equivalent objective terms. 
     */

    @OSID @Override
    public void clearEquivalentObjectiveTerms() {
        getAssembler().clearTerms(getEquivalentObjectiveColumn());
        return;
    }


    /**
     *  Gets the equivalent objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getEquivalentObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the EquivalentObjective column name.
     *
     * @return the column name
     */

    protected String getEquivalentObjectiveColumn() {
        return ("equivalent_objective");
    }


    /**
     *  Sets the objective <code> Id </code> for this query to match 
     *  objectives that have the specified objective as an ancestor. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAncestorObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the ancestor objective <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorObjectiveIdTerms() {
        getAssembler().clearTerms(getAncestorObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the ancestor objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getAncestorObjectiveIdColumn()));
    }


    /**
     *  Gets the AncestorObjectiveId column name.
     *
     * @return the column name
     */

    protected String getAncestorObjectiveIdColumn() {
        return ("ancestor_objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getAncestorObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorObjectiveQuery() is false");
    }


    /**
     *  Matches objectives that have any ancestor. 
     *
     *  @param  match <code> true </code> to match objective with any 
     *          ancestor, <code> false </code> to match root objectives 
     */

    @OSID @Override
    public void matchAnyAncestorObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor objective query terms. 
     */

    @OSID @Override
    public void clearAncestorObjectiveTerms() {
        getAssembler().clearTerms(getAncestorObjectiveColumn());
        return;
    }


    /**
     *  Gets the ancestor objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getAncestorObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the AncestorObjective column name.
     *
     * @return the column name
     */

    protected String getAncestorObjectiveColumn() {
        return ("ancestor_objective");
    }


    /**
     *  Sets the objective <code> Id </code> for this query to match 
     *  objectives that have the specified objective as a descendant. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantObjectiveId(org.osid.id.Id objectiveId, 
                                           boolean match) {
        getAssembler().addIdTerm(getDescendantObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the descendant objective <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantObjectiveIdTerms() {
        getAssembler().clearTerms(getDescendantObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the descendant objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getDescendantObjectiveIdColumn()));
    }


    /**
     *  Gets the DescendantObjectiveId column name.
     *
     * @return the column name
     */

    protected String getDescendantObjectiveIdColumn() {
        return ("descendant_objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getDescendantObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantObjectiveQuery() is false");
    }


    /**
     *  Matches objectives that have any ancestor. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          ancestor, <code> false </code> to match leaf objectives 
     */

    @OSID @Override
    public void matchAnyDescendantObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the descendant objective query terms. 
     */

    @OSID @Override
    public void clearDescendantObjectiveTerms() {
        getAssembler().clearTerms(getDescendantObjectiveColumn());
        return;
    }


    /**
     *  Gets the descendant objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getDescendantObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the DescendantObjective column name.
     *
     * @return the column name
     */

    protected String getDescendantObjectiveColumn() {
        return ("descendant_objective");
    }


    /**
     *  Sets the objective bank <code> Id </code> for this query. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveBankId(org.osid.id.Id objectiveBankId, 
                                     boolean match) {
        getAssembler().addIdTerm(getObjectiveBankIdColumn(), objectiveBankId, match);
        return;
    }


    /**
     *  Clears the objective bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveBankIdTerms() {
        getAssembler().clearTerms(getObjectiveBankIdColumn());
        return;
    }


    /**
     *  Gets the objective bank <code> Id </code> query terms. 
     *
     *  @return the objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveBankIdTerms() {
        return (getAssembler().getIdTerms(getObjectiveBankIdColumn()));
    }


    /**
     *  Gets the ObjectiveBankId column name.
     *
     * @return the column name
     */

    protected String getObjectiveBankIdColumn() {
        return ("objective_bank_id");
    }


    /**
     *  Tests if a <code> ObjectiveBankQuery </code> is available for querying 
     *  objective banks. 
     *
     *  @return <code> true </code> if an objective bank query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the objective bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuery getObjectiveBankQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveBankQuery() is false");
    }


    /**
     *  Clears the objective bank terms. 
     */

    @OSID @Override
    public void clearObjectiveBankTerms() {
        getAssembler().clearTerms(getObjectiveBankColumn());
        return;
    }


    /**
     *  Gets the objective bank query terms. 
     *
     *  @return the objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }


    /**
     *  Gets the ObjectiveBank column name.
     *
     * @return the column name
     */

    protected String getObjectiveBankColumn() {
        return ("objective_bank");
    }


    /**
     *  Tests if this objective supports the given record
     *  <code>Type</code>.
     *
     *  @param  objectiveRecordType an objective record type 
     *  @return <code>true</code> if the objectiveRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type objectiveRecordType) {
        for (org.osid.learning.records.ObjectiveQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  objectiveRecordType the objective record type 
     *  @return the objective query record 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveQueryRecord getObjectiveQueryRecord(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  objectiveRecordType the objective record type 
     *  @return the objective query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveQueryInspectorRecord getObjectiveQueryInspectorRecord(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param objectiveRecordType the objective record type
     *  @return the objective search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveSearchOrderRecord getObjectiveSearchOrderRecord(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this objective. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param objectiveQueryRecord the objective query record
     *  @param objectiveQueryInspectorRecord the objective query inspector
     *         record
     *  @param objectiveSearchOrderRecord the objective search order record
     *  @param objectiveRecordType objective record type
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveQueryRecord</code>,
     *          <code>objectiveQueryInspectorRecord</code>,
     *          <code>objectiveSearchOrderRecord</code> or
     *          <code>objectiveRecordTypeobjective</code> is
     *          <code>null</code>
     */
            
    protected void addObjectiveRecords(org.osid.learning.records.ObjectiveQueryRecord objectiveQueryRecord, 
                                      org.osid.learning.records.ObjectiveQueryInspectorRecord objectiveQueryInspectorRecord, 
                                      org.osid.learning.records.ObjectiveSearchOrderRecord objectiveSearchOrderRecord, 
                                      org.osid.type.Type objectiveRecordType) {

        addRecordType(objectiveRecordType);

        nullarg(objectiveQueryRecord, "objective query record");
        nullarg(objectiveQueryInspectorRecord, "objective query inspector record");
        nullarg(objectiveSearchOrderRecord, "objective search odrer record");

        this.queryRecords.add(objectiveQueryRecord);
        this.queryInspectorRecords.add(objectiveQueryInspectorRecord);
        this.searchOrderRecords.add(objectiveSearchOrderRecord);
        
        return;
    }
}
