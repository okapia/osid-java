//
// AbstractIndexedMapSyllabusLookupSession.java
//
//    A simple framework for providing a Syllabus lookup service
//    backed by a fixed collection of syllabi with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Syllabus lookup service backed by a
 *  fixed collection of syllabi. The syllabi are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some syllabi may be compatible
 *  with more types than are indicated through these syllabus
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Syllabi</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSyllabusLookupSession
    extends AbstractMapSyllabusLookupSession
    implements org.osid.course.syllabus.SyllabusLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.syllabus.Syllabus> syllabiByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.syllabus.Syllabus>());
    private final MultiMap<org.osid.type.Type, org.osid.course.syllabus.Syllabus> syllabiByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.syllabus.Syllabus>());


    /**
     *  Makes a <code>Syllabus</code> available in this session.
     *
     *  @param  syllabus a syllabus
     *  @throws org.osid.NullArgumentException <code>syllabus<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSyllabus(org.osid.course.syllabus.Syllabus syllabus) {
        super.putSyllabus(syllabus);

        this.syllabiByGenus.put(syllabus.getGenusType(), syllabus);
        
        try (org.osid.type.TypeList types = syllabus.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.syllabiByRecord.put(types.getNextType(), syllabus);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a syllabus from this session.
     *
     *  @param syllabusId the <code>Id</code> of the syllabus
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSyllabus(org.osid.id.Id syllabusId) {
        org.osid.course.syllabus.Syllabus syllabus;
        try {
            syllabus = getSyllabus(syllabusId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.syllabiByGenus.remove(syllabus.getGenusType());

        try (org.osid.type.TypeList types = syllabus.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.syllabiByRecord.remove(types.getNextType(), syllabus);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSyllabus(syllabusId);
        return;
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  syllabus genus <code>Type</code> which does not include
     *  syllabi of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known syllabi or an error results. Otherwise,
     *  the returned list may contain only those syllabi that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  syllabusGenusType a syllabus genus type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByGenusType(org.osid.type.Type syllabusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.syllabus.syllabus.ArraySyllabusList(this.syllabiByGenus.get(syllabusGenusType)));
    }


    /**
     *  Gets a <code>SyllabusList</code> containing the given
     *  syllabus record <code>Type</code>. In plenary mode, the
     *  returned list contains all known syllabi or an error
     *  results. Otherwise, the returned list may contain only those
     *  syllabi that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  syllabusRecordType a syllabus record type 
     *  @return the returned <code>syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByRecordType(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.syllabus.syllabus.ArraySyllabusList(this.syllabiByRecord.get(syllabusRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.syllabiByGenus.clear();
        this.syllabiByRecord.clear();

        super.close();

        return;
    }
}
