//
// AbstractParameterProcessorQueryInspector.java
//
//     A template for making a ParameterProcessorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.parameterprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for parameter processors.
 */

public abstract class AbstractParameterProcessorQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQueryInspector
    implements org.osid.configuration.rules.ParameterProcessorQueryInspector {

    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the parameter <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledParameterIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the parameter query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQueryInspector[] getRuledParameterTerms() {
        return (new org.osid.configuration.ParameterQueryInspector[0]);
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given parameter processor query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a parameter processor implementing the requested record.
     *
     *  @param parameterProcessorRecordType a parameter processor record type
     *  @return the parameter processor query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorQueryInspectorRecord getParameterProcessorQueryInspectorRecord(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(parameterProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter processor query. 
     *
     *  @param parameterProcessorQueryInspectorRecord parameter processor query inspector
     *         record
     *  @param parameterProcessorRecordType parameterProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParameterProcessorQueryInspectorRecord(org.osid.configuration.rules.records.ParameterProcessorQueryInspectorRecord parameterProcessorQueryInspectorRecord, 
                                                   org.osid.type.Type parameterProcessorRecordType) {

        addRecordType(parameterProcessorRecordType);
        nullarg(parameterProcessorRecordType, "parameter processor record type");
        this.records.add(parameterProcessorQueryInspectorRecord);        
        return;
    }
}
