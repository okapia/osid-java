//
// AbstractMutableSchedule.java
//
//     Defines a mutable Schedule.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.schedule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Schedule</code>.
 */

public abstract class AbstractMutableSchedule
    extends net.okapia.osid.jamocha.calendaring.schedule.spi.AbstractSchedule
    implements org.osid.calendaring.Schedule,
               net.okapia.osid.jamocha.builder.calendaring.schedule.ScheduleMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this schedule. 
     *
     *  @param record schedule record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addScheduleRecord(org.osid.calendaring.records.ScheduleRecord record, org.osid.type.Type recordType) {
        super.addScheduleRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this schedule.
     *
     *  @param displayName the name for this schedule
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this schedule.
     *
     *  @param description the description of this schedule
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the schedule slot.
     *
     *  @param scheduleSlot a schedule slot
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlot</code> is <code>null</code>
     */

    @Override
    public void setScheduleSlot(org.osid.calendaring.ScheduleSlot scheduleSlot) {
        super.setScheduleSlot(scheduleSlot);
        return;
    }


    /**
     *  Sets the time period.
     *
     *  @param timePeriod a time period
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriod</code> is <code>null</code>
     */

    @Override
    public void setTimePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        super.setTimePeriod(timePeriod);
        return;
    }


    /**
     *  Sets the schedule start.
     *
     *  @param scheduleStart a schedule start
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleStart</code> is <code>null</code>
     */

    @Override
    public void setScheduleStart(org.osid.calendaring.DateTime scheduleStart) {
        super.setScheduleStart(scheduleStart);
        return;
    }


    /**
     *  Sets the schedule end.
     *
     *  @param scheduleEnd a schedule end
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleEnd</code> is <code>null</code>
     */

    @Override
    public void setScheduleEnd(org.osid.calendaring.DateTime scheduleEnd) {
        super.setScheduleEnd(scheduleEnd);
        return;
    }


    /**
     *  Sets the limit.
     *
     *  @param limit a limit
     *  @throws org.osid.InvalidArgumentException <code>limit</code>
     *          is negative
     */

    @Override
    public void setLimit(long limit) {
        super.setLimit(limit);
        return;
    }


    /**
     *  Sets the total duration.
     *
     *  @param duration the total duration
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    @Override
    public void setTotalDuration(org.osid.calendaring.Duration duration) {
        super.setTotalDuration(duration);
        return;
    }


    /**
     *  Sets the location description.
     *
     *  @param locationDescription a location description
     *  @throws org.osid.NullArgumentException
     *          <code>locationDescription</code> is <code>null</code>
     */

    @Override
    public void setLocationDescription(org.osid.locale.DisplayText locationDescription) {
        super.setLocationDescription(locationDescription);
        return;
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    @Override
    public void setLocation(org.osid.mapping.Location location) {
        super.setLocation(location);
        return;
    }
}

