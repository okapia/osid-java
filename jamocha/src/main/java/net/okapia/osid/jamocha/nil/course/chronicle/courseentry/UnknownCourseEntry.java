//
// UnknownCourseEntry.java
//
//     Defines an unknown CourseEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.course.chronicle.courseentry;


/**
 *  Defines an unknown <code>CourseEntry</code>.
 */

public final class UnknownCourseEntry
    extends net.okapia.osid.jamocha.nil.course.chronicle.courseentry.spi.AbstractUnknownCourseEntry
    implements org.osid.course.chronicle.CourseEntry {


    /**
     *  Constructs a new <code>UnknownCourseEntry</code>.
     */

    public UnknownCourseEntry() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownCourseEntry</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownCourseEntry(boolean optional) {
        super(optional);
        addCourseEntryRecord(new CourseEntryRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown CourseEntry.
     *
     *  @return an unknown CourseEntry
     */

    public static org.osid.course.chronicle.CourseEntry create() {
        return (net.okapia.osid.jamocha.builder.validator.course.chronicle.courseentry.CourseEntryValidator.validateCourseEntry(new UnknownCourseEntry()));
    }


    public class CourseEntryRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.course.chronicle.records.CourseEntryRecord {

        
        protected CourseEntryRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
