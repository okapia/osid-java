//
// AbstractMapRecurringEventLookupSession
//
//    A simple framework for providing a RecurringEvent lookup service
//    backed by a fixed collection of recurring events.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a RecurringEvent lookup service backed by a
 *  fixed collection of recurring events. The recurring events are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RecurringEvents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRecurringEventLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractRecurringEventLookupSession
    implements org.osid.calendaring.RecurringEventLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.RecurringEvent> recurringEvents = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.RecurringEvent>());


    /**
     *  Makes a <code>RecurringEvent</code> available in this session.
     *
     *  @param  recurringEvent a recurring event
     *  @throws org.osid.NullArgumentException <code>recurringEvent<code>
     *          is <code>null</code>
     */

    protected void putRecurringEvent(org.osid.calendaring.RecurringEvent recurringEvent) {
        this.recurringEvents.put(recurringEvent.getId(), recurringEvent);
        return;
    }


    /**
     *  Makes an array of recurring events available in this session.
     *
     *  @param  recurringEvents an array of recurring events
     *  @throws org.osid.NullArgumentException <code>recurringEvents<code>
     *          is <code>null</code>
     */

    protected void putRecurringEvents(org.osid.calendaring.RecurringEvent[] recurringEvents) {
        putRecurringEvents(java.util.Arrays.asList(recurringEvents));
        return;
    }


    /**
     *  Makes a collection of recurring events available in this session.
     *
     *  @param  recurringEvents a collection of recurring events
     *  @throws org.osid.NullArgumentException <code>recurringEvents<code>
     *          is <code>null</code>
     */

    protected void putRecurringEvents(java.util.Collection<? extends org.osid.calendaring.RecurringEvent> recurringEvents) {
        for (org.osid.calendaring.RecurringEvent recurringEvent : recurringEvents) {
            this.recurringEvents.put(recurringEvent.getId(), recurringEvent);
        }

        return;
    }


    /**
     *  Removes a RecurringEvent from this session.
     *
     *  @param  recurringEventId the <code>Id</code> of the recurring event
     *  @throws org.osid.NullArgumentException <code>recurringEventId<code> is
     *          <code>null</code>
     */

    protected void removeRecurringEvent(org.osid.id.Id recurringEventId) {
        this.recurringEvents.remove(recurringEventId);
        return;
    }


    /**
     *  Gets the <code>RecurringEvent</code> specified by its <code>Id</code>.
     *
     *  @param  recurringEventId <code>Id</code> of the <code>RecurringEvent</code>
     *  @return the recurringEvent
     *  @throws org.osid.NotFoundException <code>recurringEventId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>recurringEventId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEvent getRecurringEvent(org.osid.id.Id recurringEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.RecurringEvent recurringEvent = this.recurringEvents.get(recurringEventId);
        if (recurringEvent == null) {
            throw new org.osid.NotFoundException("recurringEvent not found: " + recurringEventId);
        }

        return (recurringEvent);
    }


    /**
     *  Gets all <code>RecurringEvents</code>. In plenary mode, the returned
     *  list contains all known recurringEvents or an error
     *  results. Otherwise, the returned list may contain only those
     *  recurringEvents that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>RecurringEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.recurringevent.ArrayRecurringEventList(this.recurringEvents.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.recurringEvents.clear();
        super.close();
        return;
    }
}
