//
// AbstractItemQuery.java
//
//     A template for making an Item Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for items.
 */

public abstract class AbstractItemQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.inventory.ItemQuery {

    private final java.util.Collection<org.osid.inventory.records.ItemQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the stock <code> Id </code> for this query. 
     *
     *  @param  stockId the stock <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStockId(org.osid.id.Id stockId, boolean match) {
        return;
    }


    /**
     *  Clears the stock <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStockIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stocjk. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getStockQuery() {
        throw new org.osid.UnimplementedException("supportsStockQuery() is false");
    }


    /**
     *  Matches items that have any stock. 
     *
     *  @param  match <code> true </code> to match items with any stock, 
     *          <code> false </code> to match items with no stock 
     */

    @OSID @Override
    public void matchAnyStock(boolean match) {
        return;
    }


    /**
     *  Clears the stock terms. 
     */

    @OSID @Override
    public void clearStockTerms() {
        return;
    }


    /**
     *  Matches a property identification number. 
     *
     *  @param  property a property number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> property </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPropertyTag(String property, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches items that have any property tag. 
     *
     *  @param  match <code> true </code> to match items with any property 
     *          tag, <code> false </code> to match items with no property tag 
     */

    @OSID @Override
    public void matchAnyPropertyTag(boolean match) {
        return;
    }


    /**
     *  Clears the property tag terms. 
     */

    @OSID @Override
    public void clearPropertyTagTerms() {
        return;
    }


    /**
     *  Matches a serial number. 
     *
     *  @param  serial a serial number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> serial </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSerialNumber(String serial, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches items that have any serial number. 
     *
     *  @param  match <code> true </code> to match items with any serial 
     *          number, <code> false </code> to match items with no serial 
     *          number 
     */

    @OSID @Override
    public void matchAnySerialNumber(boolean match) {
        return;
    }


    /**
     *  Clears the serial number terms. 
     */

    @OSID @Override
    public void clearSerialNumberTerms() {
        return;
    }


    /**
     *  Matches a location description. 
     *
     *  @param  location a location string 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        return;
    }


    /**
     *  Matches items that have any location description. 
     *
     *  @param  match <code> true </code> to match items with any location 
     *          string, <code> false </code> to match items with no location 
     *          string 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches items that have any location. 
     *
     *  @param  match <code> true </code> to match items with any location, 
     *          <code> false </code> to match items with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        return;
    }


    /**
     *  Sets the item <code> Id </code> for this query to match items part of 
     *  the given item. 
     *
     *  @param  itemId the item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if a item query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches items that have any item. 
     *
     *  @param  match <code> true </code> to match items part of any item, 
     *          <code> false </code> to match items psrt of no item 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match items 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given item query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an item implementing the requested record.
     *
     *  @param itemRecordType an item record type
     *  @return the item query record
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ItemQueryRecord getItemQueryRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ItemQueryRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item query. 
     *
     *  @param itemQueryRecord item query record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addItemQueryRecord(org.osid.inventory.records.ItemQueryRecord itemQueryRecord, 
                                          org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);
        nullarg(itemQueryRecord, "item query record");
        this.records.add(itemQueryRecord);        
        return;
    }
}
