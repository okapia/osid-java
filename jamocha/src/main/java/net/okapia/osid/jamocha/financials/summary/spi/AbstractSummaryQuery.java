//
// AbstractSummaryQuery.java
//
//     A template for making a Summary Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.summary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for summaries.
 */

public abstract class AbstractSummaryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendiumQuery
    implements org.osid.financials.SummaryQuery {

    private final java.util.Collection<org.osid.financials.records.SummaryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches an account <code> Id. </code> 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAccountId(org.osid.id.Id accountId, boolean match) {
        return;
    }


    /**
     *  Clears the account <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAccountIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAccountQuery() is false");
    }


    /**
     *  Clears the account terms. 
     */

    @OSID @Override
    public void clearAccountTerms() {
        return;
    }


    /**
     *  Sets the fiscal period <code> Id </code> for this query. 
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchFiscalPeriodId(org.osid.id.Id fiscalPeriodId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the fiscal period <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FiscalPeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a fiscal period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the fiscal period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuery getFiscalPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodQuery() is false");
    }


    /**
     *  Clears the fiscal period query terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodTerms() {
        return;
    }


    /**
     *  Matches credits within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCredits(org.osid.financials.Currency from, 
                             org.osid.financials.Currency to, boolean match) {
        return;
    }


    /**
     *  Clears the credits terms. 
     */

    @OSID @Override
    public void clearCreditsTerms() {
        return;
    }


    /**
     *  Matches debits within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDebits(org.osid.financials.Currency from, 
                            org.osid.financials.Currency to, boolean match) {
        return;
    }


    /**
     *  Clears the debits terms. 
     */

    @OSID @Override
    public void clearDebitsTerms() {
        return;
    }


    /**
     *  Matches summaries with a delta between debits and credits. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchBalance(java.math.BigDecimal from, 
                             java.math.BigDecimal to, boolean match) {
        return;
    }


    /**
     *  Clears the balance terms. 
     */

    @OSID @Override
    public void clearBalanceTerms() {
        return;
    }


    /**
     *  Matches a budget within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBudget(org.osid.financials.Currency from, 
                            org.osid.financials.Currency to, boolean match) {
        return;
    }


    /**
     *  Clears the budget terms. 
     */

    @OSID @Override
    public void clearBudgetTerms() {
        return;
    }


    /**
     *  Matches summaries with a delta between the balance and the budget. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchDelta(java.math.BigDecimal from, java.math.BigDecimal to, 
                           boolean match) {
        return;
    }


    /**
     *  Clears the delta terms. 
     */

    @OSID @Override
    public void clearDeltaTerms() {
        return;
    }


    /**
     *  Matches a forecast within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchForecast(org.osid.financials.Currency from, 
                              org.osid.financials.Currency to, boolean match) {
        return;
    }


    /**
     *  Clears the forecast terms. 
     */

    @OSID @Override
    public void clearForecastTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given summary query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a summary implementing the requested record.
     *
     *  @param summaryRecordType a summary record type
     *  @return the summary query record
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(summaryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.SummaryQueryRecord getSummaryQueryRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.SummaryQueryRecord record : this.records) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(summaryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this summary query. 
     *
     *  @param summaryQueryRecord summary query record
     *  @param summaryRecordType summary record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSummaryQueryRecord(org.osid.financials.records.SummaryQueryRecord summaryQueryRecord, 
                                          org.osid.type.Type summaryRecordType) {

        addRecordType(summaryRecordType);
        nullarg(summaryQueryRecord, "summary query record");
        this.records.add(summaryQueryRecord);        
        return;
    }
}
