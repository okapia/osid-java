//
// MutableMapJournalLookupSession
//
//    Implements a Journal lookup service backed by a collection of
//    journals that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling;


/**
 *  Implements a Journal lookup service backed by a collection of
 *  journals. The journals are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of journals can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapJournalLookupSession
    extends net.okapia.osid.jamocha.core.journaling.spi.AbstractMapJournalLookupSession
    implements org.osid.journaling.JournalLookupSession {


    /**
     *  Constructs a new {@code MutableMapJournalLookupSession}
     *  with no journals.
     */

    public MutableMapJournalLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJournalLookupSession} with a
     *  single journal.
     *  
     *  @param journal a journal
     *  @throws org.osid.NullArgumentException {@code journal}
     *          is {@code null}
     */

    public MutableMapJournalLookupSession(org.osid.journaling.Journal journal) {
        putJournal(journal);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJournalLookupSession}
     *  using an array of journals.
     *
     *  @param journals an array of journals
     *  @throws org.osid.NullArgumentException {@code journals}
     *          is {@code null}
     */

    public MutableMapJournalLookupSession(org.osid.journaling.Journal[] journals) {
        putJournals(journals);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJournalLookupSession}
     *  using a collection of journals.
     *
     *  @param journals a collection of journals
     *  @throws org.osid.NullArgumentException {@code journals}
     *          is {@code null}
     */

    public MutableMapJournalLookupSession(java.util.Collection<? extends org.osid.journaling.Journal> journals) {
        putJournals(journals);
        return;
    }

    
    /**
     *  Makes a {@code Journal} available in this session.
     *
     *  @param journal a journal
     *  @throws org.osid.NullArgumentException {@code journal{@code  is
     *          {@code null}
     */

    @Override
    public void putJournal(org.osid.journaling.Journal journal) {
        super.putJournal(journal);
        return;
    }


    /**
     *  Makes an array of journals available in this session.
     *
     *  @param journals an array of journals
     *  @throws org.osid.NullArgumentException {@code journals{@code 
     *          is {@code null}
     */

    @Override
    public void putJournals(org.osid.journaling.Journal[] journals) {
        super.putJournals(journals);
        return;
    }


    /**
     *  Makes collection of journals available in this session.
     *
     *  @param journals a collection of journals
     *  @throws org.osid.NullArgumentException {@code journals{@code  is
     *          {@code null}
     */

    @Override
    public void putJournals(java.util.Collection<? extends org.osid.journaling.Journal> journals) {
        super.putJournals(journals);
        return;
    }


    /**
     *  Removes a Journal from this session.
     *
     *  @param journalId the {@code Id} of the journal
     *  @throws org.osid.NullArgumentException {@code journalId{@code 
     *          is {@code null}
     */

    @Override
    public void removeJournal(org.osid.id.Id journalId) {
        super.removeJournal(journalId);
        return;
    }    
}
