//
// AbstractEffortBatchFormList.java
//
//     Implements an AbstractEffortBatchFormList.
//
//
// Tom Coppeto
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.batch.effortbatchform.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AbstractEffortBatchFormList template.
 */

public abstract class AbstractEffortBatchFormList
    extends net.okapia.osid.jamocha.spi.AbstractOsidList
    implements org.osid.resourcing.batch.EffortBatchFormList {


    /**
     *  Skip the specified number of elements in the list. If the
     *  number skipped is greater than the number of elements in the
     *  list, hasNext() becomes false and available() returns zero as
     *  there are no more elements to retrieve. This method simply
     *  invokes <code>getNext()</code>.
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        while (n-- > 0) {
            if (hasNext()) {
                try {
                    getNextEffortBatchForm();
                } catch (Exception e) {}
            } else {
                return;
            }
        }

        return;
    }

        
    /**
     *  Gets the next set of <code>EffortBatchForm</code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param n the number of <code>EffortBatchForm</code> elements
     *          requested which must be less than or equal to <code>
     *          available() </code>
     *  @return an array of <code>EffortBatchForm</code> elements. <code>
     *          </code> The length of the array is less than or equal
     *          to the number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.InvalidArgumentException <code>n</code> is
     *          less than zero
     */

    @OSID @Override
    public org.osid.resourcing.batch.EffortBatchForm[] getNextEffortBatchForms(long n)
        throws org.osid.OperationFailedException {
        
        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        org.osid.resourcing.batch.EffortBatchForm[] ret = new org.osid.resourcing.batch.EffortBatchForm[(int) n];

        for (int i = 0; i < n; i++) {
            ret[i] = getNextEffortBatchForm();
        }

        return (ret);
    }
}
