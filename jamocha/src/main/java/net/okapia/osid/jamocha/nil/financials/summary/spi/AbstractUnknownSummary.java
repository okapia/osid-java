//
// AbstractUnknownSummary.java
//
//     Defines an unknown Summary.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.financials.summary.spi;


/**
 *  Defines an unknown <code>Summary</code>.
 */

public abstract class AbstractUnknownSummary
    extends net.okapia.osid.jamocha.financials.summary.spi.AbstractSummary
    implements org.osid.financials.Summary {

    protected static final String OBJECT = "osid.financials.Summary";


    /**
     *  Constructs a new <code>AbstractUnknownSummary</code>.
     */

    public AbstractUnknownSummary() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setAccount(new net.okapia.osid.jamocha.nil.financials.account.UnknownAccount());
        setFiscalPeriod(new net.okapia.osid.jamocha.nil.financials.fiscalperiod.UnknownFiscalPeriod());

        setCredits(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        setDebits(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        
        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownSummary</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownSummary(boolean optional) {
        this();

        setBudget(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        setForecast(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));

        return;
    }
}
