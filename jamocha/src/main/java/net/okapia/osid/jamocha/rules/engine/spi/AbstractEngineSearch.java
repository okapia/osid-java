//
// AbstractEngineSearch.java
//
//     A template for making an Engine Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.engine.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing engine searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractEngineSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.rules.EngineSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.rules.records.EngineSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.rules.EngineSearchOrder engineSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of engines. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  engineIds list of engines
     *  @throws org.osid.NullArgumentException
     *          <code>engineIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongEngines(org.osid.id.IdList engineIds) {
        while (engineIds.hasNext()) {
            try {
                this.ids.add(engineIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongEngines</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of engine Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getEngineIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  engineSearchOrder engine search order 
     *  @throws org.osid.NullArgumentException
     *          <code>engineSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>engineSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderEngineResults(org.osid.rules.EngineSearchOrder engineSearchOrder) {
	this.engineSearchOrder = engineSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.rules.EngineSearchOrder getEngineSearchOrder() {
	return (this.engineSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given engine search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an engine implementing the requested record.
     *
     *  @param engineSearchRecordType an engine search record
     *         type
     *  @return the engine search record
     *  @throws org.osid.NullArgumentException
     *          <code>engineSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(engineSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.records.EngineSearchRecord getEngineSearchRecord(org.osid.type.Type engineSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.rules.records.EngineSearchRecord record : this.records) {
            if (record.implementsRecordType(engineSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(engineSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this engine search. 
     *
     *  @param engineSearchRecord engine search record
     *  @param engineSearchRecordType engine search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEngineSearchRecord(org.osid.rules.records.EngineSearchRecord engineSearchRecord, 
                                           org.osid.type.Type engineSearchRecordType) {

        addRecordType(engineSearchRecordType);
        this.records.add(engineSearchRecord);        
        return;
    }
}
