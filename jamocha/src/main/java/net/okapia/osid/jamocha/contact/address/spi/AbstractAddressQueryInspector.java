//
// AbstractAddressQueryInspector.java
//
//     A template for making an AddressQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.address.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for addresses.
 */

public abstract class AbstractAddressQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.contact.AddressQueryInspector {

    private final java.util.Collection<org.osid.contact.records.AddressQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the address text terms. 
     *
     *  @return the string terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getAddressTextTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the contact <code> Id </code> terms. 
     *
     *  @return the contact <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContactIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the contact terms. 
     *
     *  @return the contact terms 
     */

    @OSID @Override
    public org.osid.contact.ContactQueryInspector[] getContactTerms() {
        return (new org.osid.contact.ContactQueryInspector[0]);
    }


    /**
     *  Gets the address book <code> Id </code> terms. 
     *
     *  @return the address book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressBookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the address book terms. 
     *
     *  @return the address book terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given address query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an address implementing the requested record.
     *
     *  @param addressRecordType an address record type
     *  @return the address query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressQueryInspectorRecord getAddressQueryInspectorRecord(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(addressRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressRecordType + " is not supported");
    }


    /**
     *  Adds a record to this address query. 
     *
     *  @param addressQueryInspectorRecord address query inspector
     *         record
     *  @param addressRecordType address record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAddressQueryInspectorRecord(org.osid.contact.records.AddressQueryInspectorRecord addressQueryInspectorRecord, 
                                                   org.osid.type.Type addressRecordType) {

        addRecordType(addressRecordType);
        nullarg(addressRecordType, "address record type");
        this.records.add(addressQueryInspectorRecord);        
        return;
    }
}
