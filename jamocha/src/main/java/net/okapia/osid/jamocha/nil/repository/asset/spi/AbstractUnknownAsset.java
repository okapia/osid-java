//
// AbstractUnknownAsset.java
//
//     Defines an unknown Asset.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.repository.asset.spi;


/**
 *  Defines an unknown <code>Asset</code>.
 */

public abstract class AbstractUnknownAsset
    extends net.okapia.osid.jamocha.repository.asset.spi.AbstractAsset
    implements org.osid.repository.Asset {

    protected static final String OBJECT = "osid.repository.Asset";


    /**
     *  Constructs a new <code>AbstractUnknownAsset</code>.
     */

    public AbstractUnknownAsset() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setProvider(net.okapia.osid.provider.Providers.UNKNOWN.getProvider().getResource());
        setTitle(net.okapia.osid.jamocha.nil.privateutil.Text.valueOf("title"));

        setSource(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setCreatedDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setPublishedDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setPrincipalCreditString(net.okapia.osid.jamocha.nil.privateutil.Text.valueOf(""));

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownAsset</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownAsset(boolean optional) {
        this();

        setCopyrightStatusKnown(true);
        setCopyright(net.okapia.osid.jamocha.nil.privateutil.Text.valueOf("copyright"));
        setCopyrightRegistration("000000000");
        addProviderLink(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        addAssetContent(new net.okapia.osid.jamocha.nil.repository.assetcontent.UnknownAssetContent());
        setComposition(new net.okapia.osid.jamocha.nil.repository.composition.UnknownComposition());

        return;
    }
}
