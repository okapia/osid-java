//
// AbstractImmutableInputEnabler.java
//
//     Wraps a mutable InputEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.rules.inputenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>InputEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized InputEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying inputEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableInputEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.control.rules.InputEnabler {

    private final org.osid.control.rules.InputEnabler inputEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableInputEnabler</code>.
     *
     *  @param inputEnabler the input enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>inputEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableInputEnabler(org.osid.control.rules.InputEnabler inputEnabler) {
        super(inputEnabler);
        this.inputEnabler = inputEnabler;
        return;
    }


    /**
     *  Gets the input enabler record corresponding to the given <code> 
     *  InputEnabler </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  deviceEnablerRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(deviceEnablerRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  inputEnablerRecordType the type of input enabler record to 
     *          retrieve 
     *  @return the input enabler record 
     *  @throws org.osid.NullArgumentException <code> inputEnablerRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(inputEnablerRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.records.InputEnablerRecord getInputEnablerRecord(org.osid.type.Type inputEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.inputEnabler.getInputEnablerRecord(inputEnablerRecordType));
    }
}

