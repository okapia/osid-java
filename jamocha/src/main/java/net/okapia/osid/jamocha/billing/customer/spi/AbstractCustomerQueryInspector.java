//
// AbstractCustomerQueryInspector.java
//
//     A template for making a CustomerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.customer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for customers.
 */

public abstract class AbstractCustomerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.billing.CustomerQueryInspector {

    private final java.util.Collection<org.osid.billing.records.CustomerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the customer number terms. 
     *
     *  @return the customer number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCustomerNumberTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given customer query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a customer implementing the requested record.
     *
     *  @param customerRecordType a customer record type
     *  @return the customer query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(customerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CustomerQueryInspectorRecord getCustomerQueryInspectorRecord(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CustomerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(customerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(customerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this customer query. 
     *
     *  @param customerQueryInspectorRecord customer query inspector
     *         record
     *  @param customerRecordType customer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCustomerQueryInspectorRecord(org.osid.billing.records.CustomerQueryInspectorRecord customerQueryInspectorRecord, 
                                                   org.osid.type.Type customerRecordType) {

        addRecordType(customerRecordType);
        nullarg(customerRecordType, "customer record type");
        this.records.add(customerQueryInspectorRecord);        
        return;
    }
}
