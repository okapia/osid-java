//
// AbstractNodeUtilityHierarchySession.java
//
//     Defines an Utility hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.metering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an utility hierarchy session for delivering a hierarchy
 *  of utilities using the UtilityNode interface.
 */

public abstract class AbstractNodeUtilityHierarchySession
    extends net.okapia.osid.jamocha.metering.spi.AbstractUtilityHierarchySession
    implements org.osid.metering.UtilityHierarchySession {

    private java.util.Collection<org.osid.metering.UtilityNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root utility <code> Ids </code> in this hierarchy.
     *
     *  @return the root utility <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootUtilityIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.metering.utilitynode.UtilityNodeToIdList(this.roots));
    }


    /**
     *  Gets the root utilities in the utility hierarchy. A node
     *  with no parents is an orphan. While all utility <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root utilities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getRootUtilities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.metering.utilitynode.UtilityNodeToUtilityList(new net.okapia.osid.jamocha.metering.utilitynode.ArrayUtilityNodeList(this.roots)));
    }


    /**
     *  Adds a root utility node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootUtility(org.osid.metering.UtilityNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root utility nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootUtilities(java.util.Collection<org.osid.metering.UtilityNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root utility node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootUtility(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.metering.UtilityNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Utility </code> has any parents. 
     *
     *  @param  utilityId an utility <code> Id </code> 
     *  @return <code> true </code> if the utility has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentUtilities(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getUtilityNode(utilityId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  utility.
     *
     *  @param  id an <code> Id </code> 
     *  @param  utilityId the <code> Id </code> of an utility 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> utilityId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> utilityId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfUtility(org.osid.id.Id id, org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.metering.UtilityNodeList parents = getUtilityNode(utilityId).getParentUtilityNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextUtilityNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given utility. 
     *
     *  @param  utilityId an utility <code> Id </code> 
     *  @return the parent <code> Ids </code> of the utility 
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentUtilityIds(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.metering.utility.UtilityToIdList(getParentUtilities(utilityId)));
    }


    /**
     *  Gets the parents of the given utility. 
     *
     *  @param  utilityId the <code> Id </code> to query 
     *  @return the parents of the utility 
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getParentUtilities(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.metering.utilitynode.UtilityNodeToUtilityList(getUtilityNode(utilityId).getParentUtilityNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  utility.
     *
     *  @param  id an <code> Id </code> 
     *  @param  utilityId the Id of an utility 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> utilityId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfUtility(org.osid.id.Id id, org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfUtility(id, utilityId)) {
            return (true);
        }

        try (org.osid.metering.UtilityList parents = getParentUtilities(utilityId)) {
            while (parents.hasNext()) {
                if (isAncestorOfUtility(id, parents.getNextUtility().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an utility has any children. 
     *
     *  @param  utilityId an utility <code> Id </code> 
     *  @return <code> true </code> if the <code> utilityId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildUtilities(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getUtilityNode(utilityId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  utility.
     *
     *  @param  id an <code> Id </code> 
     *  @param utilityId the <code> Id </code> of an 
     *         utility
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> utilityId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> utilityId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfUtility(org.osid.id.Id id, org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfUtility(utilityId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  utility.
     *
     *  @param  utilityId the <code> Id </code> to query 
     *  @return the children of the utility 
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildUtilityIds(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.metering.utility.UtilityToIdList(getChildUtilities(utilityId)));
    }


    /**
     *  Gets the children of the given utility. 
     *
     *  @param  utilityId the <code> Id </code> to query 
     *  @return the children of the utility 
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getChildUtilities(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.metering.utilitynode.UtilityNodeToUtilityList(getUtilityNode(utilityId).getChildUtilityNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  utility.
     *
     *  @param  id an <code> Id </code> 
     *  @param utilityId the <code> Id </code> of an 
     *         utility
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> utilityId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfUtility(org.osid.id.Id id, org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfUtility(utilityId, id)) {
            return (true);
        }

        try (org.osid.metering.UtilityList children = getChildUtilities(utilityId)) {
            while (children.hasNext()) {
                if (isDescendantOfUtility(id, children.getNextUtility().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  utility.
     *
     *  @param  utilityId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified utility node 
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getUtilityNodeIds(org.osid.id.Id utilityId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.metering.utilitynode.UtilityNodeToNode(getUtilityNode(utilityId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given utility.
     *
     *  @param  utilityId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified utility node 
     *  @throws org.osid.NotFoundException <code> utilityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> utilityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityNode getUtilityNodes(org.osid.id.Id utilityId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getUtilityNode(utilityId));
    }


    /**
     *  Closes this <code>UtilityHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an utility node.
     *
     *  @param utilityId the id of the utility node
     *  @throws org.osid.NotFoundException <code>utilityId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>utilityId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.metering.UtilityNode getUtilityNode(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(utilityId, "utility Id");
        for (org.osid.metering.UtilityNode utility : this.roots) {
            if (utility.getId().equals(utilityId)) {
                return (utility);
            }

            org.osid.metering.UtilityNode r = findUtility(utility, utilityId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(utilityId + " is not found");
    }


    protected org.osid.metering.UtilityNode findUtility(org.osid.metering.UtilityNode node, 
                                                        org.osid.id.Id utilityId) 
	throws org.osid.OperationFailedException {

        try (org.osid.metering.UtilityNodeList children = node.getChildUtilityNodes()) {
            while (children.hasNext()) {
                org.osid.metering.UtilityNode utility = children.getNextUtilityNode();
                if (utility.getId().equals(utilityId)) {
                    return (utility);
                }
                
                utility = findUtility(utility, utilityId);
                if (utility != null) {
                    return (utility);
                }
            }
        }

        return (null);
    }
}
