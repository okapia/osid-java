//
// AbstractAssemblyRaceConstrainerEnablerQuery.java
//
//     A RaceConstrainerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.rules.raceconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RaceConstrainerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyRaceConstrainerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.voting.rules.RaceConstrainerEnablerQuery,
               org.osid.voting.rules.RaceConstrainerEnablerQueryInspector,
               org.osid.voting.rules.RaceConstrainerEnablerSearchOrder {

    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRaceConstrainerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRaceConstrainerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the race constrainer. 
     *
     *  @param  raceConstrainerId the race constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRaceConstrainerId(org.osid.id.Id raceConstrainerId, 
                                            boolean match) {
        getAssembler().addIdTerm(getRuledRaceConstrainerIdColumn(), raceConstrainerId, match);
        return;
    }


    /**
     *  Clears the race constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRaceConstrainerIdTerms() {
        getAssembler().clearTerms(getRuledRaceConstrainerIdColumn());
        return;
    }


    /**
     *  Gets the race constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRaceConstrainerIdTerms() {
        return (getAssembler().getIdTerms(getRuledRaceConstrainerIdColumn()));
    }


    /**
     *  Gets the RuledRaceConstrainerId column name.
     *
     * @return the column name
     */

    protected String getRuledRaceConstrainerIdColumn() {
        return ("ruled_race_constrainer_id");
    }


    /**
     *  Tests if a <code> RaceConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a race constrainer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRaceConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the race constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRaceConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQuery getRuledRaceConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRaceConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any race constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any race 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no race constrainers 
     */

    @OSID @Override
    public void matchAnyRuledRaceConstrainer(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledRaceConstrainerColumn(), match);
        return;
    }


    /**
     *  Clears the race constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledRaceConstrainerTerms() {
        getAssembler().clearTerms(getRuledRaceConstrainerColumn());
        return;
    }


    /**
     *  Gets the race constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQueryInspector[] getRuledRaceConstrainerTerms() {
        return (new org.osid.voting.rules.RaceConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the RuledRaceConstrainer column name.
     *
     * @return the column name
     */

    protected String getRuledRaceConstrainerColumn() {
        return ("ruled_race_constrainer");
    }


    /**
     *  Matches enablers mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        getAssembler().addIdTerm(getPollsIdColumn(), pollsId, match);
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        getAssembler().clearTerms(getPollsIdColumn());
        return;
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (getAssembler().getIdTerms(getPollsIdColumn()));
    }


    /**
     *  Gets the PollsId column name.
     *
     * @return the column name
     */

    protected String getPollsIdColumn() {
        return ("polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        getAssembler().clearTerms(getPollsColumn());
        return;
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the Polls column name.
     *
     * @return the column name
     */

    protected String getPollsColumn() {
        return ("polls");
    }


    /**
     *  Tests if this raceConstrainerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  raceConstrainerEnablerRecordType a race constrainer enabler record type 
     *  @return <code>true</code> if the raceConstrainerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type raceConstrainerEnablerRecordType) {
        for (org.osid.voting.rules.records.RaceConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceConstrainerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  raceConstrainerEnablerRecordType the race constrainer enabler record type 
     *  @return the race constrainer enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerEnablerQueryRecord getRaceConstrainerEnablerQueryRecord(org.osid.type.Type raceConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  raceConstrainerEnablerRecordType the race constrainer enabler record type 
     *  @return the race constrainer enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerEnablerQueryInspectorRecord getRaceConstrainerEnablerQueryInspectorRecord(org.osid.type.Type raceConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceConstrainerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(raceConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param raceConstrainerEnablerRecordType the race constrainer enabler record type
     *  @return the race constrainer enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerEnablerSearchOrderRecord getRaceConstrainerEnablerSearchOrderRecord(org.osid.type.Type raceConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceConstrainerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(raceConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this race constrainer enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param raceConstrainerEnablerQueryRecord the race constrainer enabler query record
     *  @param raceConstrainerEnablerQueryInspectorRecord the race constrainer enabler query inspector
     *         record
     *  @param raceConstrainerEnablerSearchOrderRecord the race constrainer enabler search order record
     *  @param raceConstrainerEnablerRecordType race constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerQueryRecord</code>,
     *          <code>raceConstrainerEnablerQueryInspectorRecord</code>,
     *          <code>raceConstrainerEnablerSearchOrderRecord</code> or
     *          <code>raceConstrainerEnablerRecordTyperaceConstrainerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addRaceConstrainerEnablerRecords(org.osid.voting.rules.records.RaceConstrainerEnablerQueryRecord raceConstrainerEnablerQueryRecord, 
                                      org.osid.voting.rules.records.RaceConstrainerEnablerQueryInspectorRecord raceConstrainerEnablerQueryInspectorRecord, 
                                      org.osid.voting.rules.records.RaceConstrainerEnablerSearchOrderRecord raceConstrainerEnablerSearchOrderRecord, 
                                      org.osid.type.Type raceConstrainerEnablerRecordType) {

        addRecordType(raceConstrainerEnablerRecordType);

        nullarg(raceConstrainerEnablerQueryRecord, "race constrainer enabler query record");
        nullarg(raceConstrainerEnablerQueryInspectorRecord, "race constrainer enabler query inspector record");
        nullarg(raceConstrainerEnablerSearchOrderRecord, "race constrainer enabler search odrer record");

        this.queryRecords.add(raceConstrainerEnablerQueryRecord);
        this.queryInspectorRecords.add(raceConstrainerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(raceConstrainerEnablerSearchOrderRecord);
        
        return;
    }
}
