//
// AbstractMapPersonLookupSession
//
//    A simple framework for providing a Person lookup service
//    backed by a fixed collection of persons.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Person lookup service backed by a
 *  fixed collection of persons. The persons are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Persons</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPersonLookupSession
    extends net.okapia.osid.jamocha.personnel.spi.AbstractPersonLookupSession
    implements org.osid.personnel.PersonLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.personnel.Person> persons = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.personnel.Person>());


    /**
     *  Makes a <code>Person</code> available in this session.
     *
     *  @param  person a person
     *  @throws org.osid.NullArgumentException <code>person<code>
     *          is <code>null</code>
     */

    protected void putPerson(org.osid.personnel.Person person) {
        this.persons.put(person.getId(), person);
        return;
    }


    /**
     *  Makes an array of persons available in this session.
     *
     *  @param  persons an array of persons
     *  @throws org.osid.NullArgumentException <code>persons<code>
     *          is <code>null</code>
     */

    protected void putPersons(org.osid.personnel.Person[] persons) {
        putPersons(java.util.Arrays.asList(persons));
        return;
    }


    /**
     *  Makes a collection of persons available in this session.
     *
     *  @param  persons a collection of persons
     *  @throws org.osid.NullArgumentException <code>persons<code>
     *          is <code>null</code>
     */

    protected void putPersons(java.util.Collection<? extends org.osid.personnel.Person> persons) {
        for (org.osid.personnel.Person person : persons) {
            this.persons.put(person.getId(), person);
        }

        return;
    }


    /**
     *  Removes a Person from this session.
     *
     *  @param  personId the <code>Id</code> of the person
     *  @throws org.osid.NullArgumentException <code>personId<code> is
     *          <code>null</code>
     */

    protected void removePerson(org.osid.id.Id personId) {
        this.persons.remove(personId);
        return;
    }


    /**
     *  Gets the <code>Person</code> specified by its <code>Id</code>.
     *
     *  @param  personId <code>Id</code> of the <code>Person</code>
     *  @return the person
     *  @throws org.osid.NotFoundException <code>personId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>personId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Person getPerson(org.osid.id.Id personId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.personnel.Person person = this.persons.get(personId);
        if (person == null) {
            throw new org.osid.NotFoundException("person not found: " + personId);
        }

        return (person);
    }


    /**
     *  Gets all <code>Persons</code>. In plenary mode, the returned
     *  list contains all known persons or an error
     *  results. Otherwise, the returned list may contain only those
     *  persons that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Persons</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersons()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.person.ArrayPersonList(this.persons.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.persons.clear();
        super.close();
        return;
    }
}
