//
// AbstractAssemblyAuctionHouseQuery.java
//
//     An AuctionHouseQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.bidding.auctionhouse.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuctionHouseQuery that stores terms.
 */

public abstract class AbstractAssemblyAuctionHouseQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.bidding.AuctionHouseQuery,
               org.osid.bidding.AuctionHouseQueryInspector,
               org.osid.bidding.AuctionHouseSearchOrder {

    private final java.util.Collection<org.osid.bidding.records.AuctionHouseQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.records.AuctionHouseQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.records.AuctionHouseSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAuctionHouseQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAuctionHouseQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the auction <code> Id </code> for this query. 
     *
     *  @param  auctionId the auction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionId(org.osid.id.Id auctionId, boolean match) {
        getAssembler().addIdTerm(getAuctionIdColumn(), auctionId, match);
        return;
    }


    /**
     *  Clears the auction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionIdTerms() {
        getAssembler().clearTerms(getAuctionIdColumn());
        return;
    }


    /**
     *  Gets the auction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionIdTerms() {
        return (getAssembler().getIdTerms(getAuctionIdColumn()));
    }


    /**
     *  Gets the AuctionId column name.
     *
     * @return the column name
     */

    protected String getAuctionIdColumn() {
        return ("auction_id");
    }


    /**
     *  Tests if an <code> AuctionQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the auction query 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuery getAuctionQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionQuery() is false");
    }


    /**
     *  Matches auction houses with any auction. 
     *
     *  @param  match <code> true </code> to match auction houses with any 
     *          auction, <code> false </code> to match auction houses with no 
     *          auction 
     */

    @OSID @Override
    public void matchAnyAuction(boolean match) {
        getAssembler().addIdWildcardTerm(getAuctionColumn(), match);
        return;
    }


    /**
     *  Clears the auction query terms. 
     */

    @OSID @Override
    public void clearAuctionTerms() {
        getAssembler().clearTerms(getAuctionColumn());
        return;
    }


    /**
     *  Gets the auction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQueryInspector[] getAuctionTerms() {
        return (new org.osid.bidding.AuctionQueryInspector[0]);
    }


    /**
     *  Gets the Auction column name.
     *
     * @return the column name
     */

    protected String getAuctionColumn() {
        return ("auction");
    }


    /**
     *  Sets the bid <code> Id </code> for this query. 
     *
     *  @param  bidId the bid <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bidId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBidId(org.osid.id.Id bidId, boolean match) {
        getAssembler().addIdTerm(getBidIdColumn(), bidId, match);
        return;
    }


    /**
     *  Clears the bid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBidIdTerms() {
        getAssembler().clearTerms(getBidIdColumn());
        return;
    }


    /**
     *  Gets the bid <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBidIdTerms() {
        return (getAssembler().getIdTerms(getBidIdColumn()));
    }


    /**
     *  Gets the BidId column name.
     *
     * @return the column name
     */

    protected String getBidIdColumn() {
        return ("bid_id");
    }


    /**
     *  Tests if a <code> BidQuery </code> is available. 
     *
     *  @return <code> true </code> if a bid query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bid. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bid query 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuery getBidQuery() {
        throw new org.osid.UnimplementedException("supportsBidQuery() is false");
    }


    /**
     *  Matches auction houses that have any bid. 
     *
     *  @param  match <code> true </code> to match auction houses with any 
     *          bid, <code> false </code> to match auction houses with no bid 
     */

    @OSID @Override
    public void matchAnyBid(boolean match) {
        getAssembler().addIdWildcardTerm(getBidColumn(), match);
        return;
    }


    /**
     *  Clears the bid query terms. 
     */

    @OSID @Override
    public void clearBidTerms() {
        getAssembler().clearTerms(getBidColumn());
        return;
    }


    /**
     *  Gets the bid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.BidQueryInspector[] getBidTerms() {
        return (new org.osid.bidding.BidQueryInspector[0]);
    }


    /**
     *  Gets the Bid column name.
     *
     * @return the column name
     */

    protected String getBidColumn() {
        return ("bid");
    }


    /**
     *  Sets the auction house <code> Id </code> for this query to match 
     *  auction houses that have the specified auction house as an ancestor. 
     *
     *  @param  auctionHouseId an auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                            boolean match) {
        getAssembler().addIdTerm(getAncestorAuctionHouseIdColumn(), auctionHouseId, match);
        return;
    }


    /**
     *  Clears the ancestor auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorAuctionHouseIdTerms() {
        getAssembler().clearTerms(getAncestorAuctionHouseIdColumn());
        return;
    }


    /**
     *  Gets the ancestor auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAuctionHouseIdTerms() {
        return (getAssembler().getIdTerms(getAncestorAuctionHouseIdColumn()));
    }


    /**
     *  Gets the AncestorAuctionHouseId column name.
     *
     * @return the column name
     */

    protected String getAncestorAuctionHouseIdColumn() {
        return ("ancestor_auction_house_id");
    }


    /**
     *  Tests if an <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for an <code> AuctionHouse. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAuctionHouseQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAncestorAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAuctionHouseQuery() is false");
    }


    /**
     *  Matches auction houses with any ancestor. 
     *
     *  @param  match <code> true </code> to match auction houses with any 
     *          ancestor, <code> false </code> to match root auction houses 
     */

    @OSID @Override
    public void matchAnyAncestorAuctionHouse(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorAuctionHouseColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor auction house query terms. 
     */

    @OSID @Override
    public void clearAncestorAuctionHouseTerms() {
        getAssembler().clearTerms(getAncestorAuctionHouseColumn());
        return;
    }


    /**
     *  Gets the ancestor auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAncestorAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }


    /**
     *  Gets the AncestorAuctionHouse column name.
     *
     * @return the column name
     */

    protected String getAncestorAuctionHouseColumn() {
        return ("ancestor_auction_house");
    }


    /**
     *  Sets the auction house <code> Id </code> for this query to match 
     *  auction houses that have the specified auction house as a descendant. 
     *
     *  @param  auctionHouseId an auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                              boolean match) {
        getAssembler().addIdTerm(getDescendantAuctionHouseIdColumn(), auctionHouseId, match);
        return;
    }


    /**
     *  Clears the descendant auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantAuctionHouseIdTerms() {
        getAssembler().clearTerms(getDescendantAuctionHouseIdColumn());
        return;
    }


    /**
     *  Gets the descendant auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAuctionHouseIdTerms() {
        return (getAssembler().getIdTerms(getDescendantAuctionHouseIdColumn()));
    }


    /**
     *  Gets the DescendantAuctionHouseId column name.
     *
     * @return the column name
     */

    protected String getDescendantAuctionHouseIdColumn() {
        return ("descendant_auction_house_id");
    }


    /**
     *  Tests if an <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAuctionHouseQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getDescendantAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAuctionHouseQuery() is false");
    }


    /**
     *  Matches auction houses with any descendant. 
     *
     *  @param  match <code> true </code> to match auction houses with any 
     *          descendant, <code> false </code> to match leaf auction houses 
     */

    @OSID @Override
    public void matchAnyDescendantAuctionHouse(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantAuctionHouseColumn(), match);
        return;
    }


    /**
     *  Clears the descendant auction house query terms. 
     */

    @OSID @Override
    public void clearDescendantAuctionHouseTerms() {
        getAssembler().clearTerms(getDescendantAuctionHouseColumn());
        return;
    }


    /**
     *  Gets the descendant auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getDescendantAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }


    /**
     *  Gets the DescendantAuctionHouse column name.
     *
     * @return the column name
     */

    protected String getDescendantAuctionHouseColumn() {
        return ("descendant_auction_house");
    }


    /**
     *  Tests if this auctionHouse supports the given record
     *  <code>Type</code>.
     *
     *  @param  auctionHouseRecordType an auction house record type 
     *  @return <code>true</code> if the auctionHouseRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type auctionHouseRecordType) {
        for (org.osid.bidding.records.AuctionHouseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionHouseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  auctionHouseRecordType the auction house record type 
     *  @return the auction house query record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionHouseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionHouseQueryRecord getAuctionHouseQueryRecord(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionHouseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionHouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionHouseRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  auctionHouseRecordType the auction house record type 
     *  @return the auction house query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionHouseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionHouseQueryInspectorRecord getAuctionHouseQueryInspectorRecord(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionHouseQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(auctionHouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionHouseRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param auctionHouseRecordType the auction house record type
     *  @return the auction house search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionHouseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionHouseSearchOrderRecord getAuctionHouseSearchOrderRecord(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionHouseSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(auctionHouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionHouseRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this auction house. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param auctionHouseQueryRecord the auction house query record
     *  @param auctionHouseQueryInspectorRecord the auction house query inspector
     *         record
     *  @param auctionHouseSearchOrderRecord the auction house search order record
     *  @param auctionHouseRecordType auction house record type
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseQueryRecord</code>,
     *          <code>auctionHouseQueryInspectorRecord</code>,
     *          <code>auctionHouseSearchOrderRecord</code> or
     *          <code>auctionHouseRecordTypeauctionHouse</code> is
     *          <code>null</code>
     */
            
    protected void addAuctionHouseRecords(org.osid.bidding.records.AuctionHouseQueryRecord auctionHouseQueryRecord, 
                                      org.osid.bidding.records.AuctionHouseQueryInspectorRecord auctionHouseQueryInspectorRecord, 
                                      org.osid.bidding.records.AuctionHouseSearchOrderRecord auctionHouseSearchOrderRecord, 
                                      org.osid.type.Type auctionHouseRecordType) {

        addRecordType(auctionHouseRecordType);

        nullarg(auctionHouseQueryRecord, "auction house query record");
        nullarg(auctionHouseQueryInspectorRecord, "auction house query inspector record");
        nullarg(auctionHouseSearchOrderRecord, "auction house search odrer record");

        this.queryRecords.add(auctionHouseQueryRecord);
        this.queryInspectorRecords.add(auctionHouseQueryInspectorRecord);
        this.searchOrderRecords.add(auctionHouseSearchOrderRecord);
        
        return;
    }
}
