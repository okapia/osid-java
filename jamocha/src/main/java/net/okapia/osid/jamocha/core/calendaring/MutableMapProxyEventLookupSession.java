//
// MutableMapProxyEventLookupSession
//
//    Implements an Event lookup service backed by a collection of
//    events that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements an Event lookup service backed by a collection of
 *  events. The events are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of events can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyEventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapEventLookupSession
    implements org.osid.calendaring.EventLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyEventLookupSession}
     *  with no events.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyEventLookupSession} with a
     *  single event.
     *
     *  @param calendar the calendar
     *  @param event an event
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code event}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.Event event, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putEvent(event);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEventLookupSession} using an
     *  array of events.
     *
     *  @param calendar the calendar
     *  @param events an array of events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code events}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.Event[] events, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putEvents(events);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEventLookupSession} using a
     *  collection of events.
     *
     *  @param calendar the calendar
     *  @param events a collection of events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code events}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                java.util.Collection<? extends org.osid.calendaring.Event> events,
                                                org.osid.proxy.Proxy proxy) {
   
        this(calendar, proxy);
        setSessionProxy(proxy);
        putEvents(events);
        return;
    }

    
    /**
     *  Makes a {@code Event} available in this session.
     *
     *  @param event an event
     *  @throws org.osid.NullArgumentException {@code event{@code 
     *          is {@code null}
     */

    @Override
    public void putEvent(org.osid.calendaring.Event event) {
        super.putEvent(event);
        return;
    }


    /**
     *  Makes an array of events available in this session.
     *
     *  @param events an array of events
     *  @throws org.osid.NullArgumentException {@code events{@code 
     *          is {@code null}
     */

    @Override
    public void putEvents(org.osid.calendaring.Event[] events) {
        super.putEvents(events);
        return;
    }


    /**
     *  Makes collection of events available in this session.
     *
     *  @param events
     *  @throws org.osid.NullArgumentException {@code event{@code 
     *          is {@code null}
     */

    @Override
    public void putEvents(java.util.Collection<? extends org.osid.calendaring.Event> events) {
        super.putEvents(events);
        return;
    }


    /**
     *  Removes a Event from this session.
     *
     *  @param eventId the {@code Id} of the event
     *  @throws org.osid.NullArgumentException {@code eventId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEvent(org.osid.id.Id eventId) {
        super.removeEvent(eventId);
        return;
    }    
}
