//
// AbstractImmutableCommunique.java
//
//     Wraps a mutable Communique to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.communication.communique.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Communique</code> to hide modifiers. This
 *  wrapper provides an immutized Communique from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying communique whose state changes are visible.
 */

public abstract class AbstractImmutableCommunique
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.communication.Communique {

    private final org.osid.communication.Communique communique;


    /**
     *  Constructs a new <code>AbstractImmutableCommunique</code>.
     *
     *  @param communique the communique to immutablize
     *  @throws org.osid.NullArgumentException <code>communique</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCommunique(org.osid.communication.Communique communique) {
        super(communique);
        this.communique = communique;
        return;
    }


    /**
     *  Gets the message. 
     *
     *  @return the message 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getMessage() {
        return (this.communique.getMessage());
    }


    /**
     *  Gets the message level for this communique. 
     *
     *  @return the message level 
     */

    @OSID @Override
    public org.osid.communication.CommuniqueLevel getLevel() {
        return (this.communique.getLevel());
    }


    /**
     *  Tests if the provider is blocking for a response. A response
     *  may take the form of s simple acknowledgement, a selection
     *  among a list of options, or a form input.
     *
     *  @return <code> true </code> if the provider is blocking for a
     *          response <code> false </code> if no response is
     *          required
     */

    @OSID @Override
    public boolean isResponseRequired() {
        return (this.communique.isResponseRequired());
    }


    /**
     *  Tests if the provider is blocking on a choice selection. If
     *  <code> respondViaForm() </code> is <code> true, </code> then
     *  this method must return <code> false. </code> If <code>
     *  isResponseRequired() </code> is <code> false </code> and
     *  <code> respondViaOption() </code> is <code> true, </code> then
     *  a response is optional.
     *
     *  @return <code> true </code> if the provider accepts a
     *          selection input, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean respondViaOption() {
        return (this.communique.respondViaForm());
    }


    /**
     *  Gets a list of possible response choices to this communique. 
     *
     *  @return a list of possible responses 
     *  @throws org.osid.IllegalStateException <code> respondViaOption() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.communication.ResponseOption[] getResponseOptions() {
        return (this.communique.getResponseOptions());
    }


    /**
     *  Tests if the provider is blocking on a form input. If <code>
     *  respondViaOption() </code> is <code> true, </code> then this
     *  method must return <code> false </code> . If <code>
     *  isResponseRequired() </code> is false and <code>
     *  respondViaForm() </code> is <code> true </code> , then a
     *  response is optional.
     *
     *  @return <code> true </code> if the provider accepts form
     *          input, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean respondViaForm() {
        return (this.communique.respondViaForm());
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  communiqueRecordType the communique record type 
     *  @return the communique record 
     *  @throws org.osid.NullArgumentException
     *          <code>communiqueRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(communiqueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.communication.records.CommuniqueRecord getCommuniqueRecord(org.osid.type.Type communiqueRecordType)
        throws org.osid.OperationFailedException {

        return (this.communique.getCommuniqueRecord(communiqueRecordType));
    }
}

