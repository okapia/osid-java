//
// InvariantMapProxyCompositionLookupSession
//
//    Implements a Composition lookup service backed by a fixed
//    collection of compositions. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements a Composition lookup service backed by a fixed
 *  collection of compositions. The compositions are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCompositionLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractMapCompositionLookupSession
    implements org.osid.repository.CompositionLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCompositionLookupSession} with no
     *  compositions.
     *
     *  @param repository the repository
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCompositionLookupSession(org.osid.repository.Repository repository,
                                                  org.osid.proxy.Proxy proxy) {
        setRepository(repository);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCompositionLookupSession} with a single
     *  composition.
     *
     *  @param repository the repository
     *  @param composition a single composition
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code composition} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCompositionLookupSession(org.osid.repository.Repository repository,
                                                  org.osid.repository.Composition composition, org.osid.proxy.Proxy proxy) {

        this(repository, proxy);
        putComposition(composition);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCompositionLookupSession} using
     *  an array of compositions.
     *
     *  @param repository the repository
     *  @param compositions an array of compositions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code compositions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCompositionLookupSession(org.osid.repository.Repository repository,
                                                  org.osid.repository.Composition[] compositions, org.osid.proxy.Proxy proxy) {

        this(repository, proxy);
        putCompositions(compositions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCompositionLookupSession} using a
     *  collection of compositions.
     *
     *  @param repository the repository
     *  @param compositions a collection of compositions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code compositions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCompositionLookupSession(org.osid.repository.Repository repository,
                                                  java.util.Collection<? extends org.osid.repository.Composition> compositions,
                                                  org.osid.proxy.Proxy proxy) {

        this(repository, proxy);
        putCompositions(compositions);
        return;
    }
}
