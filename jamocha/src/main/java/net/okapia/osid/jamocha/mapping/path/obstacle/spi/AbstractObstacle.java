//
// AbstractObstacle.java
//
//     Defines an Obstacle.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.obstacle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Obstacle</code>.
 */

public abstract class AbstractObstacle
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObject
    implements org.osid.mapping.path.Obstacle {

    private org.osid.mapping.path.Path path;
    private org.osid.mapping.Coordinate startingCoordinate;
    private org.osid.mapping.Coordinate endingCoordinate;

    private final java.util.Collection<org.osid.mapping.path.records.ObstacleRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the path <code> Id. </code> 
     *
     *  @return the <code> Id </code> of the path 
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        return (this.path.getId());
    }


    /**
     *  Gets the path. 
     *
     *  @return the path 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        return (this.path);
    }


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException
     *          <code>path</code> is <code>null</code>
     */

    protected void setPath(org.osid.mapping.path.Path path) {
        nullarg(path, "path");
        this.path = path;
        return;
    }


    /**
     *  Gets the starting coordinate of the obstacle on the path. 
     *
     *  @return the start of the zone 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getStartingCoordinate() {
        return (this.startingCoordinate);
    }


    /**
     *  Sets the starting coordinate.
     *
     *  @param coordinate a starting coordinate
     *  @throws org.osid.NullArgumentException
     *          <code>coordinate</code> is <code>null</code>
     */

    protected void setStartingCoordinate(org.osid.mapping.Coordinate coordinate) {
        nullarg(coordinate, "starting coordinate");
        this.startingCoordinate = coordinate;
        return;
    }


    /**
     *  Gets the ending coordinate of the obstacle on the path. 
     *
     *  @return the end of the zone 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getEndingCoordinate() {
        return (this.endingCoordinate);
    }


    /**
     *  Sets the ending coordinate.
     *
     *  @param coordinate an ending coordinate
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    protected void setEndingCoordinate(org.osid.mapping.Coordinate coordinate) {
        nullarg(coordinate, "ending coordinate");
        this.endingCoordinate = coordinate;
        return;
    }


    /**
     *  Tests if this obstacle supports the given record
     *  <code>Type</code>.
     *
     *  @param  obstacleRecordType an obstacle record type 
     *  @return <code>true</code> if the obstacleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type obstacleRecordType) {
        for (org.osid.mapping.path.records.ObstacleRecord record : this.records) {
            if (record.implementsRecordType(obstacleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Obstacle</code> record <code>Type</code>.
     *
     *  @param obstacleRecordType the obstacle record type
     *  @return the obstacle record 
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(obstacleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.ObstacleRecord getObstacleRecord(org.osid.type.Type obstacleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.ObstacleRecord record : this.records) {
            if (record.implementsRecordType(obstacleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(obstacleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this obstacle. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param obstacleRecord the obstacle record
     *  @param obstacleRecordType obstacle record type
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleRecord</code> or
     *          <code>obstacleRecordTypeobstacle</code> is
     *          <code>null</code>
     */
            
    protected void addObstacleRecord(org.osid.mapping.path.records.ObstacleRecord obstacleRecord, 
                                     org.osid.type.Type obstacleRecordType) {

        nullarg(obstacleRecord, "obstacle record");
        addRecordType(obstacleRecordType);
        this.records.add(obstacleRecord);
        
        return;
    }
}
