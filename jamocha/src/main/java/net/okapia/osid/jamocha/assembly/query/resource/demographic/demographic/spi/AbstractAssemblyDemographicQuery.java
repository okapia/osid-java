//
// AbstractAssemblyDemographicQuery.java
//
//     A DemographicQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resource.demographic.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DemographicQuery that stores terms.
 */

public abstract class AbstractAssemblyDemographicQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.resource.demographic.DemographicQuery,
               org.osid.resource.demographic.DemographicQueryInspector,
               org.osid.resource.demographic.DemographicSearchOrder {

    private final java.util.Collection<org.osid.resource.demographic.records.DemographicQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.demographic.records.DemographicQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.demographic.records.DemographicSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDemographicQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDemographicQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches demographics including the given demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIncludedDemographicId(org.osid.id.Id demographicId, 
                                           boolean match) {
        getAssembler().addIdTerm(getIncludedDemographicIdColumn(), demographicId, match);
        return;
    }


    /**
     *  Clears the demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIncludedDemographicIdTerms() {
        getAssembler().clearTerms(getIncludedDemographicIdColumn());
        return;
    }


    /**
     *  Gets the included demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIncludedDemographicIdTerms() {
        return (getAssembler().getIdTerms(getIncludedDemographicIdColumn()));
    }


    /**
     *  Gets the IncludedDemographicId column name.
     *
     * @return the column name
     */

    protected String getIncludedDemographicIdColumn() {
        return ("included_demographic_id");
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIncludedDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for a demographic. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIncludedDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getIncludedDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsIncludedDemographicQuery() is false");
    }


    /**
     *  Matches demographics including any demographic. 
     *
     *  @param  match <code> true </code> for demographics that include any 
     *          demographic, <code> false </code> to match demographics 
     *          including no demographics 
     */

    @OSID @Override
    public void matchAnyIncludedDemographic(boolean match) {
        getAssembler().addIdWildcardTerm(getIncludedDemographicColumn(), match);
        return;
    }


    /**
     *  Clears the demographic query terms. 
     */

    @OSID @Override
    public void clearIncludedDemographicTerms() {
        getAssembler().clearTerms(getIncludedDemographicColumn());
        return;
    }


    /**
     *  Gets the included demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getIncludedDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the IncludedDemographic column name.
     *
     * @return the column name
     */

    protected String getIncludedDemographicColumn() {
        return ("included_demographic");
    }


    /**
     *  Matches demographics including the given intersecting demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIncludedIntersectingDemographicId(org.osid.id.Id demographicId, 
                                                       boolean match) {
        getAssembler().addIdTerm(getIncludedIntersectingDemographicIdColumn(), demographicId, match);
        return;
    }


    /**
     *  Clears the intersecting demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIncludedIntersectingDemographicIdTerms() {
        getAssembler().clearTerms(getIncludedIntersectingDemographicIdColumn());
        return;
    }


    /**
     *  Gets the intersecting demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIncludedIntersectingDemographicIdTerms() {
        return (getAssembler().getIdTerms(getIncludedIntersectingDemographicIdColumn()));
    }


    /**
     *  Gets the IncludedIntersectingDemographicId column name.
     *
     * @return the column name
     */

    protected String getIncludedIntersectingDemographicIdColumn() {
        return ("included_intersecting_demographic_id");
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIncludedIntersectingDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for an intersecting demographic. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIncludedDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getIncludedIntersectingDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsIncludedIntersectingDemographicQuery() is false");
    }


    /**
     *  Matches demographics including any intersecting demographic. 
     *
     *  @param  match <code> true </code> for demographics that include any 
     *          intersecting demographic, <code> false </code> to match 
     *          demographics including no intersecting demographics 
     */

    @OSID @Override
    public void matchAnyIncludedIntersectingDemographic(boolean match) {
        getAssembler().addIdWildcardTerm(getIncludedIntersectingDemographicColumn(), match);
        return;
    }


    /**
     *  Clears the intersecting demographic query terms. 
     */

    @OSID @Override
    public void clearIncludedIntersectingDemographicTerms() {
        getAssembler().clearTerms(getIncludedIntersectingDemographicColumn());
        return;
    }


    /**
     *  Gets the intersecting demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getIncludedIntersectingDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the IncludedIntersectingDemographic column name.
     *
     * @return the column name
     */

    protected String getIncludedIntersectingDemographicColumn() {
        return ("included_intersecting_demographic");
    }


    /**
     *  Matches demographics including the given exclusive demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIncludedExclusiveDemographicId(org.osid.id.Id demographicId, 
                                                    boolean match) {
        getAssembler().addIdTerm(getIncludedExclusiveDemographicIdColumn(), demographicId, match);
        return;
    }


    /**
     *  Clears the exclusive demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIncludedExclusiveDemographicIdTerms() {
        getAssembler().clearTerms(getIncludedExclusiveDemographicIdColumn());
        return;
    }


    /**
     *  Gets the exclsuive demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIncludedExclusiveDemographicIdTerms() {
        return (getAssembler().getIdTerms(getIncludedExclusiveDemographicIdColumn()));
    }


    /**
     *  Gets the IncludedExclusiveDemographicId column name.
     *
     * @return the column name
     */

    protected String getIncludedExclusiveDemographicIdColumn() {
        return ("included_exclusive_demographic_id");
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIncludedExclusiveDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for an exclusive demographic. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIncludedDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getIncludedExclusiveDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsIncludedExclusiveDemographicQuery() is false");
    }


    /**
     *  Matches demographics including any exclusive demographic. 
     *
     *  @param  match <code> true </code> for demographics that include any 
     *          exclusive demographic, <code> false </code> to match 
     *          demographics including no exclusive demographics 
     */

    @OSID @Override
    public void matchAnyIncludedExclusiveDemographic(boolean match) {
        getAssembler().addIdWildcardTerm(getIncludedExclusiveDemographicColumn(), match);
        return;
    }


    /**
     *  Clears the demographic query terms. 
     */

    @OSID @Override
    public void clearIncludedExclusiveDemographicTerms() {
        getAssembler().clearTerms(getIncludedExclusiveDemographicColumn());
        return;
    }


    /**
     *  Gets the exclsuive demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getIncludedExclusiveDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the IncludedExclusiveDemographic column name.
     *
     * @return the column name
     */

    protected String getIncludedExclusiveDemographicColumn() {
        return ("included_exclusive_demographic");
    }


    /**
     *  Matches demographics excluding the given a demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchExcludedDemographicId(org.osid.id.Id demographicId, 
                                           boolean match) {
        getAssembler().addIdTerm(getExcludedDemographicIdColumn(), demographicId, match);
        return;
    }


    /**
     *  Clears the excluded demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearExcludedDemographicIdTerms() {
        getAssembler().clearTerms(getExcludedDemographicIdColumn());
        return;
    }


    /**
     *  Gets the excluded demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getExcludedDemographicIdTerms() {
        return (getAssembler().getIdTerms(getExcludedDemographicIdColumn()));
    }


    /**
     *  Gets the ExcludedDemographicId column name.
     *
     * @return the column name
     */

    protected String getExcludedDemographicIdColumn() {
        return ("excluded_demographic_id");
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available to match 
     *  demographics with an excluded demographic. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExcludedDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for an excluded demographic. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsExcludedDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getExcludedDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsExcludedDemographicQuery() is false");
    }


    /**
     *  Matches demographics excluding any demographic. 
     *
     *  @param  match <code> true </code> for demographics with any excluded 
     *          demographic, <code> false </code> to match demographics with 
     *          no excluded demographics 
     */

    @OSID @Override
    public void matchAnyExcludedDemographic(boolean match) {
        getAssembler().addIdWildcardTerm(getExcludedDemographicColumn(), match);
        return;
    }


    /**
     *  Clears the excluded demographic query terms. 
     */

    @OSID @Override
    public void clearExcludedDemographicTerms() {
        getAssembler().clearTerms(getExcludedDemographicColumn());
        return;
    }


    /**
     *  Gets the excluded demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getExcludedDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the ExcludedDemographic column name.
     *
     * @return the column name
     */

    protected String getExcludedDemographicColumn() {
        return ("excluded_demographic");
    }


    /**
     *  Matches demographics including the given resource. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIncludedResourceId(org.osid.id.Id resourceId, 
                                        boolean match) {
        getAssembler().addIdTerm(getIncludedResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the included resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIncludedResourceIdTerms() {
        getAssembler().clearTerms(getIncludedResourceIdColumn());
        return;
    }


    /**
     *  Gets the included resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIncludedResourceIdTerms() {
        return (getAssembler().getIdTerms(getIncludedResourceIdColumn()));
    }


    /**
     *  Gets the IncludedResourceId column name.
     *
     * @return the column name
     */

    protected String getIncludedResourceIdColumn() {
        return ("included_resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIncludedResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIncludedResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getIncludedResourceQuery() {
        throw new org.osid.UnimplementedException("supportsIncludedResourceQuery() is false");
    }


    /**
     *  Matches demographics including any resource. 
     *
     *  @param  match <code> true </code> for demographics that include any 
     *          resource, <code> false </code> to match demographics including 
     *          no resources 
     */

    @OSID @Override
    public void matchAnyIncludedResource(boolean match) {
        getAssembler().addIdWildcardTerm(getIncludedResourceColumn(), match);
        return;
    }


    /**
     *  Clears the included resource query terms. 
     */

    @OSID @Override
    public void clearIncludedResourceTerms() {
        getAssembler().clearTerms(getIncludedResourceColumn());
        return;
    }


    /**
     *  Gets the included resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getIncludedResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the IncludedResource column name.
     *
     * @return the column name
     */

    protected String getIncludedResourceColumn() {
        return ("included_resource");
    }


    /**
     *  Matches demographics excluding the given resource. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchExcludedResourceId(org.osid.id.Id resourceId, 
                                        boolean match) {
        getAssembler().addIdTerm(getExcludedResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the excluded resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearExcludedResourceIdTerms() {
        getAssembler().clearTerms(getExcludedResourceIdColumn());
        return;
    }


    /**
     *  Gets the excluded resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getExcludedResourceIdTerms() {
        return (getAssembler().getIdTerms(getExcludedResourceIdColumn()));
    }


    /**
     *  Gets the ExcludedResourceId column name.
     *
     * @return the column name
     */

    protected String getExcludedResourceIdColumn() {
        return ("excluded_resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available to match 
     *  demographics with an excluded resource. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExcludedResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for an excluded resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsExcludedResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getExcludedResourceQuery() {
        throw new org.osid.UnimplementedException("supportsExcludedResourceQuery() is false");
    }


    /**
     *  Matches demographics excluding any resource. 
     *
     *  @param  match <code> true </code> for demographics that exclude any 
     *          resource, <code> false </code> to match demographics excluding 
     *          no resources 
     */

    @OSID @Override
    public void matchAnyExcludedResource(boolean match) {
        getAssembler().addIdWildcardTerm(getExcludedResourceColumn(), match);
        return;
    }


    /**
     *  Clears the excluded resource query terms. 
     */

    @OSID @Override
    public void clearExcludedResourceTerms() {
        getAssembler().clearTerms(getExcludedResourceColumn());
        return;
    }


    /**
     *  Gets the excluded resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getExcludedResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the ExcludedResource column name.
     *
     * @return the column name
     */

    protected String getExcludedResourceColumn() {
        return ("excluded_resource");
    }


    /**
     *  Matches demographics resulting in the given resource. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResultingResourceId(org.osid.id.Id resourceId, 
                                         boolean match) {
        getAssembler().addIdTerm(getResultingResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resulting resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResultingResourceIdTerms() {
        getAssembler().clearTerms(getResultingResourceIdColumn());
        return;
    }


    /**
     *  Gets the resulting resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResultingResourceIdTerms() {
        return (getAssembler().getIdTerms(getResultingResourceIdColumn()));
    }


    /**
     *  Gets the ResultingResourceId column name.
     *
     * @return the column name
     */

    protected String getResultingResourceIdColumn() {
        return ("resulting_resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available to match 
     *  demographics resulting in a resource. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultingResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resulting resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultingResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResultingResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResultingResourceQuery() is false");
    }


    /**
     *  Matches demographics that have any resulting resource. 
     *
     *  @param  match <code> true </code> for demographics that have any 
     *          resulting resource, <code> false </code> to match demographics 
     *          with no resulting resources 
     */

    @OSID @Override
    public void matchAnyResultingResource(boolean match) {
        getAssembler().addIdWildcardTerm(getResultingResourceColumn(), match);
        return;
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResultingResourceTerms() {
        getAssembler().clearTerms(getResultingResourceColumn());
        return;
    }


    /**
     *  Gets the resulting resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getResultingResourceTerms() {
        return (getAssembler().getBooleanTerms(getResultingResourceColumn()));
    }


    /**
     *  Gets the ResultingResource column name.
     *
     * @return the column name
     */

    protected String getResultingResourceColumn() {
        return ("resulting_resource");
    }


    /**
     *  Matches mapped to the bin. 
     *
     *  @param  binId the bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBinId(org.osid.id.Id binId, boolean match) {
        getAssembler().addIdTerm(getBinIdColumn(), binId, match);
        return;
    }


    /**
     *  Clears the bin <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBinIdTerms() {
        getAssembler().clearTerms(getBinIdColumn());
        return;
    }


    /**
     *  Gets the bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBinIdTerms() {
        return (getAssembler().getIdTerms(getBinIdColumn()));
    }


    /**
     *  Gets the BinId column name.
     *
     * @return the column name
     */

    protected String getBinIdColumn() {
        return ("bin_id");
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        throw new org.osid.UnimplementedException("supportsBinQuery() is false");
    }


    /**
     *  Clears the bin query terms. 
     */

    @OSID @Override
    public void clearBinTerms() {
        getAssembler().clearTerms(getBinColumn());
        return;
    }


    /**
     *  Gets the bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }


    /**
     *  Gets the Bin column name.
     *
     * @return the column name
     */

    protected String getBinColumn() {
        return ("bin");
    }


    /**
     *  Tests if this demographic supports the given record
     *  <code>Type</code>.
     *
     *  @param  demographicRecordType a demographic record type 
     *  @return <code>true</code> if the demographicRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type demographicRecordType) {
        for (org.osid.resource.demographic.records.DemographicQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(demographicRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  demographicRecordType the demographic record type 
     *  @return the demographic query record 
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicQueryRecord getDemographicQueryRecord(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(demographicRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  demographicRecordType the demographic record type 
     *  @return the demographic query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicQueryInspectorRecord getDemographicQueryInspectorRecord(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(demographicRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param demographicRecordType the demographic record type
     *  @return the demographic search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicSearchOrderRecord getDemographicSearchOrderRecord(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(demographicRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this demographic. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param demographicQueryRecord the demographic query record
     *  @param demographicQueryInspectorRecord the demographic query inspector
     *         record
     *  @param demographicSearchOrderRecord the demographic search order record
     *  @param demographicRecordType demographic record type
     *  @throws org.osid.NullArgumentException
     *          <code>demographicQueryRecord</code>,
     *          <code>demographicQueryInspectorRecord</code>,
     *          <code>demographicSearchOrderRecord</code> or
     *          <code>demographicRecordTypedemographic</code> is
     *          <code>null</code>
     */
            
    protected void addDemographicRecords(org.osid.resource.demographic.records.DemographicQueryRecord demographicQueryRecord, 
                                      org.osid.resource.demographic.records.DemographicQueryInspectorRecord demographicQueryInspectorRecord, 
                                      org.osid.resource.demographic.records.DemographicSearchOrderRecord demographicSearchOrderRecord, 
                                      org.osid.type.Type demographicRecordType) {

        addRecordType(demographicRecordType);

        nullarg(demographicQueryRecord, "demographic query record");
        nullarg(demographicQueryInspectorRecord, "demographic query inspector record");
        nullarg(demographicSearchOrderRecord, "demographic search odrer record");

        this.queryRecords.add(demographicQueryRecord);
        this.queryInspectorRecords.add(demographicQueryInspectorRecord);
        this.searchOrderRecords.add(demographicSearchOrderRecord);
        
        return;
    }
}
