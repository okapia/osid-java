//
// ProgramOfferingMiter.java
//
//     Defines a ProgramOffering miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.programoffering;


/**
 *  Defines a <code>ProgramOffering</code> miter for use with the builders.
 */

public interface ProgramOfferingMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.program.ProgramOffering {


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    public void setProgram(org.osid.course.program.Program program);


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public void setTerm(org.osid.course.Term term);


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public void setTitle(org.osid.locale.DisplayText title);


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setNumber(String number);


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public void addSponsor(org.osid.resource.Resource sponsor);


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors);


    /**
     *  Sets the completion requirements info.
     *
     *  @param info a completion requirements info
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    public void setCompletionRequirementsInfo(org.osid.locale.DisplayText info);


    /**
     *  Adds a completion requirement.
     *
     *  @param requirement a completion requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public void addCompletionRequirement(org.osid.course.requisite.Requisite requirement);


    /**
     *  Sets all the completion requirements.
     *
     *  @param requirements a collection of completion requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setCompletionRequirements(java.util.Collection<org.osid.course.requisite.Requisite> requirements);


    /**
     *  Adds a credential.
     *
     *  @param credential a credential
     *  @throws org.osid.NullArgumentException <code>credential</code>
     *          is <code>null</code>
     */

    public void addCredential(org.osid.course.program.Credential credential);


    /**
     *  Sets all the credentials.
     *
     *  @param credentials a collection of credentials
     *  @throws org.osid.NullArgumentException
     *          <code>credentials</code> is <code>null</code>
     */

    public void setCredentials(java.util.Collection<org.osid.course.program.Credential> credentials);


    /**
     *  Sets the requires registration flag.
     *
     *  @param requires <code> true </code> if this program offering
     *         requires advance registration, <code> false </code>
     *         otherwise
     */

    public void setRequiresRegistration(boolean requires);


    /**
     *  Sets the minimum seats.
     *
     *  @param seats a minimum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code> is
     *          negative
     */

    public void setMinimumSeats(long seats);


    /**
     *  Sets the maximum seats.
     *
     *  @param seats a maximum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code> is
     *          negative
     */

    public void setMaximumSeats(long seats);


    /**
     *  Sets the URL.
     *
     *  @param url the URL
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public void setURL(String url);


    /**
     *  Adds a ProgramOffering record.
     *
     *  @param record a programOffering record
     *  @param recordType the type of programOffering record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addProgramOfferingRecord(org.osid.course.program.records.ProgramOfferingRecord record, org.osid.type.Type recordType);
}       


