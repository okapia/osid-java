//
// InvariantMapProxySyllabusLookupSession
//
//    Implements a Syllabus lookup service backed by a fixed
//    collection of syllabi. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus;


/**
 *  Implements a Syllabus lookup service backed by a fixed
 *  collection of syllabi. The syllabi are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySyllabusLookupSession
    extends net.okapia.osid.jamocha.core.course.syllabus.spi.AbstractMapSyllabusLookupSession
    implements org.osid.course.syllabus.SyllabusLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySyllabusLookupSession} with no
     *  syllabi.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxySyllabusLookupSession} with a single
     *  syllabus.
     *
     *  @param courseCatalog the course catalog
     *  @param syllabus a single syllabus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code syllabus} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.syllabus.Syllabus syllabus, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putSyllabus(syllabus);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySyllabusLookupSession} using
     *  an array of syllabi.
     *
     *  @param courseCatalog the course catalog
     *  @param syllabi an array of syllabi
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code syllabi} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.syllabus.Syllabus[] syllabi, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putSyllabi(syllabi);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySyllabusLookupSession} using a
     *  collection of syllabi.
     *
     *  @param courseCatalog the course catalog
     *  @param syllabi a collection of syllabi
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code syllabi} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.syllabus.Syllabus> syllabi,
                                                  org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putSyllabi(syllabi);
        return;
    }
}
