//
// AbstractGradeEntryValidator.java
//
//     Validates a GradeEntry.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.grading.gradeentry.spi;


/**
 *  Validates a GradeEntry.
 */

public abstract class AbstractGradeEntryValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRelationshipValidator {


    /**
     *  Constructs a new <code>AbstractGradeEntryValidator</code>.
     */

    protected AbstractGradeEntryValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractGradeEntryValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractGradeEntryValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a GradeEntry.
     *
     *  @param gradeEntry a grade entry to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>gradeEntry</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.grading.GradeEntry gradeEntry) {
        super.validate(gradeEntry);

        testNestedObject(gradeEntry, "getGradebookColumn");
        testNestedObject(gradeEntry, "getKeyResource");

        if (gradeEntry.isDerived() && gradeEntry.overridesCalculatedEntry()) {
            throw new org.osid.BadLogicException("isDerived() and overridesCalculatedEntry() are false");
        }

        testConditionalMethod(gradeEntry, "getOverriddenCalculatedEntryId", gradeEntry.overridesCalculatedEntry(), "overridesCalculatedEntry()");
        testConditionalMethod(gradeEntry, "getOverriddenCalculatedEntry", gradeEntry.overridesCalculatedEntry(), "overridesCalculatedEntry()");
        if (gradeEntry.overridesCalculatedEntry()) {
            testNestedObject(gradeEntry, "getOverriddenCalculatedEntry");
        }

        boolean isBasedOnGrades;
        try {
            isBasedOnGrades = gradeEntry.getGradebookColumn().getGradeSystem().isBasedOnGrades();
        } catch (org.osid.OperationFailedException oe) {
            throw new org.osid.OsidRuntimeException(oe);
        }

        testConditionalMethod(gradeEntry, "getGradeId", gradeEntry.isGraded() && isBasedOnGrades, "isGraded() && isBasedOnGrades()");
        testConditionalMethod(gradeEntry, "getGrade", gradeEntry.isGraded() && isBasedOnGrades, "isGraded() && isBasedOnGrades()");
        if (gradeEntry.isGraded() && isBasedOnGrades) {
            testNestedObject(gradeEntry, "getGrade");
        }
        
        testConditionalMethod(gradeEntry, "getScore", gradeEntry.isGraded() && !isBasedOnGrades, "isGraded() && !isBasedOnGrades()");
        testConditionalMethod(gradeEntry, "getTimeGraded", gradeEntry.isGraded() && !gradeEntry.isDerived(), "isGraded() && !isDerived()");

        testConditionalMethod(gradeEntry, "getGraderId", gradeEntry.isGraded() && !gradeEntry.isDerived(), "isGraded() && !isDerived()");
        testConditionalMethod(gradeEntry, "getGrader", gradeEntry.isGraded() && !gradeEntry.isDerived(), "isGraded() && !isDerived()");
        if (gradeEntry.isGraded() && !gradeEntry.isDerived()) {
            testNestedObject(gradeEntry, "getGrader");
        }

        testConditionalMethod(gradeEntry, "getGradingAgentId", gradeEntry.isGraded() && !gradeEntry.isDerived(), "isGraded() && !isDerived()");
        testConditionalMethod(gradeEntry, "getGradingAgent", gradeEntry.isGraded() && !gradeEntry.isDerived(), "isGraded() && !isDerived()");
        if (gradeEntry.isGraded() && !gradeEntry.isDerived()) {
            testNestedObject(gradeEntry, "getGradingAgent");
        }

        return;
    }
}
