//
// AbstractProfileEntryLookupSession.java
//
//    A starter implementation framework for providing a ProfileEntry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ProfileEntry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProfileEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProfileEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.profile.ProfileEntryLookupSession {

    private boolean pedantic         = false;
    private boolean effectiveonly    = false;
    private boolean federated        = false;
    private boolean implicit         = false;
    private boolean implicitResource = false;
    private org.osid.profile.Profile profile = new net.okapia.osid.jamocha.nil.profile.profile.UnknownProfile();
    

    /**
     *  Gets the <code>Profile/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Profile Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.profile.getId());
    }


    /**
     *  Gets the <code>Profile</code> associated with this 
     *  session.
     *
     *  @return the <code>Profile</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.profile);
    }


    /**
     *  Sets the <code>Profile</code>.
     *
     *  @param  profile the profile for this session
     *  @throws org.osid.NullArgumentException <code>profile</code>
     *          is <code>null</code>
     */

    protected void setProfile(org.osid.profile.Profile profile) {
        nullarg(profile, "profile");
        this.profile = profile;
        return;
    }


    /**
     *  Tests if this user can perform <code>ProfileEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProfileEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>ProfileEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProfileEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ProfileEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProfileEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile entries in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this profile only.
     */

    @OSID @Override
    public void useIsolatedProfileView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only profile entries whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveProfileEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All profile entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveProfileEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }


    /**
     *  Sets the view for methods in this session to implicit profile
     *  entries.  An implicit view will include profile entries
     *  derived from other profile entries. This method is the
     *  opposite of <code> explicitProfileEntryView()</code>.
     */

    @OSID @Override
    public void useImplicitProfileEntryView() {
        this.implicit = true;
        return;
    }


    /**
     *  Sets the view for methods in this session to explicit profile
     *  entries.  An explicit view includes only those profile entries
     *  that were explicitly defined and not implied. This method is
     *  the opposite of <code> implicitProfileEntryView(). </code>
     */

    @OSID @Override
    public void useExplicitProfileEntryView() {
        this.implicit = false;
        return;
    }

    
    /**
     *  Tests if an implicit or explicit view is set.
     *
     *  @return <code>true</code> if explicit and implicit
     *          profile entries should be returned</code>,
     *          <code>false</code> if only explicit profile entries
     */

    protected boolean isImplicit() {
        return (this.implicit);
    }

     
    /**
     *  Include profile entries of any agent of a resource when
     *  looking up profile entries by resource.
     */

    @OSID @Override
    public void useImplicitResourceView() {
        this.implicitResource = true;
        return;
    }


    /**
     *  Only include profile entries explicitly mapped to the given
     *  resource when looking up profile entries by resource.
     */

    @OSID @Override
    public void useExplicitResourceView() {
        this.implicitResource = false;
        return;
    }


    /**
     *  Tests if an implicit or explicit resource view is set.
     *
     *  @return <code>true</code> if explicit and implicit resources
     *          should be returned</code>, <code>false</code> if only
     *          explicit resources
     */

    protected boolean isImplicitResource() {
        return (this.implicitResource);
    }


    /**
     *  Gets the <code>ProfileEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProfileEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProfileEntry</code>
     *  and retained for compatibility.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @param  profileEntryId <code>Id</code> of the
     *          <code>ProfileEntry</code>
     *  @return the profile entry
     *  @throws org.osid.NotFoundException <code>profileEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>profileEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntry getProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.profile.ProfileEntryList profileEntries = getProfileEntries()) {
            while (profileEntries.hasNext()) {
                org.osid.profile.ProfileEntry profileEntry = profileEntries.getNextProfileEntry();
                if (profileEntry.getId().equals(profileEntryId)) {
                    return (profileEntry);
                }
            }
        } 

        throw new org.osid.NotFoundException(profileEntryId + " not found");
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  profileEntries specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>ProfileEntries</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProfileEntries()</code>.
     *
     *  @param  profileEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByIds(org.osid.id.IdList profileEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.profile.ProfileEntry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = profileEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProfileEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("profile entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.profile.profileentry.LinkedProfileEntryList(ret));
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the
     *  given profile entry genus <code>Type</code> which does not
     *  include profile entries of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProfileEntries()</code>.
     *
     *  @param  profileEntryGenusType a profileEntry genus type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByGenusType(org.osid.type.Type profileEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryGenusFilterList(getProfileEntries(), profileEntryGenusType));
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the
     *  given profile entry genus <code>Type</code> and include any
     *  additional profile entries with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProfileEntries()</code>.
     *
     *  @param  profileEntryGenusType a profileEntry genus type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByParentGenusType(org.osid.type.Type profileEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProfileEntriesByGenusType(profileEntryGenusType));
    }


    /**
     *  Gets a <code>ProfileEntryList</code> containing the given
     *  profile entry record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProfileEntries()</code>.
     *
     *  @param  profileEntryRecordType a profileEntry record type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByRecordType(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryRecordFilterList(getProfileEntries(), profileEntryRecordType));
    }


    /**
     *  Gets a <code>ProfileEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *  
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProfileEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.TemporalProfileEntryFilterList(getProfileEntries(), from, to));
    }
        

    /**
     *  Gets a list of profile entries corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.profile.ProfileEntryList getProfileEntriesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryFilterList(new ResourceFilter(resourceId), getProfileEntries()));
    }


    /**
     *  Gets a list of profile entries corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.TemporalProfileEntryFilterList(getProfileEntriesForResource(resourceId), from, to));
    }


    /**
     *  Gets a list of profile entries corresponding to an agent
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.profile.ProfileEntryList getProfileEntriesForAgent(org.osid.id.Id agentId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryFilterList(new AgentFilter(agentId), getProfileEntries()));
    }


    /**
     *  Gets a list of profile entries corresponding to an agent
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentOnDate(org.osid.id.Id agentId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.TemporalProfileEntryFilterList(getProfileEntriesForAgent(agentId), from, to));
    }


    /**
     *  Gets a list of profile entries corresponding to a profile item
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>profileItemId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.profile.ProfileEntryList getProfileEntriesForProfileItem(org.osid.id.Id profileItemId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryFilterList(new ProfileItemFilter(profileItemId), getProfileEntries()));
    }


    /**
     *  Gets a list of profile entries corresponding to a profile item
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>profileItemId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForProfileItemOnDate(org.osid.id.Id profileItemId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.TemporalProfileEntryFilterList(getProfileEntriesForProfileItem(profileItemId), from, to));
    }


    /**
     *  Gets a list of profile entries corresponding to resource and
     *  profile item <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>profileItemId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceAndProfileItem(org.osid.id.Id resourceId,
                                                                                        org.osid.id.Id profileItemId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryFilterList(new ProfileItemFilter(profileItemId), getProfileEntriesForResource(resourceId)));
    }


    /**
     *  Gets a list of profile entries corresponding to resource and
     *  profile item <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>profileItemId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceAndProfileItemOnDate(org.osid.id.Id resourceId,
                                                                                              org.osid.id.Id profileItemId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.TemporalProfileEntryFilterList(getProfileEntriesForResourceAndProfileItem(resourceId, profileItemId), from, to));
    }


    /**
     *  Gets a list of profile entries corresponding to agent and
     *  profile item <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>profileItemId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentAndProfileItem(org.osid.id.Id agentId,
                                                                                        org.osid.id.Id profileItemId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryFilterList(new ProfileItemFilter(profileItemId), getProfileEntriesForAgent(agentId)));
    }


    /**
     *  Gets a list of profile entries corresponding to agent and
     *  profile item <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>profileItemId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentAndProfileItemOnDate(org.osid.id.Id agentId,
                                                                                              org.osid.id.Id profileItemId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileentry.TemporalProfileEntryFilterList(getProfileEntriesForAgentAndProfileItem(agentId, profileItemId), from, to));
    }


    /**
     *  Gets the explicit <code>ProfileEntry</code> that generated the
     *  given implicit profile entry. If the given
     *  <code>ProfileEntry</code> is explicit, then the same
     *  <code>ProfileEntry</code> is returned.
     *
     *  @param  profileEntryId a profile entry 
     *  @return the explicit <code>ProfileEntry</code> 
     *  @throws org.osid.NotFoundException <code>profileEntryId</code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.profile.ProfileEntry getExplicitProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets all <code>ProfileEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @return a list of <code>ProfileEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.profile.ProfileEntryList getProfileEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the profile entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of profile entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.profile.ProfileEntryList filterProfileEntriesOnViews(org.osid.profile.ProfileEntryList list)
        throws org.osid.OperationFailedException {

        org.osid.profile.ProfileEntryList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.profile.profileentry.EffectiveProfileEntryFilterList(ret);
        }

        return (ret);
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ProfileEntryFilterList to filter the 
         *  profile entry list based on resource.
         *
         *  @param profileEntry the profile entry
         *  @return <code>true</code> to pass the profile entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.profile.ProfileEntry profileEntry) {
            return (profileEntry.getResourceId().equals(this.resourceId));
        }
    }


    public static class AgentFilter
        implements net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryFilter {
         
        private final org.osid.id.Id agentId;
         
         
        /**
         *  Constructs a new <code>AgentFilter</code>.
         *
         *  @param agentId the agent to filter
         *  @throws org.osid.NullArgumentException
         *          <code>agentId</code> is <code>null</code>
         */
        
        public AgentFilter(org.osid.id.Id agentId) {
            nullarg(agentId, "agent Id");
            this.agentId = agentId;
            return;
        }

         
        /**
         *  Used by the ProfileEntryFilterList to filter the 
         *  profile entry list based on agent.
         *
         *  @param profileEntry the profile entry
         *  @return <code>true</code> to pass the profile entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.profile.ProfileEntry profileEntry) {
            return (profileEntry.getAgentId().equals(this.agentId));
        }
    }


    public static class ProfileItemFilter
        implements net.okapia.osid.jamocha.inline.filter.profile.profileentry.ProfileEntryFilter {
         
        private final org.osid.id.Id profileItemId;
         
         
        /**
         *  Constructs a new <code>ProfileItemFilter</code>.
         *
         *  @param profileItemId the profile item to filter
         *  @throws org.osid.NullArgumentException
         *          <code>profileItemId</code> is <code>null</code>
         */
        
        public ProfileItemFilter(org.osid.id.Id profileItemId) {
            nullarg(profileItemId, "profile item Id");
            this.profileItemId = profileItemId;
            return;
        }

         
        /**
         *  Used by the ProfileEntryFilterList to filter the 
         *  profile entry list based on profile item.
         *
         *  @param profileEntry the profile entry
         *  @return <code>true</code> to pass the profile entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.profile.ProfileEntry profileEntry) {
            return (profileEntry.getProfileItemId().equals(this.profileItemId));
        }
    }
}
