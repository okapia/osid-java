//
// AbstractCredentialEntryLookupSession.java
//
//    A starter implementation framework for providing a CredentialEntry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CredentialEntry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCredentialEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCredentialEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.chronicle.CredentialEntryLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>CredentialEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCredentialEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>CredentialEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCredentialEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CredentialEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCredentialEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include credential entries in course catalogs which
     *  are children of this course catalog in the course catalog
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only credential entries whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveCredentialEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All credential entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCredentialEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>CredentialEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CredentialEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CredentialEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  @param  credentialEntryId <code>Id</code> of the
     *          <code>CredentialEntry</code>
     *  @return the credential entry
     *  @throws org.osid.NotFoundException <code>credentialEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>credentialEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntry getCredentialEntry(org.osid.id.Id credentialEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.chronicle.CredentialEntryList credentialEntries = getCredentialEntries()) {
            while (credentialEntries.hasNext()) {
                org.osid.course.chronicle.CredentialEntry credentialEntry = credentialEntries.getNextCredentialEntry();
                if (credentialEntry.getId().equals(credentialEntryId)) {
                    return (credentialEntry);
                }
            }
        } 

        throw new org.osid.NotFoundException(credentialEntryId + " not found");
    }


    /**
     *  Gets a <code>CredentialEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  credentialEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CredentialEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, credential entries are returned that are currently effective.
     *  In any effective mode, effective credential entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCredentialEntries()</code>.
     *
     *  @param  credentialEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CredentialEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByIds(org.osid.id.IdList credentialEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.chronicle.CredentialEntry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = credentialEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCredentialEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("credential entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.chronicle.credentialentry.LinkedCredentialEntryList(ret));
    }


    /**
     *  Gets a <code>CredentialEntryList</code> corresponding to the given
     *  credential entry genus <code>Type</code> which does not include
     *  credential entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credential entries are returned that are currently effective.
     *  In any effective mode, effective credential entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCredentialEntries()</code>.
     *
     *  @param  credentialEntryGenusType a credentialEntry genus type 
     *  @return the returned <code>CredentialEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByGenusType(org.osid.type.Type credentialEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.CredentialEntryGenusFilterList(getCredentialEntries(), credentialEntryGenusType));
    }


    /**
     *  Gets a <code>CredentialEntryList</code> corresponding to the given
     *  credential entry genus <code>Type</code> and include any additional
     *  credential entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCredentialEntries()</code>.
     *
     *  @param  credentialEntryGenusType a credentialEntry genus type 
     *  @return the returned <code>CredentialEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByParentGenusType(org.osid.type.Type credentialEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCredentialEntriesByGenusType(credentialEntryGenusType));
    }


    /**
     *  Gets a <code>CredentialEntryList</code> containing the given
     *  credential entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCredentialEntries()</code>.
     *
     *  @param  credentialEntryRecordType a credentialEntry record type 
     *  @return the returned <code>CredentialEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByRecordType(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.CredentialEntryRecordFilterList(getCredentialEntries(), credentialEntryRecordType));
    }


    /**
     *  Gets a <code>CredentialEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *  
     *  In active mode, credential entries are returned that are currently
     *  active. In any status mode, active and inactive credential entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CredentialEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.TemporalCredentialEntryFilterList(getCredentialEntries(), from, to));
    }
        

    /**
     *  Gets a list of credential entries corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>CredentialEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.CredentialEntryFilterList(new StudentFilter(resourceId), getCredentialEntries()));
    }


    /**
     *  Gets a list of credential entries corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CredentialEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.TemporalCredentialEntryFilterList(getCredentialEntriesForStudent(resourceId), from, to));
    }


    /**
     *  Gets a list of credential entries corresponding to a credential
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  credentialId the <code>Id</code> of the credential
     *  @return the returned <code>CredentialEntryList</code>
     *  @throws org.osid.NullArgumentException <code>credentialId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForCredential(org.osid.id.Id credentialId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.CredentialEntryFilterList(new CredentialFilter(credentialId), getCredentialEntries()));
    }


    /**
     *  Gets a list of credential entries corresponding to a credential
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  credentialId the <code>Id</code> of the credential
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CredentialEntryList</code>
     *  @throws org.osid.NullArgumentException <code>credentialId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForCredentialOnDate(org.osid.id.Id credentialId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.TemporalCredentialEntryFilterList(getCredentialEntriesForCredential(credentialId), from, to));
    }


    /**
     *  Gets a list of credential entries corresponding to student and credential
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  credentialId the <code>Id</code> of the credential
     *  @return the returned <code>CredentialEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>credentialId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForStudentAndCredential(org.osid.id.Id resourceId,
                                                                                                     org.osid.id.Id credentialId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.CredentialEntryFilterList(new CredentialFilter(credentialId), getCredentialEntriesForStudent(resourceId)));
    }


    /**
     *  Gets a list of credential entries corresponding to student and credential
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  credentialId the <code>Id</code> of the credential
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CredentialEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>credentialId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForStudentAndCredentialOnDate(org.osid.id.Id resourceId,
                                                                                                           org.osid.id.Id credentialId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.TemporalCredentialEntryFilterList(getCredentialEntriesForStudentAndCredential(resourceId, credentialId), from, to));
    }


    /**
     *  Gets all <code>CredentialEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>CredentialEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.chronicle.CredentialEntryList getCredentialEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the credential entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of credential entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.chronicle.CredentialEntryList filterCredentialEntriesOnViews(org.osid.course.chronicle.CredentialEntryList list)
        throws org.osid.OperationFailedException {

        org.osid.course.chronicle.CredentialEntryList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.EffectiveCredentialEntryFilterList(ret);
        }

        return (ret);
    }

    public static class StudentFilter
        implements net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.CredentialEntryFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>StudentFilter</code>.
         *
         *  @param resourceId the student to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public StudentFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "student Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the CredentialEntryFilterList to filter the 
         *  credential entry list based on student.
         *
         *  @param credentialEntry the credential entry
         *  @return <code>true</code> to pass the credential entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.chronicle.CredentialEntry credentialEntry) {
            return (credentialEntry.getStudentId().equals(this.resourceId));
        }
    }


    public static class CredentialFilter
        implements net.okapia.osid.jamocha.inline.filter.course.chronicle.credentialentry.CredentialEntryFilter {
         
        private final org.osid.id.Id credentialId;
         
         
        /**
         *  Constructs a new <code>CredentialFilter</code>.
         *
         *  @param credentialId the credential to filter
         *  @throws org.osid.NullArgumentException
         *          <code>credentialId</code> is <code>null</code>
         */
        
        public CredentialFilter(org.osid.id.Id credentialId) {
            nullarg(credentialId, "credential Id");
            this.credentialId = credentialId;
            return;
        }

         
        /**
         *  Used by the CredentialEntryFilterList to filter the 
         *  credential entry list based on credential.
         *
         *  @param credentialEntry the credential entry
         *  @return <code>true</code> to pass the credential entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.chronicle.CredentialEntry credentialEntry) {
            return (credentialEntry.getCredentialId().equals(this.credentialId));
        }
    }
}
