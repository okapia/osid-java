//
// AbstractEdgeEnabler.java
//
//     Defines an EdgeEnabler.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.rules.edgeenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>EdgeEnabler</code>.
 */

public abstract class AbstractEdgeEnabler
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnabler
    implements org.osid.topology.rules.EdgeEnabler {

    private final java.util.Collection<org.osid.topology.rules.records.EdgeEnablerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this edgeEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  edgeEnablerRecordType an edge enabler record type 
     *  @return <code>true</code> if the edgeEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type edgeEnablerRecordType) {
        for (org.osid.topology.rules.records.EdgeEnablerRecord record : this.records) {
            if (record.implementsRecordType(edgeEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>EdgeEnabler</code> record <code>Type</code>.
     *
     *  @param  edgeEnablerRecordType the edge enabler record type 
     *  @return the edge enabler record 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.rules.records.EdgeEnablerRecord getEdgeEnablerRecord(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.rules.records.EdgeEnablerRecord record : this.records) {
            if (record.implementsRecordType(edgeEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this edge enabler. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param edgeEnablerRecord the edge enabler record
     *  @param edgeEnablerRecordType edge enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecord</code> or
     *          <code>edgeEnablerRecordTypeedgeEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addEdgeEnablerRecord(org.osid.topology.rules.records.EdgeEnablerRecord edgeEnablerRecord, 
                                        org.osid.type.Type edgeEnablerRecordType) {

        nullarg(edgeEnablerRecord, "edge enabler record");
        addRecordType(edgeEnablerRecordType);
        this.records.add(edgeEnablerRecord);
        
        return;
    }
}
