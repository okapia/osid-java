//
// AbstractMapAddressLookupSession
//
//    A simple framework for providing an Address lookup service
//    backed by a fixed collection of addresses.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Address lookup service backed by a
 *  fixed collection of addresses. The addresses are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Addresses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAddressLookupSession
    extends net.okapia.osid.jamocha.contact.spi.AbstractAddressLookupSession
    implements org.osid.contact.AddressLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.contact.Address> addresses = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.contact.Address>());


    /**
     *  Makes an <code>Address</code> available in this session.
     *
     *  @param  address an address
     *  @throws org.osid.NullArgumentException <code>address<code>
     *          is <code>null</code>
     */

    protected void putAddress(org.osid.contact.Address address) {
        this.addresses.put(address.getId(), address);
        return;
    }


    /**
     *  Makes an array of addresses available in this session.
     *
     *  @param  addresses an array of addresses
     *  @throws org.osid.NullArgumentException <code>addresses<code>
     *          is <code>null</code>
     */

    protected void putAddresses(org.osid.contact.Address[] addresses) {
        putAddresses(java.util.Arrays.asList(addresses));
        return;
    }


    /**
     *  Makes a collection of addresses available in this session.
     *
     *  @param  addresses a collection of addresses
     *  @throws org.osid.NullArgumentException <code>addresses<code>
     *          is <code>null</code>
     */

    protected void putAddresses(java.util.Collection<? extends org.osid.contact.Address> addresses) {
        for (org.osid.contact.Address address : addresses) {
            this.addresses.put(address.getId(), address);
        }

        return;
    }


    /**
     *  Removes an Address from this session.
     *
     *  @param  addressId the <code>Id</code> of the address
     *  @throws org.osid.NullArgumentException <code>addressId<code> is
     *          <code>null</code>
     */

    protected void removeAddress(org.osid.id.Id addressId) {
        this.addresses.remove(addressId);
        return;
    }


    /**
     *  Gets the <code>Address</code> specified by its <code>Id</code>.
     *
     *  @param  addressId <code>Id</code> of the <code>Address</code>
     *  @return the address
     *  @throws org.osid.NotFoundException <code>addressId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>addressId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress(org.osid.id.Id addressId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.contact.Address address = this.addresses.get(addressId);
        if (address == null) {
            throw new org.osid.NotFoundException("address not found: " + addressId);
        }

        return (address);
    }


    /**
     *  Gets all <code>Addresses</code>. In plenary mode, the returned
     *  list contains all known addresses or an error
     *  results. Otherwise, the returned list may contain only those
     *  addresses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Addresses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddresses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.address.ArrayAddressList(this.addresses.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.addresses.clear();
        super.close();
        return;
    }
}
