//
// ConferralMiter.java
//
//     Defines a Conferral miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recognition.conferral;


/**
 *  Defines a <code>Conferral</code> miter for use with the builders.
 */

public interface ConferralMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.recognition.Conferral {


    /**
     *  Sets the award.
     *
     *  @param award an award
     *  @throws org.osid.NullArgumentException <code>award</code> is
     *          <code>null</code>
     */

    public void setAward(org.osid.recognition.Award award);


    /**
     *  Sets the recipient.
     *
     *  @param recipient a recipient
     *  @throws org.osid.NullArgumentException <code>recipient</code>
     *          is <code>null</code>
     */

    public void setRecipient(org.osid.resource.Resource recipient);


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    public void setReferenceId(org.osid.id.Id referenceId);


    /**
     *  Sets the convocation.
     *
     *  @param convocation a convocation
     *  @throws org.osid.NullArgumentException
     *          <code>convocation</code> is <code>null</code>
     */

    public void setConvocation(org.osid.recognition.Convocation convocation);


    /**
     *  Adds a Conferral record.
     *
     *  @param record a conferral record
     *  @param recordType the type of conferral record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addConferralRecord(org.osid.recognition.records.ConferralRecord record, org.osid.type.Type recordType);
}       


