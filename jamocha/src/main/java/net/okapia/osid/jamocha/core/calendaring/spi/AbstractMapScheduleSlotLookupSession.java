//
// AbstractMapScheduleSlotLookupSession
//
//    A simple framework for providing a ScheduleSlot lookup service
//    backed by a fixed collection of schedule slots.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ScheduleSlot lookup service backed by a
 *  fixed collection of schedule slots. The schedule slots are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ScheduleSlots</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapScheduleSlotLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractScheduleSlotLookupSession
    implements org.osid.calendaring.ScheduleSlotLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.ScheduleSlot> scheduleSlots = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.ScheduleSlot>());


    /**
     *  Makes a <code>ScheduleSlot</code> available in this session.
     *
     *  @param  scheduleSlot a schedule slot
     *  @throws org.osid.NullArgumentException <code>scheduleSlot<code>
     *          is <code>null</code>
     */

    protected void putScheduleSlot(org.osid.calendaring.ScheduleSlot scheduleSlot) {
        this.scheduleSlots.put(scheduleSlot.getId(), scheduleSlot);
        return;
    }


    /**
     *  Makes an array of schedule slots available in this session.
     *
     *  @param  scheduleSlots an array of schedule slots
     *  @throws org.osid.NullArgumentException <code>scheduleSlots<code>
     *          is <code>null</code>
     */

    protected void putScheduleSlots(org.osid.calendaring.ScheduleSlot[] scheduleSlots) {
        putScheduleSlots(java.util.Arrays.asList(scheduleSlots));
        return;
    }


    /**
     *  Makes a collection of schedule slots available in this session.
     *
     *  @param  scheduleSlots a collection of schedule slots
     *  @throws org.osid.NullArgumentException <code>scheduleSlots<code>
     *          is <code>null</code>
     */

    protected void putScheduleSlots(java.util.Collection<? extends org.osid.calendaring.ScheduleSlot> scheduleSlots) {
        for (org.osid.calendaring.ScheduleSlot scheduleSlot : scheduleSlots) {
            this.scheduleSlots.put(scheduleSlot.getId(), scheduleSlot);
        }

        return;
    }


    /**
     *  Removes a ScheduleSlot from this session.
     *
     *  @param  scheduleSlotId the <code>Id</code> of the schedule slot
     *  @throws org.osid.NullArgumentException <code>scheduleSlotId<code> is
     *          <code>null</code>
     */

    protected void removeScheduleSlot(org.osid.id.Id scheduleSlotId) {
        this.scheduleSlots.remove(scheduleSlotId);
        return;
    }


    /**
     *  Gets the <code>ScheduleSlot</code> specified by its <code>Id</code>.
     *
     *  @param  scheduleSlotId <code>Id</code> of the <code>ScheduleSlot</code>
     *  @return the scheduleSlot
     *  @throws org.osid.NotFoundException <code>scheduleSlotId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>scheduleSlotId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlot getScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.ScheduleSlot scheduleSlot = this.scheduleSlots.get(scheduleSlotId);
        if (scheduleSlot == null) {
            throw new org.osid.NotFoundException("scheduleSlot not found: " + scheduleSlotId);
        }

        return (scheduleSlot);
    }


    /**
     *  Gets all <code>ScheduleSlots</code>. In plenary mode, the returned
     *  list contains all known scheduleSlots or an error
     *  results. Otherwise, the returned list may contain only those
     *  scheduleSlots that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ScheduleSlots</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.scheduleslot.ArrayScheduleSlotList(this.scheduleSlots.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.scheduleSlots.clear();
        super.close();
        return;
    }
}
