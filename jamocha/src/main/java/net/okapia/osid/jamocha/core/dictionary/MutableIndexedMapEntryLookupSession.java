//
// MutableIndexedMapEntryLookupSession
//
//    Implements an Entry lookup service backed by a collection of
//    entries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary;


/**
 *  Implements an Entry lookup service backed by a collection of
 *  entries. The entries are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some entries may be compatible
 *  with more types than are indicated through these entry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapEntryLookupSession
    extends net.okapia.osid.jamocha.core.dictionary.spi.AbstractIndexedMapEntryLookupSession
    implements org.osid.dictionary.EntryLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapEntryLookupSession} with no entries.
     *
     *  @param dictionary the dictionary
     *  @throws org.osid.NullArgumentException {@code dictionary}
     *          is {@code null}
     */

      public MutableIndexedMapEntryLookupSession(org.osid.dictionary.Dictionary dictionary) {
        setDictionary(dictionary);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEntryLookupSession} with a
     *  single entry.
     *  
     *  @param dictionary the dictionary
     *  @param  entry an single entry
     *  @throws org.osid.NullArgumentException {@code dictionary} or
     *          {@code entry} is {@code null}
     */

    public MutableIndexedMapEntryLookupSession(org.osid.dictionary.Dictionary dictionary,
                                                  org.osid.dictionary.Entry entry) {
        this(dictionary);
        putEntry(entry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEntryLookupSession} using an
     *  array of entries.
     *
     *  @param dictionary the dictionary
     *  @param  entries an array of entries
     *  @throws org.osid.NullArgumentException {@code dictionary} or
     *          {@code entries} is {@code null}
     */

    public MutableIndexedMapEntryLookupSession(org.osid.dictionary.Dictionary dictionary,
                                                  org.osid.dictionary.Entry[] entries) {
        this(dictionary);
        putEntries(entries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEntryLookupSession} using a
     *  collection of entries.
     *
     *  @param dictionary the dictionary
     *  @param  entries a collection of entries
     *  @throws org.osid.NullArgumentException {@code dictionary} or
     *          {@code entries} is {@code null}
     */

    public MutableIndexedMapEntryLookupSession(org.osid.dictionary.Dictionary dictionary,
                                                  java.util.Collection<? extends org.osid.dictionary.Entry> entries) {

        this(dictionary);
        putEntries(entries);
        return;
    }
    

    /**
     *  Makes an {@code Entry} available in this session.
     *
     *  @param  entry an entry
     *  @throws org.osid.NullArgumentException {@code entry{@code  is
     *          {@code null}
     */

    @Override
    public void putEntry(org.osid.dictionary.Entry entry) {
        super.putEntry(entry);
        return;
    }


    /**
     *  Makes an array of entries available in this session.
     *
     *  @param  entries an array of entries
     *  @throws org.osid.NullArgumentException {@code entries{@code 
     *          is {@code null}
     */

    @Override
    public void putEntries(org.osid.dictionary.Entry[] entries) {
        super.putEntries(entries);
        return;
    }


    /**
     *  Makes collection of entries available in this session.
     *
     *  @param  entries a collection of entries
     *  @throws org.osid.NullArgumentException {@code entry{@code  is
     *          {@code null}
     */

    @Override
    public void putEntries(java.util.Collection<? extends org.osid.dictionary.Entry> entries) {
        super.putEntries(entries);
        return;
    }


    /**
     *  Removes an Entry from this session.
     *
     *  @param entryId the {@code Id} of the entry
     *  @throws org.osid.NullArgumentException {@code entryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEntry(org.osid.id.Id entryId) {
        super.removeEntry(entryId);
        return;
    }    
}
