//
// AbstractMapBudgetLookupSession
//
//    A simple framework for providing a Budget lookup service
//    backed by a fixed collection of budgets.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Budget lookup service backed by a
 *  fixed collection of budgets. The budgets are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Budgets</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBudgetLookupSession
    extends net.okapia.osid.jamocha.financials.budgeting.spi.AbstractBudgetLookupSession
    implements org.osid.financials.budgeting.BudgetLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.financials.budgeting.Budget> budgets = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.financials.budgeting.Budget>());


    /**
     *  Makes a <code>Budget</code> available in this session.
     *
     *  @param  budget a budget
     *  @throws org.osid.NullArgumentException <code>budget<code>
     *          is <code>null</code>
     */

    protected void putBudget(org.osid.financials.budgeting.Budget budget) {
        this.budgets.put(budget.getId(), budget);
        return;
    }


    /**
     *  Makes an array of budgets available in this session.
     *
     *  @param  budgets an array of budgets
     *  @throws org.osid.NullArgumentException <code>budgets<code>
     *          is <code>null</code>
     */

    protected void putBudgets(org.osid.financials.budgeting.Budget[] budgets) {
        putBudgets(java.util.Arrays.asList(budgets));
        return;
    }


    /**
     *  Makes a collection of budgets available in this session.
     *
     *  @param  budgets a collection of budgets
     *  @throws org.osid.NullArgumentException <code>budgets<code>
     *          is <code>null</code>
     */

    protected void putBudgets(java.util.Collection<? extends org.osid.financials.budgeting.Budget> budgets) {
        for (org.osid.financials.budgeting.Budget budget : budgets) {
            this.budgets.put(budget.getId(), budget);
        }

        return;
    }


    /**
     *  Removes a Budget from this session.
     *
     *  @param  budgetId the <code>Id</code> of the budget
     *  @throws org.osid.NullArgumentException <code>budgetId<code> is
     *          <code>null</code>
     */

    protected void removeBudget(org.osid.id.Id budgetId) {
        this.budgets.remove(budgetId);
        return;
    }


    /**
     *  Gets the <code>Budget</code> specified by its <code>Id</code>.
     *
     *  @param  budgetId <code>Id</code> of the <code>Budget</code>
     *  @return the budget
     *  @throws org.osid.NotFoundException <code>budgetId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>budgetId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.Budget getBudget(org.osid.id.Id budgetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.financials.budgeting.Budget budget = this.budgets.get(budgetId);
        if (budget == null) {
            throw new org.osid.NotFoundException("budget not found: " + budgetId);
        }

        return (budget);
    }


    /**
     *  Gets all <code>Budgets</code>. In plenary mode, the returned
     *  list contains all known budgets or an error
     *  results. Otherwise, the returned list may contain only those
     *  budgets that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Budgets</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.budgeting.budget.ArrayBudgetList(this.budgets.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.budgets.clear();
        super.close();
        return;
    }
}
