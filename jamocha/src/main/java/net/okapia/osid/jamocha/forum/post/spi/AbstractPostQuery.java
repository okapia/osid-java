//
// AbstractPostQuery.java
//
//     A template for making a Post Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.post.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for posts.
 */

public abstract class AbstractPostQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.forum.PostQuery {

    private final java.util.Collection<org.osid.forum.records.PostQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches entries whose sent time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimestamp(org.osid.calendaring.DateTime startTime, 
                               org.osid.calendaring.DateTime endTime, 
                               boolean match) {
        return;
    }


    /**
     *  Clears the timestamp terms. 
     */

    @OSID @Override
    public void clearTimestampTerms() {
        return;
    }


    /**
     *  Matches the poster of the entry. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPosterId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the poster <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPosterIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  senders. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsPosterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getPosterQuery() {
        throw new org.osid.UnimplementedException("supportsPosterQuery() is false");
    }


    /**
     *  Clears the poster terms. 
     */

    @OSID @Override
    public void clearPosterTerms() {
        return;
    }


    /**
     *  Matches the posting agent of the entry. 
     *
     *  @param  agentId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPostingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the posting agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  posters. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getPostingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsPostingAgentQuery() is false");
    }


    /**
     *  Clears the posting agent terms. 
     */

    @OSID @Override
    public void clearPostingAgentTerms() {
        return;
    }


    /**
     *  Adds a subject line to match. Multiple subject line matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  subject display name to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> subject is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> subject </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSubjectLine(String subject, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches entries with any subject line. 
     *
     *  @param  match <code> true </code> to match entries with any subject 
     *          line, <code> false </code> to match entries with no subject 
     *          line 
     */

    @OSID @Override
    public void matchAnySubjectLine(boolean match) {
        return;
    }


    /**
     *  Clears the subject line terms. 
     */

    @OSID @Override
    public void clearSubjectLineTerms() {
        return;
    }


    /**
     *  Adds text to match. Multiple text matches can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  text text to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> text is </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> text </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchText(String text, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches entries with any text. 
     *
     *  @param  match <code> true </code> to match entries with any text, 
     *          <code> false </code> to match entries with no text 
     */

    @OSID @Override
    public void matchAnyText(boolean match) {
        return;
    }


    /**
     *  Clears the text terms. 
     */

    @OSID @Override
    public void clearTextTerms() {
        return;
    }


    /**
     *  Sets the reply <code> Id </code> for this query to match replies 
     *  assigned to posts. 
     *
     *  @param  replyId a reply <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> replyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReplyId(org.osid.id.Id replyId, boolean match) {
        return;
    }


    /**
     *  Clears the reply <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReplyIdTerms() {
        return;
    }


    /**
     *  Tests if a reply query is available. 
     *
     *  @return <code> true </code> if a reply query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a post. 
     *
     *  @return the reply query 
     *  @throws org.osid.UnimplementedException <code> supportsReplyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyQuery getReplyQuery() {
        throw new org.osid.UnimplementedException("supportsReplyQuery() is false");
    }


    /**
     *  Matches posts with any reply. 
     *
     *  @param  match <code> true </code> to match posts with any reply, 
     *          <code> false </code> to match posts with no replies 
     */

    @OSID @Override
    public void matchAnyReply(boolean match) {
        return;
    }


    /**
     *  Clears the reply terms. 
     */

    @OSID @Override
    public void clearReplyTerms() {
        return;
    }


    /**
     *  Sets the post <code> Id </code> for this query to match replies 
     *  assigned to forums. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchForumId(org.osid.id.Id forumId, boolean match) {
        return;
    }


    /**
     *  Clears the forum <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearForumIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ForumQuery </code> is available. 
     *
     *  @return <code> true </code> if a forum query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the forum query 
     *  @throws org.osid.UnimplementedException <code> supportsForumQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuery getForumQuery() {
        throw new org.osid.UnimplementedException("supportsForumQuery() is false");
    }


    /**
     *  Clears the forum terms. 
     */

    @OSID @Override
    public void clearForumTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given post query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a post implementing the requested record.
     *
     *  @param postRecordType a post record type
     *  @return the post query record
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.PostQueryRecord getPostQueryRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.PostQueryRecord record : this.records) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Adds a record to this post query. 
     *
     *  @param postQueryRecord post query record
     *  @param postRecordType post record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPostQueryRecord(org.osid.forum.records.PostQueryRecord postQueryRecord, 
                                          org.osid.type.Type postRecordType) {

        addRecordType(postRecordType);
        nullarg(postQueryRecord, "post query record");
        this.records.add(postQueryRecord);        
        return;
    }
}
