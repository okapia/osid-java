//
// MutableIndexedMapTodoProducerLookupSession
//
//    Implements a TodoProducer lookup service backed by a collection of
//    todoProducers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.mason;


/**
 *  Implements a TodoProducer lookup service backed by a collection of
 *  todo producers. The todo producers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some todo producers may be compatible
 *  with more types than are indicated through these todo producer
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of todo producers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapTodoProducerLookupSession
    extends net.okapia.osid.jamocha.core.checklist.mason.spi.AbstractIndexedMapTodoProducerLookupSession
    implements org.osid.checklist.mason.TodoProducerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapTodoProducerLookupSession} with no todo producers.
     *
     *  @param checklist the checklist
     *  @throws org.osid.NullArgumentException {@code checklist}
     *          is {@code null}
     */

      public MutableIndexedMapTodoProducerLookupSession(org.osid.checklist.Checklist checklist) {
        setChecklist(checklist);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapTodoProducerLookupSession} with a
     *  single todo producer.
     *  
     *  @param checklist the checklist
     *  @param  todoProducer a single todoProducer
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todoProducer} is {@code null}
     */

    public MutableIndexedMapTodoProducerLookupSession(org.osid.checklist.Checklist checklist,
                                                  org.osid.checklist.mason.TodoProducer todoProducer) {
        this(checklist);
        putTodoProducer(todoProducer);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapTodoProducerLookupSession} using an
     *  array of todo producers.
     *
     *  @param checklist the checklist
     *  @param  todoProducers an array of todo producers
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todoProducers} is {@code null}
     */

    public MutableIndexedMapTodoProducerLookupSession(org.osid.checklist.Checklist checklist,
                                                  org.osid.checklist.mason.TodoProducer[] todoProducers) {
        this(checklist);
        putTodoProducers(todoProducers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapTodoProducerLookupSession} using a
     *  collection of todo producers.
     *
     *  @param checklist the checklist
     *  @param  todoProducers a collection of todo producers
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todoProducers} is {@code null}
     */

    public MutableIndexedMapTodoProducerLookupSession(org.osid.checklist.Checklist checklist,
                                                  java.util.Collection<? extends org.osid.checklist.mason.TodoProducer> todoProducers) {

        this(checklist);
        putTodoProducers(todoProducers);
        return;
    }
    

    /**
     *  Makes a {@code TodoProducer} available in this session.
     *
     *  @param  todoProducer a todo producer
     *  @throws org.osid.NullArgumentException {@code todoProducer{@code  is
     *          {@code null}
     */

    @Override
    public void putTodoProducer(org.osid.checklist.mason.TodoProducer todoProducer) {
        super.putTodoProducer(todoProducer);
        return;
    }


    /**
     *  Makes an array of todo producers available in this session.
     *
     *  @param  todoProducers an array of todo producers
     *  @throws org.osid.NullArgumentException {@code todoProducers{@code 
     *          is {@code null}
     */

    @Override
    public void putTodoProducers(org.osid.checklist.mason.TodoProducer[] todoProducers) {
        super.putTodoProducers(todoProducers);
        return;
    }


    /**
     *  Makes collection of todo producers available in this session.
     *
     *  @param  todoProducers a collection of todo producers
     *  @throws org.osid.NullArgumentException {@code todoProducer{@code  is
     *          {@code null}
     */

    @Override
    public void putTodoProducers(java.util.Collection<? extends org.osid.checklist.mason.TodoProducer> todoProducers) {
        super.putTodoProducers(todoProducers);
        return;
    }


    /**
     *  Removes a TodoProducer from this session.
     *
     *  @param todoProducerId the {@code Id} of the todo producer
     *  @throws org.osid.NullArgumentException {@code todoProducerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeTodoProducer(org.osid.id.Id todoProducerId) {
        super.removeTodoProducer(todoProducerId);
        return;
    }    
}
