//
// AbstractPathQueryInspector.java
//
//     A template for making a PathQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.path.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for paths.
 */

public abstract class AbstractPathQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.topology.path.PathQueryInspector {

    private final java.util.Collection<org.osid.topology.path.records.PathQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the complete query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCompleteTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the closed query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getClosedTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the starting node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStartingNodeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the starting node query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getStartingNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Gets the ending node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEndingNodeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ending node query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getEndingNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Gets the along node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdSetTerm[] getAlongNodeIdsTerms() {
        return (new org.osid.search.terms.IdSetTerm[0]);
    }


    /**
     *  Gets the intersecting path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIntersectingPathIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the intersecting path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.path.PathQueryInspector[] getIntersectingPathTerms() {
        return (new org.osid.topology.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the hops query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getHopsTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the distance query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDistanceTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the cost query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getCostTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNodeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the node query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Gets the edge <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEdgeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the edge query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.EdgeQueryInspector[] getEdgeTerms() {
        return (new org.osid.topology.EdgeQueryInspector[0]);
    }


    /**
     *  Gets the graph <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGraphIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the graph query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given path query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a path implementing the requested record.
     *
     *  @param pathRecordType a path record type
     *  @return the path query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.path.records.PathQueryInspectorRecord getPathQueryInspectorRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.path.records.PathQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Adds a record to this path query. 
     *
     *  @param pathQueryInspectorRecord path query inspector
     *         record
     *  @param pathRecordType path record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPathQueryInspectorRecord(org.osid.topology.path.records.PathQueryInspectorRecord pathQueryInspectorRecord, 
                                                   org.osid.type.Type pathRecordType) {

        addRecordType(pathRecordType);
        nullarg(pathRecordType, "path record type");
        this.records.add(pathQueryInspectorRecord);        
        return;
    }
}
