//
// AbstractConfigurationBatchManager.java
//
//     An adapter for a ConfigurationBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ConfigurationBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterConfigurationBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.configuration.batch.ConfigurationBatchManager>
    implements org.osid.configuration.batch.ConfigurationBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterConfigurationBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterConfigurationBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterConfigurationBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterConfigurationBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of values is available. 
     *
     *  @return <code> true </code> if a value bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueBatchAdmin() {
        return (getAdapteeManager().supportsValueBatchAdmin());
    }


    /**
     *  Tests if bulk administration of parameters is available. 
     *
     *  @return <code> true </code> if a parameter bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterBatchAdmin() {
        return (getAdapteeManager().supportsParameterBatchAdmin());
    }


    /**
     *  Tests if bulk administration of configurations is available. 
     *
     *  @return <code> true </code> if a configuration bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationBatchAdmin() {
        return (getAdapteeManager().supportsConfigurationBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk value 
     *  administration service. 
     *
     *  @return a <code> ValueBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.batch.ValueBatchAdminSession getValueBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk value 
     *  administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ValueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.batch.ValueBatchAdminSession getValueBatchAdminSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueBatchAdminSessionForConfiguration(configurationId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk parameter 
     *  administration service. 
     *
     *  @return a <code> ParameterBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.batch.ParameterBatchAdminSession getParameterBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk parameter 
     *  administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.batch.ParameterBatchAdminSession getParameterBatchAdminSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterBatchAdminSessionForConfiguration(configurationId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  configuration administration service. 
     *
     *  @return a <code> ConfigurationBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.batch.ConfigurationBatchAdminSession getConfigurationBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
