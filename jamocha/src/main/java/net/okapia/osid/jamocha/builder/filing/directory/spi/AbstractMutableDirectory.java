//
// AbstractMutableDirectory.java
//
//     Defines a mutable Directory.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.directory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Directory</code>.
 */

public abstract class AbstractMutableDirectory
    extends net.okapia.osid.jamocha.filing.directory.spi.AbstractDirectory
    implements org.osid.filing.Directory,
               net.okapia.osid.jamocha.builder.filing.directory.DirectoryMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }

    
    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this directory. 
     *
     *  @param record directory record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addDirectoryRecord(org.osid.filing.records.DirectoryRecord record, org.osid.type.Type recordType) {
        super.addDirectoryRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the provider for this directory.
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(net.okapia.osid.provider.Provider provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Sets the provider of this directory
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(org.osid.resource.Resource provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Adds an asset for the provider branding.
     *
     *  @param asset an asset to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    @Override
    public void addAssetToBranding(org.osid.repository.Asset asset) {
        super.addAssetToBranding(asset);
        return;
    }


    /**
     *  Adds assets for the provider branding.
     *
     *  @param assets an array of assets to add
     *  @throws org.osid.NullArgumentException <code>assets</code>
     *          is <code>null</code>
     */

    @Override
    public void setBranding(java.util.Collection<org.osid.repository.Asset> assets) {
        super.setBranding(assets);
        return;
    }


    /**
     *  Sets the license.
     *
     *  @param license the license
     *  @throws org.osid.NullArgumentException <code>license</code>
     *          is <code>null</code>
     */

    @Override
    public void setLicense(org.osid.locale.DisplayText license) {
        super.setLicense(license);
        return;
    }


    /**
     *  Sets the display name for this directory.
     *
     *  @param displayName the name for this directory
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this directory.
     *
     *  @param description the description of this directory
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the name.
     *  
     *  @param name the file name
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          <code>null</code>
     */

    @Override
    public void setName(String name) {
        super.setName(name);
        return;
    }


    /**
     *  Sets the alias flag.
     *  
     *  @param alias <code>true</code> if an alias, <code>false</code>
     *         otherwise
     */

    @Override
    public void setAlias(boolean alias) {
        super.setAlias(alias);
        return;
    }


    /**
     *  Sets the path.
     *  
     *  @param path the file path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    @Override
    public void setPath(String path) {
        super.setPath(path);
        return;
    }


    /**
     *  Sets the real path.
     *  
     *  @param path the file real path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    @Override
    public void setRealPath(String path) {
        super.setRealPath(path);
        return;
    }


    /**
     *  Sets the owner.
     *  
     *  @param agent the file owner
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setOwner(org.osid.authentication.Agent agent) {
        super.setOwner(agent);
        return;
    }


    /**
     *  Sets the created time.
     *  
     *  @param time the file created time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreatedTime(org.osid.calendaring.DateTime time) {
        super.setCreatedTime(time);
        return;
    }


    /**
     *  Sets the modified time.
     *  
     *  @param time the file modified time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public void setLastModifiedTime(org.osid.calendaring.DateTime time) {
        super.setLastModifiedTime(time);
        return;
    }


    /**
     *  Sets the accessed time.
     *  
     *  @param time the file accessed time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public void setLastAccessTime(org.osid.calendaring.DateTime time) {
        super.setLastAccessTime(time);
        return;
    }
}

