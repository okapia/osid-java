//
// AbstractAgentQuerySession.java
//
//     A template for making AgentQuerySessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic template for query sessions.
 */

public abstract class AbstractAgentQuerySession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.authentication.AgentQuerySession {

    private boolean federated = false;
    private org.osid.authentication.Agency agency = new net.okapia.osid.jamocha.nil.authentication.agency.UnknownAgency();


      /**
       *  Gets the <code>Agency/code> <code>Id</code> associated
       *  with this session.
       *
       *  @return the <code>Agency Id</code> associated with
       *          this session
       *  @throws org.osid.IllegalStateException this session has been
       *          closed
       */

    @OSID @Override
      public org.osid.id.Id getAgencyId() {
        return (this.agency.getId());
    }


    /**
     *  Gets the <code>Agency</code> associated with this 
     *  session.
     *
     *  @return the <code>Agency</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.agency);
    }


    /**
     *  Sets the <code>Agency</code>.
     *
     *  @param  agency the agency for this session
     *  @throws org.osid.NullArgumentException <code>agency</code>
     *          is <code>null</code>
     */

    protected void setAgency(org.osid.authentication.Agency agency) {
        nullarg(agency, "agency");
        this.agency = agency;
        return;
    }

    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agents in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Tests if this user can perform <code> Agent </code>
     *  searches. A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer search operations to
     *  unauthorized users.
     *
     *  @return <code> false </code> if search methods are not authorized, 
     *          <code> true </code> otherwise 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canSearchAgents() {
        return (true);
    }
}
