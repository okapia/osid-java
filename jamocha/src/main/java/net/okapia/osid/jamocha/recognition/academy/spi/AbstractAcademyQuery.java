//
// AbstractAcademyQuery.java
//
//     A template for making an Academy Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.academy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for academies.
 */

public abstract class AbstractAcademyQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.recognition.AcademyQuery {

    private final java.util.Collection<org.osid.recognition.records.AcademyQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the conferral <code> Id </code> for this query to match 
     *  conferrals assigned to academies. 
     *
     *  @param  conferralId a conferral <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> conferralId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConferralId(org.osid.id.Id conferralId, boolean match) {
        return;
    }


    /**
     *  Clears the conferral <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConferralIdTerms() {
        return;
    }


    /**
     *  Tests if a conferral query is available. 
     *
     *  @return <code> true </code> if a conferral query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralQuery() {
        return (false);
    }


    /**
     *  Gets the query for an conferral. 
     *
     *  @return the conferral query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuery getConferralQuery() {
        throw new org.osid.UnimplementedException("supportsConferralQuery() is false");
    }


    /**
     *  Matches academies with any conferral. 
     *
     *  @param  match <code> true </code> to match academies with any 
     *          conferral, <code> false </code> to match academies with no 
     *          conferrals 
     */

    @OSID @Override
    public void matchAnyConferral(boolean match) {
        return;
    }


    /**
     *  Clears the conferral terms. 
     */

    @OSID @Override
    public void clearConferralTerms() {
        return;
    }


    /**
     *  Sets the award <code> Id </code> for this query to match conferrals 
     *  assigned to awards. 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Matches academies with any award. 
     *
     *  @param  match <code> true </code> to match academies with any award, 
     *          <code> false </code> to match academies with no awards 
     */

    @OSID @Override
    public void matchAnyAward(boolean match) {
        return;
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        return;
    }


    /**
     *  Sets a convocaton <code> Id. </code> 
     *
     *  @param  convocationId a convocaton <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> convocationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConvocationId(org.osid.id.Id convocationId, boolean match) {
        return;
    }


    /**
     *  Clears the convocaton <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConvocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ConvocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a convocaton query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a convocaton query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the convocaton query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuery getConvocationQuery() {
        throw new org.osid.UnimplementedException("supportsConvocationQuery() is false");
    }


    /**
     *  Matches any convocaton. 
     *
     *  @param  match <code> true </code> to match academies with any 
     *          convocation, <code> false </code> to match academies with no 
     *          convocations 
     */

    @OSID @Override
    public void matchAnyConvocation(boolean match) {
        return;
    }


    /**
     *  Clears the convocaton terms. 
     */

    @OSID @Override
    public void clearConvocationTerms() {
        return;
    }


    /**
     *  Sets the academy <code> Id </code> for this query to match academies 
     *  that have the specified academy as an ancestor. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAcademyId(org.osid.id.Id academyId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorAcademyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAcademyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getAncestorAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAcademyQuery() is false");
    }


    /**
     *  Matches academies with any ancestor. 
     *
     *  @param  match <code> true </code> to match academies with any 
     *          ancestor, <code> false </code> to match root academies 
     */

    @OSID @Override
    public void matchAnyAncestorAcademy(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor academy terms. 
     */

    @OSID @Override
    public void clearAncestorAcademyTerms() {
        return;
    }


    /**
     *  Sets the academy <code> Id </code> for this query to match academies 
     *  that have the specified academy as a descendant. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAcademyId(org.osid.id.Id academyId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantAcademyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAcademyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getDescendantAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAcademyQuery() is false");
    }


    /**
     *  Matches academies with any descendant. 
     *
     *  @param  match <code> true </code> to match academies with any 
     *          descendant, <code> false </code> to match leaf academies 
     */

    @OSID @Override
    public void matchAnyDescendantAcademy(boolean match) {
        return;
    }


    /**
     *  Clears the descendant academy terms. 
     */

    @OSID @Override
    public void clearDescendantAcademyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given academy query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an academy implementing the requested record.
     *
     *  @param academyRecordType an academy record type
     *  @return the academy query record
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(academyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AcademyQueryRecord getAcademyQueryRecord(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AcademyQueryRecord record : this.records) {
            if (record.implementsRecordType(academyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(academyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this academy query. 
     *
     *  @param academyQueryRecord academy query record
     *  @param academyRecordType academy record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAcademyQueryRecord(org.osid.recognition.records.AcademyQueryRecord academyQueryRecord, 
                                          org.osid.type.Type academyRecordType) {

        addRecordType(academyRecordType);
        nullarg(academyQueryRecord, "academy query record");
        this.records.add(academyQueryRecord);        
        return;
    }
}
