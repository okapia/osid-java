//
// InvariantMapConferralLookupSession
//
//    Implements a Conferral lookup service backed by a fixed collection of
//    conferrals.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition;


/**
 *  Implements a Conferral lookup service backed by a fixed
 *  collection of conferrals. The conferrals are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapConferralLookupSession
    extends net.okapia.osid.jamocha.core.recognition.spi.AbstractMapConferralLookupSession
    implements org.osid.recognition.ConferralLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapConferralLookupSession</code> with no
     *  conferrals.
     *  
     *  @param academy the academy
     *  @throws org.osid.NullArgumnetException {@code academy} is
     *          {@code null}
     */

    public InvariantMapConferralLookupSession(org.osid.recognition.Academy academy) {
        setAcademy(academy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapConferralLookupSession</code> with a single
     *  conferral.
     *  
     *  @param academy the academy
     *  @param conferral a single conferral
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code conferral} is <code>null</code>
     */

      public InvariantMapConferralLookupSession(org.osid.recognition.Academy academy,
                                               org.osid.recognition.Conferral conferral) {
        this(academy);
        putConferral(conferral);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapConferralLookupSession</code> using an array
     *  of conferrals.
     *  
     *  @param academy the academy
     *  @param conferrals an array of conferrals
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code conferrals} is <code>null</code>
     */

      public InvariantMapConferralLookupSession(org.osid.recognition.Academy academy,
                                               org.osid.recognition.Conferral[] conferrals) {
        this(academy);
        putConferrals(conferrals);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapConferralLookupSession</code> using a
     *  collection of conferrals.
     *
     *  @param academy the academy
     *  @param conferrals a collection of conferrals
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code conferrals} is <code>null</code>
     */

      public InvariantMapConferralLookupSession(org.osid.recognition.Academy academy,
                                               java.util.Collection<? extends org.osid.recognition.Conferral> conferrals) {
        this(academy);
        putConferrals(conferrals);
        return;
    }
}
