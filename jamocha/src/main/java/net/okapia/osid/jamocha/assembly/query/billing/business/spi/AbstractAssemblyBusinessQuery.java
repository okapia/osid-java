//
// AbstractAssemblyBusinessQuery.java
//
//     A BusinessQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.billing.business.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BusinessQuery that stores terms.
 */

public abstract class AbstractAssemblyBusinessQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.billing.BusinessQuery,
               org.osid.billing.BusinessQueryInspector,
               org.osid.billing.BusinessSearchOrder {

    private final java.util.Collection<org.osid.billing.records.BusinessQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.BusinessQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.BusinessSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBusinessQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBusinessQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the customer <code> Id </code> for this query to match customers 
     *  that have a related customer. 
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> customerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id customerId, boolean match) {
        getAssembler().addIdTerm(getCustomerIdColumn(), customerId, match);
        return;
    }


    /**
     *  Clears the customer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        getAssembler().clearTerms(getCustomerIdColumn());
        return;
    }


    /**
     *  Gets the customer <code> Id </code> query terms. 
     *
     *  @return the customer <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (getAssembler().getIdTerms(getCustomerIdColumn()));
    }


    /**
     *  Gets the CustomerId column name.
     *
     * @return the column name
     */

    protected String getCustomerIdColumn() {
        return ("customer_id");
    }


    /**
     *  Tests if a <code> CustomerQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the customer query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Matches businesses that have any customer. 
     *
     *  @param  match <code> true </code> to match customers with any 
     *          customer, <code> false </code> to match customers with no 
     *          customer 
     */

    @OSID @Override
    public void matchAnyCustomer(boolean match) {
        getAssembler().addIdWildcardTerm(getCustomerColumn(), match);
        return;
    }


    /**
     *  Clears the customer query terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        getAssembler().clearTerms(getCustomerColumn());
        return;
    }


    /**
     *  Gets the customer query terms. 
     *
     *  @return the customer query terms 
     */

    @OSID @Override
    public org.osid.billing.CustomerQueryInspector[] getCustomerTerms() {
        return (new org.osid.billing.CustomerQueryInspector[0]);
    }


    /**
     *  Gets the Customer column name.
     *
     * @return the column name
     */

    protected String getCustomerColumn() {
        return ("customer");
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the item <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches businesses that have any item. 
     *
     *  @param  match <code> true </code> to match businesses with any item, 
     *          <code> false </code> to match businesses with no items 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears the item query terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.billing.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.billing.ItemQueryInspector[0]);
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Sets the catalog <code> Id </code> for this query. 
     *
     *  @param  categoryId a category <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> categoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCategoryId(org.osid.id.Id categoryId, boolean match) {
        getAssembler().addIdTerm(getCategoryIdColumn(), categoryId, match);
        return;
    }


    /**
     *  Clears the category <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCategoryIdTerms() {
        getAssembler().clearTerms(getCategoryIdColumn());
        return;
    }


    /**
     *  Gets the category <code> Id </code> query terms. 
     *
     *  @return the category <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCategoryIdTerms() {
        return (getAssembler().getIdTerms(getCategoryIdColumn()));
    }


    /**
     *  Gets the CategoryId column name.
     *
     * @return the column name
     */

    protected String getCategoryIdColumn() {
        return ("category_id");
    }


    /**
     *  Tests if a <code> CategoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a category query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a category. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the category query 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryQuery getCategoryQuery() {
        throw new org.osid.UnimplementedException("supportsCategoryQuery() is false");
    }


    /**
     *  Matches businesses that have any category. 
     *
     *  @param  match <code> true </code> to match customers with any 
     *          category, <code> false </code> to match customers with no 
     *          category 
     */

    @OSID @Override
    public void matchAnyCategory(boolean match) {
        getAssembler().addIdWildcardTerm(getCategoryColumn(), match);
        return;
    }


    /**
     *  Clears the category query terms. 
     */

    @OSID @Override
    public void clearCategoryTerms() {
        getAssembler().clearTerms(getCategoryColumn());
        return;
    }


    /**
     *  Gets the category query terms. 
     *
     *  @return the category query terms 
     */

    @OSID @Override
    public org.osid.billing.CategoryQueryInspector[] getCategoryTerms() {
        return (new org.osid.billing.CategoryQueryInspector[0]);
    }


    /**
     *  Gets the Category column name.
     *
     * @return the column name
     */

    protected String getCategoryColumn() {
        return ("category");
    }


    /**
     *  Sets the entry <code> Id </code> for this query. 
     *
     *  @param  itemId an entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getEntryIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        getAssembler().clearTerms(getEntryIdColumn());
        return;
    }


    /**
     *  Gets the entry <code> Id </code> query terms. 
     *
     *  @return the entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (getAssembler().getIdTerms(getEntryIdColumn()));
    }


    /**
     *  Gets the EntryId column name.
     *
     * @return the column name
     */

    protected String getEntryIdColumn() {
        return ("entry_id");
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches businesses that have any entry. 
     *
     *  @param  match <code> true </code> to match businesses with any entry, 
     *          <code> false </code> to match businesses with no entries 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getEntryColumn(), match);
        return;
    }


    /**
     *  Clears the entry query terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        getAssembler().clearTerms(getEntryColumn());
        return;
    }


    /**
     *  Gets the entry query terms. 
     *
     *  @return the entry query terms 
     */

    @OSID @Override
    public org.osid.billing.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.billing.EntryQueryInspector[0]);
    }


    /**
     *  Gets the Entry column name.
     *
     * @return the column name
     */

    protected String getEntryColumn() {
        return ("entry");
    }


    /**
     *  Sets the period <code> Id </code> for this query to match catalogs 
     *  containing terms. 
     *
     *  @param  periodId the period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> periodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPeriodId(org.osid.id.Id periodId, boolean match) {
        getAssembler().addIdTerm(getPeriodIdColumn(), periodId, match);
        return;
    }


    /**
     *  Clears the period <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPeriodIdTerms() {
        getAssembler().clearTerms(getPeriodIdColumn());
        return;
    }


    /**
     *  Gets the period <code> Id </code> query terms. 
     *
     *  @return the period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPeriodIdTerms() {
        return (getAssembler().getIdTerms(getPeriodIdColumn()));
    }


    /**
     *  Gets the PeriodId column name.
     *
     * @return the column name
     */

    protected String getPeriodIdColumn() {
        return ("period_id");
    }


    /**
     *  Tests if a <code> PeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a period query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a period Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the period query 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuery getPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsPeriodQuery() is false");
    }


    /**
     *  Matches businesses that have any period. 
     *
     *  @param  match <code> true </code> to match businesses with any period, 
     *          <code> false </code> to match customers with no period 
     */

    @OSID @Override
    public void matchAnyPeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getPeriodColumn(), match);
        return;
    }


    /**
     *  Clears the period query terms. 
     */

    @OSID @Override
    public void clearPeriodTerms() {
        getAssembler().clearTerms(getPeriodColumn());
        return;
    }


    /**
     *  Gets the period query terms. 
     *
     *  @return the period query terms 
     */

    @OSID @Override
    public org.osid.billing.PeriodQueryInspector[] getPeriodTerms() {
        return (new org.osid.billing.PeriodQueryInspector[0]);
    }


    /**
     *  Gets the Period column name.
     *
     * @return the column name
     */

    protected String getPeriodColumn() {
        return ("period");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match businesses 
     *  that have the specified business as an ancestor. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorBusinessId(org.osid.id.Id businessId, 
                                        boolean match) {
        getAssembler().addIdTerm(getAncestorBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the ancestor business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorBusinessIdTerms() {
        getAssembler().clearTerms(getAncestorBusinessIdColumn());
        return;
    }


    /**
     *  Gets the ancestor business <code> Id </code> query terms. 
     *
     *  @return the ancestor business <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBusinessIdTerms() {
        return (getAssembler().getIdTerms(getAncestorBusinessIdColumn()));
    }


    /**
     *  Gets the AncestorBusinessId column name.
     *
     * @return the column name
     */

    protected String getAncestorBusinessIdColumn() {
        return ("ancestor_business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBusinessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getAncestorBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBusinessQuery() is false");
    }


    /**
     *  Matches businesses with any business ancestor. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          ancestor, <code> false </code> to match root businesses 
     */

    @OSID @Override
    public void matchAnyAncestorBusiness(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorBusinessColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor business query terms. 
     */

    @OSID @Override
    public void clearAncestorBusinessTerms() {
        getAssembler().clearTerms(getAncestorBusinessColumn());
        return;
    }


    /**
     *  Gets the ancestor business query terms. 
     *
     *  @return the ancestor business terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getAncestorBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the AncestorBusiness column name.
     *
     * @return the column name
     */

    protected String getAncestorBusinessColumn() {
        return ("ancestor_business");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match businesses 
     *  that have the specified business as an descendant. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantBusinessId(org.osid.id.Id businessId, 
                                          boolean match) {
        getAssembler().addIdTerm(getDescendantBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the descendant business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantBusinessIdTerms() {
        getAssembler().clearTerms(getDescendantBusinessIdColumn());
        return;
    }


    /**
     *  Gets the descendant business <code> Id </code> query terms. 
     *
     *  @return the descendant business <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBusinessIdTerms() {
        return (getAssembler().getIdTerms(getDescendantBusinessIdColumn()));
    }


    /**
     *  Gets the DescendantBusinessId column name.
     *
     * @return the column name
     */

    protected String getDescendantBusinessIdColumn() {
        return ("descendant_business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBusinessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getDescendantBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBusinessQuery() is false");
    }


    /**
     *  Matches businesses with any descendant business. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          descendant, <code> false </code> to match leaf businesses 
     */

    @OSID @Override
    public void matchAnyDescendantBusiness(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantBusinessColumn(), match);
        return;
    }


    /**
     *  Clears the descendant business query terms. 
     */

    @OSID @Override
    public void clearDescendantBusinessTerms() {
        getAssembler().clearTerms(getDescendantBusinessColumn());
        return;
    }


    /**
     *  Gets the descendant business query terms. 
     *
     *  @return the descendant business terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getDescendantBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the DescendantBusiness column name.
     *
     * @return the column name
     */

    protected String getDescendantBusinessColumn() {
        return ("descendant_business");
    }


    /**
     *  Tests if this business supports the given record
     *  <code>Type</code>.
     *
     *  @param  businessRecordType a business record type 
     *  @return <code>true</code> if the businessRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type businessRecordType) {
        for (org.osid.billing.records.BusinessQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(businessRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  businessRecordType the business record type 
     *  @return the business query record 
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.BusinessQueryRecord getBusinessQueryRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.BusinessQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  businessRecordType the business record type 
     *  @return the business query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.BusinessQueryInspectorRecord getBusinessQueryInspectorRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.BusinessQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param businessRecordType the business record type
     *  @return the business search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.BusinessSearchOrderRecord getBusinessSearchOrderRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.BusinessSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this business. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param businessQueryRecord the business query record
     *  @param businessQueryInspectorRecord the business query inspector
     *         record
     *  @param businessSearchOrderRecord the business search order record
     *  @param businessRecordType business record type
     *  @throws org.osid.NullArgumentException
     *          <code>businessQueryRecord</code>,
     *          <code>businessQueryInspectorRecord</code>,
     *          <code>businessSearchOrderRecord</code> or
     *          <code>businessRecordTypebusiness</code> is
     *          <code>null</code>
     */
            
    protected void addBusinessRecords(org.osid.billing.records.BusinessQueryRecord businessQueryRecord, 
                                      org.osid.billing.records.BusinessQueryInspectorRecord businessQueryInspectorRecord, 
                                      org.osid.billing.records.BusinessSearchOrderRecord businessSearchOrderRecord, 
                                      org.osid.type.Type businessRecordType) {

        addRecordType(businessRecordType);

        nullarg(businessQueryRecord, "business query record");
        nullarg(businessQueryInspectorRecord, "business query inspector record");
        nullarg(businessSearchOrderRecord, "business search odrer record");

        this.queryRecords.add(businessQueryRecord);
        this.queryInspectorRecords.add(businessQueryInspectorRecord);
        this.searchOrderRecords.add(businessSearchOrderRecord);
        
        return;
    }
}
