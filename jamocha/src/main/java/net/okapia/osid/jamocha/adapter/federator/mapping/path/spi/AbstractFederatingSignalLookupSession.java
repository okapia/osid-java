//
// AbstractFederatingSignalLookupSession.java
//
//     An abstract federating adapter for a SignalLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SignalLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSignalLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.mapping.path.SignalLookupSession>
    implements org.osid.mapping.path.SignalLookupSession {

    private boolean parallel = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();


    /**
     *  Constructs a new <code>AbstractFederatingSignalLookupSession</code>.
     */

    protected AbstractFederatingSignalLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.mapping.path.SignalLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>Signal</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSignals() {
        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            if (session.canLookupSignals()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Signal</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSignalView() {
        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            session.useComparativeSignalView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Signal</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySignalView() {
        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            session.usePlenarySignalView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include signals in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            session.useFederatedMapView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            session.useIsolatedMapView();
        }

        return;
    }


    /**
     *  Only active signals are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSignalView() {
        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            session.useActiveSignalView();
        }

        return;
    }


    /**
     *  Active and inactive signals are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSignalView() {
        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            session.useAnyStatusSignalView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Signal</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Signal</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Signal</code> and
     *  retained for compatibility.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param  signalId <code>Id</code> of the
     *          <code>Signal</code>
     *  @return the signal
     *  @throws org.osid.NotFoundException <code>signalId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>signalId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Signal getSignal(org.osid.id.Id signalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            try {
                return (session.getSignal(signalId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(signalId + " not found");
    }


    /**
     *  Gets a <code>SignalList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  signals specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Signals</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param  signalIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Signal</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>signalIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByIds(org.osid.id.IdList signalIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.mapping.path.signal.MutableSignalList ret = new net.okapia.osid.jamocha.mapping.path.signal.MutableSignalList();

        try (org.osid.id.IdList ids = signalIds) {
            while (ids.hasNext()) {
                ret.addSignal(getSignal(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SignalList</code> corresponding to the given
     *  signal genus <code>Type</code> which does not include
     *  signals of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  signals or an error results. Otherwise, the returned list
     *  may contain only those signals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param  signalGenusType a signal genus type 
     *  @return the returned <code>Signal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByGenusType(org.osid.type.Type signalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.signal.FederatingSignalList ret = getSignalList();

        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            ret.addSignalList(session.getSignalsByGenusType(signalGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SignalList</code> corresponding to the given
     *  signal genus <code>Type</code> and include any additional
     *  signals with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  signals or an error results. Otherwise, the returned list
     *  may contain only those signals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param  signalGenusType a signal genus type 
     *  @return the returned <code>Signal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByParentGenusType(org.osid.type.Type signalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.signal.FederatingSignalList ret = getSignalList();

        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            ret.addSignalList(session.getSignalsByParentGenusType(signalGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SignalList</code> containing the given
     *  signal record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  signals or an error results. Otherwise, the returned list
     *  may contain only those signals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param  signalRecordType a signal record type 
     *  @return the returned <code>Signal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByRecordType(org.osid.type.Type signalRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.signal.FederatingSignalList ret = getSignalList();

        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            ret.addSignalList(session.getSignalsByRecordType(signalRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SignalList</code> containing the given path.
     *
     *  In plenary mode, the returned list contains all known signals
     *  or an error results. Otherwise, the returned list may contain
     *  only those signals that are accessible through this session.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals are
     *  returned.
     *
     *  @param  pathId a path <code>Id</code>
     *  @return the returned <code>Signal</code> list
     *  @throws org.osid.NullArgumentException <code>pathId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.signal.FederatingSignalList ret = getSignalList();

        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            ret.addSignalList(session.getSignalsForPath(pathId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SignalList</code> containing the given path
     *  between the given coordinates inclusive.
     *
     *  In plenary mode, the returned list contains all known signals
     *  or an error results. Otherwise, the returned list may contain
     *  only those signals that are accessible through this session.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals are
     *  returned.
     *
     *  @param  pathId a path <code>Id</code>
     *  @param  coordinate starting coordinate
     *  @param  distance a distance from coordinate
     *  @return the returned <code>Signal</code> list
     *  @throws org.osid.NullArgumentException <code>pathId</code>,
     *          <code>coordinate</code> or <code>distance</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsForPathAtCoordinate(org.osid.id.Id pathId,
                                                                          org.osid.mapping.Coordinate coordinate,
                                                                          org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.signal.FederatingSignalList ret = getSignalList();

        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            ret.addSignalList(session.getSignalsForPathAtCoordinate(pathId, coordinate, distance));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Signals</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  signals or an error results. Otherwise, the returned list
     *  may contain only those signals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @return a list of <code>Signals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.signal.FederatingSignalList ret = getSignalList();

        for (org.osid.mapping.path.SignalLookupSession session : getSessions()) {
            ret.addSignalList(session.getSignals());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.mapping.path.signal.FederatingSignalList getSignalList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.signal.ParallelSignalList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.signal.CompositeSignalList());
        }
    }
}
