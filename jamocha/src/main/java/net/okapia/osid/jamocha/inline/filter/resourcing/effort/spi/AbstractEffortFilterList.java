//
// AbstractEffortList
//
//     Implements a filter for an EffortList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.resourcing.effort.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an EffortList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedEffortList
 *  to improve performance.
 */

public abstract class AbstractEffortFilterList
    extends net.okapia.osid.jamocha.resourcing.effort.spi.AbstractEffortList
    implements org.osid.resourcing.EffortList,
               net.okapia.osid.jamocha.inline.filter.resourcing.effort.EffortFilter {

    private org.osid.resourcing.Effort effort;
    private final org.osid.resourcing.EffortList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractEffortFilterList</code>.
     *
     *  @param effortList an <code>EffortList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>effortList</code> is <code>null</code>
     */

    protected AbstractEffortFilterList(org.osid.resourcing.EffortList effortList) {
        nullarg(effortList, "effort list");
        this.list = effortList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.effort == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Effort </code> in this list. 
     *
     *  @return the next <code> Effort </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Effort </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Effort getNextEffort()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.resourcing.Effort effort = this.effort;
            this.effort = null;
            return (effort);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in effort list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.effort = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Efforts.
     *
     *  @param effort the effort to filter
     *  @return <code>true</code> if the effort passes the filter,
     *          <code>false</code> if the effort should be filtered
     */

    public abstract boolean pass(org.osid.resourcing.Effort effort);


    protected void prime() {
        if (this.effort != null) {
            return;
        }

        org.osid.resourcing.Effort effort = null;

        while (this.list.hasNext()) {
            try {
                effort = this.list.getNextEffort();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(effort)) {
                this.effort = effort;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
