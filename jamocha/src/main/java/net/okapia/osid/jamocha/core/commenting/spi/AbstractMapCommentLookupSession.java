//
// AbstractMapCommentLookupSession
//
//    A simple framework for providing a Comment lookup service
//    backed by a fixed collection of comments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Comment lookup service backed by a
 *  fixed collection of comments. The comments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Comments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCommentLookupSession
    extends net.okapia.osid.jamocha.commenting.spi.AbstractCommentLookupSession
    implements org.osid.commenting.CommentLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.commenting.Comment> comments = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.commenting.Comment>());


    /**
     *  Makes a <code>Comment</code> available in this session.
     *
     *  @param  comment a comment
     *  @throws org.osid.NullArgumentException <code>comment<code>
     *          is <code>null</code>
     */

    protected void putComment(org.osid.commenting.Comment comment) {
        this.comments.put(comment.getId(), comment);
        return;
    }


    /**
     *  Makes an array of comments available in this session.
     *
     *  @param  comments an array of comments
     *  @throws org.osid.NullArgumentException <code>comments<code>
     *          is <code>null</code>
     */

    protected void putComments(org.osid.commenting.Comment[] comments) {
        putComments(java.util.Arrays.asList(comments));
        return;
    }


    /**
     *  Makes a collection of comments available in this session.
     *
     *  @param  comments a collection of comments
     *  @throws org.osid.NullArgumentException <code>comments<code>
     *          is <code>null</code>
     */

    protected void putComments(java.util.Collection<? extends org.osid.commenting.Comment> comments) {
        for (org.osid.commenting.Comment comment : comments) {
            this.comments.put(comment.getId(), comment);
        }

        return;
    }


    /**
     *  Removes a Comment from this session.
     *
     *  @param  commentId the <code>Id</code> of the comment
     *  @throws org.osid.NullArgumentException <code>commentId<code> is
     *          <code>null</code>
     */

    protected void removeComment(org.osid.id.Id commentId) {
        this.comments.remove(commentId);
        return;
    }


    /**
     *  Gets the <code>Comment</code> specified by its <code>Id</code>.
     *
     *  @param  commentId <code>Id</code> of the <code>Comment</code>
     *  @return the comment
     *  @throws org.osid.NotFoundException <code>commentId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>commentId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Comment getComment(org.osid.id.Id commentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.commenting.Comment comment = this.comments.get(commentId);
        if (comment == null) {
            throw new org.osid.NotFoundException("comment not found: " + commentId);
        }

        return (comment);
    }


    /**
     *  Gets all <code>Comments</code>. In plenary mode, the returned
     *  list contains all known comments or an error
     *  results. Otherwise, the returned list may contain only those
     *  comments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Comments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getComments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.commenting.comment.ArrayCommentList(this.comments.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.comments.clear();
        super.close();
        return;
    }
}
