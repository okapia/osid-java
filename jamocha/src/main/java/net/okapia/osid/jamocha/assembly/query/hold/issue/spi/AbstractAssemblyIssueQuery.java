//
// AbstractAssemblyIssueQuery.java
//
//     An IssueQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.hold.issue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An IssueQuery that stores terms.
 */

public abstract class AbstractAssemblyIssueQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.hold.IssueQuery,
               org.osid.hold.IssueQueryInspector,
               org.osid.hold.IssueSearchOrder {

    private final java.util.Collection<org.osid.hold.records.IssueQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.records.IssueQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.records.IssueSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyIssueQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyIssueQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBureauId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getBureauIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBureauIdTerms() {
        getAssembler().clearTerms(getBureauIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBureauIdTerms() {
        return (getAssembler().getIdTerms(getBureauIdColumn()));
    }


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBureau(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBureauColumn(), style);
        return;
    }


    /**
     *  Gets the BureauId column name.
     *
     * @return the column name
     */

    protected String getBureauIdColumn() {
        return ("bureau_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBureauQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getBureauQuery() {
        throw new org.osid.UnimplementedException("supportsBureauQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearBureauTerms() {
        getAssembler().clearTerms(getBureauColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getBureauTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBureauSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getBureauSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBureauSearchOrder() is false");
    }


    /**
     *  Gets the Bureau column name.
     *
     * @return the column name
     */

    protected String getBureauColumn() {
        return ("bureau");
    }


    /**
     *  Sets the block <code> Id </code> for this query. 
     *
     *  @param  blockId the block <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlockId(org.osid.id.Id blockId, boolean match) {
        getAssembler().addIdTerm(getBlockIdColumn(), blockId, match);
        return;
    }


    /**
     *  Clears the block <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBlockIdTerms() {
        getAssembler().clearTerms(getBlockIdColumn());
        return;
    }


    /**
     *  Gets the block <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBlockIdTerms() {
        return (getAssembler().getIdTerms(getBlockIdColumn()));
    }


    /**
     *  Gets the BlockId column name.
     *
     * @return the column name
     */

    protected String getBlockIdColumn() {
        return ("block_id");
    }


    /**
     *  Tests if a <code> BlockQuery </code> is available. 
     *
     *  @return <code> true </code> if a block query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a block. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the block query 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuery getBlockQuery() {
        throw new org.osid.UnimplementedException("supportsBlockQuery() is false");
    }


    /**
     *  Matches issues used for any block. 
     *
     *  @param  match <code> true </code> to match issues with any blocks, 
     *          <code> false </code> to match issues with no blocks 
     */

    @OSID @Override
    public void matchAnyBlock(boolean match) {
        getAssembler().addIdWildcardTerm(getBlockColumn(), match);
        return;
    }


    /**
     *  Clears the block query terms. 
     */

    @OSID @Override
    public void clearBlockTerms() {
        getAssembler().clearTerms(getBlockColumn());
        return;
    }


    /**
     *  Gets the block query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.BlockQueryInspector[] getBlockTerms() {
        return (new org.osid.hold.BlockQueryInspector[0]);
    }


    /**
     *  Gets the Block column name.
     *
     * @return the column name
     */

    protected String getBlockColumn() {
        return ("block");
    }


    /**
     *  Sets the hold <code> Id </code> for this query. 
     *
     *  @param  holdId the hold <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> holdId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchHoldId(org.osid.id.Id holdId, boolean match) {
        getAssembler().addIdTerm(getHoldIdColumn(), holdId, match);
        return;
    }


    /**
     *  Clears the hold <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearHoldIdTerms() {
        getAssembler().clearTerms(getHoldIdColumn());
        return;
    }


    /**
     *  Gets the hold query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getHoldIdTerms() {
        return (getAssembler().getIdTerms(getHoldIdColumn()));
    }


    /**
     *  Gets the HoldId column name.
     *
     * @return the column name
     */

    protected String getHoldIdColumn() {
        return ("hold_id");
    }


    /**
     *  Tests if a <code> HoldQuery </code> is available. 
     *
     *  @return <code> true </code> if a hold query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldQuery() {
        return (false);
    }


    /**
     *  Gets the query for a hold. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the hold query 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuery getHoldQuery() {
        throw new org.osid.UnimplementedException("supportsHoldQuery() is false");
    }


    /**
     *  Matches issues referenced by any hold. 
     *
     *  @param  match <code> true </code> to match issues with any holds, 
     *          <code> false </code> to match issues with no holds 
     */

    @OSID @Override
    public void matchAnyHold(boolean match) {
        getAssembler().addIdWildcardTerm(getHoldColumn(), match);
        return;
    }


    /**
     *  Clears the hold query terms. 
     */

    @OSID @Override
    public void clearHoldTerms() {
        getAssembler().clearTerms(getHoldColumn());
        return;
    }


    /**
     *  Gets the hold query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.HoldQueryInspector[] getHoldTerms() {
        return (new org.osid.hold.HoldQueryInspector[0]);
    }


    /**
     *  Gets the Hold column name.
     *
     * @return the column name
     */

    protected String getHoldColumn() {
        return ("hold");
    }


    /**
     *  Sets the oubliette <code> Id </code> for this query to match issues 
     *  assigned to foundries. 
     *
     *  @param  oublietteId the oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOublietteId(org.osid.id.Id oublietteId, boolean match) {
        getAssembler().addIdTerm(getOublietteIdColumn(), oublietteId, match);
        return;
    }


    /**
     *  Clears the oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOublietteIdTerms() {
        getAssembler().clearTerms(getOublietteIdColumn());
        return;
    }


    /**
     *  Gets the oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOublietteIdTerms() {
        return (getAssembler().getIdTerms(getOublietteIdColumn()));
    }


    /**
     *  Gets the OublietteId column name.
     *
     * @return the column name
     */

    protected String getOublietteIdColumn() {
        return ("oubliette_id");
    }


    /**
     *  Tests if a <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsOublietteQuery() is false");
    }


    /**
     *  Clears the oubliette query terms. 
     */

    @OSID @Override
    public void clearOublietteTerms() {
        getAssembler().clearTerms(getOublietteColumn());
        return;
    }


    /**
     *  Gets the oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }


    /**
     *  Gets the Oubliette column name.
     *
     * @return the column name
     */

    protected String getOublietteColumn() {
        return ("oubliette");
    }


    /**
     *  Tests if this issue supports the given record
     *  <code>Type</code>.
     *
     *  @param  issueRecordType an issue record type 
     *  @return <code>true</code> if the issueRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type issueRecordType) {
        for (org.osid.hold.records.IssueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(issueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  issueRecordType the issue record type 
     *  @return the issue query record 
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(issueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.IssueQueryRecord getIssueQueryRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.IssueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  issueRecordType the issue record type 
     *  @return the issue query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(issueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.IssueQueryInspectorRecord getIssueQueryInspectorRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.IssueQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param issueRecordType the issue record type
     *  @return the issue search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(issueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.IssueSearchOrderRecord getIssueSearchOrderRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.IssueSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this issue. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param issueQueryRecord the issue query record
     *  @param issueQueryInspectorRecord the issue query inspector
     *         record
     *  @param issueSearchOrderRecord the issue search order record
     *  @param issueRecordType issue record type
     *  @throws org.osid.NullArgumentException
     *          <code>issueQueryRecord</code>,
     *          <code>issueQueryInspectorRecord</code>,
     *          <code>issueSearchOrderRecord</code> or
     *          <code>issueRecordTypeissue</code> is
     *          <code>null</code>
     */
            
    protected void addIssueRecords(org.osid.hold.records.IssueQueryRecord issueQueryRecord, 
                                      org.osid.hold.records.IssueQueryInspectorRecord issueQueryInspectorRecord, 
                                      org.osid.hold.records.IssueSearchOrderRecord issueSearchOrderRecord, 
                                      org.osid.type.Type issueRecordType) {

        addRecordType(issueRecordType);

        nullarg(issueQueryRecord, "issue query record");
        nullarg(issueQueryInspectorRecord, "issue query inspector record");
        nullarg(issueSearchOrderRecord, "issue search odrer record");

        this.queryRecords.add(issueQueryRecord);
        this.queryInspectorRecords.add(issueQueryInspectorRecord);
        this.searchOrderRecords.add(issueSearchOrderRecord);
        
        return;
    }
}
