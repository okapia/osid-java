//
// UpdateResponse.java
//
//     Defines a UpdateResponse.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transaction.batch.updateresponse;


/**
 *  Defines a <code>UpdateResponse</code>.
 */

public final class UpdateResponse
    extends net.okapia.osid.jamocha.transaction.batch.updateresponse.spi.AbstractUpdateResponse
    implements org.osid.transaction.batch.UpdateResponse {


    /**
     *  Constructs a new successful <code>UpdateResponse</code>.
     *
     *  @param formId the form Id
     *  @param updatedId the updated Id
     *  @throws org.osid.NullArgumentException <code>formId</code> or
     *          <code>updatedId</code> is <code>null</code>
     */

    public UpdateResponse(org.osid.id.Id formId, org.osid.id.Id updatedId) {
        setFormId(formId);
        setUpdatedId(updatedId);
        return;
    }


    /**
     *  Constructs a new unsuccessful <code>UpdateResponse</code>.
     *
     *  @param formId the form Id
     *  @param updatedId the updated Id
     *  @param message the error message
     *  @throws org.osid.NullArgumentException <code>formId</code>,
     *          <code>updatedId</code>, or <code>message</code> is
     *          <code>null</code>
     */

    public UpdateResponse(org.osid.id.Id formId, org.osid.id.Id updatedId,
                          org.osid.locale.DisplayText message) {

        this(formId, updatedId);
        setErrorMessage(message);
        return;
    }
}
