//
// InputElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.input;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class InputElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the InputElement Id.
     *
     *  @return the input element Id
     */

    public static org.osid.id.Id getInputEntityId() {
        return (makeEntityId("osid.control.Input"));
    }


    /**
     *  Gets the DeviceId element Id.
     *
     *  @return the DeviceId element Id
     */

    public static org.osid.id.Id getDeviceId() {
        return (makeElementId("osid.control.input.DeviceId"));
    }


    /**
     *  Gets the Trigger element Id.
     *
     *  @return the Trigger element Id
     */

    public static org.osid.id.Id getTrigger() {
        return (makeElementId("osid.control.input.Trigger"));
    }


    /**
     *  Gets the ControllerId element Id.
     *
     *  @return the ControllerId element Id
     */

    public static org.osid.id.Id getControllerId() {
        return (makeElementId("osid.control.input.ControllerId"));
    }


    /**
     *  Gets the Controller element Id.
     *
     *  @return the Controller element Id
     */

    public static org.osid.id.Id getController() {
        return (makeElementId("osid.control.input.Controller"));
    }


    /**
     *  Gets the Device element Id.
     *
     *  @return the Device element Id
     */

    public static org.osid.id.Id getDevice() {
        return (makeQueryElementId("osid.control.input.Device"));
    }


    /**
     *  Gets the SystemId element Id.
     *
     *  @return the SystemId element Id
     */

    public static org.osid.id.Id getSystemId() {
        return (makeQueryElementId("osid.control.input.SystemId"));
    }


    /**
     *  Gets the System element Id.
     *
     *  @return the System element Id
     */

    public static org.osid.id.Id getSystem() {
        return (makeQueryElementId("osid.control.input.System"));
    }
}
