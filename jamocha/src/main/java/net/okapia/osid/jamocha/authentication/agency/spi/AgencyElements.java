//
// AgencyElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.agency.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class AgencyElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the AgencyElement Id.
     *
     *  @return the agency element Id
     */

    public static org.osid.id.Id getAgencyEntityId() {
        return (makeEntityId("osid.authentication.Agency"));
    }


    /**
     *  Gets the AgentId element Id.
     *
     *  @return the AgentId element Id
     */

    public static org.osid.id.Id getAgentId() {
        return (makeQueryElementId("osid.authentication.agency.AgentId"));
    }


    /**
     *  Gets the Agent element Id.
     *
     *  @return the Agent element Id
     */

    public static org.osid.id.Id getAgent() {
        return (makeQueryElementId("osid.authentication.agency.Agent"));
    }


    /**
     *  Gets the AncestorAgencyId element Id.
     *
     *  @return the AncestorAgencyId element Id
     */

    public static org.osid.id.Id getAncestorAgencyId() {
        return (makeQueryElementId("osid.authentication.agency.AncestorAgencyId"));
    }


    /**
     *  Gets the AncestorAgency element Id.
     *
     *  @return the AncestorAgency element Id
     */

    public static org.osid.id.Id getAncestorAgency() {
        return (makeQueryElementId("osid.authentication.agency.AncestorAgency"));
    }


    /**
     *  Gets the DescendantAgencyId element Id.
     *
     *  @return the DescendantAgencyId element Id
     */

    public static org.osid.id.Id getDescendantAgencyId() {
        return (makeQueryElementId("osid.authentication.agency.DescendantAgencyId"));
    }


    /**
     *  Gets the DescendantAgency element Id.
     *
     *  @return the DescendantAgency element Id
     */

    public static org.osid.id.Id getDescendantAgency() {
        return (makeQueryElementId("osid.authentication.agency.DescendantAgency"));
    }
}
