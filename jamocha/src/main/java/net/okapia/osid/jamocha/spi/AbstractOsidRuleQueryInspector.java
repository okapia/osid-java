//
// AbstractOsidRuleQueryInspector.java
//
//     Defines an OsidRuleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an OsidRuleQueryInspector to extend. 
 */

public abstract class AbstractOsidRuleQueryInspector
    extends AbstractOperableOsidObjectQueryInspector
    implements org.osid.OsidRuleQueryInspector {
    

    /**
     *  Gets the rule <code>Id</code> query terms.
     *
     *  @return the rule <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }

    
    /**
     *  Gets the rule query terms.
     *
     *  @return the rule terms
     */

    @OSID @Override
    public org.osid.rules.RuleQueryInspector[] getRuleTerms() {
        return (new org.osid.rules.RuleQueryInspector[0]);
    }
}

