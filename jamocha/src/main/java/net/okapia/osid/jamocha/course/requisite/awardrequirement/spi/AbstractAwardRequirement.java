//
// AbstractAwardRequirement.java
//
//     Defines an AwardRequirement.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.awardrequirement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AwardRequirement</code>.
 */

public abstract class AbstractAwardRequirement
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.course.requisite.AwardRequirement {

    private org.osid.recognition.Award award;
    private org.osid.calendaring.Duration timeframe;

    private final java.util.Collection<org.osid.course.requisite.Requisite> altRequisites = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.records.AwardRequirementRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets any <code> Requisites </code> that may be substituted in
     *  place of this <code> AwardRequirement. </code> All <code>
     *  Requisites </code> must be satisifed to be a substitute for
     *  this award requirement.  Inactive <code> Requisites </code>
     *  are not evaluated but if no applicable requisite exists, then
     *  the alternate requisite is not satisifed.
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.altRequisites.toArray(new org.osid.course.requisite.Requisite[this.altRequisites.size()]));
    }


    /**
     *  Adds an alternative requisite.
     *
     *  @param altRequisite an alternate requisite
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisite</code> is <code>null</code>
     */

    protected void addAltRequisite(org.osid.course.requisite.Requisite altRequisite) {
        nullarg(altRequisite, "alt requisite");
        this.altRequisites.add(altRequisite);
        return;
    }


    /**
     *  Sets all the alternative requisites.
     *
     *  @param altRequisites a collection of alternate requisites
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisites</code> is <code>null</code>
     */

    protected void setAltRequisites(java.util.Collection<org.osid.course.requisite.Requisite> altRequisites) {
        nullarg(altRequisites, "alt requisites");

        this.altRequisites.clear();
        this.altRequisites.addAll(altRequisites);

        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Award. </code> 
     *
     *  @return the award <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAwardId() {
        return (this.award.getId());
    }


    /**
     *  Gets the <code> Award. </code> 
     *
     *  @return the award 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward()
        throws org.osid.OperationFailedException {

        return (this.award);
    }


    /**
     *  Sets the award.
     *
     *  @param award an award
     *  @throws org.osid.NullArgumentException
     *          <code>award</code> is <code>null</code>
     */

    protected void setAward(org.osid.recognition.Award award) {
        nullarg(award, "award");
        this.award = award;
        return;
    }


    /**
     *  Tests if the award must have been earned within the required duration. 
     *
     *  @return <code> true </code> if the award has to be earned within a 
     *          required time, <code> false </code> if it could have been 
     *          completed at any time in the past 
     */

    @OSID @Override
    public boolean hasTimeframe() {
        return (this.timeframe != null);
    }


    /**
     *  Gets the timeframe in which the award has to be earned. A
     *  negative duration indicates the award had to be completed
     *  within the specified amount of time in the past. A posiitive
     *  duration indicates the award must be completed within the
     *  specified amount of time in the future. A zero duration
     *  indicates the award must be completed in the current term.
     *
     *  @return the time frame 
     *  @throws org.osid.IllegalStateException <code> hasTimeframe()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeframe() {
        if (!hasTimeframe()) {
            throw new org.osid.IllegalStateException("hasTimeframe() is false");
        }

        return (this.timeframe);
    }


    /**
     *  Sets the timeframe.
     *
     *  @param timeframe a timeframe
     *  @throws org.osid.NullArgumentException <code>timeframe</code>
     *          is <code>null</code>
     */

    protected void setTimeframe(org.osid.calendaring.Duration timeframe) {
        nullarg(timeframe, "timeframe");
        this.timeframe = timeframe;
        return;
    }


    /**
     *  Tests if this awardRequirement supports the given record
     *  <code>Type</code>.
     *
     *  @param  awardRequirementRecordType an award requirement record type 
     *  @return <code>true</code> if the awardRequirementRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type awardRequirementRecordType) {
        for (org.osid.course.requisite.records.AwardRequirementRecord record : this.records) {
            if (record.implementsRecordType(awardRequirementRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AwardRequirement</code> record <code>Type</code>.
     *
     *  @param  awardRequirementRecordType the award requirement record type 
     *  @return the award requirement record 
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardRequirementRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.AwardRequirementRecord getAwardRequirementRecord(org.osid.type.Type awardRequirementRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.AwardRequirementRecord record : this.records) {
            if (record.implementsRecordType(awardRequirementRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardRequirementRecordType + " is not supported");
    }


    /**
     *  Adds a record to this award requirement. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param awardRequirementRecord the award requirement record
     *  @param awardRequirementRecordType award requirement record type
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementRecord</code> or
     *          <code>awardRequirementRecordTypeawardRequirement</code>
     *          is <code>null</code>
     */
            
    protected void addAwardRequirementRecord(org.osid.course.requisite.records.AwardRequirementRecord awardRequirementRecord, 
                                             org.osid.type.Type awardRequirementRecordType) {

        nullarg(awardRequirementRecord, "award requirement record");
        addRecordType(awardRequirementRecordType);
        this.records.add(awardRequirementRecord);
        
        return;
    }
}
