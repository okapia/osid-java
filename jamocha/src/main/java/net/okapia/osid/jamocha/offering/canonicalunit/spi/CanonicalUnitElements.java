//
// CanonicalUnitElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.canonicalunit.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CanonicalUnitElements
    extends net.okapia.osid.jamocha.spi.OperableOsidObjectElements {


    /**
     *  Gets the CanonicalUnitElement Id.
     *
     *  @return the canonical unit element Id
     */

    public static org.osid.id.Id getCanonicalUnitEntityId() {
        return (makeEntityId("osid.offering.CanonicalUnit"));
    }


    /**
     *  Gets the Title element Id.
     *
     *  @return the Title element Id
     */

    public static org.osid.id.Id getTitle() {
        return (makeElementId("osid.offering.canonicalunit.Title"));
    }


    /**
     *  Gets the Code element Id.
     *
     *  @return the Code element Id
     */

    public static org.osid.id.Id getCode() {
        return (makeElementId("osid.offering.canonicalunit.Code"));
    }


    /**
     *  Gets the OfferedCyclicTimePeriodIds element Id.
     *
     *  @return the OfferedCyclicTimePeriodIds element Id
     */

    public static org.osid.id.Id getOfferedCyclicTimePeriodIds() {
        return (makeElementId("osid.offering.canonicalunit.OfferedCyclicTimePeriodIds"));
    }


    /**
     *  Gets the OfferedCyclicTimePeriods element Id.
     *
     *  @return the OfferedCyclicTimePeriods element Id
     */

    public static org.osid.id.Id getOfferedCyclicTimePeriods() {
        return (makeElementId("osid.offering.canonicalunit.OfferedCyclicTimePeriods"));
    }


    /**
     *  Gets the ResultOptionIds element Id.
     *
     *  @return the ResultOptionIds element Id
     */

    public static org.osid.id.Id getResultOptionIds() {
        return (makeElementId("osid.offering.canonicalunit.ResultOptionIds"));
    }


    /**
     *  Gets the ResultOptions element Id.
     *
     *  @return the ResultOptions element Id
     */

    public static org.osid.id.Id getResultOptions() {
        return (makeElementId("osid.offering.canonicalunit.ResultOptions"));
    }


    /**
     *  Gets the SponsorIds element Id.
     *
     *  @return the SponsorIds element Id
     */

    public static org.osid.id.Id getSponsorIds() {
        return (makeElementId("osid.offering.canonicalunit.SponsorIds"));
    }


    /**
     *  Gets the Sponsors element Id.
     *
     *  @return the Sponsors element Id
     */

    public static org.osid.id.Id getSponsors() {
        return (makeElementId("osid.offering.canonicalunit.Sponsors"));
    }


    /**
     *  Gets the CatalogueId element Id.
     *
     *  @return the CatalogueId element Id
     */

    public static org.osid.id.Id getCatalogueId() {
        return (makeQueryElementId("osid.offering.canonicalunit.CatalogueId"));
    }


    /**
     *  Gets the Catalogue element Id.
     *
     *  @return the Catalogue element Id
     */

    public static org.osid.id.Id getCatalogue() {
        return (makeQueryElementId("osid.offering.canonicalunit.Catalogue"));
    }
}
