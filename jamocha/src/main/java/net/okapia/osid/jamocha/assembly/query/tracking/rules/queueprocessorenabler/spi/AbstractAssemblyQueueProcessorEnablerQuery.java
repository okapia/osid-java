//
// AbstractAssemblyQueueProcessorEnablerQuery.java
//
//     A QueueProcessorEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.tracking.rules.queueprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A QueueProcessorEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyQueueProcessorEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.tracking.rules.QueueProcessorEnablerQuery,
               org.osid.tracking.rules.QueueProcessorEnablerQueryInspector,
               org.osid.tracking.rules.QueueProcessorEnablerSearchOrder {

    private final java.util.Collection<org.osid.tracking.rules.records.QueueProcessorEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.rules.records.QueueProcessorEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.rules.records.QueueProcessorEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyQueueProcessorEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyQueueProcessorEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the queue processor. 
     *
     *  @param  queueProcessorId the queue processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueProcessorId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledQueueProcessorId(org.osid.id.Id queueProcessorId, 
                                           boolean match) {
        getAssembler().addIdTerm(getRuledQueueProcessorIdColumn(), queueProcessorId, match);
        return;
    }


    /**
     *  Clears the queue processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledQueueProcessorIdTerms() {
        getAssembler().clearTerms(getRuledQueueProcessorIdColumn());
        return;
    }


    /**
     *  Gets the queue processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledQueueProcessorIdTerms() {
        return (getAssembler().getIdTerms(getRuledQueueProcessorIdColumn()));
    }


    /**
     *  Gets the RuledQueueProcessorId column name.
     *
     * @return the column name
     */

    protected String getRuledQueueProcessorIdColumn() {
        return ("ruled_queue_processor_id");
    }


    /**
     *  Tests if a <code> QueueProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledQueueProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the queue processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledQueueProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorQuery getRuledQueueProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledQueueProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any queue processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any queue 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          queue processors 
     */

    @OSID @Override
    public void matchAnyRuledQueueProcessor(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledQueueProcessorColumn(), match);
        return;
    }


    /**
     *  Clears the queue processor query terms. 
     */

    @OSID @Override
    public void clearRuledQueueProcessorTerms() {
        getAssembler().clearTerms(getRuledQueueProcessorColumn());
        return;
    }


    /**
     *  Gets the queue processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorQueryInspector[] getRuledQueueProcessorTerms() {
        return (new org.osid.tracking.rules.QueueProcessorQueryInspector[0]);
    }


    /**
     *  Gets the RuledQueueProcessor column name.
     *
     * @return the column name
     */

    protected String getRuledQueueProcessorColumn() {
        return ("ruled_queue_processor");
    }


    /**
     *  Matches enablers mapped to the front office. 
     *
     *  @param  frontOfficeId the front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFrontOfficeId(org.osid.id.Id frontOfficeId, boolean match) {
        getAssembler().addIdTerm(getFrontOfficeIdColumn(), frontOfficeId, match);
        return;
    }


    /**
     *  Clears the front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeIdTerms() {
        getAssembler().clearTerms(getFrontOfficeIdColumn());
        return;
    }


    /**
     *  Gets the front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFrontOfficeIdTerms() {
        return (getAssembler().getIdTerms(getFrontOfficeIdColumn()));
    }


    /**
     *  Gets the FrontOfficeId column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeIdColumn() {
        return ("front_office_id");
    }


    /**
     *  Tests if an <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a front office. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsFrontOfficeQuery() is false");
    }


    /**
     *  Clears the front office query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeTerms() {
        getAssembler().clearTerms(getFrontOfficeColumn());
        return;
    }


    /**
     *  Gets the front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }


    /**
     *  Gets the FrontOffice column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeColumn() {
        return ("front_office");
    }


    /**
     *  Tests if this queueProcessorEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  queueProcessorEnablerRecordType a queue processor enabler record type 
     *  @return <code>true</code> if the queueProcessorEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type queueProcessorEnablerRecordType) {
        for (org.osid.tracking.rules.records.QueueProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueProcessorEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  queueProcessorEnablerRecordType the queue processor enabler record type 
     *  @return the queue processor enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueProcessorEnablerQueryRecord getQueueProcessorEnablerQueryRecord(org.osid.type.Type queueProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  queueProcessorEnablerRecordType the queue processor enabler record type 
     *  @return the queue processor enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueProcessorEnablerQueryInspectorRecord getQueueProcessorEnablerQueryInspectorRecord(org.osid.type.Type queueProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueProcessorEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(queueProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param queueProcessorEnablerRecordType the queue processor enabler record type
     *  @return the queue processor enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueProcessorEnablerSearchOrderRecord getQueueProcessorEnablerSearchOrderRecord(org.osid.type.Type queueProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueProcessorEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(queueProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this queue processor enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param queueProcessorEnablerQueryRecord the queue processor enabler query record
     *  @param queueProcessorEnablerQueryInspectorRecord the queue processor enabler query inspector
     *         record
     *  @param queueProcessorEnablerSearchOrderRecord the queue processor enabler search order record
     *  @param queueProcessorEnablerRecordType queue processor enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerQueryRecord</code>,
     *          <code>queueProcessorEnablerQueryInspectorRecord</code>,
     *          <code>queueProcessorEnablerSearchOrderRecord</code> or
     *          <code>queueProcessorEnablerRecordTypequeueProcessorEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addQueueProcessorEnablerRecords(org.osid.tracking.rules.records.QueueProcessorEnablerQueryRecord queueProcessorEnablerQueryRecord, 
                                      org.osid.tracking.rules.records.QueueProcessorEnablerQueryInspectorRecord queueProcessorEnablerQueryInspectorRecord, 
                                      org.osid.tracking.rules.records.QueueProcessorEnablerSearchOrderRecord queueProcessorEnablerSearchOrderRecord, 
                                      org.osid.type.Type queueProcessorEnablerRecordType) {

        addRecordType(queueProcessorEnablerRecordType);

        nullarg(queueProcessorEnablerQueryRecord, "queue processor enabler query record");
        nullarg(queueProcessorEnablerQueryInspectorRecord, "queue processor enabler query inspector record");
        nullarg(queueProcessorEnablerSearchOrderRecord, "queue processor enabler search odrer record");

        this.queryRecords.add(queueProcessorEnablerQueryRecord);
        this.queryInspectorRecords.add(queueProcessorEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(queueProcessorEnablerSearchOrderRecord);
        
        return;
    }
}
