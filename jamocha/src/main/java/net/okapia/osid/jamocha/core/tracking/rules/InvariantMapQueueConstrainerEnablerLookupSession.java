//
// InvariantMapQueueConstrainerEnablerLookupSession
//
//    Implements a QueueConstrainerEnabler lookup service backed by a fixed collection of
//    queueConstrainerEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.rules;


/**
 *  Implements a QueueConstrainerEnabler lookup service backed by a fixed
 *  collection of queue constrainer enablers. The queue constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapQueueConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.tracking.rules.spi.AbstractMapQueueConstrainerEnablerLookupSession
    implements org.osid.tracking.rules.QueueConstrainerEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueConstrainerEnablerLookupSession</code> with no
     *  queue constrainer enablers.
     *  
     *  @param frontOffice the front office
     *  @throws org.osid.NullArgumnetException {@code frontOffice} is
     *          {@code null}
     */

    public InvariantMapQueueConstrainerEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice) {
        setFrontOffice(frontOffice);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueConstrainerEnablerLookupSession</code> with a single
     *  queue constrainer enabler.
     *  
     *  @param frontOffice the front office
     *  @param queueConstrainerEnabler a single queue constrainer enabler
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueConstrainerEnabler} is <code>null</code>
     */

      public InvariantMapQueueConstrainerEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                               org.osid.tracking.rules.QueueConstrainerEnabler queueConstrainerEnabler) {
        this(frontOffice);
        putQueueConstrainerEnabler(queueConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueConstrainerEnablerLookupSession</code> using an array
     *  of queue constrainer enablers.
     *  
     *  @param frontOffice the front office
     *  @param queueConstrainerEnablers an array of queue constrainer enablers
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueConstrainerEnablers} is <code>null</code>
     */

      public InvariantMapQueueConstrainerEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                               org.osid.tracking.rules.QueueConstrainerEnabler[] queueConstrainerEnablers) {
        this(frontOffice);
        putQueueConstrainerEnablers(queueConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueConstrainerEnablerLookupSession</code> using a
     *  collection of queue constrainer enablers.
     *
     *  @param frontOffice the front office
     *  @param queueConstrainerEnablers a collection of queue constrainer enablers
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueConstrainerEnablers} is <code>null</code>
     */

      public InvariantMapQueueConstrainerEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                               java.util.Collection<? extends org.osid.tracking.rules.QueueConstrainerEnabler> queueConstrainerEnablers) {
        this(frontOffice);
        putQueueConstrainerEnablers(queueConstrainerEnablers);
        return;
    }
}
