//
// AbstractGradebookNotificationSession.java
//
//     A template for making GradebookNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Gradebook} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Gradebook} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for gradebook entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractGradebookNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.grading.GradebookNotificationSession {


    /**
     *  Tests if this user can register for {@code Gradebook}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForGradebookNotifications() {
        return (true);
    }


    /**
     *  Register for notifications of new gradebooks. {@code
     *  GradebookReceiver.newGradebook()} is invoked when a new {@code
     *  Gradebook} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewGradebooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified
     *  gradebook. {@code GradebookReceiver.newAncestorGradebook()} is
     *  invoked when the specified gradebook node gets a new ancestor.
     *
     *  @param gradebookId the {@code Id} of the
     *         {@code Gradebook} node to monitor
     *  @throws org.osid.NullArgumentException {@code gradebookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewGradebookAncestors(org.osid.id.Id gradebookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  gradebook. {@code GradebookReceiver.newDescendantGradebook()}
     *  is invoked when the specified gradebook node gets a new
     *  descendant.
     *
     *  @param gradebookId the {@code Id} of the
     *         {@code Gradebook} node to monitor
     *  @throws org.osid.NullArgumentException {@code gradebookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewGradebookDescendants(org.osid.id.Id gradebookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated gradebooks. {@code
     *  GradebookReceiver.changedGradebook()} is invoked when a
     *  gradebook is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedGradebooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated gradebook. {@code
     *  GradebookReceiver.changedGradebook()} is invoked when the
     *  specified gradebook is changed.
     *
     *  @param gradebookId the {@code Id} of the {@code Gradebook} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code gradebookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedGradebook(org.osid.id.Id gradebookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted gradebooks. {@code
     *  GradebookReceiver.deletedGradebook()} is invoked when a
     *  gradebook is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedGradebooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted gradebook. {@code
     *  GradebookReceiver.deletedGradebook()} is invoked when the
     *  specified gradebook is deleted.
     *
     *  @param gradebookId the {@code Id} of the
     *          {@code Gradebook} to monitor
     *  @throws org.osid.NullArgumentException {@code gradebookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedGradebook(org.osid.id.Id gradebookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified gradebook. {@code
     *  GradebookReceiver.deletedAncestor()} is invoked when the
     *  specified gradebook node loses an ancestor.
     *
     *  @param gradebookId the {@code Id} of the
     *         {@code Gradebook} node to monitor
     *  @throws org.osid.NullArgumentException {@code gradebookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedGradebookAncestors(org.osid.id.Id gradebookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified gradebook. {@code
     *  GradebookReceiver.deletedDescendant()} is invoked when the
     *  specified gradebook node loses a descendant.
     *
     *  @param gradebookId the {@code Id} of the
     *          {@code Gradebook} node to monitor
     *  @throws org.osid.NullArgumentException {@code gradebookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedGradebookDescendants(org.osid.id.Id gradebookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
