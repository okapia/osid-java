//
// AbstractAssetLookupSession.java
//
//    A starter implementation framework for providing an Asset
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Asset
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAssets(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAssetLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.repository.AssetLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.repository.Repository repository = new net.okapia.osid.jamocha.nil.repository.repository.UnknownRepository();
    

    /**
     *  Gets the <code>Repository/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Repository Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.repository.getId());
    }


    /**
     *  Gets the <code>Repository</code> associated with this session.
     *
     *  @return the <code>Repository</code> associated with this
     *          session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.repository);
    }


    /**
     *  Sets the <code>Repository</code>.
     *
     *  @param  repository the repository for this session
     *  @throws org.osid.NullArgumentException <code>repository</code>
     *          is <code>null</code>
     */

    protected void setRepository(org.osid.repository.Repository repository) {
        nullarg(repository, "repository");
        this.repository = repository;
        return;
    }


    /**
     *  Tests if this user can perform <code>Asset</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssets() {
        return (true);
    }


    /**
     *  A complete view of the <code>Asset</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssetView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Asset</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssetView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assets in repositories which are children of
     *  this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this repository only.
     */

    @OSID @Override
    public void useIsolatedRepositoryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Asset</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Asset</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Asset</code> and retained for
     *  compatibility.
     *
     *  @param  assetId <code>Id</code> of the
     *          <code>Asset</code>
     *  @return the asset
     *  @throws org.osid.NotFoundException <code>assetId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assetId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Asset getAsset(org.osid.id.Id assetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.repository.AssetList assets = getAssets()) {
            while (assets.hasNext()) {
                org.osid.repository.Asset asset = assets.getNextAsset();
                if (asset.getId().equals(assetId)) {
                    return (asset);
                }
            }
        } 

        throw new org.osid.NotFoundException(assetId + " not found");
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the assets
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Assets</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAssets()</code>.
     *
     *  @param  assetIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>assetIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByIds(org.osid.id.IdList assetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.repository.Asset> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = assetIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAsset(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("asset " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.repository.asset.LinkedAssetList(ret));
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  asset genus <code>Type</code> which does not include assets of
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known assets
     *  or an error results. Otherwise, the returned list may contain
     *  only those assets that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAssets()</code>.
     *
     *  @param  assetGenusType an asset genus type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByGenusType(org.osid.type.Type assetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.repository.asset.AssetGenusFilterList(getAssets(), assetGenusType));
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  asset genus <code>Type</code> and include any additional
     *  assets with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known assets
     *  or an error results. Otherwise, the returned list may contain
     *  only those assets that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssets()</code>.
     *
     *  @param  assetGenusType an asset genus type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByParentGenusType(org.osid.type.Type assetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAssetsByGenusType(assetGenusType));
    }


    /**
     *  Gets an <code>AssetList</code> containing the given asset
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known assets
     *  or an error results. Otherwise, the returned list may contain
     *  only those assets that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssets()</code>.
     *
     *  @param  assetRecordType an asset record type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByRecordType(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.repository.asset.AssetRecordFilterList(getAssets(), assetRecordType));
    }


    /**
     *  Gets an <code>AssetList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known assets
     *  or an error results. Otherwise, the returned list may contain
     *  only those assets that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Asset</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.repository.asset.AssetProviderFilterList(getAssets(), resourceId));
    }


    /**
     *  Gets all <code>Assets</code>.
     *
     *  In plenary mode, the returned list contains all known assets
     *  or an error results. Otherwise, the returned list may contain
     *  only those assets that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Assets</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the asset list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of assets
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.repository.AssetList filterAssetsOnViews(org.osid.repository.AssetList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
