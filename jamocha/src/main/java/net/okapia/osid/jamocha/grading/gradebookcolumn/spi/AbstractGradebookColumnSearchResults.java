//
// AbstractGradebookColumnSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumn.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractGradebookColumnSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.grading.GradebookColumnSearchResults {

    private org.osid.grading.GradebookColumnList gradebookColumns;
    private final org.osid.grading.GradebookColumnQueryInspector inspector;
    private final java.util.Collection<org.osid.grading.records.GradebookColumnSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractGradebookColumnSearchResults.
     *
     *  @param gradebookColumns the result set
     *  @param gradebookColumnQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>gradebookColumns</code>
     *          or <code>gradebookColumnQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractGradebookColumnSearchResults(org.osid.grading.GradebookColumnList gradebookColumns,
                                            org.osid.grading.GradebookColumnQueryInspector gradebookColumnQueryInspector) {
        nullarg(gradebookColumns, "gradebook columns");
        nullarg(gradebookColumnQueryInspector, "gradebook column query inspectpr");

        this.gradebookColumns = gradebookColumns;
        this.inspector = gradebookColumnQueryInspector;

        return;
    }


    /**
     *  Gets the gradebook column list resulting from a search.
     *
     *  @return a gradebook column list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumns() {
        if (this.gradebookColumns == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.grading.GradebookColumnList gradebookColumns = this.gradebookColumns;
        this.gradebookColumns = null;
	return (gradebookColumns);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.grading.GradebookColumnQueryInspector getGradebookColumnQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  gradebook column search record <code> Type. </code> This method must
     *  be used to retrieve a gradebookColumn implementing the requested
     *  record.
     *
     *  @param gradebookColumnSearchRecordType a gradebookColumn search 
     *         record type 
     *  @return the gradebook column search
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(gradebookColumnSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSearchResultsRecord getGradebookColumnSearchResultsRecord(org.osid.type.Type gradebookColumnSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.grading.records.GradebookColumnSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(gradebookColumnSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record gradebook column search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addGradebookColumnRecord(org.osid.grading.records.GradebookColumnSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "gradebook column record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
