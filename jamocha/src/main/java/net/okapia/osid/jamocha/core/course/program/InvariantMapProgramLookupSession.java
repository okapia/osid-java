//
// InvariantMapProgramLookupSession
//
//    Implements a Program lookup service backed by a fixed collection of
//    programs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements a Program lookup service backed by a fixed
 *  collection of programs. The programs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProgramLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractMapProgramLookupSession
    implements org.osid.course.program.ProgramLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapProgramLookupSession</code> with no
     *  programs.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapProgramLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProgramLookupSession</code> with a single
     *  program.
     *  
     *  @param courseCatalog the course catalog
     *  @param program a single program
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code program} is <code>null</code>
     */

      public InvariantMapProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.program.Program program) {
        this(courseCatalog);
        putProgram(program);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProgramLookupSession</code> using an array
     *  of programs.
     *  
     *  @param courseCatalog the course catalog
     *  @param programs an array of programs
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code programs} is <code>null</code>
     */

      public InvariantMapProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.program.Program[] programs) {
        this(courseCatalog);
        putPrograms(programs);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProgramLookupSession</code> using a
     *  collection of programs.
     *
     *  @param courseCatalog the course catalog
     *  @param programs a collection of programs
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code programs} is <code>null</code>
     */

      public InvariantMapProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.program.Program> programs) {
        this(courseCatalog);
        putPrograms(programs);
        return;
    }
}
