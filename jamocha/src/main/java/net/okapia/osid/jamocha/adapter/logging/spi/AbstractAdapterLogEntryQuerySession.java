//
// AbstractQueryLogEntryLookupSession.java
//
//    A LogEntryQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.logging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A LogEntryQuerySession adapter.
 */

public abstract class AbstractAdapterLogEntryQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.logging.LogEntryQuerySession {

    private final org.osid.logging.LogEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterLogEntryQuerySession.
     *
     *  @param session the underlying log entry query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterLogEntryQuerySession(org.osid.logging.LogEntryQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeLog</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeLog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getLogId() {
        return (this.session.getLogId());
    }


    /**
     *  Gets the {@codeLog</code> associated with this 
     *  session.
     *
     *  @return the {@codeLog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.Log getLog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getLog());
    }


    /**
     *  Tests if this user can perform {@codeLogEntry</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchLogEntries() {
        return (this.session.canSearchLogEntries());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include log entries in logs which are children
     *  of this log in the log hierarchy.
     */

    @OSID @Override
    public void useFederatedLogView() {
        this.session.useFederatedLogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this log only.
     */
    
    @OSID @Override
    public void useIsolatedLogView() {
        this.session.useIsolatedLogView();
        return;
    }
    
      
    /**
     *  Gets a log entry query. The returned query will not have an
     *  extension query.
     *
     *  @return the log entry query 
     */
      
    @OSID @Override
    public org.osid.logging.LogEntryQuery getLogEntryQuery() {
        return (this.session.getLogEntryQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  logEntryQuery the log entry query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code logEntryQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code logEntryQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByQuery(org.osid.logging.LogEntryQuery logEntryQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getLogEntriesByQuery(logEntryQuery));
    }
}
