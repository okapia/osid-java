//
// AbstractMapJobProcessorLookupSession
//
//    A simple framework for providing a JobProcessor lookup service
//    backed by a fixed collection of job processors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a JobProcessor lookup service backed by a
 *  fixed collection of job processors. The job processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>JobProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapJobProcessorLookupSession
    extends net.okapia.osid.jamocha.resourcing.rules.spi.AbstractJobProcessorLookupSession
    implements org.osid.resourcing.rules.JobProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.rules.JobProcessor> jobProcessors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.rules.JobProcessor>());


    /**
     *  Makes a <code>JobProcessor</code> available in this session.
     *
     *  @param  jobProcessor a job processor
     *  @throws org.osid.NullArgumentException <code>jobProcessor<code>
     *          is <code>null</code>
     */

    protected void putJobProcessor(org.osid.resourcing.rules.JobProcessor jobProcessor) {
        this.jobProcessors.put(jobProcessor.getId(), jobProcessor);
        return;
    }


    /**
     *  Makes an array of job processors available in this session.
     *
     *  @param  jobProcessors an array of job processors
     *  @throws org.osid.NullArgumentException <code>jobProcessors<code>
     *          is <code>null</code>
     */

    protected void putJobProcessors(org.osid.resourcing.rules.JobProcessor[] jobProcessors) {
        putJobProcessors(java.util.Arrays.asList(jobProcessors));
        return;
    }


    /**
     *  Makes a collection of job processors available in this session.
     *
     *  @param  jobProcessors a collection of job processors
     *  @throws org.osid.NullArgumentException <code>jobProcessors<code>
     *          is <code>null</code>
     */

    protected void putJobProcessors(java.util.Collection<? extends org.osid.resourcing.rules.JobProcessor> jobProcessors) {
        for (org.osid.resourcing.rules.JobProcessor jobProcessor : jobProcessors) {
            this.jobProcessors.put(jobProcessor.getId(), jobProcessor);
        }

        return;
    }


    /**
     *  Removes a JobProcessor from this session.
     *
     *  @param  jobProcessorId the <code>Id</code> of the job processor
     *  @throws org.osid.NullArgumentException <code>jobProcessorId<code> is
     *          <code>null</code>
     */

    protected void removeJobProcessor(org.osid.id.Id jobProcessorId) {
        this.jobProcessors.remove(jobProcessorId);
        return;
    }


    /**
     *  Gets the <code>JobProcessor</code> specified by its <code>Id</code>.
     *
     *  @param  jobProcessorId <code>Id</code> of the <code>JobProcessor</code>
     *  @return the jobProcessor
     *  @throws org.osid.NotFoundException <code>jobProcessorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>jobProcessorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessor getJobProcessor(org.osid.id.Id jobProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.rules.JobProcessor jobProcessor = this.jobProcessors.get(jobProcessorId);
        if (jobProcessor == null) {
            throw new org.osid.NotFoundException("jobProcessor not found: " + jobProcessorId);
        }

        return (jobProcessor);
    }


    /**
     *  Gets all <code>JobProcessors</code>. In plenary mode, the returned
     *  list contains all known jobProcessors or an error
     *  results. Otherwise, the returned list may contain only those
     *  jobProcessors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>JobProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobprocessor.ArrayJobProcessorList(this.jobProcessors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.jobProcessors.clear();
        super.close();
        return;
    }
}
