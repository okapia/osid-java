//
// BufferedFiscalPeriodBatchFormList.java
//
//     Implements a FiscalPeriodBatchFormList. This list wraps another list with
//     an internal buffer.
//
//
// Tom Coppeto
// OnTapSolutions
// 17 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.financials.batch.fiscalperiodbatchform;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a FiscalPeriodBatchFormList. This list wraps another list in a
 *  buffer.
 */

public final class BufferedFiscalPeriodBatchFormList
    extends net.okapia.osid.jamocha.financials.batch.fiscalperiodbatchform.spi.AbstractFiscalPeriodBatchFormList
    implements org.osid.financials.batch.FiscalPeriodBatchFormList,
               Runnable {

    private java.util.Queue<org.osid.financials.batch.FiscalPeriodBatchForm> stack = new java.util.concurrent.ConcurrentLinkedQueue<>();
    private org.osid.financials.batch.FiscalPeriodBatchFormList fiscalPeriodBatchFormList;
    private int bufferSize = 10;
    private volatile boolean eol = false;

    private volatile boolean running = false;
    private volatile int consumerIsWaiting = 0;
    private volatile int providerIsWaiting = 0;

    private volatile org.osid.OsidException error;
    private FiscalPeriodBatchFormListCallback callback;


    /**
     *  Creates a new <code>BufferedFiscalPeriodBatchFormList</code>.
     *
     *  @param fiscalPeriodBatchFormList a <code>FiscalPeriodBatchFormList</code>
     *  @throws org.osid.NullArgumentException <code>fiscalPeriodBatchFormList</code>
     *          is <code>null</code>
     */

    public BufferedFiscalPeriodBatchFormList(org.osid.financials.batch.FiscalPeriodBatchFormList fiscalPeriodBatchFormList) {
        nullarg(fiscalPeriodBatchFormList, "fiscalPeriodBatchForm list");
        this.fiscalPeriodBatchFormList = fiscalPeriodBatchFormList;
        return;
    }


    /**
     *  Creates a new <code>BufferedFiscalPeriodBatchFormList</code>.
     *
     *  @param fiscalPeriodBatchFormList a <code>FiscalPeriodBatchFormList</code>
     *  @param callback callback interface for events
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodBatchFormList</code> is <code>null</code>
     */

    public BufferedFiscalPeriodBatchFormList(org.osid.financials.batch.FiscalPeriodBatchFormList fiscalPeriodBatchFormList, FiscalPeriodBatchFormListCallback callback) {
        nullarg(fiscalPeriodBatchFormList, "fiscalperiodbatchformlist");
        nullarg(callback, "callback");

        this.fiscalPeriodBatchFormList = fiscalPeriodBatchFormList;
        this.callback   = callback;

        return;
    }


    /**
     *  Creates a new <code>BufferedFiscalPeriodBatchFormList</code>.
     *
     *  @param fiscalPeriodBatchFormList a <code>FiscalPeriodBatchFormList</code>
     *  @param bufferSize number of xlabelsx to buffer
     *  @param callback callback interface for events
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodBatchFormList</code> is <code>null</code>
     *  @throws org.osid.InvalidArgumentException buffer size must be
     *          greater than zero
     */

    public BufferedFiscalPeriodBatchFormList(org.osid.financials.batch.FiscalPeriodBatchFormList fiscalPeriodBatchFormList, int bufferSize, 
                                FiscalPeriodBatchFormListCallback callback) {

        nullarg(fiscalPeriodBatchFormList, "fiscalperiodbatchform list");
        nullarg(callback, "callback");

        if (bufferSize < 1) {
            throw new org.osid.InvalidArgumentException("buffer size must be at least 1");
        }

        this.fiscalPeriodBatchFormList = fiscalPeriodBatchFormList;
        this.bufferSize   = bufferSize;
        this.callback     = callback;

        return;
    }


    /**
     *  Creates a new <code>BufferedFiscalPeriodBatchFormList</code> and runs it
     *  upon instantiation.
     *
     *  @param fiscalPeriodBatchFormList a <code>FiscalPeriodBatchFormList</code>
     *  @param bufferSize number of fiscalperiodbatchforms to buffer
     *  @param callback callback interface for events
     *  @param run
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodBatchFormList</code> is <code>null</code>
     *  @throws org.osid.InvalidArgumentException buffer size must be greater
     *          than zero
     */

    public BufferedFiscalPeriodBatchFormList(org.osid.financials.batch.FiscalPeriodBatchFormList fiscalPeriodBatchFormList, int bufferSize, 
                                FiscalPeriodBatchFormListCallback callback, boolean run) {

        this(fiscalPeriodBatchFormList, bufferSize, callback);
        run();
        return;
    }


    /**
     *  Starts buffering fiscalperiodbatchforms.
     *
     *  @throws org.osid.IllegalStateException already started
     */

    @Override
    public void run() {
        if (this.running || this.eol) {
            throw new org.osid.IllegalStateException("already ran");
        }

        (new FiscalPeriodBatchFormFetcher()).start();
        return;
    }

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        synchronized (this.stack) {
            while (true) {
                if (this.stack.size() > 0) {
                    return (true);
                }

                if (hasError()) {
                    return (true);
                }

                if (this.eol) {
                    return (false);
                }

                waitForProvider();
            }
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The number 
     *  returned by this method may be less than or equal to the total number 
     *  of elements in this list. To determine if the end of the list has been 
     *  reached, the method <code> hasNext() </code> should be used. This 
     *  method conveys what is known about the number of remaining elements at 
     *  a point in time and can be used to determine a minimum size of the 
     *  remaining elements, if known. A valid return is zero even if <code> 
     *  hasNext() </code> is true. 
     *  <p>
     *  This method does not imply asynchronous usage. All OSID methods may 
     *  block. 
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        synchronized (this.stack) {
            return (this.stack.size());
        }
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        synchronized (this.stack) {
            while (n > 0) {

                if (hasError()) {
                    return;
                }

                /* blasted protected removeRange */
                if (this.stack.size() > 0) {
                    this.stack.remove();
                    --n;
                } else if (this.eol) {
                    return;
                } else {
                    waitForProvider();
                }
            }
            
            notifyProvider();
        }

        return;
    }


    /**
     *  Gets the next <code>FiscalPeriodBatchForm</code> in this list. 
     *
     *  @return the next <code>FiscalPeriodBatchForm</code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code>FiscalPeriodBatchForm</code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.batch.FiscalPeriodBatchForm getNextFiscalPeriodBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            synchronized (this.stack) {
                org.osid.financials.batch.FiscalPeriodBatchForm ret = this.stack.remove();
                notifyProvider();
                return (ret);
            }
        } else {
            throw new org.osid.IllegalStateException("no more elements available in fiscalperiodbatchform list");
        }
    }

        
    /**
     *  Gets the next set of <code>FiscalPeriodBatchForm</code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param  n the number of <code>FiscalPeriodBatchForm</code> elements requested which 
     *          must be less than or equal to <code> available() </code> 
     *  @return an array of <code>FiscalPeriodBatchForm</code> elements. <code> </code> The 
     *          length of the array is less than or equal to the number 
     *          specified. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.batch.FiscalPeriodBatchForm[] getNextFiscalPeriodBatchForms(long n)
        throws org.osid.OperationFailedException {

        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        org.osid.financials.batch.FiscalPeriodBatchForm[] ret = new org.osid.financials.batch.FiscalPeriodBatchForm[(int) n];

        synchronized (this.stack) {
            for (int i = 0; i < n; i++) {
                ret[i] = this.stack.remove();
            }

            notifyProvider();
        }       

        return (ret);
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.stack = null;
        this.running = false;
        
        notifyProvider();

        if (this.callback != null) {
            this.callback.doneForList(this);
        }

        return;
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked and no error has
     *  occurred.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    public boolean isOperational() {
        if (hasError()) {
            return (false);
        }

        if (this.stack == null) {
            return (false);
        }

        
        return (true);
    }


    /**
     *  Tests if the list thread is running to populate elements from
     *  the underlying list.
     *
     *  @return <code>true</code> if the list is running,
     *          <code>false</code> otherwise.
     */

    public boolean isRunning() {
        return (this.running);
    }


    /**
     *  Tests if the list is empty however it may still be running.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty() {
        if (this.stack == null) {
            return (true);
        } else if (this.stack.size() > 0) {
            return (false);
        }

        return (true);
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    private void error(org.osid.OsidException error) {
        this.error = error;
        notifyConsumer();

        if (this.callback != null) {
            this.callback.errorInList(this);
        }

        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    private boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  There are no more elements to be added to this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    private void eol() {
        this.eol = true;
        return;
    }

    
    /* wait/notify on the stack */

    public void waitForProvider() {
        this.consumerIsWaiting++;

        try {
            this.stack.wait();
        } catch (InterruptedException ie) { }

        this.consumerIsWaiting--;
        return;
    }


    private void notifyProvider() {
        if (this.providerIsWaiting > 0) {
            this.stack.notify();
        }

        return;
    }


    private void waitForConsumer() {
        this.providerIsWaiting++;

        try {
            this.stack.wait();
        } catch (InterruptedException ie) { }

        this.providerIsWaiting--;
        return;
    }


    private void notifyNewFiscalPeriodBatchForm() {
        notifyConsumer();
        if (this.callback != null) {
            this.callback.newFiscalPeriodBatchFormInList(this);
        }
    }
        

    private void notifyConsumer() {     
        if (this.consumerIsWaiting > 0) {
            this.stack.notify();
        }

        return;
    }

 
    class FiscalPeriodBatchFormFetcher
        extends Thread {
        
        public void run() {
            BufferedFiscalPeriodBatchFormList enc = BufferedFiscalPeriodBatchFormList.this;
            enc.running = true;

            try {
                while (enc.fiscalPeriodBatchFormList.hasNext()) {
                    if (!enc.running) {
                        return;
                    }

                    synchronized (enc.stack) {
                        if (enc.stack.size() < enc.bufferSize) {
                            enc.stack.add(enc.fiscalPeriodBatchFormList.getNextFiscalPeriodBatchForm());
                            notifyNewFiscalPeriodBatchForm();
                        } else {
                            waitForConsumer();
                        }
                    }
                }
            } catch (Exception e) {
                enc.running = false;
                enc.error(new org.osid.OperationFailedException(e));
                return;
            } finally {
                try {
                    enc.fiscalPeriodBatchFormList.close();
                } catch (Exception e) {}
            }

            enc.running = false;            
            enc.eol();
            return;
        }
    }
}
