//
// MutableMapProcessLookupSession
//
//    Implements a Process lookup service backed by a collection of
//    processes that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process;


/**
 *  Implements a Process lookup service backed by a collection of
 *  processes. The processes are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of processes can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProcessLookupSession
    extends net.okapia.osid.jamocha.core.process.spi.AbstractMapProcessLookupSession
    implements org.osid.process.ProcessLookupSession {


    /**
     *  Constructs a new {@code MutableMapProcessLookupSession}
     *  with no processes.
     */

    public MutableMapProcessLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProcessLookupSession} with a
     *  single process.
     *  
     *  @param process a process
     *  @throws org.osid.NullArgumentException {@code process}
     *          is {@code null}
     */

    public MutableMapProcessLookupSession(org.osid.process.Process process) {
        putProcess(process);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProcessLookupSession}
     *  using an array of processes.
     *
     *  @param processes an array of processes
     *  @throws org.osid.NullArgumentException {@code processes}
     *          is {@code null}
     */

    public MutableMapProcessLookupSession(org.osid.process.Process[] processes) {
        putProcesses(processes);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProcessLookupSession}
     *  using a collection of processes.
     *
     *  @param processes a collection of processes
     *  @throws org.osid.NullArgumentException {@code processes}
     *          is {@code null}
     */

    public MutableMapProcessLookupSession(java.util.Collection<? extends org.osid.process.Process> processes) {
        putProcesses(processes);
        return;
    }

    
    /**
     *  Makes a {@code Process} available in this session.
     *
     *  @param process a process
     *  @throws org.osid.NullArgumentException {@code process{@code  is
     *          {@code null}
     */

    @Override
    public void putProcess(org.osid.process.Process process) {
        super.putProcess(process);
        return;
    }


    /**
     *  Makes an array of processes available in this session.
     *
     *  @param processes an array of processes
     *  @throws org.osid.NullArgumentException {@code processes{@code 
     *          is {@code null}
     */

    @Override
    public void putProcesses(org.osid.process.Process[] processes) {
        super.putProcesses(processes);
        return;
    }


    /**
     *  Makes collection of processes available in this session.
     *
     *  @param processes a collection of processes
     *  @throws org.osid.NullArgumentException {@code processes{@code  is
     *          {@code null}
     */

    @Override
    public void putProcesses(java.util.Collection<? extends org.osid.process.Process> processes) {
        super.putProcesses(processes);
        return;
    }


    /**
     *  Removes a Process from this session.
     *
     *  @param processId the {@code Id} of the process
     *  @throws org.osid.NullArgumentException {@code processId{@code 
     *          is {@code null}
     */

    @Override
    public void removeProcess(org.osid.id.Id processId) {
        super.removeProcess(processId);
        return;
    }    
}
