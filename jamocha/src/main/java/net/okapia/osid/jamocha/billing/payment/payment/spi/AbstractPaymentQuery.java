//
// AbstractPaymentQuery.java
//
//     A template for making a Payment Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for payments.
 */

public abstract class AbstractPaymentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.billing.payment.PaymentQuery {

    private final java.util.Collection<org.osid.billing.payment.records.PaymentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the payer <code> Id </code> for this query. 
     *
     *  @param  payerId a payer <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> payerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPayerId(org.osid.id.Id payerId, boolean match) {
        return;
    }


    /**
     *  Clears the payer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPayerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PayerQuery </code> is available. 
     *
     *  @return <code> true </code> if a payer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a payer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the payer query 
     *  @throws org.osid.UnimplementedException <code> supportsPayerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuery getPayerQuery() {
        throw new org.osid.UnimplementedException("supportsPayerQuery() is false");
    }


    /**
     *  Clears the payer terms. 
     */

    @OSID @Override
    public void clearPayerTerms() {
        return;
    }


    /**
     *  Sets the customer <code> Id </code> for this query. 
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> customerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id customerId, boolean match) {
        return;
    }


    /**
     *  Clears the customer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CustomereQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a customer query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Clears the customer terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        return;
    }


    /**
     *  Sets the biliing period <code> Id </code> for this query. 
     *
     *  @param  periodId a period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> periodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPeriodId(org.osid.id.Id periodId, boolean match) {
        return;
    }


    /**
     *  Clears the billing period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a period query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the period query 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuery getPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsPeriodQuery() is false");
    }


    /**
     *  Matches payments with any billing period. 
     *
     *  @param  match <code> true </code> to match payments with any period, 
     *          <code> false </code> to match peyments with no period 
     */

    @OSID @Override
    public void matchAnyPeriod(boolean match) {
        return;
    }


    /**
     *  Clears the billing period terms. 
     */

    @OSID @Override
    public void clearPeriodTerms() {
        return;
    }


    /**
     *  Matches payment dates between the given date range inclusive. 
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPaymentDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the payment date terms. 
     */

    @OSID @Override
    public void clearPaymentDateTerms() {
        return;
    }


    /**
     *  Matches process dates between the given date range inclusive. 
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchProcessDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches payments with any process date. 
     *
     *  @param  match <code> true </code> to match payments with any process 
     *          date, <code> false </code> to match peyments with no process 
     *          date 
     */

    @OSID @Override
    public void matchAnyProcessDate(boolean match) {
        return;
    }


    /**
     *  Clears the process date terms. 
     */

    @OSID @Override
    public void clearProcessDateTerms() {
        return;
    }


    /**
     *  Matches amounts between the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency from, 
                            org.osid.financials.Currency to, boolean match) {
        return;
    }


    /**
     *  Matches payments with any amount. 
     *
     *  @param  match <code> true </code> to match payments with any amount, 
     *          <code> false </code> to match peyments with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        return;
    }


    /**
     *  Clears the amount. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match payments 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given payment query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a payment implementing the requested record.
     *
     *  @param paymentRecordType a payment record type
     *  @return the payment query record
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(paymentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentQueryRecord getPaymentQueryRecord(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PaymentQueryRecord record : this.records) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(paymentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this payment query. 
     *
     *  @param paymentQueryRecord payment query record
     *  @param paymentRecordType payment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPaymentQueryRecord(org.osid.billing.payment.records.PaymentQueryRecord paymentQueryRecord, 
                                          org.osid.type.Type paymentRecordType) {

        addRecordType(paymentRecordType);
        nullarg(paymentQueryRecord, "payment query record");
        this.records.add(paymentQueryRecord);        
        return;
    }
}
