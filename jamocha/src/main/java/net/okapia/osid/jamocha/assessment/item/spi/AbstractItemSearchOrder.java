//
// AbstractItemSearchOdrer.java
//
//     Defines an ItemSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.item.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code ItemSearchOrder}.
 */

public abstract class AbstractItemSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.assessment.ItemSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.ItemSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the question. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuestion(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a question search order is available. 
     *
     *  @return <code> true </code> if a question search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQuestionSearchOrder() {
        return (false);
    }


    /**
     *  Gets a question search order. 
     *
     *  @return a question search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQuestionSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.QuestionSearchOrder getQuestionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsQuestionSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  itemRecordType an item record type 
     *  @return {@code true} if the itemRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code itemRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type itemRecordType) {
        for (org.osid.assessment.records.ItemSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item search order record
     *  @throws org.osid.NullArgumentException
     *          {@code itemRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(itemRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.assessment.records.ItemSearchOrderRecord getItemSearchOrderRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.ItemSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this item. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param itemRecord the item search odrer record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException
     *          {@code itemRecord} or
     *          {@code itemRecordTypeitem} is
     *          {@code null}
     */
            
    protected void addItemRecord(org.osid.assessment.records.ItemSearchOrderRecord itemSearchOrderRecord, 
                                     org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);
        this.records.add(itemSearchOrderRecord);
        
        return;
    }
}
