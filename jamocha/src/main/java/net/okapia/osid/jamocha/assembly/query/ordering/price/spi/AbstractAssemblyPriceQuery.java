//
// AbstractAssemblyPriceQuery.java
//
//     A PriceQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ordering.price.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PriceQuery that stores terms.
 */

public abstract class AbstractAssemblyPriceQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.ordering.PriceQuery,
               org.osid.ordering.PriceQueryInspector,
               org.osid.ordering.PriceSearchOrder {

    private final java.util.Collection<org.osid.ordering.records.PriceQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.PriceQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.PriceSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPriceQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPriceQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the price schedule <code> Id </code> for this query. 
     *
     *  @param  priceScheduleId a price schedule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchPriceScheduleId(org.osid.id.Id priceScheduleId, 
                                     boolean match) {
        getAssembler().addIdTerm(getPriceScheduleIdColumn(), priceScheduleId, match);
        return;
    }


    /**
     *  Clears the price schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPriceScheduleIdTerms() {
        getAssembler().clearTerms(getPriceScheduleIdColumn());
        return;
    }


    /**
     *  Gets the price schedule <code> Id </code> terms. 
     *
     *  @return the price schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPriceScheduleIdTerms() {
        return (getAssembler().getIdTerms(getPriceScheduleIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the price 
     *  schedule. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPriceSchedule(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPriceScheduleColumn(), style);
        return;
    }


    /**
     *  Gets the PriceScheduleId column name.
     *
     * @return the column name
     */

    protected String getPriceScheduleIdColumn() {
        return ("price_schedule_id");
    }


    /**
     *  Tests if a price schedule query is available. 
     *
     *  @return <code> true </code> if a price schedule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price schedule. 
     *
     *  @return the price schedule query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuery getPriceScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsPriceScheduleQuery() is false");
    }


    /**
     *  Clears the price schedule terms. 
     */

    @OSID @Override
    public void clearPriceScheduleTerms() {
        getAssembler().clearTerms(getPriceScheduleColumn());
        return;
    }


    /**
     *  Gets the price schedule terms. 
     *
     *  @return the price schedule terms 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQueryInspector[] getPriceScheduleTerms() {
        return (new org.osid.ordering.PriceScheduleQueryInspector[0]);
    }


    /**
     *  Tests if a price schedule order is available. 
     *
     *  @return <code> true </code> if a price schedule order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the price schedule order. 
     *
     *  @return the price schedule search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSearchOrder getPriceScheduleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPriceScheduleSearchOrder() is false");
    }


    /**
     *  Gets the PriceSchedule column name.
     *
     * @return the column name
     */

    protected String getPriceScheduleColumn() {
        return ("price_schedule");
    }


    /**
     *  Matches the minimum quantity between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMinimumQuantity(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getMinimumQuantityColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the minimum quantity terms. 
     */

    @OSID @Override
    public void clearMinimumQuantityTerms() {
        getAssembler().clearTerms(getMinimumQuantityColumn());
        return;
    }


    /**
     *  Gets the minimum quantity terms. 
     *
     *  @return the minimum quantity terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumQuantityTerms() {
        return (getAssembler().getCardinalRangeTerms(getMinimumQuantityColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the minimum 
     *  quantity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumQuantity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMinimumQuantityColumn(), style);
        return;
    }


    /**
     *  Gets the MinimumQuantity column name.
     *
     * @return the column name
     */

    protected String getMinimumQuantityColumn() {
        return ("minimum_quantity");
    }


    /**
     *  Matches the maximum quantity between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMaximumQuantity(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getMaximumQuantityColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the maximum quantity terms. 
     */

    @OSID @Override
    public void clearMaximumQuantityTerms() {
        getAssembler().clearTerms(getMaximumQuantityColumn());
        return;
    }


    /**
     *  Gets the maximum quantity terms. 
     *
     *  @return the maximum quantity terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMaximumQuantityTerms() {
        return (getAssembler().getCardinalRangeTerms(getMaximumQuantityColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the maximum 
     *  quanttity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumQuantity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMaximumQuantityColumn(), style);
        return;
    }


    /**
     *  Gets the MaximumQuantity column name.
     *
     * @return the column name
     */

    protected String getMaximumQuantityColumn() {
        return ("maximum_quantity");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resource </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDemographicId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getDemographicIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDemographicIdTerms() {
        getAssembler().clearTerms(getDemographicIdColumn());
        return;
    }


    /**
     *  Gets the demographic <code> Id </code> terms. 
     *
     *  @return the demographic <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDemographicIdTerms() {
        return (getAssembler().getIdTerms(getDemographicIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the demographic. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDemographic(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDemographicColumn(), style);
        return;
    }


    /**
     *  Gets the DemographicId column name.
     *
     * @return the column name
     */

    protected String getDemographicIdColumn() {
        return ("demographic_id");
    }


    /**
     *  Tests if a resource query is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsDemographicQuery() is false");
    }


    /**
     *  Matches prices with any demographic. 
     *
     *  @param  match <code> true </code> to match prices with any 
     *          demographic, <code> false </code> to match prices with no 
     *          orders 
     */

    @OSID @Override
    public void matchAnyDemographic(boolean match) {
        getAssembler().addIdWildcardTerm(getDemographicColumn(), match);
        return;
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearDemographicTerms() {
        getAssembler().clearTerms(getDemographicColumn());
        return;
    }


    /**
     *  Gets the demographic terms. 
     *
     *  @return the demographic terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getDemographicTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a demographic order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getDemographicSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDemographicSearchOrder() is false");
    }


    /**
     *  Gets the Demographic column name.
     *
     * @return the column name
     */

    protected String getDemographicColumn() {
        return ("demographic");
    }


    /**
     *  Matches the amount between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency low, 
                            org.osid.financials.Currency high, boolean match) {
        getAssembler().addCurrencyRangeTerm(getAmountColumn(), low, high, match);
        return;
    }


    /**
     *  Matches items that have any amount set. 
     *
     *  @param  match <code> true </code> to match items with any amount, 
     *          <code> false </code> to match items with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getAmountColumn(), match);
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        getAssembler().clearTerms(getAmountColumn());
        return;
    }


    /**
     *  Gets the amount terms. 
     *
     *  @return the amount terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (getAssembler().getCurrencyRangeTerms(getAmountColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the amount. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAmountColumn(), style);
        return;
    }


    /**
     *  Gets the Amount column name.
     *
     * @return the column name
     */

    protected String getAmountColumn() {
        return ("amount");
    }


    /**
     *  Matches the a minimum price amount. 
     *
     *  @param  amount an amount 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumAmount(org.osid.financials.Currency amount, 
                                   boolean match) {
        getAssembler().addCurrencyTerm(getMinimumAmountColumn(), amount, match);
        return;
    }


    /**
     *  Clears the minimum amount terms. 
     */

    @OSID @Override
    public void clearMinimumAmountTerms() {
        getAssembler().clearTerms(getMinimumAmountColumn());
        return;
    }


    /**
     *  Gets the minimum amount terms. 
     *
     *  @return the minimum amount terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyTerm[] getMinimumAmountTerms() {
        return (getAssembler().getCurrencyTerms(getMinimumAmountColumn()));
    }


    /**
     *  Gets the MinimumAmount column name.
     *
     * @return the column name
     */

    protected String getMinimumAmountColumn() {
        return ("minimum_amount");
    }


    /**
     *  Matches the recurring interval between the given duration range 
     *  inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRecurringInterval(org.osid.calendaring.Duration low, 
                                       org.osid.calendaring.Duration high, 
                                       boolean match) {
        getAssembler().addDurationRangeTerm(getRecurringIntervalColumn(), low, high, match);
        return;
    }


    /**
     *  Matches prices with any recurring interval. 
     *
     *  @param  match <code> true </code> to match prices with any recurring 
     *          interval, <code> false </code> to match one-time prices 
     */

    @OSID @Override
    public void matchAnyRecurringInterval(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getRecurringIntervalColumn(), match);
        return;
    }


    /**
     *  Clears the recurring interval terms. 
     */

    @OSID @Override
    public void clearRecurringIntervalTerms() {
        getAssembler().clearTerms(getRecurringIntervalColumn());
        return;
    }


    /**
     *  Gets the recurring interval terms. 
     *
     *  @return the recurring interval terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getRecurringIntervalTerms() {
        return (getAssembler().getDurationRangeTerms(getRecurringIntervalColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the recurring 
     *  interval. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecurringInterval(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRecurringIntervalColumn(), style);
        return;
    }


    /**
     *  Gets the RecurringInterval column name.
     *
     * @return the column name
     */

    protected String getRecurringIntervalColumn() {
        return ("recurring_interval");
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if an item query is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches prices used with any item. 
     *
     *  @param  match <code> true </code> to match prices with any order, 
     *          <code> false </code> to match prices with no orders 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item terms. 
     *
     *  @return the item terms 
     */

    @OSID @Override
    public org.osid.ordering.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.ordering.ItemQueryInspector[0]);
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Sets the price <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        getAssembler().addIdTerm(getStoreIdColumn(), storeId, match);
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        getAssembler().clearTerms(getStoreIdColumn());
        return;
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (getAssembler().getIdTerms(getStoreIdColumn()));
    }


    /**
     *  Gets the StoreId column name.
     *
     * @return the column name
     */

    protected String getStoreIdColumn() {
        return ("store_id");
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        getAssembler().clearTerms(getStoreColumn());
        return;
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the Store column name.
     *
     * @return the column name
     */

    protected String getStoreColumn() {
        return ("store");
    }


    /**
     *  Tests if this price supports the given record
     *  <code>Type</code>.
     *
     *  @param  priceRecordType a price record type 
     *  @return <code>true</code> if the priceRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>priceRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type priceRecordType) {
        for (org.osid.ordering.records.PriceQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(priceRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  priceRecordType the price record type 
     *  @return the price query record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceQueryRecord getPriceQueryRecord(org.osid.type.Type priceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(priceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  priceRecordType the price record type 
     *  @return the price query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceQueryInspectorRecord getPriceQueryInspectorRecord(org.osid.type.Type priceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(priceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param priceRecordType the price record type
     *  @return the price search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceSearchOrderRecord getPriceSearchOrderRecord(org.osid.type.Type priceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(priceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this price. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param priceQueryRecord the price query record
     *  @param priceQueryInspectorRecord the price query inspector
     *         record
     *  @param priceSearchOrderRecord the price search order record
     *  @param priceRecordType price record type
     *  @throws org.osid.NullArgumentException
     *          <code>priceQueryRecord</code>,
     *          <code>priceQueryInspectorRecord</code>,
     *          <code>priceSearchOrderRecord</code> or
     *          <code>priceRecordTypeprice</code> is
     *          <code>null</code>
     */
            
    protected void addPriceRecords(org.osid.ordering.records.PriceQueryRecord priceQueryRecord, 
                                      org.osid.ordering.records.PriceQueryInspectorRecord priceQueryInspectorRecord, 
                                      org.osid.ordering.records.PriceSearchOrderRecord priceSearchOrderRecord, 
                                      org.osid.type.Type priceRecordType) {

        addRecordType(priceRecordType);

        nullarg(priceQueryRecord, "price query record");
        nullarg(priceQueryInspectorRecord, "price query inspector record");
        nullarg(priceSearchOrderRecord, "price search odrer record");

        this.queryRecords.add(priceQueryRecord);
        this.queryInspectorRecords.add(priceQueryInspectorRecord);
        this.searchOrderRecords.add(priceSearchOrderRecord);
        
        return;
    }
}
