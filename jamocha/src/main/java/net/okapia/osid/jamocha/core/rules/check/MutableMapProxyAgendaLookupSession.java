//
// MutableMapProxyAgendaLookupSession
//
//    Implements an Agenda lookup service backed by a collection of
//    agendas that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check;


/**
 *  Implements an Agenda lookup service backed by a collection of
 *  agendas. The agendas are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of agendas can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyAgendaLookupSession
    extends net.okapia.osid.jamocha.core.rules.check.spi.AbstractMapAgendaLookupSession
    implements org.osid.rules.check.AgendaLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyAgendaLookupSession}
     *  with no agendas.
     *
     *  @param engine the engine
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyAgendaLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.proxy.Proxy proxy) {
        setEngine(engine);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyAgendaLookupSession} with a
     *  single agenda.
     *
     *  @param engine the engine
     *  @param agenda an agenda
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code agenda}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAgendaLookupSession(org.osid.rules.Engine engine,
                                                org.osid.rules.check.Agenda agenda, org.osid.proxy.Proxy proxy) {
        this(engine, proxy);
        putAgenda(agenda);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAgendaLookupSession} using an
     *  array of agendas.
     *
     *  @param engine the engine
     *  @param agendas an array of agendas
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code agendas}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAgendaLookupSession(org.osid.rules.Engine engine,
                                                org.osid.rules.check.Agenda[] agendas, org.osid.proxy.Proxy proxy) {
        this(engine, proxy);
        putAgendas(agendas);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAgendaLookupSession} using a
     *  collection of agendas.
     *
     *  @param engine the engine
     *  @param agendas a collection of agendas
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code agendas}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAgendaLookupSession(org.osid.rules.Engine engine,
                                                java.util.Collection<? extends org.osid.rules.check.Agenda> agendas,
                                                org.osid.proxy.Proxy proxy) {
   
        this(engine, proxy);
        setSessionProxy(proxy);
        putAgendas(agendas);
        return;
    }

    
    /**
     *  Makes a {@code Agenda} available in this session.
     *
     *  @param agenda an agenda
     *  @throws org.osid.NullArgumentException {@code agenda{@code 
     *          is {@code null}
     */

    @Override
    public void putAgenda(org.osid.rules.check.Agenda agenda) {
        super.putAgenda(agenda);
        return;
    }


    /**
     *  Makes an array of agendas available in this session.
     *
     *  @param agendas an array of agendas
     *  @throws org.osid.NullArgumentException {@code agendas{@code 
     *          is {@code null}
     */

    @Override
    public void putAgendas(org.osid.rules.check.Agenda[] agendas) {
        super.putAgendas(agendas);
        return;
    }


    /**
     *  Makes collection of agendas available in this session.
     *
     *  @param agendas
     *  @throws org.osid.NullArgumentException {@code agenda{@code 
     *          is {@code null}
     */

    @Override
    public void putAgendas(java.util.Collection<? extends org.osid.rules.check.Agenda> agendas) {
        super.putAgendas(agendas);
        return;
    }


    /**
     *  Removes a Agenda from this session.
     *
     *  @param agendaId the {@code Id} of the agenda
     *  @throws org.osid.NullArgumentException {@code agendaId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAgenda(org.osid.id.Id agendaId) {
        super.removeAgenda(agendaId);
        return;
    }    
}
