//
// AbstractMapBuildingLookupSession
//
//    A simple framework for providing a Building lookup service
//    backed by a fixed collection of buildings.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Building lookup service backed by a
 *  fixed collection of buildings. The buildings are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Buildings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBuildingLookupSession
    extends net.okapia.osid.jamocha.room.spi.AbstractBuildingLookupSession
    implements org.osid.room.BuildingLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.room.Building> buildings = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.room.Building>());


    /**
     *  Makes a <code>Building</code> available in this session.
     *
     *  @param  building a building
     *  @throws org.osid.NullArgumentException <code>building<code>
     *          is <code>null</code>
     */

    protected void putBuilding(org.osid.room.Building building) {
        this.buildings.put(building.getId(), building);
        return;
    }


    /**
     *  Makes an array of buildings available in this session.
     *
     *  @param  buildings an array of buildings
     *  @throws org.osid.NullArgumentException <code>buildings<code>
     *          is <code>null</code>
     */

    protected void putBuildings(org.osid.room.Building[] buildings) {
        putBuildings(java.util.Arrays.asList(buildings));
        return;
    }


    /**
     *  Makes a collection of buildings available in this session.
     *
     *  @param  buildings a collection of buildings
     *  @throws org.osid.NullArgumentException <code>buildings<code>
     *          is <code>null</code>
     */

    protected void putBuildings(java.util.Collection<? extends org.osid.room.Building> buildings) {
        for (org.osid.room.Building building : buildings) {
            this.buildings.put(building.getId(), building);
        }

        return;
    }


    /**
     *  Removes a Building from this session.
     *
     *  @param  buildingId the <code>Id</code> of the building
     *  @throws org.osid.NullArgumentException <code>buildingId<code> is
     *          <code>null</code>
     */

    protected void removeBuilding(org.osid.id.Id buildingId) {
        this.buildings.remove(buildingId);
        return;
    }


    /**
     *  Gets the <code>Building</code> specified by its <code>Id</code>.
     *
     *  @param  buildingId <code>Id</code> of the <code>Building</code>
     *  @return the building
     *  @throws org.osid.NotFoundException <code>buildingId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>buildingId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding(org.osid.id.Id buildingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.Building building = this.buildings.get(buildingId);
        if (building == null) {
            throw new org.osid.NotFoundException("building not found: " + buildingId);
        }

        return (building);
    }


    /**
     *  Gets all <code>Buildings</code>. In plenary mode, the returned
     *  list contains all known buildings or an error
     *  results. Otherwise, the returned list may contain only those
     *  buildings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Buildings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.building.ArrayBuildingList(this.buildings.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.buildings.clear();
        super.close();
        return;
    }
}
