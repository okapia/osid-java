//
// AbstractAssemblyOffsetEventEnablerQuery.java
//
//     An OffsetEventEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.rules.offseteventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OffsetEventEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyOffsetEventEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.calendaring.rules.OffsetEventEnablerQuery,
               org.osid.calendaring.rules.OffsetEventEnablerQueryInspector,
               org.osid.calendaring.rules.OffsetEventEnablerSearchOrder {

    private final java.util.Collection<org.osid.calendaring.rules.records.OffsetEventEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.OffsetEventEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.OffsetEventEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyOffsetEventEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOffsetEventEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the offset event. 
     *
     *  @param  offsetEventId the offset event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> offsetEventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledOffsetEventId(org.osid.id.Id offsetEventId, 
                                        boolean match) {
        getAssembler().addIdTerm(getRuledOffsetEventIdColumn(), offsetEventId, match);
        return;
    }


    /**
     *  Clears the offset event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledOffsetEventIdTerms() {
        getAssembler().clearTerms(getRuledOffsetEventIdColumn());
        return;
    }


    /**
     *  Gets the offset event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledOffsetEventIdTerms() {
        return (getAssembler().getIdTerms(getRuledOffsetEventIdColumn()));
    }


    /**
     *  Gets the RuledOffsetEventId column name.
     *
     * @return the column name
     */

    protected String getRuledOffsetEventIdColumn() {
        return ("ruled_offset_event_id");
    }


    /**
     *  Tests if an <code> OffsetEventQuery </code> is available. 
     *
     *  @return <code> true </code> if an offset event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledOffsetEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offset event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the offset event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledOffsetEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuery getRuledOffsetEventQuery() {
        throw new org.osid.UnimplementedException("supportsRuledOffsetEventQuery() is false");
    }


    /**
     *  Matches enablers mapped to any offset event. 
     *
     *  @param  match <code> true </code> for enablers mapped to any offset 
     *          event, <code> false </code> to match enablers mapped to no 
     *          offset events 
     */

    @OSID @Override
    public void matchAnyRuledOffsetEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledOffsetEventColumn(), match);
        return;
    }


    /**
     *  Clears the offset event query terms. 
     */

    @OSID @Override
    public void clearRuledOffsetEventTerms() {
        getAssembler().clearTerms(getRuledOffsetEventColumn());
        return;
    }


    /**
     *  Gets the offset event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQueryInspector[] getRuledOffsetEventTerms() {
        return (new org.osid.calendaring.OffsetEventQueryInspector[0]);
    }


    /**
     *  Gets the RuledOffsetEvent column name.
     *
     * @return the column name
     */

    protected String getRuledOffsetEventColumn() {
        return ("ruled_offset_event");
    }


    /**
     *  Matches enablers mapped to the calendar. 
     *
     *  @param  calendarHouseId the calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarHouseId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarHouseId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar query terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this offsetEventEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  offsetEventEnablerRecordType an offset event enabler record type 
     *  @return <code>true</code> if the offsetEventEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type offsetEventEnablerRecordType) {
        for (org.osid.calendaring.rules.records.OffsetEventEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(offsetEventEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  offsetEventEnablerRecordType the offset event enabler record type 
     *  @return the offset event enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.OffsetEventEnablerQueryRecord getOffsetEventEnablerQueryRecord(org.osid.type.Type offsetEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.OffsetEventEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(offsetEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  offsetEventEnablerRecordType the offset event enabler record type 
     *  @return the offset event enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.OffsetEventEnablerQueryInspectorRecord getOffsetEventEnablerQueryInspectorRecord(org.osid.type.Type offsetEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.OffsetEventEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(offsetEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param offsetEventEnablerRecordType the offset event enabler record type
     *  @return the offset event enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.OffsetEventEnablerSearchOrderRecord getOffsetEventEnablerSearchOrderRecord(org.osid.type.Type offsetEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.OffsetEventEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(offsetEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this offset event enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param offsetEventEnablerQueryRecord the offset event enabler query record
     *  @param offsetEventEnablerQueryInspectorRecord the offset event enabler query inspector
     *         record
     *  @param offsetEventEnablerSearchOrderRecord the offset event enabler search order record
     *  @param offsetEventEnablerRecordType offset event enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerQueryRecord</code>,
     *          <code>offsetEventEnablerQueryInspectorRecord</code>,
     *          <code>offsetEventEnablerSearchOrderRecord</code> or
     *          <code>offsetEventEnablerRecordTypeoffsetEventEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addOffsetEventEnablerRecords(org.osid.calendaring.rules.records.OffsetEventEnablerQueryRecord offsetEventEnablerQueryRecord, 
                                      org.osid.calendaring.rules.records.OffsetEventEnablerQueryInspectorRecord offsetEventEnablerQueryInspectorRecord, 
                                      org.osid.calendaring.rules.records.OffsetEventEnablerSearchOrderRecord offsetEventEnablerSearchOrderRecord, 
                                      org.osid.type.Type offsetEventEnablerRecordType) {

        addRecordType(offsetEventEnablerRecordType);

        nullarg(offsetEventEnablerQueryRecord, "offset event enabler query record");
        nullarg(offsetEventEnablerQueryInspectorRecord, "offset event enabler query inspector record");
        nullarg(offsetEventEnablerSearchOrderRecord, "offset event enabler search odrer record");

        this.queryRecords.add(offsetEventEnablerQueryRecord);
        this.queryInspectorRecords.add(offsetEventEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(offsetEventEnablerSearchOrderRecord);
        
        return;
    }
}
