//
// AbstractNodeQualifierHierarchySession.java
//
//     Defines a Qualifier hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a qualifier hierarchy session for delivering a hierarchy
 *  of qualifiers using the QualifierNode interface.
 */

public abstract class AbstractNodeQualifierHierarchySession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractQualifierHierarchySession
    implements org.osid.authorization.QualifierHierarchySession {

    private java.util.Collection<org.osid.authorization.QualifierNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root qualifier <code> Ids </code> in this hierarchy.
     *
     *  @return the root qualifier <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootQualifierIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.authorization.qualifiernode.QualifierNodeToIdList(this.roots));
    }


    /**
     *  Gets the root qualifiers in the qualifier hierarchy. A node
     *  with no parents is an orphan. While all qualifier <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root qualifiers 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getRootQualifiers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.qualifiernode.QualifierNodeToQualifierList(new net.okapia.osid.jamocha.authorization.qualifiernode.ArrayQualifierNodeList(this.roots)));
    }


    /**
     *  Adds a root qualifier node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootQualifier(org.osid.authorization.QualifierNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root qualifier nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootQualifiers(java.util.Collection<org.osid.authorization.QualifierNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root qualifier node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootQualifier(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.authorization.QualifierNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Qualifier </code> has any parents. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @return <code> true </code> if the qualifier has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentQualifiers(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getQualifierNode(qualifierId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  qualifier.
     *
     *  @param  id an <code> Id </code> 
     *  @param  qualifierId the <code> Id </code> of a qualifier 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> qualifierId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> qualifierId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfQualifier(org.osid.id.Id id, org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.authorization.QualifierNodeList parents = getQualifierNode(qualifierId).getParentQualifierNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextQualifierNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given qualifier. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @return the parent <code> Ids </code> of the qualifier 
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentQualifierIds(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.qualifier.QualifierToIdList(getParentQualifiers(qualifierId)));
    }


    /**
     *  Gets the parents of the given qualifier. 
     *
     *  @param  qualifierId the <code> Id </code> to query 
     *  @return the parents of the qualifier 
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getParentQualifiers(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.qualifiernode.QualifierNodeToQualifierList(getQualifierNode(qualifierId).getParentQualifierNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  qualifier.
     *
     *  @param  id an <code> Id </code> 
     *  @param  qualifierId the Id of a qualifier 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> qualifierId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfQualifier(org.osid.id.Id id, org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfQualifier(id, qualifierId)) {
            return (true);
        }

        try (org.osid.authorization.QualifierList parents = getParentQualifiers(qualifierId)) {
            while (parents.hasNext()) {
                if (isAncestorOfQualifier(id, parents.getNextQualifier().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a qualifier has any children. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @return <code> true </code> if the <code> qualifierId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildQualifiers(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getQualifierNode(qualifierId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  qualifier.
     *
     *  @param  id an <code> Id </code> 
     *  @param qualifierId the <code> Id </code> of a 
     *         qualifier
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> qualifierId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> qualifierId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfQualifier(org.osid.id.Id id, org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfQualifier(qualifierId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  qualifier.
     *
     *  @param  qualifierId the <code> Id </code> to query 
     *  @return the children of the qualifier 
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildQualifierIds(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.qualifier.QualifierToIdList(getChildQualifiers(qualifierId)));
    }


    /**
     *  Gets the children of the given qualifier. 
     *
     *  @param  qualifierId the <code> Id </code> to query 
     *  @return the children of the qualifier 
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getChildQualifiers(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.qualifiernode.QualifierNodeToQualifierList(getQualifierNode(qualifierId).getChildQualifierNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  qualifier.
     *
     *  @param  id an <code> Id </code> 
     *  @param qualifierId the <code> Id </code> of a 
     *         qualifier
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> qualifierId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfQualifier(org.osid.id.Id id, org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfQualifier(qualifierId, id)) {
            return (true);
        }

        try (org.osid.authorization.QualifierList children = getChildQualifiers(qualifierId)) {
            while (children.hasNext()) {
                if (isDescendantOfQualifier(id, children.getNextQualifier().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  qualifier.
     *
     *  @param  qualifierId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified qualifier node 
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getQualifierNodeIds(org.osid.id.Id qualifierId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.authorization.qualifiernode.QualifierNodeToNode(getQualifierNode(qualifierId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given qualifier.
     *
     *  @param  qualifierId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified qualifier node 
     *  @throws org.osid.NotFoundException <code> qualifierId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> qualifierId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierNode getQualifierNodes(org.osid.id.Id qualifierId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getQualifierNode(qualifierId));
    }


    /**
     *  Closes this <code>QualifierHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a qualifier node.
     *
     *  @param qualifierId the id of the qualifier node
     *  @throws org.osid.NotFoundException <code>qualifierId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>qualifierId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.authorization.QualifierNode getQualifierNode(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(qualifierId, "qualifier Id");
        for (org.osid.authorization.QualifierNode qualifier : this.roots) {
            if (qualifier.getId().equals(qualifierId)) {
                return (qualifier);
            }

            org.osid.authorization.QualifierNode r = findQualifier(qualifier, qualifierId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(qualifierId + " is not found");
    }


    protected org.osid.authorization.QualifierNode findQualifier(org.osid.authorization.QualifierNode node, 
                                                            org.osid.id.Id qualifierId)
        throws org.osid.OperationFailedException {

        try (org.osid.authorization.QualifierNodeList children = node.getChildQualifierNodes()) {
            while (children.hasNext()) {
                org.osid.authorization.QualifierNode qualifier = children.getNextQualifierNode();
                if (qualifier.getId().equals(qualifierId)) {
                    return (qualifier);
                }
                
                qualifier = findQualifier(qualifier, qualifierId);
                if (qualifier != null) {
                    return (qualifier);
                }
            }
        }

        return (null);
    }
}
