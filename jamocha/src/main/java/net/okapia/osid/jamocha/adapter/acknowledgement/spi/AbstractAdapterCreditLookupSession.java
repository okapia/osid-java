//
// AbstractAdapterCreditLookupSession.java
//
//    A Credit lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Credit lookup session adapter.
 */

public abstract class AbstractAdapterCreditLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.acknowledgement.CreditLookupSession {

    private final org.osid.acknowledgement.CreditLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCreditLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCreditLookupSession(org.osid.acknowledgement.CreditLookupSession session) {
        super(session);
        this.session = session;
        return;
    }

    /**
     *  Gets the {@code Billing/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Billing Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBillingId() {
        return (this.session.getBillingId());
    }


    /**
     *  Gets the {@code Billing} associated with this session.
     *
     *  @return the {@code Billing} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Billing getBilling()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBilling());
    }


    /**
     *  Tests if this user can perform {@code Credit} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCredits() {
        return (this.session.canLookupCredits());
    }


    /**
     *  A complete view of the {@code Credit} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCreditView() {
        this.session.useComparativeCreditView();
        return;
    }


    /**
     *  A complete view of the {@code Credit} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCreditView() {
        this.session.usePlenaryCreditView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include credits in billings which are children
     *  of this billing in the billing hierarchy.
     */

    @OSID @Override
    public void useFederatedBillingView() {
        this.session.useFederatedBillingView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this billing only.
     */

    @OSID @Override
    public void useIsolatedBillingView() {
        this.session.useIsolatedBillingView();
        return;
    }
    

    /**
     *  Only credits whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCreditView() {
        this.session.useEffectiveCreditView();
        return;
    }
    

    /**
     *  All credits of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCreditView() {
        this.session.useAnyEffectiveCreditView();
        return;
    }

     
    /**
     *  Gets the {@code Credit} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Credit} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Credit} and
     *  retained for compatibility.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *  @param creditId {@code Id} of the {@code Credit}
     *  @return the credit
     *  @throws org.osid.NotFoundException {@code creditId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code creditId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Credit getCredit(org.osid.id.Id creditId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredit(creditId));
    }


    /**
     *  Gets a {@code CreditList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  credits specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Credits} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, credits are returned that are currently effective.
     *  In any effective mode, effective credits and those currently expired
     *  are returned.
     *
     *
     *  @param  creditIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Credit} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code creditIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByIds(org.osid.id.IdList creditIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsByIds(creditIds));
    }


    /**
     *  Gets a {@code CreditList} corresponding to the given
     *  credit genus {@code Type} which does not include
     *  credits of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *
     *  @param  creditGenusType a credit genus type 
     *  @return the returned {@code Credit} list
     *  @throws org.osid.NullArgumentException
     *          {@code creditGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsByGenusType(creditGenusType));
    }


    /**
     *  Gets a {@code CreditList} corresponding to the given
     *  credit genus {@code Type} and include any additional
     *  credits with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *
     *  @param  creditGenusType a credit genus type 
     *  @return the returned {@code Credit} list
     *  @throws org.osid.NullArgumentException
     *          {@code creditGenusType} is {@code }
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByParentGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsByParentGenusType(creditGenusType));
    }


    /**
     *  Gets a {@code CreditList} containing the given
     *  credit record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *
     *  @param  creditRecordType a credit record type 
     *  @return the returned {@code Credit} list
     *  @throws org.osid.NullArgumentException
     *          {@code creditRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByRecordType(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsByRecordType(creditRecordType));
    }


    /**
     *  Gets a {@code CreditList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *  
     *  In active mode, credits are returned that are currently
     *  active. In any status mode, active and inactive credits
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Credit} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsOnDate(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsOnDate(from, to));
    }
        

    /**
     *  Gets a list of credits of the given genus type and effective
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *  
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  creditGenusType a credit genus {@code Type} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CreditList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code creditGenusType, from, 
     *         } or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeOnDate(org.osid.type.Type creditGenusType, 
                                                                           org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCreditsByGenusTypeOnDate(creditGenusType, from, to));
    }


    /**
     *  Gets a list of credits corresponding to a resource
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code CreditList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsForResource(resourceId));
    }


    /**
     *  Gets a list of credits corresponding to a resource
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CreditList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResourceOnDate(org.osid.id.Id resourceId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of credits by a genus type for a resource.
     *  
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *  
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  creditGenusType a credit genus {@code Type} 
     *  @return the returned {@code CreditList} 
     *  @throws org.osid.NullArgumentException {@code creditGenusType} 
     *          or {@code resourceId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResource(org.osid.id.Id resourceId, 
                                                                                org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCreditsByGenusTypeForResource(resourceId, creditGenusType));
    }


    /**
     *  Gets a list of credits by genus type for a resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *  
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  creditGenusType a credit genus {@code Type} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CreditList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          creditGenusType, from}, or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                      org.osid.type.Type creditGenusType, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsByGenusTypeForResourceOnDate(resourceId, creditGenusType, from, to));
    }


    /**
     *  Gets a list of credits corresponding to a reference
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @return the returned {@code CreditList}
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsForReference(referenceId));
    }


    /**
     *  Gets a list of credits corresponding to a reference
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CreditList}
     *  @throws org.osid.NullArgumentException {@code referenceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForReferenceOnDate(org.osid.id.Id referenceId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsForReferenceOnDate(referenceId, from, to));
    }


    /**
     *  Gets a list of credits by a genus type for a reference. 
     *  
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *  
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  referenceId a reference {@code Id} 
     *  @param  creditGenusType a credit genus {@code Type} 
     *  @return the returned {@code CreditList} 
     *  @throws org.osid.NullArgumentException {@code referenceId} or 
     *          {@code creditGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForReference(org.osid.id.Id referenceId, 
                                                                                 org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsByGenusTypeForReference(referenceId, creditGenusType));
    }


    /**
     *  Gets a list of credits of the given genus type for a reference
     *  and effective entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *  
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  referenceId a reference {@code Id} 
     *  @param  creditGenusType a credit genus {@code Type} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CreditList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code referenceId,
     *          creditGenusType, from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForReferenceOnDate(org.osid.id.Id referenceId, 
                                                                                       org.osid.type.Type creditGenusType, 
                                                                                       org.osid.calendaring.DateTime from, 
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getCreditsByGenusTypeForReferenceOnDate(referenceId, creditGenusType, from, to));
    }


    /**
     *  Gets a list of credits corresponding to resource and reference
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  referenceId the {@code Id} of the reference
     *  @return the returned {@code CreditList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code referenceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResourceAndReference(org.osid.id.Id resourceId,
                                                                        org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsForResourceAndReference(resourceId, referenceId));
    }


    /**
     *  Gets a list of credits corresponding to resource and reference
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective. In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CreditList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code referenceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResourceAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                       org.osid.id.Id referenceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsForResourceAndReferenceOnDate(resourceId, referenceId, from, to));
    }


    /**
     *  Gets a {@code CreditList} of the given genus type for
     *  the given resource and reference.
     *  
     *  In plenary mode, the returned list contains all of the credits
     *  corresponding to the given resource and reference, including
     *  duplicates, or an error results if a credit is inaccessible.
     *  Otherwise, inaccessible {@code Credits} may be omitted
     *  from the list.
     *  
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  referenceId a reference {@code Id} 
     *  @param  creditGenusType a credit genus {@code Type} 
     *  @return the returned {@code CreditList} 
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code referenceId}, or {@code creditGenusType} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceAndReference(org.osid.id.Id resourceId, 
                                                                                            org.osid.id.Id referenceId, 
                                                                                            org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCreditsByGenusTypeForResourceAndReference(resourceId, referenceId, creditGenusType));
    }


    /**
     *  Gets a {@code CreditList} of the given genus type
     *  corresponding to the given resource and reference and
     *  effective entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all of the credits
     *  corresponding to the given peer, including duplicates, or an
     *  error results if a credit is inaccessible. Otherwise,
     *  inaccessible {@code Credits} may be omitted from the
     *  list.
     *  
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  referenceId a reference {@code Id} 
     *  @param  creditGenusType a credit genus {@code Type} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CreditList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *         referenceId}, {@code creditGenusType}, {@code from},
     *         or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceAndReferenceOnDate(org.osid.id.Id resourceId, 
                                                                                                  org.osid.id.Id referenceId, 
                                                                                                  org.osid.type.Type creditGenusType, 
                                                                                                  org.osid.calendaring.DateTime from, 
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCreditsByGenusTypeForResourceAndReferenceOnDate(resourceId, referenceId, creditGenusType, from, to));
    }


    /**
     *  Gets all {@code Credits}. 
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Credits} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCredits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredits());
    }
}
