//
// AbstractFilingAllocationManager.java
//
//     An adapter for a FilingAllocationManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.filing.allocation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a FilingAllocationManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterFilingAllocationManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.filing.allocation.FilingAllocationManager>
    implements org.osid.filing.allocation.FilingAllocationManager {


    /**
     *  Constructs a new {@code AbstractAdapterFilingAllocationManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterFilingAllocationManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterFilingAllocationManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterFilingAllocationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any dictionary federation is exposed. Federation is exposed 
     *  when a specific dictionary may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of dictionaries appears as a single dictionary. 
     *
     *  @return <code> true </code> if federation is visible <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if filing allocation is supported. 
     *
     *  @return <code> true </code> if a <code> AllocationSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAllocation() {
        return (getAdapteeManager().supportsAllocation());
    }


    /**
     *  Tests if filing allocation lookup is supported. 
     *
     *  @return <code> true </code> if a <code> AllocationLookupSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAllocationLookup() {
        return (getAdapteeManager().supportsAllocationLookup());
    }


    /**
     *  Tests if quota administration is supported. 
     *
     *  @return <code> true </code> if a <code> AllocationAdminSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAllocationAdmin() {
        return (getAdapteeManager().supportsAllocationAdmin());
    }


    /**
     *  Tests if an allocation <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if a <code> AllocationNotificationSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAllocationNotification() {
        return (getAdapteeManager().supportsAllocationNotification());
    }


    /**
     *  Gets the supported allocation record types. 
     *
     *  @return a list containing the supported <code> Allocation </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAllocationRecordTypes() {
        return (getAdapteeManager().getAllocationRecordTypes());
    }


    /**
     *  Tests if the given allocation record type is supported. 
     *
     *  @param  allocationRecordType a <code> Type </code> indicating an 
     *          allocation record type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> allocationRecordType 
     *          </code> is null 
     */

    @OSID @Override
    public boolean supportsAllocationRecordType(org.osid.type.Type allocationRecordType) {
        return (getAdapteeManager().supportsAllocationRecordType(allocationRecordType));
    }


    /**
     *  Gets the session for accessing usage and quotas. 
     *
     *  @return an <code> AllocationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAllocation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationSession getAllocationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAllocationSession());
    }


    /**
     *  Gets the session for accessing usage and quotas for a given directory. 
     *  If the path is an alias, the target directory is used. The path 
     *  indicates the file alias and the real path indicates the target 
     *  directory. 
     *
     *  @param  directoryPath the pathname to the directory 
     *  @return an <code> AllocationSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryPath </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAllocation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationSession getAllocationSessionForDirectory(String directoryPath)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAllocationSessionForDirectory(directoryPath));
    }


    /**
     *  Gets the session for accessing usage and quotas. 
     *
     *  @return an <code> AllocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationLookupSession getAllocationLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAllocationLookupSession());
    }


    /**
     *  Gets the session for accessing usage and quotas for a given directory. 
     *  If the path is an alias, the target directory is used. The path 
     *  indicates the file alias and the real path indicates the target 
     *  directory. 
     *
     *  @param  directoryPath the pathname to the directory 
     *  @return an <code> AllocationLookupSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryPath </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationLookupSession getAllocationLookupSessionForDirectory(String directoryPath)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAllocationLookupSessionForDirectory(directoryPath));
    }


    /**
     *  Gets the session for assigning quotas. 
     *
     *  @return an <code> AllocationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationAdminSession getAllocationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAllocationAdminSession());
    }


    /**
     *  Gets the session for assigning quotas for the given directory. If the 
     *  path is an alias, the target directory is used. The path indicates the 
     *  file alias and the real path indicates the target directory. 
     *
     *  @param  directoryPath the pathname to the directory 
     *  @return an <code> AllocationAdminSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryPath </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationAdminSession getAllocationAdminSessionForDirectory(String directoryPath)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAllocationAdminSessionForDirectory(directoryPath));
    }


    /**
     *  Gets the session for receiving messages about changes to directories. 
     *
     *  @param  allocationReceiver the notification callback 
     *  @return an <code> AllocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> allocationReceiver 
     *          </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationNotificationSession getAllocationNotificationSession(org.osid.filing.allocation.AllocationReceiver allocationReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAllocationNotificationSession(allocationReceiver));
    }


    /**
     *  Gets the session for receiving messages about usage warnings and quota 
     *  changes for the given directory. If the path is an alias, the target 
     *  directory is used. The path indicates the file alias and the real path 
     *  indicates the target directory. 
     *
     *  @param  allocationReceiver the notification callback 
     *  @param  directoryPath the pathname to the directory 
     *  @return an <code> AllocationNotificationSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> allocationReceiver 
     *          </code> or <code> directoryPath </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationNotificationSession getAllocationNotificationSessionForDirectory(org.osid.filing.allocation.AllocationReceiver allocationReceiver, 
                                                                                                                 String directoryPath)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAllocationNotificationSessionForDirectory(allocationReceiver, directoryPath));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
