//
// AbstractCourseLookupSession.java
//
//    A starter implementation framework for providing a Course
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Course
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCourses(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCourseLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.CourseLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Course</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCourses() {
        return (true);
    }


    /**
     *  A complete view of the <code>Course</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCourseView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Course</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCourseView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include course entries in course catalogs which are
     *  children of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active courses are returned by methods in this session.
     */

    @OSID @Override
    public void useActiveCourseView() {
       this.activeonly = true;         
       return;
    }


    /**
     *  All courses of any active or inactive status are returned by
     *  all methods in this session.
     */

    @OSID @Override
    public void useAnyStatusCourseView() {
        this.activeonly = false;
        return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */

    protected boolean isActiveOnly() {
        return (this.activeonly);
    }

     
    /**
     *  Gets the <code>Course</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Course</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Course</code> and
     *  retained for compatibility.
     *
     *  In active mode, courses are returned that are currently
     *  active.  In any status mode, active and inactive courses
     *  expired are returned.
     *
     *  @param  courseId <code>Id</code> of the
     *          <code>Course</code>
     *  @return the course
     *  @throws org.osid.NotFoundException <code>courseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Course getCourse(org.osid.id.Id courseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.CourseList courses = getCourses()) {
            while (courses.hasNext()) {
                org.osid.course.Course course = courses.getNextCourse();
                if (course.getId().equals(courseId)) {
                    return (course);
                }
            }
        } 

        throw new org.osid.NotFoundException(courseId + " not found");
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Courses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, courses are returned that are currently
     *  active.  In any status mode, active and inactive courses
     *  expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCourses()</code>.
     *
     *  @param  courseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>courseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByIds(org.osid.id.IdList courseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.Course> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = courseIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCourse(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("course " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.course.LinkedCourseList(ret));
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  course genus <code>Type</code> which does not include
     *  courses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active.  In any status mode, active and inactive courses
     *  expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCourses()</code>.
     *
     *  @param  courseGenusType a course genus type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByGenusType(org.osid.type.Type courseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.course.CourseGenusFilterList(getCourses(), courseGenusType));
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  course genus <code>Type</code> and include any additional
     *  courses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active.  In any status mode, active and inactive courses
     *  expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourses()</code>.
     *
     *  @param  courseGenusType a course genus type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByParentGenusType(org.osid.type.Type courseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCoursesByGenusType(courseGenusType));
    }


    /**
     *  Gets a <code>CourseList</code> containing the given
     *  course record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active.  In any status mode, active and inactive courses
     *  expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourses()</code>.
     *
     *  @param  courseRecordType a course record type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByRecordType(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.course.CourseRecordFilterList(getCourses(), courseRecordType));
    }


    /**
     *  Gets a <code> CourseList </code> by the given number. 
     *  
     *  In plenary mode, the returned list contains all known courses
     *  or an error results. Otherwise, the returned list may contain
     *  only those courses that are accessible through this session.
     *  
     *  In active mode, courses are returned that are currently
     *  active.  In any status mode, active and inactive courses
     *  expired are returned.
     *
     *  @param  number a course number 
     *  @return the returned <code> CourseList </code> list 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.course.CourseFilterList(new NumberFilter(number), getCourses()));
    }


    /**
     *  Gets all <code>Courses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active.  In any status mode, active and inactive courses
     *  expired are returned.
     *
     *  @return a list of <code>Courses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.CourseList getCourses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the course list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of courses
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.CourseList filterCoursesOnViews(org.osid.course.CourseList list)
        throws org.osid.OperationFailedException {

        org.osid.course.CourseList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.course.ActiveCourseFilterList(ret);
        }

        return (ret);
    }


    public static class NumberFilter
        implements net.okapia.osid.jamocha.inline.filter.course.course.CourseFilter {

        private final String number;

        
        /**
         *  Constructs a new <code>NumberFilter</code>.
         *
         *  @param number the number to filter
         *  @throws org.osid.NullArgumentException <code>number</code>
         *          is <code>null</code>
         */

        public NumberFilter(String number) {
            nullarg(number, "course number");
            this.number = number;
            return;
        }


        /**
         *  Used by the CourseFilterList to filter the course list
         *  based on number.
         *
         *  @param course the course 
         *  @return <code>true</code> to pass the course,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.Course course) {
            return (course.getNumber().equals(this.number));
        }
    }        
}
