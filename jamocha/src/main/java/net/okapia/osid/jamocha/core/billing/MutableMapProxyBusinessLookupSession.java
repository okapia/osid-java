//
// MutableMapProxyBusinessLookupSession
//
//    Implements a Business lookup service backed by a collection of
//    businesses that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements a Business lookup service backed by a collection of
 *  businesses. The businesses are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of businesses can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyBusinessLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractMapBusinessLookupSession
    implements org.osid.billing.BusinessLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBusinessLookupSession} with no
     *  businesses.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyBusinessLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBusinessLookupSession} with a
     *  single business.
     *
     *  @param business a business
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyBusinessLookupSession(org.osid.billing.Business business, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBusinessLookupSession} using an
     *  array of businesses.
     *
     *  @param businesses an array of businesses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code businesses} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyBusinessLookupSession(org.osid.billing.Business[] businesses, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBusinesses(businesses);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBusinessLookupSession} using
     *  a collection of businesses.
     *
     *  @param businesses a collection of businesses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code businesses} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyBusinessLookupSession(java.util.Collection<? extends org.osid.billing.Business> businesses,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBusinesses(businesses);
        return;
    }

    
    /**
     *  Makes a {@code Business} available in this session.
     *
     *  @param business an business
     *  @throws org.osid.NullArgumentException {@code business{@code 
     *          is {@code null}
     */

    @Override
    public void putBusiness(org.osid.billing.Business business) {
        super.putBusiness(business);
        return;
    }


    /**
     *  Makes an array of businesses available in this session.
     *
     *  @param businesses an array of businesses
     *  @throws org.osid.NullArgumentException {@code businesses{@code 
     *          is {@code null}
     */

    @Override
    public void putBusinesses(org.osid.billing.Business[] businesses) {
        super.putBusinesses(businesses);
        return;
    }


    /**
     *  Makes collection of businesses available in this session.
     *
     *  @param businesses
     *  @throws org.osid.NullArgumentException {@code business{@code 
     *          is {@code null}
     */

    @Override
    public void putBusinesses(java.util.Collection<? extends org.osid.billing.Business> businesses) {
        super.putBusinesses(businesses);
        return;
    }


    /**
     *  Removes a Business from this session.
     *
     *  @param businessId the {@code Id} of the business
     *  @throws org.osid.NullArgumentException {@code businessId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBusiness(org.osid.id.Id businessId) {
        super.removeBusiness(businessId);
        return;
    }    
}
