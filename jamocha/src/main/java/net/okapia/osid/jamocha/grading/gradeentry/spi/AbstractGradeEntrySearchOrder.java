//
// AbstractGradeEntrySearchOdrer.java
//
//     Defines a GradeEntrySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradeentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code GradeEntrySearchOrder}.
 */

public abstract class AbstractGradeEntrySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.grading.GradeEntrySearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradeEntrySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the gradebook column. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradebookColumn(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> GradebookColumnSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a gradebook column search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a gradebook column. 
     *
     *  @return the gradebook column search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSearchOrder getGradebookColumnSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the key resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByKeyResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a key resource search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a resource. 
     *
     *  @return the key resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyResourceSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getKeyResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsKeyResourceSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the derived entries. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDerived(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the ignore for 
     *  calculations flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByIgnoredForCalculations(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the grade or score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrade(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> GradeSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a grade. 
     *
     *  @return the grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getGradeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the time graded. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeGraded(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the grader. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrader(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available for grader 
     *  resources. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraderSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a grader. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getGraderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGraderSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the grading agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> AgentSearchOrder </code> is available fo grading 
     *  agents. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a grading agent. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getGradingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradingAgentSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  gradeEntryRecordType a grade entry record type 
     *  @return {@code true} if the gradeEntryRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code gradeEntryRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeEntryRecordType) {
        for (org.osid.grading.records.GradeEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  gradeEntryRecordType the grade entry record type 
     *  @return the grade entry search order record
     *  @throws org.osid.NullArgumentException
     *          {@code gradeEntryRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(gradeEntryRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntrySearchOrderRecord getGradeEntrySearchOrderRecord(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeEntryRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this grade entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeEntryRecord the grade entry search odrer record
     *  @param gradeEntryRecordType grade entry record type
     *  @throws org.osid.NullArgumentException
     *          {@code gradeEntryRecord} or
     *          {@code gradeEntryRecordTypegradeEntry} is
     *          {@code null}
     */
            
    protected void addGradeEntryRecord(org.osid.grading.records.GradeEntrySearchOrderRecord gradeEntrySearchOrderRecord, 
                                     org.osid.type.Type gradeEntryRecordType) {

        addRecordType(gradeEntryRecordType);
        this.records.add(gradeEntrySearchOrderRecord);
        
        return;
    }
}
