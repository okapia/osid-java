//
// AssessmentOfferedMiter.java
//
//     Defines an AssessmentOffered miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmentoffered;


/**
 *  Defines an <code>AssessmentOffered</code> miter for use with the builders.
 */

public interface AssessmentOfferedMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.assessment.AssessmentOffered {


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    public void setAssessment(org.osid.assessment.Assessment assessment);


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    public void setLevel(org.osid.grading.Grade level);


    /**
     *  Sets the sequential flag.
     *
     *  @param sequential <code> true </code> if the items are taken
     *         sequentially, <code> false </code> if the items can be
     *         skipped and revisited
     */

    public void setItemsSequential(boolean sequential);


    /**
     *  Sets the shuffled flag.
     *
     *  @param shuffled <code> true </code> if the items appear in a
     *         random order, <code> false </code> otherwise
     */

    public void setItemsShuffled(boolean shuffled);


    /**
     *  Sets the start time.
     *
     *  @param time a start time
     *  @throws org.osid.NullArgumentException <code>time</code>
     *          is <code>null</code>
     */

    public void setStartTime(org.osid.calendaring.DateTime time);


    /**
     *  Sets the deadline.
     *
     *  @param deadline a deadline
     *  @throws org.osid.NullArgumentException <code>deadline</code>
     *          is <code>null</code>
     */

    public void setDeadline(org.osid.calendaring.DateTime deadline);


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setDuration(org.osid.calendaring.Duration duration);



    /**
     *  Sets the score system.
     *
     *  @param gradeSystem the scoring system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    public void setScoreSystem(org.osid.grading.GradeSystem gradeSystem);    


    /**
     *  Sets the grade system.
     *
     *  @param gradeSystem the scoring system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    public void setGradeSystem(org.osid.grading.GradeSystem gradeSystem);


    /**
     *  Sets the rubric.
     *
     *  @param assessmentOffered the rubric assessment offered
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOffered</code> is <code>null</code>
     */

    public void setRubric(org.osid.assessment.AssessmentOffered assessmentOffered);


    /**
     *  Adds an AssessmentOffered record.
     *
     *  @param record an assessmentOffered record
     *  @param recordType the type of assessmentOffered record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAssessmentOfferedRecord(org.osid.assessment.records.AssessmentOfferedRecord record, org.osid.type.Type recordType);
}       


