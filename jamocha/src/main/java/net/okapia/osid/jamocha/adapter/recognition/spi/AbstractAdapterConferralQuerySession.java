//
// AbstractQueryConferralLookupSession.java
//
//    A ConferralQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ConferralQuerySession adapter.
 */

public abstract class AbstractAdapterConferralQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recognition.ConferralQuerySession {

    private final org.osid.recognition.ConferralQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterConferralQuerySession.
     *
     *  @param session the underlying conferral query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterConferralQuerySession(org.osid.recognition.ConferralQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeAcademy</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeAcademy Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.session.getAcademyId());
    }


    /**
     *  Gets the {@codeAcademy</code> associated with this 
     *  session.
     *
     *  @return the {@codeAcademy</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAcademy());
    }


    /**
     *  Tests if this user can perform {@codeConferral</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchConferrals() {
        return (this.session.canSearchConferrals());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include conferrals in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.session.useFederatedAcademyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this academy only.
     */
    
    @OSID @Override
    public void useIsolatedAcademyView() {
        this.session.useIsolatedAcademyView();
        return;
    }
    
      
    /**
     *  Gets a conferral query. The returned query will not have an
     *  extension query.
     *
     *  @return the conferral query 
     */
      
    @OSID @Override
    public org.osid.recognition.ConferralQuery getConferralQuery() {
        return (this.session.getConferralQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  conferralQuery the conferral query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code conferralQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code conferralQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByQuery(org.osid.recognition.ConferralQuery conferralQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getConferralsByQuery(conferralQuery));
    }
}
