//
// MutableMapTriggerEnablerLookupSession
//
//    Implements a TriggerEnabler lookup service backed by a collection of
//    triggerEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules;


/**
 *  Implements a TriggerEnabler lookup service backed by a collection of
 *  trigger enablers. The trigger enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of trigger enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapTriggerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.control.rules.spi.AbstractMapTriggerEnablerLookupSession
    implements org.osid.control.rules.TriggerEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapTriggerEnablerLookupSession}
     *  with no trigger enablers.
     *
     *  @param system the system
     *  @throws org.osid.NullArgumentException {@code system} is
     *          {@code null}
     */

      public MutableMapTriggerEnablerLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapTriggerEnablerLookupSession} with a
     *  single triggerEnabler.
     *
     *  @param system the system  
     *  @param triggerEnabler a trigger enabler
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code triggerEnabler} is {@code null}
     */

    public MutableMapTriggerEnablerLookupSession(org.osid.control.System system,
                                           org.osid.control.rules.TriggerEnabler triggerEnabler) {
        this(system);
        putTriggerEnabler(triggerEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapTriggerEnablerLookupSession}
     *  using an array of trigger enablers.
     *
     *  @param system the system
     *  @param triggerEnablers an array of trigger enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code triggerEnablers} is {@code null}
     */

    public MutableMapTriggerEnablerLookupSession(org.osid.control.System system,
                                           org.osid.control.rules.TriggerEnabler[] triggerEnablers) {
        this(system);
        putTriggerEnablers(triggerEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapTriggerEnablerLookupSession}
     *  using a collection of trigger enablers.
     *
     *  @param system the system
     *  @param triggerEnablers a collection of trigger enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code triggerEnablers} is {@code null}
     */

    public MutableMapTriggerEnablerLookupSession(org.osid.control.System system,
                                           java.util.Collection<? extends org.osid.control.rules.TriggerEnabler> triggerEnablers) {

        this(system);
        putTriggerEnablers(triggerEnablers);
        return;
    }

    
    /**
     *  Makes a {@code TriggerEnabler} available in this session.
     *
     *  @param triggerEnabler a trigger enabler
     *  @throws org.osid.NullArgumentException {@code triggerEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putTriggerEnabler(org.osid.control.rules.TriggerEnabler triggerEnabler) {
        super.putTriggerEnabler(triggerEnabler);
        return;
    }


    /**
     *  Makes an array of trigger enablers available in this session.
     *
     *  @param triggerEnablers an array of trigger enablers
     *  @throws org.osid.NullArgumentException {@code triggerEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putTriggerEnablers(org.osid.control.rules.TriggerEnabler[] triggerEnablers) {
        super.putTriggerEnablers(triggerEnablers);
        return;
    }


    /**
     *  Makes collection of trigger enablers available in this session.
     *
     *  @param triggerEnablers a collection of trigger enablers
     *  @throws org.osid.NullArgumentException {@code triggerEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putTriggerEnablers(java.util.Collection<? extends org.osid.control.rules.TriggerEnabler> triggerEnablers) {
        super.putTriggerEnablers(triggerEnablers);
        return;
    }


    /**
     *  Removes a TriggerEnabler from this session.
     *
     *  @param triggerEnablerId the {@code Id} of the trigger enabler
     *  @throws org.osid.NullArgumentException {@code triggerEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeTriggerEnabler(org.osid.id.Id triggerEnablerId) {
        super.removeTriggerEnabler(triggerEnablerId);
        return;
    }    
}
