//
// RequestTransactionMiter.java
//
//     Defines a RequestTransaction miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.requesttransaction;


/**
 *  Defines a <code>RequestTransaction</code> miter for use with the builders.
 */

public interface RequestTransactionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.provisioning.RequestTransaction {


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException <code>broker</code> is
     *          <code>null</code>
     */

    public void setBroker(org.osid.provisioning.Broker broker);


    /**
     *  Sets the submit date.
     *
     *  @param date a submit date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setSubmitDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the submitter.
     *
     *  @param submitter a submitter
     *  @throws org.osid.NullArgumentException <code>submitter</code>
     *          is <code>null</code>
     */

    public void setSubmitter(org.osid.resource.Resource submitter);


    /**
     *  Sets the submitting agent.
     *
     *  @param agent a submitting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setSubmittingAgent(org.osid.authentication.Agent agent);


    /**
     *  Adds a request.
     *
     *  @param request a request
     *  @throws org.osid.NullArgumentException <code>request</code> is
     *          <code>null</code>
     */

    public void addRequest(org.osid.provisioning.Request request);


    /**
     *  Sets all the requests.
     *
     *  @param requests a collection of requests
     *  @throws org.osid.NullArgumentException <code>requests</code>
     *          is <code>null</code>
     */

    public void setRequests(java.util.Collection<org.osid.provisioning.Request> requests);


    /**
     *  Adds a RequestTransaction record.
     *
     *  @param record a requestTransaction record
     *  @param recordType the type of requestTransaction record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRequestTransactionRecord(org.osid.provisioning.records.RequestTransactionRecord record, org.osid.type.Type recordType);
}       


