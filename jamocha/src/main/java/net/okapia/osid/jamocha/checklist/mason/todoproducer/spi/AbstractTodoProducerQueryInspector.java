//
// AbstractTodoProducerQueryInspector.java
//
//     A template for making a TodoProducerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.todoproducer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for todo producers.
 */

public abstract class AbstractTodoProducerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.checklist.mason.TodoProducerQueryInspector {

    private final java.util.Collection<org.osid.checklist.mason.records.TodoProducerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the creationquery terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCreationRuleTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the cyclic event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCyclicEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the cyclic event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQueryInspector[] getCyclicEventTerms() {
        return (new org.osid.calendaring.cycle.CyclicEventQueryInspector[0]);
    }


    /**
     *  Gets the stock <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStockIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the stock query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Gets the stock level query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getStockLevelTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the produced <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProducedTodoIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the produced query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getProducedTodoTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the checklist <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getChecklistIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the checklist query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQueryInspector[] getChecklistTerms() {
        return (new org.osid.checklist.ChecklistQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given todo producer query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a todo producer implementing the requested record.
     *
     *  @param todoProducerRecordType a todo producer record type
     *  @return the todo producer query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoProducerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerQueryInspectorRecord getTodoProducerQueryInspectorRecord(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.mason.records.TodoProducerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoProducerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this todo producer query. 
     *
     *  @param todoProducerQueryInspectorRecord todo producer query inspector
     *         record
     *  @param todoProducerRecordType todoProducer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTodoProducerQueryInspectorRecord(org.osid.checklist.mason.records.TodoProducerQueryInspectorRecord todoProducerQueryInspectorRecord, 
                                                   org.osid.type.Type todoProducerRecordType) {

        addRecordType(todoProducerRecordType);
        nullarg(todoProducerRecordType, "todo producer record type");
        this.records.add(todoProducerQueryInspectorRecord);        
        return;
    }
}
