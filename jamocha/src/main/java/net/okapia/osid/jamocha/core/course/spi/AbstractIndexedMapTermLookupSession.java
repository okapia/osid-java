//
// AbstractIndexedMapTermLookupSession.java
//
//    A simple framework for providing a Term lookup service
//    backed by a fixed collection of terms with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Term lookup service backed by a
 *  fixed collection of terms. The terms are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some terms may be compatible
 *  with more types than are indicated through these term
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Terms</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapTermLookupSession
    extends AbstractMapTermLookupSession
    implements org.osid.course.TermLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.Term> termsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.Term>());
    private final MultiMap<org.osid.type.Type, org.osid.course.Term> termsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.Term>());


    /**
     *  Makes a <code>Term</code> available in this session.
     *
     *  @param  term a term
     *  @throws org.osid.NullArgumentException <code>term<code> is
     *          <code>null</code>
     */

    @Override
    protected void putTerm(org.osid.course.Term term) {
        super.putTerm(term);

        this.termsByGenus.put(term.getGenusType(), term);
        
        try (org.osid.type.TypeList types = term.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.termsByRecord.put(types.getNextType(), term);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a term from this session.
     *
     *  @param termId the <code>Id</code> of the term
     *  @throws org.osid.NullArgumentException <code>termId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeTerm(org.osid.id.Id termId) {
        org.osid.course.Term term;
        try {
            term = getTerm(termId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.termsByGenus.remove(term.getGenusType());

        try (org.osid.type.TypeList types = term.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.termsByRecord.remove(types.getNextType(), term);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeTerm(termId);
        return;
    }


    /**
     *  Gets a <code>TermList</code> corresponding to the given
     *  term genus <code>Type</code> which does not include
     *  terms of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known terms or an error results. Otherwise,
     *  the returned list may contain only those terms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  termGenusType a term genus type 
     *  @return the returned <code>Term</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>termGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByGenusType(org.osid.type.Type termGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.term.ArrayTermList(this.termsByGenus.get(termGenusType)));
    }


    /**
     *  Gets a <code>TermList</code> containing the given
     *  term record <code>Type</code>. In plenary mode, the
     *  returned list contains all known terms or an error
     *  results. Otherwise, the returned list may contain only those
     *  terms that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  termRecordType a term record type 
     *  @return the returned <code>term</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByRecordType(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.term.ArrayTermList(this.termsByRecord.get(termRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.termsByGenus.clear();
        this.termsByRecord.clear();

        super.close();

        return;
    }
}
