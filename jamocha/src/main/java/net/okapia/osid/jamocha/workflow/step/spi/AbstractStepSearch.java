//
// AbstractStepSearch.java
//
//     A template for making a Step Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.step.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing step searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractStepSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.workflow.StepSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.workflow.records.StepSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.workflow.StepSearchOrder stepSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of steps. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  stepIds list of steps
     *  @throws org.osid.NullArgumentException
     *          <code>stepIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSteps(org.osid.id.IdList stepIds) {
        while (stepIds.hasNext()) {
            try {
                this.ids.add(stepIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSteps</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of step Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getStepIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  stepSearchOrder step search order 
     *  @throws org.osid.NullArgumentException
     *          <code>stepSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>stepSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderStepResults(org.osid.workflow.StepSearchOrder stepSearchOrder) {
	this.stepSearchOrder = stepSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.workflow.StepSearchOrder getStepSearchOrder() {
	return (this.stepSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given step search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a step implementing the requested record.
     *
     *  @param stepSearchRecordType a step search record
     *         type
     *  @return the step search record
     *  @throws org.osid.NullArgumentException
     *          <code>stepSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.StepSearchRecord getStepSearchRecord(org.osid.type.Type stepSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.workflow.records.StepSearchRecord record : this.records) {
            if (record.implementsRecordType(stepSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step search. 
     *
     *  @param stepSearchRecord step search record
     *  @param stepSearchRecordType step search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepSearchRecord(org.osid.workflow.records.StepSearchRecord stepSearchRecord, 
                                           org.osid.type.Type stepSearchRecordType) {

        addRecordType(stepSearchRecordType);
        this.records.add(stepSearchRecord);        
        return;
    }
}
