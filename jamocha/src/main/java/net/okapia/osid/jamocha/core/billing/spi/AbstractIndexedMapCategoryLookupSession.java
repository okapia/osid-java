//
// AbstractIndexedMapCategoryLookupSession.java
//
//    A simple framework for providing a Category lookup service
//    backed by a fixed collection of categories with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Category lookup service backed by a
 *  fixed collection of categories. The categories are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some categories may be compatible
 *  with more types than are indicated through these category
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Categories</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCategoryLookupSession
    extends AbstractMapCategoryLookupSession
    implements org.osid.billing.CategoryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.billing.Category> categoriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Category>());
    private final MultiMap<org.osid.type.Type, org.osid.billing.Category> categoriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Category>());


    /**
     *  Makes a <code>Category</code> available in this session.
     *
     *  @param  category a category
     *  @throws org.osid.NullArgumentException <code>category<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCategory(org.osid.billing.Category category) {
        super.putCategory(category);

        this.categoriesByGenus.put(category.getGenusType(), category);
        
        try (org.osid.type.TypeList types = category.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.categoriesByRecord.put(types.getNextType(), category);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a category from this session.
     *
     *  @param categoryId the <code>Id</code> of the category
     *  @throws org.osid.NullArgumentException <code>categoryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCategory(org.osid.id.Id categoryId) {
        org.osid.billing.Category category;
        try {
            category = getCategory(categoryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.categoriesByGenus.remove(category.getGenusType());

        try (org.osid.type.TypeList types = category.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.categoriesByRecord.remove(types.getNextType(), category);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCategory(categoryId);
        return;
    }


    /**
     *  Gets a <code>CategoryList</code> corresponding to the given
     *  category genus <code>Type</code> which does not include
     *  categories of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known categories or an error results. Otherwise,
     *  the returned list may contain only those categories that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  categoryGenusType a category genus type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByGenusType(org.osid.type.Type categoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.category.ArrayCategoryList(this.categoriesByGenus.get(categoryGenusType)));
    }


    /**
     *  Gets a <code>CategoryList</code> containing the given
     *  category record <code>Type</code>. In plenary mode, the
     *  returned list contains all known categories or an error
     *  results. Otherwise, the returned list may contain only those
     *  categories that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  categoryRecordType a category record type 
     *  @return the returned <code>category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByRecordType(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.category.ArrayCategoryList(this.categoriesByRecord.get(categoryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.categoriesByGenus.clear();
        this.categoriesByRecord.clear();

        super.close();

        return;
    }
}
