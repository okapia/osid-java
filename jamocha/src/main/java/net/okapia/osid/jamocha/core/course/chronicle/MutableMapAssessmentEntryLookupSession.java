//
// MutableMapAssessmentEntryLookupSession
//
//    Implements an AssessmentEntry lookup service backed by a collection of
//    assessmentEntries that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements an AssessmentEntry lookup service backed by a collection of
 *  assessment entries. The assessment entries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of assessment entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapAssessmentEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractMapAssessmentEntryLookupSession
    implements org.osid.course.chronicle.AssessmentEntryLookupSession {


    /**
     *  Constructs a new {@code MutableMapAssessmentEntryLookupSession}
     *  with no assessment entries.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog} is
     *          {@code null}
     */

      public MutableMapAssessmentEntryLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAssessmentEntryLookupSession} with a
     *  single assessmentEntry.
     *
     *  @param courseCatalog the course catalog  
     *  @param assessmentEntry an assessment entry
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code assessmentEntry} is {@code null}
     */

    public MutableMapAssessmentEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.chronicle.AssessmentEntry assessmentEntry) {
        this(courseCatalog);
        putAssessmentEntry(assessmentEntry);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAssessmentEntryLookupSession}
     *  using an array of assessment entries.
     *
     *  @param courseCatalog the course catalog
     *  @param assessmentEntries an array of assessment entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code assessmentEntries} is {@code null}
     */

    public MutableMapAssessmentEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.chronicle.AssessmentEntry[] assessmentEntries) {
        this(courseCatalog);
        putAssessmentEntries(assessmentEntries);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAssessmentEntryLookupSession}
     *  using a collection of assessment entries.
     *
     *  @param courseCatalog the course catalog
     *  @param assessmentEntries a collection of assessment entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code assessmentEntries} is {@code null}
     */

    public MutableMapAssessmentEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           java.util.Collection<? extends org.osid.course.chronicle.AssessmentEntry> assessmentEntries) {

        this(courseCatalog);
        putAssessmentEntries(assessmentEntries);
        return;
    }

    
    /**
     *  Makes an {@code AssessmentEntry} available in this session.
     *
     *  @param assessmentEntry an assessment entry
     *  @throws org.osid.NullArgumentException {@code assessmentEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putAssessmentEntry(org.osid.course.chronicle.AssessmentEntry assessmentEntry) {
        super.putAssessmentEntry(assessmentEntry);
        return;
    }


    /**
     *  Makes an array of assessment entries available in this session.
     *
     *  @param assessmentEntries an array of assessment entries
     *  @throws org.osid.NullArgumentException {@code assessmentEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessmentEntries(org.osid.course.chronicle.AssessmentEntry[] assessmentEntries) {
        super.putAssessmentEntries(assessmentEntries);
        return;
    }


    /**
     *  Makes collection of assessment entries available in this session.
     *
     *  @param assessmentEntries a collection of assessment entries
     *  @throws org.osid.NullArgumentException {@code assessmentEntries{@code  is
     *          {@code null}
     */

    @Override
    public void putAssessmentEntries(java.util.Collection<? extends org.osid.course.chronicle.AssessmentEntry> assessmentEntries) {
        super.putAssessmentEntries(assessmentEntries);
        return;
    }


    /**
     *  Removes an AssessmentEntry from this session.
     *
     *  @param assessmentEntryId the {@code Id} of the assessment entry
     *  @throws org.osid.NullArgumentException {@code assessmentEntryId{@code 
     *          is {@code null}
     */

    @Override
    public void removeAssessmentEntry(org.osid.id.Id assessmentEntryId) {
        super.removeAssessmentEntry(assessmentEntryId);
        return;
    }    
}
