//
// AbstractRelevancy.java
//
//     Defines a Relevancy.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.relevancy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Relevancy</code>.
 */

public abstract class AbstractRelevancy
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.ontology.Relevancy {

    private org.osid.ontology.Subject subject;
    private org.osid.id.Id mappedId;

    private final java.util.Collection<org.osid.ontology.records.RelevancyRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the subject <code> Id. </code> 
     *
     *  @return the subject <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubjectId() {
        return (this.subject.getId());
    }


    /**
     *  Gets the <code> Subject. </code> 
     *
     *  @return the subject 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ontology.Subject getSubject()
        throws org.osid.OperationFailedException {

        return (this.subject);
    }


    /**
     *  Sets the subject.
     *
     *  @param subject a subject
     *  @throws org.osid.NullArgumentException
     *          <code>subject</code> is <code>null</code>
     */

    protected void setSubject(org.osid.ontology.Subject subject) {
        nullarg(subject, "subject");
        this.subject = subject;
        return;
    }


    /**
     *  Gets the <code> Id </code> mapped to this <code> Subject. </code> 
     *
     *  @return a mapped <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMappedId() {
        return (this.mappedId);
    }


    /**
     *  Sets the mapped id.
     *
     *  @param mappedId a mapped id
     *  @throws org.osid.NullArgumentException
     *          <code>mappedId</code> is <code>null</code>
     */

    protected void setMappedId(org.osid.id.Id mappedId) {
        nullarg(mappedId, "mapped Id");
        this.mappedId = mappedId;
        return;
    }


    /**
     *  Tests if this relevancy supports the given record
     *  <code>Type</code>.
     *
     *  @param  relevancyRecordType a relevancy record type 
     *  @return <code>true</code> if the relevancyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type relevancyRecordType) {
        for (org.osid.ontology.records.RelevancyRecord record : this.records) {
            if (record.implementsRecordType(relevancyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Relevancy</code> record <code>Type</code>.
     *
     *  @param  relevancyRecordType the relevancy record type 
     *  @return the relevancy record 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.RelevancyRecord getRelevancyRecord(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.RelevancyRecord record : this.records) {
            if (record.implementsRecordType(relevancyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relevancy. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param relevancyRecord the relevancy record
     *  @param relevancyRecordType relevancy record type
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecord</code> or
     *          <code>relevancyRecordTyperelevancy</code> is
     *          <code>null</code>
     */
            
    protected void addRelevancyRecord(org.osid.ontology.records.RelevancyRecord relevancyRecord, 
                                      org.osid.type.Type relevancyRecordType) {

        nullarg(relevancyRecord, "relevancy record");
        addRecordType(relevancyRecordType);
        this.records.add(relevancyRecord);
        
        return;
    }
}
