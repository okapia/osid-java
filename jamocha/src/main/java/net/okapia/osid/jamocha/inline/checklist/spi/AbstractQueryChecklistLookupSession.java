//
// AbstractQueryChecklistLookupSession.java
//
//    An inline adapter that maps a ChecklistLookupSession to
//    a ChecklistQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ChecklistLookupSession to
 *  a ChecklistQuerySession.
 */

public abstract class AbstractQueryChecklistLookupSession
    extends net.okapia.osid.jamocha.checklist.spi.AbstractChecklistLookupSession
    implements org.osid.checklist.ChecklistLookupSession {

    private final org.osid.checklist.ChecklistQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryChecklistLookupSession.
     *
     *  @param querySession the underlying checklist query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryChecklistLookupSession(org.osid.checklist.ChecklistQuerySession querySession) {
        nullarg(querySession, "checklist query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Checklist</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupChecklists() {
        return (this.session.canSearchChecklists());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Checklist</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Checklist</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Checklist</code> and
     *  retained for compatibility.
     *
     *  @param  checklistId <code>Id</code> of the
     *          <code>Checklist</code>
     *  @return the checklist
     *  @throws org.osid.NotFoundException <code>checklistId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>checklistId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.ChecklistQuery query = getQuery();
        query.matchId(checklistId, true);
        org.osid.checklist.ChecklistList checklists = this.session.getChecklistsByQuery(query);
        if (checklists.hasNext()) {
            return (checklists.getNextChecklist());
        } 
        
        throw new org.osid.NotFoundException(checklistId + " not found");
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  checklists specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Checklists</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  checklistIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>checklistIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByIds(org.osid.id.IdList checklistIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.ChecklistQuery query = getQuery();

        try (org.osid.id.IdList ids = checklistIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getChecklistsByQuery(query));
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  checklist genus <code>Type</code> which does not include
     *  checklists of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  checklistGenusType a checklist genus type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByGenusType(org.osid.type.Type checklistGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.ChecklistQuery query = getQuery();
        query.matchGenusType(checklistGenusType, true);
        return (this.session.getChecklistsByQuery(query));
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  checklist genus <code>Type</code> and include any additional
     *  checklists with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  checklistGenusType a checklist genus type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByParentGenusType(org.osid.type.Type checklistGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.ChecklistQuery query = getQuery();
        query.matchParentGenusType(checklistGenusType, true);
        return (this.session.getChecklistsByQuery(query));
    }


    /**
     *  Gets a <code>ChecklistList</code> containing the given
     *  checklist record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  checklistRecordType a checklist record type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByRecordType(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.ChecklistQuery query = getQuery();
        query.matchRecordType(checklistRecordType, true);
        return (this.session.getChecklistsByQuery(query));
    }


    /**
     *  Gets a <code>ChecklistList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known checklists or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  checklists that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Checklist</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.ChecklistQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getChecklistsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Checklists</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Checklists</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklists()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.ChecklistQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getChecklistsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.checklist.ChecklistQuery getQuery() {
        org.osid.checklist.ChecklistQuery query = this.session.getChecklistQuery();
        return (query);
    }
}
