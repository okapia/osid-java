//
// AbstractCommitmentPeerList
//
//     Implements a filter for a CommitmentPeerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.batch.commitmentpeer.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a CommitmentPeerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedCommitmentPeerList
 *  to improve performance.
 */

public abstract class AbstractCommitmentPeerFilterList
    extends net.okapia.osid.jamocha.calendaring.batch.commitmentpeer.spi.AbstractCommitmentPeerList
    implements org.osid.calendaring.batch.CommitmentPeerList,
               net.okapia.osid.jamocha.inline.filter.calendaring.batch.commitmentpeer.CommitmentPeerFilter {

    private org.osid.calendaring.batch.CommitmentPeer commitmentPeer;
    private final org.osid.calendaring.batch.CommitmentPeerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractCommitmentPeerFilterList</code>.
     *
     *  @param commitmentPeerList a <code>CommitmentPeerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentPeerList</code> is <code>null</code>
     */

    protected AbstractCommitmentPeerFilterList(org.osid.calendaring.batch.CommitmentPeerList commitmentPeerList) {
        nullarg(commitmentPeerList, "commitment peer list");
        this.list = commitmentPeerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.commitmentPeer == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> CommitmentPeer </code> in this list. 
     *
     *  @return the next <code> CommitmentPeer </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> CommitmentPeer </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.batch.CommitmentPeer getNextCommitmentPeer()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.calendaring.batch.CommitmentPeer commitmentPeer = this.commitmentPeer;
            this.commitmentPeer = null;
            return (commitmentPeer);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in commitment peer list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.commitmentPeer = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters CommitmentPeers.
     *
     *  @param commitmentPeer the commitment peer to filter
     *  @return <code>true</code> if the commitment peer passes the filter,
     *          <code>false</code> if the commitment peer should be filtered
     */

    public abstract boolean pass(org.osid.calendaring.batch.CommitmentPeer commitmentPeer);


    protected void prime() {
        if (this.commitmentPeer != null) {
            return;
        }

        org.osid.calendaring.batch.CommitmentPeer commitmentPeer = null;

        while (this.list.hasNext()) {
            try {
                commitmentPeer = this.list.getNextCommitmentPeer();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(commitmentPeer)) {
                this.commitmentPeer = commitmentPeer;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
