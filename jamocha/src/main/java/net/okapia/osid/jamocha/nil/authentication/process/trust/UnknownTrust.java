//
// UnknownTrust.java
//
//     Defines an unknown Trust.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.authentication.process.trust;


/**
 *  Defines an unknown <code>Trust</code>.
 */

public final class UnknownTrust
    extends net.okapia.osid.jamocha.nil.authentication.process.trust.spi.AbstractUnknownTrust
    implements org.osid.authentication.process.Trust {


    /**
     *  Constructs a new <code>UnknownTrust</code>.
     */

    public UnknownTrust() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownTrust</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownTrust(boolean optional) {
        super(optional);
        addTrustRecord(new TrustRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Trust.
     *
     *  @return an unknown Trust
     */

    public static org.osid.authentication.process.Trust create() {
        return (net.okapia.osid.jamocha.builder.validator.authentication.process.trust.TrustValidator.validateTrust(new UnknownTrust()));
    }


    public class TrustRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.authentication.process.records.TrustRecord {

        
        protected TrustRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
