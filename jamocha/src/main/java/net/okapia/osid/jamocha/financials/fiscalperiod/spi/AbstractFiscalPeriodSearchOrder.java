//
// AbstractFiscalPeriodSearchOdrer.java
//
//     Defines a FiscalPeriodSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.fiscalperiod.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code FiscalPeriodSearchOrder}.
 */

public abstract class AbstractFiscalPeriodSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.financials.FiscalPeriodSearchOrder {

    private final java.util.Collection<org.osid.financials.records.FiscalPeriodSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by display label. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisplayLabel(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by year. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFiscalYear(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by fiscal period 
     *  start date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by fiscal period 
     *  end date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by duration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the budget 
     *  deadline. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBudgetDeadline(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the posting 
     *  deadline. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPostingDeadline(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the closing 
     *  date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosing(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  fiscalPeriodRecordType a fiscal period record type 
     *  @return {@code true} if the fiscalPeriodRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code fiscalPeriodRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type fiscalPeriodRecordType) {
        for (org.osid.financials.records.FiscalPeriodSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  fiscalPeriodRecordType the fiscal period record type 
     *  @return the fiscal period search order record
     *  @throws org.osid.NullArgumentException
     *          {@code fiscalPeriodRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(fiscalPeriodRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.financials.records.FiscalPeriodSearchOrderRecord getFiscalPeriodSearchOrderRecord(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.FiscalPeriodSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fiscalPeriodRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this fiscal period. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param fiscalPeriodRecord the fiscal period search odrer record
     *  @param fiscalPeriodRecordType fiscal period record type
     *  @throws org.osid.NullArgumentException
     *          {@code fiscalPeriodRecord} or
     *          {@code fiscalPeriodRecordTypefiscalPeriod} is
     *          {@code null}
     */
            
    protected void addFiscalPeriodRecord(org.osid.financials.records.FiscalPeriodSearchOrderRecord fiscalPeriodSearchOrderRecord, 
                                     org.osid.type.Type fiscalPeriodRecordType) {

        addRecordType(fiscalPeriodRecordType);
        this.records.add(fiscalPeriodSearchOrderRecord);
        
        return;
    }
}
