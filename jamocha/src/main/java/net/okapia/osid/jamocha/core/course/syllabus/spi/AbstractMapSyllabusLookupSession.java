//
// AbstractMapSyllabusLookupSession
//
//    A simple framework for providing a Syllabus lookup service
//    backed by a fixed collection of syllabi.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Syllabus lookup service backed by a
 *  fixed collection of syllabi. The syllabi are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Syllabi</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSyllabusLookupSession
    extends net.okapia.osid.jamocha.course.syllabus.spi.AbstractSyllabusLookupSession
    implements org.osid.course.syllabus.SyllabusLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.syllabus.Syllabus> syllabi = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.syllabus.Syllabus>());


    /**
     *  Makes a <code>Syllabus</code> available in this session.
     *
     *  @param  syllabus a syllabus
     *  @throws org.osid.NullArgumentException <code>syllabus<code>
     *          is <code>null</code>
     */

    protected void putSyllabus(org.osid.course.syllabus.Syllabus syllabus) {
        this.syllabi.put(syllabus.getId(), syllabus);
        return;
    }


    /**
     *  Makes an array of syllabi available in this session.
     *
     *  @param  syllabi an array of syllabi
     *  @throws org.osid.NullArgumentException <code>syllabi<code>
     *          is <code>null</code>
     */

    protected void putSyllabi(org.osid.course.syllabus.Syllabus[] syllabi) {
        putSyllabi(java.util.Arrays.asList(syllabi));
        return;
    }


    /**
     *  Makes a collection of syllabi available in this session.
     *
     *  @param  syllabi a collection of syllabi
     *  @throws org.osid.NullArgumentException <code>syllabi<code>
     *          is <code>null</code>
     */

    protected void putSyllabi(java.util.Collection<? extends org.osid.course.syllabus.Syllabus> syllabi) {
        for (org.osid.course.syllabus.Syllabus syllabus : syllabi) {
            this.syllabi.put(syllabus.getId(), syllabus);
        }

        return;
    }


    /**
     *  Removes a Syllabus from this session.
     *
     *  @param  syllabusId the <code>Id</code> of the syllabus
     *  @throws org.osid.NullArgumentException <code>syllabusId<code> is
     *          <code>null</code>
     */

    protected void removeSyllabus(org.osid.id.Id syllabusId) {
        this.syllabi.remove(syllabusId);
        return;
    }


    /**
     *  Gets the <code>Syllabus</code> specified by its <code>Id</code>.
     *
     *  @param  syllabusId <code>Id</code> of the <code>Syllabus</code>
     *  @return the syllabus
     *  @throws org.osid.NotFoundException <code>syllabusId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>syllabusId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Syllabus getSyllabus(org.osid.id.Id syllabusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.syllabus.Syllabus syllabus = this.syllabi.get(syllabusId);
        if (syllabus == null) {
            throw new org.osid.NotFoundException("syllabus not found: " + syllabusId);
        }

        return (syllabus);
    }


    /**
     *  Gets all <code>Syllabi</code>. In plenary mode, the returned
     *  list contains all known syllabi or an error
     *  results. Otherwise, the returned list may contain only those
     *  syllabi that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Syllabi</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabi()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.syllabus.syllabus.ArraySyllabusList(this.syllabi.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.syllabi.clear();
        super.close();
        return;
    }
}
