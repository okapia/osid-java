//
// AbstractAdapterCampusLookupSession.java
//
//    A Campus lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Campus lookup session adapter.
 */

public abstract class AbstractAdapterCampusLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.room.CampusLookupSession {

    private final org.osid.room.CampusLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCampusLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCampusLookupSession(org.osid.room.CampusLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Campus} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCampuses() {
        return (this.session.canLookupCampuses());
    }


    /**
     *  A complete view of the {@code Campus} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCampusView() {
        this.session.useComparativeCampusView();
        return;
    }


    /**
     *  A complete view of the {@code Campus} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCampusView() {
        this.session.usePlenaryCampusView();
        return;
    }

     
    /**
     *  Gets the {@code Campus} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Campus} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Campus} and
     *  retained for compatibility.
     *
     *  @param campusId {@code Id} of the {@code Campus}
     *  @return the campus
     *  @throws org.osid.NotFoundException {@code campusId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code campusId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCampus(campusId));
    }


    /**
     *  Gets a {@code CampusList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  campuses specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Campuses} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  campusIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Campus} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code campusIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByIds(org.osid.id.IdList campusIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCampusesByIds(campusIds));
    }


    /**
     *  Gets a {@code CampusList} corresponding to the given
     *  campus genus {@code Type} which does not include
     *  campuses of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  campusGenusType a campus genus type 
     *  @return the returned {@code Campus} list
     *  @throws org.osid.NullArgumentException
     *          {@code campusGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByGenusType(org.osid.type.Type campusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCampusesByGenusType(campusGenusType));
    }


    /**
     *  Gets a {@code CampusList} corresponding to the given
     *  campus genus {@code Type} and include any additional
     *  campuses with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  campusGenusType a campus genus type 
     *  @return the returned {@code Campus} list
     *  @throws org.osid.NullArgumentException
     *          {@code campusGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByParentGenusType(org.osid.type.Type campusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCampusesByParentGenusType(campusGenusType));
    }


    /**
     *  Gets a {@code CampusList} containing the given
     *  campus record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  campusRecordType a campus record type 
     *  @return the returned {@code Campus} list
     *  @throws org.osid.NullArgumentException
     *          {@code campusRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByRecordType(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCampusesByRecordType(campusRecordType));
    }


    /**
     *  Gets a {@code CampusList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Campus} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCampusesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Campuses}. 
     *
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Campuses} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampuses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCampuses());
    }
}
