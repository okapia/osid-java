//
// AbstractAuctionHouseQuery.java
//
//     A template for making an AuctionHouse Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auctionhouse.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for auction houses.
 */

public abstract class AbstractAuctionHouseQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.bidding.AuctionHouseQuery {

    private final java.util.Collection<org.osid.bidding.records.AuctionHouseQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the auction <code> Id </code> for this query. 
     *
     *  @param  auctionId the auction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionId(org.osid.id.Id auctionId, boolean match) {
        return;
    }


    /**
     *  Clears the auction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuctionQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the auction query 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuery getAuctionQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionQuery() is false");
    }


    /**
     *  Matches auction houses with any auction. 
     *
     *  @param  match <code> true </code> to match auction houses with any 
     *          auction, <code> false </code> to match auction houses with no 
     *          auction 
     */

    @OSID @Override
    public void matchAnyAuction(boolean match) {
        return;
    }


    /**
     *  Clears the auction query terms. 
     */

    @OSID @Override
    public void clearAuctionTerms() {
        return;
    }


    /**
     *  Sets the bid <code> Id </code> for this query. 
     *
     *  @param  bidId the bid <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bidId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBidId(org.osid.id.Id bidId, boolean match) {
        return;
    }


    /**
     *  Clears the bid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBidIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BidQuery </code> is available. 
     *
     *  @return <code> true </code> if a bid query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bid. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bid query 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuery getBidQuery() {
        throw new org.osid.UnimplementedException("supportsBidQuery() is false");
    }


    /**
     *  Matches auction houses that have any bid. 
     *
     *  @param  match <code> true </code> to match auction houses with any 
     *          bid, <code> false </code> to match auction houses with no bid 
     */

    @OSID @Override
    public void matchAnyBid(boolean match) {
        return;
    }


    /**
     *  Clears the bid query terms. 
     */

    @OSID @Override
    public void clearBidTerms() {
        return;
    }


    /**
     *  Sets the auction house <code> Id </code> for this query to match 
     *  auction houses that have the specified auction house as an ancestor. 
     *
     *  @param  auctionHouseId an auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the ancestor auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorAuctionHouseIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for an <code> AuctionHouse. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAuctionHouseQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAncestorAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAuctionHouseQuery() is false");
    }


    /**
     *  Matches auction houses with any ancestor. 
     *
     *  @param  match <code> true </code> to match auction houses with any 
     *          ancestor, <code> false </code> to match root auction houses 
     */

    @OSID @Override
    public void matchAnyAncestorAuctionHouse(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor auction house query terms. 
     */

    @OSID @Override
    public void clearAncestorAuctionHouseTerms() {
        return;
    }


    /**
     *  Sets the auction house <code> Id </code> for this query to match 
     *  auction houses that have the specified auction house as a descendant. 
     *
     *  @param  auctionHouseId an auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the descendant auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantAuctionHouseIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAuctionHouseQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getDescendantAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAuctionHouseQuery() is false");
    }


    /**
     *  Matches auction houses with any descendant. 
     *
     *  @param  match <code> true </code> to match auction houses with any 
     *          descendant, <code> false </code> to match leaf auction houses 
     */

    @OSID @Override
    public void matchAnyDescendantAuctionHouse(boolean match) {
        return;
    }


    /**
     *  Clears the descendant auction house query terms. 
     */

    @OSID @Override
    public void clearDescendantAuctionHouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given auction house query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction house implementing the requested record.
     *
     *  @param auctionHouseRecordType an auction house record type
     *  @return the auction house query record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionHouseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionHouseQueryRecord getAuctionHouseQueryRecord(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionHouseQueryRecord record : this.records) {
            if (record.implementsRecordType(auctionHouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionHouseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction house query. 
     *
     *  @param auctionHouseQueryRecord auction house query record
     *  @param auctionHouseRecordType auctionHouse record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionHouseQueryRecord(org.osid.bidding.records.AuctionHouseQueryRecord auctionHouseQueryRecord, 
                                          org.osid.type.Type auctionHouseRecordType) {

        addRecordType(auctionHouseRecordType);
        nullarg(auctionHouseQueryRecord, "auction house query record");
        this.records.add(auctionHouseQueryRecord);        
        return;
    }
}
