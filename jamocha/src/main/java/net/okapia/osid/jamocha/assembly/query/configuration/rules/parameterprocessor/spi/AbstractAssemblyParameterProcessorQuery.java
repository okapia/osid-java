//
// AbstractAssemblyParameterProcessorQuery.java
//
//     A ParameterProcessorQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.configuration.rules.parameterprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ParameterProcessorQuery that stores terms.
 */

public abstract class AbstractAssemblyParameterProcessorQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidProcessorQuery
    implements org.osid.configuration.rules.ParameterProcessorQuery,
               org.osid.configuration.rules.ParameterProcessorQueryInspector,
               org.osid.configuration.rules.ParameterProcessorSearchOrder {

    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyParameterProcessorQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyParameterProcessorQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to the parameter. 
     *
     *  @param  parameterId the parameter <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledParameterId(org.osid.id.Id parameterId, 
                                      boolean match) {
        getAssembler().addIdTerm(getRuledParameterIdColumn(), parameterId, match);
        return;
    }


    /**
     *  Clears the parameter <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledParameterIdTerms() {
        getAssembler().clearTerms(getRuledParameterIdColumn());
        return;
    }


    /**
     *  Gets the parameter <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledParameterIdTerms() {
        return (getAssembler().getIdTerms(getRuledParameterIdColumn()));
    }


    /**
     *  Gets the RuledParameterId column name.
     *
     * @return the column name
     */

    protected String getRuledParameterIdColumn() {
        return ("ruled_parameter_id");
    }


    /**
     *  Tests if a <code> ParameterQuery </code> is available. 
     *
     *  @return <code> true </code> if a parameter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledParameterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a parameter. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the parameter query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledParameterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuery getRuledParameterQuery() {
        throw new org.osid.UnimplementedException("supportsRuledParameterQuery() is false");
    }


    /**
     *  Matches mapped to any parameter. 
     *
     *  @param  match <code> true </code> for mapped to any parameter, <code> 
     *          false </code> to match mapped to no parameters 
     */

    @OSID @Override
    public void matchAnyRuledParameter(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledParameterColumn(), match);
        return;
    }


    /**
     *  Clears the parameter query terms. 
     */

    @OSID @Override
    public void clearRuledParameterTerms() {
        getAssembler().clearTerms(getRuledParameterColumn());
        return;
    }


    /**
     *  Gets the parameter query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQueryInspector[] getRuledParameterTerms() {
        return (new org.osid.configuration.ParameterQueryInspector[0]);
    }


    /**
     *  Gets the RuledParameter column name.
     *
     * @return the column name
     */

    protected String getRuledParameterColumn() {
        return ("ruled_parameter");
    }


    /**
     *  Matches mapped to the configuration. 
     *
     *  @param  configurationId the configuration <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        getAssembler().addIdTerm(getConfigurationIdColumn(), configurationId, match);
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        getAssembler().clearTerms(getConfigurationIdColumn());
        return;
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (getAssembler().getIdTerms(getConfigurationIdColumn()));
    }


    /**
     *  Gets the ConfigurationId column name.
     *
     * @return the column name
     */

    protected String getConfigurationIdColumn() {
        return ("configuration_id");
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration query terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        getAssembler().clearTerms(getConfigurationColumn());
        return;
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the Configuration column name.
     *
     * @return the column name
     */

    protected String getConfigurationColumn() {
        return ("configuration");
    }


    /**
     *  Tests if this parameterProcessor supports the given record
     *  <code>Type</code>.
     *
     *  @param  parameterProcessorRecordType a parameter processor record type 
     *  @return <code>true</code> if the parameterProcessorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type parameterProcessorRecordType) {
        for (org.osid.configuration.rules.records.ParameterProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(parameterProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  parameterProcessorRecordType the parameter processor record type 
     *  @return the parameter processor query record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorQueryRecord getParameterProcessorQueryRecord(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(parameterProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  parameterProcessorRecordType the parameter processor record type 
     *  @return the parameter processor query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorQueryInspectorRecord getParameterProcessorQueryInspectorRecord(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(parameterProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param parameterProcessorRecordType the parameter processor record type
     *  @return the parameter processor search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorSearchOrderRecord getParameterProcessorSearchOrderRecord(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(parameterProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this parameter processor. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param parameterProcessorQueryRecord the parameter processor query record
     *  @param parameterProcessorQueryInspectorRecord the parameter processor query inspector
     *         record
     *  @param parameterProcessorSearchOrderRecord the parameter processor search order record
     *  @param parameterProcessorRecordType parameter processor record type
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorQueryRecord</code>,
     *          <code>parameterProcessorQueryInspectorRecord</code>,
     *          <code>parameterProcessorSearchOrderRecord</code> or
     *          <code>parameterProcessorRecordTypeparameterProcessor</code> is
     *          <code>null</code>
     */
            
    protected void addParameterProcessorRecords(org.osid.configuration.rules.records.ParameterProcessorQueryRecord parameterProcessorQueryRecord, 
                                      org.osid.configuration.rules.records.ParameterProcessorQueryInspectorRecord parameterProcessorQueryInspectorRecord, 
                                      org.osid.configuration.rules.records.ParameterProcessorSearchOrderRecord parameterProcessorSearchOrderRecord, 
                                      org.osid.type.Type parameterProcessorRecordType) {

        addRecordType(parameterProcessorRecordType);

        nullarg(parameterProcessorQueryRecord, "parameter processor query record");
        nullarg(parameterProcessorQueryInspectorRecord, "parameter processor query inspector record");
        nullarg(parameterProcessorSearchOrderRecord, "parameter processor search odrer record");

        this.queryRecords.add(parameterProcessorQueryRecord);
        this.queryInspectorRecords.add(parameterProcessorQueryInspectorRecord);
        this.searchOrderRecords.add(parameterProcessorSearchOrderRecord);
        
        return;
    }
}
