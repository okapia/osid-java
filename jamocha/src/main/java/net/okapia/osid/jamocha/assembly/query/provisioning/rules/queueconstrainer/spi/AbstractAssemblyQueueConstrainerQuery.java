//
// AbstractAssemblyQueueConstrainerQuery.java
//
//     A QueueConstrainerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.rules.queueconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A QueueConstrainerQuery that stores terms.
 */

public abstract class AbstractAssemblyQueueConstrainerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidConstrainerQuery
    implements org.osid.provisioning.rules.QueueConstrainerQuery,
               org.osid.provisioning.rules.QueueConstrainerQueryInspector,
               org.osid.provisioning.rules.QueueConstrainerSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.QueueConstrainerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.QueueConstrainerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.QueueConstrainerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyQueueConstrainerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyQueueConstrainerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches queues of the given size limit inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchSizeLimit(long start, long end, boolean match) {
        getAssembler().addCardinalRangeTerm(getSizeLimitColumn(), start, end, match);
        return;
    }


    /**
     *  Matches queues with any known size limit. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySizeLimit(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getSizeLimitColumn(), match);
        return;
    }


    /**
     *  Clears the size limit query terms. 
     */

    @OSID @Override
    public void clearSizeLimitTerms() {
        getAssembler().clearTerms(getSizeLimitColumn());
        return;
    }


    /**
     *  Gets the size limit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getSizeLimitTerms() {
        return (getAssembler().getCardinalRangeTerms(getSizeLimitColumn()));
    }


    /**
     *  Orders the results by queue size limit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySizeLimit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSizeLimitColumn(), style);
        return;
    }


    /**
     *  Gets the SizeLimit column name.
     *
     * @return the column name
     */

    protected String getSizeLimitColumn() {
        return ("size_limit");
    }


    /**
     *  Matches constraints that require provisions. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRequiresProvisions(boolean match) {
        getAssembler().addBooleanTerm(getRequiresProvisionsColumn(), match);
        return;
    }


    /**
     *  Clears the requires provisions query terms. 
     */

    @OSID @Override
    public void clearRequiresProvisionsTerms() {
        getAssembler().clearTerms(getRequiresProvisionsColumn());
        return;
    }


    /**
     *  Gets the requires provisions query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRequiresProvisionsTerms() {
        return (getAssembler().getBooleanTerms(getRequiresProvisionsColumn()));
    }


    /**
     *  Orders the results by the requires provisions flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequiresProvisions(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequiresProvisionsColumn(), style);
        return;
    }


    /**
     *  Gets the RequiresProvisions column name.
     *
     * @return the column name
     */

    protected String getRequiresProvisionsColumn() {
        return ("requires_provisions");
    }


    /**
     *  Sets the required provision pool <code> Id </code> for this query. 
     *
     *  @param  poolId a pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRequiredProvisionPoolId(org.osid.id.Id poolId, 
                                             boolean match) {
        getAssembler().addIdTerm(getRequiredProvisionPoolIdColumn(), poolId, match);
        return;
    }


    /**
     *  Clears the required provision pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequiredProvisionPoolIdTerms() {
        getAssembler().clearTerms(getRequiredProvisionPoolIdColumn());
        return;
    }


    /**
     *  Gets the required provision pool <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequiredProvisionPoolIdTerms() {
        return (getAssembler().getIdTerms(getRequiredProvisionPoolIdColumn()));
    }


    /**
     *  Gets the RequiredProvisionPoolId column name.
     *
     * @return the column name
     */

    protected String getRequiredProvisionPoolIdColumn() {
        return ("required_provision_pool_id");
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a required provision pool query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequiredProvisionPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Pool. </code> Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the required provision pool query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequiredProvisionPoolQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getRequiredProvisionPoolQuery() {
        throw new org.osid.UnimplementedException("supportsRequiredProvisionPoolQuery() is false");
    }


    /**
     *  Matches queues that have any required provision pool. 
     *
     *  @param  match <code> true </code> to match queues with any required 
     *          provision pool, <code> false </code> to match queues with no 
     *          required provision pool 
     */

    @OSID @Override
    public void matchAnyRequiredProvisionPool(boolean match) {
        getAssembler().addIdWildcardTerm(getRequiredProvisionPoolColumn(), match);
        return;
    }


    /**
     *  Clears the required provision pool query terms. 
     */

    @OSID @Override
    public void clearRequiredProvisionPoolTerms() {
        getAssembler().clearTerms(getRequiredProvisionPoolColumn());
        return;
    }


    /**
     *  Gets the required provision pool query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQueryInspector[] getRequiredProvisionPoolTerms() {
        return (new org.osid.provisioning.PoolQueryInspector[0]);
    }


    /**
     *  Gets the RequiredProvisionPool column name.
     *
     * @return the column name
     */

    protected String getRequiredProvisionPoolColumn() {
        return ("required_provision_pool");
    }


    /**
     *  Matches mapped to a queue. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledQueueId(org.osid.id.Id queueId, boolean match) {
        getAssembler().addIdTerm(getRuledQueueIdColumn(), queueId, match);
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledQueueIdTerms() {
        getAssembler().clearTerms(getRuledQueueIdColumn());
        return;
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledQueueIdTerms() {
        return (getAssembler().getIdTerms(getRuledQueueIdColumn()));
    }


    /**
     *  Gets the RuledQueueId column name.
     *
     * @return the column name
     */

    protected String getRuledQueueIdColumn() {
        return ("ruled_queue_id");
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledQueueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuery getRuledQueueQuery() {
        throw new org.osid.UnimplementedException("supportsRuledQueueQuery() is false");
    }


    /**
     *  Matches mapped to any queue. 
     *
     *  @param  match <code> true </code> for mapped to any queue, <code> 
     *          false </code> to match mapped to no queues 
     */

    @OSID @Override
    public void matchAnyRuledQueue(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledQueueColumn(), match);
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearRuledQueueTerms() {
        getAssembler().clearTerms(getRuledQueueColumn());
        return;
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQueryInspector[] getRuledQueueTerms() {
        return (new org.osid.provisioning.QueueQueryInspector[0]);
    }


    /**
     *  Gets the RuledQueue column name.
     *
     * @return the column name
     */

    protected String getRuledQueueColumn() {
        return ("ruled_queue");
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this queueConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  queueConstrainerRecordType a queue constrainer record type 
     *  @return <code>true</code> if the queueConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type queueConstrainerRecordType) {
        for (org.osid.provisioning.rules.records.QueueConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  queueConstrainerRecordType the queue constrainer record type 
     *  @return the queue constrainer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueConstrainerQueryRecord getQueueConstrainerQueryRecord(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  queueConstrainerRecordType the queue constrainer record type 
     *  @return the queue constrainer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueConstrainerQueryInspectorRecord getQueueConstrainerQueryInspectorRecord(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueConstrainerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param queueConstrainerRecordType the queue constrainer record type
     *  @return the queue constrainer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueConstrainerSearchOrderRecord getQueueConstrainerSearchOrderRecord(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueConstrainerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this queue constrainer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param queueConstrainerQueryRecord the queue constrainer query record
     *  @param queueConstrainerQueryInspectorRecord the queue constrainer query inspector
     *         record
     *  @param queueConstrainerSearchOrderRecord the queue constrainer search order record
     *  @param queueConstrainerRecordType queue constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerQueryRecord</code>,
     *          <code>queueConstrainerQueryInspectorRecord</code>,
     *          <code>queueConstrainerSearchOrderRecord</code> or
     *          <code>queueConstrainerRecordTypequeueConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addQueueConstrainerRecords(org.osid.provisioning.rules.records.QueueConstrainerQueryRecord queueConstrainerQueryRecord, 
                                      org.osid.provisioning.rules.records.QueueConstrainerQueryInspectorRecord queueConstrainerQueryInspectorRecord, 
                                      org.osid.provisioning.rules.records.QueueConstrainerSearchOrderRecord queueConstrainerSearchOrderRecord, 
                                      org.osid.type.Type queueConstrainerRecordType) {

        addRecordType(queueConstrainerRecordType);

        nullarg(queueConstrainerQueryRecord, "queue constrainer query record");
        nullarg(queueConstrainerQueryInspectorRecord, "queue constrainer query inspector record");
        nullarg(queueConstrainerSearchOrderRecord, "queue constrainer search odrer record");

        this.queryRecords.add(queueConstrainerQueryRecord);
        this.queryInspectorRecords.add(queueConstrainerQueryInspectorRecord);
        this.searchOrderRecords.add(queueConstrainerSearchOrderRecord);
        
        return;
    }
}
