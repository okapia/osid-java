//
// AbstractModelQueryInspector.java
//
//     A template for making a ModelQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.model.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for models.
 */

public abstract class AbstractModelQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.inventory.ModelQueryInspector {

    private final java.util.Collection<org.osid.inventory.records.ModelQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the manufacturer <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getManufacturerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the manufacturer query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getManufacturerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the archetype query terms. 
     *
     *  @return the archetype query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getArchetypeTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the model number query terms. 
     *
     *  @return the model number query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given model query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a model implementing the requested record.
     *
     *  @param modelRecordType a model record type
     *  @return the model query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(modelRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ModelQueryInspectorRecord getModelQueryInspectorRecord(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ModelQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(modelRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(modelRecordType + " is not supported");
    }


    /**
     *  Adds a record to this model query. 
     *
     *  @param modelQueryInspectorRecord model query inspector
     *         record
     *  @param modelRecordType model record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addModelQueryInspectorRecord(org.osid.inventory.records.ModelQueryInspectorRecord modelQueryInspectorRecord, 
                                                   org.osid.type.Type modelRecordType) {

        addRecordType(modelRecordType);
        nullarg(modelRecordType, "model record type");
        this.records.add(modelQueryInspectorRecord);        
        return;
    }
}
