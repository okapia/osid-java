//
// AbstractActionEnablerQuery.java
//
//     A template for making an ActionEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.actionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for action enablers.
 */

public abstract class AbstractActionEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.control.rules.ActionEnablerQuery {

    private final java.util.Collection<org.osid.control.rules.records.ActionEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the action. 
     *
     *  @param  actionId the action <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledActionId(org.osid.id.Id actionId, boolean match) {
        return;
    }


    /**
     *  Clears the action <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledActionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActionQuery </code> is available. 
     *
     *  @return <code> true </code> if an action query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledActionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the action query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledActionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionQuery getRuledActionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledActionQuery() is false");
    }


    /**
     *  Matches enablers mapped to any action. 
     *
     *  @param  match <code> true </code> for enablers mapped to any action, 
     *          <code> false </code> to match enablers mapped to no actions 
     */

    @OSID @Override
    public void matchAnyRuledAction(boolean match) {
        return;
    }


    /**
     *  Clears the action query terms. 
     */

    @OSID @Override
    public void clearRuledActionTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the system. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given action enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an action enabler implementing the requested record.
     *
     *  @param actionEnablerRecordType an action enabler record type
     *  @return the action enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.ActionEnablerQueryRecord getActionEnablerQueryRecord(org.osid.type.Type actionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.ActionEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(actionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this action enabler query. 
     *
     *  @param actionEnablerQueryRecord action enabler query record
     *  @param actionEnablerRecordType actionEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActionEnablerQueryRecord(org.osid.control.rules.records.ActionEnablerQueryRecord actionEnablerQueryRecord, 
                                          org.osid.type.Type actionEnablerRecordType) {

        addRecordType(actionEnablerRecordType);
        nullarg(actionEnablerQueryRecord, "action enabler query record");
        this.records.add(actionEnablerQueryRecord);        
        return;
    }
}
