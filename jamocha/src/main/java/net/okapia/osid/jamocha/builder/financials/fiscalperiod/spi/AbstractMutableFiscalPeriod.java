//
// AbstractMutableFiscalPeriod.java
//
//     Defines a mutable FiscalPeriod.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.fiscalperiod.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>FiscalPeriod</code>.
 */

public abstract class AbstractMutableFiscalPeriod
    extends net.okapia.osid.jamocha.financials.fiscalperiod.spi.AbstractFiscalPeriod
    implements org.osid.financials.FiscalPeriod,
               net.okapia.osid.jamocha.builder.financials.fiscalperiod.FiscalPeriodMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this fiscal period. 
     *
     *  @param record fiscal period record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addFiscalPeriodRecord(org.osid.financials.records.FiscalPeriodRecord record, org.osid.type.Type recordType) {
        super.addFiscalPeriodRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this fiscal period.
     *
     *  @param displayName the name for this fiscal period
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this fiscal period.
     *
     *  @param description the description of this fiscal period
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the display label.
     *
     *  @param displayLabel a display label
     *  @throws org.osid.NullArgumentException
     *          <code>displayLabel</code> is <code>null</code>
     */

    @Override
    public void setDisplayLabel(org.osid.locale.DisplayText displayLabel) {
        super.setDisplayLabel(displayLabel);
        return;
    }


    /**
     *  Sets the fiscal year.
     *
     *  @param year a fiscal year
     *  @throws org.osid.NullArgumentException <code>year</code> is
     *          <code>null</code>
     */

    @Override
    public void setFiscalYear(long year) {
        super.setFiscalYear(year);
        return;
    }


    /**
     *  Sets the start date.
     *
     *  @param date a start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date.
     *
     *  @param date an end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the budget deadline.
     *
     *  @param date a budget deadline
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setBudgetDeadline(org.osid.calendaring.DateTime date) {
        super.setBudgetDeadline(date);
        return;
    }


    /**
     *  Sets the posting deadline.
     *
     *  @param date a posting deadline
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setPostingDeadline(org.osid.calendaring.DateTime date) {
        super.setPostingDeadline(date);
        return;
    }


    /**
     *  Sets the closing.
     *
     *  @param date a closing
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    @Override
    public void setClosing(org.osid.calendaring.DateTime date) {
        super.setClosing(date);
        return;
    }
}

