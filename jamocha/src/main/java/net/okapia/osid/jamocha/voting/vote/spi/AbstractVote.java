//
// AbstractVote.java
//
//     Defines a Vote.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.vote.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Vote</code>.
 */

public abstract class AbstractVote
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.voting.Vote {

    private org.osid.voting.Candidate candidate;
    private org.osid.resource.Resource voter;
    private org.osid.authentication.Agent votingAgent;
    private long votes = 0;

    private final java.util.Collection<org.osid.voting.records.VoteRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the candidate <code>Id</code>. 
     *
     *  @return a candidate <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCandidateId() {
        return (this.candidate.getId());
    }


    /**
     *  Gets the <code>Candidate</code>. 
     *
     *  @return the candidate 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.voting.Candidate getCandidate()
        throws org.osid.OperationFailedException {

        return (this.candidate);
    }


    /**
     *  Sets the candidate.
     *
     *  @param candidate a candidate
     *  @throws org.osid.NullArgumentException
     *          <code>candidate</code> is <code>null</code>
     */

    protected void setCandidate(org.osid.voting.Candidate candidate) {
        nullarg(candidate, "candidate");
        this.candidate = candidate;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> of the voter. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getVoterId() {
        return (this.voter.getId());
    }


    /**
     *  Gets the resource of the voter. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getVoter()
        throws org.osid.OperationFailedException {

        return (this.voter);
    }


    /**
     *  Sets the voter.
     *
     *  @param voter a voter
     *  @throws org.osid.NullArgumentException
     *          <code>voter</code> is <code>null</code>
     */

    protected void setVoter(org.osid.resource.Resource voter) {
        nullarg(voter, "voter");
        this.voter = voter;
        return;
    }


    /**
     *  Geta the agent <code> Id </code> of the voter. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getVotingAgentId() {
        return (this.votingAgent.getId());
    }


    /**
     *  Gets the agent of the voter. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getVotingAgent()
        throws org.osid.OperationFailedException {

        return (this.votingAgent);
    }


    /**
     *  Sets the voting agent.
     *
     *  @param agent a voting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setVotingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "voting agent");
        this.votingAgent = agent;
        return;
    }


    /**
     *  Gets the number of votes cast for this candidate. 
     *
     *  @return the number of votes cast 
     */

    @OSID @Override
    public long getVotes() {
        return (this.votes);
    }


    /**
     *  Sets the votes.
     *
     *  @param votes the number of votes cast
     */

    protected void setVotes(long votes) {
        this.votes = votes;
        return;
    }


    /**
     *  Tests if this vote supports the given record
     *  <code>Type</code>.
     *
     *  @param  voteRecordType a vote record type 
     *  @return <code>true</code> if the voteRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type voteRecordType) {
        for (org.osid.voting.records.VoteRecord record : this.records) {
            if (record.implementsRecordType(voteRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Vote</code>
     *  record <code>Type</code>.
     *
     *  @param  voteRecordType the vote record type 
     *  @return the vote record 
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(voteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.VoteRecord getVoteRecord(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.VoteRecord record : this.records) {
            if (record.implementsRecordType(voteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(voteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this vote. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param voteRecord the vote record
     *  @param voteRecordType vote record type
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecord</code> or
     *          <code>voteRecordTypevote</code> is
     *          <code>null</code>
     */
            
    protected void addVoteRecord(org.osid.voting.records.VoteRecord voteRecord, 
                                 org.osid.type.Type voteRecordType) {

        nullarg(voteRecord, "vote record");
        addRecordType(voteRecordType);
        this.records.add(voteRecord);
        
        return;
    }
}
