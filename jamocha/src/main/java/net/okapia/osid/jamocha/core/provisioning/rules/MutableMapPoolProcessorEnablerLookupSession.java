//
// MutableMapPoolProcessorEnablerLookupSession
//
//    Implements a PoolProcessorEnabler lookup service backed by a collection of
//    poolProcessorEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolProcessorEnabler lookup service backed by a collection of
 *  pool processor enablers. The pool processor enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of pool processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapPoolProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapPoolProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.PoolProcessorEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapPoolProcessorEnablerLookupSession}
     *  with no pool processor enablers.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor} is
     *          {@code null}
     */

      public MutableMapPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPoolProcessorEnablerLookupSession} with a
     *  single poolProcessorEnabler.
     *
     *  @param distributor the distributor  
     *  @param poolProcessorEnabler a pool processor enabler
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessorEnabler} is {@code null}
     */

    public MutableMapPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler) {
        this(distributor);
        putPoolProcessorEnabler(poolProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPoolProcessorEnablerLookupSession}
     *  using an array of pool processor enablers.
     *
     *  @param distributor the distributor
     *  @param poolProcessorEnablers an array of pool processor enablers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessorEnablers} is {@code null}
     */

    public MutableMapPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.PoolProcessorEnabler[] poolProcessorEnablers) {
        this(distributor);
        putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPoolProcessorEnablerLookupSession}
     *  using a collection of pool processor enablers.
     *
     *  @param distributor the distributor
     *  @param poolProcessorEnablers a collection of pool processor enablers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessorEnablers} is {@code null}
     */

    public MutableMapPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                           java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessorEnabler> poolProcessorEnablers) {

        this(distributor);
        putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }

    
    /**
     *  Makes a {@code PoolProcessorEnabler} available in this session.
     *
     *  @param poolProcessorEnabler a pool processor enabler
     *  @throws org.osid.NullArgumentException {@code poolProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putPoolProcessorEnabler(org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler) {
        super.putPoolProcessorEnabler(poolProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of pool processor enablers available in this session.
     *
     *  @param poolProcessorEnablers an array of pool processor enablers
     *  @throws org.osid.NullArgumentException {@code poolProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putPoolProcessorEnablers(org.osid.provisioning.rules.PoolProcessorEnabler[] poolProcessorEnablers) {
        super.putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of pool processor enablers available in this session.
     *
     *  @param poolProcessorEnablers a collection of pool processor enablers
     *  @throws org.osid.NullArgumentException {@code poolProcessorEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putPoolProcessorEnablers(java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessorEnabler> poolProcessorEnablers) {
        super.putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }


    /**
     *  Removes a PoolProcessorEnabler from this session.
     *
     *  @param poolProcessorEnablerId the {@code Id} of the pool processor enabler
     *  @throws org.osid.NullArgumentException {@code poolProcessorEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removePoolProcessorEnabler(org.osid.id.Id poolProcessorEnablerId) {
        super.removePoolProcessorEnabler(poolProcessorEnablerId);
        return;
    }    
}
