//
// AbstractRelevancyEnabler.java
//
//     Defines a RelevancyEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ontology.rules.relevancyenabler.spi;


/**
 *  Defines a <code>RelevancyEnabler</code> builder.
 */

public abstract class AbstractRelevancyEnablerBuilder<T extends AbstractRelevancyEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.ontology.rules.relevancyenabler.RelevancyEnablerMiter relevancyEnabler;


    /**
     *  Constructs a new <code>AbstractRelevancyEnablerBuilder</code>.
     *
     *  @param relevancyEnabler the relevancy enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRelevancyEnablerBuilder(net.okapia.osid.jamocha.builder.ontology.rules.relevancyenabler.RelevancyEnablerMiter relevancyEnabler) {
        super(relevancyEnabler);
        this.relevancyEnabler = relevancyEnabler;
        return;
    }


    /**
     *  Builds the relevancy enabler.
     *
     *  @return the new relevancy enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.ontology.rules.RelevancyEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.ontology.rules.relevancyenabler.RelevancyEnablerValidator(getValidations())).validate(this.relevancyEnabler);
        return (new net.okapia.osid.jamocha.builder.ontology.rules.relevancyenabler.ImmutableRelevancyEnabler(this.relevancyEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the relevancy enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.ontology.rules.relevancyenabler.RelevancyEnablerMiter getMiter() {
        return (this.relevancyEnabler);
    }


    /**
     *  Adds a RelevancyEnabler record.
     *
     *  @param record a relevancy enabler record
     *  @param recordType the type of relevancy enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.ontology.rules.records.RelevancyEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addRelevancyEnablerRecord(record, recordType);
        return (self());
    }
}       


