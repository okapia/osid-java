//
// AbstractModuleQuery.java
//
//     A template for making a Module Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.module.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for modules.
 */

public abstract class AbstractModuleQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.course.syllabus.ModuleQuery {

    private final java.util.Collection<org.osid.course.syllabus.records.ModuleQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the syllabus <code> Id </code> for this query. 
     *
     *  @param  syllabusId a syllabus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> syllabusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSyllabusId(org.osid.id.Id syllabusId, boolean match) {
        return;
    }


    /**
     *  Clears the syllabus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSyllabusIdTerms() {
        return;
    }


    /**
     *  Tests if a syllabus query is available. 
     *
     *  @return <code> true </code> if a syllabus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a docet. 
     *
     *  @return the syllabus query 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuery getSyllabusQuery() {
        throw new org.osid.UnimplementedException("supportsSyllabusQuery() is false");
    }


    /**
     *  Clears the syllabus terms. 
     */

    @OSID @Override
    public void clearSyllabusTerms() {
        return;
    }


    /**
     *  Sets a docet <code> Id. </code> 
     *
     *  @param  docetId a docet <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> docetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDocetId(org.osid.id.Id docetId, boolean match) {
        return;
    }


    /**
     *  Clears the docet <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDocetIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> DocetQuery </code> is available. 
     *
     *  @return <code> true </code> if a docet query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetQuery() {
        return (false);
    }


    /**
     *  Gets the query for a docet query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the docet query 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuery getDocetQuery() {
        throw new org.osid.UnimplementedException("supportsDocetQuery() is false");
    }


    /**
     *  Matches modules with any docet. 
     *
     *  @param  match <code> true </code> to match modules with any docet, 
     *          <code> false </code> to match modules with no docets 
     */

    @OSID @Override
    public void matchAnyDocet(boolean match) {
        return;
    }


    /**
     *  Clears the docet terms. 
     */

    @OSID @Override
    public void clearDocetTerms() {
        return;
    }


    /**
     *  Sets the docet <code> Id </code> for this query to match modules 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given module query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a module implementing the requested record.
     *
     *  @param moduleRecordType a module record type
     *  @return the module query record
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(moduleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.ModuleQueryRecord getModuleQueryRecord(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.ModuleQueryRecord record : this.records) {
            if (record.implementsRecordType(moduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(moduleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this module query. 
     *
     *  @param moduleQueryRecord module query record
     *  @param moduleRecordType module record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addModuleQueryRecord(org.osid.course.syllabus.records.ModuleQueryRecord moduleQueryRecord, 
                                          org.osid.type.Type moduleRecordType) {

        addRecordType(moduleRecordType);
        nullarg(moduleQueryRecord, "module query record");
        this.records.add(moduleQueryRecord);        
        return;
    }
}
