//
// AbstractCompetency.java
//
//     Defines a Competency.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.competency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Competency</code>.
 */

public abstract class AbstractCompetency
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.resourcing.Competency {

    private boolean hasLearningObjectives = false;
    private final java.util.Collection<org.osid.learning.Objective> learningObjectives = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.resourcing.records.CompetencyRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if learning objectives are available for this competency. 
     *
     *  @return true if learning objectives are available, false otherwise 
     */

    @OSID @Override
    public boolean hasLearningObjectives() {
        return (this.hasLearningObjectives);
    }


    /**
     *  Gets the Ids of the learning objectives. 
     *
     *  @return the learning objective <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        if (!hasLearningObjectives()) {
            throw new org.osid.IllegalStateException("hasLearningObjectives() is false");
        }

        try {
            org.osid.learning.ObjectiveList learningObjectives = getLearningObjectives();
            return (new net.okapia.osid.jamocha.adapter.converter.learning.objective.ObjectiveToIdList(learningObjectives));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the learning objectives. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        if (!hasLearningObjectives()) {
            throw new org.osid.IllegalStateException("hasLearningObjectives() is false");
        }

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.learningObjectives));
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException
     *          <code>objective</code> is <code>null</code>
     */

    protected void addLearningObjective(org.osid.learning.Objective objective) {
        nullarg(objective, "learning objective");
        this.learningObjectives.add(objective);
        this.hasLearningObjectives = true;
        return;
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException
     *          <code>objectives</code> is <code>null</code>
     */

    protected void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        nullarg(objectives, "learning objectives");
        this.learningObjectives.clear();
        this.learningObjectives.addAll(learningObjectives);
        this.hasLearningObjectives = true;
        return;
    }


    /**
     *  Tests if this competency supports the given record
     *  <code>Type</code>.
     *
     *  @param  competencyRecordType a competency record type 
     *  @return <code>true</code> if the competencyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type competencyRecordType) {
        for (org.osid.resourcing.records.CompetencyRecord record : this.records) {
            if (record.implementsRecordType(competencyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Competency</code> record <code>Type</code>.
     *
     *  @param  competencyRecordType the competency record type 
     *  @return the competency record 
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(competencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CompetencyRecord getCompetencyRecord(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CompetencyRecord record : this.records) {
            if (record.implementsRecordType(competencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(competencyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this competency. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param competencyRecord the competency record
     *  @param competencyRecordType competency record type
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecord</code> or
     *          <code>competencyRecordTypecompetency</code> is
     *          <code>null</code>
     */
            
    protected void addCompetencyRecord(org.osid.resourcing.records.CompetencyRecord competencyRecord, 
                                       org.osid.type.Type competencyRecordType) {
        
        nullarg(competencyRecord, "competency record");
        addRecordType(competencyRecordType);
        this.records.add(competencyRecord);
        
        return;
    }
}
