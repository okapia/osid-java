//
// AbstractMutableTodo.java
//
//     Defines a mutable Todo.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.checklist.todo.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Todo</code>.
 */

public abstract class AbstractMutableTodo
    extends net.okapia.osid.jamocha.checklist.todo.spi.AbstractTodo
    implements org.osid.checklist.Todo,
               net.okapia.osid.jamocha.builder.checklist.todo.TodoMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    @Override
    public void setSequestered(boolean sequestered) {
        super.setSequestered(sequestered);
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this todo. 
     *
     *  @param record todo record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addTodoRecord(org.osid.checklist.records.TodoRecord record, org.osid.type.Type recordType) {
        super.addTodoRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this todo is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this todo ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this todo.
     *
     *  @param displayName the name for this todo
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this todo.
     *
     *  @param description the description of this todo
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @throws org.osid.NullArgumentException
     *          <code>priority</code> is <code>null</code>
     */

    @Override
    public void setPriority(org.osid.type.Type priority) {
        super.setPriority(priority);
        return;
    }


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code> true </code> if this todo is complete,
     *          <code> false </code> if not complete
     */

    @Override
    public void setComplete(boolean complete) {
        super.setComplete(complete);
        return;
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setDueDate(org.osid.calendaring.DateTime date) {
        super.setDueDate(date);
        return;
    }


    /**
     *  Adds a dependency.
     *
     *  @param dependency a dependency
     *  @throws org.osid.NullArgumentException <code>dependency</code>
     *          is <code>null</code>
     */

    @Override
    public void addDependency(org.osid.checklist.Todo dependency) {
        super.addDependency(dependency);
        return;
    }


    /**
     *  Sets all the dependencies.
     *
     *  @param dependencies a collection of dependencies
     *  @throws org.osid.NullArgumentException
     *          <code>dependencies</code> is <code>null</code>
     */

    @Override
    public void setDependencies(java.util.Collection<org.osid.checklist.Todo> dependencies) {
        super.setDependencies(dependencies);
        return;
    }
}

