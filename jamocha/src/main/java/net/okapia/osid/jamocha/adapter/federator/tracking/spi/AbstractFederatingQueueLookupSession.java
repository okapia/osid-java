//
// AbstractFederatingQueueLookupSession.java
//
//     An abstract federating adapter for a QueueLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  QueueLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingQueueLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.tracking.QueueLookupSession>
    implements org.osid.tracking.QueueLookupSession {

    private boolean parallel = false;
    private org.osid.tracking.FrontOffice frontOffice = new net.okapia.osid.jamocha.nil.tracking.frontoffice.UnknownFrontOffice();


    /**
     *  Constructs a new <code>AbstractFederatingQueueLookupSession</code>.
     */

    protected AbstractFederatingQueueLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.tracking.QueueLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>FrontOffice/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.frontOffice.getId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this 
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.frontOffice);
    }


    /**
     *  Sets the <code>FrontOffice</code>.
     *
     *  @param  frontOffice the front office for this session
     *  @throws org.osid.NullArgumentException <code>frontOffice</code>
     *          is <code>null</code>
     */

    protected void setFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        nullarg(frontOffice, "front office");
        this.frontOffice = frontOffice;
        return;
    }


    /**
     *  Tests if this user can perform <code>Queue</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQueues() {
        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            if (session.canLookupQueues()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Queue</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueView() {
        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            session.useComparativeQueueView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Queue</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueView() {
        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            session.usePlenaryQueueView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queues in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            session.useFederatedFrontOfficeView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            session.useIsolatedFrontOfficeView();
        }

        return;
    }


    /**
     *  Only active queues are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveQueueView() {
        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            session.useActiveQueueView();
        }

        return;
    }


    /**
     *  Active and inactive queues are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueView() {
        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            session.useAnyStatusQueueView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Queue</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Queue</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Queue</code> and
     *  retained for compatibility.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @param  queueId <code>Id</code> of the
     *          <code>Queue</code>
     *  @return the queue
     *  @throws org.osid.NotFoundException <code>queueId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>queueId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.Queue getQueue(org.osid.id.Id queueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            try {
                return (session.getQueue(queueId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(queueId + " not found");
    }


    /**
     *  Gets a <code>QueueList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  queues specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Queues</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  @param  queueIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Queue</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>queueIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByIds(org.osid.id.IdList queueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.tracking.queue.MutableQueueList ret = new net.okapia.osid.jamocha.tracking.queue.MutableQueueList();

        try (org.osid.id.IdList ids = queueIds) {
            while (ids.hasNext()) {
                ret.addQueue(getQueue(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>QueueList</code> corresponding to the given
     *  queue genus <code>Type</code> which does not include
     *  queues of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  queues or an error results. Otherwise, the returned list
     *  may contain only those queues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  @param  queueGenusType a queue genus type 
     *  @return the returned <code>Queue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByGenusType(org.osid.type.Type queueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.queue.FederatingQueueList ret = getQueueList();

        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            ret.addQueueList(session.getQueuesByGenusType(queueGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueList</code> corresponding to the given
     *  queue genus <code>Type</code> and include any additional
     *  queues with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  queues or an error results. Otherwise, the returned list
     *  may contain only those queues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  @param  queueGenusType a queue genus type 
     *  @return the returned <code>Queue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByParentGenusType(org.osid.type.Type queueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.queue.FederatingQueueList ret = getQueueList();

        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            ret.addQueueList(session.getQueuesByParentGenusType(queueGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueList</code> containing the given
     *  queue record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  queues or an error results. Otherwise, the returned list
     *  may contain only those queues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  @param  queueRecordType a queue record type 
     *  @return the returned <code>Queue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByRecordType(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.queue.FederatingQueueList ret = getQueueList();

        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            ret.addQueueList(session.getQueuesByRecordType(queueRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known queues or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  queues that are accessible through this session. 
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Queue</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.tracking.queue.FederatingQueueList ret = getQueueList();

        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            ret.addQueueList(session.getQueuesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Queues</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  queues or an error results. Otherwise, the returned list
     *  may contain only those queues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @return a list of <code>Queues</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.queue.FederatingQueueList ret = getQueueList();

        for (org.osid.tracking.QueueLookupSession session : getSessions()) {
            ret.addQueueList(session.getQueues());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.tracking.queue.FederatingQueueList getQueueList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.queue.ParallelQueueList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.queue.CompositeQueueList());
        }
    }
}
