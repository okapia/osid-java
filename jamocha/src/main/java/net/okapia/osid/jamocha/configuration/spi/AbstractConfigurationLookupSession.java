//
// AbstractConfigurationLookupSession.java
//
//    A starter implementation framework for providing a Configuration
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Configuration
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getConfigurations(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractConfigurationLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.configuration.ConfigurationLookupSession {

    private boolean pedantic = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();
    


    /**
     *  Tests if this user can perform <code>Configuration</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupConfigurations() {
        return (true);
    }


    /**
     *  A complete view of the <code>Configuration</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeConfigurationView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Configuration</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryConfigurationView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Configuration</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Configuration</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Configuration</code> and
     *  retained for compatibility.
     *
     *  @param  configurationId <code>Id</code> of the
     *          <code>Configuration</code>
     *  @return the configuration
     *  @throws org.osid.NotFoundException <code>configurationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>configurationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.configuration.ConfigurationList configurations = getConfigurations()) {
            while (configurations.hasNext()) {
                org.osid.configuration.Configuration configuration = configurations.getNextConfiguration();
                if (configuration.getId().equals(configurationId)) {
                    return (configuration);
                }
            }
        } 

        throw new org.osid.NotFoundException(configurationId + " not found");
    }


    /**
     *  Gets a <code>ConfigurationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  configurations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Configurations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getConfigurations()</code>.
     *
     *  @param  configurationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Configuration</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>configurationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByIds(org.osid.id.IdList configurationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.configuration.Configuration> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = configurationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getConfiguration(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("configuration " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.configuration.configuration.LinkedConfigurationList(ret));
    }


    /**
     *  Gets a <code>ConfigurationList</code> corresponding to the given
     *  configuration genus <code>Type</code> which does not include
     *  configurations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getConfigurations()</code>.
     *
     *  @param  configurationGenusType a configuration genus type 
     *  @return the returned <code>Configuration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>configurationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByGenusType(org.osid.type.Type configurationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.configuration.ConfigurationGenusFilterList(getConfigurations(), configurationGenusType));
    }


    /**
     *  Gets a <code>ConfigurationList</code> corresponding to the given
     *  configuration genus <code>Type</code> and include any additional
     *  configurations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getConfigurations()</code>.
     *
     *  @param  configurationGenusType a configuration genus type 
     *  @return the returned <code>Configuration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>configurationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByParentGenusType(org.osid.type.Type configurationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getConfigurationsByGenusType(configurationGenusType));
    }


    /**
     *  Gets a <code>ConfigurationList</code> containing the given
     *  configuration record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getConfigurations()</code>.
     *
     *  @param  configurationRecordType a configuration record type 
     *  @return the returned <code>Configuration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByRecordType(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.configuration.ConfigurationRecordFilterList(getConfigurations(), configurationRecordType));
    }


    /**
     *  Gets a <code>ConfigurationList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known configurations or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  configurations that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Configuration</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.configuration.configuration.ConfigurationProviderFilterList(getConfigurations(), resourceId));
    }


    /**
     *  Gets all <code>Configurations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Configurations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.configuration.ConfigurationList getConfigurations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the configuration list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of configurations
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.configuration.ConfigurationList filterConfigurationsOnViews(org.osid.configuration.ConfigurationList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
