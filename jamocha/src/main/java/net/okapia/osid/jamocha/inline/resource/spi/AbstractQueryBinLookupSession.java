//
// AbstractQueryBinLookupSession.java
//
//    An inline adapter that maps a BinLookupSession to
//    a BinQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BinLookupSession to
 *  a BinQuerySession.
 */

public abstract class AbstractQueryBinLookupSession
    extends net.okapia.osid.jamocha.resource.spi.AbstractBinLookupSession
    implements org.osid.resource.BinLookupSession {

    private final org.osid.resource.BinQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBinLookupSession.
     *
     *  @param querySession the underlying bin query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBinLookupSession(org.osid.resource.BinQuerySession querySession) {
        nullarg(querySession, "bin query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Bin</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBins() {
        return (this.session.canSearchBins());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Bin</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Bin</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Bin</code> and
     *  retained for compatibility.
     *
     *  @param  binId <code>Id</code> of the
     *          <code>Bin</code>
     *  @return the bin
     *  @throws org.osid.NotFoundException <code>binId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>binId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.BinQuery query = getQuery();
        query.matchId(binId, true);
        org.osid.resource.BinList bins = this.session.getBinsByQuery(query);
        if (bins.hasNext()) {
            return (bins.getNextBin());
        } 
        
        throw new org.osid.NotFoundException(binId + " not found");
    }


    /**
     *  Gets a <code>BinList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  bins specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Bins</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  binIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Bin</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>binIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByIds(org.osid.id.IdList binIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.BinQuery query = getQuery();

        try (org.osid.id.IdList ids = binIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBinsByQuery(query));
    }


    /**
     *  Gets a <code>BinList</code> corresponding to the given
     *  bin genus <code>Type</code> which does not include
     *  bins of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  bins or an error results. Otherwise, the returned list
     *  may contain only those bins that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  binGenusType a bin genus type 
     *  @return the returned <code>Bin</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>binGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByGenusType(org.osid.type.Type binGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.BinQuery query = getQuery();
        query.matchGenusType(binGenusType, true);
        return (this.session.getBinsByQuery(query));
    }


    /**
     *  Gets a <code>BinList</code> corresponding to the given
     *  bin genus <code>Type</code> and include any additional
     *  bins with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bins or an error results. Otherwise, the returned list
     *  may contain only those bins that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  binGenusType a bin genus type 
     *  @return the returned <code>Bin</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>binGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByParentGenusType(org.osid.type.Type binGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.BinQuery query = getQuery();
        query.matchParentGenusType(binGenusType, true);
        return (this.session.getBinsByQuery(query));
    }


    /**
     *  Gets a <code>BinList</code> containing the given
     *  bin record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  bins or an error results. Otherwise, the returned list
     *  may contain only those bins that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  binRecordType a bin record type 
     *  @return the returned <code>Bin</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>binRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByRecordType(org.osid.type.Type binRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.BinQuery query = getQuery();
        query.matchRecordType(binRecordType, true);
        return (this.session.getBinsByQuery(query));
    }


    /**
     *  Gets a <code>BinList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known bins or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  bins that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Bin</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.BinQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getBinsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Bins</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  bins or an error results. Otherwise, the returned list
     *  may contain only those bins that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Bins</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBins()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.BinQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBinsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resource.BinQuery getQuery() {
        org.osid.resource.BinQuery query = this.session.getBinQuery();
        return (query);
    }
}
