//
// AbstractResourceRelationshipNotificationSession.java
//
//     A template for making ResourceRelationshipNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code ResourceRelationship} objects. This session
 *  is intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code ResourceRelationship} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for resource relationship entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractResourceRelationshipNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resource.ResourceRelationshipNotificationSession {

    private boolean federated = false;
    private org.osid.resource.Bin bin = new net.okapia.osid.jamocha.nil.resource.bin.UnknownBin();


    /**
     *  Gets the {@code Bin/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Bin Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.bin.getId());
    }

    
    /**
     *  Gets the {@code Bin} associated with this session.
     *
     *  @return the {@code Bin} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bin);
    }


    /**
     *  Sets the {@code Bin}.
     *
     *  @param bin the bin for this session
     *  @throws org.osid.NullArgumentException {@code bin}
     *          is {@code null}
     */

    protected void setBin(org.osid.resource.Bin bin) {
        nullarg(bin, "bin");
        this.bin = bin;
        return;
    }


    /**
     *  Tests if this user can register for {@code
     *  ResourceRelationship} notifications.  A return of true does
     *  not guarantee successful authorization. A return of false
     *  indicates that it is known all methods in this session will
     *  result in a {@code PERMISSION_DENIED}. This is intended as a
     *  hint to an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForResourceRelationshipNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resource relationships in bins which are
     *  children of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new resource
     *  relationships. {@code
     *  ResourceRelationshipReceiver.newResourceRelationship()} is
     *  invoked when a new {@code ResourceRelationship} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewResourceRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new relationships of the given
     *  genus type. {@code
     *  ResourceRelationshipReceiver.newResourceRelationship()} is
     *  invoked when a new relationship is created.
     *
     *  @param resourceRelationshipGenusType the rsource relationship
     *          genus type
     *  @throws org.osid.NullArgumentException {@code 
     *          resourceRelationshipGenusType is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewResourceRelationshipsByGenusType(org.osid.type.Type resourceRelationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new resourceRelationships for
     *  the given source resource {@code Id}. {@code
     *  ResourceRelationshipReceiver.newResourceRelationship()} is
     *  invoked when a new {@code ResourceRelationship} is created.
     *
     *  @param sourceResourceId the {@code Id} of the source resource
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          sourceResourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewResourceRelationshipsForSourceResource(org.osid.id.Id sourceResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new resourceRelationships for
     *  the given destination resource {@code Id}. {@code
     *  ResourceRelationshipReceiver.newResourceRelationship()} is
     *  invoked when a new {@code ResourceRelationship} is created.
     *
     *  @param destinationResourceId the {@code Id} of the destination
     *         resource to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          destinationResourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewResourceRelationshipsForDestinationResource(org.osid.id.Id destinationResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated resource
     *  relationships. {@code
     *  ResourceRelationshipReceiver.changedResourceRelationship()} is
     *  invoked when a resource relationship is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedResourceRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of changed relationshipsof the
     *  given genus type. {@code
     *  ResourceRelationshipReceiver.changedResourceRelationship()} is
     *  invoked when a relationship is changed.
     *
     *  @param resourceRelationshipGenusType the rsource relationship
     *          genus type
     *  @throws org.osid.NullArgumentException {@code
     *          resourceRelationshipGenusType is null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedResourceRelationshipsByGenusType(org.osid.type.Type resourceRelationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated resourceRelationships
     *  for the given source resource {@code Id}. {@code
     *  ResourceRelationshipReceiver.changedResourceRelationship()} is
     *  invoked when a {@code ResourceRelationship} in this bin is
     *  changed.
     *
     *  @param sourceResourceId the {@code Id} of the source resource
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          sourceResourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedResourceRelationshipsForSourceResource(org.osid.id.Id sourceResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated resourceRelationships
     *  for the given destination resource {@code Id}. {@code
     *  ResourceRelationshipReceiver.changedResourceRelationship()} is
     *  invoked when a {@code ResourceRelationship} in this bin is
     *  changed.
     *
     *  @param destinationResourceId the {@code Id} of the destination
     *         resource to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          destinationResourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedResourceRelationshipsForDestinationResource(org.osid.id.Id destinationResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated resource
     *  relationship. {@code
     *  ResourceRelationshipReceiver.changedResourceRelationship()} is
     *  invoked when the specified resource relationship is changed.
     *
     *  @param resourceRelationshipId the {@code Id} of the {@code
     *         ResourceRelationship} to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          resourceRelationshipId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedResourceRelationship(org.osid.id.Id resourceRelationshipId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted resource
     *  relationships. {@code
     *  ResourceRelationshipReceiver.deletedResourceRelationship()} is
     *  invoked when a resource relationship is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedResourceRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted relationships of the
     *  given genus type. {@code
     *  ResourceRelationshipReceiver.deletedResourceRelationship()} is
     *  invoked when a relationship is deleted.
     *
     *  @param  resourceRelationshipGenusType the rsource relationship genus 
     *          type 
     *  @throws org.osid.NullArgumentException {@code 
     *          resourceRelationshipGenusType is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedResourceRelationshipsByGenusType(org.osid.type.Type resourceRelationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted resourceRelationships
     *  for the given source resource {@code Id}. {@code
     *  ResourceRelationshipReceiver.deletedResourceRelationship()} is
     *  invoked when a {@code ResourceRelationship} is deleted or
     *  removed from this bin.
     *
     *  @param sourceResourceId the {@code Id} of the source resource
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          sourceResourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedResourceRelationshipsForSourceResource(org.osid.id.Id sourceResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted resourceRelationships
     *  for the given destination resource {@code Id}. {@code
     *  ResourceRelationshipReceiver.deletedResourceRelationship()} is
     *  invoked when a {@code ResourceRelationship} is deleted or
     *  removed from this bin.
     *
     *  @param destinationResourceId the {@code Id} of the destination
     *         resource to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          destinationResourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedResourceRelationshipsForDestinationResource(org.osid.id.Id destinationResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted resource
     *  relationship. {@code
     *  ResourceRelationshipReceiver.deletedResourceRelationship()} is
     *  invoked when the specified resource relationship is deleted.
     *
     *  @param resourceRelationshipId the {@code Id} of the {@code
     *          ResourceRelationship} to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          resourceRelationshipId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedResourceRelationship(org.osid.id.Id resourceRelationshipId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
