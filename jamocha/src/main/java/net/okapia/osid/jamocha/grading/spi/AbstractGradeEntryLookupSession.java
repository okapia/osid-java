//
// AbstractGradeEntryLookupSession.java
//
//    A starter implementation framework for providing a GradeEntry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a GradeEntry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getGradeEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractGradeEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.grading.GradeEntryLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.grading.Gradebook gradebook = new net.okapia.osid.jamocha.nil.grading.gradebook.UnknownGradebook();
    

    /**
     *  Gets the <code>Gradebook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Gradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.gradebook.getId());
    }


    /**
     *  Gets the <code>Gradebook</code> associated with this 
     *  session.
     *
     *  @return the <code>Gradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.gradebook);
    }


    /**
     *  Sets the <code>Gradebook</code>.
     *
     *  @param  gradebook the gradebook for this session
     *  @throws org.osid.NullArgumentException <code>gradebook</code>
     *          is <code>null</code>
     */

    protected void setGradebook(org.osid.grading.Gradebook gradebook) {
        nullarg(gradebook, "gradebook");
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Tests if this user can perform <code>GradeEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradeEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>GradeEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradeEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>GradeEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradeEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade entries in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only grade entries whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveGradeEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All grade entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveGradeEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>GradeEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradeEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>GradeEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param  gradeEntryId <code>Id</code> of the
     *          <code>GradeEntry</code>
     *  @return the grade entry
     *  @throws org.osid.NotFoundException <code>gradeEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradeEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntry getGradeEntry(org.osid.id.Id gradeEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.grading.GradeEntryList gradeEntries = getGradeEntries()) {
            while (gradeEntries.hasNext()) {
                org.osid.grading.GradeEntry gradeEntry = gradeEntries.getNextGradeEntry();
                if (gradeEntry.getId().equals(gradeEntryId)) {
                    return (gradeEntry);
                }
            }
        } 

        throw new org.osid.NotFoundException(gradeEntryId + " not found");
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>GradeEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, grade entries are returned that are currently effective.
     *  In any effective mode, effective grade entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getGradeEntries()</code>.
     *
     *  @param  gradeEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByIds(org.osid.id.IdList gradeEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.grading.GradeEntry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = gradeEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getGradeEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("grade entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.grading.gradeentry.LinkedGradeEntryList(ret));
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  grade entry genus <code>Type</code> which does not include
     *  grade entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently effective.
     *  In any effective mode, effective grade entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getGradeEntries()</code>.
     *
     *  @param  gradeEntryGenusType a gradeEntry genus type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByGenusType(org.osid.type.Type gradeEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.GradeEntryGenusFilterList(getGradeEntries(), gradeEntryGenusType));
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  grade entry genus <code>Type</code> and include any additional
     *  grade entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGradeEntries()</code>.
     *
     *  @param  gradeEntryGenusType a gradeEntry genus type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByParentGenusType(org.osid.type.Type gradeEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getGradeEntriesByGenusType(gradeEntryGenusType));
    }


    /**
     *  Gets a <code>GradeEntryList</code> containing the given
     *  grade entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGradeEntries()</code>.
     *
     *  @param  gradeEntryRecordType a gradeEntry record type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByRecordType(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.GradeEntryRecordFilterList(getGradeEntries(), gradeEntryRecordType));
    }


    /**
     *  Gets a <code>GradeEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible
     *  through this session.
     *  
     *  In active mode, grade entries are returned that are currently
     *  active. In any status mode, active and inactive grade entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>GradeEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.TemporalGradeEntryFilterList(getGradeEntries(), from, to));
    }
        

    /**
     *  Gets a list of grade entries corresponding to a gradebook
     *  column <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the <code>Id</code> of the gradebook column
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.GradeEntryFilterList(new GradebookColumnFilter(gradebookColumnId), getGradeEntries()));
    }
    

    /**
     *  Gets a list of grade entries corresponding to a gradebook
     *  column <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible
     *  through this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param gradebookColumnId the <code>Id</code> of the gradebook
     *         column
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnOnDate(org.osid.id.Id gradebookColumnId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.TemporalGradeEntryFilterList(getGradeEntriesForGradebookColumn(gradebookColumnId), from, to));
    }


    /**
     *  Gets a list of grade entries corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible
     *  through this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.grading.GradeEntryList getGradeEntriesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.GradeEntryFilterList(new ResourceFilter(resourceId), getGradeEntries()));
    }


    /**
     *  Gets a list of grade entries corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible
     *  through this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForResourceOnDate(org.osid.id.Id resourceId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.TemporalGradeEntryFilterList(getGradeEntriesForResource(resourceId), from, to));
    }


    /**
     *  Gets a list of grade entries corresponding to gradebook column
     *  and resource <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible
     *  through this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param gradebookColumnId the <code>Id</code> of the gradebook
     *         column
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnAndResource(org.osid.id.Id gradebookColumnId,
                                                                                        org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.GradeEntryFilterList(new ResourceFilter(resourceId), getGradeEntriesForGradebookColumn(gradebookColumnId)));
    }


    /**
     *  Gets a list of grade entries corresponding to gradebook column
     *  and resource <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible
     *  through this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnAndResourceOnDate(org.osid.id.Id gradebookColumnId,
                                                                                              org.osid.id.Id resourceId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.TemporalGradeEntryFilterList(getGradeEntriesForGradebookColumnAndResource(gradebookColumnId, resourceId), from, to));
    }


    /**
     *  Gets a <code>GradeEntryList</code> for the given grader. 
     *  
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *  
     *  In effective mode, grade entries are returned that are
     *  currently effective. In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId an resource <code>Id</code> 
     *  @return the returned <code>GradeEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByGrader(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.GradeEntryFilterList(new GraderFilter(resourceId), getGradeEntries()));
    }
        

    /**
     *  Gets all <code>GradeEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>GradeEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.grading.GradeEntryList getGradeEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the grade entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of grade entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.grading.GradeEntryList filterGradeEntriesOnViews(org.osid.grading.GradeEntryList list)
        throws org.osid.OperationFailedException {

        org.osid.grading.GradeEntryList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.grading.gradeentry.EffectiveGradeEntryFilterList(ret);
        }

        return (ret);
    }


    public static class GradebookColumnFilter
        implements net.okapia.osid.jamocha.inline.filter.grading.gradeentry.GradeEntryFilter {
         
        private final org.osid.id.Id gradebookColumnId;
         
         
        /**
         *  Constructs a new <code>GradebookColumnFilter</code>.
         *
         *  @param gradebookColumnId the gradebook column to filter
         *  @throws org.osid.NullArgumentException
         *          <code>gradebookColumnId</code> is <code>null</code>
         */
        
        public GradebookColumnFilter(org.osid.id.Id gradebookColumnId) {
            nullarg(gradebookColumnId, "gradebook column Id");
            this.gradebookColumnId = gradebookColumnId;
            return;
        }

         
        /**
         *  Used by the GradeEntryFilterList to filter the 
         *  grade entry list based on gradebook column.
         *
         *  @param gradeEntry the grade entry
         *  @return <code>true</code> to pass the grade entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.grading.GradeEntry gradeEntry) {
            return (gradeEntry.getGradebookColumnId().equals(this.gradebookColumnId));
        }
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.grading.gradeentry.GradeEntryFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the GradeEntryFilterList to filter the 
         *  grade entry list based on resource.
         *
         *  @param gradeEntry the grade entry
         *  @return <code>true</code> to pass the grade entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.grading.GradeEntry gradeEntry) {
            return (gradeEntry.getKeyResourceId().equals(this.resourceId));
        }
    }


    public static class GraderFilter
        implements net.okapia.osid.jamocha.inline.filter.grading.gradeentry.GradeEntryFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>GraderFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public GraderFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the GradeEntryFilterList to filter the 
         *  grade entry list based on resource.
         *
         *  @param gradeEntry the grade entry
         *  @return <code>true</code> to pass the grade entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.grading.GradeEntry gradeEntry) {
            if (gradeEntry.isGraded() && !gradeEntry.isDerived()) {
                return (gradeEntry.getGraderId().equals(this.resourceId));
            } else {
                return (false);
            }
        }
    }
}
