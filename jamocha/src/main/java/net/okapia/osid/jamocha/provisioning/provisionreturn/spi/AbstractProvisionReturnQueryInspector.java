//
// AbstractProvisionReturnQueryInspector.java
//
//     A template for making a ProvisionReturnQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provisionreturn.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for provision returns.
 */

public abstract class AbstractProvisionReturnQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.provisioning.ProvisionReturnQueryInspector {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionReturnQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the return date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getReturnDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the returner <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReturnerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the returner query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getReturnerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReturningAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getReturningAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given provision return query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a provision return implementing the requested record.
     *
     *  @param provisionReturnRecordType a provision return record type
     *  @return the provision return query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturnRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionReturnRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionReturnQueryInspectorRecord getProvisionReturnQueryInspectorRecord(org.osid.type.Type provisionReturnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionReturnQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(provisionReturnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionReturnRecordType + " is not supported");
    }


    /**
     *  Adds a record to this provision return query. 
     *
     *  @param provisionReturnQueryInspectorRecord provision return query inspector
     *         record
     *  @param provisionReturnRecordType provisionReturn record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProvisionReturnQueryInspectorRecord(org.osid.provisioning.records.ProvisionReturnQueryInspectorRecord provisionReturnQueryInspectorRecord, 
                                                   org.osid.type.Type provisionReturnRecordType) {

        addRecordType(provisionReturnRecordType);
        nullarg(provisionReturnRecordType, "provision return record type");
        this.records.add(provisionReturnQueryInspectorRecord);        
        return;
    }
}
