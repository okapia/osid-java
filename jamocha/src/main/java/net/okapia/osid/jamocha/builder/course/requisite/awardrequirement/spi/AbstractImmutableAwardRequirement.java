//
// AbstractImmutableAwardRequirement.java
//
//     Wraps a mutable AwardRequirement to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.awardrequirement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AwardRequirement</code> to hide modifiers. This
 *  wrapper provides an immutized AwardRequirement from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying awardRequirement whose state changes are visible.
 */

public abstract class AbstractImmutableAwardRequirement
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.course.requisite.AwardRequirement {

    private final org.osid.course.requisite.AwardRequirement awardRequirement;


    /**
     *  Constructs a new <code>AbstractImmutableAwardRequirement</code>.
     *
     *  @param awardRequirement the award requirement to immutablize
     *  @throws org.osid.NullArgumentException <code>awardRequirement</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAwardRequirement(org.osid.course.requisite.AwardRequirement awardRequirement) {
        super(awardRequirement);
        this.awardRequirement = awardRequirement;
        return;
    }


    /**
     *  Gets any <code> Requisites </code> that may be substituted in place of 
     *  this <code> AwardRequirement. </code> All <code> Requisites </code> 
     *  must be satisifed to be a substitute for this award requirement. 
     *  Inactive <code> Requisites </code> are not evaluated but if no 
     *  applicable requisite exists, then the alternate requisite is not 
     *  satisifed. 
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.awardRequirement.getAltRequisites());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Award. </code> 
     *
     *  @return the award <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAwardId() {
        return (this.awardRequirement.getAwardId());
    }


    /**
     *  Gets the <code> Award. </code> 
     *
     *  @return the award 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward()
        throws org.osid.OperationFailedException {

        return (this.awardRequirement.getAward());
    }


    /**
     *  Tests if the award must have been earned within the required duration. 
     *
     *  @return <code> true </code> if the award has to be earned within a 
     *          required time, <code> false </code> if it could have been 
     *          completed at any time in the past 
     */

    @OSID @Override
    public boolean hasTimeframe() {
        return (this.awardRequirement.hasTimeframe());
    }


    /**
     *  Gets the timeframe in which the award has to be earned. A negative 
     *  duration indicates the award had to be completed within the specified 
     *  amount of time in the past. A posiitive duration indicates the award 
     *  must be completed within the specified amount of time in the future. A 
     *  zero duration indicates the award must be completed in the current 
     *  term. 
     *
     *  @return the time frame 
     *  @throws org.osid.IllegalStateException <code> hasTimeframe() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeframe() {
        return (this.awardRequirement.getTimeframe());
    }


    /**
     *  Gets the award requirement record corresponding to the given <code> 
     *  AwardRequirement </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> awardRequirementRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(awardRequirementRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  awardRequirementRecordType the type of award requirement 
     *          record to retrieve 
     *  @return the award requirement record 
     *  @throws org.osid.NullArgumentException <code> 
     *          awardRequirementRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(awardRequirementRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.records.AwardRequirementRecord getAwardRequirementRecord(org.osid.type.Type awardRequirementRecordType)
        throws org.osid.OperationFailedException {

        return (this.awardRequirement.getAwardRequirementRecord(awardRequirementRecordType));
    }
}

