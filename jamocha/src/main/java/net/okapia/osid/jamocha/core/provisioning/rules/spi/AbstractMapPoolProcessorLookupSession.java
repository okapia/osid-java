//
// AbstractMapPoolProcessorLookupSession
//
//    A simple framework for providing a PoolProcessor lookup service
//    backed by a fixed collection of pool processors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a PoolProcessor lookup service backed by a
 *  fixed collection of pool processors. The pool processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PoolProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPoolProcessorLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractPoolProcessorLookupSession
    implements org.osid.provisioning.rules.PoolProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.PoolProcessor> poolProcessors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.PoolProcessor>());


    /**
     *  Makes a <code>PoolProcessor</code> available in this session.
     *
     *  @param  poolProcessor a pool processor
     *  @throws org.osid.NullArgumentException <code>poolProcessor<code>
     *          is <code>null</code>
     */

    protected void putPoolProcessor(org.osid.provisioning.rules.PoolProcessor poolProcessor) {
        this.poolProcessors.put(poolProcessor.getId(), poolProcessor);
        return;
    }


    /**
     *  Makes an array of pool processors available in this session.
     *
     *  @param  poolProcessors an array of pool processors
     *  @throws org.osid.NullArgumentException <code>poolProcessors<code>
     *          is <code>null</code>
     */

    protected void putPoolProcessors(org.osid.provisioning.rules.PoolProcessor[] poolProcessors) {
        putPoolProcessors(java.util.Arrays.asList(poolProcessors));
        return;
    }


    /**
     *  Makes a collection of pool processors available in this session.
     *
     *  @param  poolProcessors a collection of pool processors
     *  @throws org.osid.NullArgumentException <code>poolProcessors<code>
     *          is <code>null</code>
     */

    protected void putPoolProcessors(java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessor> poolProcessors) {
        for (org.osid.provisioning.rules.PoolProcessor poolProcessor : poolProcessors) {
            this.poolProcessors.put(poolProcessor.getId(), poolProcessor);
        }

        return;
    }


    /**
     *  Removes a PoolProcessor from this session.
     *
     *  @param  poolProcessorId the <code>Id</code> of the pool processor
     *  @throws org.osid.NullArgumentException <code>poolProcessorId<code> is
     *          <code>null</code>
     */

    protected void removePoolProcessor(org.osid.id.Id poolProcessorId) {
        this.poolProcessors.remove(poolProcessorId);
        return;
    }


    /**
     *  Gets the <code>PoolProcessor</code> specified by its <code>Id</code>.
     *
     *  @param  poolProcessorId <code>Id</code> of the <code>PoolProcessor</code>
     *  @return the poolProcessor
     *  @throws org.osid.NotFoundException <code>poolProcessorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>poolProcessorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessor getPoolProcessor(org.osid.id.Id poolProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.PoolProcessor poolProcessor = this.poolProcessors.get(poolProcessorId);
        if (poolProcessor == null) {
            throw new org.osid.NotFoundException("poolProcessor not found: " + poolProcessorId);
        }

        return (poolProcessor);
    }


    /**
     *  Gets all <code>PoolProcessors</code>. In plenary mode, the returned
     *  list contains all known poolProcessors or an error
     *  results. Otherwise, the returned list may contain only those
     *  poolProcessors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>PoolProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorList getPoolProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolprocessor.ArrayPoolProcessorList(this.poolProcessors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.poolProcessors.clear();
        super.close();
        return;
    }
}
