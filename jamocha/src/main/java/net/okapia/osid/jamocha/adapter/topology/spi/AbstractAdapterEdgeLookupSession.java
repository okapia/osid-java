//
// AbstractAdapterEdgeLookupSession.java
//
//    An Edge lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Edge lookup session adapter.
 */

public abstract class AbstractAdapterEdgeLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.topology.EdgeLookupSession {

    private final org.osid.topology.EdgeLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterEdgeLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEdgeLookupSession(org.osid.topology.EdgeLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Graph/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Graph Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.session.getGraphId());
    }


    /**
     *  Gets the {@code Graph} associated with this session.
     *
     *  @return the {@code Graph} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGraph());
    }


    /**
     *  Tests if this user can perform {@code Edge} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupEdges() {
        return (this.session.canLookupEdges());
    }


    /**
     *  A complete view of the {@code Edge} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEdgeView() {
        this.session.useComparativeEdgeView();
        return;
    }


    /**
     *  A complete view of the {@code Edge} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEdgeView() {
        this.session.usePlenaryEdgeView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edges in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.session.useFederatedGraphView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.session.useIsolatedGraphView();
        return;
    }
    

    /**
     *  Only edges whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveEdgeView() {
        this.session.useEffectiveEdgeView();
        return;
    }
    

    /**
     *  All edges of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveEdgeView() {
        this.session.useAnyEffectiveEdgeView();
        return;
    }

     
    /**
     *  Gets the {@code Edge} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Edge} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Edge} and
     *  retained for compatibility.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param edgeId {@code Id} of the {@code Edge}
     *  @return the edge
     *  @throws org.osid.NotFoundException {@code edgeId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code edgeId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Edge getEdge(org.osid.id.Id edgeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdge(edgeId));
    }


    /**
     *  Gets an {@code EdgeList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  edges specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Edges} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param  edgeIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Edge} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code edgeIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByIds(org.osid.id.IdList edgeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByIds(edgeIds));
    }


    /**
     *  Gets an {@code EdgeList} corresponding to the given
     *  edge genus {@code Type} which does not include
     *  edges of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @return the returned {@code Edge} list
     *  @throws org.osid.NullArgumentException
     *          {@code edgeGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByGenusType(edgeGenusType));
    }


    /**
     *  Gets an {@code EdgeList} corresponding to the given
     *  edge genus {@code Type} and include any additional
     *  edges with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @return the returned {@code Edge} list
     *  @throws org.osid.NullArgumentException
     *          {@code edgeGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByParentGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByParentGenusType(edgeGenusType));
    }


    /**
     *  Gets an {@code EdgeList} containing the given
     *  edge record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param  edgeRecordType an edge record type 
     *  @return the returned {@code Edge} list
     *  @throws org.osid.NullArgumentException
     *          {@code edgeRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByRecordType(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByRecordType(edgeRecordType));
    }


    /**
     *  Gets an {@code EdgeList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *  
     *  In active mode, edges are returned that are currently
     *  active. In any status mode, active and inactive edges
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Edge} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.EdgeList getEdgesOnDate(org.osid.calendaring.DateTime from, 
                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesOnDate(from, to));
    }
        

    /**
     *  Gets an {@code EdgeList} of a genus type effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Edge} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code edgeGenusType,
     *          from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeOnDate(org.osid.type.Type edgeGenusType, 
                                                                org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByGenusTypeOnDate(edgeGenusType, from, to));
    }


    /**
     *  Gets a list of edges corresponding to a source node {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the {@code Id} of the source node
     *  @return the returned {@code EdgeList}
     *  @throws org.osid.NullArgumentException {@code sourceNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForSourceNode(org.osid.id.Id sourceNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesForSourceNode(sourceNodeId));
    }


    /**
     *  Gets a list of edges corresponding to a source node {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the {@code Id} of the source node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EdgeList}
     *  @throws org.osid.NullArgumentException {@code sourceNodeId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForSourceNodeOnDate(org.osid.id.Id sourceNodeId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesForSourceNodeOnDate(sourceNodeId, from, to));
    }


    /**
     *  Gets an {@code EdgeList} corresponding to the given source
     *  node {@code Id} and edge genus {@code Type} and includes any
     *  genus types derived from the given genus type.
     *  
     *  In plenary mode, the returned list contains all of the edges
     *  corresponding to the given peer, including duplicates, or an
     *  error results if an edge is inaccessible. Otherwise,
     *  inaccessible {@code Edges} may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId a node {@code Id} 
     *  @param  edgeGenusType an edge genus type 
     *  @return a list of edges 
     *  @throws org.osid.NullArgumentException {@code sourceNodeId} or
     *          {@code edgeGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForSourceNode(org.osid.id.Id sourceNodeId, 
                                                                       org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByGenusTypeForSourceNode(sourceNodeId, edgeGenusType));
    }


    /**
     *  Gets an {@code EdgeList} with the given genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  nodeId a node {@code Id} 
     *  @param  edgeGenusType an edge genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return a list of edges 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code sourceNodeId,
     *          edgeGenusType, from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForSourceNodeOnDate(org.osid.id.Id nodeId, 
                                                                             org.osid.type.Type edgeGenusType, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByGenusTypeForSourceNodeOnDate(nodeId, edgeGenusType, from, to));
    }


    /**
     *  Gets a list of edges corresponding to a destination node
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId the {@code Id} of the destination node
     *  @return the returned {@code EdgeList}
     *  @throws org.osid.NullArgumentException {@code destinationNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForDestinationNode(org.osid.id.Id destinationNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesForDestinationNode(destinationNodeId));
    }


    /**
     *  Gets a list of edges corresponding to a destination node
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId the {@code Id} of the destination node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EdgeList}
     *  @throws org.osid.NullArgumentException {@code destinationNodeId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForDestinationNodeOnDate(org.osid.id.Id destinationNodeId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesForDestinationNodeOnDate(destinationNodeId, from, to));
    }


    /**
     *  Gets an {@code EdgeList} corresponding to the given
     *  destination node {@code Id} and edge genus {@code Type} and
     *  includes any genus types derived from the given genus type.
     *  
     *  In plenary mode, the returned list contains all of the edges
     *  corresponding to the given peer, including duplicates, or an
     *  error results if an edge is inaccessible. Otherwise,
     *  inaccessible {@code Edges} may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId a node {@code Id} 
     *  @param  edgeGenusType an edge genus type 
     *  @return a list of edges 
     *  @throws org.osid.NullArgumentException {@code
     *         destinationNodeId} or {@code edgeGenusType} is {@code
     *         null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForDestinationNode(org.osid.id.Id destinationNodeId, 
                                                                            org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByGenusTypeForDestinationNode(destinationNodeId, edgeGenusType));
    }


    /**
     *  Gets an {@code EdgeList} with the given genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId a node {@code Id} 
     *  @param  edgeGenusType an edge genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return a list of edges 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code
     *          destinationNodeId, edgeGenusType, from} or {@code to}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForDestinationNodeOnDate(org.osid.id.Id destinationNodeId, 
                                                                                  org.osid.type.Type edgeGenusType, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByGenusTypeForDestinationNodeOnDate(destinationNodeId, edgeGenusType, from, to));
    }


    /**
     *  Gets a list of edges corresponding to source node and
     *  destination node {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the {@code Id} of the source node
     *  @param  destinationNodeId the {@code Id} of the destination node
     *  @return the returned {@code EdgeList}
     *  @throws org.osid.NullArgumentException {@code sourceNodeId},
     *          {@code destinationNodeId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForNodes(org.osid.id.Id sourceNodeId,
                                                       org.osid.id.Id destinationNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesForNodes(sourceNodeId, destinationNodeId));
    }


    /**
     *  Gets a list of edges corresponding to source node and
     *  destination node {@code Ids} and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId the {@code Id} of the destination node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EdgeList}
     *  @throws org.osid.NullArgumentException {@code sourceNodeId},
     *          {@code destinationNodeId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForNodesOnDate(org.osid.id.Id sourceNodeId,
                                                             org.osid.id.Id destinationNodeId,
                                                             org.osid.calendaring.DateTime from,
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesForNodesOnDate(sourceNodeId, destinationNodeId, from, to));
    }


    /**
     *  Gets an EdgeList corresponding to the given peer {@code Ids}
     *  and edge genus type and includes and genus types derived from
     *  the given genus type.
     *  
     *  In plenary mode, the returned list contains all of the edges
     *  corresponding to the given peer, including duplicates, or an
     *  error results if an edge is inaccessible. Otherwise,
     *  inaccessible {@code Edges} may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId a node {@code Id} 
     *  @param  destinationNodeId a node {@code Id} 
     *  @param  edgeGenusType an edge genus type 
     *  @return the edges 
     *  @throws org.osid.NullArgumentException {@code sourceNodeId,
     *          destinationNodeId}, or {@code edgeGenusType} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForNodes(org.osid.id.Id sourceNodeId, 
                                                                  org.osid.id.Id destinationNodeId, 
                                                                  org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByGenusTypeForNodes(sourceNodeId, destinationNodeId, edgeGenusType));
    }


    /**
     *  Gets an {@code EdgeList} of the given genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range. 
     *  
     *  In plenary mode, the returned list contains all known 
     *  edges or an error results. Otherwise, the returned list may contain 
     *  only those edges that are accessible through this session. 
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId a node {@code Id} 
     *  @param  destinationNodeId a node {@code Id} 
     *  @param  edgeGenusType an edge genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return a list of edges 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code sourceNodeId,
     *          destinationNodeId, edgeGenusType, from} or {@code to}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForNodesOnDate(org.osid.id.Id sourceNodeId, 
                                                                        org.osid.id.Id destinationNodeId, 
                                                                        org.osid.type.Type edgeGenusType, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgesByGenusTypeForNodesOnDate(sourceNodeId, destinationNodeId, edgeGenusType, from, to));
    }
    

    /**
     *  Gets all {@code Edges}. 
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Edges} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdges()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdges());
    }
}
