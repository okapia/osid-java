//
// AbstractNodeSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.node.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractNodeSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.topology.NodeSearchResults {

    private org.osid.topology.NodeList nodes;
    private final org.osid.topology.NodeQueryInspector inspector;
    private final java.util.Collection<org.osid.topology.records.NodeSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractNodeSearchResults.
     *
     *  @param nodes the result set
     *  @param nodeQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>nodes</code>
     *          or <code>nodeQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractNodeSearchResults(org.osid.topology.NodeList nodes,
                                            org.osid.topology.NodeQueryInspector nodeQueryInspector) {
        nullarg(nodes, "nodes");
        nullarg(nodeQueryInspector, "node query inspectpr");

        this.nodes = nodes;
        this.inspector = nodeQueryInspector;

        return;
    }


    /**
     *  Gets the node list resulting from a search.
     *
     *  @return a node list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodes() {
        if (this.nodes == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.topology.NodeList nodes = this.nodes;
        this.nodes = null;
	return (nodes);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.topology.NodeQueryInspector getNodeQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  node search record <code> Type. </code> This method must
     *  be used to retrieve a node implementing the requested
     *  record.
     *
     *  @param nodeSearchRecordType a node search 
     *         record type 
     *  @return the node search
     *  @throws org.osid.NullArgumentException
     *          <code>nodeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(nodeSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.NodeSearchResultsRecord getNodeSearchResultsRecord(org.osid.type.Type nodeSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.topology.records.NodeSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(nodeSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(nodeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record node search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addNodeRecord(org.osid.topology.records.NodeSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "node record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
