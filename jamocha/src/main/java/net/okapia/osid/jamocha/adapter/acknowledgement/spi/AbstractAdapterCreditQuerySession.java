//
// AbstractQueryCreditLookupSession.java
//
//    A CreditQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CreditQuerySession adapter.
 */

public abstract class AbstractAdapterCreditQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.acknowledgement.CreditQuerySession {

    private final org.osid.acknowledgement.CreditQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterCreditQuerySession.
     *
     *  @param session the underlying credit query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCreditQuerySession(org.osid.acknowledgement.CreditQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeBilling</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeBilling Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBillingId() {
        return (this.session.getBillingId());
    }


    /**
     *  Gets the {@codeBilling</code> associated with this 
     *  session.
     *
     *  @return the {@codeBilling</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Billing getBilling()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBilling());
    }


    /**
     *  Tests if this user can perform {@codeCredit</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchCredits() {
        return (this.session.canSearchCredits());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include credits in billings which are children
     *  of this billing in the billing hierarchy.
     */

    @OSID @Override
    public void useFederatedBillingView() {
        this.session.useFederatedBillingView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this billing only.
     */
    
    @OSID @Override
    public void useIsolatedBillingView() {
        this.session.useIsolatedBillingView();
        return;
    }
    
      
    /**
     *  Gets a credit query. The returned query will not have an
     *  extension query.
     *
     *  @return the credit query 
     */
      
    @OSID @Override
    public org.osid.acknowledgement.CreditQuery getCreditQuery() {
        return (this.session.getCreditQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  creditQuery the credit query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code creditQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code creditQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByQuery(org.osid.acknowledgement.CreditQuery creditQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getCreditsByQuery(creditQuery));
    }
}
