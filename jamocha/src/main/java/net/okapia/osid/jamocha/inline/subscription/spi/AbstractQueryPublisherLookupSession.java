//
// AbstractQueryPublisherLookupSession.java
//
//    An inline adapter that maps a PublisherLookupSession to
//    a PublisherQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PublisherLookupSession to
 *  a PublisherQuerySession.
 */

public abstract class AbstractQueryPublisherLookupSession
    extends net.okapia.osid.jamocha.subscription.spi.AbstractPublisherLookupSession
    implements org.osid.subscription.PublisherLookupSession {

    private final org.osid.subscription.PublisherQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPublisherLookupSession.
     *
     *  @param querySession the underlying publisher query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPublisherLookupSession(org.osid.subscription.PublisherQuerySession querySession) {
        nullarg(querySession, "publisher query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Publisher</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPublishers() {
        return (this.session.canSearchPublishers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Publisher</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Publisher</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Publisher</code> and
     *  retained for compatibility.
     *
     *  @param  publisherId <code>Id</code> of the
     *          <code>Publisher</code>
     *  @return the publisher
     *  @throws org.osid.NotFoundException <code>publisherId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>publisherId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.PublisherQuery query = getQuery();
        query.matchId(publisherId, true);
        org.osid.subscription.PublisherList publishers = this.session.getPublishersByQuery(query);
        if (publishers.hasNext()) {
            return (publishers.getNextPublisher());
        } 
        
        throw new org.osid.NotFoundException(publisherId + " not found");
    }


    /**
     *  Gets a <code>PublisherList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  publishers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Publishers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  publisherIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Publisher</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>publisherIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByIds(org.osid.id.IdList publisherIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.PublisherQuery query = getQuery();

        try (org.osid.id.IdList ids = publisherIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPublishersByQuery(query));
    }


    /**
     *  Gets a <code>PublisherList</code> corresponding to the given
     *  publisher genus <code>Type</code> which does not include
     *  publishers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  publisherGenusType a publisher genus type 
     *  @return the returned <code>Publisher</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>publisherGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByGenusType(org.osid.type.Type publisherGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.PublisherQuery query = getQuery();
        query.matchGenusType(publisherGenusType, true);
        return (this.session.getPublishersByQuery(query));
    }


    /**
     *  Gets a <code>PublisherList</code> corresponding to the given
     *  publisher genus <code>Type</code> and include any additional
     *  publishers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  publisherGenusType a publisher genus type 
     *  @return the returned <code>Publisher</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>publisherGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByParentGenusType(org.osid.type.Type publisherGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.PublisherQuery query = getQuery();
        query.matchParentGenusType(publisherGenusType, true);
        return (this.session.getPublishersByQuery(query));
    }


    /**
     *  Gets a <code>PublisherList</code> containing the given
     *  publisher record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  publisherRecordType a publisher record type 
     *  @return the returned <code>Publisher</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>publisherRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByRecordType(org.osid.type.Type publisherRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.PublisherQuery query = getQuery();
        query.matchRecordType(publisherRecordType, true);
        return (this.session.getPublishersByQuery(query));
    }


    /**
     *  Gets a <code>PublisherList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known publishers or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  publishers that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Publisher</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.PublisherQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getPublishersByQuery(query));        
    }

    
    /**
     *  Gets all <code>Publishers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Publishers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.PublisherQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPublishersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.subscription.PublisherQuery getQuery() {
        org.osid.subscription.PublisherQuery query = this.session.getPublisherQuery();
        return (query);
    }
}
