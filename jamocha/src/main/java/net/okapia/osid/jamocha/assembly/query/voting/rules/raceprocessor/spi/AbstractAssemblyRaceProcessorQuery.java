//
// AbstractAssemblyRaceProcessorQuery.java
//
//     A RaceProcessorQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.rules.raceprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RaceProcessorQuery that stores terms.
 */

public abstract class AbstractAssemblyRaceProcessorQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidProcessorQuery
    implements org.osid.voting.rules.RaceProcessorQuery,
               org.osid.voting.rules.RaceProcessorQueryInspector,
               org.osid.voting.rules.RaceProcessorSearchOrder {

    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRaceProcessorQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRaceProcessorQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches rules with a maximum winners value between the given range 
     *  inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMaximumWinners(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getMaximumWinnersColumn(), low, high, match);
        return;
    }


    /**
     *  Matches rules with any maximum winner value. 
     *
     *  @param  match <code> true </code> to match rules with a maximum winner 
     *          value, <code> false </code> to match rules with no maximum 
     *          winner value 
     */

    @OSID @Override
    public void matchAnyMaximumWinners(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMaximumWinnersColumn(), match);
        return;
    }


    /**
     *  Clears the maximum winners terms. 
     */

    @OSID @Override
    public void clearMaximumWinnersTerms() {
        getAssembler().clearTerms(getMaximumWinnersColumn());
        return;
    }


    /**
     *  Gets the maximum winners query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMaximumWinnersTerms() {
        return (getAssembler().getCardinalRangeTerms(getMaximumWinnersColumn()));
    }


    /**
     *  Specified a preference for ordering the maximum winners value. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumWinners(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMaximumWinnersColumn(), style);
        return;
    }


    /**
     *  Gets the MaximumWinners column name.
     *
     * @return the column name
     */

    protected String getMaximumWinnersColumn() {
        return ("maximum_winners");
    }


    /**
     *  Matches rules with a minimum percentage to win value between the given 
     *  range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMinimumPercentageToWin(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getMinimumPercentageToWinColumn(), low, high, match);
        return;
    }


    /**
     *  Matches rules with any minimum percentage to win value. 
     *
     *  @param  match <code> true </code> to match rules with a minimum value, 
     *          <code> false </code> to match rules with no minimum value 
     */

    @OSID @Override
    public void matchAnyMinimumPercentageToWin(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMinimumPercentageToWinColumn(), match);
        return;
    }


    /**
     *  Clears the minimum percentage to win terms. 
     */

    @OSID @Override
    public void clearMinimumPercentageToWinTerms() {
        getAssembler().clearTerms(getMinimumPercentageToWinColumn());
        return;
    }


    /**
     *  Gets the minimum percentage to win query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumPercentageToWinTerms() {
        return (getAssembler().getCardinalRangeTerms(getMinimumPercentageToWinColumn()));
    }


    /**
     *  Specified a preference for ordering the minimum percentage to win 
     *  value. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumPercentageToWin(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMinimumPercentageToWinColumn(), style);
        return;
    }


    /**
     *  Gets the MinimumPercentageToWin column name.
     *
     * @return the column name
     */

    protected String getMinimumPercentageToWinColumn() {
        return ("minimum_percentage_to_win");
    }


    /**
     *  Matches rules with a minimum votes to win value between the given 
     *  range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMinimumVotesToWin(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getMinimumVotesToWinColumn(), low, high, match);
        return;
    }


    /**
     *  Matches rules with any minimum votes to win value. 
     *
     *  @param  match <code> true </code> to match rules with a minimum value, 
     *          <code> false </code> to match rules with no minimum value 
     */

    @OSID @Override
    public void matchAnyMinimumVotesToWin(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMinimumVotesToWinColumn(), match);
        return;
    }


    /**
     *  Clears the minimum votes to win terms. 
     */

    @OSID @Override
    public void clearMinimumVotesToWinTerms() {
        getAssembler().clearTerms(getMinimumVotesToWinColumn());
        return;
    }


    /**
     *  Gets the minimum votes to win query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumVotesToWinTerms() {
        return (getAssembler().getCardinalRangeTerms(getMinimumVotesToWinColumn()));
    }


    /**
     *  Specified a preference for ordering the minimum votes to win value. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumVotesToWin(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMinimumVotesToWinColumn(), style);
        return;
    }


    /**
     *  Gets the MinimumVotesToWin column name.
     *
     * @return the column name
     */

    protected String getMinimumVotesToWinColumn() {
        return ("minimum_votes_to_win");
    }


    /**
     *  Matches rules mapped to the race. 
     *
     *  @param  raceId the race <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuledRaceId(org.osid.id.Id raceId, boolean match) {
        getAssembler().addIdTerm(getRuledRaceIdColumn(), raceId, match);
        return;
    }


    /**
     *  Clears the race <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRaceIdTerms() {
        getAssembler().clearTerms(getRuledRaceIdColumn());
        return;
    }


    /**
     *  Gets the race <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRaceIdTerms() {
        return (getAssembler().getIdTerms(getRuledRaceIdColumn()));
    }


    /**
     *  Gets the RuledRaceId column name.
     *
     * @return the column name
     */

    protected String getRuledRaceIdColumn() {
        return ("ruled_race_id");
    }


    /**
     *  Tests if a <code> RaceQuery </code> is available. 
     *
     *  @return <code> true </code> if a race query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRaceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the race query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRaceQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuery getRuledRaceQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRaceQuery() is false");
    }


    /**
     *  Matches any rule mapped to any race. 
     *
     *  @param  match <code> true </code> for rules mapped to any race, <code> 
     *          false </code> to match rules mapped to no race 
     */

    @OSID @Override
    public void matchAnyRuledRace(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledRaceColumn(), match);
        return;
    }


    /**
     *  Clears the race query terms. 
     */

    @OSID @Override
    public void clearRuledRaceTerms() {
        getAssembler().clearTerms(getRuledRaceColumn());
        return;
    }


    /**
     *  Gets the race query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.RaceQueryInspector[] getRuledRaceTerms() {
        return (new org.osid.voting.RaceQueryInspector[0]);
    }


    /**
     *  Gets the RuledRace column name.
     *
     * @return the column name
     */

    protected String getRuledRaceColumn() {
        return ("ruled_race");
    }


    /**
     *  Matches rules mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        getAssembler().addIdTerm(getPollsIdColumn(), pollsId, match);
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        getAssembler().clearTerms(getPollsIdColumn());
        return;
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (getAssembler().getIdTerms(getPollsIdColumn()));
    }


    /**
     *  Gets the PollsId column name.
     *
     * @return the column name
     */

    protected String getPollsIdColumn() {
        return ("polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        getAssembler().clearTerms(getPollsColumn());
        return;
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the Polls column name.
     *
     * @return the column name
     */

    protected String getPollsColumn() {
        return ("polls");
    }


    /**
     *  Tests if this raceProcessor supports the given record
     *  <code>Type</code>.
     *
     *  @param  raceProcessorRecordType a race processor record type 
     *  @return <code>true</code> if the raceProcessorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type raceProcessorRecordType) {
        for (org.osid.voting.rules.records.RaceProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  raceProcessorRecordType the race processor record type 
     *  @return the race processor query record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorQueryRecord getRaceProcessorQueryRecord(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  raceProcessorRecordType the race processor record type 
     *  @return the race processor query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorQueryInspectorRecord getRaceProcessorQueryInspectorRecord(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(raceProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param raceProcessorRecordType the race processor record type
     *  @return the race processor search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorSearchOrderRecord getRaceProcessorSearchOrderRecord(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(raceProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this race processor. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param raceProcessorQueryRecord the race processor query record
     *  @param raceProcessorQueryInspectorRecord the race processor query inspector
     *         record
     *  @param raceProcessorSearchOrderRecord the race processor search order record
     *  @param raceProcessorRecordType race processor record type
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorQueryRecord</code>,
     *          <code>raceProcessorQueryInspectorRecord</code>,
     *          <code>raceProcessorSearchOrderRecord</code> or
     *          <code>raceProcessorRecordTyperaceProcessor</code> is
     *          <code>null</code>
     */
            
    protected void addRaceProcessorRecords(org.osid.voting.rules.records.RaceProcessorQueryRecord raceProcessorQueryRecord, 
                                      org.osid.voting.rules.records.RaceProcessorQueryInspectorRecord raceProcessorQueryInspectorRecord, 
                                      org.osid.voting.rules.records.RaceProcessorSearchOrderRecord raceProcessorSearchOrderRecord, 
                                      org.osid.type.Type raceProcessorRecordType) {

        addRecordType(raceProcessorRecordType);

        nullarg(raceProcessorQueryRecord, "race processor query record");
        nullarg(raceProcessorQueryInspectorRecord, "race processor query inspector record");
        nullarg(raceProcessorSearchOrderRecord, "race processor search odrer record");

        this.queryRecords.add(raceProcessorQueryRecord);
        this.queryInspectorRecords.add(raceProcessorQueryInspectorRecord);
        this.searchOrderRecords.add(raceProcessorSearchOrderRecord);
        
        return;
    }
}
