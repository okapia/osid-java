//
// AbstractContact.java
//
//     Defines a Contact.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Contact</code>.
 */

public abstract class AbstractContact
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.contact.Contact {

    private org.osid.id.Id referenceId;
    private org.osid.resource.Resource addressee;
    private org.osid.contact.Address address;

    private final java.util.Collection<org.osid.contact.records.ContactRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the reference. 
     *
     *  @return the reference <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        return (this.referenceId);
    }


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    protected void setReferenceId(org.osid.id.Id referenceId) {
        nullarg(referenceId, "reference Id");
        this.referenceId = referenceId;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the addressee. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAddresseeId() {
        return (this.addressee.getId());
    }


    /**
     *  Gets the addressee. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getAddressee()
        throws org.osid.OperationFailedException {

        return (this.addressee);
    }


    /**
     *  Sets the addressee.
     *
     *  @param addressee an addressee
     *  @throws org.osid.NullArgumentException
     *          <code>addressee</code> is <code>null</code>
     */

    protected void setAddressee(org.osid.resource.Resource addressee) {
        nullarg(addressee, "addressee");
        this.addressee = addressee;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the subscriber's address. 
     *
     *  @return the subscriber <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAddressId() {
        return (this.address.getId());
    }


    /**
     *  Gets the subscriber's address. 
     *
     *  @return the subscriber's address. 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress()
        throws org.osid.OperationFailedException {

        return (this.address);
    }


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @throws org.osid.NullArgumentException <code>address</code> is
     *          <code>null</code>
     */

    protected void setAddress(org.osid.contact.Address address) {
        nullarg(address, "address");
        this.address = address;
        return;
    }


    /**
     *  Tests if this contact supports the given record
     *  <code>Type</code>.
     *
     *  @param  contactRecordType a contact record type 
     *  @return <code>true</code> if the contactRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type contactRecordType) {
        for (org.osid.contact.records.ContactRecord record : this.records) {
            if (record.implementsRecordType(contactRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Contact</code> record <code>Type</code>.
     *
     *  @param contactRecordType the contact record type
     *  @return the contact record 
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.ContactRecord getContactRecord(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.ContactRecord record : this.records) {
            if (record.implementsRecordType(contactRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactRecordType + " is not supported");
    }


    /**
     *  Adds a record to this contact. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param contactRecord the contact record
     *  @param contactRecordType contact record type
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecord</code> or
     *          <code>contactRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addContactRecord(org.osid.contact.records.ContactRecord contactRecord, 
                                    org.osid.type.Type contactRecordType) {

        nullarg(contactRecord, "contact record");
        addRecordType(contactRecordType);
        this.records.add(contactRecord);
        
        return;
    }
}
