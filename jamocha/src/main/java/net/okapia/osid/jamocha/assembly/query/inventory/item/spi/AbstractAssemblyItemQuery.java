//
// AbstractAssemblyItemQuery.java
//
//     An ItemQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inventory.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ItemQuery that stores terms.
 */

public abstract class AbstractAssemblyItemQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.inventory.ItemQuery,
               org.osid.inventory.ItemQueryInspector,
               org.osid.inventory.ItemSearchOrder {

    private final java.util.Collection<org.osid.inventory.records.ItemQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.ItemQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.ItemSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyItemQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyItemQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the stock <code> Id </code> for this query. 
     *
     *  @param  stockId the stock <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStockId(org.osid.id.Id stockId, boolean match) {
        getAssembler().addIdTerm(getStockIdColumn(), stockId, match);
        return;
    }


    /**
     *  Clears the stock <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStockIdTerms() {
        getAssembler().clearTerms(getStockIdColumn());
        return;
    }


    /**
     *  Gets the stock <code> Id </code> query terms. 
     *
     *  @return the stock <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStockIdTerms() {
        return (getAssembler().getIdTerms(getStockIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the stock. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStock(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStockColumn(), style);
        return;
    }


    /**
     *  Gets the StockId column name.
     *
     * @return the column name
     */

    protected String getStockIdColumn() {
        return ("stock_id");
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stocjk. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getStockQuery() {
        throw new org.osid.UnimplementedException("supportsStockQuery() is false");
    }


    /**
     *  Matches items that have any stock. 
     *
     *  @param  match <code> true </code> to match items with any stock, 
     *          <code> false </code> to match items with no stock 
     */

    @OSID @Override
    public void matchAnyStock(boolean match) {
        getAssembler().addIdWildcardTerm(getStockColumn(), match);
        return;
    }


    /**
     *  Clears the stock terms. 
     */

    @OSID @Override
    public void clearStockTerms() {
        getAssembler().clearTerms(getStockColumn());
        return;
    }


    /**
     *  Gets the stock query terms. 
     *
     *  @return the stock query terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Tests if a <code> StockSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a stock search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a stock. 
     *
     *  @return the stock search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchOrder getStockSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStockSearchOrder() is false");
    }


    /**
     *  Gets the Stock column name.
     *
     * @return the column name
     */

    protected String getStockColumn() {
        return ("stock");
    }


    /**
     *  Matches a property identification number. 
     *
     *  @param  property a property number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> property </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPropertyTag(String property, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        getAssembler().addStringTerm(getPropertyTagColumn(), property, stringMatchType, match);
        return;
    }


    /**
     *  Matches items that have any property tag. 
     *
     *  @param  match <code> true </code> to match items with any property 
     *          tag, <code> false </code> to match items with no property tag 
     */

    @OSID @Override
    public void matchAnyPropertyTag(boolean match) {
        getAssembler().addStringWildcardTerm(getPropertyTagColumn(), match);
        return;
    }


    /**
     *  Clears the property tag terms. 
     */

    @OSID @Override
    public void clearPropertyTagTerms() {
        getAssembler().clearTerms(getPropertyTagColumn());
        return;
    }


    /**
     *  Gets the property tag query terms. 
     *
     *  @return the property tag query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPropertyTagTerms() {
        return (getAssembler().getStringTerms(getPropertyTagColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the property 
     *  tag. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPropertyTag(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPropertyTagColumn(), style);
        return;
    }


    /**
     *  Gets the PropertyTag column name.
     *
     * @return the column name
     */

    protected String getPropertyTagColumn() {
        return ("property_tag");
    }


    /**
     *  Matches a serial number. 
     *
     *  @param  serial a serial number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> serial </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSerialNumber(String serial, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        getAssembler().addStringTerm(getSerialNumberColumn(), serial, stringMatchType, match);
        return;
    }


    /**
     *  Matches items that have any serial number. 
     *
     *  @param  match <code> true </code> to match items with any serial 
     *          number, <code> false </code> to match items with no serial 
     *          number 
     */

    @OSID @Override
    public void matchAnySerialNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getSerialNumberColumn(), match);
        return;
    }


    /**
     *  Clears the serial number terms. 
     */

    @OSID @Override
    public void clearSerialNumberTerms() {
        getAssembler().clearTerms(getSerialNumberColumn());
        return;
    }


    /**
     *  Gets the serial number query terms. 
     *
     *  @return the serial number query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSerialNumberTerms() {
        return (getAssembler().getStringTerms(getSerialNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the serial 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySerialNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSerialNumberColumn(), style);
        return;
    }


    /**
     *  Gets the SerialNumber column name.
     *
     * @return the column name
     */

    protected String getSerialNumberColumn() {
        return ("serial_number");
    }


    /**
     *  Matches a location description. 
     *
     *  @param  location a location string 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        getAssembler().addStringTerm(getLocationDescriptionColumn(), location, stringMatchType, match);
        return;
    }


    /**
     *  Matches items that have any location description. 
     *
     *  @param  match <code> true </code> to match items with any location 
     *          string, <code> false </code> to match items with no location 
     *          string 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        getAssembler().addStringWildcardTerm(getLocationDescriptionColumn(), match);
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        getAssembler().clearTerms(getLocationDescriptionColumn());
        return;
    }


    /**
     *  Gets the location description query terms. 
     *
     *  @return the location description query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLocationDescriptionTerms() {
        return (getAssembler().getStringTerms(getLocationDescriptionColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the location 
     *  description. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocationDescription(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLocationDescriptionColumn(), style);
        return;
    }


    /**
     *  Gets the LocationDescription column name.
     *
     * @return the column name
     */

    protected String getLocationDescriptionColumn() {
        return ("location_description");
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        getAssembler().clearTerms(getLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the location <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (getAssembler().getIdTerms(getLocationIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the location. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocation(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLocationColumn(), style);
        return;
    }


    /**
     *  Gets the LocationId column name.
     *
     * @return the column name
     */

    protected String getLocationIdColumn() {
        return ("location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches items that have any location. 
     *
     *  @param  match <code> true </code> to match items with any location, 
     *          <code> false </code> to match items with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getLocationColumn(), match);
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        getAssembler().clearTerms(getLocationColumn());
        return;
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the location query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Tests if a <code> LocationSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a location search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the location order for a stock. 
     *
     *  @return the location search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchOrder getLocationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLocationSearchOrder() is false");
    }


    /**
     *  Gets the Location column name.
     *
     * @return the column name
     */

    protected String getLocationColumn() {
        return ("location");
    }


    /**
     *  Sets the item <code> Id </code> for this query to match items part of 
     *  the given item. 
     *
     *  @param  itemId the item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the containing 
     *  item. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getItemColumn(), style);
        return;
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if a item query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches items that have any item. 
     *
     *  @param  match <code> true </code> to match items part of any item, 
     *          <code> false </code> to match items psrt of no item 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.inventory.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.inventory.ItemQueryInspector[0]);
    }


    /**
     *  Tests if an <code> ItemSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an item search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the item order for a stock. 
     *
     *  @return the location search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSearchOrder getItemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsItemSearchOrder() is false");
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match items 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        getAssembler().addIdTerm(getWarehouseIdColumn(), warehouseId, match);
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        getAssembler().clearTerms(getWarehouseIdColumn());
        return;
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (getAssembler().getIdTerms(getWarehouseIdColumn()));
    }


    /**
     *  Gets the WarehouseId column name.
     *
     * @return the column name
     */

    protected String getWarehouseIdColumn() {
        return ("warehouse_id");
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        getAssembler().clearTerms(getWarehouseColumn());
        return;
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }


    /**
     *  Gets the Warehouse column name.
     *
     * @return the column name
     */

    protected String getWarehouseColumn() {
        return ("warehouse");
    }


    /**
     *  Tests if this item supports the given record
     *  <code>Type</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return <code>true</code> if the itemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type itemRecordType) {
        for (org.osid.inventory.records.ItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item query record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ItemQueryRecord getItemQueryRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ItemQueryInspectorRecord getItemQueryInspectorRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ItemQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param itemRecordType the item record type
     *  @return the item search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ItemSearchOrderRecord getItemSearchOrderRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ItemSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this item. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param itemQueryRecord the item query record
     *  @param itemQueryInspectorRecord the item query inspector
     *         record
     *  @param itemSearchOrderRecord the item search order record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException
     *          <code>itemQueryRecord</code>,
     *          <code>itemQueryInspectorRecord</code>,
     *          <code>itemSearchOrderRecord</code> or
     *          <code>itemRecordTypeitem</code> is
     *          <code>null</code>
     */
            
    protected void addItemRecords(org.osid.inventory.records.ItemQueryRecord itemQueryRecord, 
                                      org.osid.inventory.records.ItemQueryInspectorRecord itemQueryInspectorRecord, 
                                      org.osid.inventory.records.ItemSearchOrderRecord itemSearchOrderRecord, 
                                      org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);

        nullarg(itemQueryRecord, "item query record");
        nullarg(itemQueryInspectorRecord, "item query inspector record");
        nullarg(itemSearchOrderRecord, "item search odrer record");

        this.queryRecords.add(itemQueryRecord);
        this.queryInspectorRecords.add(itemQueryInspectorRecord);
        this.searchOrderRecords.add(itemSearchOrderRecord);
        
        return;
    }
}
