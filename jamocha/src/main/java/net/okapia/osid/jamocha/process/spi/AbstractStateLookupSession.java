//
// AbstractStateLookupSession.java
//
//    A starter implementation framework for providing a State
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a State
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getStates(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractStateLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.process.StateLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.process.Process process = new net.okapia.osid.jamocha.nil.process.process.UnknownProcess();
    

    /**
     *  Gets the <code>Process/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Process Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProcessId() {
        return (this.process.getId());
    }


    /**
     *  Gets the <code>Process</code> associated with this 
     *  session.
     *
     *  @return the <code>Process</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.Process getProcess()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.process);
    }


    /**
     *  Sets the <code>Process</code>.
     *
     *  @param  process the process for this session
     *  @throws org.osid.NullArgumentException <code>process</code>
     *          is <code>null</code>
     */

    protected void setProcess(org.osid.process.Process process) {
        nullarg(process, "process");
        this.process = process;
        return;
    }


    /**
     *  Tests if this user can perform <code>State</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStates() {
        return (true);
    }


    /**
     *  A complete view of the <code>State</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStateView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>State</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStateView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include states in processes which are children
     *  of this process in the process hierarchy.
     */

    @OSID @Override
    public void useFederatedProcessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this process only.
     */

    @OSID @Override
    public void useIsolatedProcessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>State</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>State</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>State</code> and
     *  retained for compatibility.
     *
     *  @param  stateId <code>Id</code> of the
     *          <code>State</code>
     *  @return the state
     *  @throws org.osid.NotFoundException <code>stateId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stateId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.State getState(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.process.StateList states = getStates()) {
            while (states.hasNext()) {
                org.osid.process.State state = states.getNextState();
                if (state.getId().equals(stateId)) {
                    return (state);
                }
            }
        } 

        throw new org.osid.NotFoundException(stateId + " not found");
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  states specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>States</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getStates()</code>.
     *
     *  @param  stateIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>stateIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByIds(org.osid.id.IdList stateIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.process.State> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = stateIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getState(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("state " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.process.state.LinkedStateList(ret));
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  state genus <code>Type</code> which does not include
     *  states of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getStates()</code>.
     *
     *  @param  stateGenusType a state genus type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByGenusType(org.osid.type.Type stateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.process.state.StateGenusFilterList(getStates(), stateGenusType));
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  state genus <code>Type</code> and include any additional
     *  states with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStates()</code>.
     *
     *  @param  stateGenusType a state genus type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByParentGenusType(org.osid.type.Type stateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStatesByGenusType(stateGenusType));
    }


    /**
     *  Gets a <code>StateList</code> containing the given
     *  state record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStates()</code>.
     *
     *  @param  stateRecordType a state record type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByRecordType(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.process.state.StateRecordFilterList(getStates(), stateRecordType));
    }


    /**
     *  Gets all <code>States</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>States</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.process.StateList getStates()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets the next valid states for the given state. 
     *
     *  @param  stateId a state <code>Id</code> 
     *  @return the valid next <code>States</code> 
     *  @throws org.osid.NotFoundException <code>stateId</code> is not found 
     *  @throws org.osid.NullArgumentException <code>stateId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.process.StateList getValidNextStates(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the state list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of states
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.process.StateList filterStatesOnViews(org.osid.process.StateList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
