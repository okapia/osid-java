//
// AbstractAssemblyOffsetEventQuery.java
//
//     An OffsetEventQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OffsetEventQuery that stores terms.
 */

public abstract class AbstractAssemblyOffsetEventQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.calendaring.OffsetEventQuery,
               org.osid.calendaring.OffsetEventQueryInspector,
               org.osid.calendaring.OffsetEventSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.OffsetEventQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.OffsetEventQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.OffsetEventSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyOffsetEventQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOffsetEventQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches a fixed start time between the given range inclusive. 
     *
     *  @param  from the start of the range 
     *  @param  to the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchFixedStartTime(org.osid.calendaring.DateTime from, 
                                    org.osid.calendaring.DateTime to, 
                                    boolean match) {
        getAssembler().addDateTimeRangeTerm(getFixedStartTimeColumn(), from, to, match);
        return;
    }


    /**
     *  Matches events with fixed start times. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyFixedStartTime(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getFixedStartTimeColumn(), match);
        return;
    }


    /**
     *  Clears the fixed start time terms. 
     */

    @OSID @Override
    public void clearFixedStartTimeTerms() {
        getAssembler().clearTerms(getFixedStartTimeColumn());
        return;
    }


    /**
     *  Gets the fixed start time terms. 
     *
     *  @return the fixed start time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getFixedStartTimeTerms() {
        return (getAssembler().getDateTimeRangeTerms(getFixedStartTimeColumn()));
    }


    /**
     *  Specified a preference for ordering results by the fixed start time. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedStartTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFixedStartTimeColumn(), style);
        return;
    }


    /**
     *  Gets the FixedStartTime column name.
     *
     * @return the column name
     */

    protected String getFixedStartTimeColumn() {
        return ("fixed_start_time");
    }


    /**
     *  Sets the start reference event <code> Id </code> for this query. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStartReferenceEventId(org.osid.id.Id eventId, 
                                           boolean match) {
        getAssembler().addIdTerm(getStartReferenceEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the start reference event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStartReferenceEventIdTerms() {
        getAssembler().clearTerms(getStartReferenceEventIdColumn());
        return;
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStartReferenceEventIdTerms() {
        return (getAssembler().getIdTerms(getStartReferenceEventIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the starting reference 
     *  event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartReferenceEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStartReferenceEventColumn(), style);
        return;
    }


    /**
     *  Gets the StartReferenceEventId column name.
     *
     * @return the column name
     */

    protected String getStartReferenceEventIdColumn() {
        return ("start_reference_event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying start 
     *  reference event terms. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartReferenceEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for the start reference event. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartReferenceEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getStartReferenceEventQuery() {
        throw new org.osid.UnimplementedException("supportsStartReferenceEventQuery() is false");
    }


    /**
     *  Matches offset events with any starting reference event. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyStartReferenceEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getStartReferenceEventColumn(), match);
        return;
    }


    /**
     *  Clears the start reference event terms. 
     */

    @OSID @Override
    public void clearStartReferenceEventTerms() {
        getAssembler().clearTerms(getStartReferenceEventColumn());
        return;
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getStartReferenceEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Tests if an <code> EventSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an event search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartReferenceEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an event. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartReferenceEventSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getStartReferenceEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStartReferenceEventSearchOrder() is false");
    }


    /**
     *  Gets the StartReferenceEvent column name.
     *
     * @return the column name
     */

    protected String getStartReferenceEventColumn() {
        return ("start_reference_event");
    }


    /**
     *  Matches a fixed offset amount between the given range inclusive. 
     *
     *  @param  from the start of the range 
     *  @param  to the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchFixedStartOffset(org.osid.calendaring.Duration from, 
                                      org.osid.calendaring.Duration to, 
                                      boolean match) {
        getAssembler().addDurationRangeTerm(getFixedStartOffsetColumn(), from, to, match);
        return;
    }


    /**
     *  Matches fixed offset events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyFixedStartOffset(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getFixedStartOffsetColumn(), match);
        return;
    }


    /**
     *  Clears the fixed offset terms. 
     */

    @OSID @Override
    public void clearFixedStartOffsetTerms() {
        getAssembler().clearTerms(getFixedStartOffsetColumn());
        return;
    }


    /**
     *  Gets the fixed offset terms. 
     *
     *  @return the fixed offset terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getFixedStartOffsetTerms() {
        return (getAssembler().getIntegerRangeTerms(getFixedStartOffsetColumn()));
    }


    /**
     *  Specified a preference for ordering results by the fixed offset. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedStartOffset(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFixedStartOffsetColumn(), style);
        return;
    }


    /**
     *  Gets the FixedStartOffset column name.
     *
     * @return the column name
     */

    protected String getFixedStartOffsetColumn() {
        return ("fixed_start_offset");
    }


    /**
     *  Matches a relative weekday offset amount between the given range 
     *  inclusive. 
     *
     *  @param  low the start of the range 
     *  @param  high the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRelativeWeekdayStartOffset(long low, long high, 
                                                boolean match) {
        getAssembler().addIntegerRangeTerm(getRelativeWeekdayStartOffsetColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the relative weekday offset terms. 
     */

    @OSID @Override
    public void clearRelativeWeekdayStartOffsetTerms() {
        getAssembler().clearTerms(getRelativeWeekdayStartOffsetColumn());
        return;
    }


    /**
     *  Gets the relative weekday offset terms. 
     *
     *  @return the relative weekday offset terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getRelativeWeekdayStartOffsetTerms() {
        return (getAssembler().getIntegerRangeTerms(getRelativeWeekdayStartOffsetColumn()));
    }


    /**
     *  Specified a preference for ordering results by the relative weekday 
     *  offset. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRelativeWeekdayStartOffset(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRelativeWeekdayStartOffsetColumn(), style);
        return;
    }


    /**
     *  Gets the RelativeWeekdayStartOffset column name.
     *
     * @return the column name
     */

    protected String getRelativeWeekdayStartOffsetColumn() {
        return ("relative_weekday_start_offset");
    }


    /**
     *  Matches a relative weekday. 
     *
     *  @param  weekday the weekday 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRelativeStartWeekday(long weekday, boolean match) {
        getAssembler().addCardinalTerm(getRelativeStartWeekdayColumn(), weekday, match);
        return;
    }


    /**
     *  Matches relative weekday offset events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyRelativeStartWeekday(boolean match) {
        getAssembler().addCardinalWildcardTerm(getRelativeStartWeekdayColumn(), match);
        return;
    }


    /**
     *  Clears the relative weekday terms. 
     */

    @OSID @Override
    public void clearRelativeStartWeekdayTerms() {
        getAssembler().clearTerms(getRelativeStartWeekdayColumn());
        return;
    }


    /**
     *  Gets the relative weekday terms. 
     *
     *  @return the relative weekday terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getRelativeStartWeekdayTerms() {
        return (getAssembler().getCardinalTerms(getRelativeStartWeekdayColumn()));
    }


    /**
     *  Specified a preference for ordering results by the relative weekday. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRelativeStartWeekday(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRelativeStartWeekdayColumn(), style);
        return;
    }


    /**
     *  Gets the RelativeStartWeekday column name.
     *
     * @return the column name
     */

    protected String getRelativeStartWeekdayColumn() {
        return ("relative_start_weekday");
    }


    /**
     *  Matches a fixed duration between the given range inclusive. 
     *
     *  @param  low the start of the range 
     *  @param  high the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchFixedDuration(org.osid.calendaring.Duration low, 
                                   org.osid.calendaring.Duration high, 
                                   boolean match) {
        getAssembler().addDurationRangeTerm(getFixedDurationColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the fixed duration offset terms. 
     */

    @OSID @Override
    public void clearFixedDurationTerms() {
        getAssembler().clearTerms(getFixedDurationColumn());
        return;
    }


    /**
     *  Gets the fixed duration terms. 
     *
     *  @return the fixed duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getFixedDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getFixedDurationColumn()));
    }


    /**
     *  Specified a preference for ordering results by the fixed duration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFixedDurationColumn(), style);
        return;
    }


    /**
     *  Gets the FixedDuration column name.
     *
     * @return the column name
     */

    protected String getFixedDurationColumn() {
        return ("fixed_duration");
    }


    /**
     *  Sets the end reference event <code> Id </code> for this query. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEndReferenceEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getEndReferenceEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the end reference event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEndReferenceEventIdTerms() {
        getAssembler().clearTerms(getEndReferenceEventIdColumn());
        return;
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEndReferenceEventIdTerms() {
        return (getAssembler().getIdTerms(getEndReferenceEventIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the ending reference 
     *  event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndReferenceEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEndReferenceEventColumn(), style);
        return;
    }


    /**
     *  Gets the EndReferenceEventId column name.
     *
     * @return the column name
     */

    protected String getEndReferenceEventIdColumn() {
        return ("end_reference_event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying end 
     *  reference event terms. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndReferenceEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for the end reference event. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventReferenceEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEndReferenceEventQuery() {
        throw new org.osid.UnimplementedException("supportsEndReferenceEventQuery() is false");
    }


    /**
     *  Matches any end reference event events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyEndReferenceEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getEndReferenceEventColumn(), match);
        return;
    }


    /**
     *  Clears the end reference event terms. 
     */

    @OSID @Override
    public void clearEndReferenceEventTerms() {
        getAssembler().clearTerms(getEndReferenceEventColumn());
        return;
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEndReferenceEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Tests if an <code> EventSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an event search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndReferenceEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an event. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndReferenceEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getEndReferenceEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEndReferenceEventSearchOrder() is false");
    }


    /**
     *  Gets the EndReferenceEvent column name.
     *
     * @return the column name
     */

    protected String getEndReferenceEventColumn() {
        return ("end_reference_event");
    }


    /**
     *  Matches a fixed offset amount between the given range inclusive. 
     *
     *  @param  from the start of the range 
     *  @param  to the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchFixedEndOffset(org.osid.calendaring.Duration from, 
                                    org.osid.calendaring.Duration to, 
                                    boolean match) {
        getAssembler().addDurationRangeTerm(getFixedEndOffsetColumn(), from, to, match);
        return;
    }


    /**
     *  Matches fixed offset events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyFixedEndOffset(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getFixedEndOffsetColumn(), match);
        return;
    }


    /**
     *  Clears the fixed offset terms. 
     */

    @OSID @Override
    public void clearFixedEndOffsetTerms() {
        getAssembler().clearTerms(getFixedEndOffsetColumn());
        return;
    }


    /**
     *  Gets the fixed offset terms. 
     *
     *  @return the fixed offset terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getFixedEndOffsetTerms() {
        return (getAssembler().getIntegerRangeTerms(getFixedEndOffsetColumn()));
    }


    /**
     *  Specified a preference for ordering results by the fixed offset. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedEndOffset(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFixedEndOffsetColumn(), style);
        return;
    }


    /**
     *  Gets the FixedEndOffset column name.
     *
     * @return the column name
     */

    protected String getFixedEndOffsetColumn() {
        return ("fixed_end_offset");
    }


    /**
     *  Matches a relative weekday offset amount between the given range 
     *  inclusive. 
     *
     *  @param  low the start of the range 
     *  @param  high the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRelativeWeekdayEndOffset(long low, long high, 
                                              boolean match) {
        getAssembler().addIntegerRangeTerm(getRelativeWeekdayEndOffsetColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the relative weekday offset terms. 
     */

    @OSID @Override
    public void clearRelativeWeekdayEndOffsetTerms() {
        getAssembler().clearTerms(getRelativeWeekdayEndOffsetColumn());
        return;
    }


    /**
     *  Gets the relative weekday offset terms. 
     *
     *  @return the relative weekday offset terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getRelativeWeekdayEndOffsetTerms() {
        return (getAssembler().getIntegerRangeTerms(getRelativeWeekdayEndOffsetColumn()));
    }


    /**
     *  Specified a preference for ordering results by the relative weekday 
     *  offset. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRelativeWeekdayEndOffset(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRelativeWeekdayEndOffsetColumn(), style);
        return;
    }


    /**
     *  Gets the RelativeWeekdayEndOffset column name.
     *
     * @return the column name
     */

    protected String getRelativeWeekdayEndOffsetColumn() {
        return ("relative_weekday_end_offset");
    }


    /**
     *  Matches a relative weekday. 
     *
     *  @param  weekday the weekday 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRelativeEndWeekday(long weekday, boolean match) {
        getAssembler().addCardinalTerm(getRelativeEndWeekdayColumn(), weekday, match);
        return;
    }


    /**
     *  Matches relative weekday offset events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyRelativeEndWeekday(boolean match) {
        getAssembler().addCardinalWildcardTerm(getRelativeEndWeekdayColumn(), match);
        return;
    }


    /**
     *  Clears the relative weekday terms. 
     */

    @OSID @Override
    public void clearRelativeEndWeekdayTerms() {
        getAssembler().clearTerms(getRelativeEndWeekdayColumn());
        return;
    }


    /**
     *  Gets the relative weekday terms. 
     *
     *  @return the relative weekday terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getRelativeEndWeekdayTerms() {
        return (getAssembler().getCardinalTerms(getRelativeEndWeekdayColumn()));
    }


    /**
     *  Specified a preference for ordering results by the relative weekday. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRelativeEndWeekday(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRelativeEndWeekdayColumn(), style);
        return;
    }


    /**
     *  Gets the RelativeEndWeekday column name.
     *
     * @return the column name
     */

    protected String getRelativeEndWeekdayColumn() {
        return ("relative_end_weekday");
    }


    /**
     *  Matches the location description string. 
     *
     *  @param  location location string 
     *  @param  stringMatchType string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> location </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        getAssembler().addStringTerm(getLocationDescriptionColumn(), location, stringMatchType, match);
        return;
    }


    /**
     *  Matches an event that has any location description assigned. 
     *
     *  @param  match <code> true </code> to match events with any location 
     *          description, <code> false </code> to match events with no 
     *          location description 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        getAssembler().addStringWildcardTerm(getLocationDescriptionColumn(), match);
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        getAssembler().clearTerms(getLocationDescriptionColumn());
        return;
    }


    /**
     *  Gets the location description terms. 
     *
     *  @return the location description terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLocationDescriptionTerms() {
        return (getAssembler().getStringTerms(getLocationDescriptionColumn()));
    }


    /**
     *  Specified a preference for ordering results by the location 
     *  description. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocationDescription(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLocationDescriptionColumn(), style);
        return;
    }


    /**
     *  Gets the LocationDescription column name.
     *
     * @return the column name
     */

    protected String getLocationDescriptionColumn() {
        return ("location_description");
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        getAssembler().clearTerms(getLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> terms. 
     *
     *  @return the location <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (getAssembler().getIdTerms(getLocationIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the location. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocation(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLocationColumn(), style);
        return;
    }


    /**
     *  Gets the LocationId column name.
     *
     * @return the column name
     */

    protected String getLocationIdColumn() {
        return ("location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  locations. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches an event that has any location assigned. 
     *
     *  @param  match <code> true </code> to match events with any location, 
     *          <code> false </code> to match events with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getLocationColumn(), match);
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        getAssembler().clearTerms(getLocationColumn());
        return;
    }


    /**
     *  Gets the location terms. 
     *
     *  @return the location terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Tests if a <code> LocationSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a location search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a location. 
     *
     *  @return the location search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchOrder getLocationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLocationSearchOrder() is false");
    }


    /**
     *  Gets the Location column name.
     *
     * @return the column name
     */

    protected String getLocationColumn() {
        return ("location");
    }


    /**
     *  Sets the sponsor <code> Id </code> for this query. 
     *
     *  @param  sponsorId a sponsor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sponsorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id sponsorId, boolean match) {
        getAssembler().addIdTerm(getSponsorIdColumn(), sponsorId, match);
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        getAssembler().clearTerms(getSponsorIdColumn());
        return;
    }


    /**
     *  Gets the sponsor <code> Id </code> terms. 
     *
     *  @return the sponsor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (getAssembler().getIdTerms(getSponsorIdColumn()));
    }


    /**
     *  Gets the SponsorId column name.
     *
     * @return the column name
     */

    protected String getSponsorIdColumn() {
        return ("sponsor_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  sponsors. 
     *
     *  @return <code> true </code> if a sponsor query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the sponsor query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        getAssembler().clearTerms(getSponsorColumn());
        return;
    }


    /**
     *  Gets the sponsor terms. 
     *
     *  @return the sponsor terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Sponsor column name.
     *
     * @return the column name
     */

    protected String getSponsorColumn() {
        return ("sponsor");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this offsetEvent supports the given record
     *  <code>Type</code>.
     *
     *  @param  offsetEventRecordType an offset event record type 
     *  @return <code>true</code> if the offsetEventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type offsetEventRecordType) {
        for (org.osid.calendaring.records.OffsetEventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  offsetEventRecordType the offset event record type 
     *  @return the offset event query record 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventQueryRecord getOffsetEventQueryRecord(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.OffsetEventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  offsetEventRecordType the offset event record type 
     *  @return the offset event query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventQueryInspectorRecord getOffsetEventQueryInspectorRecord(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.OffsetEventQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param offsetEventRecordType the offset event record type
     *  @return the offset event search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventSearchOrderRecord getOffsetEventSearchOrderRecord(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.OffsetEventSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this offset event. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param offsetEventQueryRecord the offset event query record
     *  @param offsetEventQueryInspectorRecord the offset event query inspector
     *         record
     *  @param offsetEventSearchOrderRecord the offset event search order record
     *  @param offsetEventRecordType offset event record type
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventQueryRecord</code>,
     *          <code>offsetEventQueryInspectorRecord</code>,
     *          <code>offsetEventSearchOrderRecord</code> or
     *          <code>offsetEventRecordTypeoffsetEvent</code> is
     *          <code>null</code>
     */
            
    protected void addOffsetEventRecords(org.osid.calendaring.records.OffsetEventQueryRecord offsetEventQueryRecord, 
                                      org.osid.calendaring.records.OffsetEventQueryInspectorRecord offsetEventQueryInspectorRecord, 
                                      org.osid.calendaring.records.OffsetEventSearchOrderRecord offsetEventSearchOrderRecord, 
                                      org.osid.type.Type offsetEventRecordType) {

        addRecordType(offsetEventRecordType);

        nullarg(offsetEventQueryRecord, "offset event query record");
        nullarg(offsetEventQueryInspectorRecord, "offset event query inspector record");
        nullarg(offsetEventSearchOrderRecord, "offset event search odrer record");

        this.queryRecords.add(offsetEventQueryRecord);
        this.queryInspectorRecords.add(offsetEventQueryInspectorRecord);
        this.searchOrderRecords.add(offsetEventSearchOrderRecord);
        
        return;
    }
}
