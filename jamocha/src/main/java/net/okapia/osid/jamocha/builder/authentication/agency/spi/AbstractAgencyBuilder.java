//
// AbstractAgency.java
//
//     Defines an Agency builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.agency.spi;


/**
 *  Defines an <code>Agency</code> builder.
 */

public abstract class AbstractAgencyBuilder<T extends AbstractAgencyBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCatalogBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.authentication.agency.AgencyMiter agency;


    /**
     *  Constructs a new <code>AbstractAgencyBuilder</code>.
     *
     *  @param agency the agency to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAgencyBuilder(net.okapia.osid.jamocha.builder.authentication.agency.AgencyMiter agency) {
        super(agency);
        this.agency = agency;
        return;
    }


    /**
     *  Builds the agency.
     *
     *  @return the new agency
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.authentication.Agency build() {
        (new net.okapia.osid.jamocha.builder.validator.authentication.agency.AgencyValidator(getValidations())).validate(this.agency);
        return (new net.okapia.osid.jamocha.builder.authentication.agency.ImmutableAgency(this.agency));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the agency miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.authentication.agency.AgencyMiter getMiter() {
        return (this.agency);
    }


    /**
     *  Adds an Agency record.
     *
     *  @param record an agency record
     *  @param recordType the type of agency record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.authentication.records.AgencyRecord record, org.osid.type.Type recordType) {
        getMiter().addAgencyRecord(record, recordType);
        return (self());
    }
}       


