//
// AbstractQueryAddressBookLookupSession.java
//
//    An inline adapter that maps an AddressBookLookupSession to
//    an AddressBookQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AddressBookLookupSession to
 *  an AddressBookQuerySession.
 */

public abstract class AbstractQueryAddressBookLookupSession
    extends net.okapia.osid.jamocha.contact.spi.AbstractAddressBookLookupSession
    implements org.osid.contact.AddressBookLookupSession {

    private final org.osid.contact.AddressBookQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAddressBookLookupSession.
     *
     *  @param querySession the underlying address book query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAddressBookLookupSession(org.osid.contact.AddressBookQuerySession querySession) {
        nullarg(querySession, "address book query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>AddressBook</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAddressBooks() {
        return (this.session.canSearchAddressBooks());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>AddressBook</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AddressBook</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AddressBook</code> and
     *  retained for compatibility.
     *
     *  @param  addressBookId <code>Id</code> of the
     *          <code>AddressBook</code>
     *  @return the address book
     *  @throws org.osid.NotFoundException <code>addressBookId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>addressBookId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressBookQuery query = getQuery();
        query.matchId(addressBookId, true);
        org.osid.contact.AddressBookList addressBooks = this.session.getAddressBooksByQuery(query);
        if (addressBooks.hasNext()) {
            return (addressBooks.getNextAddressBook());
        } 
        
        throw new org.osid.NotFoundException(addressBookId + " not found");
    }


    /**
     *  Gets an <code>AddressBookList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  addressBooks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AddressBooks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  addressBookIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AddressBook</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByIds(org.osid.id.IdList addressBookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressBookQuery query = getQuery();

        try (org.osid.id.IdList ids = addressBookIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAddressBooksByQuery(query));
    }


    /**
     *  Gets an <code>AddressBookList</code> corresponding to the given
     *  address book genus <code>Type</code> which does not include
     *  address books of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressBookGenusType an addressBook genus type 
     *  @return the returned <code>AddressBook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByGenusType(org.osid.type.Type addressBookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressBookQuery query = getQuery();
        query.matchGenusType(addressBookGenusType, true);
        return (this.session.getAddressBooksByQuery(query));
    }


    /**
     *  Gets an <code>AddressBookList</code> corresponding to the given
     *  address book genus <code>Type</code> and include any additional
     *  address books with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressBookGenusType an addressBook genus type 
     *  @return the returned <code>AddressBook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByParentGenusType(org.osid.type.Type addressBookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressBookQuery query = getQuery();
        query.matchParentGenusType(addressBookGenusType, true);
        return (this.session.getAddressBooksByQuery(query));
    }


    /**
     *  Gets an <code>AddressBookList</code> containing the given
     *  address book record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressBookRecordType an addressBook record type 
     *  @return the returned <code>AddressBook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByRecordType(org.osid.type.Type addressBookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressBookQuery query = getQuery();
        query.matchRecordType(addressBookRecordType, true);
        return (this.session.getAddressBooksByQuery(query));
    }


    /**
     *  Gets an <code>AddressBookList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known address books or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  address books that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>AddressBook</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressBookQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getAddressBooksByQuery(query));        
    }

    
    /**
     *  Gets all <code>AddressBooks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AddressBooks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.contact.AddressBookQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAddressBooksByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.contact.AddressBookQuery getQuery() {
        org.osid.contact.AddressBookQuery query = this.session.getAddressBookQuery();
        
        return (query);
    }
}
