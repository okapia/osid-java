//
// AbstractCourseChronicleManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCourseChronicleManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.course.chronicle.CourseChronicleManager,
               org.osid.course.chronicle.CourseChronicleProxyManager {

    private final Types programEntryRecordTypes            = new TypeRefSet();
    private final Types programEntrySearchRecordTypes      = new TypeRefSet();

    private final Types courseEntryRecordTypes             = new TypeRefSet();
    private final Types courseEntrySearchRecordTypes       = new TypeRefSet();

    private final Types credentialEntryRecordTypes         = new TypeRefSet();
    private final Types credentialEntrySearchRecordTypes   = new TypeRefSet();

    private final Types assessmentEntryRecordTypes         = new TypeRefSet();
    private final Types assessmentEntrySearchRecordTypes   = new TypeRefSet();

    private final Types awardEntryRecordTypes              = new TypeRefSet();
    private final Types awardEntrySearchRecordTypes        = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCourseChronicleManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCourseChronicleManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if retrieving an academic record is supported. 
     *
     *  @return <code> true </code> if academic record is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademicRecord() {
        return (false);
    }


    /**
     *  Tests if looking up program entries is supported. 
     *
     *  @return <code> true </code> if program entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying program entries is supported. 
     *
     *  @return <code> true </code> if program entry query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching program entries is supported. 
     *
     *  @return <code> true </code> if program entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntrySearch() {
        return (false);
    }


    /**
     *  Tests if program entry administrative service is supported. 
     *
     *  @return <code> true </code> if program entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if a program entry notification service is supported. 
     *
     *  @return <code> true </code> if program entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryNotification() {
        return (false);
    }


    /**
     *  Tests if a program entry cataloging service is supported. 
     *
     *  @return <code> true </code> if program entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps program entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a program entry smart course catalog session is available. 
     *
     *  @return <code> true </code> if a program entry smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntrySmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up course entries is supported. 
     *
     *  @return <code> true </code> if course entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying course entries is supported. 
     *
     *  @return <code> true </code> if course entry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching course entries is supported. 
     *
     *  @return <code> true </code> if course entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntrySearch() {
        return (false);
    }


    /**
     *  Tests if course entry administrative service is supported. 
     *
     *  @return <code> true </code> if course entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if a course entry notification service is supported. 
     *
     *  @return <code> true </code> if course entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryNotification() {
        return (false);
    }


    /**
     *  Tests if a course entry cataloging service is supported. 
     *
     *  @return <code> true </code> if course entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps course entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a course entry smart course catalog session is available. 
     *
     *  @return <code> true </code> if a course entry smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntrySmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up credential entries is supported. 
     *
     *  @return <code> true </code> if credential entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying credential entries is supported. 
     *
     *  @return <code> true </code> if credential entry query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching credential entries is supported. 
     *
     *  @return <code> true </code> if credential entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntrySearch() {
        return (false);
    }


    /**
     *  Tests if credential entry administrative service is supported. 
     *
     *  @return <code> true </code> if credential entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if a credential entry notification service is supported. 
     *
     *  @return <code> true </code> if credential entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryNotification() {
        return (false);
    }


    /**
     *  Tests if a credential entry cataloging service is supported. 
     *
     *  @return <code> true </code> if credential entry cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps credential entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a credential entry smart course catalog session is available. 
     *
     *  @return <code> true </code> if a credential entry smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntrySmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up assessment entries is supported. 
     *
     *  @return <code> true </code> if assessment entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying assessment entries is supported. 
     *
     *  @return <code> true </code> if assessment entry query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching assessment entries is supported. 
     *
     *  @return <code> true </code> if assessment entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntrySearch() {
        return (false);
    }


    /**
     *  Tests if assessment entry administrative service is supported. 
     *
     *  @return <code> true </code> if assessment entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if an assessment entry notification service is supported. 
     *
     *  @return <code> true </code> if assessment entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryNotification() {
        return (false);
    }


    /**
     *  Tests if an assessment entry cataloging service is supported. 
     *
     *  @return <code> true </code> if assessment entry cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps assessment entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if an assessment entry smart course catalog session is 
     *  available. 
     *
     *  @return <code> true </code> if an assessment entry smart course 
     *          catalog session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntrySmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up award entries is supported. 
     *
     *  @return <code> true </code> if award entry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying award entries is supported. 
     *
     *  @return <code> true </code> if award entry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching award entries is supported. 
     *
     *  @return <code> true </code> if award entry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntrySearch() {
        return (false);
    }


    /**
     *  Tests if award entry administrative service is supported. 
     *
     *  @return <code> true </code> if award entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if an award entry notification service is supported. 
     *
     *  @return <code> true </code> if award entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryNotification() {
        return (false);
    }


    /**
     *  Tests if an award entry cataloging service is supported. 
     *
     *  @return <code> true </code> if award entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps award entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if an award entry smart course catalog session is available. 
     *
     *  @return <code> true </code> if an award entry smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntrySmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course chronicle batch service is available. 
     *
     *  @return <code> true </code> if a course chronicle batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseChronicalBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> ProgramEntry </code> record types. 
     *
     *  @return a list containing the supported <code> ProgramEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.programEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProgramEntry </code> record type is 
     *  supported. 
     *
     *  @param  programEntryRecordType a <code> Type </code> indicating a 
     *          <code> ProgramEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> programEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramEntryRecordType(org.osid.type.Type programEntryRecordType) {
        return (this.programEntryRecordTypes.contains(programEntryRecordType));
    }


    /**
     *  Adds support for a program entry record type.
     *
     *  @param programEntryRecordType a program entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>programEntryRecordType</code> is <code>null</code>
     */

    protected void addProgramEntryRecordType(org.osid.type.Type programEntryRecordType) {
        this.programEntryRecordTypes.add(programEntryRecordType);
        return;
    }


    /**
     *  Removes support for a program entry record type.
     *
     *  @param programEntryRecordType a program entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>programEntryRecordType</code> is <code>null</code>
     */

    protected void removeProgramEntryRecordType(org.osid.type.Type programEntryRecordType) {
        this.programEntryRecordTypes.remove(programEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProgramEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> ProgramEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.programEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProgramEntry </code> search record type is 
     *  supported. 
     *
     *  @param  programEntrySearchRecordType a <code> Type </code> indicating 
     *          a <code> ProgramEntry </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          programEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramEntrySearchRecordType(org.osid.type.Type programEntrySearchRecordType) {
        return (this.programEntrySearchRecordTypes.contains(programEntrySearchRecordType));
    }


    /**
     *  Adds support for a program entry search record type.
     *
     *  @param programEntrySearchRecordType a program entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>programEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addProgramEntrySearchRecordType(org.osid.type.Type programEntrySearchRecordType) {
        this.programEntrySearchRecordTypes.add(programEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a program entry search record type.
     *
     *  @param programEntrySearchRecordType a program entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>programEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeProgramEntrySearchRecordType(org.osid.type.Type programEntrySearchRecordType) {
        this.programEntrySearchRecordTypes.remove(programEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CourseEntry </code> record types. 
     *
     *  @return a list containing the supported <code> CourseEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.courseEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CourseEntry </code> record type is 
     *  supported. 
     *
     *  @param  courseEntryRecordType a <code> Type </code> indicating a 
     *          <code> CourseEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseEntryRecordType(org.osid.type.Type courseEntryRecordType) {
        return (this.courseEntryRecordTypes.contains(courseEntryRecordType));
    }


    /**
     *  Adds support for a course entry record type.
     *
     *  @param courseEntryRecordType a course entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>courseEntryRecordType</code> is <code>null</code>
     */

    protected void addCourseEntryRecordType(org.osid.type.Type courseEntryRecordType) {
        this.courseEntryRecordTypes.add(courseEntryRecordType);
        return;
    }


    /**
     *  Removes support for a course entry record type.
     *
     *  @param courseEntryRecordType a course entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>courseEntryRecordType</code> is <code>null</code>
     */

    protected void removeCourseEntryRecordType(org.osid.type.Type courseEntryRecordType) {
        this.courseEntryRecordTypes.remove(courseEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CourseEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> CourseEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.courseEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CourseEntry </code> search record type is 
     *  supported. 
     *
     *  @param  courseEntrySearchRecordType a <code> Type </code> indicating a 
     *          <code> CourseEntry </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          courseEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseEntrySearchRecordType(org.osid.type.Type courseEntrySearchRecordType) {
        return (this.courseEntrySearchRecordTypes.contains(courseEntrySearchRecordType));
    }


    /**
     *  Adds support for a course entry search record type.
     *
     *  @param courseEntrySearchRecordType a course entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>courseEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addCourseEntrySearchRecordType(org.osid.type.Type courseEntrySearchRecordType) {
        this.courseEntrySearchRecordTypes.add(courseEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a course entry search record type.
     *
     *  @param courseEntrySearchRecordType a course entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>courseEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeCourseEntrySearchRecordType(org.osid.type.Type courseEntrySearchRecordType) {
        this.courseEntrySearchRecordTypes.remove(courseEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CredentialEntry </code> record types. 
     *
     *  @return a list containing the supported <code> CredentialEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.credentialEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CredentialEntry </code> record type is 
     *  supported. 
     *
     *  @param  credentialEntryRecordType a <code> Type </code> indicating a 
     *          <code> CredentialEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialEntryRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialEntryRecordType(org.osid.type.Type credentialEntryRecordType) {
        return (this.credentialEntryRecordTypes.contains(credentialEntryRecordType));
    }


    /**
     *  Adds support for a credential entry record type.
     *
     *  @param credentialEntryRecordType a credential entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>credentialEntryRecordType</code> is <code>null</code>
     */

    protected void addCredentialEntryRecordType(org.osid.type.Type credentialEntryRecordType) {
        this.credentialEntryRecordTypes.add(credentialEntryRecordType);
        return;
    }


    /**
     *  Removes support for a credential entry record type.
     *
     *  @param credentialEntryRecordType a credential entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>credentialEntryRecordType</code> is <code>null</code>
     */

    protected void removeCredentialEntryRecordType(org.osid.type.Type credentialEntryRecordType) {
        this.credentialEntryRecordTypes.remove(credentialEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CredentialEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> CredentialEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.credentialEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CredentialEntry </code> search record type 
     *  is supported. 
     *
     *  @param  credentialEntrySearchRecordType a <code> Type </code> 
     *          indicating a <code> CredentialEntry </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialEntrySearchRecordType(org.osid.type.Type credentialEntrySearchRecordType) {
        return (this.credentialEntrySearchRecordTypes.contains(credentialEntrySearchRecordType));
    }


    /**
     *  Adds support for a credential entry search record type.
     *
     *  @param credentialEntrySearchRecordType a credential entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>credentialEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addCredentialEntrySearchRecordType(org.osid.type.Type credentialEntrySearchRecordType) {
        this.credentialEntrySearchRecordTypes.add(credentialEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a credential entry search record type.
     *
     *  @param credentialEntrySearchRecordType a credential entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>credentialEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeCredentialEntrySearchRecordType(org.osid.type.Type credentialEntrySearchRecordType) {
        this.credentialEntrySearchRecordTypes.remove(credentialEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AssessmentEntry </code> record types. 
     *
     *  @return a list containing the supported <code> AssessmentEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.assessmentEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AssessmentEntry </code> record type is 
     *  supported. 
     *
     *  @param  assessmentEntryRecordType an <code> Type </code> indicating an 
     *          <code> AssessmentEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentEntryRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryRecordType(org.osid.type.Type assessmentEntryRecordType) {
        return (this.assessmentEntryRecordTypes.contains(assessmentEntryRecordType));
    }


    /**
     *  Adds support for an assessment entry record type.
     *
     *  @param assessmentEntryRecordType an assessment entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentEntryRecordType</code> is <code>null</code>
     */

    protected void addAssessmentEntryRecordType(org.osid.type.Type assessmentEntryRecordType) {
        this.assessmentEntryRecordTypes.add(assessmentEntryRecordType);
        return;
    }


    /**
     *  Removes support for an assessment entry record type.
     *
     *  @param assessmentEntryRecordType an assessment entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentEntryRecordType</code> is <code>null</code>
     */

    protected void removeAssessmentEntryRecordType(org.osid.type.Type assessmentEntryRecordType) {
        this.assessmentEntryRecordTypes.remove(assessmentEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AssessmentEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> AssessmentEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.assessmentEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AssessmentEntry </code> search record type 
     *  is supported. 
     *
     *  @param  assessmentEntrySearchRecordType an <code> Type </code> 
     *          indicating an <code> AssessmentEntry </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentEntrySearchRecordType(org.osid.type.Type assessmentEntrySearchRecordType) {
        return (this.assessmentEntrySearchRecordTypes.contains(assessmentEntrySearchRecordType));
    }


    /**
     *  Adds support for an assessment entry search record type.
     *
     *  @param assessmentEntrySearchRecordType an assessment entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addAssessmentEntrySearchRecordType(org.osid.type.Type assessmentEntrySearchRecordType) {
        this.assessmentEntrySearchRecordTypes.add(assessmentEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for an assessment entry search record type.
     *
     *  @param assessmentEntrySearchRecordType an assessment entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeAssessmentEntrySearchRecordType(org.osid.type.Type assessmentEntrySearchRecordType) {
        this.assessmentEntrySearchRecordTypes.remove(assessmentEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AwardEntry </code> record types. 
     *
     *  @return a list containing the supported <code> AwardEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.awardEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AwardEntry </code> record type is supported. 
     *
     *  @param  awardEntryRecordType an <code> Type </code> indicating an 
     *          <code> AwardEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> awardEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardEntryRecordType(org.osid.type.Type awardEntryRecordType) {
        return (this.awardEntryRecordTypes.contains(awardEntryRecordType));
    }


    /**
     *  Adds support for an award entry record type.
     *
     *  @param awardEntryRecordType an award entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>awardEntryRecordType</code> is <code>null</code>
     */

    protected void addAwardEntryRecordType(org.osid.type.Type awardEntryRecordType) {
        this.awardEntryRecordTypes.add(awardEntryRecordType);
        return;
    }


    /**
     *  Removes support for an award entry record type.
     *
     *  @param awardEntryRecordType an award entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>awardEntryRecordType</code> is <code>null</code>
     */

    protected void removeAwardEntryRecordType(org.osid.type.Type awardEntryRecordType) {
        this.awardEntryRecordTypes.remove(awardEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AwardEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> AwardEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.awardEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AwardEntry </code> search record type is 
     *  supported. 
     *
     *  @param  awardEntrySearchRecordType an <code> Type </code> indicating 
     *          an <code> AwardEntry </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          awardEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardEntrySearchRecordType(org.osid.type.Type awardEntrySearchRecordType) {
        return (this.awardEntrySearchRecordTypes.contains(awardEntrySearchRecordType));
    }


    /**
     *  Adds support for an award entry search record type.
     *
     *  @param awardEntrySearchRecordType an award entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>awardEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addAwardEntrySearchRecordType(org.osid.type.Type awardEntrySearchRecordType) {
        this.awardEntrySearchRecordTypes.add(awardEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for an award entry search record type.
     *
     *  @param awardEntrySearchRecordType an award entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>awardEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeAwardEntrySearchRecordType(org.osid.type.Type awardEntrySearchRecordType) {
        this.awardEntrySearchRecordTypes.remove(awardEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academic 
     *  record service. 
     *
     *  @return an <code> AcademicRecordSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademicRecord() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AcademicRecordSession getAcademicRecordSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAcademicRecordSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academic 
     *  record service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AcademicRecordSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademicRecord() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AcademicRecordSession getAcademicRecordSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAcademicRecordSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academic 
     *  record service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> AcademicRecordSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademicRecord() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AcademicRecordSession getAcademicRecordSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAcademicRecordSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academic 
     *  record service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> AcademicRecordSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademicRecord() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AcademicRecordSession getAcademicRecordSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAcademicRecordSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  lookup service. 
     *
     *  @return a <code> ProgramEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryLookupSession getProgramEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryLookupSession getProgramEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> ProgramEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryLookupSession getProgramEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryLookupSession getProgramEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  query service. 
     *
     *  @return a <code> ProgramEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryQuerySession getProgramEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryQuerySession getProgramEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryQuerySession getProgramEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryQuerySession getProgramEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  search service. 
     *
     *  @return a <code> ProgramEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntrySearchSession getProgramEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntrySearchSession getProgramEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntrySearchSession getProgramEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntrySearchSession getProgramEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  administration service. 
     *
     *  @return a <code> ProgramEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryAdminSession getProgramEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryAdminSession getProgramEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryAdminSession getProgramEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryAdminSession getProgramEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  notification service. 
     *
     *  @param  programEntryReceiver the notification callback 
     *  @return a <code> ProgramEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> programEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryNotificationSession getProgramEntryNotificationSession(org.osid.course.chronicle.ProgramEntryReceiver programEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  notification service. 
     *
     *  @param  programEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> programEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryNotificationSession getProgramEntryNotificationSession(org.osid.course.chronicle.ProgramEntryReceiver programEntryReceiver, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  notification service for the given course catalog. 
     *
     *  @param  programEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> programEntryReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryNotificationSession getProgramEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.ProgramEntryReceiver programEntryReceiver, 
                                                                                                                        org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  notification service for the given course catalog. 
     *
     *  @param  programEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> programEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryNotificationSession getProgramEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.ProgramEntryReceiver programEntryReceiver, 
                                                                                                                        org.osid.id.Id courseCatalogId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup program entry/catalog 
     *  mappings. 
     *
     *  @return a <code> ProgramEntryCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryCourseCatalogSession getProgramEntryCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup program entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryCourseCatalogSession getProgramEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning program 
     *  entries to course catalogs. 
     *
     *  @return a <code> ProgramEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryCourseCatalogAssignmentSession getProgramEntryCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning program 
     *  entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryCourseCatalogAssignmentSession getProgramEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntrySmartCourseCatalogSession getProgramEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getProgramEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntrySmartCourseCatalogSession getProgramEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getProgramEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  lookup service. 
     *
     *  @return a <code> CourseEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryLookupSession getCourseEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryLookupSession getCourseEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CourseEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryLookupSession getCourseEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryLookupSession getCourseEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  query service. 
     *
     *  @return a <code> CourseEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryQuerySession getCourseEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryQuerySession getCourseEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryQuerySession getCourseEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryQuerySession getCourseEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  search service. 
     *
     *  @return a <code> CourseEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntrySearchSession getCourseEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntrySearchSession getCourseEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntrySearchSession getCourseEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntrySearchSession getCourseEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  administration service. 
     *
     *  @return a <code> CourseEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryAdminSession getCourseEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryAdminSession getCourseEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryAdminSession getCourseEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryAdminSession getCourseEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  notification service. 
     *
     *  @param  courseEntryReceiver the notification callback 
     *  @return a <code> CourseEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryNotificationSession getCourseEntryNotificationSession(org.osid.course.chronicle.CourseEntryReceiver courseEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  notification service. 
     *
     *  @param  courseEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryNotificationSession getCourseEntryNotificationSession(org.osid.course.chronicle.CourseEntryReceiver courseEntryReceiver, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  notification service for the given course catalog. 
     *
     *  @param  courseEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseEntryReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryNotificationSession getCourseEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.CourseEntryReceiver courseEntryReceiver, 
                                                                                                                      org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  notification service for the given course catalog. 
     *
     *  @param  courseEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryNotificationSession getCourseEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.CourseEntryReceiver courseEntryReceiver, 
                                                                                                                      org.osid.id.Id courseCatalogId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup course entry/catalog 
     *  mappings. 
     *
     *  @return a <code> CourseEntryCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryCourseCatalogSession getCourseEntryCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup course entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryCourseCatalogSession getCourseEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning course 
     *  entries to course catalogs. 
     *
     *  @return a <code> CourseEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryCourseCatalogAssignmentSession getCourseEntryCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning course 
     *  entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryCourseCatalogAssignmentSession getCourseEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntrySmartCourseCatalogSession getCourseEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntrySmartCourseCatalogSession getCourseEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry lookup service. 
     *
     *  @return a <code> CredentialEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryLookupSession getCredentialEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryLookupSession getCredentialEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CredentialEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryLookupSession getCredentialEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryLookupSession getCredentialEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry query service. 
     *
     *  @return a <code> CredentialEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryQuerySession getCredentialEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryQuerySession getCredentialEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryQuerySession getCredentialEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryQuerySession getCredentialEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry search service. 
     *
     *  @return a <code> CredentialEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntrySearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntrySearchSession getCredentialEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntrySearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntrySearchSession getCredentialEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntrySearchSession getCredentialEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntrySearchSession getCredentialEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry administration service. 
     *
     *  @return a <code> CredentialEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryAdminSession getCredentialEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryAdminSession getCredentialEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryAdminSession getCredentialEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryAdminSession getCredentialEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry notification service. 
     *
     *  @param  credentialEntryReceiver the notification callback 
     *  @return a <code> CredentialEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> credentialEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryNotificationSession getCredentialEntryNotificationSession(org.osid.course.chronicle.CredentialEntryReceiver credentialEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry notification service. 
     *
     *  @param  credentialEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> credentialEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryNotificationSession getCredentialEntryNotificationSession(org.osid.course.chronicle.CredentialEntryReceiver credentialEntryReceiver, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry notification service for the given course catalog. 
     *
     *  @param  credentialEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> credentialEntryReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryNotificationSession getCredentialEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.CredentialEntryReceiver credentialEntryReceiver, 
                                                                                                                              org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry notification service for the given course catalog. 
     *
     *  @param  credentialEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> credentialEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryNotificationSession getCredentialEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.CredentialEntryReceiver credentialEntryReceiver, 
                                                                                                                              org.osid.id.Id courseCatalogId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup credential entry/catalog 
     *  mappings. 
     *
     *  @return a <code> CredentialEntryCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryCourseCatalogSession getCredentialEntryCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup credential entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryCourseCatalogSession getCredentialEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  credential entries to course catalogs. 
     *
     *  @return a <code> CredentialEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryCourseCatalogAssignmentSession getCredentialEntryCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  credential entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryCourseCatalogAssignmentSession getCredentialEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntrySmartCourseCatalogSession getCredentialEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCredentialEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntrySmartCourseCatalogSession getCredentialEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCredentialEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry lookup service. 
     *
     *  @return an <code> AssessmentEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryLookupSession getAssessmentEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryLookupSession getAssessmentEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> AssessmentEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryLookupSession getAssessmentEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryLookupSession getAssessmentEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry query service. 
     *
     *  @return an <code> AssessmentEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryQuerySession getAssessmentEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryQuerySession getAssessmentEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AssessmentEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryQuerySession getAssessmentEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryQuerySession getAssessmentEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry search service. 
     *
     *  @return an <code> AssessmentEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntrySearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntrySearchSession getAssessmentEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntrySearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntrySearchSession getAssessmentEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AssessmentEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntrySearchSession getAssessmentEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntrySearchSession getAssessmentEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry administration service. 
     *
     *  @return an <code> AssessmentEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryAdminSession getAssessmentEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryAdminSession getAssessmentEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AssessmentEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryAdminSession getAssessmentEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryAdminSession getAssessmentEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry notification service. 
     *
     *  @param  assessmentEntryReceiver the notification callback 
     *  @return an <code> AssessmentEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryNotificationSession getAssessmentEntryNotificationSession(org.osid.course.chronicle.AssessmentEntryReceiver assessmentEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry notification service. 
     *
     *  @param  assessmentEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryNotificationSession getAssessmentEntryNotificationSession(org.osid.course.chronicle.AssessmentEntryReceiver assessmentEntryReceiver, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  assessmentEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AssessmentEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentEntryReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryNotificationSession getAssessmentEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.AssessmentEntryReceiver assessmentEntryReceiver, 
                                                                                                                              org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry notification service for the given course catalog. 
     *
     *  @param  assessmentEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryNotificationSession getAssessmentEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.AssessmentEntryReceiver assessmentEntryReceiver, 
                                                                                                                              org.osid.id.Id courseCatalogId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup assessment entry/catalog 
     *  mappings. 
     *
     *  @return an <code> AssessmentEntryCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryCourseCatalogSession getAssessmentEntryCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup assessment entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryCourseCatalogSession getAssessmentEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  assessment entries to course catalogs. 
     *
     *  @return an <code> AssessmentEntryCourseCatalogAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryCourseCatalogAssignmentSession getAssessmentEntryCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  assessment entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryCourseCatalogAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryCourseCatalogAssignmentSession getAssessmentEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AssessmentEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntrySmartCourseCatalogSession getAssessmentEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAssessmentEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntrySmartCourseCatalogSession getAssessmentEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAssessmentEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  lookup service. 
     *
     *  @return an <code> AwardEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryLookupSession getAwardEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryLookupSession getAwardEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> AwardEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryLookupSession getAwardEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryLookupSession getAwardEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  query service. 
     *
     *  @return an <code> AwardEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryQuerySession getAwardEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryQuerySession getAwardEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AwardEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryQuerySession getAwardEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryQuerySession getAwardEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  search service. 
     *
     *  @return an <code> AwardEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntrySearchSession getAwardEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntrySearchSession getAwardEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AwardEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntrySearchSession getAwardEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntrySearchSession getAwardEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntrySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  administration service. 
     *
     *  @return an <code> AwardEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryAdminSession getAwardEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryAdminSession getAwardEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AwardEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryAdminSession getAwardEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryAdminSession getAwardEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  notification service. 
     *
     *  @param  awardEntryReceiver the notification callback 
     *  @return an <code> AwardEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> awardEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryNotificationSession getAwardEntryNotificationSession(org.osid.course.chronicle.AwardEntryReceiver awardEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  notification service. 
     *
     *  @param  awardEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> awardEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryNotificationSession getAwardEntryNotificationSession(org.osid.course.chronicle.AwardEntryReceiver awardEntryReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  awardEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AwardEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> awardEntryReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryNotificationSession getAwardEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.AwardEntryReceiver awardEntryReceiver, 
                                                                                                                    org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  notification service for the given course catalog. 
     *
     *  @param  awardEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> awardEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryNotificationSession getAwardEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.AwardEntryReceiver awardEntryReceiver, 
                                                                                                                    org.osid.id.Id courseCatalogId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup award entry/catalog 
     *  mappings. 
     *
     *  @return an <code> AwardEntryCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryCourseCatalogSession getAwardEntryCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup award entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryCourseCatalogSession getAwardEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning award 
     *  entries to course catalogs. 
     *
     *  @return an <code> AwardEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryCourseCatalogAssignmentSession getAwardEntryCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning award 
     *  entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryCourseCatalogAssignmentSession getAwardEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntryCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AwardEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntrySmartCourseCatalogSession getAwardEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getAwardEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntrySmartCourseCatalogSession getAwardEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getAwardEntrySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> CourseChronicleBatchManager. </code> 
     *
     *  @return a <code> CourseChronicleBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseChronicleBatchManager() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CourseChronicleBatchManager getCourseChronicleBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleManager.getCourseChronicleBatchManager not implemented");
    }


    /**
     *  Gets the <code> CourseChronicleBatchProxyManager. </code> 
     *
     *  @return a <code> CourseChronicleBatcProxyhManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseChronicleBatchProxyManager() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager getCourseChronicleBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.CourseChronicleProxyManager.getCourseChronicleBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.programEntryRecordTypes.clear();
        this.programEntryRecordTypes.clear();

        this.programEntrySearchRecordTypes.clear();
        this.programEntrySearchRecordTypes.clear();

        this.courseEntryRecordTypes.clear();
        this.courseEntryRecordTypes.clear();

        this.courseEntrySearchRecordTypes.clear();
        this.courseEntrySearchRecordTypes.clear();

        this.credentialEntryRecordTypes.clear();
        this.credentialEntryRecordTypes.clear();

        this.credentialEntrySearchRecordTypes.clear();
        this.credentialEntrySearchRecordTypes.clear();

        this.assessmentEntryRecordTypes.clear();
        this.assessmentEntryRecordTypes.clear();

        this.assessmentEntrySearchRecordTypes.clear();
        this.assessmentEntrySearchRecordTypes.clear();

        this.awardEntryRecordTypes.clear();
        this.awardEntryRecordTypes.clear();

        this.awardEntrySearchRecordTypes.clear();
        this.awardEntrySearchRecordTypes.clear();

        return;
    }
}
