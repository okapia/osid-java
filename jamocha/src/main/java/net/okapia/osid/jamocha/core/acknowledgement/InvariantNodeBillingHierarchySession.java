//
// InvariantNodeBillingHierarchySession.java
//
//     Defines a Billing hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement;


/**
 *  Defines a repository hierarchy session for delivering a hierarchy of
 *  repositories using the BillingNode interface.
 */

public final class InvariantNodeBillingHierarchySession
    extends net.okapia.osid.jamocha.core.acknowledgement.spi.AbstractNodeBillingHierarchySession
    implements org.osid.acknowledgement.BillingHierarchySession {


    /**
     *  Constructs a new
     *  <code>InvariantNodeBillingHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public InvariantNodeBillingHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantNodeBillingHierarchySession</code> from a
     *  single root node also used to identify the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>root</code> 
     *          is <code>null</code>
     */

    public InvariantNodeBillingHierarchySession(org.osid.acknowledgement.BillingNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getBilling().getDisplayName())
                     .description(root.getBilling().getDescription())
                     .build());

        addRootBilling(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantNodeBillingHierarchySession</code> using a
     *  single node as the root.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public InvariantNodeBillingHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                                 org.osid.acknowledgement.BillingNode root) {
        setHierarchy(hierarchy);
        addRootBilling(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantNodeBillingHierarchySession</code> using a
     *  collection of nodes as the root.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root roots
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public InvariantNodeBillingHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                                java.util.Collection<org.osid.acknowledgement.BillingNode> roots) {
        setHierarchy(hierarchy);
        addRootBillings(roots);
        return;
    }
}
