//
// AbstractFederatingRaceConstrainerLookupSession.java
//
//     An abstract federating adapter for a RaceConstrainerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RaceConstrainerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRaceConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.voting.rules.RaceConstrainerLookupSession>
    implements org.osid.voting.rules.RaceConstrainerLookupSession {

    private boolean parallel = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();


    /**
     *  Constructs a new <code>AbstractFederatingRaceConstrainerLookupSession</code>.
     */

    protected AbstractFederatingRaceConstrainerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.voting.rules.RaceConstrainerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>RaceConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaceConstrainers() {
        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            if (session.canLookupRaceConstrainers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>RaceConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceConstrainerView() {
        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            session.useComparativeRaceConstrainerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>RaceConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceConstrainerView() {
        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            session.usePlenaryRaceConstrainerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race constrainers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            session.useFederatedPollsView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            session.useIsolatedPollsView();
        }

        return;
    }


    /**
     *  Only active race constrainers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveRaceConstrainerView() {
        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            session.useActiveRaceConstrainerView();
        }

        return;
    }


    /**
     *  Active and inactive race constrainers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceConstrainerView() {
        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            session.useAnyStatusRaceConstrainerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>RaceConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RaceConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RaceConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, race constrainers are returned that are currently
     *  active. In any status mode, active and inactive race constrainers
     *  are returned.
     *
     *  @param  raceConstrainerId <code>Id</code> of the
     *          <code>RaceConstrainer</code>
     *  @return the race constrainer
     *  @throws org.osid.NotFoundException <code>raceConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainer getRaceConstrainer(org.osid.id.Id raceConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            try {
                return (session.getRaceConstrainer(raceConstrainerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(raceConstrainerId + " not found");
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  raceConstrainers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>RaceConstrainers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @param  raceConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RaceConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByIds(org.osid.id.IdList raceConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.voting.rules.raceconstrainer.MutableRaceConstrainerList ret = new net.okapia.osid.jamocha.voting.rules.raceconstrainer.MutableRaceConstrainerList();

        try (org.osid.id.IdList ids = raceConstrainerIds) {
            while (ids.hasNext()) {
                ret.addRaceConstrainer(getRaceConstrainer(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> corresponding to the
     *  given race constrainer genus <code>Type</code> which does not
     *  include race constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @param  raceConstrainerGenusType a raceConstrainer genus type 
     *  @return the returned <code>RaceConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByGenusType(org.osid.type.Type raceConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceconstrainer.FederatingRaceConstrainerList ret = getRaceConstrainerList();

        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            ret.addRaceConstrainerList(session.getRaceConstrainersByGenusType(raceConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> corresponding to the
     *  given race constrainer genus <code>Type</code> and include any
     *  additional race constrainers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @param  raceConstrainerGenusType a raceConstrainer genus type 
     *  @return the returned <code>RaceConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByParentGenusType(org.osid.type.Type raceConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceconstrainer.FederatingRaceConstrainerList ret = getRaceConstrainerList();

        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            ret.addRaceConstrainerList(session.getRaceConstrainersByParentGenusType(raceConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> containing the given
     *  race constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known race
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @param  raceConstrainerRecordType a raceConstrainer record type 
     *  @return the returned <code>RaceConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByRecordType(org.osid.type.Type raceConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceconstrainer.FederatingRaceConstrainerList ret = getRaceConstrainerList();

        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            ret.addRaceConstrainerList(session.getRaceConstrainersByRecordType(raceConstrainerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>RaceConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  race constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race constrainers are returned that are currently
     *  active. In any status mode, active and inactive race constrainers
     *  are returned.
     *
     *  @return a list of <code>RaceConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceconstrainer.FederatingRaceConstrainerList ret = getRaceConstrainerList();

        for (org.osid.voting.rules.RaceConstrainerLookupSession session : getSessions()) {
            ret.addRaceConstrainerList(session.getRaceConstrainers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.voting.rules.raceconstrainer.FederatingRaceConstrainerList getRaceConstrainerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.rules.raceconstrainer.ParallelRaceConstrainerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.rules.raceconstrainer.CompositeRaceConstrainerList());
        }
    }
}
