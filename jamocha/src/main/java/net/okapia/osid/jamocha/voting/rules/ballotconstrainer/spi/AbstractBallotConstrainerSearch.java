//
// AbstractBallotConstrainerSearch.java
//
//     A template for making a BallotConstrainer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.ballotconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing ballot constrainer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBallotConstrainerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.voting.rules.BallotConstrainerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.voting.rules.BallotConstrainerSearchOrder ballotConstrainerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of ballot constrainers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  ballotConstrainerIds list of ballot constrainers
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBallotConstrainers(org.osid.id.IdList ballotConstrainerIds) {
        while (ballotConstrainerIds.hasNext()) {
            try {
                this.ids.add(ballotConstrainerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBallotConstrainers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of ballot constrainer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBallotConstrainerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  ballotConstrainerSearchOrder ballot constrainer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>ballotConstrainerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBallotConstrainerResults(org.osid.voting.rules.BallotConstrainerSearchOrder ballotConstrainerSearchOrder) {
	this.ballotConstrainerSearchOrder = ballotConstrainerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.voting.rules.BallotConstrainerSearchOrder getBallotConstrainerSearchOrder() {
	return (this.ballotConstrainerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given ballot constrainer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a ballot constrainer implementing the requested record.
     *
     *  @param ballotConstrainerSearchRecordType a ballot constrainer search record
     *         type
     *  @return the ballot constrainer search record
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerSearchRecord getBallotConstrainerSearchRecord(org.osid.type.Type ballotConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.voting.rules.records.BallotConstrainerSearchRecord record : this.records) {
            if (record.implementsRecordType(ballotConstrainerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ballot constrainer search. 
     *
     *  @param ballotConstrainerSearchRecord ballot constrainer search record
     *  @param ballotConstrainerSearchRecordType ballotConstrainer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBallotConstrainerSearchRecord(org.osid.voting.rules.records.BallotConstrainerSearchRecord ballotConstrainerSearchRecord, 
                                           org.osid.type.Type ballotConstrainerSearchRecordType) {

        addRecordType(ballotConstrainerSearchRecordType);
        this.records.add(ballotConstrainerSearchRecord);        
        return;
    }
}
