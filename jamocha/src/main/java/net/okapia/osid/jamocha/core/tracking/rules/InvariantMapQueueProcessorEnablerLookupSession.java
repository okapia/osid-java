//
// InvariantMapQueueProcessorEnablerLookupSession
//
//    Implements a QueueProcessorEnabler lookup service backed by a fixed collection of
//    queueProcessorEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.rules;


/**
 *  Implements a QueueProcessorEnabler lookup service backed by a fixed
 *  collection of queue processor enablers. The queue processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapQueueProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.tracking.rules.spi.AbstractMapQueueProcessorEnablerLookupSession
    implements org.osid.tracking.rules.QueueProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorEnablerLookupSession</code> with no
     *  queue processor enablers.
     *  
     *  @param frontOffice the front office
     *  @throws org.osid.NullArgumnetException {@code frontOffice} is
     *          {@code null}
     */

    public InvariantMapQueueProcessorEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice) {
        setFrontOffice(frontOffice);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorEnablerLookupSession</code> with a single
     *  queue processor enabler.
     *  
     *  @param frontOffice the front office
     *  @param queueProcessorEnabler a single queue processor enabler
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueProcessorEnabler} is <code>null</code>
     */

      public InvariantMapQueueProcessorEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                               org.osid.tracking.rules.QueueProcessorEnabler queueProcessorEnabler) {
        this(frontOffice);
        putQueueProcessorEnabler(queueProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorEnablerLookupSession</code> using an array
     *  of queue processor enablers.
     *  
     *  @param frontOffice the front office
     *  @param queueProcessorEnablers an array of queue processor enablers
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueProcessorEnablers} is <code>null</code>
     */

      public InvariantMapQueueProcessorEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                               org.osid.tracking.rules.QueueProcessorEnabler[] queueProcessorEnablers) {
        this(frontOffice);
        putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorEnablerLookupSession</code> using a
     *  collection of queue processor enablers.
     *
     *  @param frontOffice the front office
     *  @param queueProcessorEnablers a collection of queue processor enablers
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueProcessorEnablers} is <code>null</code>
     */

      public InvariantMapQueueProcessorEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                               java.util.Collection<? extends org.osid.tracking.rules.QueueProcessorEnabler> queueProcessorEnablers) {
        this(frontOffice);
        putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }
}
