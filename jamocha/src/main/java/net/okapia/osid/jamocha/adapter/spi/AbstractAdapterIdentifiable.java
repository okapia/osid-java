//
// AbstractAdapterIdentifiable.java
//
//     Defines an Identifiable wrapper.
//
//
// Tom Coppeto
// Okapia
// 20 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an Identifier wrapper.
 */

public abstract class AbstractAdapterIdentifiable
    extends net.okapia.osid.jamocha.spi.AbstractIdentifiable
    implements org.osid.Identifiable {

    private final org.osid.Identifiable identifiable;

        
    /**
     *  Constructs a new <code>AbstractAdapterIdentifiable</code>.
     *
     *  @param identifiable
     *  @throws org.osid.NullArgumentException <code>identifiable</code> is
     *          <code>null</code>
     */
    
    protected AbstractAdapterIdentifiable(org.osid.Identifiable identifiable) {
        nullarg(identifiable, "identificable");
        setId(identifiable.getId());
        this.identifiable = identifiable;
        return;
    }


    /**
     *  Tests to see if the last method invoked retrieved up-to-date
     *  data.  Simple retrieval methods do not specify errors as,
     *  generally, the data is retrieved once at the time this object
     *  is instantiated. Some implementations may provide real-time
     *  data though the application may not always care. An
     *  implementation providing a real-time service may fall back to
     *  a previous snapshot in case of error. This method returns
     *  false if the data last retrieved was stale.
     *
     *  @return <code> true </code> if the last data retrieval was up to date, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isCurrent() {
        return (this.identifiable.isCurrent());
    }
}
