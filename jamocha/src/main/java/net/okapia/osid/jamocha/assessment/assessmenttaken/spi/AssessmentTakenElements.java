//
// AssessmentTakenElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmenttaken.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class AssessmentTakenElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the AssessmentTakenElement Id.
     *
     *  @return the assessment taken element Id
     */

    public static org.osid.id.Id getAssessmentTakenEntityId() {
        return (makeEntityId("osid.assessment.AssessmentTaken"));
    }


    /**
     *  Gets the AssessmentOfferedId element Id.
     *
     *  @return the AssessmentOfferedId element Id
     */

    public static org.osid.id.Id getAssessmentOfferedId() {
        return (makeElementId("osid.assessment.assessmenttaken.AssessmentOfferedId"));
    }


    /**
     *  Gets the AssessmentOffered element Id.
     *
     *  @return the AssessmentOffered element Id
     */

    public static org.osid.id.Id getAssessmentOffered() {
        return (makeElementId("osid.assessment.assessmenttaken.AssessmentOffered"));
    }


    /**
     *  Gets the TakerId element Id.
     *
     *  @return the TakerId element Id
     */

    public static org.osid.id.Id getTakerId() {
        return (makeElementId("osid.assessment.assessmenttaken.TakerId"));
    }


    /**
     *  Gets the Taker element Id.
     *
     *  @return the Taker element Id
     */

    public static org.osid.id.Id getTaker() {
        return (makeElementId("osid.assessment.assessmenttaken.Taker"));
    }


    /**
     *  Gets the TakingAgentId element Id.
     *
     *  @return the TakingAgentId element Id
     */

    public static org.osid.id.Id getTakingAgentId() {
        return (makeElementId("osid.assessment.assessmenttaken.TakingAgentId"));
    }


    /**
     *  Gets the TakingAgent element Id.
     *
     *  @return the TakingAgent element Id
     */

    public static org.osid.id.Id getTakingAgent() {
        return (makeElementId("osid.assessment.assessmenttaken.TakingAgent"));
    }


    /**
     *  Gets the ActualStartTime element Id.
     *
     *  @return the ActualStartTime element Id
     */

    public static org.osid.id.Id getActualStartTime() {
        return (makeElementId("osid.assessment.assessmenttaken.ActualStartTime"));
    }


    /**
     *  Gets the CompletionTime element Id.
     *
     *  @return the CompletionTime element Id
     */

    public static org.osid.id.Id getCompletionTime() {
        return (makeElementId("osid.assessment.assessmenttaken.CompletionTime"));
    }


    /**
     *  Gets the TimeSpent element Id.
     *
     *  @return the TimeSpent element Id
     */

    public static org.osid.id.Id getTimeSpent() {
        return (makeElementId("osid.assessment.assessmenttaken.TimeSpent"));
    }


    /**
     *  Gets the Completion element Id.
     *
     *  @return the Completion element Id
     */

    public static org.osid.id.Id getCompletion() {
        return (makeElementId("osid.assessment.assessmenttaken.Completion"));
    }


    /**
     *  Gets the ScoreSystemId element Id.
     *
     *  @return the ScoreSystemId element Id
     */

    public static org.osid.id.Id getScoreSystemId() {
        return (makeElementId("osid.assessment.assessmenttaken.ScoreSystemId"));
    }


    /**
     *  Gets the ScoreSystem element Id.
     *
     *  @return the ScoreSystem element Id
     */

    public static org.osid.id.Id getScoreSystem() {
        return (makeElementId("osid.assessment.assessmenttaken.ScoreSystem"));
    }


    /**
     *  Gets the Score element Id.
     *
     *  @return the Score element Id
     */

    public static org.osid.id.Id getScore() {
        return (makeElementId("osid.assessment.assessmenttaken.Score"));
    }


    /**
     *  Gets the GradeId element Id.
     *
     *  @return the GradeId element Id
     */

    public static org.osid.id.Id getGradeId() {
        return (makeElementId("osid.assessment.assessmenttaken.GradeId"));
    }


    /**
     *  Gets the Grade element Id.
     *
     *  @return the Grade element Id
     */

    public static org.osid.id.Id getGrade() {
        return (makeElementId("osid.assessment.assessmenttaken.Grade"));
    }


    /**
     *  Gets the Feedback element Id.
     *
     *  @return the Feedback element Id
     */

    public static org.osid.id.Id getFeedback() {
        return (makeElementId("osid.assessment.assessmenttaken.Feedback"));
    }


    /**
     *  Gets the RubricId element Id.
     *
     *  @return the RubricId element Id
     */

    public static org.osid.id.Id getRubricId() {
        return (makeElementId("osid.assessment.assessmenttaken.RubricId"));
    }


    /**
     *  Gets the Rubric element Id.
     *
     *  @return the Rubric element Id
     */

    public static org.osid.id.Id getRubric() {
        return (makeElementId("osid.assessment.assessmenttaken.Rubric"));
    }


    /**
     *  Gets the Scored element Id.
     *
     *  @return the Scored element Id
     */

    public static org.osid.id.Id getScored() {
        return (makeQueryElementId("osid.assessment.assessmenttaken.Scored"));
    }


    /**
     *  Gets the BankId element Id.
     *
     *  @return the BankId element Id
     */

    public static org.osid.id.Id getBankId() {
        return (makeQueryElementId("osid.assessment.assessmenttaken.BankId"));
    }


    /**
     *  Gets the Bank element Id.
     *
     *  @return the Bank element Id
     */

    public static org.osid.id.Id getBank() {
        return (makeQueryElementId("osid.assessment.assessmenttaken.Bank"));
    }
}
