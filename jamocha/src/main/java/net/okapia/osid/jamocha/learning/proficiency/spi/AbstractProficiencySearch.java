//
// AbstractProficiencySearch.java
//
//     A template for making a Proficiency Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.proficiency.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing proficiency searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProficiencySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.learning.ProficiencySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.learning.records.ProficiencySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.learning.ProficiencySearchOrder proficiencySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of proficiencies. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  proficiencyIds list of proficiencies
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProficiencies(org.osid.id.IdList proficiencyIds) {
        while (proficiencyIds.hasNext()) {
            try {
                this.ids.add(proficiencyIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProficiencies</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of proficiency Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProficiencyIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  proficiencySearchOrder proficiency search order 
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>proficiencySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProficiencyResults(org.osid.learning.ProficiencySearchOrder proficiencySearchOrder) {
	this.proficiencySearchOrder = proficiencySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.learning.ProficiencySearchOrder getProficiencySearchOrder() {
	return (this.proficiencySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given proficiency search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a proficiency implementing the requested record.
     *
     *  @param proficiencySearchRecordType a proficiency search record
     *         type
     *  @return the proficiency search record
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(proficiencySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencySearchRecord getProficiencySearchRecord(org.osid.type.Type proficiencySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.learning.records.ProficiencySearchRecord record : this.records) {
            if (record.implementsRecordType(proficiencySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proficiencySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this proficiency search. 
     *
     *  @param proficiencySearchRecord proficiency search record
     *  @param proficiencySearchRecordType proficiency search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProficiencySearchRecord(org.osid.learning.records.ProficiencySearchRecord proficiencySearchRecord, 
                                           org.osid.type.Type proficiencySearchRecordType) {

        addRecordType(proficiencySearchRecordType);
        this.records.add(proficiencySearchRecord);        
        return;
    }
}
