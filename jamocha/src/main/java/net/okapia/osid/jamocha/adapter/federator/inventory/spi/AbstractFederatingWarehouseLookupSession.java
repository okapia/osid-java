//
// AbstractFederatingWarehouseLookupSession.java
//
//     An abstract federating adapter for a WarehouseLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  WarehouseLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingWarehouseLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.inventory.WarehouseLookupSession>
    implements org.osid.inventory.WarehouseLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingWarehouseLookupSession</code>.
     */

    protected AbstractFederatingWarehouseLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.inventory.WarehouseLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Warehouse</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupWarehouses() {
        for (org.osid.inventory.WarehouseLookupSession session : getSessions()) {
            if (session.canLookupWarehouses()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Warehouse</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWarehouseView() {
        for (org.osid.inventory.WarehouseLookupSession session : getSessions()) {
            session.useComparativeWarehouseView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Warehouse</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWarehouseView() {
        for (org.osid.inventory.WarehouseLookupSession session : getSessions()) {
            session.usePlenaryWarehouseView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Warehouse</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Warehouse</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Warehouse</code> and
     *  retained for compatibility.
     *
     *  @param  warehouseId <code>Id</code> of the
     *          <code>Warehouse</code>
     *  @return the warehouse
     *  @throws org.osid.NotFoundException <code>warehouseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>warehouseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.inventory.WarehouseLookupSession session : getSessions()) {
            try {
                return (session.getWarehouse(warehouseId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(warehouseId + " not found");
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  warehouses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Warehouses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  warehouseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByIds(org.osid.id.IdList warehouseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.inventory.warehouse.MutableWarehouseList ret = new net.okapia.osid.jamocha.inventory.warehouse.MutableWarehouseList();

        try (org.osid.id.IdList ids = warehouseIds) {
            while (ids.hasNext()) {
                ret.addWarehouse(getWarehouse(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  warehouse genus <code>Type</code> which does not include
     *  warehouses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  warehouseGenusType a warehouse genus type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByGenusType(org.osid.type.Type warehouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.warehouse.FederatingWarehouseList ret = getWarehouseList();

        for (org.osid.inventory.WarehouseLookupSession session : getSessions()) {
            ret.addWarehouseList(session.getWarehousesByGenusType(warehouseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  warehouse genus <code>Type</code> and include any additional
     *  warehouses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  warehouseGenusType a warehouse genus type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByParentGenusType(org.osid.type.Type warehouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.warehouse.FederatingWarehouseList ret = getWarehouseList();

        for (org.osid.inventory.WarehouseLookupSession session : getSessions()) {
            ret.addWarehouseList(session.getWarehousesByParentGenusType(warehouseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>WarehouseList</code> containing the given
     *  warehouse record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  warehouseRecordType a warehouse record type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByRecordType(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.warehouse.FederatingWarehouseList ret = getWarehouseList();

        for (org.osid.inventory.WarehouseLookupSession session : getSessions()) {
            ret.addWarehouseList(session.getWarehousesByRecordType(warehouseRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>WarehouseList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known warehouses or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  warehouses that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Warehouse</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.inventory.warehouse.FederatingWarehouseList ret = getWarehouseList();

        for (org.osid.inventory.WarehouseLookupSession session : getSessions()) {
            ret.addWarehouseList(session.getWarehousesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Warehouses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Warehouses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.warehouse.FederatingWarehouseList ret = getWarehouseList();

        for (org.osid.inventory.WarehouseLookupSession session : getSessions()) {
            ret.addWarehouseList(session.getWarehouses());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.inventory.warehouse.FederatingWarehouseList getWarehouseList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.warehouse.ParallelWarehouseList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.warehouse.CompositeWarehouseList());
        }
    }
}
