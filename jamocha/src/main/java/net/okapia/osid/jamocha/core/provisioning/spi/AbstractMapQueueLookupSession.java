//
// AbstractMapQueueLookupSession
//
//    A simple framework for providing a Queue lookup service
//    backed by a fixed collection of queues.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Queue lookup service backed by a
 *  fixed collection of queues. The queues are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Queues</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapQueueLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractQueueLookupSession
    implements org.osid.provisioning.QueueLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.Queue> queues = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.Queue>());


    /**
     *  Makes a <code>Queue</code> available in this session.
     *
     *  @param  queue a queue
     *  @throws org.osid.NullArgumentException <code>queue<code>
     *          is <code>null</code>
     */

    protected void putQueue(org.osid.provisioning.Queue queue) {
        this.queues.put(queue.getId(), queue);
        return;
    }


    /**
     *  Makes an array of queues available in this session.
     *
     *  @param  queues an array of queues
     *  @throws org.osid.NullArgumentException <code>queues<code>
     *          is <code>null</code>
     */

    protected void putQueues(org.osid.provisioning.Queue[] queues) {
        putQueues(java.util.Arrays.asList(queues));
        return;
    }


    /**
     *  Makes a collection of queues available in this session.
     *
     *  @param  queues a collection of queues
     *  @throws org.osid.NullArgumentException <code>queues<code>
     *          is <code>null</code>
     */

    protected void putQueues(java.util.Collection<? extends org.osid.provisioning.Queue> queues) {
        for (org.osid.provisioning.Queue queue : queues) {
            this.queues.put(queue.getId(), queue);
        }

        return;
    }


    /**
     *  Removes a Queue from this session.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @throws org.osid.NullArgumentException <code>queueId<code> is
     *          <code>null</code>
     */

    protected void removeQueue(org.osid.id.Id queueId) {
        this.queues.remove(queueId);
        return;
    }


    /**
     *  Gets the <code>Queue</code> specified by its <code>Id</code>.
     *
     *  @param  queueId <code>Id</code> of the <code>Queue</code>
     *  @return the queue
     *  @throws org.osid.NotFoundException <code>queueId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>queueId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Queue getQueue(org.osid.id.Id queueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.Queue queue = this.queues.get(queueId);
        if (queue == null) {
            throw new org.osid.NotFoundException("queue not found: " + queueId);
        }

        return (queue);
    }


    /**
     *  Gets all <code>Queues</code>. In plenary mode, the returned
     *  list contains all known queues or an error
     *  results. Otherwise, the returned list may contain only those
     *  queues that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Queues</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.QueueList getQueues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.queue.ArrayQueueList(this.queues.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.queues.clear();
        super.close();
        return;
    }
}
