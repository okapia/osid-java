//
// MutableMapBinLookupSession
//
//    Implements a Bin lookup service backed by a collection of
//    bins that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource;


/**
 *  Implements a Bin lookup service backed by a collection of
 *  bins. The bins are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of bins can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapBinLookupSession
    extends net.okapia.osid.jamocha.core.resource.spi.AbstractMapBinLookupSession
    implements org.osid.resource.BinLookupSession {


    /**
     *  Constructs a new {@code MutableMapBinLookupSession}
     *  with no bins.
     */

    public MutableMapBinLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBinLookupSession} with a
     *  single bin.
     *  
     *  @param bin a bin
     *  @throws org.osid.NullArgumentException {@code bin}
     *          is {@code null}
     */

    public MutableMapBinLookupSession(org.osid.resource.Bin bin) {
        putBin(bin);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBinLookupSession}
     *  using an array of bins.
     *
     *  @param bins an array of bins
     *  @throws org.osid.NullArgumentException {@code bins}
     *          is {@code null}
     */

    public MutableMapBinLookupSession(org.osid.resource.Bin[] bins) {
        putBins(bins);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBinLookupSession}
     *  using a collection of bins.
     *
     *  @param bins a collection of bins
     *  @throws org.osid.NullArgumentException {@code bins}
     *          is {@code null}
     */

    public MutableMapBinLookupSession(java.util.Collection<? extends org.osid.resource.Bin> bins) {
        putBins(bins);
        return;
    }

    
    /**
     *  Makes a {@code Bin} available in this session.
     *
     *  @param bin a bin
     *  @throws org.osid.NullArgumentException {@code bin{@code  is
     *          {@code null}
     */

    @Override
    public void putBin(org.osid.resource.Bin bin) {
        super.putBin(bin);
        return;
    }


    /**
     *  Makes an array of bins available in this session.
     *
     *  @param bins an array of bins
     *  @throws org.osid.NullArgumentException {@code bins{@code 
     *          is {@code null}
     */

    @Override
    public void putBins(org.osid.resource.Bin[] bins) {
        super.putBins(bins);
        return;
    }


    /**
     *  Makes collection of bins available in this session.
     *
     *  @param bins a collection of bins
     *  @throws org.osid.NullArgumentException {@code bins{@code  is
     *          {@code null}
     */

    @Override
    public void putBins(java.util.Collection<? extends org.osid.resource.Bin> bins) {
        super.putBins(bins);
        return;
    }


    /**
     *  Removes a Bin from this session.
     *
     *  @param binId the {@code Id} of the bin
     *  @throws org.osid.NullArgumentException {@code binId{@code 
     *          is {@code null}
     */

    @Override
    public void removeBin(org.osid.id.Id binId) {
        super.removeBin(binId);
        return;
    }    
}
