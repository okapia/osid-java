//
// CalendarElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.calendar.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CalendarElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the CalendarElement Id.
     *
     *  @return the calendar element Id
     */

    public static org.osid.id.Id getCalendarEntityId() {
        return (makeEntityId("osid.calendaring.Calendar"));
    }


    /**
     *  Gets the EventId element Id.
     *
     *  @return the EventId element Id
     */

    public static org.osid.id.Id getEventId() {
        return (makeQueryElementId("osid.calendaring.calendar.EventId"));
    }


    /**
     *  Gets the Event element Id.
     *
     *  @return the Event element Id
     */

    public static org.osid.id.Id getEvent() {
        return (makeQueryElementId("osid.calendaring.calendar.Event"));
    }


    /**
     *  Gets the TimePeriodId element Id.
     *
     *  @return the TimePeriodId element Id
     */

    public static org.osid.id.Id getTimePeriodId() {
        return (makeQueryElementId("osid.calendaring.calendar.TimePeriodId"));
    }


    /**
     *  Gets the TimePeriod element Id.
     *
     *  @return the TimePeriod element Id
     */

    public static org.osid.id.Id getTimePeriod() {
        return (makeQueryElementId("osid.calendaring.calendar.TimePeriod"));
    }


    /**
     *  Gets the CommitmentId element Id.
     *
     *  @return the CommitmentId element Id
     */

    public static org.osid.id.Id getCommitmentId() {
        return (makeQueryElementId("osid.calendaring.calendar.CommitmentId"));
    }


    /**
     *  Gets the Commitment element Id.
     *
     *  @return the Commitment element Id
     */

    public static org.osid.id.Id getCommitment() {
        return (makeQueryElementId("osid.calendaring.calendar.Commitment"));
    }


    /**
     *  Gets the AncestorCalendarId element Id.
     *
     *  @return the AncestorCalendarId element Id
     */

    public static org.osid.id.Id getAncestorCalendarId() {
        return (makeQueryElementId("osid.calendaring.calendar.AncestorCalendarId"));
    }


    /**
     *  Gets the AncestorCalendar element Id.
     *
     *  @return the AncestorCalendar element Id
     */

    public static org.osid.id.Id getAncestorCalendar() {
        return (makeQueryElementId("osid.calendaring.calendar.AncestorCalendar"));
    }


    /**
     *  Gets the DescendantCalendarId element Id.
     *
     *  @return the DescendantCalendarId element Id
     */

    public static org.osid.id.Id getDescendantCalendarId() {
        return (makeQueryElementId("osid.calendaring.calendar.DescendantCalendarId"));
    }


    /**
     *  Gets the DescendantCalendar element Id.
     *
     *  @return the DescendantCalendar element Id
     */

    public static org.osid.id.Id getDescendantCalendar() {
        return (makeQueryElementId("osid.calendaring.calendar.DescendantCalendar"));
    }
}
