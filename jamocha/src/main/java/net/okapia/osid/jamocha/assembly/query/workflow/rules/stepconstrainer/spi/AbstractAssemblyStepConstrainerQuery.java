//
// AbstractAssemblyStepConstrainerQuery.java
//
//     A StepConstrainerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.workflow.rules.stepconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StepConstrainerQuery that stores terms.
 */

public abstract class AbstractAssemblyStepConstrainerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidConstrainerQuery
    implements org.osid.workflow.rules.StepConstrainerQuery,
               org.osid.workflow.rules.StepConstrainerQueryInspector,
               org.osid.workflow.rules.StepConstrainerSearchOrder {

    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyStepConstrainerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyStepConstrainerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to a step. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuledStepId(org.osid.id.Id stepId, boolean match) {
        getAssembler().addIdTerm(getRuledStepIdColumn(), stepId, match);
        return;
    }


    /**
     *  Clears the step <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledStepIdTerms() {
        getAssembler().clearTerms(getRuledStepIdColumn());
        return;
    }


    /**
     *  Gets the step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledStepIdTerms() {
        return (getAssembler().getIdTerms(getRuledStepIdColumn()));
    }


    /**
     *  Gets the RuledStepId column name.
     *
     * @return the column name
     */

    protected String getRuledStepIdColumn() {
        return ("ruled_step_id");
    }


    /**
     *  Tests if a <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if a step query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledStepQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getRuledStepQuery() {
        throw new org.osid.UnimplementedException("supportsRuledStepQuery() is false");
    }


    /**
     *  Matches constrainers mapped to any step. 
     *
     *  @param  match <code> true </code> for constrainers mapped to any step, 
     *          <code> false </code> to match constrainers mapped to no steps 
     */

    @OSID @Override
    public void matchAnyRuledStep(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledStepColumn(), match);
        return;
    }


    /**
     *  Clears the step query terms. 
     */

    @OSID @Override
    public void clearRuledStepTerms() {
        getAssembler().clearTerms(getRuledStepColumn());
        return;
    }


    /**
     *  Gets the step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getRuledStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Gets the RuledStep column name.
     *
     * @return the column name
     */

    protected String getRuledStepColumn() {
        return ("ruled_step");
    }


    /**
     *  Matches constrainers mapped to the office. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        getAssembler().addIdTerm(getOfficeIdColumn(), officeId, match);
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        getAssembler().clearTerms(getOfficeIdColumn());
        return;
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (getAssembler().getIdTerms(getOfficeIdColumn()));
    }


    /**
     *  Gets the OfficeId column name.
     *
     * @return the column name
     */

    protected String getOfficeIdColumn() {
        return ("office_id");
    }


    /**
     *  Tests if an <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if an office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        getAssembler().clearTerms(getOfficeColumn());
        return;
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the Office column name.
     *
     * @return the column name
     */

    protected String getOfficeColumn() {
        return ("office");
    }


    /**
     *  Tests if this stepConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  stepConstrainerRecordType a step constrainer record type 
     *  @return <code>true</code> if the stepConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type stepConstrainerRecordType) {
        for (org.osid.workflow.rules.records.StepConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stepConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  stepConstrainerRecordType the step constrainer record type 
     *  @return the step constrainer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerQueryRecord getStepConstrainerQueryRecord(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stepConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  stepConstrainerRecordType the step constrainer record type 
     *  @return the step constrainer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerQueryInspectorRecord getStepConstrainerQueryInspectorRecord(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(stepConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param stepConstrainerRecordType the step constrainer record type
     *  @return the step constrainer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerSearchOrderRecord getStepConstrainerSearchOrderRecord(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(stepConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this step constrainer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param stepConstrainerQueryRecord the step constrainer query record
     *  @param stepConstrainerQueryInspectorRecord the step constrainer query inspector
     *         record
     *  @param stepConstrainerSearchOrderRecord the step constrainer search order record
     *  @param stepConstrainerRecordType step constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerQueryRecord</code>,
     *          <code>stepConstrainerQueryInspectorRecord</code>,
     *          <code>stepConstrainerSearchOrderRecord</code> or
     *          <code>stepConstrainerRecordTypestepConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addStepConstrainerRecords(org.osid.workflow.rules.records.StepConstrainerQueryRecord stepConstrainerQueryRecord, 
                                      org.osid.workflow.rules.records.StepConstrainerQueryInspectorRecord stepConstrainerQueryInspectorRecord, 
                                      org.osid.workflow.rules.records.StepConstrainerSearchOrderRecord stepConstrainerSearchOrderRecord, 
                                      org.osid.type.Type stepConstrainerRecordType) {

        addRecordType(stepConstrainerRecordType);

        nullarg(stepConstrainerQueryRecord, "step constrainer query record");
        nullarg(stepConstrainerQueryInspectorRecord, "step constrainer query inspector record");
        nullarg(stepConstrainerSearchOrderRecord, "step constrainer search odrer record");

        this.queryRecords.add(stepConstrainerQueryRecord);
        this.queryInspectorRecords.add(stepConstrainerQueryInspectorRecord);
        this.searchOrderRecords.add(stepConstrainerSearchOrderRecord);
        
        return;
    }
}
