//
// LogEntryMiter.java
//
//     Defines a LogEntry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.logging.logentry;


/**
 *  Defines a <code>LogEntry</code> miter for use with the builders.
 */

public interface LogEntryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.logging.LogEntry {


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @throws org.osid.NullArgumentException <code>priority</code>
     *          is <code>null</code>
     */

    public void setPriority(org.osid.type.Type priority);


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    public void setTimestamp(org.osid.calendaring.DateTime timestamp);


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setAgent(org.osid.authentication.Agent agent);


    /**
     *  Adds a LogEntry record.
     *
     *  @param record a logEntry record
     *  @param recordType the type of logEntry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addLogEntryRecord(org.osid.logging.records.LogEntryRecord record, org.osid.type.Type recordType);
}       


