//
// AbstractFederatingCanonicalUnitLookupSession.java
//
//     An abstract federating adapter for a CanonicalUnitLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CanonicalUnitLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCanonicalUnitLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.offering.CanonicalUnitLookupSession>
    implements org.osid.offering.CanonicalUnitLookupSession {

    private boolean parallel = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();


    /**
     *  Constructs a new <code>AbstractFederatingCanonicalUnitLookupSession</code>.
     */

    protected AbstractFederatingCanonicalUnitLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.offering.CanonicalUnitLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>CanonicalUnit</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCanonicalUnits() {
        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            if (session.canLookupCanonicalUnits()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>CanonicalUnit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCanonicalUnitView() {
        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            session.useComparativeCanonicalUnitView();
        }

        return;
    }


    /**
     *  A complete view of the <code>CanonicalUnit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCanonicalUnitView() {
        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            session.usePlenaryCanonicalUnitView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical units in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            session.useFederatedCatalogueView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            session.useIsolatedCatalogueView();
        }

        return;
    }


    /**
     *  Only active canonical units are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitView() {
        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            session.useActiveCanonicalUnitView();
        }

        return;
    }


    /**
     *  Active and inactive canonical units are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitView() {
        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            session.useAnyStatusCanonicalUnitView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>CanonicalUnit</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CanonicalUnit</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CanonicalUnit</code> and
     *  retained for compatibility.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitId <code>Id</code> of the
     *          <code>CanonicalUnit</code>
     *  @return the canonical unit
     *  @throws org.osid.NotFoundException <code>canonicalUnitId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnit getCanonicalUnit(org.osid.id.Id canonicalUnitId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            try {
                return (session.getCanonicalUnit(canonicalUnitId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(canonicalUnitId + " not found");
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnits specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>CanonicalUnits</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, canonical units are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical units are returned.
     *
     *  @param  canonicalUnitIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByIds(org.osid.id.IdList canonicalUnitIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.offering.canonicalunit.MutableCanonicalUnitList ret = new net.okapia.osid.jamocha.offering.canonicalunit.MutableCanonicalUnitList();

        try (org.osid.id.IdList ids = canonicalUnitIds) {
            while (ids.hasNext()) {
                ret.addCanonicalUnit(getCanonicalUnit(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the
     *  given canonical unit genus <code>Type</code> which does not
     *  include canonical units of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned
     *  list may contain only those canonical units that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, canonical units are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical units are returned.
     *
     *  @param  canonicalUnitGenusType a canonicalUnit genus type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByGenusType(org.osid.type.Type canonicalUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.canonicalunit.FederatingCanonicalUnitList ret = getCanonicalUnitList();

        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            ret.addCanonicalUnitList(session.getCanonicalUnitsByGenusType(canonicalUnitGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the
     *  given canonical unit genus <code>Type</code> and include any
     *  additional canonical units with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned
     *  list may contain only those canonical units that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, canonical units are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical units are returned.
     *
     *  @param  canonicalUnitGenusType a canonicalUnit genus type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByParentGenusType(org.osid.type.Type canonicalUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.canonicalunit.FederatingCanonicalUnitList ret = getCanonicalUnitList();

        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            ret.addCanonicalUnitList(session.getCanonicalUnitsByParentGenusType(canonicalUnitGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> containing the given
     *  canonical unit record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned
     *  list may contain only those canonical units that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, canonical units are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical units are returned.
     *
     *  @param  canonicalUnitRecordType a canonicalUnit record type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByRecordType(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.canonicalunit.FederatingCanonicalUnitList ret = getCanonicalUnitList();

        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            ret.addCanonicalUnitList(session.getCanonicalUnitsByRecordType(canonicalUnitRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets canonical units by code. 
     *  
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned
     *  list may contain only those canonical units that are
     *  accessible through this session.
     *  
     *  In active mode, canonicals are returned that are currently
     *  active. In any status mode, active and inactive canonicals are
     *  returned.
     *
     *  @param  code a code 
     *  @return a list of canonical units 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByCode(String code)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.canonicalunit.FederatingCanonicalUnitList ret = getCanonicalUnitList();

        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            ret.addCanonicalUnitList(session.getCanonicalUnitsByCode(code));
        }

        ret.noMore();
        return (ret);
    }
    

    /**
     *  Gets all <code>CanonicalUnits</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned
     *  list may contain only those canonical units that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, canonical units are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical units are returned.
     *
     *  @return a list of <code>CanonicalUnits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.canonicalunit.FederatingCanonicalUnitList ret = getCanonicalUnitList();

        for (org.osid.offering.CanonicalUnitLookupSession session : getSessions()) {
            ret.addCanonicalUnitList(session.getCanonicalUnits());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.offering.canonicalunit.FederatingCanonicalUnitList getCanonicalUnitList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.canonicalunit.ParallelCanonicalUnitList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.canonicalunit.CompositeCanonicalUnitList());
        }
    }
}
