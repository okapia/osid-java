//
// AbstractRule.java
//
//     Defines a Rule builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.rules.evaluation.spi;


/**
 *  Defines a <code>Rule</code> builder.
 */

public abstract class AbstractRuleBuilder<T extends AbstractRuleBuilder<T>>
    extends net.okapia.osid.jamocha.builder.rules.rule.spi.AbstractRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.assembly.rules.evaluation.RuleMiter rule;


    /**
     *  Constructs a new <code>AbstractRuleBuilder</code>.
     *
     *  @param rule the rule to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRuleBuilder(net.okapia.osid.jamocha.assembly.rules.evaluation.RuleMiter rule) {
        super(rule);
        this.rule = rule;
        return;
    }


    /**
     *  Sets the term.
     *
     *  @param term the root term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public T term(net.okapia.osid.jamocha.assembly.rules.evaluation.Term term) {
        this.rule.setTerm(term);
        return (self());
    }

    
    /**
     *  Builds the rule.
     *
     *  @return the new rule.
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>rule</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.rules.Rule build() {
        (new net.okapia.osid.jamocha.builder.validator.rules.rule.RuleValidator()).validate(this.rule);
        return (new net.okapia.osid.jamocha.assembly.rules.evaluation.ImmutableRule(this.rule));
    }


    /**
     *  Gets the rule. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new rule
     */

    @Override
    public net.okapia.osid.jamocha.assembly.rules.evaluation.RuleMiter getMiter() {
        return (this.rule);
    }
}       
