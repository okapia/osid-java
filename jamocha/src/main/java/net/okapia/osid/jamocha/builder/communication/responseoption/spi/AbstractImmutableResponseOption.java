//
// AbstractImmutableResponseOption.java
//
//     Wraps a mutable ResponseOption to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.communication.responseoption.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ResponseOption</code> to hide modifiers. This
 *  wrapper provides an immutized ResponseOption from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying responseOption whose state changes are visible.
 */

public abstract class AbstractImmutableResponseOption
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.communication.ResponseOption {

    private final org.osid.communication.ResponseOption responseOption;


    /**
     *  Constructs a new <code>AbstractImmutableResponseOption</code>.
     *
     *  @param responseOption the response option to immutablize
     *  @throws org.osid.NullArgumentException <code>responseOption</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableResponseOption(org.osid.communication.ResponseOption responseOption) {
        super(responseOption);
        this.responseOption = responseOption;
        return;
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  responseOptionRecordType the response option record type 
     *  @return the response option record 
     *  @throws org.osid.NullArgumentException
     *          <code>responseOptionRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(responseOptionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.communication.records.ResponseOptionRecord getResponseOptionRecord(org.osid.type.Type responseOptionRecordType)
        throws org.osid.OperationFailedException {

        return (this.responseOption.getResponseOptionRecord(responseOptionRecordType));
    }
}

