//
// AbstractAdapterQueueProcessorEnablerLookupSession.java
//
//    A QueueProcessorEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A QueueProcessorEnabler lookup session adapter.
 */

public abstract class AbstractAdapterQueueProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.QueueProcessorEnablerLookupSession {

    private final org.osid.provisioning.rules.QueueProcessorEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterQueueProcessorEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterQueueProcessorEnablerLookupSession(org.osid.provisioning.rules.QueueProcessorEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code QueueProcessorEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupQueueProcessorEnablers() {
        return (this.session.canLookupQueueProcessorEnablers());
    }


    /**
     *  A complete view of the {@code QueueProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueProcessorEnablerView() {
        this.session.useComparativeQueueProcessorEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code QueueProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueProcessorEnablerView() {
        this.session.usePlenaryQueueProcessorEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue processor enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active queue processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveQueueProcessorEnablerView() {
        this.session.useActiveQueueProcessorEnablerView();
        return;
    }


    /**
     *  Active and inactive queue processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueProcessorEnablerView() {
        this.session.useAnyStatusQueueProcessorEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code QueueProcessorEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code QueueProcessorEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code QueueProcessorEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, queue processor enablers are returned that are currently
     *  active. In any status mode, active and inactive queue processor enablers
     *  are returned.
     *
     *  @param queueProcessorEnablerId {@code Id} of the {@code QueueProcessorEnabler}
     *  @return the queue processor enabler
     *  @throws org.osid.NotFoundException {@code queueProcessorEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code queueProcessorEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnabler getQueueProcessorEnabler(org.osid.id.Id queueProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorEnabler(queueProcessorEnablerId));
    }


    /**
     *  Gets a {@code QueueProcessorEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  queueProcessorEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code QueueProcessorEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, queue processor enablers are returned that are currently
     *  active. In any status mode, active and inactive queue processor enablers
     *  are returned.
     *
     *  @param  queueProcessorEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code QueueProcessorEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerList getQueueProcessorEnablersByIds(org.osid.id.IdList queueProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorEnablersByIds(queueProcessorEnablerIds));
    }


    /**
     *  Gets a {@code QueueProcessorEnablerList} corresponding to the given
     *  queue processor enabler genus {@code Type} which does not include
     *  queue processor enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  queue processor enablers or an error results. Otherwise, the returned list
     *  may contain only those queue processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue processor enablers are returned that are currently
     *  active. In any status mode, active and inactive queue processor enablers
     *  are returned.
     *
     *  @param  queueProcessorEnablerGenusType a queueProcessorEnabler genus type 
     *  @return the returned {@code QueueProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerList getQueueProcessorEnablersByGenusType(org.osid.type.Type queueProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorEnablersByGenusType(queueProcessorEnablerGenusType));
    }


    /**
     *  Gets a {@code QueueProcessorEnablerList} corresponding to the given
     *  queue processor enabler genus {@code Type} and include any additional
     *  queue processor enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  queue processor enablers or an error results. Otherwise, the returned list
     *  may contain only those queue processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue processor enablers are returned that are currently
     *  active. In any status mode, active and inactive queue processor enablers
     *  are returned.
     *
     *  @param  queueProcessorEnablerGenusType a queueProcessorEnabler genus type 
     *  @return the returned {@code QueueProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerList getQueueProcessorEnablersByParentGenusType(org.osid.type.Type queueProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorEnablersByParentGenusType(queueProcessorEnablerGenusType));
    }


    /**
     *  Gets a {@code QueueProcessorEnablerList} containing the given
     *  queue processor enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  queue processor enablers or an error results. Otherwise, the returned list
     *  may contain only those queue processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue processor enablers are returned that are currently
     *  active. In any status mode, active and inactive queue processor enablers
     *  are returned.
     *
     *  @param  queueProcessorEnablerRecordType a queueProcessorEnabler record type 
     *  @return the returned {@code QueueProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerList getQueueProcessorEnablersByRecordType(org.osid.type.Type queueProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorEnablersByRecordType(queueProcessorEnablerRecordType));
    }


    /**
     *  Gets a {@code QueueProcessorEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  queue processor enablers or an error results. Otherwise, the returned list
     *  may contain only those queue processor enablers that are accessible
     *  through this session.
     *  
     *  In active mode, queue processor enablers are returned that are currently
     *  active. In any status mode, active and inactive queue processor enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code QueueProcessorEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerList getQueueProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code QueueProcessorEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  queue processor enablers or an error results. Otherwise, the returned list
     *  may contain only those queue processor enablers that are accessible
     *  through this session.
     *
     *  In active mode, queue processor enablers are returned that are currently
     *  active. In any status mode, active and inactive queue processor enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code QueueProcessorEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerList getQueueProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getQueueProcessorEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code QueueProcessorEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  queue processor enablers or an error results. Otherwise, the returned list
     *  may contain only those queue processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue processor enablers are returned that are currently
     *  active. In any status mode, active and inactive queue processor enablers
     *  are returned.
     *
     *  @return a list of {@code QueueProcessorEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerList getQueueProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorEnablers());
    }
}
