//
// AbstractImmutableCredential.java
//
//     Wraps a mutable Credential to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.credential.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Credential</code> to hide modifiers. This
 *  wrapper provides an immutized Credential from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying credential whose state changes are visible.
 */

public abstract class AbstractImmutableCredential
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.course.program.Credential {

    private final org.osid.course.program.Credential credential;


    /**
     *  Constructs a new <code>AbstractImmutableCredential</code>.
     *
     *  @param credential the credential to immutablize
     *  @throws org.osid.NullArgumentException <code>credential</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCredential(org.osid.course.program.Credential credential) {
        super(credential);
        this.credential = credential;
        return;
    }


    /**
     *  Tests if this credential has a limited lifetime. 
     *
     *  @return <code> true </code> if this credential expires, <code> false 
     *          </code> if permanent 
     */

    @OSID @Override
    public boolean hasLifeTime() {
        return (this.credential.hasLifeTime());
    }


    /**
     *  Gets the lifetime of this credential once awarded. 
     *
     *  @return the lifetime 
     *  @throws org.osid.IllegalStateException <code> hasLifetime() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getLifetime() {
        return (this.credential.getLifetime());
    }


    /**
     *  Gets the credential record corresponding to the given <code> 
     *  Credential </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  credentialRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(credentialRecordType) </code> is <code> true </code> . 
     *
     *  @param  credentialRecordType the type of credential record to retrieve 
     *  @return the credential record 
     *  @throws org.osid.NullArgumentException <code> credentialRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(credentialRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.records.CredentialRecord getCredentialRecord(org.osid.type.Type credentialRecordType)
        throws org.osid.OperationFailedException {

        return (this.credential.getCredentialRecord(credentialRecordType));
    }
}

