//
// AbstractScheduleLookupSession.java
//
//    A starter implementation framework for providing a Schedule
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Schedule
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSchedules(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractScheduleLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.ScheduleLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }

    /**
     *  Tests if this user can perform <code>Schedule</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSchedules() {
        return (true);
    }


    /**
     *  A complete view of the <code>Schedule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeScheduleView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Schedule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryScheduleView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include schedules in calendars which are
     *  children of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Schedule</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Schedule</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Schedule</code> and
     *  retained for compatibility.
     *
     *  @param  scheduleId <code>Id</code> of the
     *          <code>Schedule</code>
     *  @return the schedule
     *  @throws org.osid.NotFoundException <code>scheduleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>scheduleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Schedule getSchedule(org.osid.id.Id scheduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.ScheduleList schedules = getSchedules()) {
            while (schedules.hasNext()) {
                org.osid.calendaring.Schedule schedule = schedules.getNextSchedule();
                if (schedule.getId().equals(scheduleId)) {
                    return (schedule);
                }
            }
        } 

        throw new org.osid.NotFoundException(scheduleId + " not found");
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  schedules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Schedules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSchedules()</code>.
     *
     *  @param  scheduleIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByIds(org.osid.id.IdList scheduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.Schedule> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = scheduleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSchedule(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("schedule " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.schedule.LinkedScheduleList(ret));
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  schedule genus <code>Type</code> which does not include
     *  schedules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSchedules()</code>.
     *
     *  @param  scheduleGenusType a schedule genus type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByGenusType(org.osid.type.Type scheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.schedule.ScheduleGenusFilterList(getSchedules(), scheduleGenusType));
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  schedule genus <code>Type</code> and include any additional
     *  schedules with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSchedules()</code>.
     *
     *  @param  scheduleGenusType a schedule genus type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByParentGenusType(org.osid.type.Type scheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSchedulesByGenusType(scheduleGenusType));
    }


    /**
     *  Gets a <code>ScheduleList</code> containing the given
     *  schedule record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSchedules()</code>.
     *
     *  @param  scheduleRecordType a schedule record type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByRecordType(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.schedule.ScheduleRecordFilterList(getSchedules(), scheduleRecordType));
    }


    /**
     *  Gets a <code> ScheduleList </code> directly containing the
     *  given shedule slot. <code> </code> In plenary mode, the
     *  returned list contains all known schedule or an error
     *  results. Otherwise, the returned list may contain only those
     *  schedule that are accessible through this session.
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code> 
     *  @return the returned <code> Schedule </code> list 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.schedule.ScheduleFilterList(new ScheduleSlotFilter(scheduleSlotId), getSchedules()));
    }


    /**
     *  Gets a <code> ScheduleList </code> containing the given
     *  location.  <code> </code> In plenary mode, the returned list
     *  contains all known schedule or an error results. Otherwise,
     *  the returned list may contain only those schedule that are
     *  accessible through this session.
     *
     *  @param  locationId a location <code> Id </code>
     *  @return the returned <code> Schedule </code> list
     *  @throws org.osid.NullArgumentException <code> locationId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.schedule.ScheduleFilterList(new LocationFilter(locationId), getSchedules()));
    }


    /**
     *  Gets a <code>ScheduleList</code> containing the given date. In
     *  plenary mode, the returned list contains all known schedule or
     *  an error results. Otherwise, the returned list may contain
     *  only those schedule that are accessible through this session.
     *
     *  Todo: this method only looks at the span of the schedule, n0t
     *  whether the date falls on a slot.
     *
     *  @param  date a date
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByDate(org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.Schedule> ret = new java.util.ArrayList<>();

        try (org.osid.calendaring.ScheduleList schedules = getSchedules()) {
            while (schedules.hasNext()) {
                org.osid.calendaring.Schedule schedule = schedules.getNextSchedule();
                org.osid.calendaring.DateTime start;
                org.osid.calendaring.DateTime end;
                if (schedule.hasTimePeriod()) {
                    org.osid.calendaring.TimePeriod period = schedule.getTimePeriod();
                    start = period.getStart();
                    end   = period.getEnd();
                } else {
                    start = schedule.getScheduleStart();
                    end   = schedule.getScheduleEnd();
                }

                if (start.isGreater(date) || end.isLess(start)) {
                    continue;
                }

                // this doesn't resolve schedule slots!
                ret.add(schedule);
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.schedule.LinkedScheduleList(ret));
    }


    /**
     *  Gets a <code>ScheduleList</code> contained by the given date
     *  range inclusive. In plenary mode, the returned
     *  list contains all known schedule or an error
     *  results. Otherwise, the returned list may contain only those
     *  schedule that are accessible through this session.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByDateRange(org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.Schedule> ret = new java.util.ArrayList<>();

        try (org.osid.calendaring.ScheduleList schedules = getSchedules()) {
            while (schedules.hasNext()) {
                org.osid.calendaring.Schedule schedule = schedules.getNextSchedule();
                org.osid.calendaring.DateTime start;
                org.osid.calendaring.DateTime end;
                if (schedule.hasTimePeriod()) {
                    org.osid.calendaring.TimePeriod period = schedule.getTimePeriod();
                    start = period.getStart();
                    end   = period.getEnd();
                } else {
                    start = schedule.getScheduleStart();
                    end   = schedule.getScheduleEnd();
                }

                if (start.isLess(from) || end.isGreater(end)) {
                    continue;
                }

                ret.add(schedule);
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.schedule.LinkedScheduleList(ret));
    }


    /**
     *  Gets all <code>Schedules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Schedules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the schedule list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of schedules
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.ScheduleList filterSchedulesOnViews(org.osid.calendaring.ScheduleList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }


    public static class ScheduleSlotFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.schedule.ScheduleFilter {
         
        private final org.osid.id.Id scheduleSlotId;
         
         
        /**
         *  Constructs a new <code>ScheduleSlotFilter</code>.
         *
         *  @param scheduleSlotId the schedule slot to filter
         *  @throws org.osid.NullArgumentException
         *          <code>scheduleSlotId</code> is <code>null</code>
         */
        
        public ScheduleSlotFilter(org.osid.id.Id scheduleSlotId) {
            nullarg(scheduleSlotId, "schedule slot Id");
            this.scheduleSlotId = scheduleSlotId;
            return;
        }

         
        /**
         *  Used by the ScheduleFilterList to filter the schedule list
         *  based on schedule slot.
         *
         *  @param schedule the schedule
         *  @return <code>true</code> to pass the schedule,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.Schedule schedule) {
            return (schedule.getScheduleSlotId().equals(this.scheduleSlotId));
        }
    }


    public static class LocationFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.schedule.ScheduleFilter {
         
        private final org.osid.id.Id locationId;
         
         
        /**
         *  Constructs a new <code>LocationFilter</code>.
         *
         *  @param locationId the location to filter
         *  @throws org.osid.NullArgumentException
         *          <code>locationId</code> is <code>null</code>
         */
        
        public LocationFilter(org.osid.id.Id locationId) {
            nullarg(locationId, "location Id");
            this.locationId = locationId;
            return;
        }

         
        /**
         *  Used by the ScheduleFilterList to filter the schedule list
         *  based on location.
         *
         *  @param schedule the schedule
         *  @return <code>true</code> to pass the schedule,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.Schedule schedule) {
            if (schedule.hasLocation()) {
                return (schedule.getLocationId().equals(this.locationId));
            } else {
                return (false);
            }
        }
    }
}
