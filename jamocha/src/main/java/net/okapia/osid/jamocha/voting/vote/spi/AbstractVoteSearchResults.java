//
// AbstractVoteSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.vote.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractVoteSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.voting.VoteSearchResults {

    private org.osid.voting.VoteList votes;
    private final org.osid.voting.VoteQueryInspector inspector;
    private final java.util.Collection<org.osid.voting.records.VoteSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractVoteSearchResults.
     *
     *  @param votes the result set
     *  @param voteQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>votes</code>
     *          or <code>voteQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractVoteSearchResults(org.osid.voting.VoteList votes,
                                            org.osid.voting.VoteQueryInspector voteQueryInspector) {
        nullarg(votes, "votes");
        nullarg(voteQueryInspector, "vote query inspectpr");

        this.votes = votes;
        this.inspector = voteQueryInspector;

        return;
    }


    /**
     *  Gets the vote list resulting from a search.
     *
     *  @return a vote list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotes() {
        if (this.votes == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.voting.VoteList votes = this.votes;
        this.votes = null;
	return (votes);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.voting.VoteQueryInspector getVoteQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  vote search record <code> Type. </code> This method must
     *  be used to retrieve a vote implementing the requested
     *  record.
     *
     *  @param voteSearchRecordType a vote search 
     *         record type 
     *  @return the vote search
     *  @throws org.osid.NullArgumentException
     *          <code>voteSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(voteSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.VoteSearchResultsRecord getVoteSearchResultsRecord(org.osid.type.Type voteSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.voting.records.VoteSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(voteSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(voteSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record vote search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addVoteRecord(org.osid.voting.records.VoteSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "vote record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
