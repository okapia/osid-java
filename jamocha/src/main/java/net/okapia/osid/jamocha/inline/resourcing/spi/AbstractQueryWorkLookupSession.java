//
// AbstractQueryWorkLookupSession.java
//
//    An inline adapter that maps a WorkLookupSession to
//    a WorkQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a WorkLookupSession to
 *  a WorkQuerySession.
 */

public abstract class AbstractQueryWorkLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractWorkLookupSession
    implements org.osid.resourcing.WorkLookupSession {

    private final org.osid.resourcing.WorkQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryWorkLookupSession.
     *
     *  @param querySession the underlying work query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryWorkLookupSession(org.osid.resourcing.WorkQuerySession querySession) {
        nullarg(querySession, "work query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Foundry</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform <code>Work</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupWorks() {
        return (this.session.canSearchWorks());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include works in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    
     
    /**
     *  Gets the <code>Work</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Work</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Work</code> and
     *  retained for compatibility.
     *
     *  @param  workId <code>Id</code> of the
     *          <code>Work</code>
     *  @return the work
     *  @throws org.osid.NotFoundException <code>workId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>workId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Work getWork(org.osid.id.Id workId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchId(workId, true);
        org.osid.resourcing.WorkList works = this.session.getWorksByQuery(query);
        if (works.hasNext()) {
            return (works.getNextWork());
        } 
        
        throw new org.osid.NotFoundException(workId + " not found");
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  works specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Works</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  workIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>workIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByIds(org.osid.id.IdList workIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();

        try (org.osid.id.IdList ids = workIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given
     *  work genus <code>Type</code> which does not include
     *  works of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchGenusType(workGenusType, true);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given
     *  work genus <code>Type</code> and include any additional
     *  works with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByParentGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchParentGenusType(workGenusType, true);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets a <code>WorkList</code> containing the given
     *  work record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workRecordType a work record type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByRecordType(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchRecordType(workRecordType, true);
        return (this.session.getWorksByQuery(query));
    }

    
    /**
     *  Gets a <code> WorkList </code> corresponding to the given job.
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session.
     *
     *  @param  jobId a job <code> Id </code> 
     *  @return the returned <code> Work </code> list 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchJobId(jobId, true);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets a <code> WorkList </code> of uncommitted works
     *  corresponding to the given job. In plenary mode, the returned
     *  list contains all known works or an error results. Otherwise,
     *  the returned list may contain only those works that are
     *  accessible through this session.
     *
     *  @param  jobId a job <code> Id </code> 
     *  @return the returned <code> Work </code> list 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getUncommittedWorksForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchJobId(jobId, true);
        query.matchAnyCommission(false);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets a <code> WorkList </code> of incomplete works
     *  corresponding to the given job. In plenary mode, the returned
     *  list contains all known works or an error results. Otherwise,
     *  the returned list may contain only those works that are
     *  accessible through this session.
     *
     *  @param  jobId a job <code> Id </code> 
     *  @return the returned <code> Work </code> list 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorksForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchJobId(jobId, true);
        query.matchAnyCompletionDate(true);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets a <code> WorkList </code> of all works created between
     *  the given date range inclusive.In plenary mode, the returned
     *  list contains all known works or an error results. Otherwise,
     *  the returned list may contain only those works that are
     *  accessible through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code> Work </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchCreatedDate(from, to, true);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets a <code> WorkList </code> of incomplete works created
     *  between the given date range inclusive. In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code> Work </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorksByDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchCreatedDate(from, to, true);
        query.matchAnyCompletionDate(false);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets a <code> WorkList </code> of uncommitted works created
     *  between the given date range inclusive. In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code> Work </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getUncommittedWorksByDate(org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchCreatedDate(from, to, true);
        query.matchAnyCommission(false);
        return (this.session.getWorksByQuery(query));
    }

    
    /**
     *  Gets a <code> WorkList </code> of all works of corresponding
     *  to the given job and created between the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through this
     *  session.
     *
     *  @param  jobId a job <code> Id </code> 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code> Work </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> jobId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByDateForJob(org.osid.id.Id jobId, 
                                                             org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchJobId(jobId, true);
        query.matchCreatedDate(from, to, true);
        return (this.session.getWorksByQuery(query));
    }



    /**
     *  Gets a <code> WorkList </code> of incomplete works
     *  corresponding to the given job and created between the given
     *  date range inclusive. In plenary mode, the returned list
     *  contains all known works or an error results. Otherwise, the
     *  returned list may contain only those works that are accessible
     *  through this session.
     *
     *  @param  jobId a job <code> Id </code> 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code> Work </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> jobId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorksByDateForJob(org.osid.id.Id jobId, 
                                                                       org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchJobId(jobId, true);
        query.matchAnyCompletionDate(false);
        query.matchCreatedDate(from, to, true);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets all incomplete <code> Works. </code> In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @return a list of <code> Works </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchAnyCompletionDate(false);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets all <code> Works </code> that have no commitments. In
     *  plenary mode, the returned list contains all known works or an
     *  error results.  Otherwise, the returned list may contain only
     *  those works that are accessible through this session.
     *
     *  @return a list of uncommitted <code> Works </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getUncommittedWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchAnyCommission(false);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets all <code>Works</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Works</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.resourcing.WorkQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getWorksByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resourcing.WorkQuery getQuery() {
        org.osid.resourcing.WorkQuery query = this.session.getWorkQuery();
        
        return (query);
    }
}
