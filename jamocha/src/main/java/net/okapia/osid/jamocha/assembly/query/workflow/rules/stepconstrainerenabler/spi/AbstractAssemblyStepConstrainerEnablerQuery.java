//
// AbstractAssemblyStepConstrainerEnablerQuery.java
//
//     A StepConstrainerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.workflow.rules.stepconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StepConstrainerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyStepConstrainerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.workflow.rules.StepConstrainerEnablerQuery,
               org.osid.workflow.rules.StepConstrainerEnablerQueryInspector,
               org.osid.workflow.rules.StepConstrainerEnablerSearchOrder {

    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyStepConstrainerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyStepConstrainerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the step constrainer. 
     *
     *  @param  stepConstrainerId the step constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledStepConstrainerId(org.osid.id.Id stepConstrainerId, 
                                            boolean match) {
        getAssembler().addIdTerm(getRuledStepConstrainerIdColumn(), stepConstrainerId, match);
        return;
    }


    /**
     *  Clears the step constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledStepConstrainerIdTerms() {
        getAssembler().clearTerms(getRuledStepConstrainerIdColumn());
        return;
    }


    /**
     *  Gets the step constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledStepConstrainerIdTerms() {
        return (getAssembler().getIdTerms(getRuledStepConstrainerIdColumn()));
    }


    /**
     *  Gets the RuledStepConstrainerId column name.
     *
     * @return the column name
     */

    protected String getRuledStepConstrainerIdColumn() {
        return ("ruled_step_constrainer_id");
    }


    /**
     *  Tests if a <code> StepConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a step constrainer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledStepConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the step constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledStepConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQuery getRuledStepConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledStepConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any step constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any step 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no step constrainers 
     */

    @OSID @Override
    public void matchAnyRuledStepConstrainer(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledStepConstrainerColumn(), match);
        return;
    }


    /**
     *  Clears the step constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledStepConstrainerTerms() {
        getAssembler().clearTerms(getRuledStepConstrainerColumn());
        return;
    }


    /**
     *  Gets the step constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQueryInspector[] getRuledStepConstrainerTerms() {
        return (new org.osid.workflow.rules.StepConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the RuledStepConstrainer column name.
     *
     * @return the column name
     */

    protected String getRuledStepConstrainerColumn() {
        return ("ruled_step_constrainer");
    }


    /**
     *  Matches enablers mapped to the office. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        getAssembler().addIdTerm(getOfficeIdColumn(), officeId, match);
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        getAssembler().clearTerms(getOfficeIdColumn());
        return;
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (getAssembler().getIdTerms(getOfficeIdColumn()));
    }


    /**
     *  Gets the OfficeId column name.
     *
     * @return the column name
     */

    protected String getOfficeIdColumn() {
        return ("office_id");
    }


    /**
     *  Tests if an <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if an office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        getAssembler().clearTerms(getOfficeColumn());
        return;
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the Office column name.
     *
     * @return the column name
     */

    protected String getOfficeColumn() {
        return ("office");
    }


    /**
     *  Tests if this stepConstrainerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  stepConstrainerEnablerRecordType a step constrainer enabler record type 
     *  @return <code>true</code> if the stepConstrainerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type stepConstrainerEnablerRecordType) {
        for (org.osid.workflow.rules.records.StepConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stepConstrainerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  stepConstrainerEnablerRecordType the step constrainer enabler record type 
     *  @return the step constrainer enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerEnablerQueryRecord getStepConstrainerEnablerQueryRecord(org.osid.type.Type stepConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stepConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  stepConstrainerEnablerRecordType the step constrainer enabler record type 
     *  @return the step constrainer enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerEnablerQueryInspectorRecord getStepConstrainerEnablerQueryInspectorRecord(org.osid.type.Type stepConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(stepConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param stepConstrainerEnablerRecordType the step constrainer enabler record type
     *  @return the step constrainer enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerEnablerSearchOrderRecord getStepConstrainerEnablerSearchOrderRecord(org.osid.type.Type stepConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(stepConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this step constrainer enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param stepConstrainerEnablerQueryRecord the step constrainer enabler query record
     *  @param stepConstrainerEnablerQueryInspectorRecord the step constrainer enabler query inspector
     *         record
     *  @param stepConstrainerEnablerSearchOrderRecord the step constrainer enabler search order record
     *  @param stepConstrainerEnablerRecordType step constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerQueryRecord</code>,
     *          <code>stepConstrainerEnablerQueryInspectorRecord</code>,
     *          <code>stepConstrainerEnablerSearchOrderRecord</code> or
     *          <code>stepConstrainerEnablerRecordTypestepConstrainerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addStepConstrainerEnablerRecords(org.osid.workflow.rules.records.StepConstrainerEnablerQueryRecord stepConstrainerEnablerQueryRecord, 
                                      org.osid.workflow.rules.records.StepConstrainerEnablerQueryInspectorRecord stepConstrainerEnablerQueryInspectorRecord, 
                                      org.osid.workflow.rules.records.StepConstrainerEnablerSearchOrderRecord stepConstrainerEnablerSearchOrderRecord, 
                                      org.osid.type.Type stepConstrainerEnablerRecordType) {

        addRecordType(stepConstrainerEnablerRecordType);

        nullarg(stepConstrainerEnablerQueryRecord, "step constrainer enabler query record");
        nullarg(stepConstrainerEnablerQueryInspectorRecord, "step constrainer enabler query inspector record");
        nullarg(stepConstrainerEnablerSearchOrderRecord, "step constrainer enabler search odrer record");

        this.queryRecords.add(stepConstrainerEnablerQueryRecord);
        this.queryInspectorRecords.add(stepConstrainerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(stepConstrainerEnablerSearchOrderRecord);
        
        return;
    }
}
