//
// MutableIndexedMapEngineLookupSession
//
//    Implements an Engine lookup service backed by a collection of
//    engines indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.search;


/**
 *  Implements an Engine lookup service backed by a collection of
 *  engines. The engines are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some engines may be compatible
 *  with more types than are indicated through these engine
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of engines can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapEngineLookupSession
    extends net.okapia.osid.jamocha.core.search.spi.AbstractIndexedMapEngineLookupSession
    implements org.osid.search.EngineLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEngineLookupSession} with no
     *  engines.
     */

    public MutableIndexedMapEngineLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEngineLookupSession} with a
     *  single engine.
     *  
     *  @param  engine an single engine
     *  @throws org.osid.NullArgumentException {@code engine}
     *          is {@code null}
     */

    public MutableIndexedMapEngineLookupSession(org.osid.search.Engine engine) {
        putEngine(engine);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEngineLookupSession} using an
     *  array of engines.
     *
     *  @param  engines an array of engines
     *  @throws org.osid.NullArgumentException {@code engines}
     *          is {@code null}
     */

    public MutableIndexedMapEngineLookupSession(org.osid.search.Engine[] engines) {
        putEngines(engines);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEngineLookupSession} using a
     *  collection of engines.
     *
     *  @param  engines a collection of engines
     *  @throws org.osid.NullArgumentException {@code engines} is
     *          {@code null}
     */

    public MutableIndexedMapEngineLookupSession(java.util.Collection<? extends org.osid.search.Engine> engines) {
        putEngines(engines);
        return;
    }
    

    /**
     *  Makes an {@code Engine} available in this session.
     *
     *  @param  engine an engine
     *  @throws org.osid.NullArgumentException {@code engine{@code  is
     *          {@code null}
     */

    @Override
    public void putEngine(org.osid.search.Engine engine) {
        super.putEngine(engine);
        return;
    }


    /**
     *  Makes an array of engines available in this session.
     *
     *  @param  engines an array of engines
     *  @throws org.osid.NullArgumentException {@code engines{@code 
     *          is {@code null}
     */

    @Override
    public void putEngines(org.osid.search.Engine[] engines) {
        super.putEngines(engines);
        return;
    }


    /**
     *  Makes collection of engines available in this session.
     *
     *  @param  engines a collection of engines
     *  @throws org.osid.NullArgumentException {@code engine{@code  is
     *          {@code null}
     */

    @Override
    public void putEngines(java.util.Collection<? extends org.osid.search.Engine> engines) {
        super.putEngines(engines);
        return;
    }


    /**
     *  Removes an Engine from this session.
     *
     *  @param engineId the {@code Id} of the engine
     *  @throws org.osid.NullArgumentException {@code engineId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEngine(org.osid.id.Id engineId) {
        super.removeEngine(engineId);
        return;
    }    
}
