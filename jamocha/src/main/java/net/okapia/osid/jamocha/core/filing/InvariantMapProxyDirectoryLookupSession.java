//
// InvariantMapProxyDirectoryLookupSession
//
//    Implements a Directory lookup service backed by a fixed
//    collection of directories. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing;


/**
 *  Implements a Directory lookup service backed by a fixed
 *  collection of directories. The directories are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyDirectoryLookupSession
    extends net.okapia.osid.jamocha.core.filing.spi.AbstractMapDirectoryLookupSession
    implements org.osid.filing.DirectoryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDirectoryLookupSession} with no
     *  directories.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyDirectoryLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDirectoryLookupSession} with a
     *  single directory.
     *
     *  @param directory a single directory
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directory} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDirectoryLookupSession(org.osid.filing.Directory directory, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDirectory(directory);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDirectoryLookupSession} using
     *  an array of directories.
     *
     *  @param directories an array of directories
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directories} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDirectoryLookupSession(org.osid.filing.Directory[] directories, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDirectories(directories);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDirectoryLookupSession} using a
     *  collection of directories.
     *
     *  @param directories a collection of directories
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directories} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDirectoryLookupSession(java.util.Collection<? extends org.osid.filing.Directory> directories,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDirectories(directories);
        return;
    }
}
