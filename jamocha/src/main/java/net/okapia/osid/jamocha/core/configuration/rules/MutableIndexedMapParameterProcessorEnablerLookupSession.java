//
// MutableIndexedMapParameterProcessorEnablerLookupSession
//
//    Implements a ParameterProcessorEnabler lookup service backed by a collection of
//    parameterProcessorEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules;


/**
 *  Implements a ParameterProcessorEnabler lookup service backed by a collection of
 *  parameter processor enablers. The parameter processor enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some parameter processor enablers may be compatible
 *  with more types than are indicated through these parameter processor enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of parameter processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapParameterProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.configuration.rules.spi.AbstractIndexedMapParameterProcessorEnablerLookupSession
    implements org.osid.configuration.rules.ParameterProcessorEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapParameterProcessorEnablerLookupSession} with no parameter processor enablers.
     *
     *  @param configuration the configuration
     *  @throws org.osid.NullArgumentException {@code configuration}
     *          is {@code null}
     */

      public MutableIndexedMapParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration) {
        setConfiguration(configuration);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapParameterProcessorEnablerLookupSession} with a
     *  single parameter processor enabler.
     *  
     *  @param configuration the configuration
     *  @param  parameterProcessorEnabler a single parameterProcessorEnabler
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameterProcessorEnabler} is {@code null}
     */

    public MutableIndexedMapParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler) {
        this(configuration);
        putParameterProcessorEnabler(parameterProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapParameterProcessorEnablerLookupSession} using an
     *  array of parameter processor enablers.
     *
     *  @param configuration the configuration
     *  @param  parameterProcessorEnablers an array of parameter processor enablers
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameterProcessorEnablers} is {@code null}
     */

    public MutableIndexedMapParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.rules.ParameterProcessorEnabler[] parameterProcessorEnablers) {
        this(configuration);
        putParameterProcessorEnablers(parameterProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapParameterProcessorEnablerLookupSession} using a
     *  collection of parameter processor enablers.
     *
     *  @param configuration the configuration
     *  @param  parameterProcessorEnablers a collection of parameter processor enablers
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameterProcessorEnablers} is {@code null}
     */

    public MutableIndexedMapParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                  java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessorEnabler> parameterProcessorEnablers) {

        this(configuration);
        putParameterProcessorEnablers(parameterProcessorEnablers);
        return;
    }
    

    /**
     *  Makes a {@code ParameterProcessorEnabler} available in this session.
     *
     *  @param  parameterProcessorEnabler a parameter processor enabler
     *  @throws org.osid.NullArgumentException {@code parameterProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putParameterProcessorEnabler(org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler) {
        super.putParameterProcessorEnabler(parameterProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of parameter processor enablers available in this session.
     *
     *  @param  parameterProcessorEnablers an array of parameter processor enablers
     *  @throws org.osid.NullArgumentException {@code parameterProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putParameterProcessorEnablers(org.osid.configuration.rules.ParameterProcessorEnabler[] parameterProcessorEnablers) {
        super.putParameterProcessorEnablers(parameterProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of parameter processor enablers available in this session.
     *
     *  @param  parameterProcessorEnablers a collection of parameter processor enablers
     *  @throws org.osid.NullArgumentException {@code parameterProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putParameterProcessorEnablers(java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessorEnabler> parameterProcessorEnablers) {
        super.putParameterProcessorEnablers(parameterProcessorEnablers);
        return;
    }


    /**
     *  Removes a ParameterProcessorEnabler from this session.
     *
     *  @param parameterProcessorEnablerId the {@code Id} of the parameter processor enabler
     *  @throws org.osid.NullArgumentException {@code parameterProcessorEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeParameterProcessorEnabler(org.osid.id.Id parameterProcessorEnablerId) {
        super.removeParameterProcessorEnabler(parameterProcessorEnablerId);
        return;
    }    
}
