//
// AbstractIndexedMapBrokerLookupSession.java
//
//    A simple framework for providing a Broker lookup service
//    backed by a fixed collection of brokers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Broker lookup service backed by a
 *  fixed collection of brokers. The brokers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some brokers may be compatible
 *  with more types than are indicated through these broker
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Brokers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBrokerLookupSession
    extends AbstractMapBrokerLookupSession
    implements org.osid.provisioning.BrokerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Broker> brokersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Broker>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Broker> brokersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Broker>());


    /**
     *  Makes a <code>Broker</code> available in this session.
     *
     *  @param  broker a broker
     *  @throws org.osid.NullArgumentException <code>broker<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBroker(org.osid.provisioning.Broker broker) {
        super.putBroker(broker);

        this.brokersByGenus.put(broker.getGenusType(), broker);
        
        try (org.osid.type.TypeList types = broker.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokersByRecord.put(types.getNextType(), broker);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a broker from this session.
     *
     *  @param brokerId the <code>Id</code> of the broker
     *  @throws org.osid.NullArgumentException <code>brokerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBroker(org.osid.id.Id brokerId) {
        org.osid.provisioning.Broker broker;
        try {
            broker = getBroker(brokerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.brokersByGenus.remove(broker.getGenusType());

        try (org.osid.type.TypeList types = broker.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokersByRecord.remove(types.getNextType(), broker);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBroker(brokerId);
        return;
    }


    /**
     *  Gets a <code>BrokerList</code> corresponding to the given
     *  broker genus <code>Type</code> which does not include
     *  brokers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known brokers or an error results. Otherwise,
     *  the returned list may contain only those brokers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  brokerGenusType a broker genus type 
     *  @return the returned <code>Broker</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByGenusType(org.osid.type.Type brokerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.broker.ArrayBrokerList(this.brokersByGenus.get(brokerGenusType)));
    }


    /**
     *  Gets a <code>BrokerList</code> containing the given
     *  broker record <code>Type</code>. In plenary mode, the
     *  returned list contains all known brokers or an error
     *  results. Otherwise, the returned list may contain only those
     *  brokers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  brokerRecordType a broker record type 
     *  @return the returned <code>broker</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByRecordType(org.osid.type.Type brokerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.broker.ArrayBrokerList(this.brokersByRecord.get(brokerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.brokersByGenus.clear();
        this.brokersByRecord.clear();

        super.close();

        return;
    }
}
