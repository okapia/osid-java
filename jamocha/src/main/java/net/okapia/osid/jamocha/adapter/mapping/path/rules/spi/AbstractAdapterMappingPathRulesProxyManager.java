//
// AbstractMappingPathRulesProxyManager.java
//
//     An adapter for a MappingPathRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MappingPathRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMappingPathRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.mapping.path.rules.MappingPathRulesProxyManager>
    implements org.osid.mapping.path.rules.MappingPathRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterMappingPathRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMappingPathRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMappingPathRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMappingPathRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up speed zone enablers is supported. 
     *
     *  @return <code> true </code> if speed zone enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerLookup() {
        return (getAdapteeManager().supportsSpeedZoneEnablerLookup());
    }


    /**
     *  Tests if querying speed zone enablers is supported. 
     *
     *  @return <code> true </code> if speed zone enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerQuery() {
        return (getAdapteeManager().supportsSpeedZoneEnablerQuery());
    }


    /**
     *  Tests if searching speed zone enablers is supported. 
     *
     *  @return <code> true </code> if speed zone enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerSearch() {
        return (getAdapteeManager().supportsSpeedZoneEnablerSearch());
    }


    /**
     *  Tests if a speed zone enabler administrative service is supported. 
     *
     *  @return <code> true </code> if speed zone enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerAdmin() {
        return (getAdapteeManager().supportsSpeedZoneEnablerAdmin());
    }


    /**
     *  Tests if a speed zone enabler notification service is supported. 
     *
     *  @return <code> true </code> if speed zone enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerNotification() {
        return (getAdapteeManager().supportsSpeedZoneEnablerNotification());
    }


    /**
     *  Tests if a speed zone enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if a speed zone enabler map lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerMap() {
        return (getAdapteeManager().supportsSpeedZoneEnablerMap());
    }


    /**
     *  Tests if a speed zone enabler map service is supported. 
     *
     *  @return <code> true </code> if speed zone enabler map assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerMapAssignment() {
        return (getAdapteeManager().supportsSpeedZoneEnablerMapAssignment());
    }


    /**
     *  Tests if a speed zone enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if a speed zone enabler map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerSmartMap() {
        return (getAdapteeManager().supportsSpeedZoneEnablerSmartMap());
    }


    /**
     *  Tests if a speed zone enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a speed zone enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerRuleLookup() {
        return (getAdapteeManager().supportsSpeedZoneEnablerRuleLookup());
    }


    /**
     *  Tests if a speed zone enabler rule application service is supported. 
     *
     *  @return <code> true </code> if speed zone enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerRuleApplication() {
        return (getAdapteeManager().supportsSpeedZoneEnablerRuleApplication());
    }


    /**
     *  Tests if looking up signal enabler is supported. 
     *
     *  @return <code> true </code> if signal enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerLookup() {
        return (getAdapteeManager().supportsSignalEnablerLookup());
    }


    /**
     *  Tests if querying signal enabler is supported. 
     *
     *  @return <code> true </code> if signal enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerQuery() {
        return (getAdapteeManager().supportsSignalEnablerQuery());
    }


    /**
     *  Tests if searching signal enabler is supported. 
     *
     *  @return <code> true </code> if signal enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerSearch() {
        return (getAdapteeManager().supportsSignalEnablerSearch());
    }


    /**
     *  Tests if a signal enabler administrative service is supported. 
     *
     *  @return <code> true </code> if signal enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerAdmin() {
        return (getAdapteeManager().supportsSignalEnablerAdmin());
    }


    /**
     *  Tests if a signal enabler notification service is supported. 
     *
     *  @return <code> true </code> if signal enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerNotification() {
        return (getAdapteeManager().supportsSignalEnablerNotification());
    }


    /**
     *  Tests if a signal enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if a signal enabler map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerMap() {
        return (getAdapteeManager().supportsSignalEnablerMap());
    }


    /**
     *  Tests if a signal enabler map service is supported. 
     *
     *  @return <code> true </code> if signal enabler map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerMapAssignment() {
        return (getAdapteeManager().supportsSignalEnablerMapAssignment());
    }


    /**
     *  Tests if a signal enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if a signal enabler map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerSmartMap() {
        return (getAdapteeManager().supportsSignalEnablerSmartMap());
    }


    /**
     *  Tests if a signal enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a signal enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerRuleLookup() {
        return (getAdapteeManager().supportsSignalEnablerRuleLookup());
    }


    /**
     *  Tests if a signal enabler rule application service is supported. 
     *
     *  @return <code> true </code> if a signal enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerRuleApplication() {
        return (getAdapteeManager().supportsSignalEnablerRuleApplication());
    }


    /**
     *  Tests if looking up obstacle enabler is supported. 
     *
     *  @return <code> true </code> if obstacle enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerLookup() {
        return (getAdapteeManager().supportsObstacleEnablerLookup());
    }


    /**
     *  Tests if querying obstacle enabler is supported. 
     *
     *  @return <code> true </code> if obstacle enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerQuery() {
        return (getAdapteeManager().supportsObstacleEnablerQuery());
    }


    /**
     *  Tests if searching obstacle enabler is supported. 
     *
     *  @return <code> true </code> if obstacle enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerSearch() {
        return (getAdapteeManager().supportsObstacleEnablerSearch());
    }


    /**
     *  Tests if an obstacle enabler administrative service is supported. 
     *
     *  @return <code> true </code> if obstacle enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerAdmin() {
        return (getAdapteeManager().supportsObstacleEnablerAdmin());
    }


    /**
     *  Tests if an obstacle enabler notification service is supported. 
     *
     *  @return <code> true </code> if obstacle enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerNotification() {
        return (getAdapteeManager().supportsObstacleEnablerNotification());
    }


    /**
     *  Tests if an obstacle enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if an obstacle enabler map lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerMap() {
        return (getAdapteeManager().supportsObstacleEnablerMap());
    }


    /**
     *  Tests if an obstacle enabler map service is supported. 
     *
     *  @return <code> true </code> if obstacle enabler map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerMapAssignment() {
        return (getAdapteeManager().supportsObstacleEnablerMapAssignment());
    }


    /**
     *  Tests if an obstacle enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if an obstacle enabler map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerSmartMap() {
        return (getAdapteeManager().supportsObstacleEnablerSmartMap());
    }


    /**
     *  Tests if an obstacle enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an obstacle enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerRuleLookup() {
        return (getAdapteeManager().supportsObstacleEnablerRuleLookup());
    }


    /**
     *  Tests if an obstacle enabler rule application service is supported. 
     *
     *  @return <code> true </code> if an obstacle enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerRuleApplication() {
        return (getAdapteeManager().supportsObstacleEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> SpeedZoneEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> SpeedZoneEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpeedZoneEnablerRecordTypes() {
        return (getAdapteeManager().getSpeedZoneEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> SpeedZoneEnabler </code> record type is 
     *  supported. 
     *
     *  @param  speedZoneEnablerRecordType a <code> Type </code> indicating a 
     *          <code> SpeedZoneEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          speedZoneEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerRecordType(org.osid.type.Type speedZoneEnablerRecordType) {
        return (getAdapteeManager().supportsSpeedZoneEnablerRecordType(speedZoneEnablerRecordType));
    }


    /**
     *  Gets the supported <code> SpeedZoneEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> SpeedZoneEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpeedZoneEnablerSearchRecordTypes() {
        return (getAdapteeManager().getSpeedZoneEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> SpeedZoneEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  speedZoneEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> SpeedZoneEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          speedZoneEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerSearchRecordType(org.osid.type.Type speedZoneEnablerSearchRecordType) {
        return (getAdapteeManager().supportsSpeedZoneEnablerSearchRecordType(speedZoneEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> SignalEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> SignalEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSignalEnablerRecordTypes() {
        return (getAdapteeManager().getSignalEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> SignalEnabler </code> record type is 
     *  supported. 
     *
     *  @param  signalEnablerRecordType a <code> Type </code> indicating a 
     *          <code> SignalEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> signalEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSignalEnablerRecordType(org.osid.type.Type signalEnablerRecordType) {
        return (getAdapteeManager().supportsSignalEnablerRecordType(signalEnablerRecordType));
    }


    /**
     *  Gets the supported <code> SignalEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> SignalEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSignalEnablerSearchRecordTypes() {
        return (getAdapteeManager().getSignalEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> SignalEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  signalEnablerSearchRecordType a <code> Type </code> indicating 
     *          a <code> SignalEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          signalEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSignalEnablerSearchRecordType(org.osid.type.Type signalEnablerSearchRecordType) {
        return (getAdapteeManager().supportsSignalEnablerSearchRecordType(signalEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> ObstacleEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ObstacleEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObstacleEnablerRecordTypes() {
        return (getAdapteeManager().getObstacleEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> ObstacleEnabler </code> record type is 
     *  supported. 
     *
     *  @param  obstacleEnablerRecordType a <code> Type </code> indicating an 
     *          <code> ObstacleEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          obstacleEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerRecordType(org.osid.type.Type obstacleEnablerRecordType) {
        return (getAdapteeManager().supportsObstacleEnablerRecordType(obstacleEnablerRecordType));
    }


    /**
     *  Gets the supported <code> ObstacleEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ObstacleEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObstacleEnablerSearchRecordTypes() {
        return (getAdapteeManager().getObstacleEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ObstacleEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  obstacleEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> ObstacleEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          obstacleEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerSearchRecordType(org.osid.type.Type obstacleEnablerSearchRecordType) {
        return (getAdapteeManager().supportsObstacleEnablerSearchRecordType(obstacleEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession getSpeedZoneEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession getSpeedZoneEnablerLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerQuerySession getSpeedZoneEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerQuerySession getSpeedZoneEnablerQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerQuerySessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerSearchSession getSpeedZoneEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enablers earch service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerSearchSession getSpeedZoneEnablerSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerSearchSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerAdminSession getSpeedZoneEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerAdminSession getSpeedZoneEnablerAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerAdminSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler notification service. 
     *
     *  @param  speedZoneEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerNotificationSession getSpeedZoneEnablerNotificationSession(org.osid.mapping.path.rules.SpeedZoneEnablerReceiver speedZoneEnablerReceiver, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerNotificationSession(speedZoneEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler notification service for the given map. 
     *
     *  @param  speedZoneEnablerReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          speedZoneEnablerReceiver, mapId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerNotificationSession getSpeedZoneEnablerNotificationSessionForMap(org.osid.mapping.path.rules.SpeedZoneEnablerReceiver speedZoneEnablerReceiver, 
                                                                                                                        org.osid.id.Id mapId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerNotificationSessionForMap(speedZoneEnablerReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup speed zone enabler/map 
     *  mappings for speed zone enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerMapSession getSpeedZoneEnablerMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerMapSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning speed 
     *  zone enablers to map. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerMapAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerMapAssignmentSession getSpeedZoneEnablerMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerMapAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage speed zone enabler smart 
     *  map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerSmartMap() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerSmartMapSession getSpeedZoneEnablerSmartMapSession(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerSmartMapSession(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleLookupSession getSpeedZoneEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler mapping lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleLookupSession getSpeedZoneEnablerRuleLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerRuleLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleApplicationSession getSpeedZoneEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler assignment service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleApplicationSession getSpeedZoneEnablerRuleApplicationSessionForMap(org.osid.id.Id mapId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneEnablerRuleApplicationSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerLookupSession getSignalEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerLookupSession getSignalEnablerLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerQuerySession getSignalEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerQuerySession getSignalEnablerQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerQuerySessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException a <code> 
     *          SignalEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerSearchSession getSignalEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  earch service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerSearchSession getSignalEnablerSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerSearchSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerAdminSession getSignalEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerAdminSession getSignalEnablerAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerAdminSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  notification service. 
     *
     *  @param  signalEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> signalEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerNotificationSession getSignalEnablerNotificationSession(org.osid.mapping.path.rules.SignalEnablerReceiver signalEnablerReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerNotificationSession(signalEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  notification service for the given map. 
     *
     *  @param  signalEnablerReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> signalEnablerReceiver, 
     *          mapId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerNotificationSession getSignalEnablerNotificationSessionForMap(org.osid.mapping.path.rules.SignalEnablerReceiver signalEnablerReceiver, 
                                                                                                                  org.osid.id.Id mapId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerNotificationSessionForMap(signalEnablerReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup signal enabler/map 
     *  mappings for signal enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerMapSession getSignalEnablerMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerMapSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning signal 
     *  enabler to map. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerMapAssignmentSession getSignalEnablerMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerMapAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage signal enabler smart 
     *  map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerSmartMap() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerSmartMapSession getSignalEnablerSmartMapSession(org.osid.id.Id mapId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerSmartMapSession(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  mapping lookup service for looking up the rules applied to a signal. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleLookupSession getSignalEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  mapping lookup service for the given map for looking up rules applied 
     *  to a signal. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleLookupSession getSignalEnablerRuleLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerRuleLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  assignment service to apply to signals. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleApplicationSession getSignalEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  assignment service for the given map to apply to signals. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleApplicationSession getSignalEnablerRuleApplicationSessionForMap(org.osid.id.Id mapId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalEnablerRuleApplicationSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerLookupSession getObstacleEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerLookupSession getObstacleEnablerLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerQuerySession getObstacleEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerQuerySession getObstacleEnablerQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerQuerySessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerSearchSession getObstacleEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler earch service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerSearchSession getObstacleEnablerSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerSearchSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerAdminSession getObstacleEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerAdminSession getObstacleEnablerAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerAdminSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler notification service. 
     *
     *  @param  obstacleEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerNotificationSession getObstacleEnablerNotificationSession(org.osid.mapping.path.rules.ObstacleEnablerReceiver obstacleEnablerReceiver, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerNotificationSession(obstacleEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler notification service for the given map. 
     *
     *  @param  obstacleEnablerReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleEnablerReceiver, 
     *          mapId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerNotificationSession getObstacleEnablerNotificationSessionForMap(org.osid.mapping.path.rules.ObstacleEnablerReceiver obstacleEnablerReceiver, 
                                                                                                                      org.osid.id.Id mapId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerNotificationSessionForMap(obstacleEnablerReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup obstacle enabler/map 
     *  mappings for obstacle enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerMapSession getObstacleEnablerMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerMapSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning obstacle 
     *  enabler to map. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerMapAssignmentSession getObstacleEnablerMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerMapAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage obstacle enabler smart 
     *  map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerSmartMap() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerSmartMapSession getObstacleEnablerSmartMapSession(org.osid.id.Id mapId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerSmartMapSession(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler mapping lookup service for looking up the rules applied to an 
     *  obstacle. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleLookupSession getObstacleEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler mapping lookup service for the given map for looking up rules 
     *  applied to an obstacle. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleLookupSession getObstacleEnablerRuleLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerRuleLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler assignment service to apply to obstacles. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleApplicationSession getObstacleEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler assignment service for the given map to apply to obstacles. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleApplicationSession getObstacleEnablerRuleApplicationSessionForMap(org.osid.id.Id mapId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleEnablerRuleApplicationSessionForMap(mapId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
