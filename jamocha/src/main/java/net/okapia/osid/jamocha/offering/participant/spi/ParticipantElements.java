//
// ParticipantElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.participant.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ParticipantElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ParticipantElement Id.
     *
     *  @return the participant element Id
     */

    public static org.osid.id.Id getParticipantEntityId() {
        return (makeEntityId("osid.offering.Participant"));
    }


    /**
     *  Gets the OfferingId element Id.
     *
     *  @return the OfferingId element Id
     */

    public static org.osid.id.Id getOfferingId() {
        return (makeElementId("osid.offering.participant.OfferingId"));
    }


    /**
     *  Gets the Offering element Id.
     *
     *  @return the Offering element Id
     */

    public static org.osid.id.Id getOffering() {
        return (makeElementId("osid.offering.participant.Offering"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.offering.participant.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.offering.participant.Resource"));
    }


    /**
     *  Gets the TimePeriodId element Id.
     *
     *  @return the TimePeriodId element Id
     */

    public static org.osid.id.Id getTimePeriodId() {
        return (makeElementId("osid.offering.participant.TimePeriodId"));
    }


    /**
     *  Gets the TimePeriod element Id.
     *
     *  @return the TimePeriod element Id
     */

    public static org.osid.id.Id getTimePeriod() {
        return (makeElementId("osid.offering.participant.TimePeriod"));
    }


    /**
     *  Gets the ResultOptionIds element Id.
     *
     *  @return the ResultOptionIds element Id
     */

    public static org.osid.id.Id getResultOptionIds() {
        return (makeElementId("osid.offering.participant.ResultOptionIds"));
    }


    /**
     *  Gets the ResultOptions element Id.
     *
     *  @return the ResultOptions element Id
     */

    public static org.osid.id.Id getResultOptions() {
        return (makeElementId("osid.offering.participant.ResultOptions"));
    }


    /**
     *  Gets the CatalogueId element Id.
     *
     *  @return the CatalogueId element Id
     */

    public static org.osid.id.Id getCatalogueId() {
        return (makeQueryElementId("osid.offering.participant.CatalogueId"));
    }


    /**
     *  Gets the Catalogue element Id.
     *
     *  @return the Catalogue element Id
     */

    public static org.osid.id.Id getCatalogue() {
        return (makeQueryElementId("osid.offering.participant.Catalogue"));
    }
}
