//
// AbstractContactRulesManager.java
//
//     An adapter for a ContactRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.contact.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ContactRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterContactRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.contact.rules.ContactRulesManager>
    implements org.osid.contact.rules.ContactRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterContactRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterContactRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterContactRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterContactRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up contact enablers is supported. 
     *
     *  @return <code> true </code> if contact enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerLookup() {
        return (getAdapteeManager().supportsContactEnablerLookup());
    }


    /**
     *  Tests if querying contact enablers is supported. 
     *
     *  @return <code> true </code> if contact enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerQuery() {
        return (getAdapteeManager().supportsContactEnablerQuery());
    }


    /**
     *  Tests if searching contact enablers is supported. 
     *
     *  @return <code> true </code> if contact enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerSearch() {
        return (getAdapteeManager().supportsContactEnablerSearch());
    }


    /**
     *  Tests if a contact enabler administrative service is supported. 
     *
     *  @return <code> true </code> if contact enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerAdmin() {
        return (getAdapteeManager().supportsContactEnablerAdmin());
    }


    /**
     *  Tests if a contact enabler notification service is supported. 
     *
     *  @return <code> true </code> if contact enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerNotification() {
        return (getAdapteeManager().supportsContactEnablerNotification());
    }


    /**
     *  Tests if a contact enabler address book lookup service is supported. 
     *
     *  @return <code> true </code> if a contact enabler address book lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerAddressBook() {
        return (getAdapteeManager().supportsContactEnablerAddressBook());
    }


    /**
     *  Tests if a contact enabler address book service is supported. 
     *
     *  @return <code> true </code> if contact enabler address book assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerAddressBookAssignment() {
        return (getAdapteeManager().supportsContactEnablerAddressBookAssignment());
    }


    /**
     *  Tests if a contact enabler address book lookup service is supported. 
     *
     *  @return <code> true </code> if a contact enabler address book service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerSmartAddressBook() {
        return (getAdapteeManager().supportsContactEnablerSmartAddressBook());
    }


    /**
     *  Tests if a contact enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a contact enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerRuleLookup() {
        return (getAdapteeManager().supportsContactEnablerRuleLookup());
    }


    /**
     *  Tests if a contact enabler rule application service is supported. 
     *
     *  @return <code> true </code> if contact enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerRuleApplication() {
        return (getAdapteeManager().supportsContactEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> ContactEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ContactEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getContactEnablerRecordTypes() {
        return (getAdapteeManager().getContactEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> ContactEnabler </code> record type is 
     *  supported. 
     *
     *  @param  contactEnablerRecordType a <code> Type </code> indicating a 
     *          <code> ContactEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> contactEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsContactEnablerRecordType(org.osid.type.Type contactEnablerRecordType) {
        return (getAdapteeManager().supportsContactEnablerRecordType(contactEnablerRecordType));
    }


    /**
     *  Gets the supported <code> ContactEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ContactEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getContactEnablerSearchRecordTypes() {
        return (getAdapteeManager().getContactEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ContactEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  contactEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> ContactEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          contactEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsContactEnablerSearchRecordType(org.osid.type.Type contactEnablerSearchRecordType) {
        return (getAdapteeManager().supportsContactEnablerSearchRecordType(contactEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler lookup service. 
     *
     *  @return a <code> ContactEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerLookupSession getContactEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler lookup service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerLookupSession getContactEnablerLookupSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerLookupSessionForAddressBook(addressBookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler query service. 
     *
     *  @return a <code> ContactEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerQuerySession getContactEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler query service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerQuerySession getContactEnablerQuerySessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerQuerySessionForAddressBook(addressBookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler search service. 
     *
     *  @return a <code> ContactEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerSearchSession getContactEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enablers earch service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerSearchSession getContactEnablerSearchSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerSearchSessionForAddressBook(addressBookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler administration service. 
     *
     *  @return a <code> ContactEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAdminSession getContactEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler administration service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAdminSession getContactEnablerAdminSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerAdminSessionForAddressBook(addressBookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler notification service. 
     *
     *  @param  contactEnablerReceiver the notification callback 
     *  @return a <code> ContactEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> contactEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerNotificationSession getContactEnablerNotificationSession(org.osid.contact.rules.ContactEnablerReceiver contactEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerNotificationSession(contactEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler notification service for the given address book. 
     *
     *  @param  contactEnablerReceiver the notification callback 
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no address book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> contactEnablerReceiver 
     *          </code> or <code> addressBookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerNotificationSession getContactEnablerNotificationSessionForAddressBook(org.osid.contact.rules.ContactEnablerReceiver contactEnablerReceiver, 
                                                                                                                       org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerNotificationSessionForAddressBook(contactEnablerReceiver, addressBookId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup contact enabler/address 
     *  book mappings for contact enablers. 
     *
     *  @return a <code> ContactEnablerAddressBookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAddressBook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAddressBookSession getContactEnablerAddressBookSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerAddressBookSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning contact 
     *  enablers to address books for contact. 
     *
     *  @return a <code> ContactEnablerAddressBookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAddressBookAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAddressBookAssignmentSession getContactEnablerAddressBookAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerAddressBookAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage contact enabler smart 
     *  address books. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerSmartAddressBookSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerSmartAddressBook() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerSmartAddressBookSession getContactEnablerSmartAddressBookSession(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerSmartAddressBookSession(addressBookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> ContactEnablertRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleLookupSession getContactEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler mapping lookup service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleLookupSession getContactEnablerRuleLookupSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerRuleLookupSessionForAddressBook(addressBookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler assignment service to apply enablers. 
     *
     *  @return a <code> ContactEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleApplicationSession getContactEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler assignment service for the given address book to apply 
     *  enablers. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleApplicationSession getContactEnablerRuleApplicationSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactEnablerRuleApplicationSessionForAddressBook(addressBookId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
