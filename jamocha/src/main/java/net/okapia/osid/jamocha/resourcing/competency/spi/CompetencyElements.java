//
// CompetencyElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.competency.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CompetencyElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the CompetencyElement Id.
     *
     *  @return the competency element Id
     */

    public static org.osid.id.Id getCompetencyEntityId() {
        return (makeEntityId("osid.resourcing.Competency"));
    }


    /**
     *  Gets the LearningObjectiveIds element Id.
     *
     *  @return the LearningObjectiveIds element Id
     */

    public static org.osid.id.Id getLearningObjectiveIds() {
        return (makeElementId("osid.resourcing.competency.LearningObjectiveIds"));
    }


    /**
     *  Gets the LearningObjectives element Id.
     *
     *  @return the LearningObjectives element Id
     */

    public static org.osid.id.Id getLearningObjectives() {
        return (makeElementId("osid.resourcing.competency.LearningObjectives"));
    }


    /**
     *  Gets the AvailabilityId element Id.
     *
     *  @return the AvailabilityId element Id
     */

    public static org.osid.id.Id getAvailabilityId() {
        return (makeQueryElementId("osid.resourcing.competency.AvailabilityId"));
    }


    /**
     *  Gets the Availability element Id.
     *
     *  @return the Availability element Id
     */

    public static org.osid.id.Id getAvailability() {
        return (makeQueryElementId("osid.resourcing.competency.Availability"));
    }


    /**
     *  Gets the WorkId element Id.
     *
     *  @return the WorkId element Id
     */

    public static org.osid.id.Id getWorkId() {
        return (makeQueryElementId("osid.resourcing.competency.WorkId"));
    }


    /**
     *  Gets the Work element Id.
     *
     *  @return the Work element Id
     */

    public static org.osid.id.Id getWork() {
        return (makeQueryElementId("osid.resourcing.competency.Work"));
    }


    /**
     *  Gets the JobId element Id.
     *
     *  @return the JobId element Id
     */

    public static org.osid.id.Id getJobId() {
        return (makeQueryElementId("osid.resourcing.competency.JobId"));
    }


    /**
     *  Gets the Job element Id.
     *
     *  @return the Job element Id
     */

    public static org.osid.id.Id getJob() {
        return (makeQueryElementId("osid.resourcing.competency.Job"));
    }


    /**
     *  Gets the FoundryId element Id.
     *
     *  @return the FoundryId element Id
     */

    public static org.osid.id.Id getFoundryId() {
        return (makeQueryElementId("osid.resourcing.competency.FoundryId"));
    }


    /**
     *  Gets the Foundry element Id.
     *
     *  @return the Foundry element Id
     */

    public static org.osid.id.Id getFoundry() {
        return (makeQueryElementId("osid.resourcing.competency.Foundry"));
    }
}
