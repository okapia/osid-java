//
// AbstractAddressSearchOdrer.java
//
//     Defines an AddressSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.address.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AddressSearchOrder}.
 */

public abstract class AbstractAddressSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.contact.AddressSearchOrder {

    private final java.util.Collection<org.osid.contact.records.AddressSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddresseeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  addressRecordType an address record type 
     *  @return {@code true} if the addressRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code addressRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type addressRecordType) {
        for (org.osid.contact.records.AddressSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(addressRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  addressRecordType the address record type 
     *  @return the address search order record
     *  @throws org.osid.NullArgumentException
     *          {@code addressRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(addressRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.contact.records.AddressSearchOrderRecord getAddressSearchOrderRecord(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(addressRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this address. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param addressRecord the address search odrer record
     *  @param addressRecordType address record type
     *  @throws org.osid.NullArgumentException
     *          {@code addressRecord} or
     *          {@code addressRecordTypeaddress} is
     *          {@code null}
     */
            
    protected void addAddressRecord(org.osid.contact.records.AddressSearchOrderRecord addressSearchOrderRecord, 
                                     org.osid.type.Type addressRecordType) {

        addRecordType(addressRecordType);
        this.records.add(addressSearchOrderRecord);
        
        return;
    }
}
