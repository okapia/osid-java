//
// AbstractIndexedMapInquestLookupSession.java
//
//    A simple framework for providing an Inquest lookup service
//    backed by a fixed collection of inquests with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Inquest lookup service backed by a
 *  fixed collection of inquests. The inquests are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some inquests may be compatible
 *  with more types than are indicated through these inquest
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Inquests</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapInquestLookupSession
    extends AbstractMapInquestLookupSession
    implements org.osid.inquiry.InquestLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inquiry.Inquest> inquestsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.Inquest>());
    private final MultiMap<org.osid.type.Type, org.osid.inquiry.Inquest> inquestsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.Inquest>());


    /**
     *  Makes an <code>Inquest</code> available in this session.
     *
     *  @param  inquest an inquest
     *  @throws org.osid.NullArgumentException <code>inquest<code> is
     *          <code>null</code>
     */

    @Override
    protected void putInquest(org.osid.inquiry.Inquest inquest) {
        super.putInquest(inquest);

        this.inquestsByGenus.put(inquest.getGenusType(), inquest);
        
        try (org.osid.type.TypeList types = inquest.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inquestsByRecord.put(types.getNextType(), inquest);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of inquests available in this session.
     *
     *  @param  inquests an array of inquests
     *  @throws org.osid.NullArgumentException <code>inquests<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInquests(org.osid.inquiry.Inquest[] inquests) {
        for (org.osid.inquiry.Inquest inquest : inquests) {
            putInquest(inquest);
        }

        return;
    }


    /**
     *  Makes a collection of inquests available in this session.
     *
     *  @param  inquests a collection of inquests
     *  @throws org.osid.NullArgumentException <code>inquests<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInquests(java.util.Collection<? extends org.osid.inquiry.Inquest> inquests) {
        for (org.osid.inquiry.Inquest inquest : inquests) {
            putInquest(inquest);
        }

        return;
    }


    /**
     *  Removes an inquest from this session.
     *
     *  @param inquestId the <code>Id</code> of the inquest
     *  @throws org.osid.NullArgumentException <code>inquestId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeInquest(org.osid.id.Id inquestId) {
        org.osid.inquiry.Inquest inquest;
        try {
            inquest = getInquest(inquestId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.inquestsByGenus.remove(inquest.getGenusType());

        try (org.osid.type.TypeList types = inquest.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inquestsByRecord.remove(types.getNextType(), inquest);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeInquest(inquestId);
        return;
    }


    /**
     *  Gets an <code>InquestList</code> corresponding to the given
     *  inquest genus <code>Type</code> which does not include
     *  inquests of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known inquests or an error results. Otherwise,
     *  the returned list may contain only those inquests that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  inquestGenusType an inquest genus type 
     *  @return the returned <code>Inquest</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquestGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByGenusType(org.osid.type.Type inquestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.inquest.ArrayInquestList(this.inquestsByGenus.get(inquestGenusType)));
    }


    /**
     *  Gets an <code>InquestList</code> containing the given
     *  inquest record <code>Type</code>. In plenary mode, the
     *  returned list contains all known inquests or an error
     *  results. Otherwise, the returned list may contain only those
     *  inquests that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  inquestRecordType an inquest record type 
     *  @return the returned <code>inquest</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquestRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByRecordType(org.osid.type.Type inquestRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.inquest.ArrayInquestList(this.inquestsByRecord.get(inquestRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inquestsByGenus.clear();
        this.inquestsByRecord.clear();

        super.close();

        return;
    }
}
