//
// AbstractProvisionSearch.java
//
//     A template for making a Provision Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provision.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing provision searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProvisionSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.ProvisionSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.ProvisionSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.ProvisionSearchOrder provisionSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of provisions. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  provisionIds list of provisions
     *  @throws org.osid.NullArgumentException
     *          <code>provisionIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProvisions(org.osid.id.IdList provisionIds) {
        while (provisionIds.hasNext()) {
            try {
                this.ids.add(provisionIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProvisions</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of provision Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProvisionIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  provisionSearchOrder provision search order 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>provisionSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProvisionResults(org.osid.provisioning.ProvisionSearchOrder provisionSearchOrder) {
	this.provisionSearchOrder = provisionSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.ProvisionSearchOrder getProvisionSearchOrder() {
	return (this.provisionSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given provision search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a provision implementing the requested record.
     *
     *  @param provisionSearchRecordType a provision search record
     *         type
     *  @return the provision search record
     *  @throws org.osid.NullArgumentException
     *          <code>provisionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionSearchRecord getProvisionSearchRecord(org.osid.type.Type provisionSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.records.ProvisionSearchRecord record : this.records) {
            if (record.implementsRecordType(provisionSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this provision search. 
     *
     *  @param provisionSearchRecord provision search record
     *  @param provisionSearchRecordType provision search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProvisionSearchRecord(org.osid.provisioning.records.ProvisionSearchRecord provisionSearchRecord, 
                                           org.osid.type.Type provisionSearchRecordType) {

        addRecordType(provisionSearchRecordType);
        this.records.add(provisionSearchRecord);        
        return;
    }
}
