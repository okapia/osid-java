//
// InvariantMapProxyPriceEnablerLookupSession
//
//    Implements a PriceEnabler lookup service backed by a fixed
//    collection of priceEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.rules;


/**
 *  Implements a PriceEnabler lookup service backed by a fixed
 *  collection of price enablers. The price enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyPriceEnablerLookupSession
    extends net.okapia.osid.jamocha.core.ordering.rules.spi.AbstractMapPriceEnablerLookupSession
    implements org.osid.ordering.rules.PriceEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPriceEnablerLookupSession} with no
     *  price enablers.
     *
     *  @param store the store
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPriceEnablerLookupSession(org.osid.ordering.Store store,
                                                  org.osid.proxy.Proxy proxy) {
        setStore(store);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyPriceEnablerLookupSession} with a single
     *  price enabler.
     *
     *  @param store the store
     *  @param priceEnabler a single price enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code priceEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPriceEnablerLookupSession(org.osid.ordering.Store store,
                                                  org.osid.ordering.rules.PriceEnabler priceEnabler, org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putPriceEnabler(priceEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPriceEnablerLookupSession} using
     *  an array of price enablers.
     *
     *  @param store the store
     *  @param priceEnablers an array of price enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code priceEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPriceEnablerLookupSession(org.osid.ordering.Store store,
                                                  org.osid.ordering.rules.PriceEnabler[] priceEnablers, org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putPriceEnablers(priceEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPriceEnablerLookupSession} using a
     *  collection of price enablers.
     *
     *  @param store the store
     *  @param priceEnablers a collection of price enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code priceEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPriceEnablerLookupSession(org.osid.ordering.Store store,
                                                  java.util.Collection<? extends org.osid.ordering.rules.PriceEnabler> priceEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putPriceEnablers(priceEnablers);
        return;
    }
}
