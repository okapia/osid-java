//
// AbstractCatalogingManager.java
//
//     An adapter for a CatalogingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.cataloging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CatalogingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCatalogingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.cataloging.CatalogingManager>
    implements org.osid.cataloging.CatalogingManager {


    /**
     *  Constructs a new {@code AbstractAdapterCatalogingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCatalogingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCatalogingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCatalogingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a cataloging service retrieving
     *  <code> Id </code> to <code> Catalog </code> mappings.
     *
     *  @return <code> true </code> if cataloging is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalog() {
        return (getAdapteeManager().supportsCatalog());
    }


    /**
     *  Tests for the availability of a cataloging service for mapping <code> 
     *  Ids </code> to <code> Catalogs. </code> 
     *
     *  @return <code> true </code> if catalog assignment is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogAssignment() {
        return (getAdapteeManager().supportsCatalogAssignment());
    }


    /**
     *  Tests for the availability of a cataloging notification service for 
     *  mapping <code> Ids </code> to <code> Catalogs. </code> 
     *
     *  @return <code> true </code> if catalog entry notification is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEntryNotification() {
        return (getAdapteeManager().supportsCatalogEntryNotification());
    }


    /**
     *  Tests for the availability of a catalog lookup service. 
     *
     *  @return <code> true </code> if catalog lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogLookup() {
        return (getAdapteeManager().supportsCatalogLookup());
    }


    /**
     *  Tests for the availability of a catalog query service that defines 
     *  more comprehensive queries. 
     *
     *  @return <code> true </code> if catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogQuery() {
        return (getAdapteeManager().supportsCatalogQuery());
    }


    /**
     *  Tests for the availability of a catalog search service that defines 
     *  more comprehensive queries. 
     *
     *  @return <code> true </code> if catalog search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogSearch() {
        return (getAdapteeManager().supportsCatalogSearch());
    }


    /**
     *  Tests for the availability of a catalog administration service for the 
     *  addition and deletion of catalogs. 
     *
     *  @return <code> true </code> if catalog administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogAdmin() {
        return (getAdapteeManager().supportsCatalogAdmin());
    }


    /**
     *  Tests for the availability of a catalog notification service. 
     *
     *  @return <code> true </code> if catalog notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogNotification() {
        return (getAdapteeManager().supportsCatalogNotification());
    }


    /**
     *  Tests for the availability of a catalog hierarchy traversal service. 
     *
     *  @return <code> true </code> if catalog hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogHierarchy() {
        return (getAdapteeManager().supportsCatalogHierarchy());
    }


    /**
     *  Tests for the availability of a catalog hierarchy design service. 
     *
     *  @return <code> true </code> if catalog hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogHierarchyDesign() {
        return (getAdapteeManager().supportsCatalogHierarchyDesign());
    }


    /**
     *  Tests if the cataloging rules sub services is supported. 
     *
     *  @return <code> true </code> if cataloging rules is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogingRules() {
        return (getAdapteeManager().supportsCatalogingRules());
    }


    /**
     *  Gets the supported <code> Catalog </code> record types. 
     *
     *  @return a list containing the supported <code> Catalog </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogRecordTypes() {
        return (getAdapteeManager().getCatalogRecordTypes());
    }


    /**
     *  Tests if the given <code> Catalog </code> record type is supported. 
     *
     *  @param  catalogRecordType a <code> Type </code> indicating a <code> 
     *          Catalog </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> catalogRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogRecordType(org.osid.type.Type catalogRecordType) {
        return (getAdapteeManager().supportsCatalogRecordType(catalogRecordType));
    }


    /**
     *  Gets the supported catalog search reciord types. 
     *
     *  @return a list containing the supported search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogSearchRecordTypes() {
        return (getAdapteeManager().getCatalogSearchRecordTypes());
    }


    /**
     *  Tests if the given catalog search record type is supported. 
     *
     *  @param  catalogSearchRecordType a <code> Type </code> indicating a 
     *          catalog search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> catalogSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogSearchRecordType(org.osid.type.Type catalogSearchRecordType) {
        return (getAdapteeManager().supportsCatalogSearchRecordType(catalogSearchRecordType));
    }


    /**
     *  Gets the cataloging session for retrieving mappings to catalogs. 
     *
     *  @return a <code> CatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogSession getCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogSession());
    }


    /**
     *  Gets the cataloging session for adding and removing mappings to 
     *  catalogs. 
     *
     *  @return a <code> CatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogAssignmentSession getCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogAssignmentSession());
    }


    /**
     *  Gets the notification session for subscribing to changes to catalogs. 
     *
     *  @param  catalogEntryReceiver the notification callback 
     *  @return a <code> CatalogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogEntryNotificationSession getCatalogEntryNotificationSession(org.osid.cataloging.CatalogEntryReceiver catalogEntryReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEntryNotificationSession(catalogEntryReceiver));
    }


    /**
     *  Gets the notification session for subscribing to changes to catalogs 
     *  for the given catalog. 
     *
     *  @param  catalogEntryReceiver the notification callback 
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code>
     *          catalogEntryReceiver</code> or <code>catalogId</code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogEntryNotificationSession getCatalogEntryNotificationSessionForCatalog(org.osid.cataloging.CatalogEntryReceiver catalogEntryReceiver, 
                                                                                                            org.osid.id.Id catalogId)
        throws org.osid.OperationFailedException {
        
        return (getAdapteeManager().getCatalogEntryNotificationSessionForCatalog(catalogEntryReceiver, catalogId));
    }


    /**
     *  Gets the catalog lookup session. 
     *
     *  @return a <code> CatalogLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogLookupSession getCatalogLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogLookupSession());
    }


    /**
     *  Gets the catalog query session. 
     *
     *  @return a <code> CatalogQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuerySession getCatalogQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogQuerySession());
    }


    /**
     *  Gets the catalog search session. 
     *
     *  @return a <code> CatalogSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogSearchSession getCatalogSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogSearchSession());
    }


    /**
     *  Gets the catalog administrative session for creating, updating and 
     *  deleting catalogs. 
     *
     *  @return a <code> CatalogAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogAdminSession getCatalogAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogAdminSession());
    }


    /**
     *  Gets the notification session for subscribing to changes to catalogs. 
     *
     *  @param  catalogReceiver the notification callback 
     *  @return a <code> CatalogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogNotificationSession getCatalogNotificationSession(org.osid.cataloging.CatalogReceiver catalogReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogNotificationSession(catalogReceiver));
    }


    /**
     *  Gets the catalog hierarchy traversal session. 
     *
     *  @return a <code> CatalogHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogHierarchySession getCatalogHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogHierarchySession());
    }


    /**
     *  Gets the catalog hierarchy design session. 
     *
     *  @return a <code> CatalogHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogHierarchyDesignSession getCatalogHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogHierarchyDesignSession());
    }


    /**
     *  Gets the cataloging rules manager. 
     *
     *  @return a <code> CatalogingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogingRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogingRulesManager getCatalogingRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogingRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
