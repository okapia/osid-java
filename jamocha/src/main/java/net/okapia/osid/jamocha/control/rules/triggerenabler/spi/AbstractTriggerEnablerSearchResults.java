//
// AbstractTriggerEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.triggerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractTriggerEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.rules.TriggerEnablerSearchResults {

    private org.osid.control.rules.TriggerEnablerList triggerEnablers;
    private final org.osid.control.rules.TriggerEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.control.rules.records.TriggerEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractTriggerEnablerSearchResults.
     *
     *  @param triggerEnablers the result set
     *  @param triggerEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>triggerEnablers</code>
     *          or <code>triggerEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractTriggerEnablerSearchResults(org.osid.control.rules.TriggerEnablerList triggerEnablers,
                                            org.osid.control.rules.TriggerEnablerQueryInspector triggerEnablerQueryInspector) {
        nullarg(triggerEnablers, "trigger enablers");
        nullarg(triggerEnablerQueryInspector, "trigger enabler query inspectpr");

        this.triggerEnablers = triggerEnablers;
        this.inspector = triggerEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the trigger enabler list resulting from a search.
     *
     *  @return a trigger enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablers() {
        if (this.triggerEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.rules.TriggerEnablerList triggerEnablers = this.triggerEnablers;
        this.triggerEnablers = null;
	return (triggerEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.rules.TriggerEnablerQueryInspector getTriggerEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  trigger enabler search record <code> Type. </code> This method must
     *  be used to retrieve a triggerEnabler implementing the requested
     *  record.
     *
     *  @param triggerEnablerSearchRecordType a triggerEnabler search 
     *         record type 
     *  @return the trigger enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(triggerEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.TriggerEnablerSearchResultsRecord getTriggerEnablerSearchResultsRecord(org.osid.type.Type triggerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.rules.records.TriggerEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(triggerEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(triggerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record trigger enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addTriggerEnablerRecord(org.osid.control.rules.records.TriggerEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "trigger enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
