//
// AbstractIndexedMapFrontOfficeLookupSession.java
//
//    A simple framework for providing a FrontOffice lookup service
//    backed by a fixed collection of front offices with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a FrontOffice lookup service backed by a
 *  fixed collection of front offices. The front offices are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some front offices may be compatible
 *  with more types than are indicated through these front office
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>FrontOffices</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapFrontOfficeLookupSession
    extends AbstractMapFrontOfficeLookupSession
    implements org.osid.tracking.FrontOfficeLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.tracking.FrontOffice> frontOfficesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.tracking.FrontOffice>());
    private final MultiMap<org.osid.type.Type, org.osid.tracking.FrontOffice> frontOfficesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.tracking.FrontOffice>());


    /**
     *  Makes a <code>FrontOffice</code> available in this session.
     *
     *  @param  frontOffice a front office
     *  @throws org.osid.NullArgumentException <code>frontOffice<code> is
     *          <code>null</code>
     */

    @Override
    protected void putFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        super.putFrontOffice(frontOffice);

        this.frontOfficesByGenus.put(frontOffice.getGenusType(), frontOffice);
        
        try (org.osid.type.TypeList types = frontOffice.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.frontOfficesByRecord.put(types.getNextType(), frontOffice);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a front office from this session.
     *
     *  @param frontOfficeId the <code>Id</code> of the front office
     *  @throws org.osid.NullArgumentException <code>frontOfficeId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeFrontOffice(org.osid.id.Id frontOfficeId) {
        org.osid.tracking.FrontOffice frontOffice;
        try {
            frontOffice = getFrontOffice(frontOfficeId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.frontOfficesByGenus.remove(frontOffice.getGenusType());

        try (org.osid.type.TypeList types = frontOffice.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.frontOfficesByRecord.remove(types.getNextType(), frontOffice);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeFrontOffice(frontOfficeId);
        return;
    }


    /**
     *  Gets a <code>FrontOfficeList</code> corresponding to the given
     *  front office genus <code>Type</code> which does not include
     *  front offices of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known front offices or an error results. Otherwise,
     *  the returned list may contain only those front offices that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  frontOfficeGenusType a front office genus type 
     *  @return the returned <code>FrontOffice</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByGenusType(org.osid.type.Type frontOfficeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.frontoffice.ArrayFrontOfficeList(this.frontOfficesByGenus.get(frontOfficeGenusType)));
    }


    /**
     *  Gets a <code>FrontOfficeList</code> containing the given
     *  front office record <code>Type</code>. In plenary mode, the
     *  returned list contains all known front offices or an error
     *  results. Otherwise, the returned list may contain only those
     *  front offices that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  frontOfficeRecordType a front office record type 
     *  @return the returned <code>frontOffice</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByRecordType(org.osid.type.Type frontOfficeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.frontoffice.ArrayFrontOfficeList(this.frontOfficesByRecord.get(frontOfficeRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.frontOfficesByGenus.clear();
        this.frontOfficesByRecord.clear();

        super.close();

        return;
    }
}
