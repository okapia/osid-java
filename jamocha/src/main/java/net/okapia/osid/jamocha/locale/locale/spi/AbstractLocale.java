//
// AbstractLocale.java
//
//     Defines a Locale.
//
//
// Tom Coppeto
// Okapia
// 8 November 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.locale.locale.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Locale</code>.
 */

public abstract class AbstractLocale
    implements org.osid.locale.Locale {

    private org.osid.type.Type languageType;
    private org.osid.type.Type scriptType;
    private org.osid.type.Type calendarType;
    private org.osid.type.Type timeType;
    private org.osid.type.Type currencyType;
    private org.osid.type.Type unitSystemType;
    private org.osid.type.Type numericFormatType;
    private org.osid.type.Type calendarFormatType;
    private org.osid.type.Type timeFormatType;
    private org.osid.type.Type currencyFormatType;
    private org.osid.type.Type coordinateFormatType;


    /**
     *  Gets the language <code>Type</code>. 
     *
     *  @return the language type 
     */

    @OSID @Override
    public org.osid.type.Type getLanguageType() {
        return (this.languageType);
    }


    /**
     *  Sets the language <code>Type</code>.
     *
     *  @param type the language type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setLanguageType(org.osid.type.Type type) {
        nullarg(type, "language type");
        this.languageType = type;
        return;
    }


    /**
     *  Gets the script <code>Type</code>.
     *
     *  @return the script type 
     */

    @OSID @Override
    public org.osid.type.Type getScriptType() {
        return (this.scriptType);
    }


    /**
     *  Sets the script <code>Type</code>.
     *
     *  @param type the script type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setScriptType(org.osid.type.Type type) {
        nullarg(type, "script type");
        this.scriptType = type;
        return;
    }


    /**
     *  Gets the calendar <code>Type</code>. 
     *
     *  @return the calendar type 
     */

    @OSID @Override
    public org.osid.type.Type getCalendarType() {
        return (this.calendarType);
    }


    /**
     *  Sets the calendar <code>Type</code>.
     *
     *  @param type the calendar type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setCalendarType(org.osid.type.Type type) {
        nullarg(type, "calendar type");
        this.calendarType = type;
        return;
    }


    /**
     *  Gets the time <code>Type</code>. 
     *
     *  @return the time type 
     */

    @OSID @Override
    public org.osid.type.Type getTimeType() {
        return (this.timeType);
    }


    /**
     *  Sets the time <code>Type</code>.
     *
     *  @param type the time type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setTimeType(org.osid.type.Type type) {
        nullarg(type, "time type");
        this.timeType = type;
        return;
    }


    /**
     *  Gets the currency <code>Type</code>. 
     *
     *  @return the currency type 
     */

    @OSID @Override
    public org.osid.type.Type getCurrencyType() {
        return (this.currencyType);
    }


    /**
     *  Sets the currency <code>Type</code>.
     *
     *  @param type the currency type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setCurrencyType(org.osid.type.Type type) {
        nullarg(type, "currency type");
        this.currencyType = type;
        return;
    }


    /**
     *  Gets the unit system <code>Type</code>. 
     *
     *  @return the unit system type 
     */

    @OSID @Override
    public org.osid.type.Type getUnitSystemType() {
        return (this.unitSystemType);
    }


    /**
     *  Sets the unit system <code>Type</code>.
     *
     *  @param type the unit system type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setUnitSystemType(org.osid.type.Type type) {
        nullarg(type, "unit system type");
        this.unitSystemType = type;
        return;
    }


    /**
     *  Gets the numeric format <code>Type</code>. 
     *
     *  @return the numeric format type 
     */

    @OSID @Override
    public org.osid.type.Type getNumericFormatType() {
        return (this.numericFormatType);
    }


    /**
     *  Sets the numeric format <code>Type</code>.
     *
     *  @param type the numeric format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setNumericFormatType(org.osid.type.Type type) {
        nullarg(type, "numeric format type");
        this.numericFormatType = type;
        return;
    }


    /**
     *  Gets the calendar format <code>Type</code>. 
     *
     *  @return the calendar format type 
     */

    @OSID @Override
    public org.osid.type.Type getCalendarFormatType() {
        return (this.calendarFormatType);
    }


    /**
     *  Sets the calendar format <code>Type</code>.
     *
     *  @param type the calendar format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setCalendarFormatType(org.osid.type.Type type) {
        nullarg(type, "calendar format type");
        this.calendarFormatType = type;
        return;
    }


    /**
     *  Gets the time format <code>Type</code>. 
     *
     *  @return the time format type 
     */

    @OSID @Override
    public org.osid.type.Type getTimeFormatType() {
        return (this.timeFormatType);
    }


    /**
     *  Sets the time format <code>Type</code>.
     *
     *  @param type the time format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setTimeFormatType(org.osid.type.Type type) {
        nullarg(type, "time format type");
        this.timeFormatType = type;
        return;
    }


    /**
     *  Gets the currency format <code>Type</code>. 
     *
     *  @return the currency format type 
     */

    @OSID @Override
    public org.osid.type.Type getCurrencyFormatType() {
        return (this.currencyFormatType);
    }


    /**
     *  Sets the currency format <code>Type</code>.
     *
     *  @param type the currency format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setCurrencyFormatType(org.osid.type.Type type) {
        nullarg(type, "currency format type");
        this.currencyFormatType = type;
        return;
    }


    /**
     *  Gets the coordinate format <code>Type</code>. 
     *
     *  @return the coordinate format type 
     */

    @OSID @Override
    public org.osid.type.Type getCoordinateFormatType() {
        return (this.coordinateFormatType);
    }


    /**
     *  Sets the coordinate format <code>Type</code>.
     *
     *  @param type the coordinate format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void setCoordinateFormatType(org.osid.type.Type type) {
        nullarg(type, "coordinate format type");
        this.coordinateFormatType = type;
        return;
    }
}
