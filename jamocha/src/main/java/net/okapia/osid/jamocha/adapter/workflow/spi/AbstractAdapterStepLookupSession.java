//
// AbstractAdapterStepLookupSession.java
//
//    A Step lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Step lookup session adapter.
 */

public abstract class AbstractAdapterStepLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.StepLookupSession {

    private final org.osid.workflow.StepLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterStepLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStepLookupSession(org.osid.workflow.StepLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Office/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Office Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the {@code Office} associated with this session.
     *
     *  @return the {@code Office} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform {@code Step} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSteps() {
        return (this.session.canLookupSteps());
    }


    /**
     *  A complete view of the {@code Step} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepView() {
        this.session.useComparativeStepView();
        return;
    }


    /**
     *  A complete view of the {@code Step} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepView() {
        this.session.usePlenaryStepView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include steps in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    

    /**
     *  Only active steps are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepView() {
        this.session.useActiveStepView();
        return;
    }


    /**
     *  Active and inactive steps are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepView() {
        this.session.useAnyStatusStepView();
        return;
    }
    
     
    /**
     *  Gets the {@code Step} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Step} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Step} and
     *  retained for compatibility.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param stepId {@code Id} of the {@code Step}
     *  @return the step
     *  @throws org.osid.NotFoundException {@code stepId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code stepId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Step getStep(org.osid.id.Id stepId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStep(stepId));
    }


    /**
     *  Gets a {@code StepList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  steps specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Steps} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Step} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code stepIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByIds(org.osid.id.IdList stepIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepsByIds(stepIds));
    }


    /**
     *  Gets a {@code StepList} corresponding to the given
     *  step genus {@code Type} which does not include
     *  steps of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepGenusType a step genus type 
     *  @return the returned {@code Step} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByGenusType(org.osid.type.Type stepGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepsByGenusType(stepGenusType));
    }


    /**
     *  Gets a {@code StepList} corresponding to the given
     *  step genus {@code Type} and include any additional
     *  steps with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepGenusType a step genus type 
     *  @return the returned {@code Step} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByParentGenusType(org.osid.type.Type stepGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepsByParentGenusType(stepGenusType));
    }


    /**
     *  Gets a {@code StepList} containing the given
     *  step record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepRecordType a step record type 
     *  @return the returned {@code Step} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByRecordType(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepsByRecordType(stepRecordType));
    }


    /**
     *  Gets a {@code StepList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Step} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepsByProvider(resourceId));
    }


    /**
     *  Gets a list of steps by process. 
     *  
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session.
     *  
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  processId a process {@code Id} 
     *  @return the returned {@code Step} list 
     *  @throws org.osid.NullArgumentException {@code processId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsForProcess(org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepsForProcess(processId));
    }

    
    /**
     *  Gets a list of steps for which the given state is valid. The
     *  steps returned are the states specified in the previous step
     *  or the process.
     *  
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session.
     *  
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  stateId a stateId {@code Id} 
     *  @return the returned {@code Step} list 
     *  @throws org.osid.NullArgumentException {@code stateId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByState(org.osid.id.Id stateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getStepsByState(stateId));
    }


    /**
     *  Gets all {@code Steps}. 
     *
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @return a list of {@code Steps} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getSteps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSteps());
    }
}
