//
// AbstractOffsetEvent.java
//
//     Defines an OffsetEvent.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>OffsetEvent</code>.
 */

public abstract class AbstractOffsetEvent
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.calendaring.OffsetEvent {

    private org.osid.calendaring.DateTime fixedStartTime;
    private org.osid.calendaring.Event startReferenceEvent;
    private org.osid.calendaring.Duration fixedStartOffset;

    private long relativeWeekdayStartOffset;
    private long relativeStartWeekday = -1;

    private org.osid.calendaring.Duration duration;
    private org.osid.calendaring.Event endReferenceEvent;
    private org.osid.calendaring.Duration fixedEndOffset;

    private long relativeWeekdayEndOffset;
    private long relativeEndWeekday = -1;

    private org.osid.locale.DisplayText locationDescription;
    private org.osid.mapping.Location location;

    private boolean hasSponsors = false;

    private final java.util.Collection<org.osid.resource.Resource> sponsors = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.OffsetEventRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this offset event is based on a fixed start time. 
     *
     *  @return <code> true </code> if this offset is based on a fixed
     *          start time, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasFixedStartTime() {
        return ((this.fixedStartTime != null) && (this.startReferenceEvent == null));
    }


    /**
     *  Gets the fixed start time for this event. 
     *
     *  @return the fixed start time 
     *  @throws org.osid.IllegalStateException <code>
     *          hasFixedStartIme() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getFixedStartTime() {
        if (!hasFixedStartTime()) {
            throw new org.osid.IllegalStateException("hasFixedStartTime() is false");
        }

        return (this.fixedStartTime);
    }


    /**
     *  Sets the fixed start time.
     *
     *  @param time a fixed start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setFixedStartTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "fixed start time");
        this.fixedStartTime = time;
        return;
    }


    /**
     *  Gets the <code> Event Id </code> to which the start of this
     *  event is offset.
     *
     *  @return the relative event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code>
     *          hasFixedStartTime() </code> is <code> true </code>
     */

    @OSID @Override
    public org.osid.id.Id getStartReferenceEventId() {
        if (hasFixedStartTime()) {
            throw new org.osid.IllegalStateException("hasFixedStartTime() is true");
        }

        return (this.startReferenceEvent.getId());
    }


    /**
     *  Gets the <code> Event </code> to which the start of this event is 
     *  offset. 
     *
     *  @return the relative event 
     *  @throws org.osid.IllegalStateException <code> hasFixedStartTime() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getStartReferenceEvent()
        throws org.osid.OperationFailedException {

        if (hasFixedStartTime()) {
            throw new org.osid.IllegalStateException("hasFixedStartTime() is true");
        }

        return (this.startReferenceEvent);
    }


    /**
     *  Sets the start reference event.
     *
     *  @param event a start reference event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    protected void setStartReferenceEvent(org.osid.calendaring.Event event) {
        nullarg(event, "starting reference event");
        this.startReferenceEvent = event;
        return;
    }


    /**
     *  Tests if this offset start time based on a fixed period of time. 
     *
     *  @return <code> true </code> if this offset is based on a fixed
     *          period of time, <code> false </code> otherwise
     *  @throws org.osid.IllegalStateException <code>
     *          hasFixedStartTime() </code> is <code> true </code>
     */

    @OSID @Override
    public boolean hasFixedStartOffset() {
        if (hasFixedStartTime()) {
            throw new org.osid.IllegalStateException("hasFixedStartTime() is true");
        }

        return (this.fixedStartOffset != null);
    }


    /**
     *  Gets the fixed starting time offset. 
     *
     *  @return the offset 
     *  @throws org.osid.IllegalStateException <code> hasFixedStartOffset() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedStartOffset() {
        if (!hasFixedStartOffset()) {
            throw new org.osid.IllegalStateException("getFixedStartOffset() is false");
        }

        return (this.fixedStartOffset);
    }


    /**
     *  Sets the fixed start offset.
     *
     *  @param offset a fixed start offset
     *  @throws org.osid.NullArgumentException <code>offset</code> is
     *          <code>null</code>
     */

    protected void setFixedStartOffset(org.osid.calendaring.Duration offset) {
        nullarg(offset, "fixed start offset");
        this.fixedStartOffset = fixedStartOffset;
        return;
    }


    /**
     *  Tests if this starting time offset is a based on a relative weekday. 
     *
     *  @return <code> true </code> if this offset is based on a
     *          relative weekday, <code> false </code> otherwise
     *  @throws org.osid.IllegalStateException <code>
     *          hasFixedStartOffset() </code> is <code> true </code>
     */

    @OSID @Override
    public boolean hasRelativeWeekdayStartOffset() {
        if (hasFixedStartTime()) {
            throw new org.osid.IllegalStateException("hasFixedStartTime() is true");
        }

        return (this.relativeStartWeekday >= 0);
    }


    /**
     *  Gets the starting offset as the nth weekday from the relative
     *  event. Zero is no offset.
     *
     *  @return the offset 
     *  @throws org.osid.IllegalStateException <code>
     *          hasRelativeWeekdayOffset() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public long getRelativeWeekdayStartOffset() {
        if (!hasRelativeWeekdayStartOffset()) {
            throw new org.osid.IllegalStateException("hasRelativeWeekdayStartOffset() is false");
        }

        return (this.relativeWeekdayStartOffset);
    }


    /**
     *  Sets the relative weekday start offset.
     *
     *  @param offset a relative weekday start offset
     */

    protected void setRelativeWeekdayStartOffset(long offset) {
        this.relativeWeekdayStartOffset = offset;
        return;
    }


    /**
     *  Gets the starting weekday number. The weekday is based on the
     *  calendar type. On the Gregorian calendar, 0 is Sunday.
     *
     *  @return the weekday number 
     *  @throws org.osid.IllegalStateException <code>
     *          hasRelativeWeekdayOffset() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public long getRelativeStartWeekday() {
        if (!hasRelativeWeekdayStartOffset()) {
            throw new org.osid.IllegalStateException("hasRelativeWeekdayStartOffset() is false");
        }

        return (this.relativeStartWeekday);
    }


    /**
     *  Sets the relative start weekday.
     *
     *  @param weekday a relative start weekday
     *  @throws org.osid.InvalidArgumentException <code>offset</code>
     *          is negative
     */

    protected void setRelativeStartWeekday(long weekday) {
        cardinalarg(weekday, "start offset weekday");
        this.relativeStartWeekday = weekday;
        return;
    }


    /**
     *  Tests if this offset event is based on a fixed duration. 
     *
     *  @return <code> true </code> if this offset is based on a fixed 
     *          duration, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasFixedDuration() {
        return ((this.duration != null) && (this.endReferenceEvent == null));
    }


    /**
     *  Gets the duration of the offset event. 
     *
     *  @param  units the units of the duration 
     *  @return the duration 
     *  @throws org.osid.IllegalStateException <code> hasFixedDuration() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.NullArgumentException <code> units </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedDuration(org.osid.calendaring.DateTimeResolution units) {
        if (!hasFixedDuration()) {
            throw new org.osid.IllegalStateException("hasFixedDuration() is false");
        }

        return (this.duration);
    }


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setFixedDuration(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.duration = duration;
        return;
    }


    /**
     *  Gets the <code> Event Id </code> to which the end of this event is 
     *  offset. 
     *
     *  @return the relative event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasFixedDuration() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndReferenceEventId() {
        if (hasFixedDuration()) {
            throw new org.osid.IllegalStateException("hasFixedDuration() is true");
        }

        return (this.endReferenceEvent.getId());
    }


    /**
     *  Gets the <code> Event </code> to which the end of this event is 
     *  offset. 
     *
     *  @return the relative event 
     *  @throws org.osid.IllegalStateException <code> hasFixedDuration() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEndReferenceEvent()
        throws org.osid.OperationFailedException {

        if (hasFixedDuration()) {
            throw new org.osid.IllegalStateException("hasFixedDuration() is true");
        }

        return (this.endReferenceEvent);
    }


    /**
     *  Sets the end reference event.
     *
     *  @param event an end reference event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    protected void setEndReferenceEvent(org.osid.calendaring.Event event) {
        nullarg(event, "end reference event");
        this.endReferenceEvent = event;
        return;
    }


    /**
     *  Tests if this offset end time based on a fixed period of time. 
     *
     *  @return <code> true </code> if this offset is based on a fixed period 
     *          of time, <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasFixedDuration() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public boolean hasFixedEndOffset() {
        if (hasFixedDuration()) {
            throw new org.osid.IllegalStateException("hasFixedDuration() is true");
        }

        return (this.fixedEndOffset != null);
    }


    /**
     *  Gets the fixed ending time offset. 
     *
     *  @return the offset 
     *  @throws org.osid.IllegalStateException <code> hasFixedEndOffset() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedEndOffset() {
        if (!hasFixedEndOffset()) {
            throw new org.osid.IllegalStateException("hasFixedEndOffset() is false");
        }

        return (this.fixedEndOffset);
    }


    /**
     *  Sets the fixed end offset.
     *
     *  @param offset a fixed end offset
     *  @throws org.osid.NullArgumentException <code>offset</code> is
     *          <code>null</code>
     */

    protected void setFixedEndOffset(org.osid.calendaring.Duration offset) {
        nullarg(offset, "fixed end offset");
        this.fixedEndOffset = offset;
        return;
    }


    /**
     *  Tests if this ending time offset is a based on a relative
     *  weekday.
     *
     *  @return <code> true </code> if this offset is based on a
     *          relative weekday, <code> false </code> otherwise
     *  @throws org.osid.IllegalStateException <code>
     *          hasFixedEndOffset() </code> is <code> true </code>
     */

    @OSID @Override
    public boolean hasRelativeWeekdayEndOffset() {

        if (hasFixedEndOffset()) {
            throw new org.osid.IllegalStateException("hasFixedEndOffset() is true");
        }

        return (this.relativeEndWeekday >= 0);
    }


    /**
     *  Gets the ending offset as the nth weekday from the relative event. 
     *  Zero is no offset. 
     *
     *  @return the offset 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasRelativeWeekdayOffset() </code> is <code> false </code> 
     */

    @OSID @Override
    public long getRelativeWeekdayEndOffset() {
        if (!hasRelativeWeekdayEndOffset()) {
            throw new org.osid.IllegalStateException("hasRelativeWeekdayEndOffset() is false");
        }

        return (this.relativeWeekdayEndOffset);
    }


    /**
     *  Sets the relative weekday end offset.
     *
     *  @param offset a relative weekday end offset
     */

    protected void setRelativeWeekdayEndOffset(long offset) {
        this.relativeWeekdayEndOffset = offset;
        return;
    }


    /**
     *  Gets the ending weekday number. The weekday is based on the calendar 
     *  type. On the Gregorian calendar, 0 is Sunday. 
     *
     *  @return the weekday number 
     *  @throws org.osid.IllegalStateException <code>
     *          hasRelativeWeekdayOffset() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public long getRelativeEndWeekday() {
        if (!hasRelativeWeekdayEndOffset()) {
            throw new org.osid.IllegalStateException("hasRelativeWeekdayEndOffset() is false");
        }

        return (this.relativeEndWeekday);
    }


    /**
     *  Sets the relative end weekday.
     *
     *  @param weekday a relative end weekday
     *  @throws org.osid.InvalidArgumentException <code>weekday</code>
     *          is negative
     */

    protected void setRelativeEndWeekday(long weekday) {
        cardinalarg(weekday, "weekday");
        this.relativeEndWeekday = weekday;
        return;
    }


    /**
     *  Gets a descriptive location. 
     *
     *  @return the location 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.locationDescription);
    }


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    protected void setLocationDescription(org.osid.locale.DisplayText description) {
        nullarg(description, "location description");
        this.locationDescription = description;
        return;
    }


    /**
     *  Tests if a location is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated location, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.location != null);
    }


    /**
     *  Gets the location <code> Id </code>. 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location.getId());
    }


    /**
     *  Gets the <code> Location. </code> 
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location);
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    protected void setLocation(org.osid.mapping.Location location) {
        nullarg(location, "location");
        this.location = location;
        return;
    }


    /**
     *  Tests if a sponsor is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated sponsor. <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.hasSponsors);
    }


    /**
     *  Gets the <code> Id </code> of the event sponsors. 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        try {
            org.osid.resource.ResourceList sponsors = getSponsors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(sponsors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> Sponsors. </code> 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.sponsors));
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    protected void addSponsor(org.osid.resource.Resource sponsor) {
        nullarg(sponsor, "sponsor");

        this.sponsors.add(sponsor);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    protected void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        nullarg(sponsors, "sponsors");

        this.sponsors.clear();
        this.sponsors.addAll(sponsors);
        this.hasSponsors = true;

        return;
    }

    /**
     *  Tests if this offset event supports the given record
     *  <code>Type</code>.
     *
     *  @param  offsetEventRecordType an offset event record type 
     *  @return <code>true</code> if the offsetEventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type offsetEventRecordType) {
        for (org.osid.calendaring.records.OffsetEventRecord record : this.records) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>OffsetEvent</code> record <code>Type</code>.
     *
     *  @param  offsetEventRecordType the offset event record type 
     *  @return the offset event record 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventRecord getOffsetEventRecord(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.OffsetEventRecord record : this.records) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offset event. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param offsetEventRecord the offset event record
     *  @param offsetEventRecordType offset event record type
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecord</code> or
     *          <code>offsetEventRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addOffsetEventRecord(org.osid.calendaring.records.OffsetEventRecord offsetEventRecord, 
                                        org.osid.type.Type offsetEventRecordType) {

        nullarg(offsetEventRecord, "offset event record");
        addRecordType(offsetEventRecordType);
        this.records.add(offsetEventRecord);
        
        return;
    }
}
