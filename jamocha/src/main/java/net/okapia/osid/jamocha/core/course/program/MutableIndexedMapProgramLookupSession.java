//
// MutableIndexedMapProgramLookupSession
//
//    Implements a Program lookup service backed by a collection of
//    programs indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements a Program lookup service backed by a collection of
 *  programs. The programs are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some programs may be compatible
 *  with more types than are indicated through these program
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of programs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProgramLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractIndexedMapProgramLookupSession
    implements org.osid.course.program.ProgramLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapProgramLookupSession} with no programs.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

      public MutableIndexedMapProgramLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProgramLookupSession} with a
     *  single program.
     *  
     *  @param courseCatalog the course catalog
     *  @param  program a single program
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code program} is {@code null}
     */

    public MutableIndexedMapProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.program.Program program) {
        this(courseCatalog);
        putProgram(program);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProgramLookupSession} using an
     *  array of programs.
     *
     *  @param courseCatalog the course catalog
     *  @param  programs an array of programs
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code programs} is {@code null}
     */

    public MutableIndexedMapProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.program.Program[] programs) {
        this(courseCatalog);
        putPrograms(programs);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProgramLookupSession} using a
     *  collection of programs.
     *
     *  @param courseCatalog the course catalog
     *  @param  programs a collection of programs
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code programs} is {@code null}
     */

    public MutableIndexedMapProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.program.Program> programs) {

        this(courseCatalog);
        putPrograms(programs);
        return;
    }
    

    /**
     *  Makes a {@code Program} available in this session.
     *
     *  @param  program a program
     *  @throws org.osid.NullArgumentException {@code program{@code  is
     *          {@code null}
     */

    @Override
    public void putProgram(org.osid.course.program.Program program) {
        super.putProgram(program);
        return;
    }


    /**
     *  Makes an array of programs available in this session.
     *
     *  @param  programs an array of programs
     *  @throws org.osid.NullArgumentException {@code programs{@code 
     *          is {@code null}
     */

    @Override
    public void putPrograms(org.osid.course.program.Program[] programs) {
        super.putPrograms(programs);
        return;
    }


    /**
     *  Makes collection of programs available in this session.
     *
     *  @param  programs a collection of programs
     *  @throws org.osid.NullArgumentException {@code program{@code  is
     *          {@code null}
     */

    @Override
    public void putPrograms(java.util.Collection<? extends org.osid.course.program.Program> programs) {
        super.putPrograms(programs);
        return;
    }


    /**
     *  Removes a Program from this session.
     *
     *  @param programId the {@code Id} of the program
     *  @throws org.osid.NullArgumentException {@code programId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProgram(org.osid.id.Id programId) {
        super.removeProgram(programId);
        return;
    }    
}
