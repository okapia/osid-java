//
// AbstractFederatingResourceLookupSession.java
//
//     An abstract federating adapter for a ResourceLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ResourceLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingResourceLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.resource.ResourceLookupSession>
    implements org.osid.resource.ResourceLookupSession {

    private boolean parallel = false;
    private org.osid.resource.Bin bin = new net.okapia.osid.jamocha.nil.resource.bin.UnknownBin();


    /**
     *  Constructs a new <code>AbstractFederatingResourceLookupSession</code>.
     */

    protected AbstractFederatingResourceLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.resource.ResourceLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Bin/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.bin.getId());
    }


    /**
     *  Gets the <code>Bin</code> associated with this 
     *  session.
     *
     *  @return the <code>Bin</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bin);
    }


    /**
     *  Sets the <code>Bin</code>.
     *
     *  @param  bin the bin for this session
     *  @throws org.osid.NullArgumentException <code>bin</code>
     *          is <code>null</code>
     */

    protected void setBin(org.osid.resource.Bin bin) {
        nullarg(bin, "bin");
        this.bin = bin;
        return;
    }


    /**
     *  Tests if this user can perform <code>Resource</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResources() {
        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            if (session.canLookupResources()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Resource</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResourceView() {
        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            session.useComparativeResourceView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Resource</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResourceView() {
        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            session.usePlenaryResourceView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resources in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            session.useFederatedBinView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            session.useIsolatedBinView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Resource</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Resource</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Resource</code> and
     *  retained for compatibility.
     *
     *  @param  resourceId <code>Id</code> of the
     *          <code>Resource</code>
     *  @return the resource
     *  @throws org.osid.NotFoundException <code>resourceId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource(org.osid.id.Id resourceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            try {
                return (session.getResource(resourceId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(resourceId + " not found");
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  resources specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Resources</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  resourceIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>resourceIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByIds(org.osid.id.IdList resourceIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.resource.resource.MutableResourceList ret = new net.okapia.osid.jamocha.resource.resource.MutableResourceList();

        try (org.osid.id.IdList ids = resourceIds) {
            while (ids.hasNext()) {
                ret.addResource(getResource(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  resource genus <code>Type</code> which does not include
     *  resources of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  resourceGenusType a resource genus type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByGenusType(org.osid.type.Type resourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.resource.FederatingResourceList ret = getResourceList();

        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            ret.addResourceList(session.getResourcesByGenusType(resourceGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  resource genus <code>Type</code> and include any additional
     *  resources with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  resourceGenusType a resource genus type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByParentGenusType(org.osid.type.Type resourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.resource.FederatingResourceList ret = getResourceList();

        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            ret.addResourceList(session.getResourcesByParentGenusType(resourceGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ResourceList</code> containing the given
     *  resource record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  resourceRecordType a resource record type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByRecordType(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.resource.FederatingResourceList ret = getResourceList();

        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            ret.addResourceList(session.getResourcesByRecordType(resourceRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Resources</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Resources</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.resource.FederatingResourceList ret = getResourceList();

        for (org.osid.resource.ResourceLookupSession session : getSessions()) {
            ret.addResourceList(session.getResources());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.resource.resource.FederatingResourceList getResourceList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.resource.resource.ParallelResourceList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.resource.resource.CompositeResourceList());
        }
    }
}
