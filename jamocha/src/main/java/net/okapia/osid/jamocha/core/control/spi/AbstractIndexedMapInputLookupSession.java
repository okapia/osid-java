//
// AbstractIndexedMapInputLookupSession.java
//
//    A simple framework for providing an Input lookup service
//    backed by a fixed collection of inputs with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Input lookup service backed by a
 *  fixed collection of inputs. The inputs are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some inputs may be compatible
 *  with more types than are indicated through these input
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Inputs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapInputLookupSession
    extends AbstractMapInputLookupSession
    implements org.osid.control.InputLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.Input> inputsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Input>());
    private final MultiMap<org.osid.type.Type, org.osid.control.Input> inputsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Input>());


    /**
     *  Makes an <code>Input</code> available in this session.
     *
     *  @param  input an input
     *  @throws org.osid.NullArgumentException <code>input<code> is
     *          <code>null</code>
     */

    @Override
    protected void putInput(org.osid.control.Input input) {
        super.putInput(input);

        this.inputsByGenus.put(input.getGenusType(), input);
        
        try (org.osid.type.TypeList types = input.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inputsByRecord.put(types.getNextType(), input);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of inputs available in this session.
     *
     *  @param  inputs an array of inputs
     *  @throws org.osid.NullArgumentException <code>inputs<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInputs(org.osid.control.Input[] inputs) {
        for (org.osid.control.Input input : inputs) {
            putInput(input);
        }

        return;
    }


    /**
     *  Makes a collection of inputs available in this session.
     *
     *  @param  inputs a collection of inputs
     *  @throws org.osid.NullArgumentException <code>inputs<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInputs(java.util.Collection<? extends org.osid.control.Input> inputs) {
        for (org.osid.control.Input input : inputs) {
            putInput(input);
        }

        return;
    }


    /**
     *  Removes an input from this session.
     *
     *  @param inputId the <code>Id</code> of the input
     *  @throws org.osid.NullArgumentException <code>inputId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeInput(org.osid.id.Id inputId) {
        org.osid.control.Input input;
        try {
            input = getInput(inputId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.inputsByGenus.remove(input.getGenusType());

        try (org.osid.type.TypeList types = input.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inputsByRecord.remove(types.getNextType(), input);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeInput(inputId);
        return;
    }


    /**
     *  Gets an <code>InputList</code> corresponding to the given
     *  input genus <code>Type</code> which does not include
     *  inputs of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known inputs or an error results. Otherwise,
     *  the returned list may contain only those inputs that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  inputGenusType an input genus type 
     *  @return the returned <code>Input</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.InputList getInputsByGenusType(org.osid.type.Type inputGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.input.ArrayInputList(this.inputsByGenus.get(inputGenusType)));
    }


    /**
     *  Gets an <code>InputList</code> containing the given
     *  input record <code>Type</code>. In plenary mode, the
     *  returned list contains all known inputs or an error
     *  results. Otherwise, the returned list may contain only those
     *  inputs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  inputRecordType an input record type 
     *  @return the returned <code>input</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.InputList getInputsByRecordType(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.input.ArrayInputList(this.inputsByRecord.get(inputRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inputsByGenus.clear();
        this.inputsByRecord.clear();

        super.close();

        return;
    }
}
