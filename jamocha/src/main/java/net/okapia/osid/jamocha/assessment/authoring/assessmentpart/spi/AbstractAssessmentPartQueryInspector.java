//
// AbstractAssessmentPartQueryInspector.java
//
//     A template for making an AssessmentPartQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.assessmentpart.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for assessment parts.
 */

public abstract class AbstractAssessmentPartQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQueryInspector
    implements org.osid.assessment.authoring.AssessmentPartQueryInspector {

    private final java.util.Collection<org.osid.assessment.authoring.records.AssessmentPartQueryInspectorRecord> records = new java.util.ArrayList<>();
    private final OsidContainableQueryInspector inspector = new OsidContainableQueryInspector();


    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.inspector.getSequesteredTerms());
    }

    
    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParentAssessmentPartIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getParentAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Gets the section query terms. 
     *
     *  @return the section terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSectionTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the weight terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getWeightTerms() {
        return (new org.osid.search.terms.CardinalTerm[0]);
    }


    /**
     *  Gets the allocated time terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getAllocatedTimeTerms() {
        return (new org.osid.search.terms.DurationTerm[0]);
    }


    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getChildAssessmentPartIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getChildAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given assessment part query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an assessment part implementing the requested record.
     *
     *  @param assessmentPartRecordType an assessment part record type
     *  @return the assessment part query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentPartRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartQueryInspectorRecord getAssessmentPartQueryInspectorRecord(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.AssessmentPartQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentPartRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment part query. 
     *
     *  @param assessmentPartQueryInspectorRecord assessment part query inspector
     *         record
     *  @param assessmentPartRecordType assessmentPart record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentPartQueryInspectorRecord(org.osid.assessment.authoring.records.AssessmentPartQueryInspectorRecord assessmentPartQueryInspectorRecord, 
                                                   org.osid.type.Type assessmentPartRecordType) {

        addRecordType(assessmentPartRecordType);
        nullarg(assessmentPartRecordType, "assessment part record type");
        this.records.add(assessmentPartQueryInspectorRecord);        
        return;
    }


    protected class OsidContainableQueryInspector
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQueryInspector
        implements org.osid.OsidContainableQueryInspector {
    }
}
