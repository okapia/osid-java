//
// ObjectiveNodeFilterList.java
//
//     Implements a filtering ObjectiveNodeList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.learning.objectivenode;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering ObjectiveNodeList.
 */

public final class ObjectiveNodeFilterList
    extends net.okapia.osid.jamocha.inline.filter.learning.objectivenode.spi.AbstractObjectiveNodeFilterList
    implements org.osid.learning.ObjectiveNodeList,
               ObjectiveNodeFilter {

    private final ObjectiveNodeFilter filter;


    /**
     *  Creates a new <code>ObjectiveNodeFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list an <code>ObjectiveNodeList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public ObjectiveNodeFilterList(ObjectiveNodeFilter filter, org.osid.learning.ObjectiveNodeList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters ObjectiveNodes.
     *
     *  @param objectiveNode the objective node to filter
     *  @return <code>true</code> if the objective node passes the filter,
     *          <code>false</code> if the objective node should be filtered
     */

    @Override
    public boolean pass(org.osid.learning.ObjectiveNode objectiveNode) {
        return (this.filter.pass(objectiveNode));
    }
}
