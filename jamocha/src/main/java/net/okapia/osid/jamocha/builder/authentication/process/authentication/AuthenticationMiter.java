//
// AuthenticationMiter.java
//
//     Defines an Authentication miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.process.authentication;


/**
 *  Defines an <code>Authentication</code> miter for use with the builders.
 */

public interface AuthenticationMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.authentication.process.Authentication {

    
    /**
     *  Sets the agent.
     *
     *  @param agent the authenticated agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the valid flag.
     *
     *  @param valid <code> true </code> if this authentication
     *          credential is valid, <code> false </code> otherwise
     */

    public void setValid(boolean valid);


    /**
     *  Sets the expiration.
     *
     *  @param date the authenticated expiration
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setExpiration(java.util.Date date);


    /**
     *  Adds a credential.
     *
     *  @param credentialType the tyep of credential
     *  @param credential the authenticated credential
     *  @throws org.osid.NullArgumentException
     *          <code>credentialType</code> or <code>credential</code>
     *          is <code>null</code>
     */

    public void addCredential(org.osid.type.Type credentialType, java.lang.Object credential);

    
    /**
     *  Adds an Authentication record.
     *
     *  @param record an authentication record
     *  @param recordType the type of authentication record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAuthenticationRecord(org.osid.authentication.process.records.AuthenticationRecord record, org.osid.type.Type recordType);
}       


