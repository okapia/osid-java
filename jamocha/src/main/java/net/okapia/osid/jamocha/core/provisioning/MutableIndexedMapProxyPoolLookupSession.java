//
// MutableIndexedMapProxyPoolLookupSession
//
//    Implements a Pool lookup service backed by a collection of
//    pools indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Pool lookup service backed by a collection of
 *  pools. The pools are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some pools may be compatible
 *  with more types than are indicated through these pool
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of pools can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyPoolLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractIndexedMapPoolLookupSession
    implements org.osid.provisioning.PoolLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPoolLookupSession} with
     *  no pool.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPoolLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPoolLookupSession} with
     *  a single pool.
     *
     *  @param distributor the distributor
     *  @param  pool an pool
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code pool}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPoolLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.Pool pool, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putPool(pool);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPoolLookupSession} using
     *  an array of pools.
     *
     *  @param distributor the distributor
     *  @param  pools an array of pools
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code pools}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPoolLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.Pool[] pools, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putPools(pools);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPoolLookupSession} using
     *  a collection of pools.
     *
     *  @param distributor the distributor
     *  @param  pools a collection of pools
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code pools}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPoolLookupSession(org.osid.provisioning.Distributor distributor,
                                                       java.util.Collection<? extends org.osid.provisioning.Pool> pools,
                                                       org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putPools(pools);
        return;
    }

    
    /**
     *  Makes a {@code Pool} available in this session.
     *
     *  @param  pool a pool
     *  @throws org.osid.NullArgumentException {@code pool{@code 
     *          is {@code null}
     */

    @Override
    public void putPool(org.osid.provisioning.Pool pool) {
        super.putPool(pool);
        return;
    }


    /**
     *  Makes an array of pools available in this session.
     *
     *  @param  pools an array of pools
     *  @throws org.osid.NullArgumentException {@code pools{@code 
     *          is {@code null}
     */

    @Override
    public void putPools(org.osid.provisioning.Pool[] pools) {
        super.putPools(pools);
        return;
    }


    /**
     *  Makes collection of pools available in this session.
     *
     *  @param  pools a collection of pools
     *  @throws org.osid.NullArgumentException {@code pool{@code 
     *          is {@code null}
     */

    @Override
    public void putPools(java.util.Collection<? extends org.osid.provisioning.Pool> pools) {
        super.putPools(pools);
        return;
    }


    /**
     *  Removes a Pool from this session.
     *
     *  @param poolId the {@code Id} of the pool
     *  @throws org.osid.NullArgumentException {@code poolId{@code  is
     *          {@code null}
     */

    @Override
    public void removePool(org.osid.id.Id poolId) {
        super.removePool(poolId);
        return;
    }    
}
