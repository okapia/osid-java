//
// AbstractPost.java
//
//     Defines a Post builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.posting.post.spi;


/**
 *  Defines a <code>Post</code> builder.
 */

public abstract class AbstractPostBuilder<T extends AbstractPostBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.financials.posting.post.PostMiter post;


    /**
     *  Constructs a new <code>AbstractPostBuilder</code>.
     *
     *  @param post the post to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPostBuilder(net.okapia.osid.jamocha.builder.financials.posting.post.PostMiter post) {
        super(post);
        this.post = post;
        return;
    }


    /**
     *  Builds the post.
     *
     *  @return the new post
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.financials.posting.Post build() {
        (new net.okapia.osid.jamocha.builder.validator.financials.posting.post.PostValidator(getValidations())).validate(this.post);
        return (new net.okapia.osid.jamocha.builder.financials.posting.post.ImmutablePost(this.post));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the post miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.financials.posting.post.PostMiter getMiter() {
        return (this.post);
    }


    /**
     *  Sets the fiscal period.
     *
     *  @param period a fiscal period
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>period</code> is <code>null</code>
     */

    public T fiscalPeriod(org.osid.financials.FiscalPeriod period) {
        getMiter().setFiscalPeriod(period);
        return (self());
    }


    /**
     *  Marks this post as posted.
     *
     *  @return the builder
     */

    public T posted() {
        getMiter().setPosted(true);
        return (self());
    }


    /**
     *  Marks this post as not posted.
     *
     *  @return the builder
     */

    public T unposted() {
        getMiter().setPosted(false);
        return (self());
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    public T date(org.osid.calendaring.DateTime date) {
        getMiter().setDate(date);
        return (self());
    }


    /**
     *  Adds a post entry.
     *
     *  @param entry a post entry
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    public T postEntry(org.osid.financials.posting.PostEntry entry) {
        getMiter().addPostEntry(entry);
        return (self());
    }


    /**
     *  Sets all the post entries.
     *
     *  @param entries a collection of post entries
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>entries</code> is
     *          <code>null</code>
     */

    public T postEntries(java.util.Collection<org.osid.financials.posting.PostEntry> entries) {
        getMiter().setPostEntries(entries);
        return (self());
    }


    /**
     *  Sets the corrected post.
     *
     *  @param post a corrected post
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>post</code> is
     *          <code>null</code>
     */

    public T correctedPost(org.osid.financials.posting.Post post) {
        getMiter().setCorrectedPost(post);
        return (self());
    }


    /**
     *  Adds a Post record.
     *
     *  @param record a post record
     *  @param recordType the type of post record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.financials.posting.records.PostRecord record, org.osid.type.Type recordType) {
        getMiter().addPostRecord(record, recordType);
        return (self());
    }
}       


