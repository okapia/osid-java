//
// AbstractImmutableTimePeriod.java
//
//     Wraps a mutable TimePeriod to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.timeperiod.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>TimePeriod</code> to hide modifiers. This
 *  wrapper provides an immutized TimePeriod from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying timePeriod whose state changes are visible.
 */

public abstract class AbstractImmutableTimePeriod
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.calendaring.TimePeriod {

    private final org.osid.calendaring.TimePeriod timePeriod;


    /**
     *  Constructs a new <code>AbstractImmutableTimePeriod</code>.
     *
     *  @param timePeriod the time period to immutablize
     *  @throws org.osid.NullArgumentException <code>timePeriod</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableTimePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        super(timePeriod);
        this.timePeriod = timePeriod;
        return;
    }


    /**
     *  Gets the start time of the time period. 
     *
     *  @return the start time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStart() {
        return (this.timePeriod.getStart());
    }


    /**
     *  Gets the end time of the time period. 
     *
     *  @return the end time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEnd() {
        return (this.timePeriod.getEnd());
    }


    /**
     *  Gets the exception <code> Ids </code> to this time period. Recurring 
     *  events overlapping with these events do not appear in any recurring 
     *  event for this time period. 
     *
     *  @return list of exception event <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getExceptionIds() {
        return (this.timePeriod.getExceptionIds());
    }


    /**
     *  Gets the exceptions to this time period. Recurring events overlapping 
     *  with these events do not appear in any recurring event for this time 
     *  period. 
     *
     *  @return event exceptions 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getExceptions()
        throws org.osid.OperationFailedException {

        return (this.timePeriod.getExceptions());
    }


    /**
     *  Gets the time period record corresponding to the given <code> 
     *  TimePeriod </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  timePeriodRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(timePeriodRecordType) </code> is <code> true </code> . 
     *
     *  @param  timePeriodRecordType the type of the record to retrieve 
     *  @return the time period record 
     *  @throws org.osid.NullArgumentException <code> timePeriodRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(timePeriodRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodRecord getTimePeriodRecord(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException {

        return (this.timePeriod.getTimePeriodRecord(timePeriodRecordType));
    }
}

