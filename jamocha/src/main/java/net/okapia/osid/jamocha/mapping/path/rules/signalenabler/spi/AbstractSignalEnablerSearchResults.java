//
// AbstractSignalEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.signalenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSignalEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.path.rules.SignalEnablerSearchResults {

    private org.osid.mapping.path.rules.SignalEnablerList signalEnablers;
    private final org.osid.mapping.path.rules.SignalEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.path.rules.records.SignalEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSignalEnablerSearchResults.
     *
     *  @param signalEnablers the result set
     *  @param signalEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>signalEnablers</code>
     *          or <code>signalEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSignalEnablerSearchResults(org.osid.mapping.path.rules.SignalEnablerList signalEnablers,
                                            org.osid.mapping.path.rules.SignalEnablerQueryInspector signalEnablerQueryInspector) {
        nullarg(signalEnablers, "signal enablers");
        nullarg(signalEnablerQueryInspector, "signal enabler query inspectpr");

        this.signalEnablers = signalEnablers;
        this.inspector = signalEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the signal enabler list resulting from a search.
     *
     *  @return a signal enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablers() {
        if (this.signalEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.path.rules.SignalEnablerList signalEnablers = this.signalEnablers;
        this.signalEnablers = null;
	return (signalEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.path.rules.SignalEnablerQueryInspector getSignalEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  signal enabler search record <code> Type. </code> This method must
     *  be used to retrieve a signalEnabler implementing the requested
     *  record.
     *
     *  @param signalEnablerSearchRecordType a signalEnabler search 
     *         record type 
     *  @return the signal enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(signalEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SignalEnablerSearchResultsRecord getSignalEnablerSearchResultsRecord(org.osid.type.Type signalEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.path.rules.records.SignalEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(signalEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(signalEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record signal enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSignalEnablerRecord(org.osid.mapping.path.rules.records.SignalEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "signal enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
