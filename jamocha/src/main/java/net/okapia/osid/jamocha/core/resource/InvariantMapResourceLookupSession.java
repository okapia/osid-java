//
// InvariantMapResourceLookupSession
//
//    Implements a Resource lookup service backed by a fixed collection of
//    resources.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource;


/**
 *  Implements a Resource lookup service backed by a fixed
 *  collection of resources. The resources are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapResourceLookupSession
    extends net.okapia.osid.jamocha.core.resource.spi.AbstractMapResourceLookupSession
    implements org.osid.resource.ResourceLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapResourceLookupSession</code> with no
     *  resources.
     *  
     *  @param bin the bin
     *  @throws org.osid.NullArgumnetException {@code bin} is
     *          {@code null}
     */

    public InvariantMapResourceLookupSession(org.osid.resource.Bin bin) {
        setBin(bin);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapResourceLookupSession</code> with a single
     *  resource.
     *  
     *  @param bin the bin
     *  @param resource a single resource
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resource} is <code>null</code>
     */

      public InvariantMapResourceLookupSession(org.osid.resource.Bin bin,
                                               org.osid.resource.Resource resource) {
        this(bin);
        putResource(resource);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapResourceLookupSession</code> using an array
     *  of resources.
     *  
     *  @param bin the bin
     *  @param resources an array of resources
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resources} is <code>null</code>
     */

      public InvariantMapResourceLookupSession(org.osid.resource.Bin bin,
                                               org.osid.resource.Resource[] resources) {
        this(bin);
        putResources(resources);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapResourceLookupSession</code> using a
     *  collection of resources.
     *
     *  @param bin the bin
     *  @param resources a collection of resources
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resources} is <code>null</code>
     */

      public InvariantMapResourceLookupSession(org.osid.resource.Bin bin,
                                               java.util.Collection<? extends org.osid.resource.Resource> resources) {
        this(bin);
        putResources(resources);
        return;
    }
}
