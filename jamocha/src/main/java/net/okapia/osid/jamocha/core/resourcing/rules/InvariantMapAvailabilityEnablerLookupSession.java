//
// InvariantMapAvailabilityEnablerLookupSession
//
//    Implements an AvailabilityEnabler lookup service backed by a fixed collection of
//    availabilityEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements an AvailabilityEnabler lookup service backed by a fixed
 *  collection of availability enablers. The availability enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAvailabilityEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapAvailabilityEnablerLookupSession
    implements org.osid.resourcing.rules.AvailabilityEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAvailabilityEnablerLookupSession</code> with no
     *  availability enablers.
     *  
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumnetException {@code foundry} is
     *          {@code null}
     */

    public InvariantMapAvailabilityEnablerLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAvailabilityEnablerLookupSession</code> with a single
     *  availability enabler.
     *  
     *  @param foundry the foundry
     *  @param availabilityEnabler an single availability enabler
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code availabilityEnabler} is <code>null</code>
     */

      public InvariantMapAvailabilityEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.rules.AvailabilityEnabler availabilityEnabler) {
        this(foundry);
        putAvailabilityEnabler(availabilityEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAvailabilityEnablerLookupSession</code> using an array
     *  of availability enablers.
     *  
     *  @param foundry the foundry
     *  @param availabilityEnablers an array of availability enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code availabilityEnablers} is <code>null</code>
     */

      public InvariantMapAvailabilityEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.rules.AvailabilityEnabler[] availabilityEnablers) {
        this(foundry);
        putAvailabilityEnablers(availabilityEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAvailabilityEnablerLookupSession</code> using a
     *  collection of availability enablers.
     *
     *  @param foundry the foundry
     *  @param availabilityEnablers a collection of availability enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code availabilityEnablers} is <code>null</code>
     */

      public InvariantMapAvailabilityEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                               java.util.Collection<? extends org.osid.resourcing.rules.AvailabilityEnabler> availabilityEnablers) {
        this(foundry);
        putAvailabilityEnablers(availabilityEnablers);
        return;
    }
}
