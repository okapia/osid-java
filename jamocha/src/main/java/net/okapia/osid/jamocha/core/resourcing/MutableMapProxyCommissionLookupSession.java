//
// MutableMapProxyCommissionLookupSession
//
//    Implements a Commission lookup service backed by a collection of
//    commissions that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Commission lookup service backed by a collection of
 *  commissions. The commissions are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of commissions can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyCommissionLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapCommissionLookupSession
    implements org.osid.resourcing.CommissionLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyCommissionLookupSession}
     *  with no commissions.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyCommissionLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCommissionLookupSession} with a
     *  single commission.
     *
     *  @param foundry the foundry
     *  @param commission a commission
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code commission}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCommissionLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.Commission commission, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putCommission(commission);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCommissionLookupSession} using an
     *  array of commissions.
     *
     *  @param foundry the foundry
     *  @param commissions an array of commissions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code commissions}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCommissionLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.Commission[] commissions, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putCommissions(commissions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCommissionLookupSession} using a
     *  collection of commissions.
     *
     *  @param foundry the foundry
     *  @param commissions a collection of commissions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code commissions}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCommissionLookupSession(org.osid.resourcing.Foundry foundry,
                                                java.util.Collection<? extends org.osid.resourcing.Commission> commissions,
                                                org.osid.proxy.Proxy proxy) {
   
        this(foundry, proxy);
        setSessionProxy(proxy);
        putCommissions(commissions);
        return;
    }

    
    /**
     *  Makes a {@code Commission} available in this session.
     *
     *  @param commission an commission
     *  @throws org.osid.NullArgumentException {@code commission{@code 
     *          is {@code null}
     */

    @Override
    public void putCommission(org.osid.resourcing.Commission commission) {
        super.putCommission(commission);
        return;
    }


    /**
     *  Makes an array of commissions available in this session.
     *
     *  @param commissions an array of commissions
     *  @throws org.osid.NullArgumentException {@code commissions{@code 
     *          is {@code null}
     */

    @Override
    public void putCommissions(org.osid.resourcing.Commission[] commissions) {
        super.putCommissions(commissions);
        return;
    }


    /**
     *  Makes collection of commissions available in this session.
     *
     *  @param commissions
     *  @throws org.osid.NullArgumentException {@code commission{@code 
     *          is {@code null}
     */

    @Override
    public void putCommissions(java.util.Collection<? extends org.osid.resourcing.Commission> commissions) {
        super.putCommissions(commissions);
        return;
    }


    /**
     *  Removes a Commission from this session.
     *
     *  @param commissionId the {@code Id} of the commission
     *  @throws org.osid.NullArgumentException {@code commissionId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCommission(org.osid.id.Id commissionId) {
        super.removeCommission(commissionId);
        return;
    }    
}
