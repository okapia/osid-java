//
// AbstractCommitmentEnabler.java
//
//     Defines a CommitmentEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.rules.commitmentenabler.spi;


/**
 *  Defines a <code>CommitmentEnabler</code> builder.
 */

public abstract class AbstractCommitmentEnablerBuilder<T extends AbstractCommitmentEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.rules.commitmentenabler.CommitmentEnablerMiter commitmentEnabler;


    /**
     *  Constructs a new <code>AbstractCommitmentEnablerBuilder</code>.
     *
     *  @param commitmentEnabler the commitment enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCommitmentEnablerBuilder(net.okapia.osid.jamocha.builder.calendaring.rules.commitmentenabler.CommitmentEnablerMiter commitmentEnabler) {
        super(commitmentEnabler);
        this.commitmentEnabler = commitmentEnabler;
        return;
    }


    /**
     *  Builds the commitment enabler.
     *
     *  @return the new commitment enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.rules.CommitmentEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.rules.commitmentenabler.CommitmentEnablerValidator(getValidations())).validate(this.commitmentEnabler);
        return (new net.okapia.osid.jamocha.builder.calendaring.rules.commitmentenabler.ImmutableCommitmentEnabler(this.commitmentEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the commitment enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.rules.commitmentenabler.CommitmentEnablerMiter getMiter() {
        return (this.commitmentEnabler);
    }


    /**
     *  Adds a CommitmentEnabler record.
     *
     *  @param record a commitment enabler record
     *  @param recordType the type of commitment enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.rules.records.CommitmentEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addCommitmentEnablerRecord(record, recordType);
        return (self());
    }
}       


