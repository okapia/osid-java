//
// AbstractBrowsableValidator.java
//
//     Validates Browsables.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.spi;

import net.okapia.osid.jamocha.builder.validator.Validation;


/**
 *  Validates a Browsable.
 */

public abstract class AbstractBrowsableValidator
    extends AbstractValidator {

    private final ExtensibleValidator validator;


    /**
     *  Constructs a new <code>AbstractBrowsableValidator</code>.
     */

    protected AbstractBrowsableValidator() {
        this.validator = new ExtensibleValidator();
        return;
    }


    /**
     *  Constructs a new <code>AbstractBrowsableValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractBrowsableValidator(java.util.EnumSet<Validation> validation) {
        super(validation);
        this.validator = new ExtensibleValidator(validation);
        return;
    }

    
    /**
     *  Validates a Browsable.
     *
     *  @param browsable the browsable to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.NullArgumentException <code>browsable</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public void validate(org.osid.Browsable browsable) {
        try (org.osid.PropertyList properties = browsable.getProperties()) {
                test(properties, "getProperties()");
            } catch (Throwable t) {
            throw new org.osid.OsidRuntimeException(t);
        }
        
        if (browsable instanceof org.osid.Extensible) {
            org.osid.Extensible extensible = (org.osid.Extensible) browsable;
            
            this.validator.validate(extensible);
            
            try (org.osid.type.TypeList types = extensible.getRecordTypes()) {
                if (perform(Validation.TYPE_SUPPORT)) {         
                    while (types.hasNext()) {
                        org.osid.type.Type type = types.getNextType();
                        
                        try (org.osid.PropertyList properties = browsable.getPropertiesByRecordType(type)) {
                            test(properties, "getPropertiesByRecordType()");
                        } catch (org.osid.UnsupportedException ue) {
                            throw new org.osid.BadLogicException(type + " not supported in getPropertiesByRecordType()");
                        }
                    }
                }
            } catch (org.osid.OsidException oe) {
                throw new org.osid.OsidRuntimeException(oe);
            }
        }

        return;
    }


    protected class ExtensibleValidator
        extends AbstractExtensibleValidator {

        protected ExtensibleValidator() {
            return;
        }


        protected ExtensibleValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
            super(validation);
            return;
        }

        
        public void validate(org.osid.Extensible extensible) {
            super.validate(extensible);
            return;
        }
    }
}
