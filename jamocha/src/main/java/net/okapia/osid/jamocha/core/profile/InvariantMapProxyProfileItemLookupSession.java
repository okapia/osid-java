//
// InvariantMapProxyProfileItemLookupSession
//
//    Implements a ProfileItem lookup service backed by a fixed
//    collection of profileItems. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile;


/**
 *  Implements a ProfileItem lookup service backed by a fixed
 *  collection of profile items. The profile items are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyProfileItemLookupSession
    extends net.okapia.osid.jamocha.core.profile.spi.AbstractMapProfileItemLookupSession
    implements org.osid.profile.ProfileItemLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProfileItemLookupSession} with no
     *  profile items.
     *
     *  @param profile the profile
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profile} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyProfileItemLookupSession(org.osid.profile.Profile profile,
                                                  org.osid.proxy.Proxy proxy) {
        setProfile(profile);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyProfileItemLookupSession} with a single
     *  profile item.
     *
     *  @param profile the profile
     *  @param profileItem a single profile item
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profile},
     *          {@code profileItem} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProfileItemLookupSession(org.osid.profile.Profile profile,
                                                  org.osid.profile.ProfileItem profileItem, org.osid.proxy.Proxy proxy) {

        this(profile, proxy);
        putProfileItem(profileItem);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyProfileItemLookupSession} using
     *  an array of profile items.
     *
     *  @param profile the profile
     *  @param profileItems an array of profile items
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profile},
     *          {@code profileItems} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProfileItemLookupSession(org.osid.profile.Profile profile,
                                                  org.osid.profile.ProfileItem[] profileItems, org.osid.proxy.Proxy proxy) {

        this(profile, proxy);
        putProfileItems(profileItems);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProfileItemLookupSession} using a
     *  collection of profile items.
     *
     *  @param profile the profile
     *  @param profileItems a collection of profile items
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profile},
     *          {@code profileItems} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProfileItemLookupSession(org.osid.profile.Profile profile,
                                                  java.util.Collection<? extends org.osid.profile.ProfileItem> profileItems,
                                                  org.osid.proxy.Proxy proxy) {

        this(profile, proxy);
        putProfileItems(profileItems);
        return;
    }
}
