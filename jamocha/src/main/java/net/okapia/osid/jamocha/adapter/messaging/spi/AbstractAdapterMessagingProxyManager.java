//
// AbstractMessagingProxyManager.java
//
//     An adapter for a MessagingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MessagingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMessagingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.messaging.MessagingProxyManager>
    implements org.osid.messaging.MessagingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterMessagingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMessagingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMessagingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMessagingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if sending messages is supported. 
     *
     *  @return <code> true </code> if message sending is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessaging() {
        return (getAdapteeManager().supportsMessaging());
    }


    /**
     *  Tests if message lookup is supported. 
     *
     *  @return <code> true </code> if message lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageLookup() {
        return (getAdapteeManager().supportsMessageLookup());
    }


    /**
     *  Tests if querying messages is supported. 
     *
     *  @return <code> true </code> if message query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageQuery() {
        return (getAdapteeManager().supportsMessageQuery());
    }


    /**
     *  Tests if message search is supported. 
     *
     *  @return <code> true </code> if message search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageSearch() {
        return (getAdapteeManager().supportsMessageSearch());
    }


    /**
     *  Tests if creating, updating and deleting messages is supported. 
     *
     *  @return <code> true </code> if message administration is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageAdmin() {
        return (getAdapteeManager().supportsMessageAdmin());
    }


    /**
     *  Tests if message notification is supported. Messages may be sent when 
     *  messages are created, modified, or deleted. 
     *
     *  @return <code> true </code> if message notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageNotification() {
        return (getAdapteeManager().supportsMessageNotification());
    }


    /**
     *  Tests if retrieving mappings of message and mailboxes is supported. 
     *
     *  @return <code> true </code> if message mailbox mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageMailbox() {
        return (getAdapteeManager().supportsMessageMailbox());
    }


    /**
     *  Tests if managing mappings of messages and mailboxes is supported. 
     *
     *  @return <code> true </code> if message mailbox assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageMailboxAssignment() {
        return (getAdapteeManager().supportsMessageMailboxAssignment());
    }


    /**
     *  Tests if a messaging smart mailbox service is supported. 
     *
     *  @return <code> true </code> if a message smart mailbox service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageSmartMailbox() {
        return (getAdapteeManager().supportsMessageSmartMailbox());
    }


    /**
     *  Tests if receipt lookup is supported. 
     *
     *  @return <code> true </code> if receipt lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptLookup() {
        return (getAdapteeManager().supportsReceiptLookup());
    }


    /**
     *  Tests if updating receipts is supported. 
     *
     *  @return <code> true </code> if receipt administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptAdmin() {
        return (getAdapteeManager().supportsReceiptAdmin());
    }


    /**
     *  Tests if receipts notification is supported. Messages may be sent when 
     *  receipts are created, modified, or deleted. 
     *
     *  @return <code> true </code> if receipt notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptNotification() {
        return (getAdapteeManager().supportsReceiptNotification());
    }


    /**
     *  Tests if mailbox lookup is supported. 
     *
     *  @return <code> true </code> if mailbox lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxLookup() {
        return (getAdapteeManager().supportsMailboxLookup());
    }


    /**
     *  Tests if mailbox search is supported. 
     *
     *  @return <code> true </code> if mailbox search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxSearch() {
        return (getAdapteeManager().supportsMailboxSearch());
    }


    /**
     *  Tests if mailbox administration is supported. 
     *
     *  @return <code> true </code> if mailbox administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxAdmin() {
        return (getAdapteeManager().supportsMailboxAdmin());
    }


    /**
     *  Tests if mailbox notification is supported. Messages may be sent when 
     *  <code> Mailbox </code> objects are created, deleted or updated. 
     *  Notifications for messages within mailboxes are sent via the message 
     *  notification session. 
     *
     *  @return <code> true </code> if mailbox notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxNotification() {
        return (getAdapteeManager().supportsMailboxNotification());
    }


    /**
     *  Tests if a mailbox hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a mailbox hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxHierarchy() {
        return (getAdapteeManager().supportsMailboxHierarchy());
    }


    /**
     *  Tests if a mailbox hierarchy design is supported. 
     *
     *  @return <code> true </code> if a mailbox hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxHierarchyDesign() {
        return (getAdapteeManager().supportsMailboxHierarchyDesign());
    }


    /**
     *  Tests if a messaging batch service is supported. 
     *
     *  @return <code> true </code> if a messaging batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessagingBatch() {
        return (getAdapteeManager().supportsMessagingBatch());
    }


    /**
     *  Gets all the message record types supported. 
     *
     *  @return the list of supported message record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMessageRecordTypes() {
        return (getAdapteeManager().getMessageRecordTypes());
    }


    /**
     *  Tests if a given message record type is supported. 
     *
     *  @param  messageRecordType the message type 
     *  @return <code> true </code> if the message record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> messageRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMessageRecordType(org.osid.type.Type messageRecordType) {
        return (getAdapteeManager().supportsMessageRecordType(messageRecordType));
    }


    /**
     *  Gets all the message search record types supported. 
     *
     *  @return the list of supported message search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMessageSearchRecordTypes() {
        return (getAdapteeManager().getMessageSearchRecordTypes());
    }


    /**
     *  Tests if a given message search type is supported. 
     *
     *  @param  messageSearchRecordType the message search type 
     *  @return <code> true </code> if the message search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> messageSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMessageSearchRecordType(org.osid.type.Type messageSearchRecordType) {
        return (getAdapteeManager().supportsMessageSearchRecordType(messageSearchRecordType));
    }


    /**
     *  Gets all the receipt record types supported. 
     *
     *  @return the list of supported receipt record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getReceiptRecordTypes() {
        return (getAdapteeManager().getReceiptRecordTypes());
    }


    /**
     *  Tests if a given receipt record type is supported. 
     *
     *  @param  receiptRecordType the mesreceiptsage type 
     *  @return <code> true </code> if the receipt record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> receiptRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsReceiptRecordType(org.osid.type.Type receiptRecordType) {
        return (getAdapteeManager().supportsReceiptRecordType(receiptRecordType));
    }


    /**
     *  Gets all the mailbox record types supported. 
     *
     *  @return the list of supported mailbox record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMailboxRecordTypes() {
        return (getAdapteeManager().getMailboxRecordTypes());
    }


    /**
     *  Tests if a given mailbox record type is supported. 
     *
     *  @param  mailboxRecordType the mailbox record type 
     *  @return <code> true </code> if the mailbox record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mailboxRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMailboxRecordType(org.osid.type.Type mailboxRecordType) {
        return (getAdapteeManager().supportsMailboxRecordType(mailboxRecordType));
    }


    /**
     *  Gets all the mailbox search record types supported. 
     *
     *  @return the list of supported mailbox search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMailboxSearchRecordTypes() {
        return (getAdapteeManager().getMailboxSearchRecordTypes());
    }


    /**
     *  Tests if a given mailbox search record type is supported. 
     *
     *  @param  mailboxSearchRecordType the mailbox search record type 
     *  @return <code> true </code> if the mailbox search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mailboxSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMailboxSearchRecordType(org.osid.type.Type mailboxSearchRecordType) {
        return (getAdapteeManager().supportsMailboxSearchRecordType(mailboxSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message 
     *  sending service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MessagingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessaging() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessagingSession getMessagingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessagingSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MessageLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageLookupSession getMessageLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message lookup 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a MessageLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsMessageLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageLookupSession getMessageLookupSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageLookupSessionForMailbox(mailboxId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MessageQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMessageQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message query 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return a <code> MessageQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Mailbox </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMessageQuerySessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageQuerySessionForMailbox(mailboxId, proxy));
    }


    /**
     *  Gets a message search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MessageSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSearchSession getMessageSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageSearchSession(proxy));
    }


    /**
     *  Gets a message search session for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a MessageSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsMessageSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSearchSession getMessageSearchSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageSearchSessionForMailbox(mailboxId, proxy));
    }


    /**
     *  Gets a message administration session for creating, updating and 
     *  deleting messages. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MessageAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageAdminSession getMessageAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageAdminSession(proxy));
    }


    /**
     *  Gets a message administration session for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a MessageAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageAdminSession getMessageAdminSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageAdminSessionForMailbox(mailboxId, proxy));
    }


    /**
     *  Gets the message notification session for the given mailbox. 
     *
     *  @param  messageReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a MessageNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageNotificationSession getMessageNotificationSession(org.osid.messaging.MessageReceiver messageReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageNotificationSession(messageReceiver, proxy));
    }


    /**
     *  Gets the message notification session for the given mailbox. 
     *
     *  @param  messageReceiver notification callback 
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a MessageNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> messageReceiver, 
     *          mailboxId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageNotificationSession getMessageNotificationSessionForMailbox(org.osid.messaging.MessageReceiver messageReceiver, 
                                                                                                 org.osid.id.Id mailboxId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageNotificationSessionForMailbox(messageReceiver, mailboxId, proxy));
    }


    /**
     *  Gets the session for retrieving message to mailbox mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MessageMailboxSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageMailbox() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageMailboxSession getMessageMailboxSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageMailboxSession(proxy));
    }


    /**
     *  Gets the session for assigning message to mailbox mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MessageMailboxAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageMailboxAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageMailboxAssignmentSession getMessageMailboxAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageMailboxAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage message smart mailboxes. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return a <code> MessageSmartMailboxesession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageSmartMailbox() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSmartMailboxSession getMessageSmartMailboxSession(org.osid.id.Id mailboxId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageSmartMailboxSession(mailboxId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptLookupSession getReceiptLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt lookup 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptLookupSession getReceiptLookupSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptLookupSessionForMailbox(mailboxId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptAdminSession getReceiptAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt 
     *  administrative service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptAdminSession getReceiptAdminSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptAdminSessionForMailbox(mailboxId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to receipt 
     *  changes. 
     *
     *  @param  receiptReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> receiptReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptNotificationSession getReceiptNotificationSession(org.osid.messaging.ReceiptReceiver receiptReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptNotificationSession(receiptReceiver, proxy));
    }


    /**
     *  Gets the receipt notification session for the given mailbox. 
     *
     *  @param  receiptReceiver the notification callback 
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> receiptReceiver, 
     *          mailboxId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptNotificationSession getReceiptNotificationSessionForMailbox(org.osid.messaging.ReceiptReceiver receiptReceiver, 
                                                                                                 org.osid.id.Id mailboxId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptNotificationSessionForMailbox(receiptReceiver, mailboxId, proxy));
    }


    /**
     *  Gets the mailbox lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxLookupSession getMailboxLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxLookupSession(proxy));
    }


    /**
     *  Gets the mailbox query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuerySession getMailboxQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxQuerySession(proxy));
    }


    /**
     *  Gets the mailbox search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxSearchSession getMailboxSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxSearchSession(proxy));
    }


    /**
     *  Gets the mailbox administrative session for creating, updating and 
     *  deleteing mailboxes. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxAdminSession getMailboxAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxAdminSession(proxy));
    }


    /**
     *  Gets the notification session for subscribing to changes to a mailbox. 
     *
     *  @param  mailboxReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> MailboxNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxNotificationSession getMailboxNotificationSession(org.osid.messaging.MailboxReceiver mailboxReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxNotificationSession(mailboxReceiver, proxy));
    }


    /**
     *  Gets the mailbox hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MailboxHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxHierarchySession getMailboxHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxHierarchySession(proxy));
    }


    /**
     *  Gets the mailbox hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxHierarchyDesignSession getMailboxHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> MessagingBatchProxyManager. </code> 
     *
     *  @return a <code> MessagingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessagingBatchProxyManager getMessagingBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessagingBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
