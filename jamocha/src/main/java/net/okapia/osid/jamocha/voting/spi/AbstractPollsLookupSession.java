//
// AbstractPollsLookupSession.java
//
//    A starter implementation framework for providing a Polls
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Polls lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAllPolls(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPollsLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.voting.PollsLookupSession {

    private boolean pedantic = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();
    


    /**
     *  Tests if this user can perform <code>Polls</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPolls() {
        return (true);
    }


    /**
     *  A complete view of the <code>Polls</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePollsView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Polls</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPollsView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Polls</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Polls</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Polls</code> and retained for
     *  compatibility.
     *
     *  @param  pollsId <code>Id</code> of the
     *          <code>Polls</code>
     *  @return the polls
     *  @throws org.osid.NotFoundException <code>pollsId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pollsId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.voting.PollsList pollses = getAllPolls()) {
            while (pollses.hasNext()) {
                org.osid.voting.Polls polls = pollses.getNextPolls();
                if (polls.getId().equals(pollsId)) {
                    return (polls);
                }
            }
        } 

        throw new org.osid.NotFoundException(pollsId + " not found");
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the polls
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Polls</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAllPolls()</code>.
     *
     *  @param  pollsIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>pollsIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByIds(org.osid.id.IdList pollsIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.voting.Polls> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = pollsIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPolls(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("polls " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.voting.polls.LinkedPollsList(ret));
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given polls
     *  genus <code>Type</code> which does not include polls of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known polls or
     *  an error results. Otherwise, the returned list may contain
     *  only those polls that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAllPolls()</code>.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.polls.PollsGenusFilterList(getAllPolls(), pollsGenusType));
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given polls
     *  genus <code>Type</code> and include any additional polls with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known polls or
     *  an error results. Otherwise, the returned list may contain
     *  only those polls that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAllPolls()</code>.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByParentGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPollsByGenusType(pollsGenusType));
    }


    /**
     *  Gets a <code>PollsList</code> containing the given polls
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known polls or
     *  an error results. Otherwise, the returned list may contain
     *  only those polls that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAllPolls()</code>.
     *
     *  @param  pollsRecordType a polls record type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByRecordType(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.polls.PollsRecordFilterList(getAllPolls(), pollsRecordType));
    }


    /**
     *  Gets a <code>PollsList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known polls or
     *  an error results. Otherwise, the returned list may contain
     *  only those polls that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Polls</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.voting.polls.PollsProviderFilterList(getAllPolls(), resourceId));
    }


    /**
     *  Gets all <code>Polls</code>.
     *
     *  In plenary mode, the returned list contains all known polls or
     *  an error results. Otherwise, the returned list may contain
     *  only those polls that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Polls</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.voting.PollsList getAllPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the polls list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of polls
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.voting.PollsList filterPollsOnViews(org.osid.voting.PollsList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
