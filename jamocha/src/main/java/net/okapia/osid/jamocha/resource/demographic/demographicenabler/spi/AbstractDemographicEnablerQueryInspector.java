//
// AbstractDemographicEnablerQueryInspector.java
//
//     A template for making a DemographicEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographicenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for demographic enablers.
 */

public abstract class AbstractDemographicEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.resource.demographic.DemographicEnablerQueryInspector {

    private final java.util.Collection<org.osid.resource.demographic.records.DemographicEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledDemographicIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getRuledDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBinIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given demographic enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a demographic enabler implementing the requested record.
     *
     *  @param demographicEnablerRecordType a demographic enabler record type
     *  @return the demographic enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicEnablerQueryInspectorRecord getDemographicEnablerQueryInspectorRecord(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(demographicEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this demographic enabler query. 
     *
     *  @param demographicEnablerQueryInspectorRecord demographic enabler query inspector
     *         record
     *  @param demographicEnablerRecordType demographicEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDemographicEnablerQueryInspectorRecord(org.osid.resource.demographic.records.DemographicEnablerQueryInspectorRecord demographicEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type demographicEnablerRecordType) {

        addRecordType(demographicEnablerRecordType);
        nullarg(demographicEnablerRecordType, "demographic enabler record type");
        this.records.add(demographicEnablerQueryInspectorRecord);        
        return;
    }
}
