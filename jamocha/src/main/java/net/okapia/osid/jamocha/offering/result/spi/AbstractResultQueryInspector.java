//
// AbstractResultQueryInspector.java
//
//     A template for making a ResultQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.result.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for results.
 */

public abstract class AbstractResultQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.offering.ResultQueryInspector {

    private final java.util.Collection<org.osid.offering.records.ResultQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the participant <code> Id </code> query terms. 
     *
     *  @return the participant <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParticipantIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the participant query terms. 
     *
     *  @return the participant terms 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQueryInspector[] getParticipantTerms() {
        return (new org.osid.offering.ParticipantQueryInspector[0]);
    }


    /**
     *  Gets the grade <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade query terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the value query terms. 
     *
     *  @return the value terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getValueTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given result query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a result implementing the requested record.
     *
     *  @param resultRecordType a result record type
     *  @return the result query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resultRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ResultQueryInspectorRecord getResultQueryInspectorRecord(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ResultQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(resultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resultRecordType + " is not supported");
    }


    /**
     *  Adds a record to this result query. 
     *
     *  @param resultQueryInspectorRecord result query inspector
     *         record
     *  @param resultRecordType result record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResultQueryInspectorRecord(org.osid.offering.records.ResultQueryInspectorRecord resultQueryInspectorRecord, 
                                                   org.osid.type.Type resultRecordType) {

        addRecordType(resultRecordType);
        nullarg(resultRecordType, "result record type");
        this.records.add(resultQueryInspectorRecord);        
        return;
    }
}
