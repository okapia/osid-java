//
// AbstractLocationSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.location.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractLocationSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.LocationSearchResults {

    private org.osid.mapping.LocationList locations;
    private final org.osid.mapping.LocationQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.records.LocationSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractLocationSearchResults.
     *
     *  @param locations the result set
     *  @param locationQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>locations</code>
     *          or <code>locationQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractLocationSearchResults(org.osid.mapping.LocationList locations,
                                            org.osid.mapping.LocationQueryInspector locationQueryInspector) {
        nullarg(locations, "locations");
        nullarg(locationQueryInspector, "location query inspectpr");

        this.locations = locations;
        this.inspector = locationQueryInspector;

        return;
    }


    /**
     *  Gets the location list resulting from a search.
     *
     *  @return a location list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocations() {
        if (this.locations == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.LocationList locations = this.locations;
        this.locations = null;
	return (locations);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.LocationQueryInspector getLocationQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  location search record <code> Type. </code> This method must
     *  be used to retrieve a location implementing the requested
     *  record.
     *
     *  @param locationSearchRecordType a location search 
     *         record type 
     *  @return the location search
     *  @throws org.osid.NullArgumentException
     *          <code>locationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(locationSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.LocationSearchResultsRecord getLocationSearchResultsRecord(org.osid.type.Type locationSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.records.LocationSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(locationSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(locationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record location search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addLocationRecord(org.osid.mapping.records.LocationSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "location record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
