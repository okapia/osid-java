//
// AbstractAssessmentEntrySearchOdrer.java
//
//     Defines an AssessmentEntrySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.assessmententry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AssessmentEntrySearchOrder}.
 */

public abstract class AbstractAssessmentEntrySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.course.chronicle.AssessmentEntrySearchOrder {

    private final java.util.Collection<org.osid.course.chronicle.records.AssessmentEntrySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessment(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an assessment order is available. 
     *
     *  @return <code> true </code> if an assessment order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the assessment order. 
     *
     *  @return the assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the completion 
     *  date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDateCompleted(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the program. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgram(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a program order is available. 
     *
     *  @return <code> true </code> if a program order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearchOrder() {
        return (false);
    }


    /**
     *  Gets the program order. 
     *
     *  @return the program search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchOrder getProgramSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the course. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourse(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the course search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getCourseSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for GPAs. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrade(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system search order. 
     *
     *  @return the grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getGradeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for scores. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScoreScale(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade system order is available. 
     *
     *  @return <code> true </code> if a grade system order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreScaleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system order. 
     *
     *  @return the grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreScaleSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getScoreScaleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScoreScaleSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScore(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  assessmentEntryRecordType an assessment entry record type 
     *  @return {@code true} if the assessmentEntryRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentEntryRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentEntryRecordType) {
        for (org.osid.course.chronicle.records.AssessmentEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  assessmentEntryRecordType the assessment entry record type 
     *  @return the assessment entry search order record
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentEntryRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(assessmentEntryRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AssessmentEntrySearchOrderRecord getAssessmentEntrySearchOrderRecord(org.osid.type.Type assessmentEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AssessmentEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentEntryRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this assessment entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentEntryRecord the assessment entry search odrer record
     *  @param assessmentEntryRecordType assessment entry record type
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentEntryRecord} or
     *          {@code assessmentEntryRecordTypeassessmentEntry} is
     *          {@code null}
     */
            
    protected void addAssessmentEntryRecord(org.osid.course.chronicle.records.AssessmentEntrySearchOrderRecord assessmentEntrySearchOrderRecord, 
                                     org.osid.type.Type assessmentEntryRecordType) {

        addRecordType(assessmentEntryRecordType);
        this.records.add(assessmentEntrySearchOrderRecord);
        
        return;
    }
}
