//
// AbstractQueryStepLookupSession.java
//
//    An inline adapter that maps a StepLookupSession to
//    a StepQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a StepLookupSession to
 *  a StepQuerySession.
 */

public abstract class AbstractQueryStepLookupSession
    extends net.okapia.osid.jamocha.workflow.spi.AbstractStepLookupSession
    implements org.osid.workflow.StepLookupSession {

    private boolean activeonly    = false;
    private final org.osid.workflow.StepQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryStepLookupSession.
     *
     *  @param querySession the underlying step query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryStepLookupSession(org.osid.workflow.StepQuerySession querySession) {
        nullarg(querySession, "step query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Office</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform <code>Step</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSteps() {
        return (this.session.canSearchSteps());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include steps in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    

    /**
     *  Only active steps are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive steps are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Step</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Step</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Step</code> and
     *  retained for compatibility.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepId <code>Id</code> of the
     *          <code>Step</code>
     *  @return the step
     *  @throws org.osid.NotFoundException <code>stepId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stepId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Step getStep(org.osid.id.Id stepId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.StepQuery query = getQuery();
        query.matchId(stepId, true);
        org.osid.workflow.StepList steps = this.session.getStepsByQuery(query);
        if (steps.hasNext()) {
            return (steps.getNextStep());
        } 
        
        throw new org.osid.NotFoundException(stepId + " not found");
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  steps specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Steps</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>stepIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByIds(org.osid.id.IdList stepIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.StepQuery query = getQuery();

        try (org.osid.id.IdList ids = stepIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getStepsByQuery(query));
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given
     *  step genus <code>Type</code> which does not include
     *  steps of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepGenusType a step genus type 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByGenusType(org.osid.type.Type stepGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.StepQuery query = getQuery();
        query.matchGenusType(stepGenusType, true);
        return (this.session.getStepsByQuery(query));
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given
     *  step genus <code>Type</code> and include any additional
     *  steps with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepGenusType a step genus type 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByParentGenusType(org.osid.type.Type stepGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.StepQuery query = getQuery();
        query.matchParentGenusType(stepGenusType, true);
        return (this.session.getStepsByQuery(query));
    }


    /**
     *  Gets a <code>StepList</code> containing the given
     *  step record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepRecordType a step record type 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByRecordType(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.StepQuery query = getQuery();
        query.matchRecordType(stepRecordType, true);
        return (this.session.getStepsByQuery(query));
    }


    /**
     *  Gets a <code>StepList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known steps or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  steps that are accessible through this session. 
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Step</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.StepQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getStepsByQuery(query));        
    }


    /**
     *  Gets a list of steps by process.
     *  
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session.
     *  
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  processId a process <code> Id </code> 
     *  @return the returned <code> Step </code> list 
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsForProcess(org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.StepQuery query = getQuery();
        query.matchProcessId(processId, true);
        return (this.session.getStepsByQuery(query));
    }


    /**
     *  Gets a list of steps for which the given state is valid. The
     *  steps returned are the states ified in the previous step or
     *  the process.
     *  
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session.
     *  
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  stateId a stateId <code> Id </code> 
     *  @return the returned <code> Step </code> list 
     *  @throws org.osid.NullArgumentException <code> stateId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByState(org.osid.id.Id stateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.StepQuery query = getQuery();
        query.matchInputStateId(stateId, true);
        return (this.session.getStepsByQuery(query));
    }


    /**
     *  Gets all <code>Steps</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @return a list of <code>Steps</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getSteps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.StepQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getStepsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.workflow.StepQuery getQuery() {
        org.osid.workflow.StepQuery query = this.session.getStepQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
