//
// AbstractInputLookupSession.java
//
//    A starter implementation framework for providing an Input
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Input
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getInputs(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractInputLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.control.InputLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();
    

    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>Input</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInputs() {
        return (true);
    }


    /**
     *  A complete view of the <code>Input</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInputView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Input</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInputView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inputs in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only inputs whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveInputView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All inputs of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveInputView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Input</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Input</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Input</code> and
     *  retained for compatibility.
     *
     *  In effective mode, inputs are returned that are currently
     *  effective.  In any effective mode, effective inputs and
     *  those currently expired are returned.
     *
     *  @param  inputId <code>Id</code> of the
     *          <code>Input</code>
     *  @return the input
     *  @throws org.osid.NotFoundException <code>inputId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>inputId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Input getInput(org.osid.id.Id inputId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.control.InputList inputs = getInputs()) {
            while (inputs.hasNext()) {
                org.osid.control.Input input = inputs.getNextInput();
                if (input.getId().equals(inputId)) {
                    return (input);
                }
            }
        } 

        throw new org.osid.NotFoundException(inputId + " not found");
    }


    /**
     *  Gets an <code>InputList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inputs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Inputs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, inputs are returned that are currently
     *  effective.  In any effective mode, effective inputs and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getInputs()</code>.
     *
     *  @param  inputIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Input</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>inputIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.InputList getInputsByIds(org.osid.id.IdList inputIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.control.Input> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = inputIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getInput(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("input " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.control.input.LinkedInputList(ret));
    }


    /**
     *  Gets an <code>InputList</code> corresponding to the given
     *  input genus <code>Type</code> which does not include
     *  inputs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, inputs are returned that are currently
     *  effective.  In any effective mode, effective inputs and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getInputs()</code>.
     *
     *  @param  inputGenusType an input genus type 
     *  @return the returned <code>Input</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.InputList getInputsByGenusType(org.osid.type.Type inputGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.input.InputGenusFilterList(getInputs(), inputGenusType));
    }


    /**
     *  Gets an <code>InputList</code> corresponding to the given
     *  input genus <code>Type</code> and include any additional
     *  inputs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, inputs are returned that are currently
     *  effective.  In any effective mode, effective inputs and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInputs()</code>.
     *
     *  @param  inputGenusType an input genus type 
     *  @return the returned <code>Input</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.InputList getInputsByParentGenusType(org.osid.type.Type inputGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getInputsByGenusType(inputGenusType));
    }


    /**
     *  Gets an <code>InputList</code> containing the given
     *  input record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, inputs are returned that are currently
     *  effective.  In any effective mode, effective inputs and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInputs()</code>.
     *
     *  @param  inputRecordType an input record type 
     *  @return the returned <code>Input</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.InputList getInputsByRecordType(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.input.InputRecordFilterList(getInputs(), inputRecordType));
    }


    /**
     *  Gets an <code>InputList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible
     *  through this session.
     *  
     *  In active mode, inputs are returned that are currently
     *  active. In any status mode, active and inactive inputs
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Input</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.control.InputList getInputsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.input.TemporalInputFilterList(getInputs(), from, to));
    }
        

    /**
     *  Gets a list of inputs corresponding to a device
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible
     *  through this session.
     *
     *  In effective mode, inputs are returned that are
     *  currently effective.  In any effective mode, effective
     *  inputs and those currently expired are returned.
     *
     *  @param  deviceId the <code>Id</code> of the device
     *  @return the returned <code>InputList</code>
     *  @throws org.osid.NullArgumentException <code>deviceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.control.InputList getInputsForDevice(org.osid.id.Id deviceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.input.InputFilterList(new DeviceFilter(deviceId), getInputs()));
    }


    /**
     *  Gets a list of inputs corresponding to a device
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible
     *  through this session.
     *
     *  In effective mode, inputs are returned that are
     *  currently effective.  In any effective mode, effective
     *  inputs and those currently expired are returned.
     *
     *  @param  deviceId the <code>Id</code> of the device
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>InputList</code>
     *  @throws org.osid.NullArgumentException <code>deviceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.InputList getInputsForDeviceOnDate(org.osid.id.Id deviceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.input.TemporalInputFilterList(getInputsForDevice(deviceId), from, to));
    }


    /**
     *  Gets a list of inputs corresponding to a controller
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible
     *  through this session.
     *
     *  In effective mode, inputs are returned that are
     *  currently effective.  In any effective mode, effective
     *  inputs and those currently expired are returned.
     *
     *  @param  controllerId the <code>Id</code> of the controller
     *  @return the returned <code>InputList</code>
     *  @throws org.osid.NullArgumentException <code>controllerId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.control.InputList getInputsForController(org.osid.id.Id controllerId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.input.InputFilterList(new ControllerFilter(controllerId), getInputs()));
    }


    /**
     *  Gets a list of inputs corresponding to a controller
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible
     *  through this session.
     *
     *  In effective mode, inputs are returned that are
     *  currently effective.  In any effective mode, effective
     *  inputs and those currently expired are returned.
     *
     *  @param  controllerId the <code>Id</code> of the controller
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>InputList</code>
     *  @throws org.osid.NullArgumentException <code>controllerId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.InputList getInputsForControllerOnDate(org.osid.id.Id controllerId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.input.TemporalInputFilterList(getInputsForController(controllerId), from, to));
    }


    /**
     *  Gets a list of inputs corresponding to device and controller
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible
     *  through this session.
     *
     *  In effective mode, inputs are returned that are
     *  currently effective.  In any effective mode, effective
     *  inputs and those currently expired are returned.
     *
     *  @param  deviceId the <code>Id</code> of the device
     *  @param  controllerId the <code>Id</code> of the controller
     *  @return the returned <code>InputList</code>
     *  @throws org.osid.NullArgumentException <code>deviceId</code>,
     *          <code>controllerId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.control.InputList getInputsForDeviceAndController(org.osid.id.Id deviceId,
                                                                        org.osid.id.Id controllerId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.input.InputFilterList(new ControllerFilter(controllerId), getInputsForDevice(deviceId)));
    }


    /**
     *  Gets a list of inputs corresponding to device and controller
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible
     *  through this session.
     *
     *  In effective mode, inputs are returned that are
     *  currently effective.  In any effective mode, effective
     *  inputs and those currently expired are returned.
     *
     *  @param  controllerId the <code>Id</code> of the controller
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>InputList</code>
     *  @throws org.osid.NullArgumentException <code>deviceId</code>,
     *          <code>controllerId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.InputList getInputsForDeviceAndControllerOnDate(org.osid.id.Id deviceId,
                                                                              org.osid.id.Id controllerId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.input.TemporalInputFilterList(getInputsForDeviceAndController(deviceId, controllerId), from, to));
    }


    /**
     *  Gets all <code>Inputs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  inputs or an error results. Otherwise, the returned list
     *  may contain only those inputs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, inputs are returned that are currently
     *  effective.  In any effective mode, effective inputs and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Inputs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.control.InputList getInputs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the input list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of inputs
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.control.InputList filterInputsOnViews(org.osid.control.InputList list)
        throws org.osid.OperationFailedException {

        org.osid.control.InputList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.control.input.EffectiveInputFilterList(ret);
        }

        return (ret);
    }


    public static class DeviceFilter
        implements net.okapia.osid.jamocha.inline.filter.control.input.InputFilter {
         
        private final org.osid.id.Id deviceId;
         
         
        /**
         *  Constructs a new <code>DeviceFilter</code>.
         *
         *  @param deviceId the device to filter
         *  @throws org.osid.NullArgumentException
         *          <code>deviceId</code> is <code>null</code>
         */
        
        public DeviceFilter(org.osid.id.Id deviceId) {
            nullarg(deviceId, "device Id");
            this.deviceId = deviceId;
            return;
        }

         
        /**
         *  Used by the InputFilterList to filter the input list based
         *  on device.
         *
         *  @param input the input
         *  @return <code>true</code> to pass the input,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.control.Input input) {
            return (input.getDeviceId().equals(this.deviceId));
        }
    }


    public static class ControllerFilter
        implements net.okapia.osid.jamocha.inline.filter.control.input.InputFilter {
         
        private final org.osid.id.Id controllerId;
         
         
        /**
         *  Constructs a new <code>ControllerFilter</code>.
         *
         *  @param controllerId the controller to filter
         *  @throws org.osid.NullArgumentException
         *          <code>controllerId</code> is <code>null</code>
         */
        
        public ControllerFilter(org.osid.id.Id controllerId) {
            nullarg(controllerId, "controller Id");
            this.controllerId = controllerId;
            return;
        }

         
        /**
         *  Used by the InputFilterList to filter the input list based
         *  on controller.
         *
         *  @param input the input
         *  @return <code>true</code> to pass the input,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.control.Input input) {
            return (input.getControllerId().equals(this.controllerId));
        }
    }
}
