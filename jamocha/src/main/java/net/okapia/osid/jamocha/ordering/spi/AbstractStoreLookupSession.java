//
// AbstractStoreLookupSession.java
//
//    A starter implementation framework for providing a Store
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Store
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getStores(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractStoreLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.ordering.StoreLookupSession {

    private boolean pedantic = false;
    private org.osid.ordering.Store store = new net.okapia.osid.jamocha.nil.ordering.store.UnknownStore();
    


    /**
     *  Tests if this user can perform <code>Store</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStores() {
        return (true);
    }


    /**
     *  A complete view of the <code>Store</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStoreView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Store</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStoreView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Store</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Store</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Store</code> and
     *  retained for compatibility.
     *
     *  @param  storeId <code>Id</code> of the
     *          <code>Store</code>
     *  @return the store
     *  @throws org.osid.NotFoundException <code>storeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>storeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ordering.StoreList stores = getStores()) {
            while (stores.hasNext()) {
                org.osid.ordering.Store store = stores.getNextStore();
                if (store.getId().equals(storeId)) {
                    return (store);
                }
            }
        } 

        throw new org.osid.NotFoundException(storeId + " not found");
    }


    /**
     *  Gets a <code>StoreList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stores specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Stores</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getStores()</code>.
     *
     *  @param  storeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Store</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>storeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByIds(org.osid.id.IdList storeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.ordering.Store> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = storeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getStore(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("store " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.ordering.store.LinkedStoreList(ret));
    }


    /**
     *  Gets a <code>StoreList</code> corresponding to the given
     *  store genus <code>Type</code> which does not include
     *  stores of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getStores()</code>.
     *
     *  @param  storeGenusType a store genus type 
     *  @return the returned <code>Store</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>storeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByGenusType(org.osid.type.Type storeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.store.StoreGenusFilterList(getStores(), storeGenusType));
    }


    /**
     *  Gets a <code>StoreList</code> corresponding to the given
     *  store genus <code>Type</code> and include any additional
     *  stores with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStores()</code>.
     *
     *  @param  storeGenusType a store genus type 
     *  @return the returned <code>Store</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>storeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByParentGenusType(org.osid.type.Type storeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStoresByGenusType(storeGenusType));
    }


    /**
     *  Gets a <code>StoreList</code> containing the given
     *  store record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStores()</code>.
     *
     *  @param  storeRecordType a store record type 
     *  @return the returned <code>Store</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>storeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByRecordType(org.osid.type.Type storeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.store.StoreRecordFilterList(getStores(), storeRecordType));
    }


    /**
     *  Gets a <code>StoreList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known stores or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  stores that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Store</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.ordering.store.StoreProviderFilterList(getStores(), resourceId));
    }


    /**
     *  Gets all <code>Stores</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Stores</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.ordering.StoreList getStores()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the store list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of stores
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.ordering.StoreList filterStoresOnViews(org.osid.ordering.StoreList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
