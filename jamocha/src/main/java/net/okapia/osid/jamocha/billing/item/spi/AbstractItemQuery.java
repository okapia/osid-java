//
// AbstractItemQuery.java
//
//     A template for making an Item Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for items.
 */

public abstract class AbstractItemQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.billing.ItemQuery {

    private final java.util.Collection<org.osid.billing.records.ItemQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the category <code> Id </code> for this query. 
     *
     *  @param  categoryId a category <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> categoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCategoryId(org.osid.id.Id categoryId, boolean match) {
        return;
    }


    /**
     *  Clears the category <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCategoryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CategoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a category query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a category. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a category query 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryQuery getCategoryQuery() {
        throw new org.osid.UnimplementedException("supportsCategoryQuery() is false");
    }


    /**
     *  Clears the category terms. 
     */

    @OSID @Override
    public void clearCategoryTerms() {
        return;
    }


    /**
     *  Sets the account <code> Id </code> for this query to match items that 
     *  have a related financial account. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAccountId(org.osid.id.Id accountId, boolean match) {
        return;
    }


    /**
     *  Clears the account <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAccountIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAccountQuery() is false");
    }


    /**
     *  Matches items that have any account. 
     *
     *  @param  match <code> true </code> to match items with any account, 
     *          <code> false </code> to match items with no account 
     */

    @OSID @Override
    public void matchAnyAccount(boolean match) {
        return;
    }


    /**
     *  Clears the account terms. 
     */

    @OSID @Override
    public void clearAccountTerms() {
        return;
    }


    /**
     *  Sets the product <code> Id </code> for this query to match items that 
     *  have a related product. 
     *
     *  @param  productId a product <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> productId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProductId(org.osid.id.Id productId, boolean match) {
        return;
    }


    /**
     *  Clears the account <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProductIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProductQuery </code> is available. 
     *
     *  @return <code> true </code> if a product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (false);
    }


    /**
     *  Gets the query for a product. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the product query 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuery getProductQuery() {
        throw new org.osid.UnimplementedException("supportsProductQuery() is false");
    }


    /**
     *  Matches items that have any product. 
     *
     *  @param  match <code> true </code> to match items with any product, 
     *          <code> false </code> to match items with no product 
     */

    @OSID @Override
    public void matchAnyProduct(boolean match) {
        return;
    }


    /**
     *  Clears the product terms. 
     */

    @OSID @Override
    public void clearProductTerms() {
        return;
    }


    /**
     *  Matches the amount between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency low, 
                            org.osid.financials.Currency high, boolean match) {
        return;
    }


    /**
     *  Matches items that have any amount set. 
     *
     *  @param  match <code> true </code> to match items with any amount, 
     *          <code> false </code> to match items with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        return;
    }


    /**
     *  Matches items that have debit amounts. 
     *
     *  @param  match <code> true </code> to match items with a debit amount, 
     *          <code> false </code> to match items with a credit amount 
     */

    @OSID @Override
    public void matchDebit(boolean match) {
        return;
    }


    /**
     *  Clears the debit terms. 
     */

    @OSID @Override
    public void clearDebitTerms() {
        return;
    }


    /**
     *  Matches the recurring interval between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRecurringInterval(org.osid.calendaring.Duration low, 
                                       org.osid.calendaring.Duration high, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches items that have any recurring interval. 
     *
     *  @param  match <code> true </code> to match items with any recurring 
     *          interval, <code> false </code> to match items with a one-time 
     *          charge 
     */

    @OSID @Override
    public void matchAnyRecurringInterval(boolean match) {
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearRecurringIntervalTerms() {
        return;
    }


    /**
     *  Sets the entry <code> Id </code> for this query to match items that 
     *  have a related entry. 
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches items that have any entry. 
     *
     *  @param  match <code> true </code> to match items with any entry. 
     *          <code> false </code> to match items with no entry 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match customers 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given item query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an item implementing the requested record.
     *
     *  @param itemRecordType an item record type
     *  @return the item query record
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.ItemQueryRecord getItemQueryRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.ItemQueryRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item query. 
     *
     *  @param itemQueryRecord item query record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addItemQueryRecord(org.osid.billing.records.ItemQueryRecord itemQueryRecord, 
                                          org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);
        nullarg(itemQueryRecord, "item query record");
        this.records.add(itemQueryRecord);        
        return;
    }
}
