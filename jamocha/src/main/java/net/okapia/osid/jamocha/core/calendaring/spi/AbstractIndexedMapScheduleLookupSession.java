//
// AbstractIndexedMapScheduleLookupSession.java
//
//    A simple framework for providing a Schedule lookup service
//    backed by a fixed collection of schedules with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Schedule lookup service backed by a
 *  fixed collection of schedules. The schedules are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some schedules may be compatible
 *  with more types than are indicated through these schedule
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Schedules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapScheduleLookupSession
    extends AbstractMapScheduleLookupSession
    implements org.osid.calendaring.ScheduleLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.Schedule> schedulesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.Schedule>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.Schedule> schedulesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.Schedule>());


    /**
     *  Makes a <code>Schedule</code> available in this session.
     *
     *  @param  schedule a schedule
     *  @throws org.osid.NullArgumentException <code>schedule<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSchedule(org.osid.calendaring.Schedule schedule) {
        super.putSchedule(schedule);

        this.schedulesByGenus.put(schedule.getGenusType(), schedule);
        
        try (org.osid.type.TypeList types = schedule.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.schedulesByRecord.put(types.getNextType(), schedule);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a schedule from this session.
     *
     *  @param scheduleId the <code>Id</code> of the schedule
     *  @throws org.osid.NullArgumentException <code>scheduleId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSchedule(org.osid.id.Id scheduleId) {
        org.osid.calendaring.Schedule schedule;
        try {
            schedule = getSchedule(scheduleId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.schedulesByGenus.remove(schedule.getGenusType());

        try (org.osid.type.TypeList types = schedule.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.schedulesByRecord.remove(types.getNextType(), schedule);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSchedule(scheduleId);
        return;
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  schedule genus <code>Type</code> which does not include
     *  schedules of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known schedules or an error results. Otherwise,
     *  the returned list may contain only those schedules that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  scheduleGenusType a schedule genus type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByGenusType(org.osid.type.Type scheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.schedule.ArrayScheduleList(this.schedulesByGenus.get(scheduleGenusType)));
    }


    /**
     *  Gets a <code>ScheduleList</code> containing the given
     *  schedule record <code>Type</code>. In plenary mode, the
     *  returned list contains all known schedules or an error
     *  results. Otherwise, the returned list may contain only those
     *  schedules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  scheduleRecordType a schedule record type 
     *  @return the returned <code>schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByRecordType(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.schedule.ArrayScheduleList(this.schedulesByRecord.get(scheduleRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.schedulesByGenus.clear();
        this.schedulesByRecord.clear();

        super.close();

        return;
    }
}
