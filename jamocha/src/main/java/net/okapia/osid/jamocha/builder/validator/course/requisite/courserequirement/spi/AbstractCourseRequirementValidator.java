//
// AbstractCourseRequirementValidator.java
//
//     Validates a CourseRequirement.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.requisite.courserequirement.spi;


/**
 *  Validates a CourseRequirement.
 */

public abstract class AbstractCourseRequirementValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRuleValidator {


    /**
     *  Constructs a new <code>AbstractCourseRequirementValidator</code>.
     */

    protected AbstractCourseRequirementValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractCourseRequirementValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractCourseRequirementValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a CourseRequirement.
     *
     *  @param courseRequirement a course requirement to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>courseRequirement</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.requisite.CourseRequirement courseRequirement) {
        super.validate(courseRequirement);

        test(courseRequirement.getAltRequisites(), "getAltRequisites()");
        testNestedObject(courseRequirement, "getCourse");

        testConditionalMethod(courseRequirement, "getTimeframe", courseRequirement.hasTimeframe(), "hasTimeframe()");

        testConditionalMethod(courseRequirement, "getMinimumGradeId", courseRequirement.hasMinimumGrade(), "hasMinimumGrade()");
        testConditionalMethod(courseRequirement, "getMinimumGrade", courseRequirement.hasMinimumGrade(), "hasMinimumGrade()");
        if (courseRequirement.hasMinimumGrade()) {
            testNestedObject(courseRequirement, "getMinimumGrade");
        }

        testConditionalMethod(courseRequirement, "getMinimumScoreSystemId", courseRequirement.hasMinimumScore(), "hasMinimumScore()");
        testConditionalMethod(courseRequirement, "getMinimumScoreSystem", courseRequirement.hasMinimumScore(), "hasMinimumScore()");
        testConditionalMethod(courseRequirement, "getMinimumScore", courseRequirement.hasMinimumScore(), "hasMinimumScore()");
        if (courseRequirement.hasMinimumScore()) {
            testNestedObject(courseRequirement, "getMinimumScoreSystem");
        }

        testConditionalMethod(courseRequirement, "getMinimumEarnedCredits", courseRequirement.hasMinimumEarnedCredits(), "hasMinimumEarnedCredits()");

        return;
    }
}
