//
// RepositoryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.repository.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RepositoryElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the RepositoryElement Id.
     *
     *  @return the repository element Id
     */

    public static org.osid.id.Id getRepositoryEntityId() {
        return (makeEntityId("osid.repository.Repository"));
    }


    /**
     *  Gets the AssetId element Id.
     *
     *  @return the AssetId element Id
     */

    public static org.osid.id.Id getAssetId() {
        return (makeQueryElementId("osid.repository.repository.AssetId"));
    }


    /**
     *  Gets the Asset element Id.
     *
     *  @return the Asset element Id
     */

    public static org.osid.id.Id getAsset() {
        return (makeQueryElementId("osid.repository.repository.Asset"));
    }


    /**
     *  Gets the CompositionId element Id.
     *
     *  @return the CompositionId element Id
     */

    public static org.osid.id.Id getCompositionId() {
        return (makeQueryElementId("osid.repository.repository.CompositionId"));
    }


    /**
     *  Gets the Composition element Id.
     *
     *  @return the Composition element Id
     */

    public static org.osid.id.Id getComposition() {
        return (makeQueryElementId("osid.repository.repository.Composition"));
    }


    /**
     *  Gets the AncestorRepositoryId element Id.
     *
     *  @return the AncestorRepositoryId element Id
     */

    public static org.osid.id.Id getAncestorRepositoryId() {
        return (makeQueryElementId("osid.repository.repository.AncestorRepositoryId"));
    }


    /**
     *  Gets the AncestorRepository element Id.
     *
     *  @return the AncestorRepository element Id
     */

    public static org.osid.id.Id getAncestorRepository() {
        return (makeQueryElementId("osid.repository.repository.AncestorRepository"));
    }


    /**
     *  Gets the DescendantRepositoryId element Id.
     *
     *  @return the DescendantRepositoryId element Id
     */

    public static org.osid.id.Id getDescendantRepositoryId() {
        return (makeQueryElementId("osid.repository.repository.DescendantRepositoryId"));
    }


    /**
     *  Gets the DescendantRepository element Id.
     *
     *  @return the DescendantRepository element Id
     */

    public static org.osid.id.Id getDescendantRepository() {
        return (makeQueryElementId("osid.repository.repository.DescendantRepository"));
    }
}
