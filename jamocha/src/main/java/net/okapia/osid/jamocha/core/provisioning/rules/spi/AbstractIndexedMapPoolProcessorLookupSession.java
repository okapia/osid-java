//
// AbstractIndexedMapPoolProcessorLookupSession.java
//
//    A simple framework for providing a PoolProcessor lookup service
//    backed by a fixed collection of pool processors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a PoolProcessor lookup service backed by a
 *  fixed collection of pool processors. The pool processors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some pool processors may be compatible
 *  with more types than are indicated through these pool processor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PoolProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPoolProcessorLookupSession
    extends AbstractMapPoolProcessorLookupSession
    implements org.osid.provisioning.rules.PoolProcessorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.PoolProcessor> poolProcessorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.PoolProcessor>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.PoolProcessor> poolProcessorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.PoolProcessor>());


    /**
     *  Makes a <code>PoolProcessor</code> available in this session.
     *
     *  @param  poolProcessor a pool processor
     *  @throws org.osid.NullArgumentException <code>poolProcessor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPoolProcessor(org.osid.provisioning.rules.PoolProcessor poolProcessor) {
        super.putPoolProcessor(poolProcessor);

        this.poolProcessorsByGenus.put(poolProcessor.getGenusType(), poolProcessor);
        
        try (org.osid.type.TypeList types = poolProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolProcessorsByRecord.put(types.getNextType(), poolProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a pool processor from this session.
     *
     *  @param poolProcessorId the <code>Id</code> of the pool processor
     *  @throws org.osid.NullArgumentException <code>poolProcessorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePoolProcessor(org.osid.id.Id poolProcessorId) {
        org.osid.provisioning.rules.PoolProcessor poolProcessor;
        try {
            poolProcessor = getPoolProcessor(poolProcessorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.poolProcessorsByGenus.remove(poolProcessor.getGenusType());

        try (org.osid.type.TypeList types = poolProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolProcessorsByRecord.remove(types.getNextType(), poolProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePoolProcessor(poolProcessorId);
        return;
    }


    /**
     *  Gets a <code>PoolProcessorList</code> corresponding to the given
     *  pool processor genus <code>Type</code> which does not include
     *  pool processors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known pool processors or an error results. Otherwise,
     *  the returned list may contain only those pool processors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  poolProcessorGenusType a pool processor genus type 
     *  @return the returned <code>PoolProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorList getPoolProcessorsByGenusType(org.osid.type.Type poolProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolprocessor.ArrayPoolProcessorList(this.poolProcessorsByGenus.get(poolProcessorGenusType)));
    }


    /**
     *  Gets a <code>PoolProcessorList</code> containing the given
     *  pool processor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known pool processors or an error
     *  results. Otherwise, the returned list may contain only those
     *  pool processors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  poolProcessorRecordType a pool processor record type 
     *  @return the returned <code>poolProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorList getPoolProcessorsByRecordType(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolprocessor.ArrayPoolProcessorList(this.poolProcessorsByRecord.get(poolProcessorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.poolProcessorsByGenus.clear();
        this.poolProcessorsByRecord.clear();

        super.close();

        return;
    }
}
