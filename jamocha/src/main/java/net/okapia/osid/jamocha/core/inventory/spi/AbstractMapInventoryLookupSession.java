//
// AbstractMapInventoryLookupSession
//
//    A simple framework for providing an Inventory lookup service
//    backed by a fixed collection of inventories.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Inventory lookup service backed by a
 *  fixed collection of inventories. The inventories are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Inventories</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapInventoryLookupSession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractInventoryLookupSession
    implements org.osid.inventory.InventoryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inventory.Inventory> inventories = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inventory.Inventory>());


    /**
     *  Makes an <code>Inventory</code> available in this session.
     *
     *  @param  inventory an inventory
     *  @throws org.osid.NullArgumentException <code>inventory<code>
     *          is <code>null</code>
     */

    protected void putInventory(org.osid.inventory.Inventory inventory) {
        this.inventories.put(inventory.getId(), inventory);
        return;
    }


    /**
     *  Makes an array of inventories available in this session.
     *
     *  @param  inventories an array of inventories
     *  @throws org.osid.NullArgumentException <code>inventories<code>
     *          is <code>null</code>
     */

    protected void putInventories(org.osid.inventory.Inventory[] inventories) {
        putInventories(java.util.Arrays.asList(inventories));
        return;
    }


    /**
     *  Makes a collection of inventories available in this session.
     *
     *  @param  inventories a collection of inventories
     *  @throws org.osid.NullArgumentException <code>inventories<code>
     *          is <code>null</code>
     */

    protected void putInventories(java.util.Collection<? extends org.osid.inventory.Inventory> inventories) {
        for (org.osid.inventory.Inventory inventory : inventories) {
            this.inventories.put(inventory.getId(), inventory);
        }

        return;
    }


    /**
     *  Removes an Inventory from this session.
     *
     *  @param  inventoryId the <code>Id</code> of the inventory
     *  @throws org.osid.NullArgumentException <code>inventoryId<code> is
     *          <code>null</code>
     */

    protected void removeInventory(org.osid.id.Id inventoryId) {
        this.inventories.remove(inventoryId);
        return;
    }


    /**
     *  Gets the <code>Inventory</code> specified by its <code>Id</code>.
     *
     *  @param  inventoryId <code>Id</code> of the <code>Inventory</code>
     *  @return the inventory
     *  @throws org.osid.NotFoundException <code>inventoryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>inventoryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Inventory getInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inventory.Inventory inventory = this.inventories.get(inventoryId);
        if (inventory == null) {
            throw new org.osid.NotFoundException("inventory not found: " + inventoryId);
        }

        return (inventory);
    }


    /**
     *  Gets all <code>Inventories</code>. In plenary mode, the returned
     *  list contains all known inventories or an error
     *  results. Otherwise, the returned list may contain only those
     *  inventories that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Inventories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.inventory.ArrayInventoryList(this.inventories.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inventories.clear();
        super.close();
        return;
    }
}
