//
// AbstractQueryHierarchyLookupSession.java
//
//    An inline adapter that maps a HierarchyLookupSession to
//    a HierarchyQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a HierarchyLookupSession to
 *  a HierarchyQuerySession.
 */

public abstract class AbstractQueryHierarchyLookupSession
    extends net.okapia.osid.jamocha.hierarchy.spi.AbstractHierarchyLookupSession
    implements org.osid.hierarchy.HierarchyLookupSession {

    private final org.osid.hierarchy.HierarchyQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryHierarchyLookupSession.
     *
     *  @param querySession the underlying hierarchy query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryHierarchyLookupSession(org.osid.hierarchy.HierarchyQuerySession querySession) {
        nullarg(querySession, "hierarchy query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Hierarchy</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupHierarchies() {
        return (this.session.canSearchHierarchies());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Hierarchy</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Hierarchy</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Hierarchy</code> and
     *  retained for compatibility.
     *
     *  @param  hierarchyId <code>Id</code> of the
     *          <code>Hierarchy</code>
     *  @return the hierarchy
     *  @throws org.osid.NotFoundException <code>hierarchyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>hierarchyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Hierarchy getHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hierarchy.HierarchyQuery query = getQuery();
        query.matchId(hierarchyId, true);
        org.osid.hierarchy.HierarchyList hierarchies = this.session.getHierarchiesByQuery(query);
        if (hierarchies.hasNext()) {
            return (hierarchies.getNextHierarchy());
        } 
        
        throw new org.osid.NotFoundException(hierarchyId + " not found");
    }


    /**
     *  Gets a <code>HierarchyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  hierarchies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Hierarchies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  hierarchyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Hierarchy</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByIds(org.osid.id.IdList hierarchyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hierarchy.HierarchyQuery query = getQuery();

        try (org.osid.id.IdList ids = hierarchyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getHierarchiesByQuery(query));
    }


    /**
     *  Gets a <code>HierarchyList</code> corresponding to the given
     *  hierarchy genus <code>Type</code> which does not include
     *  hierarchies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  hierarchies or an error results. Otherwise, the returned list
     *  may contain only those hierarchies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  hierarchyGenusType a hierarchy genus type 
     *  @return the returned <code>Hierarchy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByGenusType(org.osid.type.Type hierarchyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hierarchy.HierarchyQuery query = getQuery();
        query.matchGenusType(hierarchyGenusType, true);
        return (this.session.getHierarchiesByQuery(query));
    }


    /**
     *  Gets a <code>HierarchyList</code> corresponding to the given
     *  hierarchy genus <code>Type</code> and include any additional
     *  hierarchies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  hierarchies or an error results. Otherwise, the returned list
     *  may contain only those hierarchies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  hierarchyGenusType a hierarchy genus type 
     *  @return the returned <code>Hierarchy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByParentGenusType(org.osid.type.Type hierarchyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hierarchy.HierarchyQuery query = getQuery();
        query.matchParentGenusType(hierarchyGenusType, true);
        return (this.session.getHierarchiesByQuery(query));
    }


    /**
     *  Gets a <code>HierarchyList</code> containing the given
     *  hierarchy record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  hierarchies or an error results. Otherwise, the returned list
     *  may contain only those hierarchies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  hierarchyRecordType a hierarchy record type 
     *  @return the returned <code>Hierarchy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByRecordType(org.osid.type.Type hierarchyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hierarchy.HierarchyQuery query = getQuery();
        query.matchRecordType(hierarchyRecordType, true);
        return (this.session.getHierarchiesByQuery(query));
    }


    /**
     *  Gets a <code>HierarchyList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known hierarchies or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  hierarchies that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Hierarchy</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hierarchy.HierarchyQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getHierarchiesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Hierarchies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  hierarchies or an error results. Otherwise, the returned list
     *  may contain only those hierarchies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Hierarchies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hierarchy.HierarchyQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getHierarchiesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.hierarchy.HierarchyQuery getQuery() {
        org.osid.hierarchy.HierarchyQuery query = this.session.getHierarchyQuery();
        return (query);
    }
}
