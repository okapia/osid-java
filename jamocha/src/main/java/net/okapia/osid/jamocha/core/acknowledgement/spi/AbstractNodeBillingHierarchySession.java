//
// AbstractNodeBillingHierarchySession.java
//
//     Defines a Billing hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a billing hierarchy session for delivering a hierarchy
 *  of billings using the BillingNode interface.
 */

public abstract class AbstractNodeBillingHierarchySession
    extends net.okapia.osid.jamocha.acknowledgement.spi.AbstractBillingHierarchySession
    implements org.osid.acknowledgement.BillingHierarchySession {

    private java.util.Collection<org.osid.acknowledgement.BillingNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root billing <code> Ids </code> in this hierarchy.
     *
     *  @return the root billing <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootBillingIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.acknowledgement.billingnode.BillingNodeToIdList(this.roots));
    }


    /**
     *  Gets the root billings in the billing hierarchy. A node
     *  with no parents is an orphan. While all billing <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root billings 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getRootBillings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.acknowledgement.billingnode.BillingNodeToBillingList(new net.okapia.osid.jamocha.acknowledgement.billingnode.ArrayBillingNodeList(this.roots)));
    }


    /**
     *  Adds a root billing node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootBilling(org.osid.acknowledgement.BillingNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root billing nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootBillings(java.util.Collection<org.osid.acknowledgement.BillingNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root billing node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootBilling(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.acknowledgement.BillingNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Billing </code> has any parents. 
     *
     *  @param  billingId a billing <code> Id </code> 
     *  @return <code> true </code> if the billing has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentBillings(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getBillingNode(billingId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  billing.
     *
     *  @param  id an <code> Id </code> 
     *  @param  billingId the <code> Id </code> of a billing 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> billingId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> billingId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfBilling(org.osid.id.Id id, org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.acknowledgement.BillingNodeList parents = getBillingNode(billingId).getParentBillingNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextBillingNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given billing. 
     *
     *  @param  billingId a billing <code> Id </code> 
     *  @return the parent <code> Ids </code> of the billing 
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentBillingIds(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.acknowledgement.billing.BillingToIdList(getParentBillings(billingId)));
    }


    /**
     *  Gets the parents of the given billing. 
     *
     *  @param  billingId the <code> Id </code> to query 
     *  @return the parents of the billing 
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getParentBillings(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.acknowledgement.billingnode.BillingNodeToBillingList(getBillingNode(billingId).getParentBillingNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  billing.
     *
     *  @param  id an <code> Id </code> 
     *  @param  billingId the Id of a billing 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> billingId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfBilling(org.osid.id.Id id, org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBilling(id, billingId)) {
            return (true);
        }

        try (org.osid.acknowledgement.BillingList parents = getParentBillings(billingId)) {
            while (parents.hasNext()) {
                if (isAncestorOfBilling(id, parents.getNextBilling().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a billing has any children. 
     *
     *  @param  billingId a billing <code> Id </code> 
     *  @return <code> true </code> if the <code> billingId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildBillings(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBillingNode(billingId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  billing.
     *
     *  @param  id an <code> Id </code> 
     *  @param billingId the <code> Id </code> of a 
     *         billing
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> billingId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> billingId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfBilling(org.osid.id.Id id, org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfBilling(billingId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  billing.
     *
     *  @param  billingId the <code> Id </code> to query 
     *  @return the children of the billing 
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildBillingIds(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.acknowledgement.billing.BillingToIdList(getChildBillings(billingId)));
    }


    /**
     *  Gets the children of the given billing. 
     *
     *  @param  billingId the <code> Id </code> to query 
     *  @return the children of the billing 
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getChildBillings(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.acknowledgement.billingnode.BillingNodeToBillingList(getBillingNode(billingId).getChildBillingNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  billing.
     *
     *  @param  id an <code> Id </code> 
     *  @param billingId the <code> Id </code> of a 
     *         billing
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> billingId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfBilling(org.osid.id.Id id, org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBilling(billingId, id)) {
            return (true);
        }

        try (org.osid.acknowledgement.BillingList children = getChildBillings(billingId)) {
            while (children.hasNext()) {
                if (isDescendantOfBilling(id, children.getNextBilling().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  billing.
     *
     *  @param  billingId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified billing node 
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getBillingNodeIds(org.osid.id.Id billingId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.acknowledgement.billingnode.BillingNodeToNode(getBillingNode(billingId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given billing.
     *
     *  @param  billingId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified billing node 
     *  @throws org.osid.NotFoundException <code> billingId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> billingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingNode getBillingNodes(org.osid.id.Id billingId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBillingNode(billingId));
    }


    /**
     *  Closes this <code>BillingHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a billing node.
     *
     *  @param billingId the id of the billing node
     *  @throws org.osid.NotFoundException <code>billingId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>billingId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.acknowledgement.BillingNode getBillingNode(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(billingId, "billing Id");
        for (org.osid.acknowledgement.BillingNode billing : this.roots) {
            if (billing.getId().equals(billingId)) {
                return (billing);
            }

            org.osid.acknowledgement.BillingNode r = findBilling(billing, billingId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(billingId + " is not found");
    }


    protected org.osid.acknowledgement.BillingNode findBilling(org.osid.acknowledgement.BillingNode node, 
                                                               org.osid.id.Id billingId)
        throws org.osid.OperationFailedException {

        try (org.osid.acknowledgement.BillingNodeList children = node.getChildBillingNodes()) {
            while (children.hasNext()) {
                org.osid.acknowledgement.BillingNode billing = children.getNextBillingNode();
                if (billing.getId().equals(billingId)) {
                    return (billing);
                }
                
                billing = findBilling(billing, billingId);
                if (billing != null) {
                    return (billing);
                }
            }
        }

        return (null);
    }
}
