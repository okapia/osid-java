//
// AbstractIndexedMapSpeedZoneEnablerLookupSession.java
//
//    A simple framework for providing a SpeedZoneEnabler lookup service
//    backed by a fixed collection of speed zone enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a SpeedZoneEnabler lookup service backed by a
 *  fixed collection of speed zone enablers. The speed zone enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some speed zone enablers may be compatible
 *  with more types than are indicated through these speed zone enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SpeedZoneEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSpeedZoneEnablerLookupSession
    extends AbstractMapSpeedZoneEnablerLookupSession
    implements org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.rules.SpeedZoneEnabler> speedZoneEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.rules.SpeedZoneEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.rules.SpeedZoneEnabler> speedZoneEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.rules.SpeedZoneEnabler>());


    /**
     *  Makes a <code>SpeedZoneEnabler</code> available in this session.
     *
     *  @param  speedZoneEnabler a speed zone enabler
     *  @throws org.osid.NullArgumentException <code>speedZoneEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSpeedZoneEnabler(org.osid.mapping.path.rules.SpeedZoneEnabler speedZoneEnabler) {
        super.putSpeedZoneEnabler(speedZoneEnabler);

        this.speedZoneEnablersByGenus.put(speedZoneEnabler.getGenusType(), speedZoneEnabler);
        
        try (org.osid.type.TypeList types = speedZoneEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.speedZoneEnablersByRecord.put(types.getNextType(), speedZoneEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a speed zone enabler from this session.
     *
     *  @param speedZoneEnablerId the <code>Id</code> of the speed zone enabler
     *  @throws org.osid.NullArgumentException <code>speedZoneEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSpeedZoneEnabler(org.osid.id.Id speedZoneEnablerId) {
        org.osid.mapping.path.rules.SpeedZoneEnabler speedZoneEnabler;
        try {
            speedZoneEnabler = getSpeedZoneEnabler(speedZoneEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.speedZoneEnablersByGenus.remove(speedZoneEnabler.getGenusType());

        try (org.osid.type.TypeList types = speedZoneEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.speedZoneEnablersByRecord.remove(types.getNextType(), speedZoneEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSpeedZoneEnabler(speedZoneEnablerId);
        return;
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> corresponding to the given
     *  speed zone enabler genus <code>Type</code> which does not include
     *  speed zone enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known speed zone enablers or an error results. Otherwise,
     *  the returned list may contain only those speed zone enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  speedZoneEnablerGenusType a speed zone enabler genus type 
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByGenusType(org.osid.type.Type speedZoneEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.rules.speedzoneenabler.ArraySpeedZoneEnablerList(this.speedZoneEnablersByGenus.get(speedZoneEnablerGenusType)));
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> containing the given
     *  speed zone enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known speed zone enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  speed zone enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  speedZoneEnablerRecordType a speed zone enabler record type 
     *  @return the returned <code>speedZoneEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByRecordType(org.osid.type.Type speedZoneEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.rules.speedzoneenabler.ArraySpeedZoneEnablerList(this.speedZoneEnablersByRecord.get(speedZoneEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.speedZoneEnablersByGenus.clear();
        this.speedZoneEnablersByRecord.clear();

        super.close();

        return;
    }
}
