//
// AbstractFederatingCandidateLookupSession.java
//
//     An abstract federating adapter for a CandidateLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CandidateLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCandidateLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.voting.CandidateLookupSession>
    implements org.osid.voting.CandidateLookupSession {

    private boolean parallel = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();


    /**
     *  Constructs a new <code>AbstractFederatingCandidateLookupSession</code>.
     */

    protected AbstractFederatingCandidateLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.voting.CandidateLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>Candidate</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCandidates() {
        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            if (session.canLookupCandidates()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Candidate</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCandidateView() {
        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            session.useComparativeCandidateView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Candidate</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCandidateView() {
        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            session.usePlenaryCandidateView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include candidates in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            session.useFederatedPollsView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            session.useIsolatedPollsView();
        }

        return;
    }


    /**
     *  Only candidates whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveCandidateView() {
        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            session.useEffectiveCandidateView();
        }

        return;
    }


    /**
     *  All candidates of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCandidateView() {
        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            session.useAnyEffectiveCandidateView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Candidate</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Candidate</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Candidate</code> and
     *  retained for compatibility.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateId <code>Id</code> of the
     *          <code>Candidate</code>
     *  @return the candidate
     *  @throws org.osid.NotFoundException <code>candidateId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>candidateId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Candidate getCandidate(org.osid.id.Id candidateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            try {
                return (session.getCandidate(candidateId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(candidateId + " not found");
    }


    /**
     *  Gets a <code>CandidateList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  candidates specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Candidates</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param candidateIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Candidate</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>candidateIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByIds(org.osid.id.IdList candidateIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.voting.candidate.MutableCandidateList ret = new net.okapia.osid.jamocha.voting.candidate.MutableCandidateList();

        try (org.osid.id.IdList ids = candidateIds) {
            while (ids.hasNext()) {
                ret.addCandidate(getCandidate(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CandidateList</code> corresponding to the given
     *  candidate genus <code>Type</code> which does not include
     *  candidates of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateGenusType a candidate genus type 
     *  @return the returned <code>Candidate</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>candidateGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByGenusType(org.osid.type.Type candidateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesByGenusType(candidateGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CandidateList</code> corresponding to the given
     *  candidate genus <code>Type</code> and include any additional
     *  candidates with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateGenusType a candidate genus type 
     *  @return the returned <code>Candidate</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>candidateGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByParentGenusType(org.osid.type.Type candidateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesByParentGenusType(candidateGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CandidateList</code> containing the given
     *  candidate record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateRecordType a candidate record type 
     *  @return the returned <code>Candidate</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>candidateRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByRecordType(org.osid.type.Type candidateRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesByRecordType(candidateRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CandidateList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *  
     *  In active mode, candidates are returned that are currently
     *  active. In any status mode, active and inactive candidates
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Candidate</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of candidates corresponding to a race
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.CandidateList getCandidatesForRace(org.osid.id.Id raceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesForRace(raceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of candidates corresponding to a race
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRaceOnDate(org.osid.id.Id raceId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesForRaceOnDate(raceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of candidates corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.CandidateList getCandidatesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of candidates corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForResourceOnDate(org.osid.id.Id resourceId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesForResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of candidates corresponding to race and resource
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRaceAndResource(org.osid.id.Id raceId,
                                                                         org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesForRaceAndResource(raceId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of candidates corresponding to race and resource
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRaceAndResourceOnDate(org.osid.id.Id raceId,
                                                                               org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidatesForRaceAndResourceOnDate(raceId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Candidates</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Candidates</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidates()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList ret = getCandidateList();

        for (org.osid.voting.CandidateLookupSession session : getSessions()) {
            ret.addCandidateList(session.getCandidates());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.voting.candidate.FederatingCandidateList getCandidateList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.candidate.ParallelCandidateList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.candidate.CompositeCandidateList());
        }
    }
}
