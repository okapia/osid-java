//
// AbstractCanonicalUnitSearchOdrer.java
//
//     Defines a CanonicalUnitSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.canonicalunit.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code CanonicalUnitSearchOrder}.
 */

public abstract class AbstractCanonicalUnitSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectSearchOrder
    implements org.osid.offering.CanonicalUnitSearchOrder {

    private final java.util.Collection<org.osid.offering.records.CanonicalUnitSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the title. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the code. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCode(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  canonicalUnitRecordType a canonical unit record type 
     *  @return {@code true} if the canonicalUnitRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type canonicalUnitRecordType) {
        for (org.osid.offering.records.CanonicalUnitSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  canonicalUnitRecordType the canonical unit record type 
     *  @return the canonical unit search order record
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(canonicalUnitRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.offering.records.CanonicalUnitSearchOrderRecord getCanonicalUnitSearchOrderRecord(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CanonicalUnitSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this canonical unit. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param canonicalUnitRecord the canonical unit search odrer record
     *  @param canonicalUnitRecordType canonical unit record type
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitRecord} or
     *          {@code canonicalUnitRecordTypecanonicalUnit} is
     *          {@code null}
     */
            
    protected void addCanonicalUnitRecord(org.osid.offering.records.CanonicalUnitSearchOrderRecord canonicalUnitSearchOrderRecord, 
                                     org.osid.type.Type canonicalUnitRecordType) {

        addRecordType(canonicalUnitRecordType);
        this.records.add(canonicalUnitSearchOrderRecord);
        
        return;
    }
}
