//
// AbstractScheduleQuery.java
//
//     A template for making a Schedule Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.schedule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for schedules.
 */

public abstract class AbstractScheduleQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.calendaring.ScheduleQuery {

    private final java.util.Collection<org.osid.calendaring.records.ScheduleQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the schedule <code> Id </code> for this query for matching nested 
     *  schedule slots. 
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleSlotId(org.osid.id.Id scheduleSlotId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the schedule slot <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleSlotIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ScheduleSlotQuery </code> is available for querying 
     *  sechedule slots. 
     *
     *  @return <code> true </code> if a schedule slot query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a schedul slot. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the schedule slot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuery getScheduleSlotQuery() {
        throw new org.osid.UnimplementedException("supportsScheduleSlotQuery() is false");
    }


    /**
     *  Matches a schedule that has any schedule slot assigned. 
     *
     *  @param  match <code> true </code> to match schedule with any schedule 
     *          slots, <code> false </code> to match schedules with no 
     *          schedule slots 
     */

    @OSID @Override
    public void matchAnyScheduleSlot(boolean match) {
        return;
    }


    /**
     *  Clears the schedule slot terms. 
     */

    @OSID @Override
    public void clearScheduleSlotTerms() {
        return;
    }


    /**
     *  Sets the time period <code> Id </code> for this query. 
     *
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        return;
    }


    /**
     *  Clears the time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Matches a schedule that has any time period assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any time 
     *          periods, <code> false </code> to match schedules with no time 
     *          periods 
     */

    @OSID @Override
    public void matchAnyTimePeriod(boolean match) {
        return;
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        return;
    }


    /**
     *  Matches the schedule start time between the given range inclusive. 
     *
     *  @param  low low time range 
     *  @param  high high time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleStart(org.osid.calendaring.DateTime low, 
                                   org.osid.calendaring.DateTime high, 
                                   boolean match) {
        return;
    }


    /**
     *  Matches a schedule that has any start time assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any start 
     *          time, <code> false </code> to match schedules with no start 
     *          time 
     */

    @OSID @Override
    public void matchAnyScheduleStart(boolean match) {
        return;
    }


    /**
     *  Clears the schedule start terms. 
     */

    @OSID @Override
    public void clearScheduleStartTerms() {
        return;
    }


    /**
     *  Matches the schedule end time between the given range inclusive. 
     *
     *  @param  low low time range 
     *  @param  high high time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleEnd(org.osid.calendaring.DateTime low, 
                                 org.osid.calendaring.DateTime high, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches a schedule that has any end time assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any end 
     *          time, <code> false </code> to match schedules with no start 
     *          time 
     */

    @OSID @Override
    public void matchAnyScheduleEnd(boolean match) {
        return;
    }


    /**
     *  Clears the schedule end terms. 
     */

    @OSID @Override
    public void clearScheduleEndTerms() {
        return;
    }


    /**
     *  Matches schedules with start and end times between the given range 
     *  inclusive. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchScheduleTime(org.osid.calendaring.DateTime date, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches schedules that has any time assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any time, 
     *          <code> false </code> to match schedules with no time 
     */

    @OSID @Override
    public void matchAnyScheduleTime(boolean match) {
        return;
    }


    /**
     *  Clears the schedule time terms. 
     */

    @OSID @Override
    public void clearScheduleTimeTerms() {
        return;
    }


    /**
     *  Matches schedules with start and end times between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> end </code> or <code> 
     *          start </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleTimeInclusive(org.osid.calendaring.DateTime start, 
                                           org.osid.calendaring.DateTime end, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the schedule time inclusive terms. 
     */

    @OSID @Override
    public void clearScheduleTimeInclusiveTerms() {
        return;
    }


    /**
     *  Matches schedules that have the given limit in the given range 
     *  inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchLimit(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches schedules with any occurrence limit. 
     *
     *  @param  match <code> true </code> to match schedules with any limit, 
     *          to match schedules with no limit 
     */

    @OSID @Override
    public void matchAnyLimit(boolean match) {
        return;
    }


    /**
     *  Clears the limit terms. 
     */

    @OSID @Override
    public void clearLimitTerms() {
        return;
    }


    /**
     *  Matches the location description string. 
     *
     *  @param  location location string 
     *  @param  stringMatchType string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> location </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        return;
    }


    /**
     *  Matches a schedule that has any location description assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any location 
     *          description, <code> false </code> to match schedules with no 
     *          location description 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  locations. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches a schedule that has any location assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any 
     *          location, <code> false </code> to match schedules with no 
     *          location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        return;
    }


    /**
     *  Matches the total duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalDuration(org.osid.calendaring.Duration low, 
                                   org.osid.calendaring.Duration high, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the total duration terms. 
     */

    @OSID @Override
    public void clearTotalDurationTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given schedule query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a schedule implementing the requested record.
     *
     *  @param scheduleRecordType a schedule record type
     *  @return the schedule query record
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleQueryRecord getScheduleQueryRecord(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleQueryRecord record : this.records) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this schedule query. 
     *
     *  @param scheduleQueryRecord schedule query record
     *  @param scheduleRecordType schedule record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addScheduleQueryRecord(org.osid.calendaring.records.ScheduleQueryRecord scheduleQueryRecord, 
                                          org.osid.type.Type scheduleRecordType) {

        addRecordType(scheduleRecordType);
        nullarg(scheduleQueryRecord, "schedule query record");
        this.records.add(scheduleQueryRecord);        
        return;
    }
}
