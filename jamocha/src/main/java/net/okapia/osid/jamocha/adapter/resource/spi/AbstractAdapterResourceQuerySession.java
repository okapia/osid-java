//
// AbstractQueryResourceLookupSession.java
//
//    A ResourceQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ResourceQuerySession adapter.
 */

public abstract class AbstractAdapterResourceQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resource.ResourceQuerySession {

    private final org.osid.resource.ResourceQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterResourceQuerySession.
     *
     *  @param session the underlying resource query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterResourceQuerySession(org.osid.resource.ResourceQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeBin</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeBin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the {@codeBin</code> associated with this 
     *  session.
     *
     *  @return the {@codeBin</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform {@codeResource</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchResources() {
        return (this.session.canSearchResources());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resources in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this bin only.
     */
    
    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    
      
    /**
     *  Gets a resource query. The returned query will not have an
     *  extension query.
     *
     *  @return the resource query 
     */
      
    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        return (this.session.getResourceQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  resourceQuery the resource query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code resourceQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code resourceQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByQuery(org.osid.resource.ResourceQuery resourceQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getResourcesByQuery(resourceQuery));
    }
}
