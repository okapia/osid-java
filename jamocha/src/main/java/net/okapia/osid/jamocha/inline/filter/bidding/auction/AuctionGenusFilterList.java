//
// AuctionGenusFilterList.java
//
//     Implements a filter for genus types.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.bidding.auction;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for genus types.
 */

public final class AuctionGenusFilterList
    extends net.okapia.osid.jamocha.inline.filter.bidding.auction.spi.AbstractAuctionFilterList
    implements org.osid.bidding.AuctionList,
               AuctionFilter {

    private final java.util.Collection<org.osid.type.Type> genusTypes = new java.util.HashSet<>();


    /**
     *  Creates a new <code>AuctionGenusFilterList</code> passing the
     *  given genus type.
     *
     *  @param list an <code>AuctionList</code>
     *  @param type a genus type
     *  @throws org.osid.NullArgumentException <code>list</code> or
     *          <code>type</code> is <code>null</code>
     */

    public AuctionGenusFilterList(org.osid.bidding.AuctionList list, 
                                   org.osid.type.Type type) {
        super(list);

        nullarg(type, "genus type");
        this.genusTypes.add(type);

        return;
    }    


    /**
     *  Creates a new <code>AuctionFilterList</code> passing the
     *  given collection of genus types.
     *
     *  @param list an <code>AuctionList</code>
     *  @param types a collection of genus types
     *  @throws org.osid.NullArgumentException <code>list</code> or
     *          <code>types</code> is <code>null</code>
     */
    
    public AuctionGenusFilterList(org.osid.bidding.AuctionList list, 
                                   java.util.Collection<org.osid.type.Type> types) {
        super(list);

        nullarg(types, "genus types");
        this.genusTypes.addAll(types);

        return;
    }    

    
    /**
     *  Filters Auctions.
     *
     *  @param auction the auction to filter
     *  @return <code>true</code> if the auction passes the filter,
     *          <code>false</code> if the auction should be filtered
     */

    @Override
    public boolean pass(org.osid.bidding.Auction auction) {
        if (this.genusTypes.contains(auction.getGenusType())) {
            return (true);
        } else {
            return (false);
        }
    }
}
