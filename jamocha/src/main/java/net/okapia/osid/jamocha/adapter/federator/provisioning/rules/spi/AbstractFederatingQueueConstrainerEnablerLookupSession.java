//
// AbstractFederatingQueueConstrainerEnablerLookupSession.java
//
//     An abstract federating adapter for a QueueConstrainerEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  QueueConstrainerEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingQueueConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession>
    implements org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Constructs a new
     *  <code>AbstractFederatingQueueConstrainerEnablerLookupSession</code>.
     */

    protected AbstractFederatingQueueConstrainerEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform
     *  <code>QueueConstrainerEnabler</code> lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQueueConstrainerEnablers() {
        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            if (session.canLookupQueueConstrainerEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>QueueConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueConstrainerEnablerView() {
        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            session.useComparativeQueueConstrainerEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>QueueConstrainerEnabler</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueConstrainerEnablerView() {
        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            session.usePlenaryQueueConstrainerEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue constrainer enablers in distributors
     *  which are children of this distributor in the distributor
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            session.useFederatedDistributorView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            session.useIsolatedDistributorView();
        }

        return;
    }


    /**
     *  Only active queue constrainer enablers are returned by methods
     *  in this session.
     */
     
    @OSID @Override
    public void useActiveQueueConstrainerEnablerView() {
        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            session.useActiveQueueConstrainerEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive queue constrainer enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueConstrainerEnablerView() {
        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            session.useAnyStatusQueueConstrainerEnablerView();
        }

        return;
    }
     

    /**
     *  Gets the <code>QueueConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>QueueConstrainerEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>QueueConstrainerEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerId <code>Id</code> of the
     *          <code>QueueConstrainerEnabler</code>
     *  @return the queue constrainer enabler
     *  @throws org.osid.NotFoundException <code>queueConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnabler getQueueConstrainerEnabler(org.osid.id.Id queueConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            try {
                return (session.getQueueConstrainerEnabler(queueConstrainerEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(queueConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  queueConstrainerEnablers specified in the <code>Id</code>
     *  list, in the order of the list, including duplicates, or an
     *  error results if an <code>Id</code> in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible
     *  <code>QueueConstrainerEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByIds(org.osid.id.IdList queueConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.rules.queueconstrainerenabler.MutableQueueConstrainerEnablerList ret = new net.okapia.osid.jamocha.provisioning.rules.queueconstrainerenabler.MutableQueueConstrainerEnablerList();

        try (org.osid.id.IdList ids = queueConstrainerEnablerIds) {
            while (ids.hasNext()) {
                ret.addQueueConstrainerEnabler(getQueueConstrainerEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> corresponding
     *  to the given queue constrainer enabler genus <code>Type</code>
     *  which does not include queue constrainer enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those queue constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerGenusType a queueConstrainerEnabler genus type 
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByGenusType(org.osid.type.Type queueConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.FederatingQueueConstrainerEnablerList ret = getQueueConstrainerEnablerList();

        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            ret.addQueueConstrainerEnablerList(session.getQueueConstrainerEnablersByGenusType(queueConstrainerEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> corresponding
     *  to the given queue constrainer enabler genus <code>Type</code>
     *  and include any additional queue constrainer enablers with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those queue constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerGenusType a queueConstrainerEnabler genus type 
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByParentGenusType(org.osid.type.Type queueConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.FederatingQueueConstrainerEnablerList ret = getQueueConstrainerEnablerList();

        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            ret.addQueueConstrainerEnablerList(session.getQueueConstrainerEnablersByParentGenusType(queueConstrainerEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> containing the
     *  given queue constrainer enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known queue
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those queue constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerRecordType a queueConstrainerEnabler record type 
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByRecordType(org.osid.type.Type queueConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.FederatingQueueConstrainerEnablerList ret = getQueueConstrainerEnablerList();

        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            ret.addQueueConstrainerEnablerList(session.getQueueConstrainerEnablersByRecordType(queueConstrainerEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  queue constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>QueueConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.FederatingQueueConstrainerEnablerList ret = getQueueConstrainerEnablerList();

        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            ret.addQueueConstrainerEnablerList(session.getQueueConstrainerEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>QueueConstrainerEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.FederatingQueueConstrainerEnablerList ret = getQueueConstrainerEnablerList();

        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            ret.addQueueConstrainerEnablerList(session.getQueueConstrainerEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>QueueConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those queue constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @return a list of <code>QueueConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.FederatingQueueConstrainerEnablerList ret = getQueueConstrainerEnablerList();

        for (org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession session : getSessions()) {
            ret.addQueueConstrainerEnablerList(session.getQueueConstrainerEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.FederatingQueueConstrainerEnablerList getQueueConstrainerEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.ParallelQueueConstrainerEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.CompositeQueueConstrainerEnablerList());
        }
    }
}
