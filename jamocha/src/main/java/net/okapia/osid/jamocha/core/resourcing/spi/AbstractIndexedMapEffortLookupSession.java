//
// AbstractIndexedMapEffortLookupSession.java
//
//    A simple framework for providing an Effort lookup service
//    backed by a fixed collection of efforts with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Effort lookup service backed by a
 *  fixed collection of efforts. The efforts are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some efforts may be compatible
 *  with more types than are indicated through these effort
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Efforts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapEffortLookupSession
    extends AbstractMapEffortLookupSession
    implements org.osid.resourcing.EffortLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Effort> effortsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Effort>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Effort> effortsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Effort>());


    /**
     *  Makes an <code>Effort</code> available in this session.
     *
     *  @param  effort an effort
     *  @throws org.osid.NullArgumentException <code>effort<code> is
     *          <code>null</code>
     */

    @Override
    protected void putEffort(org.osid.resourcing.Effort effort) {
        super.putEffort(effort);

        this.effortsByGenus.put(effort.getGenusType(), effort);
        
        try (org.osid.type.TypeList types = effort.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.effortsByRecord.put(types.getNextType(), effort);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an effort from this session.
     *
     *  @param effortId the <code>Id</code> of the effort
     *  @throws org.osid.NullArgumentException <code>effortId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeEffort(org.osid.id.Id effortId) {
        org.osid.resourcing.Effort effort;
        try {
            effort = getEffort(effortId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.effortsByGenus.remove(effort.getGenusType());

        try (org.osid.type.TypeList types = effort.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.effortsByRecord.remove(types.getNextType(), effort);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeEffort(effortId);
        return;
    }


    /**
     *  Gets an <code>EffortList</code> corresponding to the given
     *  effort genus <code>Type</code> which does not include
     *  efforts of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known efforts or an error results. Otherwise,
     *  the returned list may contain only those efforts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  effortGenusType an effort genus type 
     *  @return the returned <code>Effort</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>effortGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByGenusType(org.osid.type.Type effortGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.effort.ArrayEffortList(this.effortsByGenus.get(effortGenusType)));
    }


    /**
     *  Gets an <code>EffortList</code> containing the given
     *  effort record <code>Type</code>. In plenary mode, the
     *  returned list contains all known efforts or an error
     *  results. Otherwise, the returned list may contain only those
     *  efforts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  effortRecordType an effort record type 
     *  @return the returned <code>effort</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByRecordType(org.osid.type.Type effortRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.effort.ArrayEffortList(this.effortsByRecord.get(effortRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.effortsByGenus.clear();
        this.effortsByRecord.clear();

        super.close();

        return;
    }
}
