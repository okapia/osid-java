//
// AbstractUnknownProgramEntry.java
//
//     Defines an unknown ProgramEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.course.chronicle.programentry.spi;


/**
 *  Defines an unknown <code>ProgramEntry</code>.
 */

public abstract class AbstractUnknownProgramEntry
    extends net.okapia.osid.jamocha.course.chronicle.programentry.spi.AbstractProgramEntry
    implements org.osid.course.chronicle.ProgramEntry {

    protected static final String OBJECT = "osid.course.chronicle.ProgramEntry";


    /**
     *  Constructs a new <code>AbstractUnknownProgramEntry</code>.
     */

    public AbstractUnknownProgramEntry() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setStudent(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setProgram(new net.okapia.osid.jamocha.nil.course.program.program.UnknownProgram());
        setAdmissionDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setCreditScale(new net.okapia.osid.jamocha.nil.grading.gradesystem.UnknownGradeSystem());
        setCreditsEarned(new java.math.BigDecimal(0));

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownProgramEntry</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownProgramEntry(boolean optional) {
        this();

        setTerm(new net.okapia.osid.jamocha.nil.course.term.UnknownTerm());
        setGPAScale(new net.okapia.osid.jamocha.nil.grading.gradesystem.UnknownGradeSystem());
        setGPA(new java.math.BigDecimal(0));
        addEnrollment(new net.okapia.osid.jamocha.nil.course.program.enrollment.UnknownEnrollment());

        return;
    }
}
