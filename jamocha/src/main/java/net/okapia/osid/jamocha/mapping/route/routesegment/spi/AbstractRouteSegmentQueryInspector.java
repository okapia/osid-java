//
// AbstractRouteSegmentQueryInspector.java
//
//     A template for making a RouteSegmentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.routesegment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for route segments.
 */

public abstract class AbstractRouteSegmentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.mapping.route.RouteSegmentQueryInspector {

    private final java.util.Collection<org.osid.mapping.route.records.RouteSegmentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the starting instruction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getStartingInstructionsTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the ending instruction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getEndingInstructionsTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the distance query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DistanceRangeTerm[] getDistanceTerms() {
        return (new org.osid.search.terms.DistanceRangeTerm[0]);
    }


    /**
     *  Gets the ETA query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getETATerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the route <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRouteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the route query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQueryInspector[] getRouteTerms() {
        return (new org.osid.mapping.route.RouteQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given route segment query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a route segment implementing the requested record.
     *
     *  @param routeSegmentRecordType a route segment record type
     *  @return the route segment query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeSegmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSegmentQueryInspectorRecord getRouteSegmentQueryInspectorRecord(org.osid.type.Type routeSegmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteSegmentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeSegmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this route segment query. 
     *
     *  @param routeSegmentQueryInspectorRecord route segment query inspector
     *         record
     *  @param routeSegmentRecordType routeSegment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRouteSegmentQueryInspectorRecord(org.osid.mapping.route.records.RouteSegmentQueryInspectorRecord routeSegmentQueryInspectorRecord, 
                                                   org.osid.type.Type routeSegmentRecordType) {

        addRecordType(routeSegmentRecordType);
        nullarg(routeSegmentRecordType, "route segment record type");
        this.records.add(routeSegmentQueryInspectorRecord);        
        return;
    }
}
