//
// AbstractRoutingCreditLookupSession
//
//     A routing federating adapter for a CreditLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A federating adapter for a CreditLookupSession. Sessions are
 *  added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 *
 *  The routing for Ids and Types are manually set by subclasses of
 *  this session that add child sessions for federation.
 *
 *  Routing for Ids is based on the existince of an Id embedded in the
 *  authority of a credit Id. The embedded Id is specified when
 *  adding child sessions. Optionally, a list of types for genus and
 *  record may also be mapped to a child session. A service Id for
 *  routing must be unique across all federated sessions while a Type
 *  may map to more than one session.
 *
 *  Lookup methods consult the mapping tables for Ids and Types, and
 *  will fallback to searching all sessions if the credit is not
 *  found and fallback is true. In the case of record and genus Type
 *  lookups, a fallback will search all child sessions regardless if
 *  any are found.
 */

public abstract class AbstractRoutingCreditLookupSession
    extends AbstractFederatingCreditLookupSession
    implements org.osid.acknowledgement.CreditLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.acknowledgement.CreditLookupSession> sessionsById = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.acknowledgement.CreditLookupSession>());
    private final MultiMap<org.osid.type.Type, org.osid.acknowledgement.CreditLookupSession> sessionsByType = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.acknowledgement.CreditLookupSession>());
    private final boolean idFallback;
    private final boolean typeFallback;

    
    /**
     *  Constructs a new
     *  <code>AbstractRoutingCreditLookupSession</code> with
     *  fallbacks for both Types and Ids.
     */

    protected AbstractRoutingCreditLookupSession() {
        this.idFallback   = true;
        this.typeFallback = true;
        selectAll();
        return;
    }


    /**
     *  Constructs a new
     *  <code>AbstractRoutingCreditLookupSession</code>.
     *
     *  @param idFallback <code>true</code> to fall back to searching
     *         all sessions if an Id is not in routing table
     *  @param typeFallback <code>true</code> to fall back to
     *         searching all sessions if a Type is not in routing
     *         table
     */

    protected AbstractRoutingCreditLookupSession(boolean idFallback, boolean typeFallback) {
        this.idFallback   = idFallback;
        this.typeFallback = typeFallback;
        selectAll();
        return;
    }


    /**
     *  Maps a session to a service Id. Use <code>addSession()</code>
     *  to add a session to this federation.
     *
     *  @param session a session to map
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void mapSession(org.osid.acknowledgement.CreditLookupSession session, org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.put(serviceId, session);
        return;
    }


    /**
     *  Maps a session to a record or genus Type. Use
     *  <code>addSession()</code> to add a session to this federation.
     *
     *  @param session a session to add
     *  @param type
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>type</code> is <code>null</code>
     */

    protected void mapSession(org.osid.acknowledgement.CreditLookupSession session, org.osid.type.Type type) {
        nullarg(type, "type");
        this.sessionsByType.put(type, session);
        return;
    }


    /**
     *  Unmaps a session from a serviceId.
     *
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException 
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.remove(serviceId);
        return;
    }


    /**
     *  Unmaps a session from both the Id and Type indices.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.acknowledgement.CreditLookupSession session) {
        nullarg(session, "session");

        for (org.osid.id.Id id : this.sessionsById.keySet()) {
            if (session.equals(this.sessionsById.get(id))) {
                this.sessionsById.remove(id);
            }
        }

        this.sessionsByType.removeValue(session);

        return;
    }


    /**
     *  Removes a session from this federation.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void removeSession(org.osid.acknowledgement.CreditLookupSession session) {
        unmapSession(session);
        super.removeSession(session);
        return;
    }


    /**
     *  Gets the <code>Credit</code> specified by its <code>Id</code>. 
     *
     *  @param  creditId <code>Id</code> of the
     *          <code>Credit</code>
     *  @return the credit
     *  @throws org.osid.NotFoundException <code>creditId</code> not 
     *          found in any session
     *  @throws org.osid.NullArgumentException <code>creditId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Credit getCredit(org.osid.id.Id creditId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.acknowledgement.CreditLookupSession session = this.sessionsById.get(creditId.getAuthority());
        if (session != null) {
            return (session.getCredit(creditId));
        }
        
        if (this.idFallback) {
            return (super.getCredit(creditId));
        }

        throw new org.osid.NotFoundException(creditId + " not found");
    }


    /**
     *  Gets a <code>CreditList</code> corresponding to the given
     *  credit genus <code>Type</code> which does not include
     *  credits of types derived from the specified
     *  <code>Type</code>.
     *  
     *
     *  @param  creditGenusType a credit genus type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByGenusType(creditGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }


        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByGenusType(creditGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CreditList</code> corresponding to the given
     *  credit genus <code>Type</code> and include any additional
     *  credits with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  @param  creditGenusType a credit genus type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByParentGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByParentGenusType(creditGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }


        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByParentGenusType(creditGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CreditList</code> containing the given
     *  credit record <code>Type</code>.
     *
     *  @param  creditRecordType a credit record type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByRecordType(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditRecordType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByRecordType(creditRecordType));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }

        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByRecordType(creditRecordType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits of the given genus type and effective entire 
     *  given date range inclusive but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known credits or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  credits that are accessible through this session. 
     *  
     *  In effective mode, credits are returned that are currently effective 
     *  in addition to being effective in the given date range. In any 
     *  effective mode, effective credits and those currently expired are 
     *  returned. 
     *
     *  @param  creditGenusType a credit genus <code> Type </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> CreditList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> creditGenusType, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeOnDate(org.osid.type.Type creditGenusType, 
                                                                           org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByGenusTypeOnDate(creditGenusType, from, to));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }


        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByGenusTypeOnDate(creditGenusType, from, to));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits by a genus type for a resource.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.NullArgumentException <code> creditGenusType </code>
     *          or <code> resourceId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResource(org.osid.id.Id resourceId,
                                                                                org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByGenusTypeForResource(resourceId, creditGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }


        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByGenusTypeForResource(resourceId, creditGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits by genus type for a resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          creditGenusType, from, </code> or <code> to </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.type.Type creditGenusType,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByGenusTypeForResourceOnDate(resourceId, creditGenusType, from, to));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }


        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByGenusTypeForResourceOnDate(resourceId, creditGenusType, from, to));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits by a genus type for a reference.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  referenceId a reference <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.NullArgumentException <code> creditGenusType </code>
     *          or <code> referenceId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForReference(org.osid.id.Id referenceId,
                                                                                org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByGenusTypeForReference(referenceId, creditGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }


        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByGenusTypeForReference(referenceId, creditGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits by genus type for a reference and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  referenceId a reference <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> referenceId,
     *          creditGenusType, from, </code> or <code> to </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForReferenceOnDate(org.osid.id.Id referenceId,
                                                                                      org.osid.type.Type creditGenusType,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByGenusTypeForReferenceOnDate(referenceId, creditGenusType, from, to));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }


        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByGenusTypeForReferenceOnDate(referenceId, creditGenusType, from, to));
        }
        
        ret.noMore();
        return (ret);
    }



    /**
     *  Gets a <code>CreditList</code> of the given genus type for
     *  the given resource and reference.
     *
     *  In plenary mode, the returned list contains all of the credits
     *  corresponding to the given resource and reference, including
     *  duplicates, or an error results if a credit is inaccessible.
     *  Otherwise, inaccessible <code> Credits </code> may be omitted
     *  from the list.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  referenceId a reference <code>Id</code>
     *  @param  creditGenusType a credit genus <code>Type</code>
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>referenceId</code> or <code>creditGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceAndReference(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id referenceId,
                                                                                            org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByGenusTypeForResourceAndReference(resourceId, referenceId, creditGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }


        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByGenusTypeForResourceAndReference(resourceId, referenceId, creditGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CreditList</code> of the given genus type
     *  corresponding to the given resource and reference and
     *  effective entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all of the credits
     *  corresponding to the given peer, including duplicates, or an error
     *  results if a credit is inaccessible. Otherwise, inaccessible <code>
     *  Credits </code> may be omitted from the list.
     *
     *  In effective mode, credits are returned that are currently effective
     *  in addition to being effective in the given date range. In any
     *  effective mode, effective credits and those currently expired are
     *  returned.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  referenceId a reference <code>Id</code>
     *  @param  creditGenusType a credit genus <code>Type</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>referenceId</code>,
     *          <code>creditGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id referenceId,
                                                                                                  org.osid.type.Type creditGenusType,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();
        java.util.Collection<org.osid.acknowledgement.CreditLookupSession> sessions = this.sessionsByType.get(creditGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCreditsByGenusTypeForResourceAndReferenceOnDate(resourceId, referenceId, creditGenusType, from, to));
            } else {
                return (new net.okapia.osid.jamocha.nil.acknowledgement.credit.EmptyCreditList());
            }
        }


        for (org.osid.acknowledgement.CreditLookupSession session : sessions) {
            ret.addCreditList(session.getCreditsByGenusTypeForResourceAndReferenceOnDate(resourceId, referenceId, creditGenusType, from, to));
        }
        
        ret.noMore();
        return (ret);
    }
}
