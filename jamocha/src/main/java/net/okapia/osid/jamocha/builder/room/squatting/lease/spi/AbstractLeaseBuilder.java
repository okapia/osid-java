//
// AbstractLease.java
//
//     Defines a Lease builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.squatting.lease.spi;


/**
 *  Defines a <code>Lease</code> builder.
 */

public abstract class AbstractLeaseBuilder<T extends AbstractLeaseBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.room.squatting.lease.LeaseMiter lease;


    /**
     *  Constructs a new <code>AbstractLeaseBuilder</code>.
     *
     *  @param lease the lease to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractLeaseBuilder(net.okapia.osid.jamocha.builder.room.squatting.lease.LeaseMiter lease) {
        super(lease);
        this.lease = lease;
        return;
    }


    /**
     *  Builds the lease.
     *
     *  @return the new lease
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.room.squatting.Lease build() {
        (new net.okapia.osid.jamocha.builder.validator.room.squatting.lease.LeaseValidator(getValidations())).validate(this.lease);
        return (new net.okapia.osid.jamocha.builder.room.squatting.lease.ImmutableLease(this.lease));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the lease miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.room.squatting.lease.LeaseMiter getMiter() {
        return (this.lease);
    }


    /**
     *  Sets the room.
     *
     *  @param room a room
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>room</code> is
     *          <code>null</code>
     */

    public T room(org.osid.room.Room room) {
        getMiter().setRoom(room);
        return (self());
    }


    /**
     *  Sets the tenant.
     *
     *  @param tenant a tenant
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>tenant</code> is
     *          <code>null</code>
     */

    public T tenant(org.osid.resource.Resource tenant) {
        getMiter().setTenant(tenant);
        return (self());
    }


    /**
     *  Adds a Lease record.
     *
     *  @param record a lease record
     *  @param recordType the type of lease record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.room.squatting.records.LeaseRecord record, org.osid.type.Type recordType) {
        getMiter().addLeaseRecord(record, recordType);
        return (self());
    }
}       


