//
// AbstractInquestSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquest.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractInquestSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inquiry.InquestSearchResults {

    private org.osid.inquiry.InquestList inquests;
    private final org.osid.inquiry.InquestQueryInspector inspector;
    private final java.util.Collection<org.osid.inquiry.records.InquestSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractInquestSearchResults.
     *
     *  @param inquests the result set
     *  @param inquestQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>inquests</code>
     *          or <code>inquestQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractInquestSearchResults(org.osid.inquiry.InquestList inquests,
                                            org.osid.inquiry.InquestQueryInspector inquestQueryInspector) {
        nullarg(inquests, "inquests");
        nullarg(inquestQueryInspector, "inquest query inspectpr");

        this.inquests = inquests;
        this.inspector = inquestQueryInspector;

        return;
    }


    /**
     *  Gets the inquest list resulting from a search.
     *
     *  @return an inquest list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquests() {
        if (this.inquests == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inquiry.InquestList inquests = this.inquests;
        this.inquests = null;
	return (inquests);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inquiry.InquestQueryInspector getInquestQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  inquest search record <code> Type. </code> This method must
     *  be used to retrieve an inquest implementing the requested
     *  record.
     *
     *  @param inquestSearchRecordType an inquest search 
     *         record type 
     *  @return the inquest search
     *  @throws org.osid.NullArgumentException
     *          <code>inquestSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(inquestSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquestSearchResultsRecord getInquestSearchResultsRecord(org.osid.type.Type inquestSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inquiry.records.InquestSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(inquestSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(inquestSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record inquest search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addInquestRecord(org.osid.inquiry.records.InquestSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "inquest record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
