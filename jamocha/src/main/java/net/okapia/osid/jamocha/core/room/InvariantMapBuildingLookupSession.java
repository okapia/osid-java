//
// InvariantMapBuildingLookupSession
//
//    Implements a Building lookup service backed by a fixed collection of
//    buildings.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room;


/**
 *  Implements a Building lookup service backed by a fixed
 *  collection of buildings. The buildings are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapBuildingLookupSession
    extends net.okapia.osid.jamocha.core.room.spi.AbstractMapBuildingLookupSession
    implements org.osid.room.BuildingLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapBuildingLookupSession</code> with no
     *  buildings.
     *  
     *  @param campus the campus
     *  @throws org.osid.NullArgumnetException {@code campus} is
     *          {@code null}
     */

    public InvariantMapBuildingLookupSession(org.osid.room.Campus campus) {
        setCampus(campus);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBuildingLookupSession</code> with a single
     *  building.
     *  
     *  @param campus the campus
     *  @param building a single building
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code building} is <code>null</code>
     */

      public InvariantMapBuildingLookupSession(org.osid.room.Campus campus,
                                               org.osid.room.Building building) {
        this(campus);
        putBuilding(building);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBuildingLookupSession</code> using an array
     *  of buildings.
     *  
     *  @param campus the campus
     *  @param buildings an array of buildings
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code buildings} is <code>null</code>
     */

      public InvariantMapBuildingLookupSession(org.osid.room.Campus campus,
                                               org.osid.room.Building[] buildings) {
        this(campus);
        putBuildings(buildings);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBuildingLookupSession</code> using a
     *  collection of buildings.
     *
     *  @param campus the campus
     *  @param buildings a collection of buildings
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code buildings} is <code>null</code>
     */

      public InvariantMapBuildingLookupSession(org.osid.room.Campus campus,
                                               java.util.Collection<? extends org.osid.room.Building> buildings) {
        this(campus);
        putBuildings(buildings);
        return;
    }
}
