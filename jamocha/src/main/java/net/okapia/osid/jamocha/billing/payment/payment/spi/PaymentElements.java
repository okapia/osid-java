//
// PaymentElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payment.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PaymentElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the PaymentElement Id.
     *
     *  @return the payment element Id
     */

    public static org.osid.id.Id getPaymentEntityId() {
        return (makeEntityId("osid.billing.payment.Payment"));
    }


    /**
     *  Gets the PayerId element Id.
     *
     *  @return the PayerId element Id
     */

    public static org.osid.id.Id getPayerId() {
        return (makeElementId("osid.billing.payment.payment.PayerId"));
    }


    /**
     *  Gets the Payer element Id.
     *
     *  @return the Payer element Id
     */

    public static org.osid.id.Id getPayer() {
        return (makeElementId("osid.billing.payment.payment.Payer"));
    }


    /**
     *  Gets the CustomerId element Id.
     *
     *  @return the CustomerId element Id
     */

    public static org.osid.id.Id getCustomerId() {
        return (makeElementId("osid.billing.payment.payment.CustomerId"));
    }


    /**
     *  Gets the Customer element Id.
     *
     *  @return the Customer element Id
     */

    public static org.osid.id.Id getCustomer() {
        return (makeElementId("osid.billing.payment.payment.Customer"));
    }


    /**
     *  Gets the PeriodId element Id.
     *
     *  @return the PeriodId element Id
     */

    public static org.osid.id.Id getPeriodId() {
        return (makeElementId("osid.billing.payment.payment.PeriodId"));
    }


    /**
     *  Gets the Period element Id.
     *
     *  @return the Period element Id
     */

    public static org.osid.id.Id getPeriod() {
        return (makeElementId("osid.billing.payment.payment.Period"));
    }


    /**
     *  Gets the PaymentDate element Id.
     *
     *  @return the PaymentDate element Id
     */

    public static org.osid.id.Id getPaymentDate() {
        return (makeElementId("osid.billing.payment.payment.PaymentDate"));
    }


    /**
     *  Gets the ProcessDate element Id.
     *
     *  @return the ProcessDate element Id
     */

    public static org.osid.id.Id getProcessDate() {
        return (makeElementId("osid.billing.payment.payment.ProcessDate"));
    }


    /**
     *  Gets the Amount element Id.
     *
     *  @return the Amount element Id
     */

    public static org.osid.id.Id getAmount() {
        return (makeElementId("osid.billing.payment.payment.Amount"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.billing.payment.payment.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.billing.payment.payment.Business"));
    }
}
