//
// AbstractRoute.java
//
//     Defines a Route builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.route.route.spi;


/**
 *  Defines a <code>Route</code> builder.
 */

public abstract class AbstractRouteBuilder<T extends AbstractRouteBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.route.route.RouteMiter route;


    /**
     *  Constructs a new <code>AbstractRouteBuilder</code>.
     *
     *  @param route the route to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRouteBuilder(net.okapia.osid.jamocha.builder.mapping.route.route.RouteMiter route) {
        super(route);
        this.route = route;
        return;
    }


    /**
     *  Builds the route.
     *
     *  @return the new route
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.route.Route build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.route.route.RouteValidator(getValidations())).validate(this.route);
        return (new net.okapia.osid.jamocha.builder.mapping.route.route.ImmutableRoute(this.route));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the route miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.route.route.RouteMiter getMiter() {
        return (this.route);
    }


    /**
     *  Sets the starting location.
     *
     *  @param location a starting location
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public T startingLocation(org.osid.mapping.Location location) {
        getMiter().setStartingLocation(location);
        return (self());
    }


    /**
     *  Sets the ending location.
     *
     *  @param location an ending location
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public T endingLocation(org.osid.mapping.Location location) {
        getMiter().setEndingLocation(location);
        return (self());
    }


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public T distance(org.osid.mapping.Distance distance) {
        getMiter().setDistance(distance);
        return (self());
    }


    /**
     *  Sets the eta.
     *
     *  @param eta astimated timeo f arrival
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>eta</code> is
     *          <code>null</code>
     */

    public T eta(org.osid.calendaring.Duration eta) {
        getMiter().setETA(eta);
        return (self());
    }


    /**
     *  Adds a segment.
     *
     *  @param segment a segment
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>segment</code> is
     *          <code>null</code>
     */

    public T segment(org.osid.mapping.route.RouteSegment segment) {
        getMiter().addSegment(segment);
        return (self());
    }


    /**
     *  Sets all the segments.
     *
     *  @param segments a collection of segments
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>segments</code>
     *          is <code>null</code>
     */

    public T segments(java.util.Collection<org.osid.mapping.route.RouteSegment> segments) {
        getMiter().setSegments(segments);
        return (self());
    }


    /**
     *  Adds a Route record.
     *
     *  @param record a route record
     *  @param recordType the type of route record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.route.records.RouteRecord record, org.osid.type.Type recordType) {
        getMiter().addRouteRecord(record, recordType);
        return (self());
    }
}       


