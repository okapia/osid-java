//
// AbstractRaceProcessor.java
//
//     Defines a RaceProcessor.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceprocessor.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.percentarg;


/**
 *  Defines a <code>RaceProcessor</code>.
 */

public abstract class AbstractRaceProcessor
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessor
    implements org.osid.voting.rules.RaceProcessor {

    private long maximumWinners         = -1;
    private long minimumPercentageToWin = 0;
    private long minimumVotesToWin      = 0;

    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if there is a limit on the number of winners in a race. 
     *
     *  @return <code> true </code> if a limit on the number of winners, 
     *          <code> false </code> if no limit exists 
     */

    @OSID @Override
    public boolean hasMaximumWinners() {
        if (maximumWinners < 0) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the number of maximum winners in a race. 
     *
     *  @return the maximum number of winners permitted 
     *  @throws org.osid.IllegalStateException <code> hasMaximumWinners() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMaximumWinners() {
        if (!hasMaximumWinners()) {
            throw new org.osid.IllegalStateException("hasMaximumWinners() is false");
        }

        return (this.maximumWinners);
    }
 


    /**
     *  Sets the maximum winners.
     *
     *  @param maximumWinners a maximum winners
     *  @throws org.osid.InvalidArgumentException
     *          <code>maximumWinnerx</code> is negative
     */

    protected void setMaximumWinners(long maximumWinners) {
        cardinalarg(maximumWinners, "maximum winners");
        this.maximumWinners = maximumWinners;
        return;
    }


    /**
     *  Gets the minimum percentage (0-100) of total votes to be declared a 
     *  winner in a race. 
     *
     *  @return the minimum percentage 
     */

    @OSID @Override
    public long getMinimumPercentageToWin() {
        return (this.minimumPercentageToWin);
    }


    /**
     *  Sets the minimum percentage to win.
     *
     *  @param percentage a minimum percentage to win
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is negative 
     */

    protected void setMinimumPercentageToWin(long percentage) {
        percentarg(percentage, "minimum percentage to win");
        this.minimumPercentageToWin = percentage;
        return;
    }


    /**
     *  Gets the minimum votesl votes to be declared a winner in a race. 
     *
     *  @return the minimum votes 
     */

    @OSID @Override
    public long getMinimumVotesToWin() {
        return (this.minimumVotesToWin);
    }


    /**
     *  Sets the minimum votes to win.
     *
     *  @param votes a minimum votes to win
     *  @throws org.osid.InvaliAdrgumentException
     *          <code>votes</code> is negative
     */

    protected void setMinimumVotesToWin(long votes) {
        cardinalarg(votes, "minimum votes to win");
        this.minimumVotesToWin = votes;
        return;
    }


    /**
     *  Tests if this raceProcessor supports the given record
     *  <code>Type</code>.
     *
     *  @param  raceProcessorRecordType a race processor record type 
     *  @return <code>true</code> if the raceProcessorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type raceProcessorRecordType) {
        for (org.osid.voting.rules.records.RaceProcessorRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>RaceProcessor</code> record <code>Type</code>.
     *
     *  @param  raceProcessorRecordType the race processor record type 
     *  @return the race processor record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorRecord getRaceProcessorRecord(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race processor. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param raceProcessorRecord the race processor record
     *  @param raceProcessorRecordType race processor record type
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecord</code> or
     *          <code>raceProcessorRecordTyperaceProcessor</code> is
     *          <code>null</code>
     */
            
    protected void addRaceProcessorRecord(org.osid.voting.rules.records.RaceProcessorRecord raceProcessorRecord, 
                                          org.osid.type.Type raceProcessorRecordType) {

        nullarg(raceProcessorRecord, "race processor record");
        addRecordType(raceProcessorRecordType);
        this.records.add(raceProcessorRecord);
        
        return;
    }
}
