//
// AbstractSequenceRuleSearchOdrer.java
//
//     Defines a SequenceRuleSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequencerule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code SequenceRuleSearchOrder}.
 */

public abstract class AbstractSequenceRuleSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.assessment.authoring.SequenceRuleSearchOrder {

    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  part. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessmentPart(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an assessment part search order is available. 
     *
     *  @return <code> true </code> if an assessment part search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartSearchOrder() {
        return (false);
    }


    /**
     *  Gets the assessment order. 
     *
     *  @return the assessment part search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchOrder getAssessmentPartSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentPartSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  part. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNextAssessmentPart(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an assessment part search order is available. 
     *
     *  @return <code> true </code> if an assessment part search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextAssessmentPartSearchOrder() {
        return (false);
    }


    /**
     *  Gets the assessment order. 
     *
     *  @return the assessment part search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNextAssessmentPartSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchOrder getNextAssessmentPartSearchOrder() {
        throw new org.osid.UnimplementedException("supportsNextAssessmentPartSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the minimum 
     *  score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumScore(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the maximum 
     *  score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumScore(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the cumulative. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCumulative(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  sequenceRuleRecordType a sequence rule record type 
     *  @return {@code true} if the sequenceRuleRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type sequenceRuleRecordType) {
        for (org.osid.assessment.authoring.records.SequenceRuleSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  sequenceRuleRecordType the sequence rule record type 
     *  @return the sequence rule search order record
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(sequenceRuleRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleSearchOrderRecord getSequenceRuleSearchOrderRecord(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this sequence rule. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param sequenceRuleRecord the sequence rule search odrer record
     *  @param sequenceRuleRecordType sequence rule record type
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleRecord} or
     *          {@code sequenceRuleRecordTypesequenceRule} is
     *          {@code null}
     */
            
    protected void addSequenceRuleRecord(org.osid.assessment.authoring.records.SequenceRuleSearchOrderRecord sequenceRuleSearchOrderRecord, 
                                     org.osid.type.Type sequenceRuleRecordType) {

        addRecordType(sequenceRuleRecordType);
        this.records.add(sequenceRuleSearchOrderRecord);
        
        return;
    }
}
