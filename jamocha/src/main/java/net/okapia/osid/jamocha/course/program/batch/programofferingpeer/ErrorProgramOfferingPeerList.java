//
// ErrorProgramOfferingPeerList.java
//
//     Implements a ErrorProgramOfferingPeerList. This is used to implement an
//     error condition.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without erroration the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT ERRORED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.batch.programofferingpeer;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This list is to implement an error condition through the OsidList
 *  interface.
 */

public final class ErrorProgramOfferingPeerList
    extends net.okapia.osid.jamocha.course.program.batch.programofferingpeer.spi.AbstractProgramOfferingPeerList
    implements org.osid.course.program.batch.ProgramOfferingPeerList {        

    private final org.osid.OperationFailedException error;


    /**
     *  Creates a new <code>ErrorProgramOfferingPeerList</code>.
     *
     *  @param e an Exception
     *  @throws org.osid.NullArgumentException <code>e</code> is
     *          <code>null</code>
     */

    public ErrorProgramOfferingPeerList(Exception e) {
        nullarg(e, "exception");

        if (e instanceof org.osid.OperationFailedException) {
            this.error = (org.osid.OperationFailedException) e;
        } else {
            this.error = new org.osid.OperationFailedException(e);
        }
        
        return;
    }


    /**
     *  Creates a new <code>ErrorProgramOfferingPeerList</code>.
     *
     *  @param message a text message for the exception
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public ErrorProgramOfferingPeerList(String message) {
        nullarg(message, "message");    
        this.error = new org.osid.OperationFailedException(message);
        return;
    }


    /**
     *  Creates a new <code>ErrorProgramOfferingPeerList</code>.
     *
     *  @param message a text message for the exception
     *  @param e an exception
     *  @throws org.osid.NullArgumentException <code>message</code> or
     *          <code>e</code> is <code>null</code>
     */

    public ErrorProgramOfferingPeerList(String message, Exception e) {
        nullarg(message, "message");
        nullarg(e, "exception");
        
        this.error = new org.osid.OperationFailedException(message, e);
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        return (true);
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *  
     *  This method does not imply asynchronous usage. All OSID
     *  methods may block.
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        return (1);
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        return;
    }


    /**
     *  Gets the next <code>ProgramOfferingPeer</code> in this list. 
     *
     *  @return the next <code>ProgramOfferingPeer</code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code>ProgramOfferingPeer</code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.batch.ProgramOfferingPeer getNextProgramOfferingPeer()
        throws org.osid.OperationFailedException {

        throw this.error;
    }
}
