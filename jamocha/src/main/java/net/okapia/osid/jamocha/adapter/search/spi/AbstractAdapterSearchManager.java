//
// AbstractSearchManager.java
//
//     An adapter for a SearchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.search.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a SearchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterSearchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.search.SearchManager>
    implements org.osid.search.SearchManager {


    /**
     *  Constructs a new {@code AbstractAdapterSearchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterSearchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterSearchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterSearchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any engine federation is exposed. Federation is exposed when 
     *  a specific engine may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  engines appears as a single engine. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if search is supported. 
     *
     *  @return <code> true </code> if search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSearch() {
        return (getAdapteeManager().supportsSearch());
    }


    /**
     *  Tests for the availability of an engine lookup service. 
     *
     *  @return <code> true </code> if engine lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineLookup() {
        return (getAdapteeManager().supportsEngineLookup());
    }


    /**
     *  Tests if querying for engines is available. 
     *
     *  @return <code> true </code> if engine query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (getAdapteeManager().supportsEngineQuery());
    }


    /**
     *  Tests if searching for engines is available. 
     *
     *  @return <code> true </code> if engine search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineSearch() {
        return (getAdapteeManager().supportsEngineSearch());
    }


    /**
     *  Tests for the availability of an engine administrative service for 
     *  creating and deleting engines. 
     *
     *  @return <code> true </code> if engine administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineAdmin() {
        return (getAdapteeManager().supportsEngineAdmin());
    }


    /**
     *  Tests for the availability of an engine notification service. 
     *
     *  @return <code> true </code> if engine notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineNotification() {
        return (getAdapteeManager().supportsEngineNotification());
    }


    /**
     *  Tests for the availability of an engine hierarchy traversal service. 
     *
     *  @return <code> true </code> if engine hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineHierarchy() {
        return (getAdapteeManager().supportsEngineHierarchy());
    }


    /**
     *  Tests for the availability of an engine hierarchy design service. 
     *
     *  @return <code> true </code> if engine hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineHierarchyDesign() {
        return (getAdapteeManager().supportsEngineHierarchyDesign());
    }


    /**
     *  Gets the supported query record types. 
     *
     *  @return a list containing the supported query record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueryRecordTypes() {
        return (getAdapteeManager().getQueryRecordTypes());
    }


    /**
     *  Tests if the given query record type is supported. 
     *
     *  @param  queryRecordType a <code> Type </code> indicating a query 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueryRecordType(org.osid.type.Type queryRecordType) {
        return (getAdapteeManager().supportsQueryRecordType(queryRecordType));
    }


    /**
     *  Gets the supported search record types. 
     *
     *  @return a list containing the supported search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSearchRecordTypes() {
        return (getAdapteeManager().getSearchRecordTypes());
    }


    /**
     *  Tests if the given search record type is supported. 
     *
     *  @param  searchRecordType a <code> Type </code> indicating a search 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> searchRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSearchRecordType(org.osid.type.Type searchRecordType) {
        return (getAdapteeManager().supportsSearchRecordType(searchRecordType));
    }


    /**
     *  Gets the supported <code> Engine </code> record types. 
     *
     *  @return a list containing the supported engine record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEngineRecordTypes() {
        return (getAdapteeManager().getEngineRecordTypes());
    }


    /**
     *  Tests if the given <code> Engine </code> record type is supported. 
     *
     *  @param  engineRecordType a <code> Type </code> indicating an <code> 
     *          Engine </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> engineRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEngineRecordType(org.osid.type.Type engineRecordType) {
        return (getAdapteeManager().supportsEngineRecordType(engineRecordType));
    }


    /**
     *  Gets the supported engine search record types. 
     *
     *  @return a list containing the supported engine search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEngineSearchRecordTypes() {
        return (getAdapteeManager().getEngineSearchRecordTypes());
    }


    /**
     *  Tests if the given engine search record type is supported. 
     *
     *  @param  engineSearchRecordType a <code> Type </code> indicating an 
     *          engine record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> engineSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEngineSearchRecordType(org.osid.type.Type engineSearchRecordType) {
        return (getAdapteeManager().supportsEngineSearchRecordType(engineSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the search 
     *  service. 
     *
     *  @return a <code> SearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchSession getSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the search service 
     *  for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return a <code> SearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchSession getSearchSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSearchSessionForEngine(engineId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine lookup 
     *  service. 
     *
     *  @return an <code> EngineLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineLookupSession getEngineLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine query 
     *  service. 
     *
     *  @return an <code> EngineQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineQuerySession getEngineQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine search 
     *  service. 
     *
     *  @return an <code> EngineSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineSearchSession getEngineSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  administrative service. 
     *
     *  @return an <code> EngineAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineAdminSession getEngineAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  notification service. 
     *
     *  @param  engineReceiver the receiver 
     *  @return an <code> EngineNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> engineReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineNotificationSession getEngineNotificationSession(org.osid.search.EngineReceiver engineReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineNotificationSession(engineReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy service. 
     *
     *  @return an <code> EngineHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineHierarchySession getEngineHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy design service. 
     *
     *  @return an <code> EngineierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.search.EngineHierarchyDesignSession getEngineHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineHierarchyDesignSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
