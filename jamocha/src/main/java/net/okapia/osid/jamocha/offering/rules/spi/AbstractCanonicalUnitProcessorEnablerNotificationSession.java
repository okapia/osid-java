//
// AbstractCanonicalUnitProcessorEnablerNotificationSession.java
//
//     A template for making CanonicalUnitProcessorEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code CanonicalUnitProcessorEnabler}
 *  objects. This session is intended for consumers needing to
 *  synchronize their state with this service without the use of
 *  polling. Notifications are cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code CanonicalUnitProcessorEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for canonical unit processor enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractCanonicalUnitProcessorEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();


    /**
     *  Gets the {@code Catalogue} {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Catalogue Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }

    
    /**
     *  Gets the {@code Catalogue} associated with this session.
     *
     *  @return the {@code Catalogue} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the {@code Catalogue}.
     *
     *  @param catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException {@code catalogue}
     *          is {@code null}
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can register for {@code
     *  CanonicalUnitProcessorEnabler} notifications.  A return of
     *  true does not guarantee successful authorization. A return of
     *  false indicates that it is known all methods in this session
     *  will result in a {@code PERMISSION_DENIED}. This is intended
     *  as a hint to an application that may opt not to offer
     *  notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForCanonicalUnitProcessorEnablerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeCanonicalUnitProcessorEnablerNotification() </code>.
     */

    @OSID @Override
    public void reliableCanonicalUnitProcessorEnablerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableCanonicalUnitProcessorEnablerNotifications() {
        return;
    }


    /**
     *  Acknowledge a canonical unit processor notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeCanonicalUnitProcessorEnablerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit processor enablers in
     *  catalogues which are children of this catalogue in the
     *  catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new canonical unit processor
     *  enablers. {@code
     *  CanonicalUnitProcessorEnablerReceiver.newCanonicalUnitProcessorEnabler()}
     *  is invoked when a new {@code CanonicalUnitProcessorEnabler} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewCanonicalUnitProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated canonical unit processor
     *  enablers. {@code
     *  CanonicalUnitProcessorEnablerReceiver.changedCanonicalUnitProcessorEnabler()}
     *  is invoked when a canonical unit processor enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCanonicalUnitProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated canonical unit
     *  processor enabler. {@code
     *  CanonicalUnitProcessorEnablerReceiver.changedCanonicalUnitProcessorEnabler()}
     *  is invoked when the specified canonical unit processor enabler
     *  is changed.
     *
     *  @param canonicalUnitProcessorEnablerId the {@code Id} of the {@code CanonicalUnitProcessorEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCanonicalUnitProcessorEnabler(org.osid.id.Id canonicalUnitProcessorEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted canonical unit processor
     *  enablers. {@code
     *  CanonicalUnitProcessorEnablerReceiver.deletedCanonicalUnitProcessorEnabler()}
     *  is invoked when a canonical unit processor enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCanonicalUnitProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted canonical unit
     *  processor enabler. {@code
     *  CanonicalUnitProcessorEnablerReceiver.deletedCanonicalUnitProcessorEnabler()}
     *  is invoked when the specified canonical unit processor enabler
     *  is deleted.
     *
     *  @param canonicalUnitProcessorEnablerId the {@code Id} of the
     *          {@code CanonicalUnitProcessorEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCanonicalUnitProcessorEnabler(org.osid.id.Id canonicalUnitProcessorEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
