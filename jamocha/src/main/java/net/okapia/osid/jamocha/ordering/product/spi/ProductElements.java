//
// ProductElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.product.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProductElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ProductElement Id.
     *
     *  @return the product element Id
     */

    public static org.osid.id.Id getProductEntityId() {
        return (makeEntityId("osid.ordering.Product"));
    }


    /**
     *  Gets the Code element Id.
     *
     *  @return the Code element Id
     */

    public static org.osid.id.Id getCode() {
        return (makeElementId("osid.ordering.product.Code"));
    }


    /**
     *  Gets the PriceScheduleIds element Id.
     *
     *  @return the PriceScheduleIds element Id
     */

    public static org.osid.id.Id getPriceScheduleIds() {
        return (makeElementId("osid.ordering.product.PriceScheduleIds"));
    }


    /**
     *  Gets the PriceSchedules element Id.
     *
     *  @return the PriceSchedules element Id
     */

    public static org.osid.id.Id getPriceSchedules() {
        return (makeElementId("osid.ordering.product.PriceSchedules"));
    }


    /**
     *  Gets the Availability element Id.
     *
     *  @return the Availability element Id
     */

    public static org.osid.id.Id getAvailability() {
        return (makeElementId("osid.ordering.product.Availability"));
    }


    /**
     *  Gets the ItemId element Id.
     *
     *  @return the ItemId element Id
     */

    public static org.osid.id.Id getItemId() {
        return (makeQueryElementId("osid.ordering.product.ItemId"));
    }


    /**
     *  Gets the Item element Id.
     *
     *  @return the Item element Id
     */

    public static org.osid.id.Id getItem() {
        return (makeQueryElementId("osid.ordering.product.Item"));
    }


    /**
     *  Gets the StoreId element Id.
     *
     *  @return the StoreId element Id
     */

    public static org.osid.id.Id getStoreId() {
        return (makeQueryElementId("osid.ordering.product.StoreId"));
    }


    /**
     *  Gets the Store element Id.
     *
     *  @return the Store element Id
     */

    public static org.osid.id.Id getStore() {
        return (makeQueryElementId("osid.ordering.product.Store"));
    }
}
