//
// AbstractGradeEntry.java
//
//     Defines a GradeEntry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradeentry.spi;


/**
 *  Defines a <code>GradeEntry</code> builder.
 */

public abstract class AbstractGradeEntryBuilder<T extends AbstractGradeEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.grading.gradeentry.GradeEntryMiter gradeEntry;


    /**
     *  Constructs a new <code>AbstractGradeEntryBuilder</code>.
     *
     *  @param gradeEntry the grade entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractGradeEntryBuilder(net.okapia.osid.jamocha.builder.grading.gradeentry.GradeEntryMiter gradeEntry) {
        super(gradeEntry);
        this.gradeEntry = gradeEntry;
        return;
    }


    /**
     *  Builds the grade entry.
     *
     *  @return the new grade entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.grading.GradeEntry build() {
        (new net.okapia.osid.jamocha.builder.validator.grading.gradeentry.GradeEntryValidator(getValidations())).validate(this.gradeEntry);
        return (new net.okapia.osid.jamocha.builder.grading.gradeentry.ImmutableGradeEntry(this.gradeEntry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the grade entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.grading.gradeentry.GradeEntryMiter getMiter() {
        return (this.gradeEntry);
    }


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    public T gradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        getMiter().setGradebookColumn(gradebookColumn);
        return (self());
    }


    /**
     *  Sets the key resource.
     *
     *  @param resource a key resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T keyResource(org.osid.resource.Resource resource) {
        getMiter().setKeyResource(resource);
        return (self());
    }


    /**
     *  Sets the overridden calculated entry.
     *
     *  @param entry an overridden calculated entry
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    public T overriddenCalculatedEntry(org.osid.grading.GradeEntry entry) {
        getMiter().setOverriddenCalculatedEntry(entry);
        return (self());
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public T grade(org.osid.grading.Grade grade) {
        getMiter().setGrade(grade);
        return (self());
    }


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public T score(java.math.BigDecimal score) {
        getMiter().setScore(score);
        return (self());
    }


    /**
     *  Sets the time graded.
     *
     *  @param graded a time graded
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>graded</code> is
     *          <code>null</code>
     */

    public T timeGraded(org.osid.calendaring.DateTime graded) {
        getMiter().setTimeGraded(graded);
        return (self());
    }


    /**
     *  Sets the grader.
     *
     *  @param grader a grader
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>grader</code> is
     *          <code>null</code>
     */

    public T grader(org.osid.resource.Resource grader) {
        getMiter().setGrader(grader);
        return (self());
    }


    /**
     *  Sets the grading agent.
     *
     *  @param agent a grading agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T gradingAgent(org.osid.authentication.Agent agent) {
        getMiter().setGradingAgent(agent);
        return (self());
    }


    /**
     *  Adds a GradeEntry record.
     *
     *  @param record a grade entry record
     *  @param recordType the type of grade entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.grading.records.GradeEntryRecord record, org.osid.type.Type recordType) {
        getMiter().addGradeEntryRecord(record, recordType);
        return (self());
    }
}       


