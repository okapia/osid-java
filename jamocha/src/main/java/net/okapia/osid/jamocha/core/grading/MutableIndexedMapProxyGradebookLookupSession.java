//
// MutableIndexedMapProxyGradebookLookupSession
//
//    Implements a Gradebook lookup service backed by a collection of
//    gradebooks indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading;


/**
 *  Implements a Gradebook lookup service backed by a collection of
 *  gradebooks. The gradebooks are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some gradebooks may be compatible
 *  with more types than are indicated through these gradebook
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of gradebooks can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyGradebookLookupSession
    extends net.okapia.osid.jamocha.core.grading.spi.AbstractIndexedMapGradebookLookupSession
    implements org.osid.grading.GradebookLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradebookLookupSession} with
     *  no gradebook.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyGradebookLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradebookLookupSession} with
     *  a single gradebook.
     *
     *  @param  gradebook an gradebook
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyGradebookLookupSession(org.osid.grading.Gradebook gradebook, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putGradebook(gradebook);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradebookLookupSession} using
     *  an array of gradebooks.
     *
     *  @param  gradebooks an array of gradebooks
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebooks} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyGradebookLookupSession(org.osid.grading.Gradebook[] gradebooks, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putGradebooks(gradebooks);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyGradebookLookupSession} using
     *  a collection of gradebooks.
     *
     *  @param  gradebooks a collection of gradebooks
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebooks} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyGradebookLookupSession(java.util.Collection<? extends org.osid.grading.Gradebook> gradebooks,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putGradebooks(gradebooks);
        return;
    }

    
    /**
     *  Makes a {@code Gradebook} available in this session.
     *
     *  @param  gradebook a gradebook
     *  @throws org.osid.NullArgumentException {@code gradebook{@code 
     *          is {@code null}
     */

    @Override
    public void putGradebook(org.osid.grading.Gradebook gradebook) {
        super.putGradebook(gradebook);
        return;
    }


    /**
     *  Makes an array of gradebooks available in this session.
     *
     *  @param  gradebooks an array of gradebooks
     *  @throws org.osid.NullArgumentException {@code gradebooks{@code 
     *          is {@code null}
     */

    @Override
    public void putGradebooks(org.osid.grading.Gradebook[] gradebooks) {
        super.putGradebooks(gradebooks);
        return;
    }


    /**
     *  Makes collection of gradebooks available in this session.
     *
     *  @param  gradebooks a collection of gradebooks
     *  @throws org.osid.NullArgumentException {@code gradebook{@code 
     *          is {@code null}
     */

    @Override
    public void putGradebooks(java.util.Collection<? extends org.osid.grading.Gradebook> gradebooks) {
        super.putGradebooks(gradebooks);
        return;
    }


    /**
     *  Removes a Gradebook from this session.
     *
     *  @param gradebookId the {@code Id} of the gradebook
     *  @throws org.osid.NullArgumentException {@code gradebookId{@code  is
     *          {@code null}
     */

    @Override
    public void removeGradebook(org.osid.id.Id gradebookId) {
        super.removeGradebook(gradebookId);
        return;
    }    
}
