//
// AbstractOsidRule.java
//
//     Defines a simple OSID rule to draw from.
//
//
// Tom Coppeto
// Okapia
// 22 January 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OSID rule to draw from. using this
 *  abstract class requires that <code>setId()</code> must be done
 *  before passing this interface to a consumer in order to maintain
 *  compliance with the OSID specification.
 */

public abstract class AbstractOsidRule
    extends AbstractOperableOsidObject
    implements org.osid.OsidRule {

    private org.osid.rules.Rule rule;


    /**
     *  Tests if an explicit rule is available.
     *
     *  @return <code> true </code> if an explicit rule is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasRule() {
        return (this.rule != null);
    }


    /**
     *  Gets the rule <code> Id </code>.
     *
     *  @return the rule <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRule() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRuleId() {
        if (!hasRule()) {
            throw new org.osid.IllegalStateException("hasRule() is false");
        }

        return (this.rule.getId());
    }


    /**
     *  Gets the rule.
     *
     *  @return the rule 
     *  @throws org.osid.IllegalStateException <code> hasRule() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.Rule getRule()
        throws org.osid.OperationFailedException {

        if (!hasRule()) {
            throw new org.osid.IllegalStateException("hasRule() is false");
        }

        return (this.rule);
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */

    protected void setRule(org.osid.rules.Rule rule) {
        nullarg(rule, "rule");
        this.rule = rule;
        return;
    }
}
