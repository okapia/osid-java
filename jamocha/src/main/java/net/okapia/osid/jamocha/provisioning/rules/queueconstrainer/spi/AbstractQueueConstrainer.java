//
// AbstractQueueConstrainer.java
//
//     Defines a QueueConstrainer.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueconstrainer.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>QueueConstrainer</code>.
 */

public abstract class AbstractQueueConstrainer
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainer
    implements org.osid.provisioning.rules.QueueConstrainer {

    private long sizeLimit = -1;
    private boolean requiresProvisions = false;
    private final java.util.Collection<org.osid.provisioning.Pool> requiredProvisionPools = new java.util.LinkedHashSet<>();
    
    private final java.util.Collection<org.osid.provisioning.rules.records.QueueConstrainerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this queue limits the number of requests.
     *
     *  @return <code> true </code> if a queue size limit is
     *          available, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasSizeLimit() {
        if (this.sizeLimit < 0) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the size limit of the queue. 
     *
     *  @return the size limit 
     *  @throws org.osid.IllegalStateException
     *          <code>hasSizeLimit()</code> is <code> false </code>
     */

    @OSID @Override
    public long getSizeLimit() {
        if (!hasSizeLimit()) {
            throw new org.osid.IllegalStateException("hasSizeLimit() is false");
        }

        return (this.sizeLimit);
    }


    /**
     *  Sets the size limit.
     *
     *  @param limit a size limit
     *  @throws org.osid.InvalidArgumentException
     *          <code>sizeLimit</code> is negative
     */

    protected void setSizeLimit(long limit) {
        cardinalarg(limit, "size limit");
        this.sizeLimit = limit;
        return;
    }


    /**
     *  Tests if a provision must exist before entering this queue. 
     *
     *  @return <code> true </code> if a provision is required, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean requiresProvisions() {
        return (this.requiresProvisions);
    }


    /**
     *  Sets the requires provisions.
     *
     *  @param requires <code> true </code> if a provision is
     *         required, <code> false </code> otherwise
     */

    protected void setRequiresProvisions(boolean requires) {
        this.requiresProvisions = requires;
        return;
    }


    /**
     *  Gets the pool <code> Ids </code> for the required provisions.
     *
     *  @return the <code> Ids </code> of the pools 
     *  @throws org.osid.IllegalStateException
     *          <code>requiresProvisions()</code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.id.IdList getRequiredProvisionPoolIds() {
        if (!requiresProvisions()) {
            throw new org.osid.IllegalStateException("requiresProvisions() is false");
        }

        try {
            org.osid.provisioning.PoolList requiredProvisionPools = getRequiredProvisionPools();
            return (new net.okapia.osid.jamocha.adapter.converter.provisioning.pool.PoolToIdList(requiredProvisionPools));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the pools for the required provisions. 
     *
     *  @return the pools 
     *  @throws org.osid.IllegalStateException
     *          <code>requiresProvisions()</code> is <code> false
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getRequiredProvisionPools()
        throws org.osid.OperationFailedException {

        if (!requiresProvisions()) {
            throw new org.osid.IllegalStateException("requiresProvisions() is false");
        }

        return (new net.okapia.osid.jamocha.provisioning.pool.ArrayPoolList(this.requiredProvisionPools));
    }


    /**
     *  Adds a required provision pool.
     *
     *  @param pool a required provision pool
     *  @throws org.osid.NullArgumentException <code>pool</code> is
     *          <code>null</code>
     */

    protected void addRequiredProvisionPool(org.osid.provisioning.Pool pool) {
        nullarg(pool, "pool");
        this.requiredProvisionPools.add(pool);
        return;
    }


    /**
     *  Sets all the required provision pools.
     *
     *  @param pools a collection of required provision pools
     *  @throws org.osid.NullArgumentException <code>pools</code> is
     *          <code>null</code>
     */

    protected void setRequiredProvisionPools(java.util.Collection<org.osid.provisioning.Pool> pools) {
        nullarg(pools, "pools");
        this.requiredProvisionPools.clear();
        this.requiredProvisionPools.addAll(pools);
        return;
    }


    /**
     *  Tests if this queueConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  queueConstrainerRecordType a queue constrainer record type 
     *  @return <code>true</code> if the queueConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type queueConstrainerRecordType) {
        for (org.osid.provisioning.rules.records.QueueConstrainerRecord record : this.records) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>QueueConstrainer</code> record <code>Type</code>.
     *
     *  @param  queueConstrainerRecordType the queue constrainer record type 
     *  @return the queue constrainer record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueConstrainerRecord getQueueConstrainerRecord(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueConstrainerRecord record : this.records) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue constrainer. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param queueConstrainerRecord the queue constrainer record
     *  @param queueConstrainerRecordType queue constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecord</code> or
     *          <code>queueConstrainerRecordTypequeueConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addQueueConstrainerRecord(org.osid.provisioning.rules.records.QueueConstrainerRecord queueConstrainerRecord, 
                                             org.osid.type.Type queueConstrainerRecordType) {

        nullarg(queueConstrainerRecord, "queue constrainer record");
        addRecordType(queueConstrainerRecordType);
        this.records.add(queueConstrainerRecord);
        
        return;
    }
}
