//
// AbstractSupersedingEventQuery.java
//
//     A template for making a SupersedingEvent Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.supersedingevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for superseding events.
 */

public abstract class AbstractSupersedingEventQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.calendaring.SupersedingEventQuery {

    private final java.util.Collection<org.osid.calendaring.records.SupersedingEventQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the event <code> Id </code> for this query for matching attached 
     *  events. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSupersededEventId(org.osid.id.Id eventId, boolean match) {
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupersededEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying 
     *  attached events. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersededEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an attached event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersededEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getSupersededEventQuery() {
        throw new org.osid.UnimplementedException("supportsSupersededEventQuery() is false");
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearSupersededEventTerms() {
        return;
    }


    /**
     *  Sets the superseding event <code> Id </code> for this query. 
     *
     *  @param  supersedingEventId a superseding event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> supersedingEventId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSupersedingEventId(org.osid.id.Id supersedingEventId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the superseding event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupersedingEventIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SupersedingEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a superseding event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a superseding event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the superseding event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getSupersedingEventQuery() {
        throw new org.osid.UnimplementedException("supportsSupersedingEventQuery() is false");
    }


    /**
     *  Clears the superseding event terms. 
     */

    @OSID @Override
    public void clearSupersedingEventTerms() {
        return;
    }


    /**
     *  Matches superseding events that supersede within the given dates 
     *  inclusive. 
     *
     *  @param  from start date 
     *  @param  to end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSupersededDate(org.osid.calendaring.DateTime from, 
                                    org.osid.calendaring.DateTime to, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches a superseding event that has any superseded date. 
     *
     *  @param  match <code> true </code> to match superseding events with any 
     *          superseded date, false to match superseding events with no 
     *          superseded date 
     */

    @OSID @Override
    public void matchAnySupersededDate(boolean match) {
        return;
    }


    /**
     *  Clears the superseded date terms. 
     */

    @OSID @Override
    public void clearSupersededDateTerms() {
        return;
    }


    /**
     *  Matches superseding events that supersede within the denormalized 
     *  event positions inclusive. 
     *
     *  @param  from start position 
     *  @param  to end position 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the absolute value of <code> 
     *          from </code> is greater than <code> to </code> 
     */

    @OSID @Override
    public void matchSupersededEventPosition(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches a superseding event that has any superseded position. 
     *
     *  @param  match <code> true </code> to match superseding events with any 
     *          superseded event position, false to match superseding events 
     *          with no superseded event position 
     */

    @OSID @Override
    public void matchAnySupersededEventPosition(boolean match) {
        return;
    }


    /**
     *  Clears the superseded position terms. 
     */

    @OSID @Override
    public void clearSupersededEventPositionTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given superseding event query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a superseding event implementing the requested record.
     *
     *  @param supersedingEventRecordType a superseding event record type
     *  @return the superseding event query record
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.SupersedingEventQueryRecord getSupersedingEventQueryRecord(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.SupersedingEventQueryRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this superseding event query. 
     *
     *  @param supersedingEventQueryRecord superseding event query record
     *  @param supersedingEventRecordType supersedingEvent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSupersedingEventQueryRecord(org.osid.calendaring.records.SupersedingEventQueryRecord supersedingEventQueryRecord, 
                                          org.osid.type.Type supersedingEventRecordType) {

        addRecordType(supersedingEventRecordType);
        nullarg(supersedingEventQueryRecord, "superseding event query record");
        this.records.add(supersedingEventQueryRecord);        
        return;
    }
}
