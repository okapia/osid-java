//
// EventElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.event.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class EventElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the sequestered element Id.
     *
     *  @return the sequestered element Id
     */

    public static org.osid.id.Id getSequestered() {
        return (net.okapia.osid.jamocha.spi.ContainableElements.getSequestered());
    }


    /**
     *  Gets the EventElement Id.
     *
     *  @return the event element Id
     */

    public static org.osid.id.Id getEventEntityId() {
        return (makeEntityId("osid.calendaring.Event"));
    }


    /**
     *  Gets the LocationDescription element Id.
     *
     *  @return the LocationDescription element Id
     */

    public static org.osid.id.Id getLocationDescription() {
        return (makeElementId("osid.calendaring.event.LocationDescription"));
    }


    /**
     *  Gets the LocationId element Id.
     *
     *  @return the LocationId element Id
     */

    public static org.osid.id.Id getLocationId() {
        return (makeElementId("osid.calendaring.event.LocationId"));
    }


    /**
     *  Gets the Location element Id.
     *
     *  @return the Location element Id
     */

    public static org.osid.id.Id getLocation() {
        return (makeElementId("osid.calendaring.event.Location"));
    }


    /**
     *  Gets the SponsorIds element Id.
     *
     *  @return the SponsorIds element Id
     */

    public static org.osid.id.Id getSponsorIds() {
        return (makeElementId("osid.calendaring.event.SponsorIds"));
    }


    /**
     *  Gets the Sponsors element Id.
     *
     *  @return the Sponsors element Id
     */

    public static org.osid.id.Id getSponsors() {
        return (makeElementId("osid.calendaring.event.Sponsors"));
    }


    /**
     *  Gets the Implicit element Id.
     *
     *  @return the Implicit element Id
     */

    public static org.osid.id.Id getImplicit() {
        return (makeQueryElementId("osid.calendaring.event.Implicit"));
    }


    /**
     *  Gets the Duration element Id.
     *
     *  @return the Duration element Id
     */

    public static org.osid.id.Id getDuration() {
        return (makeQueryElementId("osid.calendaring.event.Duration"));
    }


    /**
     *  Gets the RecurringEventId element Id.
     *
     *  @return the RecurringEventId element Id
     */

    public static org.osid.id.Id getRecurringEventId() {
        return (makeQueryElementId("osid.calendaring.event.RecurringEventId"));
    }


    /**
     *  Gets the RecurringEvent element Id.
     *
     *  @return the RecurringEvent element Id
     */

    public static org.osid.id.Id getRecurringEvent() {
        return (makeQueryElementId("osid.calendaring.event.RecurringEvent"));
    }


    /**
     *  Gets the SupersedingEventId element Id.
     *
     *  @return the SupersedingEventId element Id
     */

    public static org.osid.id.Id getSupersedingEventId() {
        return (makeQueryElementId("osid.calendaring.event.SupersedingEventId"));
    }


    /**
     *  Gets the SupersedingEvent element Id.
     *
     *  @return the SupersedingEvent element Id
     */

    public static org.osid.id.Id getSupersedingEvent() {
        return (makeQueryElementId("osid.calendaring.event.SupersedingEvent"));
    }


    /**
     *  Gets the OffsetEventId element Id.
     *
     *  @return the OffsetEventId element Id
     */

    public static org.osid.id.Id getOffsetEventId() {
        return (makeQueryElementId("osid.calendaring.event.OffsetEventId"));
    }


    /**
     *  Gets the OffsetEvent element Id.
     *
     *  @return the OffsetEvent element Id
     */

    public static org.osid.id.Id getOffsetEvent() {
        return (makeQueryElementId("osid.calendaring.event.OffsetEvent"));
    }


    /**
     *  Gets the Coordinate element Id.
     *
     *  @return the Coordinate element Id
     */

    public static org.osid.id.Id getCoordinate() {
        return (makeQueryElementId("osid.calendaring.event.Coordinate"));
    }


    /**
     *  Gets the SpatialUnit element Id.
     *
     *  @return the SpatialUnit element Id
     */

    public static org.osid.id.Id getSpatialUnit() {
        return (makeQueryElementId("osid.calendaring.event.SpatialUnit"));
    }


    /**
     *  Gets the CommitmentId element Id.
     *
     *  @return the CommitmentId element Id
     */

    public static org.osid.id.Id getCommitmentId() {
        return (makeQueryElementId("osid.calendaring.event.CommitmentId"));
    }


    /**
     *  Gets the Commitment element Id.
     *
     *  @return the Commitment element Id
     */

    public static org.osid.id.Id getCommitment() {
        return (makeQueryElementId("osid.calendaring.event.Commitment"));
    }


    /**
     *  Gets the ContainingEventId element Id.
     *
     *  @return the ContainingEventId element Id
     */

    public static org.osid.id.Id getContainingEventId() {
        return (makeQueryElementId("osid.calendaring.event.ContainingEventId"));
    }


    /**
     *  Gets the ContainingEvent element Id.
     *
     *  @return the ContainingEvent element Id
     */

    public static org.osid.id.Id getContainingEvent() {
        return (makeQueryElementId("osid.calendaring.event.ContainingEvent"));
    }


    /**
     *  Gets the CalendarId element Id.
     *
     *  @return the CalendarId element Id
     */

    public static org.osid.id.Id getCalendarId() {
        return (makeQueryElementId("osid.calendaring.event.CalendarId"));
    }


    /**
     *  Gets the Calendar element Id.
     *
     *  @return the Calendar element Id
     */

    public static org.osid.id.Id getCalendar() {
        return (makeQueryElementId("osid.calendaring.event.Calendar"));
    }
}
