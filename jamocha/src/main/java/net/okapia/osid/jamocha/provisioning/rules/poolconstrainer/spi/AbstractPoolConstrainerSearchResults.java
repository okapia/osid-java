//
// AbstractPoolConstrainerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPoolConstrainerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.rules.PoolConstrainerSearchResults {

    private org.osid.provisioning.rules.PoolConstrainerList poolConstrainers;
    private final org.osid.provisioning.rules.PoolConstrainerQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolConstrainerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPoolConstrainerSearchResults.
     *
     *  @param poolConstrainers the result set
     *  @param poolConstrainerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>poolConstrainers</code>
     *          or <code>poolConstrainerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPoolConstrainerSearchResults(org.osid.provisioning.rules.PoolConstrainerList poolConstrainers,
                                            org.osid.provisioning.rules.PoolConstrainerQueryInspector poolConstrainerQueryInspector) {
        nullarg(poolConstrainers, "pool constrainers");
        nullarg(poolConstrainerQueryInspector, "pool constrainer query inspectpr");

        this.poolConstrainers = poolConstrainers;
        this.inspector = poolConstrainerQueryInspector;

        return;
    }


    /**
     *  Gets the pool constrainer list resulting from a search.
     *
     *  @return a pool constrainer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainers() {
        if (this.poolConstrainers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.rules.PoolConstrainerList poolConstrainers = this.poolConstrainers;
        this.poolConstrainers = null;
	return (poolConstrainers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.rules.PoolConstrainerQueryInspector getPoolConstrainerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  pool constrainer search record <code> Type. </code> This method must
     *  be used to retrieve a poolConstrainer implementing the requested
     *  record.
     *
     *  @param poolConstrainerSearchRecordType a poolConstrainer search 
     *         record type 
     *  @return the pool constrainer search
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(poolConstrainerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolConstrainerSearchResultsRecord getPoolConstrainerSearchResultsRecord(org.osid.type.Type poolConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.rules.records.PoolConstrainerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(poolConstrainerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(poolConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record pool constrainer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPoolConstrainerRecord(org.osid.provisioning.rules.records.PoolConstrainerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "pool constrainer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
