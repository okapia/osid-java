//
// AbstractActivity.java
//
//     Defines an Activity builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.learning.activity.spi;


/**
 *  Defines an <code>Activity</code> builder.
 */

public abstract class AbstractActivityBuilder<T extends AbstractActivityBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.learning.activity.ActivityMiter activity;


    /**
     *  Constructs a new <code>AbstractActivityBuilder</code>.
     *
     *  @param activity the activity to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractActivityBuilder(net.okapia.osid.jamocha.builder.learning.activity.ActivityMiter activity) {
        super(activity);
        this.activity = activity;
        return;
    }


    /**
     *  Builds the activity.
     *
     *  @return the new activity
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.learning.Activity build() {
        (new net.okapia.osid.jamocha.builder.validator.learning.activity.ActivityValidator(getValidations())).validate(this.activity);
        return (new net.okapia.osid.jamocha.builder.learning.activity.ImmutableActivity(this.activity));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the activity miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.learning.activity.ActivityMiter getMiter() {
        return (this.activity);
    }


    /**
     *  Sets the objective.
     *
     *  @param objective an objective
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public T objective(org.osid.learning.Objective objective) {
        getMiter().setObjective(objective);
        return (self());
    }


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    public T asset(org.osid.repository.Asset asset) {
        getMiter().addAsset(asset);
        return (self());
    }


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>assets</code> is
     *          <code>null</code>
     */

    public T assets(java.util.Collection<org.osid.repository.Asset> assets) {
        getMiter().setAssets(assets);
        return (self());
    }


    /**
     *  Adds a course.
     *
     *  @param course a course
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public T course(org.osid.course.Course course) {
        getMiter().addCourse(course);
        return (self());
    }


    /**
     *  Sets all the courses.
     *
     *  @param courses a collection of courses
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>courses</code> is
     *          <code>null</code>
     */

    public T courses(java.util.Collection<org.osid.course.Course> courses) {
        getMiter().setCourses(courses);
        return (self());
    }


    /**
     *  Adds an assessment.
     *
     *  @param assessment an assessment
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    public T assessment(org.osid.assessment.Assessment assessment) {
        getMiter().addAssessment(assessment);
        return (self());
    }


    /**
     *  Sets all the assessments.
     *
     *  @param assessments a collection of assessments
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>assessments</code> is <code>null</code>
     */

    public T assessments(java.util.Collection<org.osid.assessment.Assessment> assessments) {
        getMiter().setAssessments(assessments);
        return (self());
    }


    /**
     *  Adds an Activity record.
     *
     *  @param record an activity record
     *  @param recordType the type of activity record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.learning.records.ActivityRecord record, org.osid.type.Type recordType) {
        getMiter().addActivityRecord(record, recordType);
        return (self());
    }
}       


