//
// AbstractCompositionQueryInspector.java
//
//     A template for making a CompositionQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.composition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for compositions.
 */

public abstract class AbstractCompositionQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectQueryInspector
    implements org.osid.repository.CompositionQueryInspector {

    private final java.util.Collection<org.osid.repository.records.CompositionQueryInspectorRecord> records = new java.util.ArrayList<>();

    private final OsidOperableQueryInspector operableInspector = new OsidOperableQueryInspector();
    private final OsidContainableQueryInspector containableInspector = new OsidContainableQueryInspector();


    /**
     *  Gets the active query terms.
     *
     *  @return the active terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getActiveTerms() {
        return (this.operableInspector.getActiveTerms());
    }


    /**
     *  Gets the enabled query terms.
     *
     *  @return the enabled terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEnabledTerms() {
        return (this.operableInspector.getEnabledTerms());
    }


    /**
     *  Gets the disabled query terms.
     *
     *  @return the disabled terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDisabledTerms() {
        return (this.operableInspector.getDisabledTerms());
    }


    /**
     *  Gets the operational query terms.
     *
     *  @return the operational terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOperationalTerms() {
        return (this.operableInspector.getOperationalTerms());
    }


    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.containableInspector.getSequesteredTerms());
    }

    
    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the containing composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingCompositionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the containing composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getContainingCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the contained composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainedCompositionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the contained composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getContainedCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRepositoryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given composition query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a composition implementing the requested record.
     *
     *  @param compositionRecordType a composition record type
     *  @return the composition query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.CompositionQueryInspectorRecord getCompositionQueryInspectorRecord(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.CompositionQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this composition query. 
     *
     *  @param compositionQueryInspectorRecord composition query inspector
     *         record
     *  @param compositionRecordType composition record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCompositionQueryInspectorRecord(org.osid.repository.records.CompositionQueryInspectorRecord compositionQueryInspectorRecord, 
                                                   org.osid.type.Type compositionRecordType) {

        addRecordType(compositionRecordType);
        nullarg(compositionRecordType, "composition record type");
        this.records.add(compositionQueryInspectorRecord);        
        return;
    }


    protected class OsidOperableQueryInspector
        extends net.okapia.osid.jamocha.spi.AbstractOsidOperableQueryInspector
        implements org.osid.OsidOperableQueryInspector {
    }


    protected class OsidContainableQueryInspector
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQueryInspector
        implements org.osid.OsidContainableQueryInspector {
    }
}
