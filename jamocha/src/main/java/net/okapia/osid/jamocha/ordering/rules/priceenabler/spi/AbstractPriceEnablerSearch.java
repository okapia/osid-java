//
// AbstractPriceEnablerSearch.java
//
//     A template for making a PriceEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.rules.priceenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing price enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPriceEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.ordering.rules.PriceEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.ordering.rules.records.PriceEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.ordering.rules.PriceEnablerSearchOrder priceEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of price enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  priceEnablerIds list of price enablers
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPriceEnablers(org.osid.id.IdList priceEnablerIds) {
        while (priceEnablerIds.hasNext()) {
            try {
                this.ids.add(priceEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPriceEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of price enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPriceEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  priceEnablerSearchOrder price enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>priceEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPriceEnablerResults(org.osid.ordering.rules.PriceEnablerSearchOrder priceEnablerSearchOrder) {
	this.priceEnablerSearchOrder = priceEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.ordering.rules.PriceEnablerSearchOrder getPriceEnablerSearchOrder() {
	return (this.priceEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given price enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a price enabler implementing the requested record.
     *
     *  @param priceEnablerSearchRecordType a price enabler search record
     *         type
     *  @return the price enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.rules.records.PriceEnablerSearchRecord getPriceEnablerSearchRecord(org.osid.type.Type priceEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.ordering.rules.records.PriceEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(priceEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price enabler search. 
     *
     *  @param priceEnablerSearchRecord price enabler search record
     *  @param priceEnablerSearchRecordType priceEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPriceEnablerSearchRecord(org.osid.ordering.rules.records.PriceEnablerSearchRecord priceEnablerSearchRecord, 
                                           org.osid.type.Type priceEnablerSearchRecordType) {

        addRecordType(priceEnablerSearchRecordType);
        this.records.add(priceEnablerSearchRecord);        
        return;
    }
}
