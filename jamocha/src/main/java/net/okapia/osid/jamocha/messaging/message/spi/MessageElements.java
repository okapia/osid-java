//
// MessageElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.message.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class MessageElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the MessageElement Id.
     *
     *  @return the message element Id
     */

    public static org.osid.id.Id getMessageEntityId() {
        return (makeEntityId("osid.messaging.Message"));
    }


    /**
     *  Gets the SubjectLine element Id.
     *
     *  @return the SubjectLine element Id
     */

    public static org.osid.id.Id getSubjectLine() {
        return (makeElementId("osid.messaging.message.SubjectLine"));
    }


    /**
     *  Gets the Text element Id.
     *
     *  @return the Text element Id
     */

    public static org.osid.id.Id getText() {
        return (makeElementId("osid.messaging.message.Text"));
    }


    /**
     *  Gets the SentTime element Id.
     *
     *  @return the SentTime element Id
     */

    public static org.osid.id.Id getSentTime() {
        return (makeElementId("osid.messaging.message.SentTime"));
    }


    /**
     *  Gets the SenderId element Id.
     *
     *  @return the SenderId element Id
     */

    public static org.osid.id.Id getSenderId() {
        return (makeElementId("osid.messaging.message.SenderId"));
    }


    /**
     *  Gets the Sender element Id.
     *
     *  @return the Sender element Id
     */

    public static org.osid.id.Id getSender() {
        return (makeElementId("osid.messaging.message.Sender"));
    }


    /**
     *  Gets the SendingAgentId element Id.
     *
     *  @return the SendingAgentId element Id
     */

    public static org.osid.id.Id getSendingAgentId() {
        return (makeElementId("osid.messaging.message.SendingAgentId"));
    }


    /**
     *  Gets the SendingAgent element Id.
     *
     *  @return the SendingAgent element Id
     */

    public static org.osid.id.Id getSendingAgent() {
        return (makeElementId("osid.messaging.message.SendingAgent"));
    }


    /**
     *  Gets the ReceivedTime element Id.
     *
     *  @return the ReceivedTime element Id
     */

    public static org.osid.id.Id getReceivedTime() {
        return (makeElementId("osid.messaging.message.ReceivedTime"));
    }


    /**
     *  Gets the RecipientIds element Id.
     *
     *  @return the RecipientIds element Id
     */

    public static org.osid.id.Id getRecipientIds() {
        return (makeElementId("osid.messaging.message.RecipientIds"));
    }


    /**
     *  Gets the Recipients element Id.
     *
     *  @return the Recipients element Id
     */

    public static org.osid.id.Id getRecipients() {
        return (makeElementId("osid.messaging.message.Recipients"));
    }


    /**
     *  Gets the ReceiptId element Id.
     *
     *  @return the ReceiptId element Id
     */

    public static org.osid.id.Id getReceiptId() {
        return (makeElementId("osid.messaging.message.ReceiptId"));
    }


    /**
     *  Gets the Receipt element Id.
     *
     *  @return the Receipt element Id
     */

    public static org.osid.id.Id getReceipt() {
        return (makeElementId("osid.messaging.message.Receipt"));
    }


    /**
     *  Gets the Sent element Id.
     *
     *  @return the Sent element Id
     */

    public static org.osid.id.Id getSent() {
        return (makeQueryElementId("osid.messaging.message.Sent"));
    }


    /**
     *  Gets the DeliveryTime element Id.
     *
     *  @return the DeliveryTime element Id
     */

    public static org.osid.id.Id getDeliveryTime() {
        return (makeQueryElementId("osid.messaging.message.DeliveryTime"));
    }


    /**
     *  Gets the MailboxId element Id.
     *
     *  @return the MailboxId element Id
     */

    public static org.osid.id.Id getMailboxId() {
        return (makeQueryElementId("osid.messaging.message.MailboxId"));
    }


    /**
     *  Gets the Mailbox element Id.
     *
     *  @return the Mailbox element Id
     */

    public static org.osid.id.Id getMailbox() {
        return (makeQueryElementId("osid.messaging.message.Mailbox"));
    }
}
