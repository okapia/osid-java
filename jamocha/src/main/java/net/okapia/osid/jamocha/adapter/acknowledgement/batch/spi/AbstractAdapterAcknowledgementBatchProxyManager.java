//
// AbstractAcknowledgementBatchProxyManager.java
//
//     An adapter for a AcknowledgementBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.acknowledgement.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AcknowledgementBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAcknowledgementBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager>
    implements org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterAcknowledgementBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAcknowledgementBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAcknowledgementBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAcknowledgementBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of credits is available. 
     *
     *  @return <code> true </code> if a credit bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditBatchAdmin() {
        return (getAdapteeManager().supportsCreditBatchAdmin());
    }


    /**
     *  Tests if bulk administration of billings is available. 
     *
     *  @return <code> true </code> if a billing bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingBatchAdmin() {
        return (getAdapteeManager().supportsBillingBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk credit 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CreditBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.CreditBatchAdminSession getCreditBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk credit 
     *  administration service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CreditBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.CreditBatchAdminSession getCreditBatchAdminSessionForBilling(org.osid.id.Id billingId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditBatchAdminSessionForBilling(billingId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk billing 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BillingBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.BillingBatchAdminSession getBillingBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
