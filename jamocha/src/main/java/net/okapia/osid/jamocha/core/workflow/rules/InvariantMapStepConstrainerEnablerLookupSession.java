//
// InvariantMapStepConstrainerEnablerLookupSession
//
//    Implements a StepConstrainerEnabler lookup service backed by a fixed collection of
//    stepConstrainerEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules;


/**
 *  Implements a StepConstrainerEnabler lookup service backed by a fixed
 *  collection of step constrainer enablers. The step constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapStepConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.workflow.rules.spi.AbstractMapStepConstrainerEnablerLookupSession
    implements org.osid.workflow.rules.StepConstrainerEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapStepConstrainerEnablerLookupSession</code> with no
     *  step constrainer enablers.
     *  
     *  @param office the office
     *  @throws org.osid.NullArgumnetException {@code office} is
     *          {@code null}
     */

    public InvariantMapStepConstrainerEnablerLookupSession(org.osid.workflow.Office office) {
        setOffice(office);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStepConstrainerEnablerLookupSession</code> with a single
     *  step constrainer enabler.
     *  
     *  @param office the office
     *  @param stepConstrainerEnabler a single step constrainer enabler
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code stepConstrainerEnabler} is <code>null</code>
     */

      public InvariantMapStepConstrainerEnablerLookupSession(org.osid.workflow.Office office,
                                               org.osid.workflow.rules.StepConstrainerEnabler stepConstrainerEnabler) {
        this(office);
        putStepConstrainerEnabler(stepConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStepConstrainerEnablerLookupSession</code> using an array
     *  of step constrainer enablers.
     *  
     *  @param office the office
     *  @param stepConstrainerEnablers an array of step constrainer enablers
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code stepConstrainerEnablers} is <code>null</code>
     */

      public InvariantMapStepConstrainerEnablerLookupSession(org.osid.workflow.Office office,
                                               org.osid.workflow.rules.StepConstrainerEnabler[] stepConstrainerEnablers) {
        this(office);
        putStepConstrainerEnablers(stepConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStepConstrainerEnablerLookupSession</code> using a
     *  collection of step constrainer enablers.
     *
     *  @param office the office
     *  @param stepConstrainerEnablers a collection of step constrainer enablers
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code stepConstrainerEnablers} is <code>null</code>
     */

      public InvariantMapStepConstrainerEnablerLookupSession(org.osid.workflow.Office office,
                                               java.util.Collection<? extends org.osid.workflow.rules.StepConstrainerEnabler> stepConstrainerEnablers) {
        this(office);
        putStepConstrainerEnablers(stepConstrainerEnablers);
        return;
    }
}
