//
// AbstractVaultQueryInspector.java
//
//     A template for making a VaultQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.vault.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for vaults.
 */

public abstract class AbstractVaultQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.authorization.VaultQueryInspector {

    private final java.util.Collection<org.osid.authorization.records.VaultQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the function <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFunctionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the function query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQueryInspector[] getFunctionTerms() {
        return (new org.osid.authorization.FunctionQueryInspector[0]);
    }


    /**
     *  Gets the qualifier <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualifierIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the qualifier query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQueryInspector[] getQualifierTerms() {
        return (new org.osid.authorization.QualifierQueryInspector[0]);
    }


    /**
     *  Gets the authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuthorizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the ancestor vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorVaultIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getAncestorVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }


    /**
     *  Gets the descendant vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantVaultIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getDescendantVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given vault query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a vault implementing the requested record.
     *
     *  @param vaultRecordType a vault record type
     *  @return the vault query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(vaultRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.VaultQueryInspectorRecord getVaultQueryInspectorRecord(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.VaultQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(vaultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(vaultRecordType + " is not supported");
    }


    /**
     *  Adds a record to this vault query. 
     *
     *  @param vaultQueryInspectorRecord vault query inspector
     *         record
     *  @param vaultRecordType vault record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addVaultQueryInspectorRecord(org.osid.authorization.records.VaultQueryInspectorRecord vaultQueryInspectorRecord, 
                                                   org.osid.type.Type vaultRecordType) {

        addRecordType(vaultRecordType);
        nullarg(vaultRecordType, "vault record type");
        this.records.add(vaultQueryInspectorRecord);        
        return;
    }
}
