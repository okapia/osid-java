//
// MutableMapQueueLookupSession
//
//    Implements a Queue lookup service backed by a collection of
//    queues that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Queue lookup service backed by a collection of
 *  queues. The queues are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of queues can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapQueueLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractMapQueueLookupSession
    implements org.osid.provisioning.QueueLookupSession {


    /**
     *  Constructs a new {@code MutableMapQueueLookupSession}
     *  with no queues.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor} is
     *          {@code null}
     */

      public MutableMapQueueLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapQueueLookupSession} with a
     *  single queue.
     *
     *  @param distributor the distributor  
     *  @param queue a queue
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queue} is {@code null}
     */

    public MutableMapQueueLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.Queue queue) {
        this(distributor);
        putQueue(queue);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapQueueLookupSession}
     *  using an array of queues.
     *
     *  @param distributor the distributor
     *  @param queues an array of queues
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queues} is {@code null}
     */

    public MutableMapQueueLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.Queue[] queues) {
        this(distributor);
        putQueues(queues);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapQueueLookupSession}
     *  using a collection of queues.
     *
     *  @param distributor the distributor
     *  @param queues a collection of queues
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queues} is {@code null}
     */

    public MutableMapQueueLookupSession(org.osid.provisioning.Distributor distributor,
                                           java.util.Collection<? extends org.osid.provisioning.Queue> queues) {

        this(distributor);
        putQueues(queues);
        return;
    }

    
    /**
     *  Makes a {@code Queue} available in this session.
     *
     *  @param queue a queue
     *  @throws org.osid.NullArgumentException {@code queue{@code  is
     *          {@code null}
     */

    @Override
    public void putQueue(org.osid.provisioning.Queue queue) {
        super.putQueue(queue);
        return;
    }


    /**
     *  Makes an array of queues available in this session.
     *
     *  @param queues an array of queues
     *  @throws org.osid.NullArgumentException {@code queues{@code 
     *          is {@code null}
     */

    @Override
    public void putQueues(org.osid.provisioning.Queue[] queues) {
        super.putQueues(queues);
        return;
    }


    /**
     *  Makes collection of queues available in this session.
     *
     *  @param queues a collection of queues
     *  @throws org.osid.NullArgumentException {@code queues{@code  is
     *          {@code null}
     */

    @Override
    public void putQueues(java.util.Collection<? extends org.osid.provisioning.Queue> queues) {
        super.putQueues(queues);
        return;
    }


    /**
     *  Removes a Queue from this session.
     *
     *  @param queueId the {@code Id} of the queue
     *  @throws org.osid.NullArgumentException {@code queueId{@code 
     *          is {@code null}
     */

    @Override
    public void removeQueue(org.osid.id.Id queueId) {
        super.removeQueue(queueId);
        return;
    }    
}
