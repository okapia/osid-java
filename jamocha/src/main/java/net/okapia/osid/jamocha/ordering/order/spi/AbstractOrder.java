//
// AbstractOrder.java
//
//     Defines an Order.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.order.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Order</code>.
 */

public abstract class AbstractOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.ordering.Order {

    private org.osid.resource.Resource customer;
    private final java.util.Collection<org.osid.ordering.Item> items = new java.util.LinkedHashSet<>();
    private org.osid.financials.Currency totalCost;
    private org.osid.calendaring.DateTime submitDate;
    private org.osid.resource.Resource submitter;
    private org.osid.authentication.Agent submittingAgent;
    private org.osid.calendaring.DateTime closedDate;
    private org.osid.resource.Resource closer;
    private org.osid.authentication.Agent closingAgent;

    private boolean atomic = false;

    private final java.util.Collection<org.osid.ordering.records.OrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the customer. 
     *
     *  @return the customer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.customer.getId());
    }


    /**
     *  Gets the customer. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCustomer()
        throws org.osid.OperationFailedException {

        return (this.customer);
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    protected void setCustomer(org.osid.resource.Resource customer) {
        nullarg(customer, "customer");
        this.customer = customer;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the items. 
     *
     *  @return the item <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getItemIds() {
        try {
            org.osid.ordering.ItemList items = getItems();
            return (new net.okapia.osid.jamocha.adapter.converter.ordering.item.ItemToIdList(items));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the items. 
     *
     *  @return the items 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.ItemList getItems()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.ordering.item.ArrayItemList(this.items));
    }


    /**
     *  Adds an item.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException
     *          <code>item</code> is <code>null</code>
     */

    protected void addItem(org.osid.ordering.Item item) {
        nullarg(item, "item");
        this.items.add(item);
        return;
    }


    /**
     *  Sets all the items.
     *
     *  @param items a collection of items
     *  @throws org.osid.NullArgumentException
     *          <code>items</code> is <code>null</code>
     */

    protected void setItems(java.util.Collection<org.osid.ordering.Item> items) {
        nullarg(items, "items");
        this.items.clear();
        this.items.addAll(items);
        return;
    }


    /**
     *  Gets the total cost for this order. 
     *
     *  @return the total cost 
     */

    @OSID @Override
    public org.osid.financials.Currency getTotalCost() {
        return (this.totalCost);
    }


    /**
     *  Sets the total cost.
     *
     *  @param cost a total cost
     *  @throws org.osid.NullArgumentException
     *          <code>cost</code> is <code>null</code>
     */

    protected void setTotalCost(org.osid.financials.Currency cost) {
        nullarg(cost, "cost");
        this.totalCost = cost;
        return;
    }


    /**
     *  Tests if all the items are processed atomically. 
     *
     *  @return <code> true </code> if the order is atomic, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isAtomic() {
        return (this.atomic);
    }


    /**
     *  Sets the atomic flag.
     *
     *  @param atomic <code> true </code> if the order is atomic,
     *          <code> false </code> otherwise
     */

    protected void setAtomic(boolean atomic) {
        this.atomic = atomic;
        return;
    }


    /**
     *  Tests if all this order has been submitted. 
     *
     *  @return <code> true </code> if the order has been submitted,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isSubmitted() {
        return (this.submitDate != null);
    }


    /**
     *  Gets the date submitted. 
     *
     *  @return the date submitted 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getSubmitDate() {
        if (!isSubmitted()) {
            throw new org.osid.IllegalStateException("isSubmitted() is false");
        }

        return (this.submitDate);
    }


    /**
     *  Sets the submit date.
     *
     *  @param date a submit date
     *  @throws org.osid.NullArgumentException <code>date</code>
     *          is <code>null</code>
     */

    protected void setSubmitDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "submit date");
        this.submitDate = date;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> who submitted the order. 
     *
     *  @return the submitting resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubmitterId() {
        if (!isSubmitted()) {
            throw new org.osid.IllegalStateException("isSubmitted() is false");
        }

        return (this.submitter.getId());
    }


    /**
     *  Gets the resource who submitted the order. 
     *
     *  @return the submitting resource 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSubmitter()
        throws org.osid.OperationFailedException {

        if (!isSubmitted()) {
            throw new org.osid.IllegalStateException("isSubmitted() is false");
        }

        return (this.submitter);
    }


    /**
     *  Sets the submitter.
     *
     *  @param resource the submitter
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setSubmitter(org.osid.resource.Resource resource) {
        nullarg(resource, "submitter");
        this.submitter = resource;
        return;
    }


    /**
     *  Gets the agent <code> Id </code> who submitted the order. 
     *
     *  @return the submitting agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubmittingAgentId() {
        if (!isSubmitted()) {
            throw new org.osid.IllegalStateException("isSubmitted() is false");
        }

        return (this.submitter.getId());
    }


    /**
     *  Gets the agent who submitted the order. 
     *
     *  @return the submitting agent 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getSubmittingAgent()
        throws org.osid.OperationFailedException {

        if (!isSubmitted()) {
            throw new org.osid.IllegalStateException("isSubmitted() is false");
        }

        return (this.submittingAgent);
    }


    /**
     *  Sets the submitting agent.
     *
     *  @param agent the submitting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setSubmittingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "submitting agent");
        this.submittingAgent = agent;
        return;
    }


    /**
     *  Tests if all this order is closed or canceled. 
     *
     *  @return <code> true </code> if the order is closed, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isClosed() {
        return (this.closedDate != null);
    }


    /**
     *  Gets the date this order has been closed. 
     *
     *  @return the date closed 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClosedDate() {
        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isClosed() is true");
        }

        return (this.closedDate);
    }


    /**
     *  Sets the closed date.
     *
     *  @param date a closed date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setClosedDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "closed date");
        this.closedDate = date;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> who closed the order. 
     *
     *  @return the closing resource <code>Id</code> 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCloserId() {
        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isSubmitted() is false");
        }

        return (this.closer.getId());
    }


    /**
     *  Gets the resource who closed the order. 
     *
     *  @return the closing resource 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCloser()
        throws org.osid.OperationFailedException {

        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isSubmitted() is false");
        }

        return (this.closer);
    }


    /**
     *  Sets the closer.
     *
     *  @param resource a closer
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setCloser(org.osid.resource.Resource resource) {
        nullarg(resource, "closing resource");
        this.closer = resource;
        return;
    }


    /**
     *  Gets the agent <code> Id </code> who closed the order. 
     *
     *  @return the closing agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isClosed()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getClosingAgentId() {
        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isSubmitted() is false");
        }

        return (this.closer.getId());
    }


    /**
     *  Gets the agent who closed the order. 
     *
     *  @return the closing agent 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getClosingAgent()
        throws org.osid.OperationFailedException {

        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isSubmitted() is false");
        }

        return (this.closingAgent);
    }


    /**
     *  Sets the closing agent.
     *
     *  @param agent the closing agent
     *  @throws org.osid.NullArgumentException
     *          <code>closer</code> is <code>null</code>
     */

    protected void setClosingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "closing agent");
        this.closingAgent = agent;
        return;
    }


    /**
     *  Tests if this order supports the given record
     *  <code>Type</code>.
     *
     *  @param  orderRecordType an order record type 
     *  @return <code>true</code> if the orderRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type orderRecordType) {
        for (org.osid.ordering.records.OrderRecord record : this.records) {
            if (record.implementsRecordType(orderRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Order</code> record <code>Type</code>.
     *
     *  @param  orderRecordType the order record type 
     *  @return the order record 
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(orderRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.OrderRecord getOrderRecord(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.OrderRecord record : this.records) {
            if (record.implementsRecordType(orderRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(orderRecordType + " is not supported");
    }


    /**
     *  Adds a record to this order. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param orderRecord the order record
     *  @param orderRecordType order record type
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecord</code> or
     *          <code>orderRecordTypeorder</code> is
     *          <code>null</code>
     */
            
    protected void addOrderRecord(org.osid.ordering.records.OrderRecord orderRecord, 
                                  org.osid.type.Type orderRecordType) {

        nullarg(orderRecord, "order record");
        addRecordType(orderRecordType);
        this.records.add(orderRecord);
        
        return;
    }
}
