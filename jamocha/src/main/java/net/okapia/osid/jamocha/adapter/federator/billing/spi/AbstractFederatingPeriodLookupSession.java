//
// AbstractFederatingPeriodLookupSession.java
//
//     An abstract federating adapter for a PeriodLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PeriodLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPeriodLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.billing.PeriodLookupSession>
    implements org.osid.billing.PeriodLookupSession {

    private boolean parallel = false;
    private org.osid.billing.Business business = new net.okapia.osid.jamocha.nil.billing.business.UnknownBusiness();


    /**
     *  Constructs a new <code>AbstractFederatingPeriodLookupSession</code>.
     */

    protected AbstractFederatingPeriodLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.billing.PeriodLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.billing.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Period</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPeriods() {
        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            if (session.canLookupPeriods()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Period</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePeriodView() {
        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            session.useComparativePeriodView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Period</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPeriodView() {
        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            session.usePlenaryPeriodView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include periods in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            session.useFederatedBusinessView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            session.useIsolatedBusinessView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Period</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Period</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Period</code> and
     *  retained for compatibility.
     *
     *  @param  periodId <code>Id</code> of the
     *          <code>Period</code>
     *  @return the period
     *  @throws org.osid.NotFoundException <code>periodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>periodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod(org.osid.id.Id periodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            try {
                return (session.getPeriod(periodId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(periodId + " not found");
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  periods specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Periods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  periodIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>periodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByIds(org.osid.id.IdList periodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.billing.period.MutablePeriodList ret = new net.okapia.osid.jamocha.billing.period.MutablePeriodList();

        try (org.osid.id.IdList ids = periodIds) {
            while (ids.hasNext()) {
                ret.addPeriod(getPeriod(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  period genus <code>Type</code> which does not include
     *  periods of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  periodGenusType a period genus type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByGenusType(org.osid.type.Type periodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.period.FederatingPeriodList ret = getPeriodList();

        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            ret.addPeriodList(session.getPeriodsByGenusType(periodGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  period genus <code>Type</code> and include any additional
     *  periods with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  periodGenusType a period genus type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByParentGenusType(org.osid.type.Type periodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.period.FederatingPeriodList ret = getPeriodList();

        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            ret.addPeriodList(session.getPeriodsByParentGenusType(periodGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PeriodList</code> containing the given
     *  period record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  periodRecordType a period record type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByRecordType(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.period.FederatingPeriodList ret = getPeriodList();

        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            ret.addPeriodList(session.getPeriodsByRecordType(periodRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> PeriodList </code> where to the given <code>
     *  DateTime </code> range falls within the period
     *  inclusive. Periods containing the given date are matched.
     *
     *  In plenary mode, the returned list contains all of the periods
     *  specified in the <code> Id </code> list, in the order of the
     *  list, including duplicates, or an error results if an <code>
     *  Id </code> in the supplied list is not found or inaccessible.
     *  Otherwise, inaccessible <code> Periods </code> may be omitted
     *  from the list including returning a unique set.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Period </code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsOnDate(org.osid.calendaring.DateTime from, 
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.period.FederatingPeriodList ret = getPeriodList();

        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            ret.addPeriodList(session.getPeriodsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Periods</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Periods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.period.FederatingPeriodList ret = getPeriodList();

        for (org.osid.billing.PeriodLookupSession session : getSessions()) {
            ret.addPeriodList(session.getPeriods());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.billing.period.FederatingPeriodList getPeriodList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.billing.period.ParallelPeriodList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.billing.period.CompositePeriodList());
        }
    }
}
