//
// AbstractQueryBrokerProcessorEnablerLookupSession.java
//
//    An inline adapter that maps a BrokerProcessorEnablerLookupSession to
//    a BrokerProcessorEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BrokerProcessorEnablerLookupSession to
 *  a BrokerProcessorEnablerQuerySession.
 */

public abstract class AbstractQueryBrokerProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractBrokerProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.provisioning.rules.BrokerProcessorEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBrokerProcessorEnablerLookupSession.
     *
     *  @param querySession the underlying broker processor enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBrokerProcessorEnablerLookupSession(org.osid.provisioning.rules.BrokerProcessorEnablerQuerySession querySession) {
        nullarg(querySession, "broker processor enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>BrokerProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBrokerProcessorEnablers() {
        return (this.session.canSearchBrokerProcessorEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker processor enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active broker processor enablers are returned by methods
     *  in this session.
     */
     
    @OSID @Override
    public void useActiveBrokerProcessorEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive broker processor enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerProcessorEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>BrokerProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BrokerProcessorEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>BrokerProcessorEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  brokerProcessorEnablerId <code>Id</code> of the
     *          <code>BrokerProcessorEnabler</code>
     *  @return the broker processor enabler
     *  @throws org.osid.NotFoundException <code>brokerProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnabler getBrokerProcessorEnabler(org.osid.id.Id brokerProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorEnablerQuery query = getQuery();
        query.matchId(brokerProcessorEnablerId, true);
        org.osid.provisioning.rules.BrokerProcessorEnablerList brokerProcessorEnablers = this.session.getBrokerProcessorEnablersByQuery(query);
        if (brokerProcessorEnablers.hasNext()) {
            return (brokerProcessorEnablers.getNextBrokerProcessorEnabler());
        } 
        
        throw new org.osid.NotFoundException(brokerProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  brokerProcessorEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>BrokerProcessorEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  brokerProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByIds(org.osid.id.IdList brokerProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = brokerProcessorEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBrokerProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> corresponding
     *  to the given broker processor enabler genus <code>Type</code>
     *  which does not include broker processor enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  brokerProcessorEnablerGenusType a brokerProcessorEnabler genus type 
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByGenusType(org.osid.type.Type brokerProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorEnablerQuery query = getQuery();
        query.matchGenusType(brokerProcessorEnablerGenusType, true);
        return (this.session.getBrokerProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> corresponding
     *  to the given broker processor enabler genus <code>Type</code>
     *  and include any additional broker processor enablers with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  brokerProcessorEnablerGenusType a brokerProcessorEnabler genus type 
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByParentGenusType(org.osid.type.Type brokerProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorEnablerQuery query = getQuery();
        query.matchParentGenusType(brokerProcessorEnablerGenusType, true);
        return (this.session.getBrokerProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> containing the
     *  given broker processor enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  brokerProcessorEnablerRecordType a brokerProcessorEnabler record type 
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByRecordType(org.osid.type.Type brokerProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorEnablerQuery query = getQuery();
        query.matchRecordType(brokerProcessorEnablerRecordType, true);
        return (this.session.getBrokerProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session.
     *  
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>BrokerProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getBrokerProcessorEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>BrokerProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @return a list of <code>BrokerProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBrokerProcessorEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.rules.BrokerProcessorEnablerQuery getQuery() {
        org.osid.provisioning.rules.BrokerProcessorEnablerQuery query = this.session.getBrokerProcessorEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
