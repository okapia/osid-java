//
// MutableMapProxyWorkLookupSession
//
//    Implements a Work lookup service backed by a collection of
//    works that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Work lookup service backed by a collection of
 *  works. The works are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of works can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyWorkLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapWorkLookupSession
    implements org.osid.resourcing.WorkLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyWorkLookupSession}
     *  with no works.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyWorkLookupSession} with a
     *  single work.
     *
     *  @param foundry the foundry
     *  @param work a work
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code work}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.Work work, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putWork(work);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyWorkLookupSession} using an
     *  array of works.
     *
     *  @param foundry the foundry
     *  @param works an array of works
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code works}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.Work[] works, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putWorks(works);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyWorkLookupSession} using a
     *  collection of works.
     *
     *  @param foundry the foundry
     *  @param works a collection of works
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code works}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                java.util.Collection<? extends org.osid.resourcing.Work> works,
                                                org.osid.proxy.Proxy proxy) {
   
        this(foundry, proxy);
        setSessionProxy(proxy);
        putWorks(works);
        return;
    }

    
    /**
     *  Makes a {@code Work} available in this session.
     *
     *  @param work an work
     *  @throws org.osid.NullArgumentException {@code work{@code 
     *          is {@code null}
     */

    @Override
    public void putWork(org.osid.resourcing.Work work) {
        super.putWork(work);
        return;
    }


    /**
     *  Makes an array of works available in this session.
     *
     *  @param works an array of works
     *  @throws org.osid.NullArgumentException {@code works{@code 
     *          is {@code null}
     */

    @Override
    public void putWorks(org.osid.resourcing.Work[] works) {
        super.putWorks(works);
        return;
    }


    /**
     *  Makes collection of works available in this session.
     *
     *  @param works
     *  @throws org.osid.NullArgumentException {@code work{@code 
     *          is {@code null}
     */

    @Override
    public void putWorks(java.util.Collection<? extends org.osid.resourcing.Work> works) {
        super.putWorks(works);
        return;
    }


    /**
     *  Removes a Work from this session.
     *
     *  @param workId the {@code Id} of the work
     *  @throws org.osid.NullArgumentException {@code workId{@code  is
     *          {@code null}
     */

    @Override
    public void removeWork(org.osid.id.Id workId) {
        super.removeWork(workId);
        return;
    }    
}
