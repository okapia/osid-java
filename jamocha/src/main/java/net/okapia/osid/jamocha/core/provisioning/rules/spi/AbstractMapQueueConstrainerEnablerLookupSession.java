//
// AbstractMapQueueConstrainerEnablerLookupSession
//
//    A simple framework for providing a QueueConstrainerEnabler lookup service
//    backed by a fixed collection of queue constrainer enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a QueueConstrainerEnabler lookup service backed by a
 *  fixed collection of queue constrainer enablers. The queue constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>QueueConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapQueueConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractQueueConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.QueueConstrainerEnabler> queueConstrainerEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.QueueConstrainerEnabler>());


    /**
     *  Makes a <code>QueueConstrainerEnabler</code> available in this session.
     *
     *  @param  queueConstrainerEnabler a queue constrainer enabler
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnabler<code>
     *          is <code>null</code>
     */

    protected void putQueueConstrainerEnabler(org.osid.provisioning.rules.QueueConstrainerEnabler queueConstrainerEnabler) {
        this.queueConstrainerEnablers.put(queueConstrainerEnabler.getId(), queueConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of queue constrainer enablers available in this session.
     *
     *  @param  queueConstrainerEnablers an array of queue constrainer enablers
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putQueueConstrainerEnablers(org.osid.provisioning.rules.QueueConstrainerEnabler[] queueConstrainerEnablers) {
        putQueueConstrainerEnablers(java.util.Arrays.asList(queueConstrainerEnablers));
        return;
    }


    /**
     *  Makes a collection of queue constrainer enablers available in this session.
     *
     *  @param  queueConstrainerEnablers a collection of queue constrainer enablers
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putQueueConstrainerEnablers(java.util.Collection<? extends org.osid.provisioning.rules.QueueConstrainerEnabler> queueConstrainerEnablers) {
        for (org.osid.provisioning.rules.QueueConstrainerEnabler queueConstrainerEnabler : queueConstrainerEnablers) {
            this.queueConstrainerEnablers.put(queueConstrainerEnabler.getId(), queueConstrainerEnabler);
        }

        return;
    }


    /**
     *  Removes a QueueConstrainerEnabler from this session.
     *
     *  @param  queueConstrainerEnablerId the <code>Id</code> of the queue constrainer enabler
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeQueueConstrainerEnabler(org.osid.id.Id queueConstrainerEnablerId) {
        this.queueConstrainerEnablers.remove(queueConstrainerEnablerId);
        return;
    }


    /**
     *  Gets the <code>QueueConstrainerEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  queueConstrainerEnablerId <code>Id</code> of the <code>QueueConstrainerEnabler</code>
     *  @return the queueConstrainerEnabler
     *  @throws org.osid.NotFoundException <code>queueConstrainerEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnabler getQueueConstrainerEnabler(org.osid.id.Id queueConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.QueueConstrainerEnabler queueConstrainerEnabler = this.queueConstrainerEnablers.get(queueConstrainerEnablerId);
        if (queueConstrainerEnabler == null) {
            throw new org.osid.NotFoundException("queueConstrainerEnabler not found: " + queueConstrainerEnablerId);
        }

        return (queueConstrainerEnabler);
    }


    /**
     *  Gets all <code>QueueConstrainerEnablers</code>. In plenary mode, the returned
     *  list contains all known queueConstrainerEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  queueConstrainerEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>QueueConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueconstrainerenabler.ArrayQueueConstrainerEnablerList(this.queueConstrainerEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.queueConstrainerEnablers.clear();
        super.close();
        return;
    }
}
