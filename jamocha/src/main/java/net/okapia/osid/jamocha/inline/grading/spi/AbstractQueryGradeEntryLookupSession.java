//
// AbstractQueryGradeEntryLookupSession.java
//
//    An inline adapter that maps a GradeEntryLookupSession to
//    a GradeEntryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a GradeEntryLookupSession to
 *  a GradeEntryQuerySession.
 */

public abstract class AbstractQueryGradeEntryLookupSession
    extends net.okapia.osid.jamocha.grading.spi.AbstractGradeEntryLookupSession
    implements org.osid.grading.GradeEntryLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.grading.GradeEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryGradeEntryLookupSession.
     *
     *  @param querySession the underlying grade entry query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryGradeEntryLookupSession(org.osid.grading.GradeEntryQuerySession querySession) {
        nullarg(querySession, "grade entry query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Gradebook</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Gradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.session.getGradebookId());
    }


    /**
     *  Gets the <code>Gradebook</code> associated with this 
     *  session.
     *
     *  @return the <code>Gradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getGradebook());
    }


    /**
     *  Tests if this user can perform <code>GradeEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradeEntries() {
        return (this.session.canSearchGradeEntries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade entries in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.session.useFederatedGradebookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.session.useIsolatedGradebookView();
        return;
    }
    

    /**
     *  Only grade entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveGradeEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All grade entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveGradeEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>GradeEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradeEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>GradeEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param  gradeEntryId <code>Id</code> of the
     *          <code>GradeEntry</code>
     *  @return the grade entry
     *  @throws org.osid.NotFoundException <code>gradeEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradeEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntry getGradeEntry(org.osid.id.Id gradeEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchId(gradeEntryId, true);
        org.osid.grading.GradeEntryList gradeEntries = this.session.getGradeEntriesByQuery(query);
        if (gradeEntries.hasNext()) {
            return (gradeEntries.getNextGradeEntry());
        } 
        
        throw new org.osid.NotFoundException(gradeEntryId + " not found");
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>GradeEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, grade entries are returned that are currently effective.
     *  In any effective mode, effective grade entries and those currently expired
     *  are returned.
     *
     *  @param  gradeEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByIds(org.osid.id.IdList gradeEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();

        try (org.osid.id.IdList ids = gradeEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  grade entry genus <code>Type</code> which does not include
     *  grade entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently effective.
     *  In any effective mode, effective grade entries and those currently expired
     *  are returned.
     *
     *  @param  gradeEntryGenusType a gradeEntry genus type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByGenusType(org.osid.type.Type gradeEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchGenusType(gradeEntryGenusType, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  grade entry genus <code>Type</code> and include any additional
     *  grade entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradeEntryGenusType a gradeEntry genus type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByParentGenusType(org.osid.type.Type gradeEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchParentGenusType(gradeEntryGenusType, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a <code>GradeEntryList</code> containing the given grade
     *  entry record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective. In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradeEntryRecordType a gradeEntry record type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByRecordType(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchRecordType(gradeEntryRecordType, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a <code>GradeEntryList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *  
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>GradeEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getGradeEntriesByQuery(query));
    }
        

    /**
     *  Gets a list of grade entries corresponding to a gradebook
     *  column <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param gradebookColumnId the <code>Id</code> of the gradebook
     *         column
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumn(org.osid.id.Id gradebookColumnId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchGradebookColumnId(gradebookColumnId, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a list of grade entries corresponding to a gradebook
     *  column <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible
     *  through this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the <code>Id</code> of the gradebook column
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnOnDate(org.osid.id.Id gradebookColumnId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchGradebookColumnId(gradebookColumnId, true);
        query.matchDate(from, to, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a list of grade entries corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.grading.GradeEntryList getGradeEntriesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchKeyResourceId(resourceId, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a list of grade entries corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible
     *  through this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForResourceOnDate(org.osid.id.Id resourceId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchKeyResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a list of grade entries corresponding to gradebook column
     *  and resource <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the <code>Id</code> of the gradebook column
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnAndResource(org.osid.id.Id gradebookColumnId,
                                                                                        org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchGradebookColumnId(gradebookColumnId, true);
        query.matchKeyResourceId(resourceId, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a list of grade entries corresponding to gradebook column and resource
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible
     *  through this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  grade entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the <code>Id</code> of the gradebook column
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnAndResourceOnDate(org.osid.id.Id gradebookColumnId,
                                                                                              org.osid.id.Id resourceId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchGradebookColumnId(gradebookColumnId, true);
        query.matchKeyResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets a <code> GradeEntryList </code> for the given grader.
     *  
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *  
     *  In effective mode, grade entries are returned that are
     *  currently effective. In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> GradeEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByGrader(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchGraderId(resourceId, true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets all <code>GradeEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>GradeEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeEntryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getGradeEntriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.grading.GradeEntryQuery getQuery() {
        org.osid.grading.GradeEntryQuery query = this.session.getGradeEntryQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
