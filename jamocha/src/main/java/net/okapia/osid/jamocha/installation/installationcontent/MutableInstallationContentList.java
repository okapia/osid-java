//
// MutableInstallationContentList.java
//
//     Implements an InstallationContentList. This list allows InstallationContents to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installationcontent;


/**
 *  <p>Implements an InstallationContentList. This list allows InstallationContents to be
 *  added after this installationContent has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this installationContent must
 *  invoke <code>eol()</code> when there are no more installationContents to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>InstallationContentList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more installationContents are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more installationContents to be added.</p>
 */

public final class MutableInstallationContentList
    extends net.okapia.osid.jamocha.installation.installationcontent.spi.AbstractMutableInstallationContentList
    implements org.osid.installation.InstallationContentList {


    /**
     *  Creates a new empty <code>MutableInstallationContentList</code>.
     */

    public MutableInstallationContentList() {
        super();
    }


    /**
     *  Creates a new <code>MutableInstallationContentList</code>.
     *
     *  @param installationContent an <code>InstallationContent</code>
     *  @throws org.osid.NullArgumentException <code>installationContent</code>
     *          is <code>null</code>
     */

    public MutableInstallationContentList(org.osid.installation.InstallationContent installationContent) {
        super(installationContent);
        return;
    }


    /**
     *  Creates a new <code>MutableInstallationContentList</code>.
     *
     *  @param array an array of installationcontents
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableInstallationContentList(org.osid.installation.InstallationContent[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableInstallationContentList</code>.
     *
     *  @param collection a java.util.Collection of installationcontents
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableInstallationContentList(java.util.Collection<org.osid.installation.InstallationContent> collection) {
        super(collection);
        return;
    }
}
