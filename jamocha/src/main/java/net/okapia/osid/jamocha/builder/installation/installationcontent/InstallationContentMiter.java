//
// InstallationContentMiter.java
//
//     Defines an InstallationContent miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.installationcontent;


/**
 *  Defines an <code>InstallationContent</code> miter for use with the builders.
 */

public interface InstallationContentMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.installation.InstallationContent {


    /**
     *  Sets the package.
     *
     *  @param pkg a package
     *  @throws org.osid.NullArgumentException
     *          <code>pkg</code> is <code>null</code>
     */

    public void setPackage(org.osid.installation.Package pkg);


    /**
     *  Sets the data length.
     *
     *  @param dataLength a data length
     *  @throws org.osid.InvalidArgumentException <code>length</code>
     *          is negative
     */

    public void setDataLength(long dataLength);


    /**
     *  Sets the data.
     *
     *  @param data the data
     *  @throws org.osid.NullArgumentException <code>data</code> is
     *          <code>null</code>
     */

    public void setData(java.nio.ByteBuffer data);


    /**
     *  Adds an InstallationContent record.
     *
     *  @param record an installationContent record
     *  @param recordType the type of installationContent record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addInstallationContentRecord(org.osid.installation.records.InstallationContentRecord record, org.osid.type.Type recordType);
}       


