//
// AbstractCheckQueryInspector.java
//
//     A template for making a CheckQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for checks.
 */

public abstract class AbstractCheckQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.rules.check.CheckQueryInspector {

    private final java.util.Collection<org.osid.rules.check.records.CheckQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the fail check query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getFailCheckTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the time check start date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeCheckStartDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the time check end date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeCheckEndDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the time check date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeCheckDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimeCheckEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getTimeCheckEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the cyclic event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimeCheckCyclicEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the cyclic event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQueryInspector[] getTimeCheckCyclicEventTerms() {
        return (new org.osid.calendaring.cycle.CyclicEventQueryInspector[0]);
    }


    /**
     *  Gets the block <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getHoldCheckBlockIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the block query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.BlockQueryInspector[] getHoldCheckBlockTerms() {
        return (new org.osid.hold.BlockQueryInspector[0]);
    }


    /**
     *  Gets the audit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquiryCheckAuditIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the audit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQueryInspector[] getInquiryCheckAuditTerms() {
        return (new org.osid.inquiry.AuditQueryInspector[0]);
    }


    /**
     *  Gets the agenda <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcessCheckAgendaIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agenda query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQueryInspector[] getProcessCheckAgendaTerms() {
        return (new org.osid.rules.check.AgendaQueryInspector[0]);
    }


    /**
     *  Gets the engine <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEngineIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the engine query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.EngineQueryInspector[] getEngineTerms() {
        return (new org.osid.rules.EngineQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given check query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a check implementing the requested record.
     *
     *  @param checkRecordType a check record type
     *  @return the check query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checkRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckQueryInspectorRecord getCheckQueryInspectorRecord(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.CheckQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(checkRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checkRecordType + " is not supported");
    }


    /**
     *  Adds a record to this check query. 
     *
     *  @param checkQueryInspectorRecord check query inspector
     *         record
     *  @param checkRecordType check record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCheckQueryInspectorRecord(org.osid.rules.check.records.CheckQueryInspectorRecord checkQueryInspectorRecord, 
                                                   org.osid.type.Type checkRecordType) {

        addRecordType(checkRecordType);
        nullarg(checkRecordType, "check record type");
        this.records.add(checkQueryInspectorRecord);        
        return;
    }
}
