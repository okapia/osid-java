//
// AbstractProgramEntrySearch.java
//
//     A template for making a ProgramEntry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.programentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing program entry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProgramEntrySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.chronicle.ProgramEntrySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.ProgramEntrySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.chronicle.ProgramEntrySearchOrder programEntrySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of program entries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  programEntryIds list of program entries
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProgramEntries(org.osid.id.IdList programEntryIds) {
        while (programEntryIds.hasNext()) {
            try {
                this.ids.add(programEntryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProgramEntries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of program entry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProgramEntryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  programEntrySearchOrder program entry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>programEntrySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>programEntrySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProgramEntryResults(org.osid.course.chronicle.ProgramEntrySearchOrder programEntrySearchOrder) {
	this.programEntrySearchOrder = programEntrySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.chronicle.ProgramEntrySearchOrder getProgramEntrySearchOrder() {
	return (this.programEntrySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given program entry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a program entry implementing the requested record.
     *
     *  @param programEntrySearchRecordType a program entry search record
     *         type
     *  @return the program entry search record
     *  @throws org.osid.NullArgumentException
     *          <code>programEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programEntrySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.ProgramEntrySearchRecord getProgramEntrySearchRecord(org.osid.type.Type programEntrySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.chronicle.records.ProgramEntrySearchRecord record : this.records) {
            if (record.implementsRecordType(programEntrySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program entry search. 
     *
     *  @param programEntrySearchRecord program entry search record
     *  @param programEntrySearchRecordType programEntry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProgramEntrySearchRecord(org.osid.course.chronicle.records.ProgramEntrySearchRecord programEntrySearchRecord, 
                                           org.osid.type.Type programEntrySearchRecordType) {

        addRecordType(programEntrySearchRecordType);
        this.records.add(programEntrySearchRecord);        
        return;
    }
}
