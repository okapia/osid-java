//
// AbstractAdapterJobConstrainerLookupSession.java
//
//    A JobConstrainer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A JobConstrainer lookup session adapter.
 */

public abstract class AbstractAdapterJobConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.rules.JobConstrainerLookupSession {

    private final org.osid.resourcing.rules.JobConstrainerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterJobConstrainerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterJobConstrainerLookupSession(org.osid.resourcing.rules.JobConstrainerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code JobConstrainer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupJobConstrainers() {
        return (this.session.canLookupJobConstrainers());
    }


    /**
     *  A complete view of the {@code JobConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJobConstrainerView() {
        this.session.useComparativeJobConstrainerView();
        return;
    }


    /**
     *  A complete view of the {@code JobConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJobConstrainerView() {
        this.session.usePlenaryJobConstrainerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job constrainers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only active job constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobConstrainerView() {
        this.session.useActiveJobConstrainerView();
        return;
    }


    /**
     *  Active and inactive job constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobConstrainerView() {
        this.session.useAnyStatusJobConstrainerView();
        return;
    }
    
     
    /**
     *  Gets the {@code JobConstrainer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code JobConstrainer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code JobConstrainer} and
     *  retained for compatibility.
     *
     *  In active mode, job constrainers are returned that are currently
     *  active. In any status mode, active and inactive job constrainers
     *  are returned.
     *
     *  @param jobConstrainerId {@code Id} of the {@code JobConstrainer}
     *  @return the job constrainer
     *  @throws org.osid.NotFoundException {@code jobConstrainerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code jobConstrainerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainer getJobConstrainer(org.osid.id.Id jobConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainer(jobConstrainerId));
    }


    /**
     *  Gets a {@code JobConstrainerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  jobConstrainers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code JobConstrainers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, job constrainers are returned that are currently
     *  active. In any status mode, active and inactive job constrainers
     *  are returned.
     *
     *  @param  jobConstrainerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code JobConstrainer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code jobConstrainerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByIds(org.osid.id.IdList jobConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainersByIds(jobConstrainerIds));
    }


    /**
     *  Gets a {@code JobConstrainerList} corresponding to the given
     *  job constrainer genus {@code Type} which does not include
     *  job constrainers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  job constrainers or an error results. Otherwise, the returned list
     *  may contain only those job constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job constrainers are returned that are currently
     *  active. In any status mode, active and inactive job constrainers
     *  are returned.
     *
     *  @param  jobConstrainerGenusType a jobConstrainer genus type 
     *  @return the returned {@code JobConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobConstrainerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByGenusType(org.osid.type.Type jobConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainersByGenusType(jobConstrainerGenusType));
    }


    /**
     *  Gets a {@code JobConstrainerList} corresponding to the given
     *  job constrainer genus {@code Type} and include any additional
     *  job constrainers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  job constrainers or an error results. Otherwise, the returned list
     *  may contain only those job constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job constrainers are returned that are currently
     *  active. In any status mode, active and inactive job constrainers
     *  are returned.
     *
     *  @param  jobConstrainerGenusType a jobConstrainer genus type 
     *  @return the returned {@code JobConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobConstrainerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByParentGenusType(org.osid.type.Type jobConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainersByParentGenusType(jobConstrainerGenusType));
    }


    /**
     *  Gets a {@code JobConstrainerList} containing the given
     *  job constrainer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  job constrainers or an error results. Otherwise, the returned list
     *  may contain only those job constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job constrainers are returned that are currently
     *  active. In any status mode, active and inactive job constrainers
     *  are returned.
     *
     *  @param  jobConstrainerRecordType a jobConstrainer record type 
     *  @return the returned {@code JobConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobConstrainerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByRecordType(org.osid.type.Type jobConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainersByRecordType(jobConstrainerRecordType));
    }


    /**
     *  Gets all {@code JobConstrainers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  job constrainers or an error results. Otherwise, the returned list
     *  may contain only those job constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job constrainers are returned that are currently
     *  active. In any status mode, active and inactive job constrainers
     *  are returned.
     *
     *  @return a list of {@code JobConstrainers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainers());
    }
}
