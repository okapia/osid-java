//
// AbstractParameterProcessor.java
//
//     Defines a ParameterProcessor builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.rules.parameterprocessor.spi;


/**
 *  Defines a <code>ParameterProcessor</code> builder.
 */

public abstract class AbstractParameterProcessorBuilder<T extends AbstractParameterProcessorBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidProcessorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.configuration.rules.parameterprocessor.ParameterProcessorMiter parameterProcessor;


    /**
     *  Constructs a new <code>AbstractParameterProcessorBuilder</code>.
     *
     *  @param parameterProcessor the parameter processor to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractParameterProcessorBuilder(net.okapia.osid.jamocha.builder.configuration.rules.parameterprocessor.ParameterProcessorMiter parameterProcessor) {
        super(parameterProcessor);
        this.parameterProcessor = parameterProcessor;
        return;
    }


    /**
     *  Builds the parameter processor.
     *
     *  @return the new parameter processor
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.configuration.rules.ParameterProcessor build() {
        (new net.okapia.osid.jamocha.builder.validator.configuration.rules.parameterprocessor.ParameterProcessorValidator(getValidations())).validate(this.parameterProcessor);
        return (new net.okapia.osid.jamocha.builder.configuration.rules.parameterprocessor.ImmutableParameterProcessor(this.parameterProcessor));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the parameter processor miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.configuration.rules.parameterprocessor.ParameterProcessorMiter getMiter() {
        return (this.parameterProcessor);
    }


    /**
     *  Adds a ParameterProcessor record.
     *
     *  @param record a parameter processor record
     *  @param recordType the type of parameter processor record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.configuration.rules.records.ParameterProcessorRecord record, org.osid.type.Type recordType) {
        getMiter().addParameterProcessorRecord(record, recordType);
        return (self());
    }
}       


