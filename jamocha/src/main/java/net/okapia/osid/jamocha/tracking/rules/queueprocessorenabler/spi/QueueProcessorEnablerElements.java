//
// QueueProcessorEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.rules.queueprocessorenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class QueueProcessorEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the QueueProcessorEnablerElement Id.
     *
     *  @return the queue processor enabler element Id
     */

    public static org.osid.id.Id getQueueProcessorEnablerEntityId() {
        return (makeEntityId("osid.tracking.rules.QueueProcessorEnabler"));
    }


    /**
     *  Gets the RuledQueueProcessorId element Id.
     *
     *  @return the RuledQueueProcessorId element Id
     */

    public static org.osid.id.Id getRuledQueueProcessorId() {
        return (makeQueryElementId("osid.tracking.rules.queueprocessorenabler.RuledQueueProcessorId"));
    }


    /**
     *  Gets the RuledQueueProcessor element Id.
     *
     *  @return the RuledQueueProcessor element Id
     */

    public static org.osid.id.Id getRuledQueueProcessor() {
        return (makeQueryElementId("osid.tracking.rules.queueprocessorenabler.RuledQueueProcessor"));
    }


    /**
     *  Gets the FrontOfficeId element Id.
     *
     *  @return the FrontOfficeId element Id
     */

    public static org.osid.id.Id getFrontOfficeId() {
        return (makeQueryElementId("osid.tracking.rules.queueprocessorenabler.FrontOfficeId"));
    }


    /**
     *  Gets the FrontOffice element Id.
     *
     *  @return the FrontOffice element Id
     */

    public static org.osid.id.Id getFrontOffice() {
        return (makeQueryElementId("osid.tracking.rules.queueprocessorenabler.FrontOffice"));
    }
}
