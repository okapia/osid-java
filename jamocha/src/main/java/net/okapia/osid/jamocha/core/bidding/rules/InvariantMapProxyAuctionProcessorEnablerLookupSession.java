//
// InvariantMapProxyAuctionProcessorEnablerLookupSession
//
//    Implements an AuctionProcessorEnabler lookup service backed by a fixed
//    collection of auctionProcessorEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules;


/**
 *  Implements an AuctionProcessorEnabler lookup service backed by a fixed
 *  collection of auction processor enablers. The auction processor enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAuctionProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.bidding.rules.spi.AbstractMapAuctionProcessorEnablerLookupSession
    implements org.osid.bidding.rules.AuctionProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuctionProcessorEnablerLookupSession} with no
     *  auction processor enablers.
     *
     *  @param auctionHouse the auction house
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionProcessorEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.proxy.Proxy proxy) {
        setAuctionHouse(auctionHouse);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAuctionProcessorEnablerLookupSession} with a single
     *  auction processor enabler.
     *
     *  @param auctionHouse the auction house
     *  @param auctionProcessorEnabler an single auction processor enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code auctionProcessorEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionProcessorEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler, org.osid.proxy.Proxy proxy) {

        this(auctionHouse, proxy);
        putAuctionProcessorEnabler(auctionProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAuctionProcessorEnablerLookupSession} using
     *  an array of auction processor enablers.
     *
     *  @param auctionHouse the auction house
     *  @param auctionProcessorEnablers an array of auction processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code auctionProcessorEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionProcessorEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.bidding.rules.AuctionProcessorEnabler[] auctionProcessorEnablers, org.osid.proxy.Proxy proxy) {

        this(auctionHouse, proxy);
        putAuctionProcessorEnablers(auctionProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuctionProcessorEnablerLookupSession} using a
     *  collection of auction processor enablers.
     *
     *  @param auctionHouse the auction house
     *  @param auctionProcessorEnablers a collection of auction processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code auctionProcessorEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionProcessorEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  java.util.Collection<? extends org.osid.bidding.rules.AuctionProcessorEnabler> auctionProcessorEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(auctionHouse, proxy);
        putAuctionProcessorEnablers(auctionProcessorEnablers);
        return;
    }
}
