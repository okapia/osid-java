//
// AbstractOsidEnablerQueryInspector.java
//
//     Defines an OsidEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an OsidEnablerQueryInspector to extend. 
 */

public abstract class AbstractOsidEnablerQueryInspector
    extends AbstractOsidRuleQueryInspector
    implements org.osid.OsidEnablerQueryInspector {

    private final java.util.Collection<org.osid.search.terms.IdTerm> scheduleIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> eventIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> cyclicEventIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> demographicIdTerms = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.calendaring.ScheduleQueryInspector> scheduleTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.EventQueryInspector> eventTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.cycle.CyclicEventQueryInspector> cyclicEventTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.ResourceQueryInspector> demographicTerms = new java.util.LinkedHashSet<>();

    private final OsidTemporalQueryInspector inspector = new OsidTemporalQueryInspector();


    /**
     *  Gets the schedule <code>Id</code> query terms.
     *
     *  @return the schedule <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScheduleIdTerms() {
        return (this.scheduleIdTerms.toArray(new org.osid.search.terms.IdTerm[this.scheduleIdTerms.size()]));
    }

    
    /**
     *  Adds a schedule <code>Id</code> term.
     *
     *  @param term a schedule <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addScheduleIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "schedule Id term");
        this.scheduleIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of schedule <code>Id</code> terms.
     *
     *  @param terms a collection of schedule <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addScheduleIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "schedule Id terms");
        this.scheduleIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a schedule <code>Id</code> term.
     *
     *  @param scheduleId the schedule <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>scheduleId</code>
     *          or is <code>null</code>
     */

    protected void addScheduleIdTerm(org.osid.id.Id scheduleId, boolean match) {
        this.scheduleIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(scheduleId, match));
        return;
    }


    /**
     *  Gets the schedule query terms.
     *
     *  @return the schedule terms
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQueryInspector[] getScheduleTerms() {
        return (this.scheduleTerms.toArray(new org.osid.calendaring.ScheduleQueryInspector[this.scheduleTerms.size()]));
    }

    
    /**
     *  Adds a schedule term.
     *
     *  @param term a schedule term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addScheduleTerm(org.osid.calendaring.ScheduleQueryInspector term) {
        nullarg(term, "schedule term");
        this.scheduleTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of schedule terms.
     *
     *  @param terms a collection of schedule terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addScheduleTerms(java.util.Collection<org.osid.calendaring.ScheduleQueryInspector> terms) {
        nullarg(terms, "schedule terms");
        this.scheduleTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the event <code>Id</code> query terms.
     *
     *  @return the event <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (this.eventIdTerms.toArray(new org.osid.search.terms.IdTerm[this.eventIdTerms.size()]));
    }

    
    /**
     *  Adds an event <code>Id</code> term.
     *
     *  @param term an event <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEventIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "event Id term");
        this.eventIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of event <code>Id</code> terms.
     *
     *  @param terms a collection of event <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addEventIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "event Id terms");
        this.eventIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds an event <code>Id</code> term.
     *
     *  @param eventId the event <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>eventId</code>
     *          or is <code>null</code>
     */

    protected void addEventIdTerm(org.osid.id.Id eventId, boolean match) {
        this.eventIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(eventId, match));
        return;
    }


    /**
     *  Gets the event query terms.
     *
     *  @return the event terms
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (this.eventTerms.toArray(new org.osid.calendaring.EventQueryInspector[this.eventTerms.size()]));
    }

    
    /**
     *  Adds an event term.
     *
     *  @param term an event term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEventTerm(org.osid.calendaring.EventQueryInspector term) {
        nullarg(term, "event term");
        this.eventTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of event terms.
     *
     *  @param terms a collection of event terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addEventTerms(java.util.Collection<org.osid.calendaring.EventQueryInspector> terms) {
        nullarg(terms, "event terms");
        this.eventTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the cyclic event <code>Id</code> query terms.
     *
     *  @return the cyclic event <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCyclicEventIdTerms() {
        return (this.cyclicEventIdTerms.toArray(new org.osid.search.terms.IdTerm[this.cyclicEventIdTerms.size()]));
    }

    
    /**
     *  Adds a cyclic event <code>Id</code> term.
     *
     *  @param term a cyclic event <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addCyclicEventIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "cyclic event Id term");
        this.cyclicEventIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of cyclic event <code>Id</code> terms.
     *
     *  @param terms a collection of cyclic event <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addCyclicEventIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "cyclic event Id terms");
        this.cyclicEventIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a cyclic event <code>Id</code> term.
     *
     *  @param eventId the cyclic event <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>eventId</code>
     *          or is <code>null</code>
     */

    protected void addCyclicEventIdTerm(org.osid.id.Id eventId, boolean match) {
        this.cyclicEventIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(eventId, match));
        return;
    }


    /**
     *  Gets the cyclic event query terms.
     *
     *  @return the cyclic event terms
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQueryInspector[] getCyclicEventTerms() {
        return (this.cyclicEventTerms.toArray(new org.osid.calendaring.cycle.CyclicEventQueryInspector[this.cyclicEventTerms.size()]));
    }

    
    /**
     *  Adds a cyclic event term.
     *
     *  @param term a cyclic event term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addCyclicEventTerm(org.osid.calendaring.cycle.CyclicEventQueryInspector term) {
        nullarg(term, "cyclic event term");
        this.cyclicEventTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of cyclic event terms.
     *
     *  @param terms a collection of cyclic event terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addCyclicEventTerms(java.util.Collection<org.osid.calendaring.cycle.CyclicEventQueryInspector> terms) {
        nullarg(terms, "cyclic event terms");
        this.cyclicEventTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the demographic <code>Id</code> query terms.
     *
     *  @return the demographic <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDemographicIdTerms() {
        return (this.demographicIdTerms.toArray(new org.osid.search.terms.IdTerm[this.demographicIdTerms.size()]));
    }

    
    /**
     *  Adds a demographic <code>Id</code> term.
     *
     *  @param term a resource <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addDemographicIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "demographic resource Id term");
        this.demographicIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of demographic resource <code>Id</code>
     *  terms.
     *
     *  @param terms a collection of demographic <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addDemographicIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "demographic resource Id terms");
        this.demographicIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a demographic resource <code>Id</code> term.
     *
     *  @param resourceId the demographic <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or is <code>null</code>
     */

    protected void addDemographicIdTerm(org.osid.id.Id resourceId, boolean match) {
        this.demographicIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(resourceId, match));
        return;
    }


    /**
     *  Gets the demographic query terms.
     *
     *  @return the demographic terms
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getDemographicTerms() {
        return (this.demographicTerms.toArray(new org.osid.resource.ResourceQueryInspector[this.demographicTerms.size()]));
    }

    
    /**
     *  Adds a demographic resource term.
     *
     *  @param term a demographic term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addDemographicTerm(org.osid.resource.ResourceQueryInspector term) {
        nullarg(term, "demographic resource term");
        this.demographicTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of demographic terms.
     *
     *  @param terms a collection of demographic terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addDemographicTerms(java.util.Collection<org.osid.resource.ResourceQueryInspector> terms) {
        nullarg(terms, "demographic terms");
        this.demographicTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the effective query terms.
     *
     *  @return the effective terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEffectiveTerms() {
        return (this.inspector.getEffectiveTerms());
    }


    /**
     *  Adds an effective term.
     *
     *  @param term an effective term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEffectiveTerm(org.osid.search.terms.BooleanTerm term) {
        this.inspector.addEffectiveTerm(term);
        return;
    }


    /**
     *  Adds a collection of effective terms.
     *
     *  @param terms a collection of effective terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is
     *          <code>null</code>
     */

    protected void addEffectiveTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        this.inspector.addEffectiveTerms(terms);
        return;
    }


    /**
     *  Adds an effective term.
     *
     *  @param effective flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addEffectiveTerm(boolean effective, boolean match) {
        this.inspector.addEffectiveTerm(effective, match);
        return;
    }


    /**
     *  Gets the start date query terms.
     *
     *  @return the start date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (this.inspector.getStartDateTerms());
    }

    
    /**
     *  Adds a start date term.
     *
     *  @param term a start date
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addStartDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
        this.inspector.addStartDateTerm(term);
        return;
    }


    /**
     *  Adds a collection of start date terms.
     *
     *  @param terms a collection of start date terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addStartDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
        this.inspector.addStartDateTerms(terms);
        return;
    }


    /**
     *  Adds a start date term.
     *
     *  @param start start of date range
     *  @param end end of date range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> or is <code>null</code>
     */

    protected void addStartDateTerm(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                    boolean match) {
        this.inspector.addStartDateTerm(start, end, match);
        return;
    }


    /**
     *  Gets the end date query terms.
     *
     *  @return the end date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (this.inspector.getEndDateTerms());
    }

    
    /**
     *  Adds an end date term.
     *
     *  @param term an end date
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEndDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
        this.inspector.addEndDateTerm(term);
        return;
    }


    /**
     *  Adds a collection of end date terms.
     *
     *  @param terms a collection of end date terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addEndDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
        this.inspector.addEndDateTerms(terms);
        return;
    }


    /**
     *  Adds an end date term.
     *
     *  @param start start of date range
     *  @param end end of date range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> or is <code>null</code>
     */

    protected void addEndDateTerm(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                    boolean match) {
        this.inspector.addEndDateTerm(start, end, match);
        return;
    }


    /**
     *  Gets the date query terms.
     *
     *  @return the date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (this.inspector.getDateTerms());
    }

    
    /**
     *  Adds a date term.
     *
     *  @param term a date term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
        this.inspector.addDateTerm(term);
        return;
    }


    /**
     *  Adds a collection of date terms.
     *
     *  @param terms a collection of date terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
        this.inspector.addDateTerms(terms);
        return;
    }


    /**
     *  Adds a date term.
     *
     *  @param from start date
     *  @param to end date
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    protected void addDateTerm(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, 
                               boolean match) {
        this.inspector.addDateTerm(from, to, match);
        return;
    }

    
    protected class OsidTemporalQueryInspector
        extends AbstractOsidTemporalQueryInspector
        implements org.osid.OsidTemporalQueryInspector {


        /**
         *  Adds an effective term.
         *
         *  @param term an effective term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addEffectiveTerm(org.osid.search.terms.BooleanTerm term) {
            super.addEffectiveTerm(term);
            return;
        }
        

        /**
         *  Adds a collection of effective terms.
         *
         *  @param terms a collection of effective terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is
         *          <code>null</code>
         */
        
        @Override
        protected void addEffectiveTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
            super.addEffectiveTerms(terms);
            return;
        }
        
        
        /**
         *  Adds an effective term.
         *
         *  @param effective flag
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         */
        
        @Override
        protected void addEffectiveTerm(boolean effective, boolean match) {
            super.addEffectiveTerm(effective, match);
            return;
        }
        
    
        /**
         *  Adds a start date term.
         *
         *  @param term a start date
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addStartDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
            super.addStartDateTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of start date terms.
         *
         *  @param terms a collection of start date terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addStartDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
            super.addStartDateTerms(terms);
            return;
        }
        
        
        /**
         *  Adds a start date term.
         *
         *  @param start start of date range
         *  @param end end of date range
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.NullArgumentException <code>start</code> or
         *          <code>end</code> or is <code>null</code>
         */
        
        @Override
        protected void addStartDateTerm(org.osid.calendaring.DateTime start, 
                                        org.osid.calendaring.DateTime end, 
                                        boolean match) {
            super.addStartDateTerm(start, end, match);
            return;
        }
        
            
        /**
         *  Adds an end date term.
         *
         *  @param term an end date
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addEndDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
            super.addEndDateTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of end date terms.
         *
         *  @param terms a collection of end date terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addEndDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
            super.addEndDateTerms(terms);
            return;
        }
        
        
        /**
         *  Adds an end date term.
         *
         *  @param start start of date range
         *  @param end end of date range
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.NullArgumentException <code>start</code> or
         *          <code>end</code> or is <code>null</code>
         */

        @Override
        protected void addEndDateTerm(org.osid.calendaring.DateTime start, 
                                      org.osid.calendaring.DateTime end, 
                                      boolean match) {
            super.addEndDateTerm(start, end, match);
            return;
        }
        

        /**
         *  Adds a date term.
         *
         *  @param term a date term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
            super.addDateTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of date terms.
         *
         *  @param terms a collection of date terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
            super.addDateTerms(terms);
            return;
        }
        
        
        /**
         *  Adds a date term.
         *
         *  @param from start date
         *  @param to end date
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.InvalidArgumentException <code>from</code>
         *          is greater than <code>to</code>
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */
        
        @Override
        protected void addDateTerm(org.osid.calendaring.DateTime from,
                                   org.osid.calendaring.DateTime to, boolean match) {
            super.addDateTerm(from, to, match);
            return;
        }
    }
}

