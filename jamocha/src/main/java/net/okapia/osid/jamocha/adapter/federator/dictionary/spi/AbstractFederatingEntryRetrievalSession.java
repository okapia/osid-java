//
// AbstractFederatingEntryRetrievalSession.java
//
//     An abstract federating adapter for an EntryRetrievalSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  EntryRetrievalSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingEntryRetrievalSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.dictionary.EntryRetrievalSession>
    implements org.osid.dictionary.EntryRetrievalSession {

    private boolean parallel = false;
    private org.osid.dictionary.Dictionary dictionary = new net.okapia.osid.jamocha.nil.dictionary.dictionary.UnknownDictionary();


    /**
     *  Constructs a new
     *  <code>AbstractFederatingEntryRetrievalSession</code>.
     */

    protected AbstractFederatingEntryRetrievalSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.dictionary.EntryRetrievalSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Dictionary/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Dictionary Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDictionaryId() {
        return (this.dictionary.getId());
    }


    /**
     *  Gets the <code>Dictionary</code> associated with this 
     *  session.
     *
     *  @return the <code>Dictionary</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Dictionary getDictionary()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.dictionary);
    }


    /**
     *  Sets the <code>Dictionary</code>.
     *
     *  @param  dictionary the dictionary for this session
     *  @throws org.osid.NullArgumentException <code>dictionary</code>
     *          is <code>null</code>
     */

    protected void setDictionary(org.osid.dictionary.Dictionary dictionary) {
        nullarg(dictionary, "dictionary");
        this.dictionary = dictionary;
        return;
    }


    /**
     *  Tests if this user can perform <code>Entry</code> 
     *  retrievals.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEntries() {
        for (org.osid.dictionary.EntryRetrievalSession session : getSessions()) {
            if (session.canLookupEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in dictionaries which are children
     *  of this dictionary in the dictionary hierarchy.
     */

    @OSID @Override
    public void useFederatedDictionaryView() {
        for (org.osid.dictionary.EntryRetrievalSession session : getSessions()) {
            session.useFederatedDictionaryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts retrievals to this dictionary only.
     */

    @OSID @Override
    public void useIsolatedDictionaryView() {
        for (org.osid.dictionary.EntryRetrievalSession session : getSessions()) {
            session.useIsolatedDictionaryView();
        }

        return;
    }


    /**
     *  Gets the <code>Dictionary</code> entry associated with the
     *  given key and types. The <code>keyType</code> indicates the
     *  key object type and the <code>valueType</code> indicates the
     *  value object return.
     *
     *  @param  key the key of the entry to retrieve
     *  @param  keyType the key type of the entry to retrieve
     *  @param  valueType the value type of the entry to retrieve
     *  @return the returned <code> object </code>
     *  @throws org.osid.InvalidArgumentException <code>key</code> is
     *          not of <code>keyType</code>
     *  @throws org.osid.NotFoundException no entry found
     *  @throws org.osid.NullArgumentException <code> key, keyType
     *          </code> or <code>valueType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public java.lang.Object retrieveEntry(java.lang.Object key,
                                          org.osid.type.Type keyType,
                                          org.osid.type.Type valueType) 
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.dictionary.EntryRetrievalSession session : getSessions()) {
            try {
                return (session.retrieveEntry(key, keyType, valueType));
            } catch (org.osid.NotFoundException | org.osid.UnsupportedException nue) {
                continue;
            }
        }

        throw new org.osid.NotFoundException("entry  not found");
    }


    protected net.okapia.osid.jamocha.adapter.federator.dictionary.entry.FederatingEntryList getEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.dictionary.entry.ParallelEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.dictionary.entry.CompositeEntryList());
        }
    }
}
