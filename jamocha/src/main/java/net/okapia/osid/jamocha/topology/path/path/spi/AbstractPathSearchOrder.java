//
// AbstractPathSearchOdrer.java
//
//     Defines a PathSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.path.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code PathSearchOrder}.
 */

public abstract class AbstractPathSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.topology.path.PathSearchOrder {

    private final java.util.Collection<org.osid.topology.path.records.PathSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the starting 
     *  node. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartingNode(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a starting node search order is available. 
     *
     *  @return <code> true </code> if a node search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartingNodeSearchOrder() {
        return (false);
    }


    /**
     *  Gets a starting node search order. 
     *
     *  @return a node search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartingNodeSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchOrder getStartingNodeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStartingNodeSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the starting 
     *  node. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndingNode(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an ending node search order is available. 
     *
     *  @return <code> true </code> if a node search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndingNodeSearchOrder() {
        return (false);
    }


    /**
     *  Gets an ending node search order. 
     *
     *  @return a node search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndingNodeSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchOrder getEndingNodeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEndingNodeSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the complete 
     *  state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByComplete(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the open and 
     *  closed paths. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosed(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the number of 
     *  hops in the path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByHops(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the path 
     *  distance. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the path cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCost(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  pathRecordType a path record type 
     *  @return {@code true} if the pathRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code pathRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type pathRecordType) {
        for (org.osid.topology.path.records.PathSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(pathRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  pathRecordType the path record type 
     *  @return the path search order record
     *  @throws org.osid.NullArgumentException
     *          {@code pathRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(pathRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.topology.path.records.PathSearchOrderRecord getPathSearchOrderRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.path.records.PathSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this path. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param pathRecord the path search odrer record
     *  @param pathRecordType path record type
     *  @throws org.osid.NullArgumentException
     *          {@code pathRecord} or
     *          {@code pathRecordTypepath} is
     *          {@code null}
     */
            
    protected void addPathRecord(org.osid.topology.path.records.PathSearchOrderRecord pathSearchOrderRecord, 
                                     org.osid.type.Type pathRecordType) {

        addRecordType(pathRecordType);
        this.records.add(pathSearchOrderRecord);
        
        return;
    }
}
