//
// AbstractAdapterCheckLookupSession.java
//
//    A Check lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.rules.check.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Check lookup session adapter.
 */

public abstract class AbstractAdapterCheckLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.rules.check.CheckLookupSession {

    private final org.osid.rules.check.CheckLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCheckLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCheckLookupSession(org.osid.rules.check.CheckLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Engine/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Engine Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.session.getEngineId());
    }


    /**
     *  Gets the {@code Engine} associated with this session.
     *
     *  @return the {@code Engine} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getEngine());
    }


    /**
     *  Tests if this user can perform {@code Check} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupChecks() {
        return (this.session.canLookupChecks());
    }


    /**
     *  A complete view of the {@code Check} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCheckView() {
        this.session.useComparativeCheckView();
        return;
    }


    /**
     *  A complete view of the {@code Check} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCheckView() {
        this.session.usePlenaryCheckView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include checks in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.session.useFederatedEngineView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.session.useIsolatedEngineView();
        return;
    }
    

    /**
     *  Only active checks are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCheckView() {
        this.session.useActiveCheckView();
        return;
    }


    /**
     *  Active and inactive checks are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCheckView() {
        this.session.useAnyStatusCheckView();
        return;
    }
    
     
    /**
     *  Gets the {@code Check} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Check} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Check} and
     *  retained for compatibility.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param checkId {@code Id} of the {@code Check}
     *  @return the check
     *  @throws org.osid.NotFoundException {@code checkId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code checkId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Check getCheck(org.osid.id.Id checkId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCheck(checkId));
    }


    /**
     *  Gets a {@code CheckList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  checks specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Checks} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param  checkIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Check} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code checkIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByIds(org.osid.id.IdList checkIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecksByIds(checkIds));
    }


    /**
     *  Gets a {@code CheckList} corresponding to the given
     *  check genus {@code Type} which does not include
     *  checks of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  checks or an error results. Otherwise, the returned list
     *  may contain only those checks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param  checkGenusType a check genus type 
     *  @return the returned {@code Check} list
     *  @throws org.osid.NullArgumentException
     *          {@code checkGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByGenusType(org.osid.type.Type checkGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecksByGenusType(checkGenusType));
    }


    /**
     *  Gets a {@code CheckList} corresponding to the given
     *  check genus {@code Type} and include any additional
     *  checks with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  checks or an error results. Otherwise, the returned list
     *  may contain only those checks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param  checkGenusType a check genus type 
     *  @return the returned {@code Check} list
     *  @throws org.osid.NullArgumentException
     *          {@code checkGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByParentGenusType(org.osid.type.Type checkGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecksByParentGenusType(checkGenusType));
    }


    /**
     *  Gets a {@code CheckList} containing the given
     *  check record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  checks or an error results. Otherwise, the returned list
     *  may contain only those checks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param  checkRecordType a check record type 
     *  @return the returned {@code Check} list
     *  @throws org.osid.NullArgumentException
     *          {@code checkRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByRecordType(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecksByRecordType(checkRecordType));
    }


    /**
     *  Gets all {@code Checks}. 
     *
     *  In plenary mode, the returned list contains all known
     *  checks or an error results. Otherwise, the returned list
     *  may contain only those checks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @return a list of {@code Checks} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecks());
    }
}
