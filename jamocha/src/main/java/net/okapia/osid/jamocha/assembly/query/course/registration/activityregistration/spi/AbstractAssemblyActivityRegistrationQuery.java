//
// AbstractAssemblyActivityRegistrationQuery.java
//
//     An ActivityRegistrationQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.registration.activityregistration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActivityRegistrationQuery that stores terms.
 */

public abstract class AbstractAssemblyActivityRegistrationQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.registration.ActivityRegistrationQuery,
               org.osid.course.registration.ActivityRegistrationQueryInspector,
               org.osid.course.registration.ActivityRegistrationSearchOrder {

    private final java.util.Collection<org.osid.course.registration.records.ActivityRegistrationQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.registration.records.ActivityRegistrationQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.registration.records.ActivityRegistrationSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyActivityRegistrationQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyActivityRegistrationQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the registration <code> Id </code> for this query. 
     *
     *  @param  registrationId a registration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> registrationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRegistrationId(org.osid.id.Id registrationId, 
                                    boolean match) {
        getAssembler().addIdTerm(getRegistrationIdColumn(), registrationId, match);
        return;
    }


    /**
     *  Clears the registration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRegistrationIdTerms() {
        getAssembler().clearTerms(getRegistrationIdColumn());
        return;
    }


    /**
     *  Gets the registration <code> Id </code> query terms. 
     *
     *  @return the registration <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRegistrationIdTerms() {
        return (getAssembler().getIdTerms(getRegistrationIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by registration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRegistration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRegistrationColumn(), style);
        return;
    }


    /**
     *  Gets the RegistrationId column name.
     *
     * @return the column name
     */

    protected String getRegistrationIdColumn() {
        return ("registration_id");
    }


    /**
     *  Tests if a <code> RegistrationQuery </code> is available. 
     *
     *  @return <code> true </code> if a registration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a registration. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the registration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuery getRegistrationQuery() {
        throw new org.osid.UnimplementedException("supportsRegistrationQuery() is false");
    }


    /**
     *  Clears the registration terms. 
     */

    @OSID @Override
    public void clearRegistrationTerms() {
        getAssembler().clearTerms(getRegistrationColumn());
        return;
    }


    /**
     *  Gets the registration query terms. 
     *
     *  @return the registration query terms 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQueryInspector[] getRegistrationTerms() {
        return (new org.osid.course.registration.RegistrationQueryInspector[0]);
    }


    /**
     *  Tests if a regiistration search order is available. 
     *
     *  @return <code> true </code> if a registration search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the registration search order. 
     *
     *  @return the registration search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSearchOrder getRegistrationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRegistrationSearchOrder() is false");
    }


    /**
     *  Gets the Registration column name.
     *
     * @return the column name
     */

    protected String getRegistrationColumn() {
        return ("registration");
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by activity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActivityColumn(), style);
        return;
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Tests if an activity search order is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the activity search order. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchOrder getActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivitySearchOrder() is false");
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Sets the student resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getStudentIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the student resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        getAssembler().clearTerms(getStudentIdColumn());
        return;
    }


    /**
     *  Gets the student <code> Id </code> query terms. 
     *
     *  @return the student <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (getAssembler().getIdTerms(getStudentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by student. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStudentColumn(), style);
        return;
    }


    /**
     *  Gets the StudentId column name.
     *
     * @return the column name
     */

    protected String getStudentIdColumn() {
        return ("student_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student resource terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        getAssembler().clearTerms(getStudentColumn());
        return;
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the student query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a student search order is available. 
     *
     *  @return <code> true </code> if a student search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the student search order. 
     *
     *  @return the student search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Gets the Student column name.
     *
     * @return the column name
     */

    protected String getStudentColumn() {
        return ("student");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this activityRegistration supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityRegistrationRecordType an activity registration record type 
     *  @return <code>true</code> if the activityRegistrationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRegistrationRecordType) {
        for (org.osid.course.registration.records.ActivityRegistrationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  activityRegistrationRecordType the activity registration record type 
     *  @return the activity registration query record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRegistrationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationQueryRecord getActivityRegistrationQueryRecord(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityRegistrationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRegistrationRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  activityRegistrationRecordType the activity registration record type 
     *  @return the activity registration query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRegistrationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationQueryInspectorRecord getActivityRegistrationQueryInspectorRecord(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityRegistrationQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRegistrationRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param activityRegistrationRecordType the activity registration record type
     *  @return the activity registration search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRegistrationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationSearchOrderRecord getActivityRegistrationSearchOrderRecord(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityRegistrationSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRegistrationRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this activity registration. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityRegistrationQueryRecord the activity registration query record
     *  @param activityRegistrationQueryInspectorRecord the activity registration query inspector
     *         record
     *  @param activityRegistrationSearchOrderRecord the activity registration search order record
     *  @param activityRegistrationRecordType activity registration record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationQueryRecord</code>,
     *          <code>activityRegistrationQueryInspectorRecord</code>,
     *          <code>activityRegistrationSearchOrderRecord</code> or
     *          <code>activityRegistrationRecordTypeactivityRegistration</code> is
     *          <code>null</code>
     */
            
    protected void addActivityRegistrationRecords(org.osid.course.registration.records.ActivityRegistrationQueryRecord activityRegistrationQueryRecord, 
                                      org.osid.course.registration.records.ActivityRegistrationQueryInspectorRecord activityRegistrationQueryInspectorRecord, 
                                      org.osid.course.registration.records.ActivityRegistrationSearchOrderRecord activityRegistrationSearchOrderRecord, 
                                      org.osid.type.Type activityRegistrationRecordType) {

        addRecordType(activityRegistrationRecordType);

        nullarg(activityRegistrationQueryRecord, "activity registration query record");
        nullarg(activityRegistrationQueryInspectorRecord, "activity registration query inspector record");
        nullarg(activityRegistrationSearchOrderRecord, "activity registration search odrer record");

        this.queryRecords.add(activityRegistrationQueryRecord);
        this.queryInspectorRecords.add(activityRegistrationQueryInspectorRecord);
        this.searchOrderRecords.add(activityRegistrationSearchOrderRecord);
        
        return;
    }
}
