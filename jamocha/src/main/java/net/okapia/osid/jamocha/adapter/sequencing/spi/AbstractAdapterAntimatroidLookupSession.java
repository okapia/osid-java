//
// AbstractAdapterAntimatroidLookupSession.java
//
//    An Antimatroid lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.sequencing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Antimatroid lookup session adapter.
 */

public abstract class AbstractAdapterAntimatroidLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.sequencing.AntimatroidLookupSession {

    private final org.osid.sequencing.AntimatroidLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAntimatroidLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAntimatroidLookupSession(org.osid.sequencing.AntimatroidLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Antimatroid} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAntimatroids() {
        return (this.session.canLookupAntimatroids());
    }


    /**
     *  A complete view of the {@code Antimatroid} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAntimatroidView() {
        this.session.useComparativeAntimatroidView();
        return;
    }


    /**
     *  A complete view of the {@code Antimatroid} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAntimatroidView() {
        this.session.usePlenaryAntimatroidView();
        return;
    }

     
    /**
     *  Gets the {@code Antimatroid} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Antimatroid} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Antimatroid} and
     *  retained for compatibility.
     *
     *  @param antimatroidId {@code Id} of the {@code Antimatroid}
     *  @return the antimatroid
     *  @throws org.osid.NotFoundException {@code antimatroidId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code antimatroidId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.Antimatroid getAntimatroid(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAntimatroid(antimatroidId));
    }


    /**
     *  Gets an {@code AntimatroidList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  antimatroids specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Antimatroids} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  antimatroidIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Antimatroid} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code antimatroidIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByIds(org.osid.id.IdList antimatroidIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAntimatroidsByIds(antimatroidIds));
    }


    /**
     *  Gets an {@code AntimatroidList} corresponding to the given
     *  antimatroid genus {@code Type} which does not include
     *  antimatroids of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  antimatroids or an error results. Otherwise, the returned list
     *  may contain only those antimatroids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  antimatroidGenusType an antimatroid genus type 
     *  @return the returned {@code Antimatroid} list
     *  @throws org.osid.NullArgumentException
     *          {@code antimatroidGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByGenusType(org.osid.type.Type antimatroidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAntimatroidsByGenusType(antimatroidGenusType));
    }


    /**
     *  Gets an {@code AntimatroidList} corresponding to the given
     *  antimatroid genus {@code Type} and include any additional
     *  antimatroids with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  antimatroids or an error results. Otherwise, the returned list
     *  may contain only those antimatroids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  antimatroidGenusType an antimatroid genus type 
     *  @return the returned {@code Antimatroid} list
     *  @throws org.osid.NullArgumentException
     *          {@code antimatroidGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByParentGenusType(org.osid.type.Type antimatroidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAntimatroidsByParentGenusType(antimatroidGenusType));
    }


    /**
     *  Gets an {@code AntimatroidList} containing the given
     *  antimatroid record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  antimatroids or an error results. Otherwise, the returned list
     *  may contain only those antimatroids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  antimatroidRecordType an antimatroid record type 
     *  @return the returned {@code Antimatroid} list
     *  @throws org.osid.NullArgumentException
     *          {@code antimatroidRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByRecordType(org.osid.type.Type antimatroidRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAntimatroidsByRecordType(antimatroidRecordType));
    }


    /**
     *  Gets an {@code AntimatroidList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  antimatroids or an error results. Otherwise, the returned list
     *  may contain only those antimatroids that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Antimatroid} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAntimatroidsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Antimatroids}. 
     *
     *  In plenary mode, the returned list contains all known
     *  antimatroids or an error results. Otherwise, the returned list
     *  may contain only those antimatroids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Antimatroids} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAntimatroids());
    }
}
