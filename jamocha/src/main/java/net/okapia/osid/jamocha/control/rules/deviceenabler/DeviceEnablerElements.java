//
// DeviceEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.deviceenabler;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class DeviceEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the DeviceEnablerElement Id.
     *
     *  @return the device enabler element Id
     */

    public static org.osid.id.Id getDeviceEnablerEntityId() {
        return (makeEntityId("osid.control.rules.DeviceEnabler"));
    }


    /**
     *  Gets the RuledTriggerId element Id.
     *
     *  @return the RuledTriggerId element Id
     */

    public static org.osid.id.Id getRuledTriggerId() {
        return (makeQueryElementId("osid.control.rules.deviceenabler.RuledTriggerId"));
    }


    /**
     *  Gets the RuledTrigger element Id.
     *
     *  @return the RuledTrigger element Id
     */

    public static org.osid.id.Id getRuledTrigger() {
        return (makeQueryElementId("osid.control.rules.deviceenabler.RuledTrigger"));
    }


    /**
     *  Gets the SystemId element Id.
     *
     *  @return the SystemId element Id
     */

    public static org.osid.id.Id getSystemId() {
        return (makeQueryElementId("osid.control.rules.deviceenabler.SystemId"));
    }


    /**
     *  Gets the System element Id.
     *
     *  @return the System element Id
     */

    public static org.osid.id.Id getSystem() {
        return (makeQueryElementId("osid.control.rules.deviceenabler.System"));
    }
}
