//
// AbstractAssemblyGraphQuery.java
//
//     A GraphQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.topology.graph.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A GraphQuery that stores terms.
 */

public abstract class AbstractAssemblyGraphQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.topology.GraphQuery,
               org.osid.topology.GraphQueryInspector,
               org.osid.topology.GraphSearchOrder {

    private final java.util.Collection<org.osid.topology.records.GraphQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.topology.records.GraphQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.topology.records.GraphSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyGraphQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyGraphQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the node <code> Id </code> for this query to match graphs that 
     *  have a related node. 
     *
     *  @param  nodeId a node <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchNodeId(org.osid.id.Id nodeId, boolean match) {
        getAssembler().addIdTerm(getNodeIdColumn(), nodeId, match);
        return;
    }


    /**
     *  Clears the node <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearNodeIdTerms() {
        getAssembler().clearTerms(getNodeIdColumn());
        return;
    }


    /**
     *  Gets the node <code> Id </code> terms. 
     *
     *  @return the node <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNodeIdTerms() {
        return (getAssembler().getIdTerms(getNodeIdColumn()));
    }


    /**
     *  Gets the NodeId column name.
     *
     * @return the column name
     */

    protected String getNodeIdColumn() {
        return ("node_id");
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a node. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getNodeQuery() {
        throw new org.osid.UnimplementedException("supportsNodeQuery() is false");
    }


    /**
     *  Matches graphs that have any node. 
     *
     *  @param  match <code> true </code> to match graphs with any node, 
     *          <code> false </code> to match graphs with no node 
     */

    @OSID @Override
    public void matchAnyNode(boolean match) {
        getAssembler().addIdWildcardTerm(getNodeColumn(), match);
        return;
    }


    /**
     *  Clears the node terms. 
     */

    @OSID @Override
    public void clearNodeTerms() {
        getAssembler().clearTerms(getNodeColumn());
        return;
    }


    /**
     *  Gets the node terms. 
     *
     *  @return the node terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Gets the Node column name.
     *
     * @return the column name
     */

    protected String getNodeColumn() {
        return ("node");
    }


    /**
     *  Sets the edge <code> Id </code> for this query to match graphs 
     *  containing edges. 
     *
     *  @param  edgeId the edge <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> edgeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEdgeId(org.osid.id.Id edgeId, boolean match) {
        getAssembler().addIdTerm(getEdgeIdColumn(), edgeId, match);
        return;
    }


    /**
     *  Clears the edge <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEdgeIdTerms() {
        getAssembler().clearTerms(getEdgeIdColumn());
        return;
    }


    /**
     *  Gets the edge <code> Id </code> terms. 
     *
     *  @return the edge <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEdgeIdTerms() {
        return (getAssembler().getIdTerms(getEdgeIdColumn()));
    }


    /**
     *  Gets the EdgeId column name.
     *
     * @return the column name
     */

    protected String getEdgeIdColumn() {
        return ("edge_id");
    }


    /**
     *  Tests if an <code> EdgeQuery </code> is available. 
     *
     *  @return <code> true </code> if an edge query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an edge. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the edge query 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuery getEdgeQuery() {
        throw new org.osid.UnimplementedException("supportsEdgeQuery() is false");
    }


    /**
     *  Matches graphs that have any edge. 
     *
     *  @param  match <code> true </code> to match graphs with any edge, 
     *          <code> false </code> to match graphs with no edge 
     */

    @OSID @Override
    public void matchAnyEdge(boolean match) {
        getAssembler().addIdWildcardTerm(getEdgeColumn(), match);
        return;
    }


    /**
     *  Clears the edge terms. 
     */

    @OSID @Override
    public void clearEdgeTerms() {
        getAssembler().clearTerms(getEdgeColumn());
        return;
    }


    /**
     *  Gets the edge terms. 
     *
     *  @return the edge terms 
     */

    @OSID @Override
    public org.osid.topology.EdgeQueryInspector[] getEdgeTerms() {
        return (new org.osid.topology.EdgeQueryInspector[0]);
    }


    /**
     *  Gets the Edge column name.
     *
     * @return the column name
     */

    protected String getEdgeColumn() {
        return ("edge");
    }


    /**
     *  Sets the graph <code> Id </code> for this query to match graphs that 
     *  have the specified graph as an ancestor. 
     *
     *  @param  graphId a graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorGraphId(org.osid.id.Id graphId, boolean match) {
        getAssembler().addIdTerm(getAncestorGraphIdColumn(), graphId, match);
        return;
    }


    /**
     *  Clears the ancestor graph <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorGraphIdTerms() {
        getAssembler().clearTerms(getAncestorGraphIdColumn());
        return;
    }


    /**
     *  Gets the ancestor graph <code> Id </code> terms. 
     *
     *  @return the ancestor graph <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorGraphIdTerms() {
        return (getAssembler().getIdTerms(getAncestorGraphIdColumn()));
    }


    /**
     *  Gets the AncestorGraphId column name.
     *
     * @return the column name
     */

    protected String getAncestorGraphIdColumn() {
        return ("ancestor_graph_id");
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorGraphQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getAncestorGraphQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorGraphQuery() is false");
    }


    /**
     *  Matches graphs with any ancestor. 
     *
     *  @param  match <code> true </code> to match graphs with any ancestor, 
     *          <code> false </code> to match root graphs 
     */

    @OSID @Override
    public void matchAnyAncestorGraph(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorGraphColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor graph terms. 
     */

    @OSID @Override
    public void clearAncestorGraphTerms() {
        getAssembler().clearTerms(getAncestorGraphColumn());
        return;
    }


    /**
     *  Gets the ancestor graph terms. 
     *
     *  @return the ancestor graph terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getAncestorGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }


    /**
     *  Gets the AncestorGraph column name.
     *
     * @return the column name
     */

    protected String getAncestorGraphColumn() {
        return ("ancestor_graph");
    }


    /**
     *  Sets the graph <code> Id </code> for this query to match graphs that 
     *  have the specified graph as a descendant. 
     *
     *  @param  graphId a graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantGraphId(org.osid.id.Id graphId, boolean match) {
        getAssembler().addIdTerm(getDescendantGraphIdColumn(), graphId, match);
        return;
    }


    /**
     *  Clears the descendant graph <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantGraphIdTerms() {
        getAssembler().clearTerms(getDescendantGraphIdColumn());
        return;
    }


    /**
     *  Gets the descendant graph <code> Id </code> terms. 
     *
     *  @return the descendant graph <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantGraphIdTerms() {
        return (getAssembler().getIdTerms(getDescendantGraphIdColumn()));
    }


    /**
     *  Gets the DescendantGraphId column name.
     *
     * @return the column name
     */

    protected String getDescendantGraphIdColumn() {
        return ("descendant_graph_id");
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantGraphQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getDescendantGraphQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantGraphQuery() is false");
    }


    /**
     *  Matches graphs with any descendant. 
     *
     *  @param  match <code> true </code> to match graphs with any descendant, 
     *          <code> false </code> to match leaf graphs 
     */

    @OSID @Override
    public void matchAnyDescendantGraph(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantGraphColumn(), match);
        return;
    }


    /**
     *  Clears the descendant graph terms. 
     */

    @OSID @Override
    public void clearDescendantGraphTerms() {
        getAssembler().clearTerms(getDescendantGraphColumn());
        return;
    }


    /**
     *  Gets the descendant graph terms. 
     *
     *  @return the descendant graph terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getDescendantGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }


    /**
     *  Gets the DescendantGraph column name.
     *
     * @return the column name
     */

    protected String getDescendantGraphColumn() {
        return ("descendant_graph");
    }


    /**
     *  Tests if this graph supports the given record
     *  <code>Type</code>.
     *
     *  @param  graphRecordType a graph record type 
     *  @return <code>true</code> if the graphRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type graphRecordType) {
        for (org.osid.topology.records.GraphQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(graphRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  graphRecordType the graph record type 
     *  @return the graph query record 
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(graphRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.GraphQueryRecord getGraphQueryRecord(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.GraphQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(graphRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(graphRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  graphRecordType the graph record type 
     *  @return the graph query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(graphRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.GraphQueryInspectorRecord getGraphQueryInspectorRecord(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.GraphQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(graphRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(graphRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param graphRecordType the graph record type
     *  @return the graph search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(graphRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.GraphSearchOrderRecord getGraphSearchOrderRecord(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.GraphSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(graphRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(graphRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this graph. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param graphQueryRecord the graph query record
     *  @param graphQueryInspectorRecord the graph query inspector
     *         record
     *  @param graphSearchOrderRecord the graph search order record
     *  @param graphRecordType graph record type
     *  @throws org.osid.NullArgumentException
     *          <code>graphQueryRecord</code>,
     *          <code>graphQueryInspectorRecord</code>,
     *          <code>graphSearchOrderRecord</code> or
     *          <code>graphRecordTypegraph</code> is
     *          <code>null</code>
     */
            
    protected void addGraphRecords(org.osid.topology.records.GraphQueryRecord graphQueryRecord, 
                                      org.osid.topology.records.GraphQueryInspectorRecord graphQueryInspectorRecord, 
                                      org.osid.topology.records.GraphSearchOrderRecord graphSearchOrderRecord, 
                                      org.osid.type.Type graphRecordType) {

        addRecordType(graphRecordType);

        nullarg(graphQueryRecord, "graph query record");
        nullarg(graphQueryInspectorRecord, "graph query inspector record");
        nullarg(graphSearchOrderRecord, "graph search odrer record");

        this.queryRecords.add(graphQueryRecord);
        this.queryInspectorRecords.add(graphQueryInspectorRecord);
        this.searchOrderRecords.add(graphSearchOrderRecord);
        
        return;
    }
}
