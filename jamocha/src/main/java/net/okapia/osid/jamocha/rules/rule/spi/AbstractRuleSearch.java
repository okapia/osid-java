//
// AbstractRuleSearch.java
//
//     A template for making a Rule Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.rule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing rule searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRuleSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.rules.RuleSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.rules.records.RuleSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.rules.RuleSearchOrder ruleSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of rules. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  ruleIds list of rules
     *  @throws org.osid.NullArgumentException
     *          <code>ruleIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRules(org.osid.id.IdList ruleIds) {
        while (ruleIds.hasNext()) {
            try {
                this.ids.add(ruleIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRules</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of rule Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRuleIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  ruleSearchOrder rule search order 
     *  @throws org.osid.NullArgumentException
     *          <code>ruleSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>ruleSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRuleResults(org.osid.rules.RuleSearchOrder ruleSearchOrder) {
	this.ruleSearchOrder = ruleSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.rules.RuleSearchOrder getRuleSearchOrder() {
	return (this.ruleSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given rule search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a rule implementing the requested record.
     *
     *  @param ruleSearchRecordType a rule search record
     *         type
     *  @return the rule search record
     *  @throws org.osid.NullArgumentException
     *          <code>ruleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ruleSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.records.RuleSearchRecord getRuleSearchRecord(org.osid.type.Type ruleSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.rules.records.RuleSearchRecord record : this.records) {
            if (record.implementsRecordType(ruleSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ruleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this rule search. 
     *
     *  @param ruleSearchRecord rule search record
     *  @param ruleSearchRecordType rule search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRuleSearchRecord(org.osid.rules.records.RuleSearchRecord ruleSearchRecord, 
                                           org.osid.type.Type ruleSearchRecordType) {

        addRecordType(ruleSearchRecordType);
        this.records.add(ruleSearchRecord);        
        return;
    }
}
