//
// AbstractValueValidator.java
//
//     Validates a Value.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.configuration.value.spi;


/**
 *  Validates a Value.
 */

public abstract class AbstractValueValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOperableOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractValueValidator</code>.
     */

    protected AbstractValueValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractValueValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractValueValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Value.
     *
     *  @param value a value to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>value</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.configuration.Value value) {
        super.validate(value);

        testNestedObject(value, "getParameter");
        org.osid.Syntax syntax = null;
        try {
            syntax = value.getParameter().getValueSyntax();
        } catch (org.osid.OperationFailedException oe) {
            throw new org.osid.OsidRuntimeException(oe);
        }

        testConditionalMethod(value, "getBooleanValue", syntax == org.osid.Syntax.BOOLEAN, "BOOLEAN");
        testConditionalMethod(value, "getBytesValue", syntax == org.osid.Syntax.BYTE, "BYTE");
        testConditionalMethod(value, "getCardinalValue", syntax == org.osid.Syntax.CARDINAL, "CARDINAL");
        testConditionalMethod(value, "getCoordinateValue", syntax == org.osid.Syntax.COORDINATE, "COORDINATE");
        testConditionalMethod(value, "getCurrencyValue", syntax == org.osid.Syntax.CURRENCY, "CURRENCY");
        testConditionalMethod(value, "getDateTimeValue", syntax == org.osid.Syntax.DATETIME, "DATETIME");
        testConditionalMethod(value, "getDecimalValue", syntax == org.osid.Syntax.DECIMAL, "DECIMAL");
        testConditionalMethod(value, "getDistanceValue", syntax == org.osid.Syntax.DISTANCE, "DISTANCE");
        testConditionalMethod(value, "getDurationValue", syntax == org.osid.Syntax.DURATION, "DURATION");
        testConditionalMethod(value, "getHeadingValue", syntax == org.osid.Syntax.HEADING, "HEADING");
        testConditionalMethod(value, "getIdValue", syntax == org.osid.Syntax.ID, "ID");
        testConditionalMethod(value, "getIntegerValue", syntax == org.osid.Syntax.INTEGER, "INTEGER");
        testConditionalMethod(value, "getObjectValue", syntax == org.osid.Syntax.OBJECT, "OBJECT");
        testConditionalMethod(value, "getSpatialUnitValue", syntax == org.osid.Syntax.SPATIALUNIT, "SPATIALUNIT");
        testConditionalMethod(value, "getSpeedValue", syntax == org.osid.Syntax.SPEED, "SPEED");
        testConditionalMethod(value, "getStringValue", syntax == org.osid.Syntax.STRING, "STRING");
        testConditionalMethod(value, "getTimeValue", syntax == org.osid.Syntax.TIME, "TIME");
        testConditionalMethod(value, "getTypeValue", syntax == org.osid.Syntax.TYPE, "TYPE");
        testConditionalMethod(value, "getVersionValue", syntax == org.osid.Syntax.VERSION, "VERSION");

        return;
    }
}
