//
// AbstractTrackingBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTrackingBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.tracking.batch.TrackingBatchManager,
               org.osid.tracking.batch.TrackingBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractTrackingBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTrackingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of issues is available. 
     *
     *  @return <code> true </code> if a issue bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of subtasks is available. 
     *
     *  @return <code> true </code> if a subtask issue bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubtaskIssueBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of queues is available. 
     *
     *  @return <code> true </code> if a queue bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of front offices is available. 
     *
     *  @return <code> true </code> if a front office bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service. 
     *
     *  @return a <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.IssueBatchAdminSession getIssueBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchManager.getIssueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.IssueBatchAdminSession getIssueBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchProxyManager.getIssueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.IssueBatchAdminSession getIssueBatchAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchManager.getIssueBatchAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.IssueBatchAdminSession getIssueBatchAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchProxyManager.getIssueBatchAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subtask 
     *  administration service. 
     *
     *  @return a <code> SubtaskBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.SubtaskIssueBatchAdminSession getSubtaskIssueBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchManager.getSubtaskIssueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subtask 
     *  issue administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.SubtaskIssueBatchAdminSession getSubtaskIssueBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchProxyManager.getSubtaskIssueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subtask 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> SubtaskBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.SubtaskIssueBatchAdminSession getSubtaskIssueBatchAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchManager.getSubtaskIssueBatchAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subtask 
     *  issue administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.SubtaskIssueBatchAdminSession getSubtaskIssueBatchAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchProxyManager.getSubtaskIssueBatchAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service. 
     *
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.QueueBatchAdminSession getQueueBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchManager.getQueueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.QueueBatchAdminSession getQueueBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchProxyManager.getQueueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.QueueBatchAdminSession getQueueBatchAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchManager.getQueueBatchAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.QueueBatchAdminSession getQueueBatchAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchProxyManager.getQueueBatchAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk front 
     *  office administration service. 
     *
     *  @return a <code> FrontOfficeBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.FrontOfficeBatchAdminSession getFrontOfficeBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchManager.getFrontOfficeBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk front 
     *  office administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.batch.FrontOfficeBatchAdminSession getFrontOfficeBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.batch.TrackingBatchProxyManager.getFrontOfficeBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
