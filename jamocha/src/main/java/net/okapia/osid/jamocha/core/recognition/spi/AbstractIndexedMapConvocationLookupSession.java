//
// AbstractIndexedMapConvocationLookupSession.java
//
//    A simple framework for providing a Convocation lookup service
//    backed by a fixed collection of convocations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Convocation lookup service backed by a
 *  fixed collection of convocations. The convocations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some convocations may be compatible
 *  with more types than are indicated through these convocation
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Convocations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapConvocationLookupSession
    extends AbstractMapConvocationLookupSession
    implements org.osid.recognition.ConvocationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.recognition.Convocation> convocationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recognition.Convocation>());
    private final MultiMap<org.osid.type.Type, org.osid.recognition.Convocation> convocationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recognition.Convocation>());


    /**
     *  Makes a <code>Convocation</code> available in this session.
     *
     *  @param  convocation a convocation
     *  @throws org.osid.NullArgumentException <code>convocation<code> is
     *          <code>null</code>
     */

    @Override
    protected void putConvocation(org.osid.recognition.Convocation convocation) {
        super.putConvocation(convocation);

        this.convocationsByGenus.put(convocation.getGenusType(), convocation);
        
        try (org.osid.type.TypeList types = convocation.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.convocationsByRecord.put(types.getNextType(), convocation);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a convocation from this session.
     *
     *  @param convocationId the <code>Id</code> of the convocation
     *  @throws org.osid.NullArgumentException <code>convocationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeConvocation(org.osid.id.Id convocationId) {
        org.osid.recognition.Convocation convocation;
        try {
            convocation = getConvocation(convocationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.convocationsByGenus.remove(convocation.getGenusType());

        try (org.osid.type.TypeList types = convocation.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.convocationsByRecord.remove(types.getNextType(), convocation);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeConvocation(convocationId);
        return;
    }


    /**
     *  Gets a <code>ConvocationList</code> corresponding to the given
     *  convocation genus <code>Type</code> which does not include
     *  convocations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known convocations or an error results. Otherwise,
     *  the returned list may contain only those convocations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  convocationGenusType a convocation genus type 
     *  @return the returned <code>Convocation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>convocationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByGenusType(org.osid.type.Type convocationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.convocation.ArrayConvocationList(this.convocationsByGenus.get(convocationGenusType)));
    }


    /**
     *  Gets a <code>ConvocationList</code> containing the given
     *  convocation record <code>Type</code>. In plenary mode, the
     *  returned list contains all known convocations or an error
     *  results. Otherwise, the returned list may contain only those
     *  convocations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  convocationRecordType a convocation record type 
     *  @return the returned <code>convocation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByRecordType(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.convocation.ArrayConvocationList(this.convocationsByRecord.get(convocationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.convocationsByGenus.clear();
        this.convocationsByRecord.clear();

        super.close();

        return;
    }
}
