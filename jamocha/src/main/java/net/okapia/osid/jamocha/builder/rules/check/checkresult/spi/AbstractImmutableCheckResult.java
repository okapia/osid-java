//
// AbstractImmutableCheckResult.java
//
//     Wraps a mutable CheckResult to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.checkresult.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>CheckResult</code> to hide modifiers. This
 *  wrapper provides an immutized CheckResult from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying checkResult whose state changes are visible.
 */

public abstract class AbstractImmutableCheckResult
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableBrowsable
    implements org.osid.rules.check.CheckResult {

    private final org.osid.rules.check.CheckResult checkResult;


    /**
     *  Constructs a new <code>AbstractImmutableCheckResult</code>.
     *
     *  @param checkResult the check result to immutablize
     *  @throws org.osid.NullArgumentException <code>checkResult</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCheckResult(org.osid.rules.check.CheckResult checkResult) {
        super(checkResult);
        this.checkResult = checkResult;
        return;
    }


    /**
     *  Gets the instruction <code> Id </code> for this result. 
     *
     *  @return the instruction <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getInstructionId() {
        return (this.checkResult.getInstructionId());
    }


    /**
     *  Gets the instruction for this result. 
     *
     *  @return the instruction 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.Instruction getInstruction()
        throws org.osid.OperationFailedException {

        return (this.checkResult.getInstruction());
    }


    /**
     *  Tests if the corresponding <code> Check </code> has failed. Failed 
     *  checks resulting in warnings should return <code> false. </code> 
     *
     *  @return <code> true </code> if this check failed, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean hasFailed() {
        return (this.checkResult.hasFailed());
    }


    /**
     *  Tests if the informational message should be displayed as a warning. 
     *
     *  @return <code> true </code> if this check is a warning, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isWarning() {
        return (this.checkResult.isWarning());
    }


    /**
     *  Gets a message which may be the cause of the failure, a warning 
     *  message, or the weather. 
     *
     *  @return a message 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getMessage() {
        return (this.checkResult.getMessage());
    }


    /**
     *  Gets the check result record corresponding to the given <code> 
     *  CheckResult </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  checkResultRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(checkResultRecordType) </code> is <code> true </code> . 
     *
     *  @param  checkResultRecordType the type of check result record to 
     *          retrieve 
     *  @return the check result record 
     *  @throws org.osid.NullArgumentException <code> checkresultRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(checkResultRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckResultRecord getCheckResultRecord(org.osid.type.Type checkResultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.checkResult.getCheckResultRecord(checkResultRecordType));
    }
}

