//
// MutableMapActivityLookupSession
//
//    Implements an Activity lookup service backed by a collection of
//    activities that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning;


/**
 *  Implements an Activity lookup service backed by a collection of
 *  activities. The activities are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of activities can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapActivityLookupSession
    extends net.okapia.osid.jamocha.core.learning.spi.AbstractMapActivityLookupSession
    implements org.osid.learning.ActivityLookupSession {


    /**
     *  Constructs a new {@code MutableMapActivityLookupSession}
     *  with no activities.
     *
     *  @param objectiveBank the objective bank
     *  @throws org.osid.NullArgumentException {@code objectiveBank} is
     *          {@code null}
     */

      public MutableMapActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank) {
        setObjectiveBank(objectiveBank);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapActivityLookupSession} with a
     *  single activity.
     *
     *  @param objectiveBank the objective bank  
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code activity} is {@code null}
     */

    public MutableMapActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                           org.osid.learning.Activity activity) {
        this(objectiveBank);
        putActivity(activity);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapActivityLookupSession}
     *  using an array of activities.
     *
     *  @param objectiveBank the objective bank
     *  @param activities an array of activities
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code activities} is {@code null}
     */

    public MutableMapActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                           org.osid.learning.Activity[] activities) {
        this(objectiveBank);
        putActivities(activities);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapActivityLookupSession}
     *  using a collection of activities.
     *
     *  @param objectiveBank the objective bank
     *  @param activities a collection of activities
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code activities} is {@code null}
     */

    public MutableMapActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                           java.util.Collection<? extends org.osid.learning.Activity> activities) {

        this(objectiveBank);
        putActivities(activities);
        return;
    }

    
    /**
     *  Makes an {@code Activity} available in this session.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException {@code activity{@code  is
     *          {@code null}
     */

    @Override
    public void putActivity(org.osid.learning.Activity activity) {
        super.putActivity(activity);
        return;
    }


    /**
     *  Makes an array of activities available in this session.
     *
     *  @param activities an array of activities
     *  @throws org.osid.NullArgumentException {@code activities{@code 
     *          is {@code null}
     */

    @Override
    public void putActivities(org.osid.learning.Activity[] activities) {
        super.putActivities(activities);
        return;
    }


    /**
     *  Makes collection of activities available in this session.
     *
     *  @param activities a collection of activities
     *  @throws org.osid.NullArgumentException {@code activities{@code  is
     *          {@code null}
     */

    @Override
    public void putActivities(java.util.Collection<? extends org.osid.learning.Activity> activities) {
        super.putActivities(activities);
        return;
    }


    /**
     *  Removes an Activity from this session.
     *
     *  @param activityId the {@code Id} of the activity
     *  @throws org.osid.NullArgumentException {@code activityId{@code 
     *          is {@code null}
     */

    @Override
    public void removeActivity(org.osid.id.Id activityId) {
        super.removeActivity(activityId);
        return;
    }    
}
