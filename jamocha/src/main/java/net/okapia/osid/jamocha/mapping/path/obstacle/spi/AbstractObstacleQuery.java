//
// AbstractObstacleQuery.java
//
//     A template for making an Obstacle Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.obstacle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for obstacles.
 */

public abstract class AbstractObstacleQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQuery
    implements org.osid.mapping.path.ObstacleQuery {

    private final java.util.Collection<org.osid.mapping.path.records.ObstacleQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the path <code> Id </code> for this query to match obstacles 
     *  along the given path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        return;
    }


    /**
     *  Matches obstacles overlapping with the specified <code>
     *  Coordinate. </code>
     *
     *  @param  coordinate a coordinate 
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> coordinate
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the coordinate query terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        return;
    }


    /**
     *  Matches obstacles contained within the specified <code> Coordinates 
     *  </code> on its path inclusive. 
     *
     *  @param spatialUnit a spatial unit
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnit</code> is <code> null </code>
     */

    @OSID @Override
    public void matchContainingSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the spatial unit query terms. 
     */

    @OSID @Override
    public void clearContainingSpatialUnitTerms() {
        return;
    }


    /**
     *  Sets the map <code> Id </code> for this query. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given obstacle query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an obstacle implementing the requested record.
     *
     *  @param obstacleRecordType an obstacle record type
     *  @return the obstacle query record
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(obstacleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.ObstacleQueryRecord getObstacleQueryRecord(org.osid.type.Type obstacleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.ObstacleQueryRecord record : this.records) {
            if (record.implementsRecordType(obstacleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(obstacleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this obstacle query. 
     *
     *  @param obstacleQueryRecord obstacle query record
     *  @param obstacleRecordType obstacle record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObstacleQueryRecord(org.osid.mapping.path.records.ObstacleQueryRecord obstacleQueryRecord, 
                                          org.osid.type.Type obstacleRecordType) {

        addRecordType(obstacleRecordType);
        nullarg(obstacleQueryRecord, "obstacle query record");
        this.records.add(obstacleQueryRecord);        
        return;
    }
}
