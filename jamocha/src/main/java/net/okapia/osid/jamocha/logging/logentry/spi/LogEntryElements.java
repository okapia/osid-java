//
// LogEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.logentry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class LogEntryElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the LogEntryElement Id.
     *
     *  @return the log entry element Id
     */

    public static org.osid.id.Id getLogEntryEntityId() {
        return (makeEntityId("osid.logging.LogEntry"));
    }


    /**
     *  Gets the Priority element Id.
     *
     *  @return the Priority element Id
     */

    public static org.osid.id.Id getPriority() {
        return (makeElementId("osid.logging.logentry.Priority"));
    }


    /**
     *  Gets the Timestamp element Id.
     *
     *  @return the Timestamp element Id
     */

    public static org.osid.id.Id getTimestamp() {
        return (makeElementId("osid.logging.logentry.Timestamp"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.logging.logentry.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.logging.logentry.Resource"));
    }


    /**
     *  Gets the AgentId element Id.
     *
     *  @return the AgentId element Id
     */

    public static org.osid.id.Id getAgentId() {
        return (makeElementId("osid.logging.logentry.AgentId"));
    }


    /**
     *  Gets the Agent element Id.
     *
     *  @return the Agent element Id
     */

    public static org.osid.id.Id getAgent() {
        return (makeElementId("osid.logging.logentry.Agent"));
    }


    /**
     *  Gets the MinimumPriority element Id.
     *
     *  @return the MinimumPriority element Id
     */

    public static org.osid.id.Id getMinimumPriority() {
        return (makeQueryElementId("osid.logging.logentry.MinimumPriority"));
    }


    /**
     *  Gets the LogId element Id.
     *
     *  @return the LogId element Id
     */

    public static org.osid.id.Id getLogId() {
        return (makeQueryElementId("osid.logging.logentry.LogId"));
    }


    /**
     *  Gets the Log element Id.
     *
     *  @return the Log element Id
     */

    public static org.osid.id.Id getLog() {
        return (makeQueryElementId("osid.logging.logentry.Log"));
    }
}
