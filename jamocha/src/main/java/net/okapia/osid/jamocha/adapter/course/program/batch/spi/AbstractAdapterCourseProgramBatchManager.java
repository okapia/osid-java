//
// AbstractCourseProgramBatchManager.java
//
//     An adapter for a CourseProgramBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.program.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseProgramBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseProgramBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.course.program.batch.CourseProgramBatchManager>
    implements org.osid.course.program.batch.CourseProgramBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseProgramBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseProgramBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseProgramBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseProgramBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of programs is available. 
     *
     *  @return <code> true </code> if a program bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramBatchAdmin() {
        return (getAdapteeManager().supportsProgramBatchAdmin());
    }


    /**
     *  Tests if bulk administration of program offerings is available. 
     *
     *  @return <code> true </code> if a program offering bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingBatchAdmin() {
        return (getAdapteeManager().supportsProgramOfferingBatchAdmin());
    }


    /**
     *  Tests if bulk administration of credentials is available. 
     *
     *  @return <code> true </code> if a credential bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialBatchAdmin() {
        return (getAdapteeManager().supportsCredentialBatchAdmin());
    }


    /**
     *  Tests if bulk administration of enrollments is available. 
     *
     *  @return <code> true </code> if an enrollment bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentBatchAdmin() {
        return (getAdapteeManager().supportsEnrollmentBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk program 
     *  administration service. 
     *
     *  @return a <code> ProgramBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.ProgramBatchAdminSession getProgramBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk program 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.ProgramBatchAdminSession getProgramBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk program 
     *  offering administration service. 
     *
     *  @return a <code> ProgramOfferingBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.ProgramOfferingBatchAdminSession getProgramOfferingBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk program 
     *  offering administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.ProgramOfferingBatchAdminSession getProgramOfferingBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  credential administration service. 
     *
     *  @return a <code> CredentialBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.CredentialBatchAdminSession getCredentialBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  credential administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.CredentialBatchAdminSession getCredentialBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  enrollment administration service. 
     *
     *  @return an <code> EnrollmentBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.EnrollmentBatchAdminSession getEnrollmentBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  enrollment administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.EnrollmentBatchAdminSession getEnrollmentBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
