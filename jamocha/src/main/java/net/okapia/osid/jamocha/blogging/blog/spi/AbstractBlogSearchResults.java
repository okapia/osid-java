//
// AbstractBlogSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.blog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBlogSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.blogging.BlogSearchResults {

    private org.osid.blogging.BlogList blogs;
    private final org.osid.blogging.BlogQueryInspector inspector;
    private final java.util.Collection<org.osid.blogging.records.BlogSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBlogSearchResults.
     *
     *  @param blogs the result set
     *  @param blogQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>blogs</code>
     *          or <code>blogQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBlogSearchResults(org.osid.blogging.BlogList blogs,
                                            org.osid.blogging.BlogQueryInspector blogQueryInspector) {
        nullarg(blogs, "blogs");
        nullarg(blogQueryInspector, "blog query inspectpr");

        this.blogs = blogs;
        this.inspector = blogQueryInspector;

        return;
    }


    /**
     *  Gets the blog list resulting from a search.
     *
     *  @return a blog list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogs() {
        if (this.blogs == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.blogging.BlogList blogs = this.blogs;
        this.blogs = null;
	return (blogs);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.blogging.BlogQueryInspector getBlogQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  blog search record <code> Type. </code> This method must
     *  be used to retrieve a blog implementing the requested
     *  record.
     *
     *  @param blogSearchRecordType a blog search 
     *         record type 
     *  @return the blog search
     *  @throws org.osid.NullArgumentException
     *          <code>blogSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(blogSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.BlogSearchResultsRecord getBlogSearchResultsRecord(org.osid.type.Type blogSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.blogging.records.BlogSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(blogSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(blogSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record blog search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBlogRecord(org.osid.blogging.records.BlogSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "blog record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
