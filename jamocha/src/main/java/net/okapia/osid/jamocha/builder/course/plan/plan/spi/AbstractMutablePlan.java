//
// AbstractMutablePlan.java
//
//     Defines a mutable Plan.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.plan.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Plan</code>.
 */

public abstract class AbstractMutablePlan
    extends net.okapia.osid.jamocha.course.plan.plan.spi.AbstractPlan
    implements org.osid.course.plan.Plan,
               net.okapia.osid.jamocha.builder.course.plan.plan.PlanMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this plan. 
     *
     *  @param record plan record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addPlanRecord(org.osid.course.plan.records.PlanRecord record, org.osid.type.Type recordType) {
        super.addPlanRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this plan is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this plan ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this plan.
     *
     *  @param displayName the name for this plan
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this plan.
     *
     *  @param description the description of this plan
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this plan
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the syllabus.
     *
     *  @param syllabus a syllabus
     *  @throws org.osid.NullArgumentException <code>syllabus</code>
     *          is <code>null</code>
     */

    @Override
    public void setSyllabus(org.osid.course.syllabus.Syllabus syllabus) {
        super.setSyllabus(syllabus);
        return;
    }


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    @Override
    public void setCourseOffering(org.osid.course.CourseOffering courseOffering) {
        super.setCourseOffering(courseOffering);
        return;
    }


    /**
     *  Adds a module.
     *
     *  @param module a module
     *  @throws org.osid.NullArgumentException <code>module</code> is
     *          <code>null</code>
     */

    @Override
    public void addModule(org.osid.course.syllabus.Module module) {
        super.addModule(module);
        return;
    }


    /**
     *  Sets all the modules.
     *
     *  @param modules a collection of modules
     *  @throws org.osid.NullArgumentException <code>modules</code> is
     *          <code>null</code>
     */

    @Override
    public void setModules(java.util.Collection<org.osid.course.syllabus.Module> modules) {
        super.setModules(modules);
        return;
    }
}

