//
// AbstractImmutableReply.java
//
//     Wraps a mutable Reply to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.forum.reply.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Reply</code> to hide modifiers. This
 *  wrapper provides an immutized Reply from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying reply whose state changes are visible.
 */

public abstract class AbstractImmutableReply
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableContainableOsidObject
    implements org.osid.forum.Reply {

    private final org.osid.forum.Reply reply;


    /**
     *  Constructs a new <code>AbstractImmutableReply</code>.
     *
     *  @param reply the reply to immutablize
     *  @throws org.osid.NullArgumentException <code>reply</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableReply(org.osid.forum.Reply reply) {
        super(reply);
        this.reply = reply;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the original top level post for this 
     *  reply. 
     *
     *  @return the post <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPostId() {
        return (this.reply.getPostId());
    }


    /**
     *  Gets the original top level post. 
     *
     *  @return the post 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.forum.Post getPost()
        throws org.osid.OperationFailedException {

        return (this.reply.getPost());
    }


    /**
     *  Gets the <code> Ids </code> of the replies to this rpely. 
     *
     *  @return the reply <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getReplyIds() {
        return (this.reply.getReplyIds());
    }


    /**
     *  Gets the replies to this reply. 
     *
     *  @return the replies 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getReplies()
        throws org.osid.OperationFailedException {

        return (this.reply.getReplies());
    }


    /**
     *  Gets the time of this entry. 
     *
     *  @return the time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.reply.getTimestamp());
    }


    /**
     *  Gets the poster resource <code> Id </code> of this entry. 
     *
     *  @return the poster resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPosterId() {
        return (this.reply.getPosterId());
    }


    /**
     *  Gets the posting of this entry. 
     *
     *  @return the poster resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getPoster()
        throws org.osid.OperationFailedException {

        return (this.reply.getPoster());
    }


    /**
     *  Gets the posting <code> Id </code> of this entry. 
     *
     *  @return the posting agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPostingAgentId() {
        return (this.reply.getPostingAgentId());
    }


    /**
     *  Gets the posting of this entry. 
     *
     *  @return the posting agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getPostingAgent()
        throws org.osid.OperationFailedException {

        return (this.reply.getPostingAgent());
    }


    /**
     *  Gets the subject line of this entry. 
     *
     *  @return the subject 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSubjectLine() {
        return (this.reply.getSubjectLine());
    }


    /**
     *  Gets the text of the entry. 
     *
     *  @return the entry text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getText() {
        return (this.reply.getText());
    }


    /**
     *  Gets the reply record corresponding to the given <code> Reply </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> replyRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(replyRecordType) </code> is <code> true </code> . 
     *
     *  @param  replyRecordType the type of reply record to retrieve 
     *  @return the reply record 
     *  @throws org.osid.NullArgumentException <code> replyRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(replyRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.records.ReplyRecord getReplyRecord(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException {

        return (this.reply.getReplyRecord(replyRecordType));
    }
}

