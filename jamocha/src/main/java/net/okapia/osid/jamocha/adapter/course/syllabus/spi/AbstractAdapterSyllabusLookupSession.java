//
// AbstractAdapterSyllabusLookupSession.java
//
//    A Syllabus lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Syllabus lookup session adapter.
 */

public abstract class AbstractAdapterSyllabusLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.syllabus.SyllabusLookupSession {

    private final org.osid.course.syllabus.SyllabusLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSyllabusLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSyllabusLookupSession(org.osid.course.syllabus.SyllabusLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Syllabus} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSyllabi() {
        return (this.session.canLookupSyllabi());
    }


    /**
     *  A complete view of the {@code Syllabus} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSyllabusView() {
        this.session.useComparativeSyllabusView();
        return;
    }


    /**
     *  A complete view of the {@code Syllabus} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySyllabusView() {
        this.session.usePlenarySyllabusView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include syllabi in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    
     
    /**
     *  Gets the {@code Syllabus} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Syllabus} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Syllabus} and
     *  retained for compatibility.
     *
     *  @param syllabusId {@code Id} of the {@code Syllabus}
     *  @return the syllabus
     *  @throws org.osid.NotFoundException {@code syllabusId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code syllabusId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Syllabus getSyllabus(org.osid.id.Id syllabusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSyllabus(syllabusId));
    }


    /**
     *  Gets a {@code SyllabusList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  syllabi specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Syllabi} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  syllabusIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Syllabus} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code syllabusIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByIds(org.osid.id.IdList syllabusIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSyllabiByIds(syllabusIds));
    }


    /**
     *  Gets a {@code SyllabusList} corresponding to the given
     *  syllabus genus {@code Type} which does not include
     *  syllabi of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  syllabusGenusType a syllabus genus type 
     *  @return the returned {@code Syllabus} list
     *  @throws org.osid.NullArgumentException
     *          {@code syllabusGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByGenusType(org.osid.type.Type syllabusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSyllabiByGenusType(syllabusGenusType));
    }


    /**
     *  Gets a {@code SyllabusList} corresponding to the given
     *  syllabus genus {@code Type} and include any additional
     *  syllabi with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  syllabusGenusType a syllabus genus type 
     *  @return the returned {@code Syllabus} list
     *  @throws org.osid.NullArgumentException
     *          {@code syllabusGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByParentGenusType(org.osid.type.Type syllabusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSyllabiByParentGenusType(syllabusGenusType));
    }


    /**
     *  Gets a {@code SyllabusList} containing the given
     *  syllabus record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  syllabusRecordType a syllabus record type 
     *  @return the returned {@code Syllabus} list
     *  @throws org.osid.NullArgumentException
     *          {@code syllabusRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByRecordType(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSyllabiByRecordType(syllabusRecordType));
    }


    /**
     *  Gets a {@code SyllabusList} for the given course.  In plenary
     * mode, the returned list contains all known syllabi or an error
     * results. Otherwise, the returned list may contain only those
     * syllabi that are accessible through this session.
     *
     *  @param  courseId a course {@code Id} 
     *  @return the returned {@code Syllabus} list 
     *  @throws org.osid.NullArgumentException {@code courseId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSyllabiForCourse(courseId));
    }


    /**
     *  Gets all {@code Syllabi}. 
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Syllabi} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabi()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSyllabi());
    }
}
