//
// AbstractCanonicalUnitSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.canonicalunit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCanonicalUnitSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.CanonicalUnitSearchResults {

    private org.osid.offering.CanonicalUnitList canonicalUnits;
    private final org.osid.offering.CanonicalUnitQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.records.CanonicalUnitSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCanonicalUnitSearchResults.
     *
     *  @param canonicalUnits the result set
     *  @param canonicalUnitQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>canonicalUnits</code>
     *          or <code>canonicalUnitQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCanonicalUnitSearchResults(org.osid.offering.CanonicalUnitList canonicalUnits,
                                            org.osid.offering.CanonicalUnitQueryInspector canonicalUnitQueryInspector) {
        nullarg(canonicalUnits, "canonical units");
        nullarg(canonicalUnitQueryInspector, "canonical unit query inspectpr");

        this.canonicalUnits = canonicalUnits;
        this.inspector = canonicalUnitQueryInspector;

        return;
    }


    /**
     *  Gets the canonical unit list resulting from a search.
     *
     *  @return a canonical unit list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnits() {
        if (this.canonicalUnits == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.CanonicalUnitList canonicalUnits = this.canonicalUnits;
        this.canonicalUnits = null;
	return (canonicalUnits);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.CanonicalUnitQueryInspector getCanonicalUnitQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  canonical unit search record <code> Type. </code> This method must
     *  be used to retrieve a canonicalUnit implementing the requested
     *  record.
     *
     *  @param canonicalUnitSearchRecordType a canonicalUnit search 
     *         record type 
     *  @return the canonical unit search
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(canonicalUnitSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CanonicalUnitSearchResultsRecord getCanonicalUnitSearchResultsRecord(org.osid.type.Type canonicalUnitSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.records.CanonicalUnitSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(canonicalUnitSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record canonical unit search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCanonicalUnitRecord(org.osid.offering.records.CanonicalUnitSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "canonical unit record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
