//
// AbstractAdapterVaultLookupSession.java
//
//    A Vault lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authorization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Vault lookup session adapter.
 */

public abstract class AbstractAdapterVaultLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authorization.VaultLookupSession {

    private final org.osid.authorization.VaultLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterVaultLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterVaultLookupSession(org.osid.authorization.VaultLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Vault} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupVaults() {
        return (this.session.canLookupVaults());
    }


    /**
     *  A complete view of the {@code Vault} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeVaultView() {
        this.session.useComparativeVaultView();
        return;
    }


    /**
     *  A complete view of the {@code Vault} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryVaultView() {
        this.session.usePlenaryVaultView();
        return;
    }

     
    /**
     *  Gets the {@code Vault} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Vault} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Vault} and
     *  retained for compatibility.
     *
     *  @param vaultId {@code Id} of the {@code Vault}
     *  @return the vault
     *  @throws org.osid.NotFoundException {@code vaultId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code vaultId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVault(vaultId));
    }


    /**
     *  Gets a {@code VaultList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  vaults specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Vaults} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  vaultIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Vault} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code vaultIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByIds(org.osid.id.IdList vaultIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVaultsByIds(vaultIds));
    }


    /**
     *  Gets a {@code VaultList} corresponding to the given
     *  vault genus {@code Type} which does not include
     *  vaults of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  vaultGenusType a vault genus type 
     *  @return the returned {@code Vault} list
     *  @throws org.osid.NullArgumentException
     *          {@code vaultGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByGenusType(org.osid.type.Type vaultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVaultsByGenusType(vaultGenusType));
    }


    /**
     *  Gets a {@code VaultList} corresponding to the given
     *  vault genus {@code Type} and include any additional
     *  vaults with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  vaultGenusType a vault genus type 
     *  @return the returned {@code Vault} list
     *  @throws org.osid.NullArgumentException
     *          {@code vaultGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByParentGenusType(org.osid.type.Type vaultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVaultsByParentGenusType(vaultGenusType));
    }


    /**
     *  Gets a {@code VaultList} containing the given
     *  vault record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  vaultRecordType a vault record type 
     *  @return the returned {@code Vault} list
     *  @throws org.osid.NullArgumentException
     *          {@code vaultRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByRecordType(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVaultsByRecordType(vaultRecordType));
    }


    /**
     *  Gets a {@code VaultList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Vault} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVaultsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Vaults}. 
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Vaults} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVaults());
    }
}
