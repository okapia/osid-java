//
// TodoProducerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.todoproducer;


/**
 *  A template for implementing a search results.
 */

public final class TodoProducerSearchResults
    extends net.okapia.osid.jamocha.checklist.mason.todoproducer.spi.AbstractTodoProducerSearchResults
    implements org.osid.checklist.mason.TodoProducerSearchResults {


    /**
     *  Constructs a new <code>TodoProducerSearchResults.
     *
     *  @param todoProducers the result set
     *  @param todoProducerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>todoProducers</code>
     *          or <code>todoProducerQueryInspector</code> is
     *          <code>null</code>
     */

    public TodoProducerSearchResults(org.osid.checklist.mason.TodoProducerList todoProducers,
                                 org.osid.checklist.mason.TodoProducerQueryInspector todoProducerQueryInspector) {
        super(todoProducers, todoProducerQueryInspector);
        return;
    }
}
