//
// AbstractMapRaceConstrainerEnablerLookupSession
//
//    A simple framework for providing a RaceConstrainerEnabler lookup service
//    backed by a fixed collection of race constrainer enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a RaceConstrainerEnabler lookup service backed by a
 *  fixed collection of race constrainer enablers. The race constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RaceConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRaceConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractRaceConstrainerEnablerLookupSession
    implements org.osid.voting.rules.RaceConstrainerEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.rules.RaceConstrainerEnabler> raceConstrainerEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.rules.RaceConstrainerEnabler>());


    /**
     *  Makes a <code>RaceConstrainerEnabler</code> available in this session.
     *
     *  @param  raceConstrainerEnabler a race constrainer enabler
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnabler<code>
     *          is <code>null</code>
     */

    protected void putRaceConstrainerEnabler(org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler) {
        this.raceConstrainerEnablers.put(raceConstrainerEnabler.getId(), raceConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of race constrainer enablers available in this session.
     *
     *  @param  raceConstrainerEnablers an array of race constrainer enablers
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putRaceConstrainerEnablers(org.osid.voting.rules.RaceConstrainerEnabler[] raceConstrainerEnablers) {
        putRaceConstrainerEnablers(java.util.Arrays.asList(raceConstrainerEnablers));
        return;
    }


    /**
     *  Makes a collection of race constrainer enablers available in this session.
     *
     *  @param  raceConstrainerEnablers a collection of race constrainer enablers
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putRaceConstrainerEnablers(java.util.Collection<? extends org.osid.voting.rules.RaceConstrainerEnabler> raceConstrainerEnablers) {
        for (org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler : raceConstrainerEnablers) {
            this.raceConstrainerEnablers.put(raceConstrainerEnabler.getId(), raceConstrainerEnabler);
        }

        return;
    }


    /**
     *  Removes a RaceConstrainerEnabler from this session.
     *
     *  @param  raceConstrainerEnablerId the <code>Id</code> of the race constrainer enabler
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeRaceConstrainerEnabler(org.osid.id.Id raceConstrainerEnablerId) {
        this.raceConstrainerEnablers.remove(raceConstrainerEnablerId);
        return;
    }


    /**
     *  Gets the <code>RaceConstrainerEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  raceConstrainerEnablerId <code>Id</code> of the <code>RaceConstrainerEnabler</code>
     *  @return the raceConstrainerEnabler
     *  @throws org.osid.NotFoundException <code>raceConstrainerEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnabler getRaceConstrainerEnabler(org.osid.id.Id raceConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler = this.raceConstrainerEnablers.get(raceConstrainerEnablerId);
        if (raceConstrainerEnabler == null) {
            throw new org.osid.NotFoundException("raceConstrainerEnabler not found: " + raceConstrainerEnablerId);
        }

        return (raceConstrainerEnabler);
    }


    /**
     *  Gets all <code>RaceConstrainerEnablers</code>. In plenary mode, the returned
     *  list contains all known raceConstrainerEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  raceConstrainerEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>RaceConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceconstrainerenabler.ArrayRaceConstrainerEnablerList(this.raceConstrainerEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.raceConstrainerEnablers.clear();
        super.close();
        return;
    }
}
