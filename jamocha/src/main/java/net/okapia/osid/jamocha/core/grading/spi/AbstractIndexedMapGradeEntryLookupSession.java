//
// AbstractIndexedMapGradeEntryLookupSession.java
//
//    A simple framework for providing a GradeEntry lookup service
//    backed by a fixed collection of grade entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a GradeEntry lookup service backed by a
 *  fixed collection of grade entries. The grade entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some grade entries may be compatible
 *  with more types than are indicated through these grade entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradeEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapGradeEntryLookupSession
    extends AbstractMapGradeEntryLookupSession
    implements org.osid.grading.GradeEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.grading.GradeEntry> gradeEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.GradeEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.grading.GradeEntry> gradeEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.GradeEntry>());


    /**
     *  Makes a <code>GradeEntry</code> available in this session.
     *
     *  @param  gradeEntry a grade entry
     *  @throws org.osid.NullArgumentException <code>gradeEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putGradeEntry(org.osid.grading.GradeEntry gradeEntry) {
        super.putGradeEntry(gradeEntry);

        this.gradeEntriesByGenus.put(gradeEntry.getGenusType(), gradeEntry);
        
        try (org.osid.type.TypeList types = gradeEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradeEntriesByRecord.put(types.getNextType(), gradeEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a grade entry from this session.
     *
     *  @param gradeEntryId the <code>Id</code> of the grade entry
     *  @throws org.osid.NullArgumentException <code>gradeEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeGradeEntry(org.osid.id.Id gradeEntryId) {
        org.osid.grading.GradeEntry gradeEntry;
        try {
            gradeEntry = getGradeEntry(gradeEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.gradeEntriesByGenus.remove(gradeEntry.getGenusType());

        try (org.osid.type.TypeList types = gradeEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradeEntriesByRecord.remove(types.getNextType(), gradeEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeGradeEntry(gradeEntryId);
        return;
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  grade entry genus <code>Type</code> which does not include
     *  grade entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known grade entries or an error results. Otherwise,
     *  the returned list may contain only those grade entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  gradeEntryGenusType a grade entry genus type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByGenusType(org.osid.type.Type gradeEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradeentry.ArrayGradeEntryList(this.gradeEntriesByGenus.get(gradeEntryGenusType)));
    }


    /**
     *  Gets a <code>GradeEntryList</code> containing the given
     *  grade entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known grade entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  grade entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  gradeEntryRecordType a grade entry record type 
     *  @return the returned <code>gradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByRecordType(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradeentry.ArrayGradeEntryList(this.gradeEntriesByRecord.get(gradeEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradeEntriesByGenus.clear();
        this.gradeEntriesByRecord.clear();

        super.close();

        return;
    }
}
