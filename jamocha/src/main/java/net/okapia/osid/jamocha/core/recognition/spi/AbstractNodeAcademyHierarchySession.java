//
// AbstractNodeAcademyHierarchySession.java
//
//     Defines an Academy hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an academy hierarchy session for delivering a hierarchy
 *  of academies using the AcademyNode interface.
 */

public abstract class AbstractNodeAcademyHierarchySession
    extends net.okapia.osid.jamocha.recognition.spi.AbstractAcademyHierarchySession
    implements org.osid.recognition.AcademyHierarchySession {

    private java.util.Collection<org.osid.recognition.AcademyNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root academy <code> Ids </code> in this hierarchy.
     *
     *  @return the root academy <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootAcademyIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.recognition.academynode.AcademyNodeToIdList(this.roots));
    }


    /**
     *  Gets the root academies in the academy hierarchy. A node
     *  with no parents is an orphan. While all academy <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root academies 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getRootAcademies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recognition.academynode.AcademyNodeToAcademyList(new net.okapia.osid.jamocha.recognition.academynode.ArrayAcademyNodeList(this.roots)));
    }


    /**
     *  Adds a root academy node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootAcademy(org.osid.recognition.AcademyNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root academy nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootAcademies(java.util.Collection<org.osid.recognition.AcademyNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root academy node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootAcademy(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.recognition.AcademyNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Academy </code> has any parents. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @return <code> true </code> if the academy has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentAcademies(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getAcademyNode(academyId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  academy.
     *
     *  @param  id an <code> Id </code> 
     *  @param  academyId the <code> Id </code> of an academy 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> academyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> academyId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfAcademy(org.osid.id.Id id, org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.recognition.AcademyNodeList parents = getAcademyNode(academyId).getParentAcademyNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextAcademyNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given academy. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @return the parent <code> Ids </code> of the academy 
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentAcademyIds(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recognition.academy.AcademyToIdList(getParentAcademies(academyId)));
    }


    /**
     *  Gets the parents of the given academy. 
     *
     *  @param  academyId the <code> Id </code> to query 
     *  @return the parents of the academy 
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getParentAcademies(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recognition.academynode.AcademyNodeToAcademyList(getAcademyNode(academyId).getParentAcademyNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  academy.
     *
     *  @param  id an <code> Id </code> 
     *  @param  academyId the Id of an academy 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> academyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfAcademy(org.osid.id.Id id, org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAcademy(id, academyId)) {
            return (true);
        }

        try (org.osid.recognition.AcademyList parents = getParentAcademies(academyId)) {
            while (parents.hasNext()) {
                if (isAncestorOfAcademy(id, parents.getNextAcademy().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an academy has any children. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @return <code> true </code> if the <code> academyId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildAcademies(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAcademyNode(academyId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  academy.
     *
     *  @param  id an <code> Id </code> 
     *  @param academyId the <code> Id </code> of an 
     *         academy
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> academyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> academyId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfAcademy(org.osid.id.Id id, org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfAcademy(academyId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  academy.
     *
     *  @param  academyId the <code> Id </code> to query 
     *  @return the children of the academy 
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildAcademyIds(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recognition.academy.AcademyToIdList(getChildAcademies(academyId)));
    }


    /**
     *  Gets the children of the given academy. 
     *
     *  @param  academyId the <code> Id </code> to query 
     *  @return the children of the academy 
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getChildAcademies(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recognition.academynode.AcademyNodeToAcademyList(getAcademyNode(academyId).getChildAcademyNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  academy.
     *
     *  @param  id an <code> Id </code> 
     *  @param academyId the <code> Id </code> of an 
     *         academy
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> academyId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfAcademy(org.osid.id.Id id, org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAcademy(academyId, id)) {
            return (true);
        }

        try (org.osid.recognition.AcademyList children = getChildAcademies(academyId)) {
            while (children.hasNext()) {
                if (isDescendantOfAcademy(id, children.getNextAcademy().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  academy.
     *
     *  @param  academyId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified academy node 
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getAcademyNodeIds(org.osid.id.Id academyId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.recognition.academynode.AcademyNodeToNode(getAcademyNode(academyId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given academy.
     *
     *  @param  academyId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified academy node 
     *  @throws org.osid.NotFoundException <code> academyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> academyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyNode getAcademyNodes(org.osid.id.Id academyId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAcademyNode(academyId));
    }


    /**
     *  Closes this <code>AcademyHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an academy node.
     *
     *  @param academyId the id of the academy node
     *  @throws org.osid.NotFoundException <code>academyId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>academyId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.recognition.AcademyNode getAcademyNode(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(academyId, "academy Id");
        for (org.osid.recognition.AcademyNode academy : this.roots) {
            if (academy.getId().equals(academyId)) {
                return (academy);
            }

            org.osid.recognition.AcademyNode r = findAcademy(academy, academyId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(academyId + " is not found");
    }


    protected org.osid.recognition.AcademyNode findAcademy(org.osid.recognition.AcademyNode node, 
                                                           org.osid.id.Id academyId) 
	throws org.osid.OperationFailedException {

        try (org.osid.recognition.AcademyNodeList children = node.getChildAcademyNodes()) {
            while (children.hasNext()) {
                org.osid.recognition.AcademyNode academy = children.getNextAcademyNode();
                if (academy.getId().equals(academyId)) {
                    return (academy);
                }
                
                academy = findAcademy(academy, academyId);
                if (academy != null) {
                    return (academy);
                }
            }
        }

        return (null);
    }
}
