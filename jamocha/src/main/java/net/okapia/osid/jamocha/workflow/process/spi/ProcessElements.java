//
// ProcessElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.process.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProcessElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the ProcessElement Id.
     *
     *  @return the process element Id
     */

    public static org.osid.id.Id getProcessEntityId() {
        return (makeEntityId("osid.workflow.Process"));
    }


    /**
     *  Gets the InitialStepId element Id.
     *
     *  @return the InitialStepId element Id
     */

    public static org.osid.id.Id getInitialStepId() {
        return (makeElementId("osid.workflow.process.InitialStepId"));
    }


    /**
     *  Gets the InitialStep element Id.
     *
     *  @return the InitialStep element Id
     */

    public static org.osid.id.Id getInitialStep() {
        return (makeElementId("osid.workflow.process.InitialStep"));
    }


    /**
     *  Gets the InitialStateId element Id.
     *
     *  @return the InitialStateId element Id
     */

    public static org.osid.id.Id getInitialStateId() {
        return (makeElementId("osid.workflow.process.InitialStateId"));
    }


    /**
     *  Gets the InitialState element Id.
     *
     *  @return the InitialState element Id
     */

    public static org.osid.id.Id getInitialState() {
        return (makeElementId("osid.workflow.process.InitialState"));
    }


    /**
     *  Gets the Enabled element Id.
     *
     *  @return the Enabled element Id
     */

    public static org.osid.id.Id getEnabled() {
        return (makeQueryElementId("osid.workflow.process.Enabled"));
    }


    /**
     *  Gets the StepId element Id.
     *
     *  @return the StepId element Id
     */

    public static org.osid.id.Id getStepId() {
        return (makeQueryElementId("osid.workflow.process.StepId"));
    }


    /**
     *  Gets the Step element Id.
     *
     *  @return the Step element Id
     */

    public static org.osid.id.Id getStep() {
        return (makeQueryElementId("osid.workflow.process.Step"));
    }


    /**
     *  Gets the WorkId element Id.
     *
     *  @return the WorkId element Id
     */

    public static org.osid.id.Id getWorkId() {
        return (makeQueryElementId("osid.workflow.process.WorkId"));
    }


    /**
     *  Gets the Work element Id.
     *
     *  @return the Work element Id
     */

    public static org.osid.id.Id getWork() {
        return (makeQueryElementId("osid.workflow.process.Work"));
    }


    /**
     *  Gets the OfficeId element Id.
     *
     *  @return the OfficeId element Id
     */

    public static org.osid.id.Id getOfficeId() {
        return (makeQueryElementId("osid.workflow.process.OfficeId"));
    }


    /**
     *  Gets the Office element Id.
     *
     *  @return the Office element Id
     */

    public static org.osid.id.Id getOffice() {
        return (makeQueryElementId("osid.workflow.process.Office"));
    }
}
