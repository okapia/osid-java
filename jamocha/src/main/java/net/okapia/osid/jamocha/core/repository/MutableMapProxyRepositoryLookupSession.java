//
// MutableMapProxyRepositoryLookupSession
//
//    Implements a Repository lookup service backed by a collection of
//    repositories that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements a Repository lookup service backed by a collection of
 *  repositories. The repositories are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of repositories can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyRepositoryLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractMapRepositoryLookupSession
    implements org.osid.repository.RepositoryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRepositoryLookupSession} with no
     *  repositories.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyRepositoryLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyRepositoryLookupSession} with a
     *  single repository.
     *
     *  @param repository a repository
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyRepositoryLookupSession(org.osid.repository.Repository repository, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRepository(repository);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRepositoryLookupSession} using an
     *  array of repositories.
     *
     *  @param repositories an array of repositories
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repositories} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyRepositoryLookupSession(org.osid.repository.Repository[] repositories, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRepositories(repositories);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyRepositoryLookupSession} using
     *  a collection of repositories.
     *
     *  @param repositories a collection of repositories
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repositories} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyRepositoryLookupSession(java.util.Collection<? extends org.osid.repository.Repository> repositories,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRepositories(repositories);
        return;
    }

    
    /**
     *  Makes a {@code Repository} available in this session.
     *
     *  @param repository an repository
     *  @throws org.osid.NullArgumentException {@code repository{@code 
     *          is {@code null}
     */

    @Override
    public void putRepository(org.osid.repository.Repository repository) {
        super.putRepository(repository);
        return;
    }


    /**
     *  Makes an array of repositories available in this session.
     *
     *  @param repositories an array of repositories
     *  @throws org.osid.NullArgumentException {@code repositories{@code 
     *          is {@code null}
     */

    @Override
    public void putRepositories(org.osid.repository.Repository[] repositories) {
        super.putRepositories(repositories);
        return;
    }


    /**
     *  Makes collection of repositories available in this session.
     *
     *  @param repositories
     *  @throws org.osid.NullArgumentException {@code repository{@code 
     *          is {@code null}
     */

    @Override
    public void putRepositories(java.util.Collection<? extends org.osid.repository.Repository> repositories) {
        super.putRepositories(repositories);
        return;
    }


    /**
     *  Removes a Repository from this session.
     *
     *  @param repositoryId the {@code Id} of the repository
     *  @throws org.osid.NullArgumentException {@code repositoryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRepository(org.osid.id.Id repositoryId) {
        super.removeRepository(repositoryId);
        return;
    }    
}
