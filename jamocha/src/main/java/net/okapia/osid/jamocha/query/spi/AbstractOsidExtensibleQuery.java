//
// AbstractOsidExtensibleQuery.java
//
//     An OisdExtensibleQuery with stored terms.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OsidExtensibleQuery with stored terms.
 */

public abstract class AbstractOsidExtensibleQuery
    extends AbstractOsidQuery
    implements org.osid.OsidExtensibleQuery {

    private final Types recordTypes = new TypeSet();
    private final java.util.Collection<org.osid.search.terms.TypeTerm> recordTypeTerms = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new <code>AbstractOsidExtensibleQuery</code>.
     *
     *  @param factory the term factory
     *  @throws org.osid.NullArgumentException <code>factory</code> is
     *          <code>null</code>
     */

    protected AbstractOsidExtensibleQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
        super(factory);
        return;
    }


    /**
     *  Sets a <code> Type </code> for querying objects having records 
     *  implementing a given record type. 
     *
     *  @param  recordType a record type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecordType(org.osid.type.Type recordType, boolean match) {
        this.recordTypeTerms.add(getTermFactory().createTypeTerm(recordType, match));
        return;
    }


    /**
     *  Matches an object that has any record. 
     *
     *  @param  match <code> true </code> to match any record, <code> false 
     *          </code> to match objects with no records 
     */

    @OSID @Override
    public void matchAnyRecord(boolean match) {
        this.recordTypeTerms.add(getTermFactory().createTypeWildcardTerm(match));
    }


    /**
     *  Clears all record <code>Type</code> terms. 
     */

    @OSID @Override
    public void clearRecordTerms() {
        this.recordTypeTerms.clear();
        return;
    }


    /**
     *  Gets all the record type query terms.
     *
     *  @return a collection of the record type query terms
     */

    protected java.util.Collection<org.osid.search.terms.TypeTerm> getRecordTypeTerms() {
        return (java.util.Collections.unmodifiableCollection(this.recordTypeTerms));
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.recordTypes.toCollection()));
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code>true</code> if <code>recordType</code> is
     *          supported, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.recordTypes.contains(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        nullarg(recordType, "recordType");
        this.recordTypes.add(recordType);
        return;
    }    
}
