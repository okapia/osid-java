//
// AbstractQueryPostLookupSession.java
//
//    An inline adapter that maps a PostLookupSession to
//    a PostQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PostLookupSession to
 *  a PostQuerySession.
 */

public abstract class AbstractQueryPostLookupSession
    extends net.okapia.osid.jamocha.financials.posting.spi.AbstractPostLookupSession
    implements org.osid.financials.posting.PostLookupSession {

    private final org.osid.financials.posting.PostQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPostLookupSession.
     *
     *  @param querySession the underlying post query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPostLookupSession(org.osid.financials.posting.PostQuerySession querySession) {
        nullarg(querySession, "post query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Business</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform <code>Post</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPosts() {
        return (this.session.canSearchPosts());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include posts in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the <code>Post</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Post</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Post</code> and
     *  retained for compatibility.
     *
     *  @param  postId <code>Id</code> of the
     *          <code>Post</code>
     *  @return the post
     *  @throws org.osid.NotFoundException <code>postId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>postId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.Post getPost(org.osid.id.Id postId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostQuery query = getQuery();
        query.matchId(postId, true);
        org.osid.financials.posting.PostList posts = this.session.getPostsByQuery(query);
        if (posts.hasNext()) {
            return (posts.getNextPost());
        } 
        
        throw new org.osid.NotFoundException(postId + " not found");
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  posts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Posts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  postIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>postIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostList getPostsByIds(org.osid.id.IdList postIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostQuery query = getQuery();

        try (org.osid.id.IdList ids = postIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  post genus <code>Type</code> which does not include
     *  posts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postGenusType a post genus type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostList getPostsByGenusType(org.osid.type.Type postGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostQuery query = getQuery();
        query.matchGenusType(postGenusType, true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  post genus <code>Type</code> and include any additional
     *  posts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postGenusType a post genus type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostList getPostsByParentGenusType(org.osid.type.Type postGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostQuery query = getQuery();
        query.matchParentGenusType(postGenusType, true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code>PostList</code> containing the given
     *  post record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postRecordType a post record type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostList getPostsByRecordType(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostQuery query = getQuery();
        query.matchRecordType(postRecordType, true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code> PostList </code> for the given fiscal period. In
     *  plenary mode, the returned list contains all known payers or
     *  an error results.  Otherwise, the returned list may contain
     *  only those posts that are accessible through this session.
     *
     *  @param  fiscalPeriodId a post record type 
     *  @return the returned <code> Post </code> list 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.financials.posting.PostList getPostsByFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostQuery query = getQuery();
        query.matchFiscalPeriodId(fiscalPeriodId, true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code> PostList </code> posted between the given date
     *  range inclusive. In plenary mode, the returned list contains
     *  all known payers or an error results. Otherwise, the returned
     *  list may contain only those posts that are accessible through
     *  this session.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Post </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostList getPostsByDate(org.osid.calendaring.DateTime from, 
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets all <code>Posts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Posts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostList getPosts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.financials.posting.PostQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.financials.posting.PostQuery getQuery() {
        org.osid.financials.posting.PostQuery query = this.session.getPostQuery();
        
        return (query);
    }
}
