//
// AbstractCourseSyllabusBatchProxyManager.java
//
//     An adapter for a CourseSyllabusBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.syllabus.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseSyllabusBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseSyllabusBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.course.syllabus.batch.CourseSyllabusBatchProxyManager>
    implements org.osid.course.syllabus.batch.CourseSyllabusBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseSyllabusBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseSyllabusBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseSyllabusBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseSyllabusBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of syllabi is available. 
     *
     *  @return <code> true </code> if a syllabus bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusBatchAdmin() {
        return (getAdapteeManager().supportsSyllabusBatchAdmin());
    }


    /**
     *  Tests if bulk administration of modules is available. 
     *
     *  @return <code> true </code> if a module bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleBatchAdmin() {
        return (getAdapteeManager().supportsModuleBatchAdmin());
    }


    /**
     *  Tests if bulk administration of docets is available. 
     *
     *  @return <code> true </code> if a docet bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetBatchAdmin() {
        return (getAdapteeManager().supportsDocetBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk syllabus 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.batch.SyllabusBatchAdminSession getSyllabusBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk syllabus 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.batch.SyllabusBatchAdminSession getSyllabusBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusBatchAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk module 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ModuleBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.batch.ModuleBatchAdminSession getModuleBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk module 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ModuleBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.batch.ModuleBatchAdminSession getModuleBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleBatchAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk docet 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> DocetBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.batch.DocetBatchAdminSession getDocetBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk docet 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return an <code> DocetBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.batch.DocetBatchAdminSession getDocetBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetBatchAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
