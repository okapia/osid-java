//
// MutableMapCyclicEventLookupSession
//
//    Implements a CyclicEvent lookup service backed by a collection of
//    cyclicEvents that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.cycle;


/**
 *  Implements a CyclicEvent lookup service backed by a collection of
 *  cyclic events. The cyclic events are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of cyclic events can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapCyclicEventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.cycle.spi.AbstractMapCyclicEventLookupSession
    implements org.osid.calendaring.cycle.CyclicEventLookupSession {


    /**
     *  Constructs a new {@code MutableMapCyclicEventLookupSession}
     *  with no cyclic events.
     *
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumentException {@code calendar} is
     *          {@code null}
     */

      public MutableMapCyclicEventLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCyclicEventLookupSession} with a
     *  single cyclicEvent.
     *
     *  @param calendar the calendar  
     *  @param cyclicEvent a cyclic event
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code cyclicEvent} is {@code null}
     */

    public MutableMapCyclicEventLookupSession(org.osid.calendaring.Calendar calendar,
                                           org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        this(calendar);
        putCyclicEvent(cyclicEvent);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCyclicEventLookupSession}
     *  using an array of cyclic events.
     *
     *  @param calendar the calendar
     *  @param cyclicEvents an array of cyclic events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code cyclicEvents} is {@code null}
     */

    public MutableMapCyclicEventLookupSession(org.osid.calendaring.Calendar calendar,
                                           org.osid.calendaring.cycle.CyclicEvent[] cyclicEvents) {
        this(calendar);
        putCyclicEvents(cyclicEvents);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCyclicEventLookupSession}
     *  using a collection of cyclic events.
     *
     *  @param calendar the calendar
     *  @param cyclicEvents a collection of cyclic events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code cyclicEvents} is {@code null}
     */

    public MutableMapCyclicEventLookupSession(org.osid.calendaring.Calendar calendar,
                                           java.util.Collection<? extends org.osid.calendaring.cycle.CyclicEvent> cyclicEvents) {

        this(calendar);
        putCyclicEvents(cyclicEvents);
        return;
    }

    
    /**
     *  Makes a {@code CyclicEvent} available in this session.
     *
     *  @param cyclicEvent a cyclic event
     *  @throws org.osid.NullArgumentException {@code cyclicEvent{@code  is
     *          {@code null}
     */

    @Override
    public void putCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        super.putCyclicEvent(cyclicEvent);
        return;
    }


    /**
     *  Makes an array of cyclic events available in this session.
     *
     *  @param cyclicEvents an array of cyclic events
     *  @throws org.osid.NullArgumentException {@code cyclicEvents{@code 
     *          is {@code null}
     */

    @Override
    public void putCyclicEvents(org.osid.calendaring.cycle.CyclicEvent[] cyclicEvents) {
        super.putCyclicEvents(cyclicEvents);
        return;
    }


    /**
     *  Makes collection of cyclic events available in this session.
     *
     *  @param cyclicEvents a collection of cyclic events
     *  @throws org.osid.NullArgumentException {@code cyclicEvents{@code  is
     *          {@code null}
     */

    @Override
    public void putCyclicEvents(java.util.Collection<? extends org.osid.calendaring.cycle.CyclicEvent> cyclicEvents) {
        super.putCyclicEvents(cyclicEvents);
        return;
    }


    /**
     *  Removes a CyclicEvent from this session.
     *
     *  @param cyclicEventId the {@code Id} of the cyclic event
     *  @throws org.osid.NullArgumentException {@code cyclicEventId{@code 
     *          is {@code null}
     */

    @Override
    public void removeCyclicEvent(org.osid.id.Id cyclicEventId) {
        super.removeCyclicEvent(cyclicEventId);
        return;
    }    
}
