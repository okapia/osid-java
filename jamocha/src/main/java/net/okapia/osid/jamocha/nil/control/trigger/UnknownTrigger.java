//
// UnknownTrigger.java
//
//     Defines an unknown Trigger.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.control.trigger;


/**
 *  Defines an unknown <code>Trigger</code>.
 */

public final class UnknownTrigger
    extends net.okapia.osid.jamocha.nil.control.trigger.spi.AbstractUnknownTrigger
    implements org.osid.control.Trigger {


    /**
     *  Constructs a new Unknown<code>Trigger</code>.
     */

    public UnknownTrigger() {
        return;
    }


    /**
     *  Constructs a new Unknown<code>Trigger</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownTrigger(boolean optional) {
        super(optional);
        addTriggerRecord(new TriggerRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Trigger.
     *
     *  @return an unknown Trigger
     */

    public static org.osid.control.Trigger create() {
        return (net.okapia.osid.jamocha.builder.validator.control.trigger.TriggerValidator.validateTrigger(new UnknownTrigger()));
    }


    public class TriggerRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.control.records.TriggerRecord {

        
        protected TriggerRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
