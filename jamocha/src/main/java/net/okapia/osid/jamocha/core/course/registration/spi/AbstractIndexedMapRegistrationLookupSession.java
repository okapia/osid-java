//
// AbstractIndexedMapRegistrationLookupSession.java
//
//    A simple framework for providing a Registration lookup service
//    backed by a fixed collection of registrations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Registration lookup service backed by a
 *  fixed collection of registrations. The registrations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some registrations may be compatible
 *  with more types than are indicated through these registration
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Registrations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRegistrationLookupSession
    extends AbstractMapRegistrationLookupSession
    implements org.osid.course.registration.RegistrationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.registration.Registration> registrationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.registration.Registration>());
    private final MultiMap<org.osid.type.Type, org.osid.course.registration.Registration> registrationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.registration.Registration>());


    /**
     *  Makes a <code>Registration</code> available in this session.
     *
     *  @param  registration a registration
     *  @throws org.osid.NullArgumentException <code>registration<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRegistration(org.osid.course.registration.Registration registration) {
        super.putRegistration(registration);

        this.registrationsByGenus.put(registration.getGenusType(), registration);
        
        try (org.osid.type.TypeList types = registration.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.registrationsByRecord.put(types.getNextType(), registration);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a registration from this session.
     *
     *  @param registrationId the <code>Id</code> of the registration
     *  @throws org.osid.NullArgumentException <code>registrationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRegistration(org.osid.id.Id registrationId) {
        org.osid.course.registration.Registration registration;
        try {
            registration = getRegistration(registrationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.registrationsByGenus.remove(registration.getGenusType());

        try (org.osid.type.TypeList types = registration.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.registrationsByRecord.remove(types.getNextType(), registration);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRegistration(registrationId);
        return;
    }


    /**
     *  Gets a <code>RegistrationList</code> corresponding to the given
     *  registration genus <code>Type</code> which does not include
     *  registrations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known registrations or an error results. Otherwise,
     *  the returned list may contain only those registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  registrationGenusType a registration genus type 
     *  @return the returned <code>Registration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>registrationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByGenusType(org.osid.type.Type registrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.registration.registration.ArrayRegistrationList(this.registrationsByGenus.get(registrationGenusType)));
    }


    /**
     *  Gets a <code>RegistrationList</code> containing the given
     *  registration record <code>Type</code>. In plenary mode, the
     *  returned list contains all known registrations or an error
     *  results. Otherwise, the returned list may contain only those
     *  registrations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  registrationRecordType a registration record type 
     *  @return the returned <code>registration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByRecordType(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.registration.registration.ArrayRegistrationList(this.registrationsByRecord.get(registrationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.registrationsByGenus.clear();
        this.registrationsByRecord.clear();

        super.close();

        return;
    }
}
