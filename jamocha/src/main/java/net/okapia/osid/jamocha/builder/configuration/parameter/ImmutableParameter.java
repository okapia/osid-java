//
// ImmutableParameter.java
//
//     Wraps a mutable Parameter to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.parameter;


/**
 *  Wraps a mutable <code>Parameter</code> to hide modifiers. This
 *  wrapper provides an immutized Parameter from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying parameter whose state changes are visible.
 */

public final class ImmutableParameter
    extends net.okapia.osid.jamocha.builder.configuration.parameter.spi.AbstractImmutableParameter
    implements org.osid.configuration.Parameter {


    /**
     *  Constructs a new <code>ImmutableParameter</code>.
     *
     *  @param parameter the parameter
     *  @throws org.osid.NullArgumentException <code>parameter</code>
     *          is <code>null</code>
     */

    public ImmutableParameter(org.osid.configuration.Parameter parameter) {
        super(parameter);
        return;
    }
}

