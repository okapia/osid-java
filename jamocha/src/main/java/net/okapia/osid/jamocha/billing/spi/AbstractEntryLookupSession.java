//
// AbstractEntryLookupSession.java
//
//    A starter implementation framework for providing an Entry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Entry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.billing.EntryLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.billing.Business business = new net.okapia.osid.jamocha.nil.billing.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.billing.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }

    /**
     *  Tests if this user can perform <code>Entry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in businesses which are children of
     *  this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only entries whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Entry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Entry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Entry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and
     *  those currently expired are returned.
     *
     *  @param  entryId <code>Id</code> of the
     *          <code>Entry</code>
     *  @return the entry
     *  @throws org.osid.NotFoundException <code>entryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>entryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Entry getEntry(org.osid.id.Id entryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.billing.EntryList entries = getEntries()) {
            while (entries.hasNext()) {
                org.osid.billing.Entry entry = entries.getNextEntry();
                if (entry.getId().equals(entryId)) {
                    return (entry);
                }
            }
        } 

        throw new org.osid.NotFoundException(entryId + " not found");
    }


    /**
     *  Gets a <code>EntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  entries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Entries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, entries are returned that are currently effective.
     *  In any effective mode, effective entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getEntries()</code>.
     *
     *  @param  entryIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>entryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByIds(org.osid.id.IdList entryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.Entry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = entryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.billing.entry.LinkedEntryList(ret));
    }


    /**
     *  Gets a <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> which does not include
     *  entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.EntryGenusFilterList(getEntries(), entryGenusType));
    }


    /**
     *  Gets a <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> and include any additional
     *  entries with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByParentGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getEntriesByGenusType(entryGenusType));
    }


    /**
     *  Gets a <code>EntryList</code> containing the given
     *  entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, [hobjects are returned that are currently
     *  effective.  In any effective mode, effective entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByRecordType(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.EntryRecordFilterList(getEntries(), entryRecordType));
    }


    /**
     *  Gets an <code>EntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In active mode, entries are returned that are currently
     *  active. In any status mode, active and inactive entries are
     *  returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesOnDate(org.osid.calendaring.DateTime from,
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.TemporalEntryFilterList(getEntries(), from, to));
    }


    /**
     *  Gets an <code> EntryList </code> in the given billing period.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently effective.
     *  In any effective mode, effective entries and those currently expired
     *  are returned.
     *
     *  @param  periodId a billing period <code> Id </code>
     *  @return the returned <code> Entry </code> list
     *  @throws org.osid.NullArgumentException <code> periodId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriod(org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilterList(new PeriodFilter(periodId), getEntries()));
    }


    /**
     *  Gets a list of entries corresponding to a customer
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective. In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId the <code>Id</code> of the customer
     *  @return the returned <code>EntryList</code>
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.billing.EntryList getEntriesForCustomer(org.osid.id.Id customerId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilterList(new CustomerFilter(customerId), getEntries()));
    }


    /**
     *  Gets a list of entries corresponding to a customer
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible
     *  through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective. In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId the <code>Id</code> of the customer
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EntryList</code>
     *  @throws org.osid.NullArgumentException <code>customer</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForCustomerOnDate(org.osid.id.Id customerId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.TemporalEntryFilterList(getEntriesForCustomer(customerId), from, to));
    }


    /**
     *  Gets an <code>EntryList</code> for the given customer in a
     *  billing period.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer <code>Id</code>
     *  @param  periodId a billing period <code>Id</code>
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException <code> customerId </code> or
     *          <code>periodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForCustomer(org.osid.id.Id customerId,
                                                                    org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilterList(new PeriodFilter(periodId), getEntriesForCustomer(customerId)));
    }


    /**
     *  Gets an <code>EntryList</code> in the given billing period for
     *  the given customer and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer <code>Id /code>
     *  @param  periodId a billing period <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *          <code>customerId</code>, <code>periodId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForCustomerOnDate(org.osid.id.Id customerId,
                                                                          org.osid.id.Id periodId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.TemporalEntryFilterList(getEntriesByPeriodForCustomer(customerId, periodId), from, to));
    }


    /**
     *  Gets a list of entries corresponding to an item
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  itemId the <code>Id</code> of the item
     *  @return the returned <code>EntryList</code>
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.billing.EntryList getEntriesForItem(org.osid.id.Id itemId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilterList(new ItemFilter(itemId), getEntries()));
    }


    /**
     *  Gets a list of entries corresponding to an item
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  itemId the <code>Id</code> of the item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EntryList</code>
     *  @throws org.osid.NullArgumentException <code>item</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForItemOnDate(org.osid.id.Id itemId,
                                                              org.osid.calendaring.DateTime from,
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.TemporalEntryFilterList(getEntriesForItem(itemId), from, to));
    }


    /**
     *  Gets an <code>EntryList</code> for the given item in a
     *  billing period.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  itemId an item <code>Id</code>
     *  @param  periodId a billing period <code>Id</code>
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException <code> itemId </code> or
     *          <code>periodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForItem(org.osid.id.Id itemId,
                                                                    org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilterList(new PeriodFilter(periodId), getEntriesForItem(itemId)));
    }


    /**
     *  Gets an <code>EntryList</code> in the given billing period for
     *  the given item and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  itemId an item <code>Id</code>
     *  @param  periodId a billing period <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Entry </code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>itemId</code>,
     *          <code>periodId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForItemOnDate(org.osid.id.Id itemId,
                                                                      org.osid.id.Id periodId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.TemporalEntryFilterList(getEntriesByPeriodForItem(itemId, periodId), from, to));
    }


    /**
     *  Gets a list of entries corresponding to customer and item
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId the <code>Id</code> of the customer
     *  @param  itemId the <code>Id</code> of the item
     *  @return the returned <code>EntryList</code>
     *  @throws org.osid.NullArgumentException <code>customer</code>,
     *          <code>item</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForCustomerAndItem(org.osid.id.Id customerId,
                                                                   org.osid.id.Id itemId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilterList(new ItemFilter(itemId), getEntriesForCustomer(customerId)));
    }


    /**
     *  Gets a list of entries corresponding to customer and item
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible
     *  through this session.
     *
     *  In effective mode, entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  entries and those currently expired are returned.
     *
     *  @param  itemId the <code>Id</code> of the item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EntryList</code>
     *  @throws org.osid.NullArgumentException <code>item</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForCustomerAndItemOnDate(org.osid.id.Id customerId,
                                                                         org.osid.id.Id itemId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.TemporalEntryFilterList(getEntriesForCustomerAndItem(customerId, itemId), from, to));
    }


    /**
     *  Gets an <code>EntryList</code> for the given customer and item
     *  in a billing period.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer <code>Id</code>
     *  @param  itemId an item <code>Id</code>
     *  @param  periodId a period <code>Id</code>
     *  @return the returned <code> ntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>customerId</code>, <code>itemId</code>, or
     *          <code>periodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForCustomerAndItem(org.osid.id.Id customerId,
                                                                           org.osid.id.Id itemId,
                                                                           org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilterList(new PeriodFilter(periodId), getEntriesForCustomerAndItem(customerId, itemId)));
    }


    /**
     *  Gets an <code>EntryList</code> for the given customer and item
     *  in a billing period and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer <code>Id</code>
     *  @param  itemId an item <code>Id</code>
     *  @param  periodId a period <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>customerId</code>, <code>itemId</code>,
     *          <code>periodId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForCustomerAndItemOnDate(org.osid.id.Id customerId,
                                                                                 org.osid.id.Id itemId,
                                                                                 org.osid.id.Id periodId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.entry.TemporalEntryFilterList(getEntriesByPeriodForCustomerAndItem(customerId, itemId, periodId), from, to));
    }


    /**
     *  Gets all <code>Entries</code>. 
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Entries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.billing.EntryList getEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.billing.EntryList filterEntriesOnViews(org.osid.billing.EntryList list)
        throws org.osid.OperationFailedException {
            
        org.osid.billing.EntryList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.billing.entry.EffectiveEntryFilterList(ret);
        }

        return (ret);
    }


    public static class PeriodFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilter {
        
        private final org.osid.id.Id periodId;
        
        
        /**
         *  Constructs a new <code>PeriodFilter</code>.
         *
         *  @param periodId the period to filter
         *  @throws org.osid.NullArgumentException
         *          <code>periodId</code> is <code>null</code>
         */
        
        public PeriodFilter(org.osid.id.Id periodId) {
            nullarg(periodId, "period Id");
            this.periodId = periodId;
            return;
        }
        
        
        /**
         *  Used by the EntryFilterList to filter the
         *  entry list based on period.
         *
         *  @param entry the entry
         *  @return <code>true</code> to pass the entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.billing.Entry entry) {
            return (entry.getPeriodId().equals(this.periodId));
        }
    }
    

    public static class CustomerFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilter {

        private final org.osid.id.Id customerId;


        /**
         *  Constructs a new <code>CustomerFilter</code>.
         *
         *  @param customerId the customer to filter
         *  @throws org.osid.NullArgumentException
         *          <code>customerId</code> is <code>null</code>
         */

        public CustomerFilter(org.osid.id.Id customerId) {
            nullarg(customerId, "customer Id");
            this.customerId = customerId;
            return;
        }


        /**
         *  Used by the EntryFilterList to filter the
         *  entry list based on customer.
         *
         *  @param entry the entry
         *  @return <code>true</code> to pass the entry,
         *          <code>false</code> to filter it
         */

        @Override
        public boolean pass(org.osid.billing.Entry entry) {
            return (entry.getCustomerId().equals(this.customerId));
        }
    }


    public static class ItemFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.entry.EntryFilter {

        private final org.osid.id.Id itemId;


        /**
         *  Constructs a new <code>ItemFilter</code>.
         *
         *  @param itemId the item to filter
         *  @throws org.osid.NullArgumentException
         *          <code>itemId</code> is <code>null</code>
         */

        public ItemFilter(org.osid.id.Id itemId) {
            nullarg(itemId, "item Id");
            this.itemId = itemId;
            return;
        }


        /**
         *  Used by the EntryFilterList to filter the
         *  entry list based on item.
         *
         *  @param entry the entry
         *  @return <code>true</code> to pass the entry,
         *          <code>false</code> to filter it
         */

        @Override
        public boolean pass(org.osid.billing.Entry entry) {
            return (entry.getItemId().equals(this.itemId));
        }
    }
}
