//
// AbstractValue.java
//
//     Defines a Value.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.value.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Value</code>.
 */

public abstract class AbstractValue
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObject
    implements org.osid.configuration.Value {

    private org.osid.configuration.Parameter parameter;
    private long priority;
    private boolean booleanValue;
    private byte[] bytesValue;
    private long cardinalValue;
    private org.osid.mapping.Coordinate coordinateValue;
    private org.osid.financials.Currency currencyValue;
    private org.osid.calendaring.DateTime dateTimeValue;
    private java.math.BigDecimal decimalValue;
    private org.osid.mapping.Distance distanceValue;
    private org.osid.calendaring.Duration durationValue;
    private org.osid.mapping.Heading headingValue;
    private org.osid.id.Id idValue;
    private long integerValue;
    private java.lang.Object objectValue;
    private org.osid.mapping.SpatialUnit spatialUnitValue;
    private org.osid.mapping.Speed speedValue;
    private String stringValue;
    private org.osid.calendaring.Time timeValue;
    private org.osid.type.Type typeValue;
    private org.osid.installation.Version versionValue;

    private final java.util.Collection<org.osid.configuration.records.ValueRecord> records = new java.util.LinkedHashSet<>();

    
    /**
     *  Gets the parameter <code> Id </code> of this value. 
     *
     *  @return the parameter <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getParameterId() {
        return (this.parameter.getId());
    }


    /**
     *  Gets the parameter of this value. 
     *
     *  @return the parameter 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.configuration.Parameter getParameter()
        throws org.osid.OperationFailedException {

        return (this.parameter);
    }


    /**
     *  Sets the parameter.
     *
     *  @param parameter a parameter
     *  @throws org.osid.NullArgumentException
     *          <code>parameter</code> is <code>null</code>
     */

    protected void setParameter(org.osid.configuration.Parameter parameter) {
        nullarg(parameter, "parameter");
        this.parameter = parameter;
        return;
    }


    /**
     *  Gets the priority of this value. If the values for this parameter are 
     *  not shuffled, the priority indicates a static ordering of the values 
     *  from lowest to highest. If the values for this parameter are shuffled, 
     *  the priority effects the weight with greater numbers having greater 
     *  weights. 
     *
     *  @return the index of this value 
     */

    @OSID @Override
    public long getPriority() {
        return (this.priority);
    }


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     */

    protected void setPriority(long priority) {
        this.priority = priority;
        return;
    }


    /**
     *  Gets the value if it is a boolean. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          BOOLEAN </code> 
     */

    @OSID @Override
    public boolean getBooleanValue() {
        check(org.osid.Syntax.BOOLEAN);
        return (this.booleanValue);
    }


    /**
     *  Sets the boolean value.
     *
     *  @param booleanValue a boolean value
     */

    protected void setBooleanValue(boolean booleanValue) {
        this.booleanValue = booleanValue;
        return;
    }


    /**
     *  Gets the value if it is a byte. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code>
     *          Parameter.getSyntax() != BYTE </code>
     */

    @OSID @Override
    public byte[] getBytesValue() {
        check(org.osid.Syntax.BYTE);
        return (this.bytesValue);
    }


    /**
     *  Sets the bytes value.
     *
     *  @param bytesValue a bytes value
     *  @throws org.osid.NullArgumentException
     *          <code>bytesValue</code> is <code>null</code>
     */

    protected void setBytesValue(byte[] bytesValue) {
        nullarg(bytesValue, "bytes value");
        this.bytesValue = bytesValue;
        return;
    }


    /**
     *  Gets the value if it is a cardinal. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          CARDINAL </code> 
     */

    @OSID @Override
    public long getCardinalValue() {
        check(org.osid.Syntax.CARDINAL);
        return (this.cardinalValue);
    }


    /**
     *  Sets the cardinal value.
     *
     *  @param cardinalValue a cardinal value
     *  @throws org.osid.NullArgumentException
     *          <code>cardinalValue</code> is <code>null</code>
     */

    protected void setCardinalValue(long cardinalValue) {
        cardinalarg(cardinalValue, "cardinal value");
        this.cardinalValue = cardinalValue;
        return;
    }


    /**
     *  Gets the value if it is a coordinate. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          COORDINATE </code> 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getCoordinateValue() {
        check(org.osid.Syntax.COORDINATE);
        return (this.coordinateValue);
    }


    /**
     *  Sets the coordinate value.
     *
     *  @param coordinateValue a coordinate value
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateValue</code> is <code>null</code>
     */

    protected void setCoordinateValue(org.osid.mapping.Coordinate coordinateValue) {
        nullarg(coordinateValue, "coordinate value");
        this.coordinateValue = coordinateValue;
        return;
    }


    /**
     *  Gets the value if it is a currency. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          CURRENCY </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getCurrencyValue() {
        check(org.osid.Syntax.CURRENCY);
        return (this.currencyValue);
    }


    /**
     *  Sets the currency value.
     *
     *  @param currencyValue a currency value
     *  @throws org.osid.NullArgumentException
     *          <code>currencyValue</code> is <code>null</code>
     */

    protected void setCurrencyValue(org.osid.financials.Currency currencyValue) {
        nullarg(currencyValue, "currency value");
        this.currencyValue = currencyValue;
        return;
    }


    /**
     *  Gets the value if it is a <code> DateTime. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          DATETIME </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateTimeValue() {
        check(org.osid.Syntax.DATETIME);
        return (this.dateTimeValue);
    }


    /**
     *  Sets the date time value.
     *
     *  @param dateTimeValue a date time value
     *  @throws org.osid.NullArgumentException
     *          <code>dateTimeValue</code> is <code>null</code>
     */

    protected void setDateTimeValue(org.osid.calendaring.DateTime dateTimeValue) {
        nullarg(dateTimeValue, "datetime value");
        this.dateTimeValue = dateTimeValue;
        return;
    }


    /**
     *  Gets the value if it is a decimal. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          DECIMAL </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getDecimalValue() {
        check(org.osid.Syntax.DECIMAL);
        return (this.decimalValue);
    }


    /**
     *  Sets the decimal value.
     *
     *  @param decimalValue a decimal value
     *  @throws org.osid.NullArgumentException
     *          <code>decimalValue</code> is <code>null</code>
     */

    protected void setDecimalValue(java.math.BigDecimal decimalValue) {
        nullarg(decimalValue, "decimal value");
        this.decimalValue = decimalValue;
        return;
    }


    /**
     *  Gets the value if it is a <code> Distance. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          DISTANCE </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance getDistanceValue() {
        check(org.osid.Syntax.DISTANCE);
        return (this.distanceValue);
    }


    /**
     *  Sets the distance value.
     *
     *  @param distanceValue a distance value
     *  @throws org.osid.NullArgumentException
     *          <code>distanceValue</code> is <code>null</code>
     */

    protected void setDistanceValue(org.osid.mapping.Distance distanceValue) {
        nullarg(distanceValue, "distance value");
        this.distanceValue = distanceValue;
        return;
    }


    /**
     *  Gets the value if it is a <code> Duration. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          DURATION </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDurationValue() {
        check(org.osid.Syntax.DURATION);
        return (this.durationValue);
    }


    /**
     *  Sets the duration value.
     *
     *  @param durationValue a duration value
     *  @throws org.osid.NullArgumentException
     *          <code>durationValue</code> is <code>null</code>
     */

    protected void setDurationValue(org.osid.calendaring.Duration durationValue) {
        nullarg(durationValue, "duration value");
        this.durationValue = durationValue;
        return;
    }


    /**
     *  Gets the value if it is a <code> Heading. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          HEADING </code> 
     */

    @OSID @Override
    public org.osid.mapping.Heading getHeadingValue() {
        check(org.osid.Syntax.HEADING);
        return (this.headingValue);
    }


    /**
     *  Sets the heading value.
     *
     *  @param headingValue a heading value
     *  @throws org.osid.NullArgumentException
     *          <code>headingValue</code> is <code>null</code>
     */

    protected void setHeadingValue(org.osid.mapping.Heading headingValue) {
        nullarg(headingValue, "heading value");
        this.headingValue = headingValue;
        return;
    }


    /**
     *  Gets the value if it is an <code> Id. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          ID </code> 
     */

    @OSID @Override
    public org.osid.id.Id getIdValue() {
        check(org.osid.Syntax.ID);
        return (this.idValue);
    }


    /**
     *  Sets the id value.
     *
     *  @param idValue an id value
     *  @throws org.osid.NullArgumentException
     *          <code>idValue</code> is <code>null</code>
     */

    protected void setIdValue(org.osid.id.Id idValue) {
        nullarg(idValue, "Id value");
        this.idValue = idValue;
        return;
    }


    /**
     *  Gets the value if it is an integer. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          INTEGER </code> 
     */

    @OSID @Override
    public long getIntegerValue() {
        check(org.osid.Syntax.INTEGER);
        return (this.integerValue);
    }


    /**
     *  Sets the integer value.
     *
     *  @param integerValue an integer value
     */

    protected void setIntegerValue(long integerValue) {
        this.integerValue = integerValue;
        return;
    }


    /**
     *  Gets the value if it is an object. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          OBJECT </code> 
     */

    @OSID @Override
    public java.lang.Object getObjectValue() {
        check(org.osid.Syntax.OBJECT);
        return (this.objectValue);
    }


    /**
     *  Sets the object value.
     *
     *  @param objectValue an object value
     *  @throws org.osid.NullArgumentException
     *          <code>objectValue</code> is <code>null</code>
     */

    protected void setObjectValue(java.lang.Object objectValue) {
        nullarg(objectValue, "object value");
        this.objectValue = objectValue;
        return;
    }


    /**
     *  Gets the value if it is a spatial unit. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit getSpatialUnitValue() {
        check(org.osid.Syntax.SPATIALUNIT);
        return (this.spatialUnitValue);
    }


    /**
     *  Sets the spatial unit value.
     *
     *  @param spatialUnitValue a spatial unit value
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnitValue</code> is <code>null</code>
     */

    protected void setSpatialUnitValue(org.osid.mapping.SpatialUnit spatialUnitValue) {
        nullarg(spatialUnitValue, "spatial unit value");
        this.spatialUnitValue = spatialUnitValue;
        return;
    }


    /**
     *  Gets the value if it is a speed. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          SPEED </code> 
     */

    @OSID @Override
    public org.osid.mapping.Speed getSpeedValue() {
        check(org.osid.Syntax.SPEED);
        return (this.speedValue);
    }


    /**
     *  Sets the speed value.
     *
     *  @param speedValue a speed value
     *  @throws org.osid.NullArgumentException
     *          <code>speedValue</code> is <code>null</code>
     */

    protected void setSpeedValue(org.osid.mapping.Speed speedValue) {
        nullarg(speedValue, "speed value");
        this.speedValue = speedValue;
        return;
    }


    /**
     *  Gets the value if it is a string. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          STRING </code> 
     */

    @OSID @Override
    public String getStringValue() {
        check(org.osid.Syntax.STRING);
        return (this.stringValue);
    }


    /**
     *  Sets the string value.
     *
     *  @param stringValue a string value
     *  @throws org.osid.NullArgumentException
     *          <code>stringValue</code> is <code>null</code>
     */

    protected void setStringValue(String stringValue) {
        nullarg(stringValue, "string value");
        this.stringValue = stringValue;
        return;
    }


    /**
     *  Gets the value if it is a time. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          TIME </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Time getTimeValue() {
        check(org.osid.Syntax.TIME);
        return (this.timeValue);
    }


    /**
     *  Sets the time value.
     *
     *  @param timeValue a time value
     *  @throws org.osid.NullArgumentException
     *          <code>timeValue</code> is <code>null</code>
     */

    protected void setTimeValue(org.osid.calendaring.Time timeValue) {
        nullarg(timeValue, "time value");
        this.timeValue = timeValue;
        return;
    }


    /**
     *  Gets the value if it is a <code> Type. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          TYPE </code> 
     */

    @OSID @Override
    public org.osid.type.Type getTypeValue() {
        check(org.osid.Syntax.TYPE);
        return (this.typeValue);
    }


    /**
     *  Sets the type value.
     *
     *  @param typeValue a type value
     *  @throws org.osid.NullArgumentException
     *          <code>typeValue</code> is <code>null</code>
     */

    protected void setTypeValue(org.osid.type.Type typeValue) {
        nullarg(typeValue, "type value");
        this.typeValue = typeValue;
        return;
    }


    /**
     *  Gets the value if it is a version. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          VERSION </code> 
     */

    @OSID @Override
    public org.osid.installation.Version getVersionValue() {
        check(org.osid.Syntax.VERSION);
        return (this.versionValue);
    }


    /**
     *  Sets the version value.
     *
     *  @param versionValue a version value
     *  @throws org.osid.NullArgumentException
     *          <code>versionValue</code> is <code>null</code>
     */

    protected void setVersionValue(org.osid.installation.Version versionValue) {
        nullarg(versionValue, "version value");
        this.versionValue = versionValue;
        return;
    }


    private void check(org.osid.Syntax syntax) {
        if (this.parameter.getValueSyntax() != syntax) {
            throw new org.osid.IllegalStateException("not a " + syntax);
        }

        return;
    }


    /**
     *  Tests if this value supports the given record
     *  <code>Type</code>.
     *
     *  @param  valueRecordType a value record type 
     *  @return <code>true</code> if the valueRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type valueRecordType) {
        for (org.osid.configuration.records.ValueRecord record : this.records) {
            if (record.implementsRecordType(valueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Value</code> record <code>Type</code>.
     *
     *  @param  valueRecordType the value record type 
     *  @return the value record 
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ValueRecord getValueRecord(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ValueRecord record : this.records) {
            if (record.implementsRecordType(valueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this value. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param valueRecord the value record
     *  @param valueRecordType value record type
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecord</code> or
     *          <code>valueRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addValueRecord(org.osid.configuration.records.ValueRecord valueRecord, 
                                  org.osid.type.Type valueRecordType) {
        
        nullarg(valueRecord, "value record");
        addRecordType(valueRecordType);
        this.records.add(valueRecord);
        
        return;
    }    
}
