//
// PostMiter.java
//
//     Defines a Post miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.posting.post;


/**
 *  Defines a <code>Post</code> miter for use with the builders.
 */

public interface PostMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.financials.posting.Post {


    /**
     *  Sets the fiscal period.
     *
     *  @param period a fiscal period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public void setFiscalPeriod(org.osid.financials.FiscalPeriod period);


    /**
     *  Sets the posted.
     *
     *  @param posted <code> true </code> if this has been posted,
     *         <code> false </code> if just lying around
     */

    public void setPosted(boolean posted);


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds a post entry.
     *
     *  @param entry a post entry
     *  @throws org.osid.NullArgumentException <code>entry</code>
     *          is <code>null</code>
     */

    public void addPostEntry(org.osid.financials.posting.PostEntry entry);


    /**
     *  Sets all the post entries.
     *
     *  @param entries a collection of post entries
     *  @throws org.osid.NullArgumentException
     *          <code>entries</code> is <code>null</code>
     */

    public void setPostEntries(java.util.Collection<org.osid.financials.posting.PostEntry> entries);


    /**
     *  Sets the corrected post.
     *
     *  @param post a corrected post
     *  @throws org.osid.NullArgumentException <code>post</code> is
     *          <code>null</code>
     */

    public void setCorrectedPost(org.osid.financials.posting.Post post);


    /**
     *  Adds a Post record.
     *
     *  @param record a post record
     *  @param recordType the type of post record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPostRecord(org.osid.financials.posting.records.PostRecord record, org.osid.type.Type recordType);
}       


