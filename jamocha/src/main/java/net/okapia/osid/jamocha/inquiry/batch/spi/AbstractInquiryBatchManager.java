//
// AbstractInquiryBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractInquiryBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.inquiry.batch.InquiryBatchManager,
               org.osid.inquiry.batch.InquiryBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractInquiryBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractInquiryBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of inquiries is available. 
     *
     *  @return <code> true </code> if an inquiry bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of audits is available. 
     *
     *  @return <code> true </code> if an audit bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of responses is available. 
     *
     *  @return <code> true </code> if a response bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of inquests is available. 
     *
     *  @return <code> true </code> if an inquest bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk inquiry 
     *  administration service. 
     *
     *  @return an <code> InquiryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.InquiryBatchAdminSession getInquiryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchManager.getInquiryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk inquiry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.InquiryBatchAdminSession getInquiryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchProxyManager.getInquiryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk inquiry 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.InquiryBatchAdminSession getInquiryBatchAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchManager.getInquiryBatchAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk inquiry 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.InquiryBatchAdminSession getInquiryBatchAdminSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchProxyManager.getInquiryBatchAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk audit 
     *  administration service. 
     *
     *  @return an <code> AuditBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.AuditBatchAdminSession getAuditBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchManager.getAuditBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk audit 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.AuditBatchAdminSession getAuditBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchProxyManager.getAuditBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk audit 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.AuditBatchAdminSession getAuditBatchAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchManager.getAuditBatchAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk audit 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.AuditBatchAdminSession getAuditBatchAdminSessionForInquest(org.osid.id.Id inquestId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchProxyManager.getAuditBatchAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk response 
     *  administration service. 
     *
     *  @return a <code> ResponseBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.ResponseBatchAdminSession getResponseAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchManager.getResponseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk response 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResponseBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.ResponseBatchAdminSession getResponseAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchProxyManager.getResponseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk response 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return a <code> ResponseBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.ResponseBatchAdminSession getResponseAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchManager.getResponseAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk response 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResponseBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.ResponseBatchAdminSession getResponseAdminSessionForInquest(org.osid.id.Id inquestId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchProxyManager.getResponseAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk inquest 
     *  administration service. 
     *
     *  @return a <code> InquestBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.InquestBatchAdminSession getInquestBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchManager.getInquestBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk inquest 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> InquestBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.InquestBatchAdminSession getInquestBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.batch.InquiryBatchProxyManager.getInquestBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
