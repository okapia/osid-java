//
// MutableMapJournalEntryLookupSession
//
//    Implements a JournalEntry lookup service backed by a collection of
//    journalEntries that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling;


/**
 *  Implements a JournalEntry lookup service backed by a collection of
 *  journal entries. The journal entries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of journal entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapJournalEntryLookupSession
    extends net.okapia.osid.jamocha.core.journaling.spi.AbstractMapJournalEntryLookupSession
    implements org.osid.journaling.JournalEntryLookupSession {


    /**
     *  Constructs a new {@code MutableMapJournalEntryLookupSession}
     *  with no journal entries.
     *
     *  @param journal the journal
     *  @throws org.osid.NullArgumentException {@code journal} is
     *          {@code null}
     */

      public MutableMapJournalEntryLookupSession(org.osid.journaling.Journal journal) {
        setJournal(journal);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJournalEntryLookupSession} with a
     *  single journalEntry.
     *
     *  @param journal the journal  
     *  @param journalEntry a journal entry
     *  @throws org.osid.NullArgumentException {@code journal} or
     *          {@code journalEntry} is {@code null}
     */

    public MutableMapJournalEntryLookupSession(org.osid.journaling.Journal journal,
                                           org.osid.journaling.JournalEntry journalEntry) {
        this(journal);
        putJournalEntry(journalEntry);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJournalEntryLookupSession}
     *  using an array of journal entries.
     *
     *  @param journal the journal
     *  @param journalEntries an array of journal entries
     *  @throws org.osid.NullArgumentException {@code journal} or
     *          {@code journalEntries} is {@code null}
     */

    public MutableMapJournalEntryLookupSession(org.osid.journaling.Journal journal,
                                           org.osid.journaling.JournalEntry[] journalEntries) {
        this(journal);
        putJournalEntries(journalEntries);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJournalEntryLookupSession}
     *  using a collection of journal entries.
     *
     *  @param journal the journal
     *  @param journalEntries a collection of journal entries
     *  @throws org.osid.NullArgumentException {@code journal} or
     *          {@code journalEntries} is {@code null}
     */

    public MutableMapJournalEntryLookupSession(org.osid.journaling.Journal journal,
                                           java.util.Collection<? extends org.osid.journaling.JournalEntry> journalEntries) {

        this(journal);
        putJournalEntries(journalEntries);
        return;
    }

    
    /**
     *  Makes a {@code JournalEntry} available in this session.
     *
     *  @param journalEntry a journal entry
     *  @throws org.osid.NullArgumentException {@code journalEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putJournalEntry(org.osid.journaling.JournalEntry journalEntry) {
        super.putJournalEntry(journalEntry);
        return;
    }


    /**
     *  Makes an array of journal entries available in this session.
     *
     *  @param journalEntries an array of journal entries
     *  @throws org.osid.NullArgumentException {@code journalEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putJournalEntries(org.osid.journaling.JournalEntry[] journalEntries) {
        super.putJournalEntries(journalEntries);
        return;
    }


    /**
     *  Makes collection of journal entries available in this session.
     *
     *  @param journalEntries a collection of journal entries
     *  @throws org.osid.NullArgumentException {@code journalEntries{@code  is
     *          {@code null}
     */

    @Override
    public void putJournalEntries(java.util.Collection<? extends org.osid.journaling.JournalEntry> journalEntries) {
        super.putJournalEntries(journalEntries);
        return;
    }


    /**
     *  Removes a JournalEntry from this session.
     *
     *  @param journalEntryId the {@code Id} of the journal entry
     *  @throws org.osid.NullArgumentException {@code journalEntryId{@code 
     *          is {@code null}
     */

    @Override
    public void removeJournalEntry(org.osid.id.Id journalEntryId) {
        super.removeJournalEntry(journalEntryId);
        return;
    }    
}
