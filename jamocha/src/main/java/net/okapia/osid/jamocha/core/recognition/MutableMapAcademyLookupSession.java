//
// MutableMapAcademyLookupSession
//
//    Implements an Academy lookup service backed by a collection of
//    academies that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition;


/**
 *  Implements an Academy lookup service backed by a collection of
 *  academies. The academies are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of academies can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapAcademyLookupSession
    extends net.okapia.osid.jamocha.core.recognition.spi.AbstractMapAcademyLookupSession
    implements org.osid.recognition.AcademyLookupSession {


    /**
     *  Constructs a new {@code MutableMapAcademyLookupSession}
     *  with no academies.
     */

    public MutableMapAcademyLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAcademyLookupSession} with a
     *  single academy.
     *  
     *  @param academy an academy
     *  @throws org.osid.NullArgumentException {@code academy}
     *          is {@code null}
     */

    public MutableMapAcademyLookupSession(org.osid.recognition.Academy academy) {
        putAcademy(academy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAcademyLookupSession}
     *  using an array of academies.
     *
     *  @param academies an array of academies
     *  @throws org.osid.NullArgumentException {@code academies}
     *          is {@code null}
     */

    public MutableMapAcademyLookupSession(org.osid.recognition.Academy[] academies) {
        putAcademies(academies);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAcademyLookupSession}
     *  using a collection of academies.
     *
     *  @param academies a collection of academies
     *  @throws org.osid.NullArgumentException {@code academies}
     *          is {@code null}
     */

    public MutableMapAcademyLookupSession(java.util.Collection<? extends org.osid.recognition.Academy> academies) {
        putAcademies(academies);
        return;
    }

    
    /**
     *  Makes an {@code Academy} available in this session.
     *
     *  @param academy an academy
     *  @throws org.osid.NullArgumentException {@code academy{@code  is
     *          {@code null}
     */

    @Override
    public void putAcademy(org.osid.recognition.Academy academy) {
        super.putAcademy(academy);
        return;
    }


    /**
     *  Makes an array of academies available in this session.
     *
     *  @param academies an array of academies
     *  @throws org.osid.NullArgumentException {@code academies{@code 
     *          is {@code null}
     */

    @Override
    public void putAcademies(org.osid.recognition.Academy[] academies) {
        super.putAcademies(academies);
        return;
    }


    /**
     *  Makes collection of academies available in this session.
     *
     *  @param academies a collection of academies
     *  @throws org.osid.NullArgumentException {@code academies{@code  is
     *          {@code null}
     */

    @Override
    public void putAcademies(java.util.Collection<? extends org.osid.recognition.Academy> academies) {
        super.putAcademies(academies);
        return;
    }


    /**
     *  Removes an Academy from this session.
     *
     *  @param academyId the {@code Id} of the academy
     *  @throws org.osid.NullArgumentException {@code academyId{@code 
     *          is {@code null}
     */

    @Override
    public void removeAcademy(org.osid.id.Id academyId) {
        super.removeAcademy(academyId);
        return;
    }    
}
