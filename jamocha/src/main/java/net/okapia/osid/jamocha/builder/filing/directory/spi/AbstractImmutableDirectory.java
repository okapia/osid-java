//
// AbstractImmutableDirectory.java
//
//     Wraps a mutable Directory to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.directory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Directory</code> to hide modifiers. This
 *  wrapper provides an immutized Directory from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying directory whose state changes are visible.
 */

public abstract class AbstractImmutableDirectory
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.filing.Directory {

    private final org.osid.filing.Directory directory;


    /**
     *  Constructs a new <code>AbstractImmutableDirectory</code>.
     *
     *  @param directory the directory to immutablize
     *  @throws org.osid.NullArgumentException <code>directory</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDirectory(org.osid.filing.Directory directory) {
        super(directory);
        this.directory = directory;
        return;
    }


    /**
     *  Gets the name of this entry. The name does not include the
     *  path. If this entry represents an alias, the name of the alias
     *  is returned.
     *
     *  @return the entry name 
     */

    @OSID @Override
    public String getName() {
        return (this.directory.getName());
    }
                                         

    /**
     *  Tests if this entry is an alias. 
     *
     *  @return <code> true </code> if this is an alias, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean isAlias() {
        return (this.directory.isAlias());
    }


    /**
     *  Gets the full path of this entry. The path includes the name. Path 
     *  components are separated by a /. If this entry represents an alias, 
     *  the path to the alias is returned. 
     *
     *  @return the path 
     */

    @OSID @Override
    public String getPath() {
        return (this.directory.getPath());
    }


    /**
     *  Gets the real path of this entry. The path includes the name. Path 
     *  components are separated by a /. If this entry represents an alias, 
     *  the full path to the target file or directory is returned. 
     *
     *  @return the path 
     */

    @OSID @Override
    public String getRealPath() {
        return (this.directory.getRealPath());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> that owns this 
     *  entry. 
     *
     *  @return the <code> Agent Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOwnerId() {
        return (this.directory.getOwnerId());
    }


    /**
     *  Gets the <code> Agent </code> that owns this entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException authentication service not 
     *          available 
     */

    @OSID @Override
    public org.osid.authentication.Agent getOwner()
        throws org.osid.OperationFailedException {

        return (this.directory.getOwner());
    }


    /**
     *  Gets the created time of this entry. 
     *
     *  @return the created time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreatedTime() {
        return (this.directory.getCreatedTime());
    }


    /**
     *  Gets the last modified time of this entry. 
     *
     *  @return the last modified time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastModifiedTime() {
        return (this.directory.getLastModifiedTime());
    }


    /**
     *  Gets the last accessed time of this entry. 
     *
     *  @return the last accessed time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastAccessTime() {
        return (this.directory.getLastAccessTime());
    }

    
    /**
     *  Gets the directory record corresponding to the given <code>
     *  Directory </code> record <code> Type. </code> This method is
     *  used to retrieve an object implementing the requested
     *  record. The <code> directoryRecordType </code> may be the
     *  <code> Type </code> returned in <code> getRecordTypes()
     *  </code> or any of its parents in a <code> Type </code>
     *  hierarchy where <code> hasRecordType(directoryRecordType)
     *  </code> is <code> true </code> .
     *
     *  @param  directoryRecordType the type of the record to retrieve 
     *  @return the directory record 
     *  @throws org.osid.NullArgumentException <code> directoryRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(directoryRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.records.DirectoryRecord getDirectoryRecord(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException {

        return (this.directory.getDirectoryRecord(directoryRecordType));
    }
}

