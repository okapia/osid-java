//
// AbstractRelevancyLookupSession.java
//
//    A starter implementation framework for providing a Relevancy
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Relevancy
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRelevancies(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRelevancyLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.ontology.RelevancyLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.ontology.Ontology ontology = new net.okapia.osid.jamocha.nil.ontology.ontology.UnknownOntology();
    

    /**
     *  Gets the <code>Ontology/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Ontology Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.ontology.getId());
    }


    /**
     *  Gets the <code>Ontology</code> associated with this 
     *  session.
     *
     *  @return the <code>Ontology</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.ontology);
    }


    /**
     *  Sets the <code>Ontology</code>.
     *
     *  @param  ontology the ontology for this session
     *  @throws org.osid.NullArgumentException <code>ontology</code>
     *          is <code>null</code>
     */

    protected void setOntology(org.osid.ontology.Ontology ontology) {
        nullarg(ontology, "ontology");
        this.ontology = ontology;
        return;
    }


    /**
     *  Tests if this user can perform <code>Relevancy</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRelevancies() {
        return (true);
    }


    /**
     *  A complete view of the <code>Relevancy</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRelevancyView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Relevancy</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRelevancyView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relevancies in ontologies which are children
     *  of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this ontology only.
     */

    @OSID @Override
    public void useIsolatedOntologyView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only relevancies whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveRelevancyView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All relevancies of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRelevancyView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Relevancy</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Relevancy</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Relevancy</code> and
     *  retained for compatibility.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyId <code>Id</code> of the
     *          <code>Relevancy</code>
     *  @return the relevancy
     *  @throws org.osid.NotFoundException <code>relevancyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>relevancyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Relevancy getRelevancy(org.osid.id.Id relevancyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ontology.RelevancyList relevancies = getRelevancies()) {
            while (relevancies.hasNext()) {
                org.osid.ontology.Relevancy relevancy = relevancies.getNextRelevancy();
                if (relevancy.getId().equals(relevancyId)) {
                    return (relevancy);
                }
            }
        } 

        throw new org.osid.NotFoundException(relevancyId + " not found");
    }


    /**
     *  Gets a <code>RelevancyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relevancies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Relevancies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, relevancies are returned that are currently effective.
     *  In any effective mode, effective relevancies and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRelevancies()</code>.
     *
     *  @param  relevancyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Relevancy</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByIds(org.osid.id.IdList relevancyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.ontology.Relevancy> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = relevancyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRelevancy(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("relevancy " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.ontology.relevancy.LinkedRelevancyList(ret));
    }


    /**
     *  Gets a <code>RelevancyList</code> corresponding to the given
     *  relevancy genus <code>Type</code> which does not include
     *  relevancies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently effective.
     *  In any effective mode, effective relevancies and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRelevancies()</code>.
     *
     *  @param  relevancyGenusType a relevancy genus type 
     *  @return the returned <code>Relevancy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyGenusFilterList(getRelevancies(), relevancyGenusType));
    }


    /**
     *  Gets a <code>RelevancyList</code> corresponding to the given
     *  relevancy genus <code>Type</code> and include any additional
     *  relevancies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRelevancies()</code>.
     *
     *  @param  relevancyGenusType a relevancy genus type 
     *  @return the returned <code>Relevancy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByParentGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRelevanciesByGenusType(relevancyGenusType));
    }


    /**
     *  Gets a <code>RelevancyList</code> containing the given
     *  relevancy record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRelevancies()</code>.
     *
     *  @param  relevancyRecordType a relevancy record type 
     *  @return the returned <code>Relevancy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByRecordType(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyRecordFilterList(getRelevancies(), relevancyRecordType));
    }


    /**
     *  Gets a <code>RelevancyList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *  
     *  In active mode, relevancies are returned that are currently
     *  active. In any status mode, active and inactive relevancies
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Relevancy</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesOnDate(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.TemporalRelevancyFilterList(getRelevancies(), from, to));
    }
        

    /**
     *  Gets a <code>RelevancyList</code> by genustype and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *  
     *  In active mode, relevancies are returned that are currently
     *  active. In any status mode, active and inactive relevancies
     *  are returned.
     *
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Relevancy</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     * @throws org.osid.NullArgumentException
     *          <code>relevancyGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeOnDate(org.osid.type.Type relevancyGenusType,
                                                                           org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.TemporalRelevancyFilterList(getRelevanciesByGenusType(relevancyGenusType), from, to));
    }
        

    /**
     *  Gets a list of relevancies corresponding to a subject
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the <code>Id</code> of the subject
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>subjectId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubject(org.osid.id.Id subjectId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyFilterList(new SubjectFilter(subjectId), getRelevancies()));
    }


    /**
     *  Gets a list of relevancies corresponding to a subject
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the <code>Id</code> of the subject
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>subjectId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjectOnDate(org.osid.id.Id subjectId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.TemporalRelevancyFilterList(getRelevanciesForSubject(subjectId), from, to));
    }


    /**
     *  Gets the <code> Relevancy </code> mapped to a subject <code> Id 
     *  </code> and relevancy <code> genus Type. </code> Genus <code> Types 
     *  </code> derived from the given genus <code> Typ </code> e are 
     *  included. 
     *  
     *  In plenary mode, the exact <code> Id </code> is found or a <code> 
     *  NOT_FOUND </code> results. Otherwise, the returned <code> Relevancy 
     *  </code> may have a different <code> Id </code> than requested, such as 
     *  the case where a duplicate <code> Id </code> was assigned to a <code> 
     *  Relevancy </code> and retained for compatibility. 
     *  
     *  In effective mode, relevancies are returned that are currently 
     *  effective. In any effective mode, effective relevancies and those 
     *  currently expired are returned. 
     *
     *  @param  subjectId the subject <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> or 
     *          <code> relevancyGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubject(org.osid.id.Id subjectId, 
                                                                               org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyGenusFilterList(getRelevanciesForSubject(subjectId), relevancyGenusType));
    }


    /**
     *  Gets a <code> RelevancyList </code> of the given genus type for the 
     *  given subject effective during the entire given date range inclusive 
     *  but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known relevancies or 
     *  an error results. Otherwise, the returned list may contain only those 
     *  relevancies that are accessible through this session. 
     *  
     *  In effective mode, relevancies are returned that are currently 
     *  effective. In any effective mode, effective relevancies and those 
     *  currently expired are returned. 
     *
     *  @param  subjectId the subject <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @return list of relevancies 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> subjectId, 
     *          relevancyGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubjectOnDate(org.osid.id.Id subjectId, 
                                                                                     org.osid.type.Type relevancyGenusType, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.TemporalRelevancyFilterList(getRelevanciesByGenusTypeForSubject(subjectId, relevancyGenusType), from, to));
    }


    /**
     *  Gets the relevancies for the given subject <code> Ids. </code> 
     *  
     *  In plenary mode, the returned list contains all of the relevancies 
     *  specified in the subject <code> Id </code> list, in the order of the 
     *  list, including duplicates, or an error results if a relevancy <code> 
     *  Id </code> in the supplied list is not found or inaccessible. 
     *  Otherwise, inaccessible relevancies may be omitted from the list and 
     *  may present the elements in any order including returning a unique 
     *  set. 
     *  
     *  In effective mode, relevancies are returned that are currently 
     *  effective. In any effective mode, effective relevancies and those 
     *  currently expired are returned. 
     *
     *  @param  subjectIds a list of subject <code> Ids </code> 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code> subjectIds
     *          </code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjects(org.osid.id.IdList subjectIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.relevancy.CompositeRelevancyList ret = new net.okapia.osid.jamocha.adapter.federator.ontology.relevancy.CompositeRelevancyList();

        try (org.osid.id.IdList ids = subjectIds) {
            while (ids.hasNext()) {
                ret.addRelevancyList(getRelevanciesForSubject(ids.getNextId()));
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of relevancies corresponding to a mapped Id
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  id the <code>Id</code> of the mapped Id
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>id</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForMappedId(org.osid.id.Id id)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyFilterList(new MappedIdFilter(id), getRelevancies()));
    }

    
    /**
     *  Gets a list of relevancies corresponding to a mapped Id
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  id the <code>Id</code> of the mapped Id
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>id</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForMappedIdOnDate(org.osid.id.Id id,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.TemporalRelevancyFilterList(getRelevanciesForMappedId(id), from, to));
    }


    /**
     *  Gets the <code> Relevancy </code> elements mapped to an <code> Id 
     *  </code> of the given relevancy genus <code> Type </code> which 
     *  includes derived genus <code> Types. </code> 
     *  
     *  <code> </code> In plenary mode, the exact <code> Id </code> is found 
     *  or a <code> NOT_FOUND </code> results. Otherwise, the returned <code> 
     *  Relevancy </code> may have a different <code> Id </code> than 
     *  requested, such as the case where a duplicate <code> Id </code> was 
     *  assigned to a <code> Relevancy </code> and retained for compatibility. 
     *  
     *  In effective mode, relevancies are returned that are currently 
     *  effective. In any effective mode, effective relevancies and those 
     *  currently expired are returned. 
     *
     *  @param  id an <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code> id </code> or <code> 
     *          relevancyGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForMappedId(org.osid.id.Id id, 
                                                                                org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyGenusFilterList(getRelevanciesForMappedId(id), relevancyGenusType));
    }


    /**
     *  Gets a <code> RelevancyList </code> of the given genus type for the 
     *  given mapped <code> Id </code> effective during the entire given date 
     *  range inclusive but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known relevancies or 
     *  an error results. Otherwise, the returned list may contain only those 
     *  relevancies that are accessible through this session. 
     *  
     *  In effective mode, relevancies are returned that are currently 
     *  effective. In any effective mode, effective relevancies and those 
     *  currently expired are returned. 
     *
     *  @param  id an <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @return list of relevancies 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> id, relevancyGenusType, 
     *          from </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForMappedIdOnDate(org.osid.id.Id id, 
                                                                                      org.osid.type.Type relevancyGenusType, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.TemporalRelevancyFilterList(getRelevanciesByGenusTypeForMappedId(id, relevancyGenusType), from, to));
    }


    /**
     *  Gets the relevancies for the given mapped <code>Ids</code>.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relevancies mapped to the <code>Id</code> or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible.  Otherwise, inaccessible relevancies may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *  
     *  In effective mode, relevancies are returned that are currently 
     *  effective. In any effective mode, effective relevancies and those 
     *  currently expired are returned. 
     *
     *  @param  ids a list of <code>Ids</code> 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code>ids</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForMappedIds(org.osid.id.IdList ids)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.relevancy.CompositeRelevancyList ret = new net.okapia.osid.jamocha.adapter.federator.ontology.relevancy.CompositeRelevancyList();

        try (org.osid.id.IdList idList = ids) {
            while (idList.hasNext()) {
                ret.addRelevancyList(getRelevanciesForMappedId(idList.getNextId()));
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of relevancies corresponding to subject and mapped Id
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the <code>Id</code> of the subject
     *  @param  id the <code>Id</code> of the mapped Id
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>subjectId</code>,
     *          <code>id</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjectAndMappedId(org.osid.id.Id subjectId,
                                                                               org.osid.id.Id id)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyFilterList(new MappedIdFilter(id), getRelevanciesForSubject(subjectId)));
    }


    /**
     *  Gets a list of relevancies corresponding to subject and mapped Id
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  id the <code>Id</code> of the mapped Id
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>subjectId</code>,
     *          <code>id</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjectAndMappedIdOnDate(org.osid.id.Id subjectId,
                                                                                     org.osid.id.Id id,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.TemporalRelevancyFilterList(getRelevanciesForSubjectAndMappedId(subjectId, id), from, to));
    }


    /**
     *  Gets the <code>Relevancy</code> of the given genus type and mapped 
     *  to a subject and mapped <code>Id.</code> 
     *  
     *  <code></code> In plenary mode, the exact <code>Id</code> is found 
     *  or a <code>NOT_FOUND</code> results. Otherwise, the returned <code>
     *  Relevancy</code> may have a different <code>Id</code> than 
     *  requested, such as the case where a duplicate <code>Id</code> was 
     *  assigned to a <code>Relevancy</code> and retained for compatibility. 
     *  
     *  In effective mode, relevancies are returned that are currently 
     *  effective. In any effective mode, effective relevancies and those 
     *  currently expired are returned. 
     *
     *  @param  subjectId the subject <code>Id</code> 
     *  @param  id the mapped <code>Id</code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code>subjectId, id</code> , 
     *          or <code>relevancyGenusType</code> is <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubjectAndMappedId(org.osid.id.Id subjectId, 
                                                                                          org.osid.id.Id id, 
                                                                                          org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyGenusFilterList(getRelevanciesForSubjectAndMappedId(subjectId, id), relevancyGenusType));
    }


    /**
     *  Gets a <code>RelevancyList</code> of the given genus type and 
     *  related to the given subject and mapped <code>Id</code> effective 
     *  during the entire given date range inclusive but not confined to the 
     *  date range. 
     *  
     *  In plenary mode, the returned list contains all known relevancies or 
     *  an error results. Otherwise, the returned list may contain only those 
     *  relevancies that are accessible through this session. 
     *  
     *  In effective mode, relevancies are returned that are currently 
     *  effective. In any effective mode, effective relevancies and those 
     *  currently expired are returned. 
     *
     *  @param  subjectId the subject <code>Id</code> 
     *  @param  id the mapped <code>Id</code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @return list of relevancies 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>subjectId, id, 
     *          relevancyGenusTYpe, from</code> or <code>to</code> is 
     *          <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubjectAndMappedIdOnDate(org.osid.id.Id subjectId, 
                                                                                                org.osid.id.Id id, 
                                                                                                org.osid.type.Type relevancyGenusType, 
                                                                                                org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.TemporalRelevancyFilterList(getRelevanciesByGenusTypeForSubjectAndMappedId(subjectId, id, relevancyGenusType), from, to));
    }


    /**
     *  Gets all <code>Relevancies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Relevancies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.ontology.RelevancyList getRelevancies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the relevancy list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of relevancies
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.ontology.RelevancyList filterRelevanciesOnViews(org.osid.ontology.RelevancyList list)
        throws org.osid.OperationFailedException {

        org.osid.ontology.RelevancyList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.ontology.relevancy.EffectiveRelevancyFilterList(ret);
        }

        return (ret);
    }

    public static class SubjectFilter
        implements net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyFilter {
         
        private final org.osid.id.Id subjectId;
         
         
        /**
         *  Constructs a new <code>SubjectFilter</code>.
         *
         *  @param subjectId the subject to filter
         *  @throws org.osid.NullArgumentException
         *          <code>subjectId</code> is <code>null</code>
         */
        
        public SubjectFilter(org.osid.id.Id subjectId) {
            nullarg(subjectId, "subject Id");
            this.subjectId = subjectId;
            return;
        }

         
        /**
         *  Used by the RelevancyFilterList to filter the 
         *  relevancy list based on subject.
         *
         *  @param relevancy the relevancy
         *  @return <code>true</code> to pass the relevancy,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.ontology.Relevancy relevancy) {
            return (relevancy.getSubjectId().equals(this.subjectId));
        }
    }


    public static class MappedIdFilter
        implements net.okapia.osid.jamocha.inline.filter.ontology.relevancy.RelevancyFilter {
         
        private final org.osid.id.Id id;
         
         
        /**
         *  Constructs a new <code>MappedIdFilter</code>.
         *
         *  @param id the mapped Id to filter
         *  @throws org.osid.NullArgumentException
         *          <code>id</code> is <code>null</code>
         */
        
        public MappedIdFilter(org.osid.id.Id id) {
            nullarg(id, "mapped Id Id");
            this.id = id;
            return;
        }

         
        /**
         *  Used by the RelevancyFilterList to filter the 
         *  relevancy list based on mapped Id.
         *
         *  @param relevancy the relevancy
         *  @return <code>true</code> to pass the relevancy,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.ontology.Relevancy relevancy) {
            return (relevancy.getMappedId().equals(this.id));
        }
    }
}
