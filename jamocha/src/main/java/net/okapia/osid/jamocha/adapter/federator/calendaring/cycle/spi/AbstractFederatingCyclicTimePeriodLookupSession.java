//
// AbstractFederatingCyclicTimePeriodLookupSession.java
//
//     An abstract federating adapter for a CyclicTimePeriodLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CyclicTimePeriodLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCyclicTimePeriodLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.cycle.CyclicTimePeriodLookupSession>
    implements org.osid.calendaring.cycle.CyclicTimePeriodLookupSession {

    private boolean parallel = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Constructs a new <code>AbstractFederatingCyclicTimePeriodLookupSession</code>.
     */

    protected AbstractFederatingCyclicTimePeriodLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>CyclicTimePeriod</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCyclicTimePeriods() {
        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            if (session.canLookupCyclicTimePeriods()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>CyclicTimePeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCyclicTimePeriodView() {
        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            session.useComparativeCyclicTimePeriodView();
        }

        return;
    }


    /**
     *  A complete view of the <code>CyclicTimePeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCyclicTimePeriodView() {
        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            session.usePlenaryCyclicTimePeriodView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include cyclic time periods in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }

     
    /**
     *  Gets the <code>CyclicTimePeriod</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CyclicTimePeriod</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CyclicTimePeriod</code> and
     *  retained for compatibility.
     *
     *  @param  cyclicTimePeriodId <code>Id</code> of the
     *          <code>CyclicTimePeriod</code>
     *  @return the cyclic time period
     *  @throws org.osid.NotFoundException <code>cyclicTimePeriodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriod getCyclicTimePeriod(org.osid.id.Id cyclicTimePeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            try {
                return (session.getCyclicTimePeriod(cyclicTimePeriodId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(cyclicTimePeriodId + " not found");
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cyclicTimePeriods specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CyclicTimePeriods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  cyclicTimePeriodIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByIds(org.osid.id.IdList cyclicTimePeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.MutableCyclicTimePeriodList ret = new net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.MutableCyclicTimePeriodList();

        try (org.osid.id.IdList ids = cyclicTimePeriodIds) {
            while (ids.hasNext()) {
                ret.addCyclicTimePeriod(getCyclicTimePeriod(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the given
     *  cyclic time period genus <code>Type</code> which does not include
     *  cyclic time periods of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicTimePeriodGenusType a cyclicTimePeriod genus type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByGenusType(org.osid.type.Type cyclicTimePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclictimeperiod.FederatingCyclicTimePeriodList ret = getCyclicTimePeriodList();

        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            ret.addCyclicTimePeriodList(session.getCyclicTimePeriodsByGenusType(cyclicTimePeriodGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the given
     *  cyclic time period genus <code>Type</code> and include any additional
     *  cyclic time periods with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicTimePeriodGenusType a cyclicTimePeriod genus type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByParentGenusType(org.osid.type.Type cyclicTimePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclictimeperiod.FederatingCyclicTimePeriodList ret = getCyclicTimePeriodList();

        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            ret.addCyclicTimePeriodList(session.getCyclicTimePeriodsByParentGenusType(cyclicTimePeriodGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> containing the given
     *  cyclic time period record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicTimePeriodRecordType a cyclicTimePeriod record type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByRecordType(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclictimeperiod.FederatingCyclicTimePeriodList ret = getCyclicTimePeriodList();

        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            ret.addCyclicTimePeriodList(session.getCyclicTimePeriodsByRecordType(cyclicTimePeriodRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>CyclicTimePeriods</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>CyclicTimePeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclictimeperiod.FederatingCyclicTimePeriodList ret = getCyclicTimePeriodList();

        for (org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session : getSessions()) {
            ret.addCyclicTimePeriodList(session.getCyclicTimePeriods());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclictimeperiod.FederatingCyclicTimePeriodList getCyclicTimePeriodList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclictimeperiod.ParallelCyclicTimePeriodList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclictimeperiod.CompositeCyclicTimePeriodList());
        }
    }
}
