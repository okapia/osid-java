//
// InvariantMapProxyAssessmentLookupSession
//
//    Implements an Assessment lookup service backed by a fixed
//    collection of assessments. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements an Assessment lookup service backed by a fixed
 *  collection of assessments. The assessments are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAssessmentLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractMapAssessmentLookupSession
    implements org.osid.assessment.AssessmentLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAssessmentLookupSession} with no
     *  assessments.
     *
     *  @param bank the bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAssessmentLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.proxy.Proxy proxy) {
        setBank(bank);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAssessmentLookupSession} with a single
     *  assessment.
     *
     *  @param bank the bank
     *  @param assessment an single assessment
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessment} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAssessmentLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.Assessment assessment, org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putAssessment(assessment);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAssessmentLookupSession} using
     *  an array of assessments.
     *
     *  @param bank the bank
     *  @param assessments an array of assessments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessments} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAssessmentLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.Assessment[] assessments, org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putAssessments(assessments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAssessmentLookupSession} using a
     *  collection of assessments.
     *
     *  @param bank the bank
     *  @param assessments a collection of assessments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessments} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAssessmentLookupSession(org.osid.assessment.Bank bank,
                                                  java.util.Collection<? extends org.osid.assessment.Assessment> assessments,
                                                  org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putAssessments(assessments);
        return;
    }
}
