//
// InvariantMapConfigurationLookupSession
//
//    Implements a Configuration lookup service backed by a fixed collection of
//    configurations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration;


/**
 *  Implements a Configuration lookup service backed by a fixed
 *  collection of configurations. The configurations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapConfigurationLookupSession
    extends net.okapia.osid.jamocha.core.configuration.spi.AbstractMapConfigurationLookupSession
    implements org.osid.configuration.ConfigurationLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapConfigurationLookupSession</code> with no
     *  configurations.
     */

    public InvariantMapConfigurationLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapConfigurationLookupSession</code> with a single
     *  configuration.
     *  
     *  @throws org.osid.NullArgumentException {@code configuration}
     *          is <code>null</code>
     */

    public InvariantMapConfigurationLookupSession(org.osid.configuration.Configuration configuration) {
        putConfiguration(configuration);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapConfigurationLookupSession</code> using an array
     *  of configurations.
     *  
     *  @throws org.osid.NullArgumentException {@code configurations}
     *          is <code>null</code>
     */

    public InvariantMapConfigurationLookupSession(org.osid.configuration.Configuration[] configurations) {
        putConfigurations(configurations);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapConfigurationLookupSession</code> using a
     *  collection of configurations.
     *
     *  @throws org.osid.NullArgumentException {@code configurations}
     *          is <code>null</code>
     */

    public InvariantMapConfigurationLookupSession(java.util.Collection<? extends org.osid.configuration.Configuration> configurations) {
        putConfigurations(configurations);
        return;
    }
}
