//
// MutableStockNodeList.java
//
//     Implements a StockNodeList. This list allows StockNodes to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.stocknode;


/**
 *  <p>Implements a StockNodeList. This list allows StockNodes to be
 *  added after this stockNode has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this stockNode must
 *  invoke <code>eol()</code> when there are no more stockNodes to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>StockNodeList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more stockNodes are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more stockNodes to be added.</p>
 */

public final class MutableStockNodeList
    extends net.okapia.osid.jamocha.inventory.stocknode.spi.AbstractMutableStockNodeList
    implements org.osid.inventory.StockNodeList {


    /**
     *  Creates a new empty <code>MutableStockNodeList</code>.
     */

    public MutableStockNodeList() {
        super();
    }


    /**
     *  Creates a new <code>MutableStockNodeList</code>.
     *
     *  @param stockNode a <code>StockNode</code>
     *  @throws org.osid.NullArgumentException <code>stockNode</code>
     *          is <code>null</code>
     */

    public MutableStockNodeList(org.osid.inventory.StockNode stockNode) {
        super(stockNode);
        return;
    }


    /**
     *  Creates a new <code>MutableStockNodeList</code>.
     *
     *  @param array an array of stocknodes
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableStockNodeList(org.osid.inventory.StockNode[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableStockNodeList</code>.
     *
     *  @param collection a java.util.Collection of stocknodes
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableStockNodeList(java.util.Collection<org.osid.inventory.StockNode> collection) {
        super(collection);
        return;
    }
}
