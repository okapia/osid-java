//
// AbstractFinancialsPostingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractFinancialsPostingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.financials.posting.FinancialsPostingManager,
               org.osid.financials.posting.FinancialsPostingProxyManager {

    private final Types postRecordTypes                    = new TypeRefSet();
    private final Types postSearchRecordTypes              = new TypeRefSet();

    private final Types postEntryRecordTypes               = new TypeRefSet();
    private final Types postEntrySearchRecordTypes         = new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractFinancialsPostingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractFinancialsPostingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any business federation is exposed. Federation is exposed 
     *  when a specific business may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up posts is supported. 
     *
     *  @return <code> true </code> if post lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostLookup() {
        return (false);
    }


    /**
     *  Tests if querying posts is supported. 
     *
     *  @return <code> true </code> if post query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostQuery() {
        return (false);
    }


    /**
     *  Tests if searching posts is supported. 
     *
     *  @return <code> true </code> if post search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSearch() {
        return (false);
    }


    /**
     *  Tests if post <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if post administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostAdmin() {
        return (false);
    }


    /**
     *  Tests if a post <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if post notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostNotification() {
        return (false);
    }


    /**
     *  Tests if a post cataloging service is supported. 
     *
     *  @return <code> true </code> if post catalog is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostBusiness() {
        return (false);
    }


    /**
     *  Tests if a post cataloging service is supported. A cataloging service 
     *  maps posts to catalogs. 
     *
     *  @return <code> true </code> if post cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if a post smart business session is available. 
     *
     *  @return <code> true </code> if a post smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up post entries is supported. 
     *
     *  @return <code> true </code> if post entry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying post entries is supported. 
     *
     *  @return <code> true </code> if post entry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching post entries is supported. 
     *
     *  @return <code> true </code> if post entry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntrySearch() {
        return (false);
    }


    /**
     *  Tests if post entry administrative service is supported. 
     *
     *  @return <code> true </code> if post entry administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if an entry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if post entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryNotification() {
        return (false);
    }


    /**
     *  Tests if an post entry cataloging service is supported. 
     *
     *  @return <code> true </code> if post entry catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryBusiness() {
        return (false);
    }


    /**
     *  Tests if an post entry cataloging service is supported. A cataloging 
     *  service maps post entries to catalogs. 
     *
     *  @return <code> true </code> if post entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if an post entry smart business session is available. 
     *
     *  @return <code> true </code> if an post entry smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntrySmartBusiness() {
        return (false);
    }


    /**
     *  Tests if a posting batch service is available. 
     *
     *  @return <code> true </code> if a posting batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsPostingBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Post </code> record types. 
     *
     *  @return a list containing the supported <code> Post </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.postRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Post </code> record type is supported. 
     *
     *  @param  postRecordType a <code> Type </code> indicating an <code> Post 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostRecordType(org.osid.type.Type postRecordType) {
        return (this.postRecordTypes.contains(postRecordType));
    }


    /**
     *  Adds support for a post record type.
     *
     *  @param postRecordType a post record type
     *  @throws org.osid.NullArgumentException
     *  <code>postRecordType</code> is <code>null</code>
     */

    protected void addPostRecordType(org.osid.type.Type postRecordType) {
        this.postRecordTypes.add(postRecordType);
        return;
    }


    /**
     *  Removes support for a post record type.
     *
     *  @param postRecordType a post record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>postRecordType</code> is <code>null</code>
     */

    protected void removePostRecordType(org.osid.type.Type postRecordType) {
        this.postRecordTypes.remove(postRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Post </code> search record types. 
     *
     *  @return a list containing the supported <code> Post </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.postSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Post </code> search record type is 
     *  supported. 
     *
     *  @param  postSearchRecordType a <code> Type </code> indicating an 
     *          <code> Post </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostSearchRecordType(org.osid.type.Type postSearchRecordType) {
        return (this.postSearchRecordTypes.contains(postSearchRecordType));
    }


    /**
     *  Adds support for a post search record type.
     *
     *  @param postSearchRecordType a post search record type
     *  @throws org.osid.NullArgumentException
     *  <code>postSearchRecordType</code> is <code>null</code>
     */

    protected void addPostSearchRecordType(org.osid.type.Type postSearchRecordType) {
        this.postSearchRecordTypes.add(postSearchRecordType);
        return;
    }


    /**
     *  Removes support for a post search record type.
     *
     *  @param postSearchRecordType a post search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>postSearchRecordType</code> is <code>null</code>
     */

    protected void removePostSearchRecordType(org.osid.type.Type postSearchRecordType) {
        this.postSearchRecordTypes.remove(postSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> PostEntry </code> record types. 
     *
     *  @return a list containing the supported <code> PostEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.postEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> PostEntry </code> record type is supported. 
     *
     *  @param  postEntryRecordType a <code> Type </code> indicating an <code> 
     *          PostEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostEntryRecordType(org.osid.type.Type postEntryRecordType) {
        return (this.postEntryRecordTypes.contains(postEntryRecordType));
    }


    /**
     *  Adds support for a post entry record type.
     *
     *  @param postEntryRecordType a post entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>postEntryRecordType</code> is <code>null</code>
     */

    protected void addPostEntryRecordType(org.osid.type.Type postEntryRecordType) {
        this.postEntryRecordTypes.add(postEntryRecordType);
        return;
    }


    /**
     *  Removes support for a post entry record type.
     *
     *  @param postEntryRecordType a post entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>postEntryRecordType</code> is <code>null</code>
     */

    protected void removePostEntryRecordType(org.osid.type.Type postEntryRecordType) {
        this.postEntryRecordTypes.remove(postEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> PostEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> PostEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.postEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> PostEntry </code> search record type is 
     *  supported. 
     *
     *  @param  postEntrySearchRecordType a <code> Type </code> indicating an 
     *          <code> PostEntry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          postEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostEntrySearchRecordType(org.osid.type.Type postEntrySearchRecordType) {
        return (this.postEntrySearchRecordTypes.contains(postEntrySearchRecordType));
    }


    /**
     *  Adds support for a post entry search record type.
     *
     *  @param postEntrySearchRecordType a post entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>postEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addPostEntrySearchRecordType(org.osid.type.Type postEntrySearchRecordType) {
        this.postEntrySearchRecordTypes.add(postEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a post entry search record type.
     *
     *  @param postEntrySearchRecordType a post entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>postEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removePostEntrySearchRecordType(org.osid.type.Type postEntrySearchRecordType) {
        this.postEntrySearchRecordTypes.remove(postEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service. 
     *
     *  @return a <code> PostSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostLookupSession getPostLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostLookupSession getPostLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> PostLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostLookupSession getPostLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> PostLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostLookupSession getPostLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service. 
     *
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQuerySession getPostQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQuerySession getPostQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQuerySession getPostQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQuerySession getPostQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service. 
     *
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSearchSession getPostSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSearchSession getPostSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSearchSession getPostSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSearchSession getPostSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administration service. 
     *
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostAdminSession getPostAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostAdminSession getPostAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostAdminSession getPostAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostAdminSession getPostAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service. 
     *
     *  @param  postReceiver the notification callback 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostNotificationSession getPostNotificationSession(org.osid.financials.posting.PostReceiver postReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service. 
     *
     *  @param  postReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostNotificationSession getPostNotificationSession(org.osid.financials.posting.PostReceiver postReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service for the given business. 
     *
     *  @param  postReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver </code> or 
     *          <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostNotificationSession getPostNotificationSessionForBusiness(org.osid.financials.posting.PostReceiver postReceiver, 
                                                                                                     org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service for the given business. 
     *
     *  @param  postReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver, businessId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostNotificationSession getPostNotificationSessionForBusiness(org.osid.financials.posting.PostReceiver postReceiver, 
                                                                                                     org.osid.id.Id businessId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup post/catalog mappings. 
     *
     *  @return a <code> PostBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostBusinessSession getPostBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup post/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostBusinessSession getPostBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning posts to 
     *  businesses. 
     *
     *  @return a <code> PostBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostBusinessAssignmentSession getPostBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning posts to 
     *  businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostBusinessAssignmentSession getPostBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSmartBusinessSession getPostSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSmartBusinessSession getPostSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  lookup service. 
     *
     *  @return a <code> PostEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryLookupSession getPostEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryLookupSession getPostEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryLookupSession getPostEntryLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryLookupSession getPostEntryLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  query service. 
     *
     *  @return a <code> PostEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryQuerySession getPostEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryQuerySession getPostEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  query service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryQuerySession getPostEntryQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  query service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryQuerySession getPostEntryQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  search service. 
     *
     *  @return a <code> PostEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntrySearchSession getPostEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntrySearchSession getPostEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntrySearchSession getPostEntrySearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntrySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntrySearchSession getPostEntrySearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntrySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  administration service. 
     *
     *  @return a <code> PostEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryAdminSession getPostEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryAdminSession getPostEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryAdminSession getPostEntryAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryAdminSession getPostEntryAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  notification service. 
     *
     *  @param  postEntryReceiver the notification callback 
     *  @return a <code> PostEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> postEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryNotificationSession getPostEntryNotificationSession(org.osid.financials.posting.PostEntryReceiver postEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  notification service. 
     *
     *  @param  postEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> postEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryNotificationSession getPostEntryNotificationSession(org.osid.financials.posting.PostEntryReceiver postEntryReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  notification service for the given business. 
     *
     *  @param  postEntryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> postEntryReceiver 
     *          </code> or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryNotificationSession getPostEntryNotificationSessionForBusiness(org.osid.financials.posting.PostEntryReceiver postEntryReceiver, 
                                                                                                               org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  notification service for the given business. 
     *
     *  @param  postEntryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> postEntryReceiver, 
     *          businessId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryNotificationSession getPostEntryNotificationSessionForBusiness(org.osid.financials.posting.PostEntryReceiver postEntryReceiver, 
                                                                                                               org.osid.id.Id businessId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup entry/catalog mappings. 
     *
     *  @return a <code> PostEntryBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryBusinessSession getPostEntryBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup entry/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryBusinessSession getPostEntryBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning post 
     *  entries to businesses. 
     *
     *  @return a <code> PostEntryBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryBusinessAssignmentSession getPostEntryBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntryBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning post 
     *  entries to businesses. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryBusinessAssignmentSession getPostEntryBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntryBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  smart business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostEntrySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntrySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntrySmartBusinessSession getPostEntrySmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getPostEntrySmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  smart business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntrySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntrySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntrySmartBusinessSession getPostEntrySmartBusinessSession(org.osid.id.Id businessId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getPostEntrySmartBusinessSession not implemented");
    }


    /**
     *  Gets a <code> FinancialsPostingBatchManager. </code> 
     *
     *  @return a <code> FinancialsPostingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsPostingBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.batch.FinancialsPostingBatchManager getFinancialsPostingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingManager.getFinancialsPostingBatchManager not implemented");
    }


    /**
     *  Gets a <code> FinancialsPostingBatchProxyManager. </code> 
     *
     *  @return a <code> FinancialsPostingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsPostingBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.batch.FinancialsPostingBatchProxyManager getFinancialsPostingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.posting.FinancialsPostingProxyManager.getFinancialsPostingBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.postRecordTypes.clear();
        this.postRecordTypes.clear();

        this.postSearchRecordTypes.clear();
        this.postSearchRecordTypes.clear();

        this.postEntryRecordTypes.clear();
        this.postEntryRecordTypes.clear();

        this.postEntrySearchRecordTypes.clear();
        this.postEntrySearchRecordTypes.clear();

        return;
    }
}
