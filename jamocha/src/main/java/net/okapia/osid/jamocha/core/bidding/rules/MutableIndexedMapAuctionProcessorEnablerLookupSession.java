//
// MutableIndexedMapAuctionProcessorEnablerLookupSession
//
//    Implements an AuctionProcessorEnabler lookup service backed by a collection of
//    auctionProcessorEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules;


/**
 *  Implements an AuctionProcessorEnabler lookup service backed by a collection of
 *  auction processor enablers. The auction processor enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some auction processor enablers may be compatible
 *  with more types than are indicated through these auction processor enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of auction processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapAuctionProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.bidding.rules.spi.AbstractIndexedMapAuctionProcessorEnablerLookupSession
    implements org.osid.bidding.rules.AuctionProcessorEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapAuctionProcessorEnablerLookupSession} with no auction processor enablers.
     *
     *  @param auctionHouse the auction house
     *  @throws org.osid.NullArgumentException {@code auctionHouse}
     *          is {@code null}
     */

      public MutableIndexedMapAuctionProcessorEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse) {
        setAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAuctionProcessorEnablerLookupSession} with a
     *  single auction processor enabler.
     *  
     *  @param auctionHouse the auction house
     *  @param  auctionProcessorEnabler an single auctionProcessorEnabler
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionProcessorEnabler} is {@code null}
     */

    public MutableIndexedMapAuctionProcessorEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler) {
        this(auctionHouse);
        putAuctionProcessorEnabler(auctionProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAuctionProcessorEnablerLookupSession} using an
     *  array of auction processor enablers.
     *
     *  @param auctionHouse the auction house
     *  @param  auctionProcessorEnablers an array of auction processor enablers
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionProcessorEnablers} is {@code null}
     */

    public MutableIndexedMapAuctionProcessorEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.bidding.rules.AuctionProcessorEnabler[] auctionProcessorEnablers) {
        this(auctionHouse);
        putAuctionProcessorEnablers(auctionProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAuctionProcessorEnablerLookupSession} using a
     *  collection of auction processor enablers.
     *
     *  @param auctionHouse the auction house
     *  @param  auctionProcessorEnablers a collection of auction processor enablers
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionProcessorEnablers} is {@code null}
     */

    public MutableIndexedMapAuctionProcessorEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  java.util.Collection<? extends org.osid.bidding.rules.AuctionProcessorEnabler> auctionProcessorEnablers) {

        this(auctionHouse);
        putAuctionProcessorEnablers(auctionProcessorEnablers);
        return;
    }
    

    /**
     *  Makes an {@code AuctionProcessorEnabler} available in this session.
     *
     *  @param  auctionProcessorEnabler an auction processor enabler
     *  @throws org.osid.NullArgumentException {@code auctionProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionProcessorEnabler(org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler) {
        super.putAuctionProcessorEnabler(auctionProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of auction processor enablers available in this session.
     *
     *  @param  auctionProcessorEnablers an array of auction processor enablers
     *  @throws org.osid.NullArgumentException {@code auctionProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putAuctionProcessorEnablers(org.osid.bidding.rules.AuctionProcessorEnabler[] auctionProcessorEnablers) {
        super.putAuctionProcessorEnablers(auctionProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of auction processor enablers available in this session.
     *
     *  @param  auctionProcessorEnablers a collection of auction processor enablers
     *  @throws org.osid.NullArgumentException {@code auctionProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionProcessorEnablers(java.util.Collection<? extends org.osid.bidding.rules.AuctionProcessorEnabler> auctionProcessorEnablers) {
        super.putAuctionProcessorEnablers(auctionProcessorEnablers);
        return;
    }


    /**
     *  Removes an AuctionProcessorEnabler from this session.
     *
     *  @param auctionProcessorEnablerId the {@code Id} of the auction processor enabler
     *  @throws org.osid.NullArgumentException {@code auctionProcessorEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAuctionProcessorEnabler(org.osid.id.Id auctionProcessorEnablerId) {
        super.removeAuctionProcessorEnabler(auctionProcessorEnablerId);
        return;
    }    
}
