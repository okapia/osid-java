//
// PositionMiter.java
//
//     Defines a Position miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.position;


/**
 *  Defines a <code>Position</code> miter for use with the builders.
 */

public interface PositionMiter
    extends net.okapia.osid.jamocha.builder.spi.TemporalOsidObjectMiter,
            org.osid.personnel.Position {


    /**
     *  Sets the organization.
     *
     *  @param organization an organization
     *  @throws org.osid.NullArgumentException
     *          <code>organization</code> is <code>null</code>
     */

    public void setOrganization(org.osid.personnel.Organization organization);


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public void setTitle(org.osid.locale.DisplayText title);


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    public void setLevel(org.osid.grading.Grade level);


    /**
     *  Adds a qualification.
     *
     *  @param qualification a qualification
     *  @throws org.osid.NullArgumentException
     *          <code>qualification</code> is <code>null</code>
     */

    public void addQualification(org.osid.learning.Objective qualification);


    /**
     *  Sets all the qualifications.
     *
     *  @param qualifications a collection of qualifications
     *  @throws org.osid.NullArgumentException
     *          <code>qualifications</code> is <code>null</code>
     */

    public void setQualifications(java.util.Collection<org.osid.learning.Objective> qualifications);


    /**
     *  Sets the target appointments.
     *
     *  @param appointments a target appointments
     *  @throws org.osid.InvalidArgumentException
     *          <code>appointment</code> is negative
     */

    public void setTargetAppointments(long appointments);


    /**
     *  Sets the required commitment.
     *
     *  @param percentage a required commitment (0-100)
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    public void setRequiredCommitment(long percentage);


    /**
     *  Sets the low salary range.
     *
     *  @param salary a low salary range
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    public void setLowSalaryRange(org.osid.financials.Currency salary);


    /**
     *  Sets the midpoint salary range.
     *
     *  @param salary a midpoint salary range
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    public void setMidpointSalaryRange(org.osid.financials.Currency salary);


    /**
     *  Sets the high salary range.
     *
     *  @param salary a high salary range
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    public void setHighSalaryRange(org.osid.financials.Currency salary);


    /**
     *  Sets the compensation frequency.
     *
     *  @param frequency a compensation frequency
     *  @throws org.osid.NullArgumentException <code>frequency</code>
     *          is <code>null</code>
     */

    public void setCompensationFrequency(org.osid.calendaring.Duration frequency);


    /**
     *  Sets the exempt flag.
     *
     *  @param exempt <code> true </code> if this position is exempt,
     *         <code> false </code> is non-exempt
     */

    public void setExempt(boolean exempt);


    /**
     *  Sets the benefits type.
     *
     *  @param benefitsType a benefits type
     *  @throws org.osid.NullArgumentException
     *          <code>benefitsType</code> is <code>null</code>
     */

    public void setBenefitsType(org.osid.type.Type benefitsType);


    /**
     *  Adds a Position record.
     *
     *  @param record a position record
     *  @param recordType the type of position record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPositionRecord(org.osid.personnel.records.PositionRecord record, org.osid.type.Type recordType);
}       


