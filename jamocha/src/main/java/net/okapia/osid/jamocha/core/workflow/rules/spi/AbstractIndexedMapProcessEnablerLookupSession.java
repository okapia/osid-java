//
// AbstractIndexedMapProcessEnablerLookupSession.java
//
//    A simple framework for providing a ProcessEnabler lookup service
//    backed by a fixed collection of process enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ProcessEnabler lookup service backed by a
 *  fixed collection of process enablers. The process enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some process enablers may be compatible
 *  with more types than are indicated through these process enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProcessEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProcessEnablerLookupSession
    extends AbstractMapProcessEnablerLookupSession
    implements org.osid.workflow.rules.ProcessEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.ProcessEnabler> processEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.ProcessEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.ProcessEnabler> processEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.ProcessEnabler>());


    /**
     *  Makes a <code>ProcessEnabler</code> available in this session.
     *
     *  @param  processEnabler a process enabler
     *  @throws org.osid.NullArgumentException <code>processEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProcessEnabler(org.osid.workflow.rules.ProcessEnabler processEnabler) {
        super.putProcessEnabler(processEnabler);

        this.processEnablersByGenus.put(processEnabler.getGenusType(), processEnabler);
        
        try (org.osid.type.TypeList types = processEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.processEnablersByRecord.put(types.getNextType(), processEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a process enabler from this session.
     *
     *  @param processEnablerId the <code>Id</code> of the process enabler
     *  @throws org.osid.NullArgumentException <code>processEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProcessEnabler(org.osid.id.Id processEnablerId) {
        org.osid.workflow.rules.ProcessEnabler processEnabler;
        try {
            processEnabler = getProcessEnabler(processEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.processEnablersByGenus.remove(processEnabler.getGenusType());

        try (org.osid.type.TypeList types = processEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.processEnablersByRecord.remove(types.getNextType(), processEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProcessEnabler(processEnablerId);
        return;
    }


    /**
     *  Gets a <code>ProcessEnablerList</code> corresponding to the given
     *  process enabler genus <code>Type</code> which does not include
     *  process enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known process enablers or an error results. Otherwise,
     *  the returned list may contain only those process enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  processEnablerGenusType a process enabler genus type 
     *  @return the returned <code>ProcessEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByGenusType(org.osid.type.Type processEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.processenabler.ArrayProcessEnablerList(this.processEnablersByGenus.get(processEnablerGenusType)));
    }


    /**
     *  Gets a <code>ProcessEnablerList</code> containing the given
     *  process enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known process enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  process enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  processEnablerRecordType a process enabler record type 
     *  @return the returned <code>processEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByRecordType(org.osid.type.Type processEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.processenabler.ArrayProcessEnablerList(this.processEnablersByRecord.get(processEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.processEnablersByGenus.clear();
        this.processEnablersByRecord.clear();

        super.close();

        return;
    }
}
