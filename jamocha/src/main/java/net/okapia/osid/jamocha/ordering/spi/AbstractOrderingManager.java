//
// AbstractOrderingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractOrderingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.ordering.OrderingManager,
               org.osid.ordering.OrderingProxyManager {

    private final Types orderRecordTypes                   = new TypeRefSet();
    private final Types orderSearchRecordTypes             = new TypeRefSet();

    private final Types itemRecordTypes                    = new TypeRefSet();
    private final Types itemSearchRecordTypes              = new TypeRefSet();

    private final Types productRecordTypes                 = new TypeRefSet();
    private final Types productSearchRecordTypes           = new TypeRefSet();

    private final Types priceScheduleRecordTypes           = new TypeRefSet();
    private final Types priceScheduleSearchRecordTypes     = new TypeRefSet();

    private final Types priceRecordTypes                   = new TypeRefSet();
    private final Types storeRecordTypes                   = new TypeRefSet();
    private final Types storeSearchRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractOrderingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractOrderingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any item federation is exposed. Federation is exposed when a 
     *  specific item may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of items 
     *  appears as a single item. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a my order lookup service. 
     *
     *  @return <code> true </code> if my order lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyOrder() {
        return (false);
    }


    /**
     *  Tests for the availability of an order lookup service. 
     *
     *  @return <code> true </code> if order lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderLookup() {
        return (false);
    }


    /**
     *  Tests if querying orders is available. 
     *
     *  @return <code> true </code> if order query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderQuery() {
        return (false);
    }


    /**
     *  Tests if searching for orders is available. 
     *
     *  @return <code> true </code> if order search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderSearch() {
        return (false);
    }


    /**
     *  Tests if searching for orders is available. 
     *
     *  @return <code> true </code> if order search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderAdmin() {
        return (false);
    }


    /**
     *  Tests if order notification is available. 
     *
     *  @return <code> true </code> if order notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderNotification() {
        return (false);
    }


    /**
     *  Tests if an order to store lookup session is available. 
     *
     *  @return <code> true </code> if order store lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderStore() {
        return (false);
    }


    /**
     *  Tests if an order to store assignment session is available. 
     *
     *  @return <code> true </code> if order store assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderStoreAssignment() {
        return (false);
    }


    /**
     *  Tests if an order smart store session is available. 
     *
     *  @return <code> true </code> if order smart store is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderSmartStore() {
        return (false);
    }


    /**
     *  Tests if an item admin session is available. 
     *
     *  @return <code> true </code> if an item admin session is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a product lookup service. 
     *
     *  @return <code> true </code> if product lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductLookup() {
        return (false);
    }


    /**
     *  Tests if querying products is available. 
     *
     *  @return <code> true </code> if product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (false);
    }


    /**
     *  Tests if searching for products is available. 
     *
     *  @return <code> true </code> if product search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a product administrative service for 
     *  creating and deleting products. 
     *
     *  @return <code> true </code> if product administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a product notification service. 
     *
     *  @return <code> true </code> if product notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductNotification() {
        return (false);
    }


    /**
     *  Tests if a product to store lookup session is available. 
     *
     *  @return <code> true </code> if product store lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductStore() {
        return (false);
    }


    /**
     *  Tests if a product to store assignment session is available. 
     *
     *  @return <code> true </code> if product store assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductStoreAssignment() {
        return (false);
    }


    /**
     *  Tests if a product smart store session is available. 
     *
     *  @return <code> true </code> if product smart store is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductSmartStore() {
        return (false);
    }


    /**
     *  Tests for the availability of a price schedule lookup service. 
     *
     *  @return <code> true </code> if price schedule lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleLookup() {
        return (false);
    }


    /**
     *  Tests if querying price schedules is available. 
     *
     *  @return <code> true </code> if price schedule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleQuery() {
        return (false);
    }


    /**
     *  Tests if searching for price schedules is available. 
     *
     *  @return <code> true </code> if price schedule search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a price schedule administrative service 
     *  for creating and deleting price schedules. 
     *
     *  @return <code> true </code> if price schedule administration is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a price schedule notification service. 
     *
     *  @return <code> true </code> if price schedule notification is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleNotification() {
        return (false);
    }


    /**
     *  Tests if a price schedule to store lookup session is available. 
     *
     *  @return <code> true </code> if price schedule store lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleStore() {
        return (false);
    }


    /**
     *  Tests if a price schedule to store assignment session is available. 
     *
     *  @return <code> true </code> if price schedule store assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleStoreAssignment() {
        return (false);
    }


    /**
     *  Tests if a price schedule smart store session is available. 
     *
     *  @return <code> true </code> if price schedule smart store is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleSmartStore() {
        return (false);
    }


    /**
     *  Tests for the availability of an store lookup service. 
     *
     *  @return <code> true </code> if store lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreLookup() {
        return (false);
    }


    /**
     *  Tests if querying stores is available. 
     *
     *  @return <code> true </code> if store query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Tests if searching for stores is available. 
     *
     *  @return <code> true </code> if store search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a store administrative service for 
     *  creating and deleting stores. 
     *
     *  @return <code> true </code> if store administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a store notification service. 
     *
     *  @return <code> true </code> if store notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a store hierarchy traversal service. 
     *
     *  @return <code> true </code> if store hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a store hierarchy design service. 
     *
     *  @return <code> true </code> if store hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of an ordering batch service. 
     *
     *  @return <code> true </code> if an ordering batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderingBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of an ordering rules service. 
     *
     *  @return <code> true </code> if an ordering rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderingRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Order </code> record types. 
     *
     *  @return a list containing the supported order record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOrderRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.orderRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Order </code> record type is supported. 
     *
     *  @param  orderRecordType a <code> Type </code> indicating an <code> 
     *          Order </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> orderRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOrderRecordType(org.osid.type.Type orderRecordType) {
        return (this.orderRecordTypes.contains(orderRecordType));
    }


    /**
     *  Adds support for an order record type.
     *
     *  @param orderRecordType an order record type
     *  @throws org.osid.NullArgumentException
     *  <code>orderRecordType</code> is <code>null</code>
     */

    protected void addOrderRecordType(org.osid.type.Type orderRecordType) {
        this.orderRecordTypes.add(orderRecordType);
        return;
    }


    /**
     *  Removes support for an order record type.
     *
     *  @param orderRecordType an order record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>orderRecordType</code> is <code>null</code>
     */

    protected void removeOrderRecordType(org.osid.type.Type orderRecordType) {
        this.orderRecordTypes.remove(orderRecordType);
        return;
    }


    /**
     *  Gets the supported order search record types. 
     *
     *  @return a list containing the supported order search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOrderSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.orderSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given order search record type is supported. 
     *
     *  @param  orderSearchRecordType a <code> Type </code> indicating an 
     *          order record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> orderSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOrderSearchRecordType(org.osid.type.Type orderSearchRecordType) {
        return (this.orderSearchRecordTypes.contains(orderSearchRecordType));
    }


    /**
     *  Adds support for an order search record type.
     *
     *  @param orderSearchRecordType an order search record type
     *  @throws org.osid.NullArgumentException
     *  <code>orderSearchRecordType</code> is <code>null</code>
     */

    protected void addOrderSearchRecordType(org.osid.type.Type orderSearchRecordType) {
        this.orderSearchRecordTypes.add(orderSearchRecordType);
        return;
    }


    /**
     *  Removes support for an order search record type.
     *
     *  @param orderSearchRecordType an order search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>orderSearchRecordType</code> is <code>null</code>
     */

    protected void removeOrderSearchRecordType(org.osid.type.Type orderSearchRecordType) {
        this.orderSearchRecordTypes.remove(orderSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Item </code> record types. 
     *
     *  @return a list containing the supported item record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.itemRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Item </code> record type is supported. 
     *
     *  @param  itemRecordType a <code> Type </code> indicating a <code> Item 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemRecordType(org.osid.type.Type itemRecordType) {
        return (this.itemRecordTypes.contains(itemRecordType));
    }


    /**
     *  Adds support for an item record type.
     *
     *  @param itemRecordType an item record type
     *  @throws org.osid.NullArgumentException
     *  <code>itemRecordType</code> is <code>null</code>
     */

    protected void addItemRecordType(org.osid.type.Type itemRecordType) {
        this.itemRecordTypes.add(itemRecordType);
        return;
    }


    /**
     *  Removes support for an item record type.
     *
     *  @param itemRecordType an item record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>itemRecordType</code> is <code>null</code>
     */

    protected void removeItemRecordType(org.osid.type.Type itemRecordType) {
        this.itemRecordTypes.remove(itemRecordType);
        return;
    }


    /**
     *  Gets the supported item search record types. 
     *
     *  @return a list containing the supported item search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.itemSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given item search record type is supported. 
     *
     *  @param  itemSearchRecordType a <code> Type </code> indicating an item 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        return (this.itemSearchRecordTypes.contains(itemSearchRecordType));
    }


    /**
     *  Adds support for an item search record type.
     *
     *  @param itemSearchRecordType an item search record type
     *  @throws org.osid.NullArgumentException
     *  <code>itemSearchRecordType</code> is <code>null</code>
     */

    protected void addItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        this.itemSearchRecordTypes.add(itemSearchRecordType);
        return;
    }


    /**
     *  Removes support for an item search record type.
     *
     *  @param itemSearchRecordType an item search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>itemSearchRecordType</code> is <code>null</code>
     */

    protected void removeItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        this.itemSearchRecordTypes.remove(itemSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Product </code> record types. 
     *
     *  @return a list containing the supported product record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProductRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.productRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Product </code> record type is supported. 
     *
     *  @param  productRecordType a <code> Type </code> indicating a <code> 
     *          Product </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> productRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProductRecordType(org.osid.type.Type productRecordType) {
        return (this.productRecordTypes.contains(productRecordType));
    }


    /**
     *  Adds support for a product record type.
     *
     *  @param productRecordType a product record type
     *  @throws org.osid.NullArgumentException
     *  <code>productRecordType</code> is <code>null</code>
     */

    protected void addProductRecordType(org.osid.type.Type productRecordType) {
        this.productRecordTypes.add(productRecordType);
        return;
    }


    /**
     *  Removes support for a product record type.
     *
     *  @param productRecordType a product record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>productRecordType</code> is <code>null</code>
     */

    protected void removeProductRecordType(org.osid.type.Type productRecordType) {
        this.productRecordTypes.remove(productRecordType);
        return;
    }


    /**
     *  Gets the supported product search record types. 
     *
     *  @return a list containing the supported product search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProductSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.productSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given product search record type is supported. 
     *
     *  @param  productSearchRecordType a <code> Type </code> indicating a 
     *          product record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> productSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProductSearchRecordType(org.osid.type.Type productSearchRecordType) {
        return (this.productSearchRecordTypes.contains(productSearchRecordType));
    }


    /**
     *  Adds support for a product search record type.
     *
     *  @param productSearchRecordType a product search record type
     *  @throws org.osid.NullArgumentException
     *  <code>productSearchRecordType</code> is <code>null</code>
     */

    protected void addProductSearchRecordType(org.osid.type.Type productSearchRecordType) {
        this.productSearchRecordTypes.add(productSearchRecordType);
        return;
    }


    /**
     *  Removes support for a product search record type.
     *
     *  @param productSearchRecordType a product search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>productSearchRecordType</code> is <code>null</code>
     */

    protected void removeProductSearchRecordType(org.osid.type.Type productSearchRecordType) {
        this.productSearchRecordTypes.remove(productSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> PriceSchedule </code> record types. 
     *
     *  @return a list containing the supported price schedule record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceScheduleRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.priceScheduleRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> PriceSchedule </code> record type is 
     *  supported. 
     *
     *  @param  priceScheduleRecordType a <code> Type </code> indicating a 
     *          <code> PriceSchedule </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priceScheduleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceScheduleRecordType(org.osid.type.Type priceScheduleRecordType) {
        return (this.priceScheduleRecordTypes.contains(priceScheduleRecordType));
    }


    /**
     *  Adds support for a price schedule record type.
     *
     *  @param priceScheduleRecordType a price schedule record type
     *  @throws org.osid.NullArgumentException
     *  <code>priceScheduleRecordType</code> is <code>null</code>
     */

    protected void addPriceScheduleRecordType(org.osid.type.Type priceScheduleRecordType) {
        this.priceScheduleRecordTypes.add(priceScheduleRecordType);
        return;
    }


    /**
     *  Removes support for a price schedule record type.
     *
     *  @param priceScheduleRecordType a price schedule record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>priceScheduleRecordType</code> is <code>null</code>
     */

    protected void removePriceScheduleRecordType(org.osid.type.Type priceScheduleRecordType) {
        this.priceScheduleRecordTypes.remove(priceScheduleRecordType);
        return;
    }


    /**
     *  Gets the supported price schedule search record types. 
     *
     *  @return a list containing the supported price schedule search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceScheduleSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.priceScheduleSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given price schedule search record type is supported. 
     *
     *  @param  priceScheduleSearchRecordType a <code> Type </code> indicating 
     *          a price schedule record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          priceScheduleSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceScheduleSearchRecordType(org.osid.type.Type priceScheduleSearchRecordType) {
        return (this.priceScheduleSearchRecordTypes.contains(priceScheduleSearchRecordType));
    }


    /**
     *  Adds support for a price schedule search record type.
     *
     *  @param priceScheduleSearchRecordType a price schedule search record type
     *  @throws org.osid.NullArgumentException
     *  <code>priceScheduleSearchRecordType</code> is <code>null</code>
     */

    protected void addPriceScheduleSearchRecordType(org.osid.type.Type priceScheduleSearchRecordType) {
        this.priceScheduleSearchRecordTypes.add(priceScheduleSearchRecordType);
        return;
    }


    /**
     *  Removes support for a price schedule search record type.
     *
     *  @param priceScheduleSearchRecordType a price schedule search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>priceScheduleSearchRecordType</code> is <code>null</code>
     */

    protected void removePriceScheduleSearchRecordType(org.osid.type.Type priceScheduleSearchRecordType) {
        this.priceScheduleSearchRecordTypes.remove(priceScheduleSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Price </code> record types. 
     *
     *  @return a list containing the supported price record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.priceRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Price </code> record type is supported. 
     *
     *  @param  priceRecordType a <code> Type </code> indicating a <code> 
     *          Price </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priceRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceRecordType(org.osid.type.Type priceRecordType) {
        return (this.priceRecordTypes.contains(priceRecordType));
    }


    /**
     *  Adds support for a price record type.
     *
     *  @param priceRecordType a price record type
     *  @throws org.osid.NullArgumentException
     *  <code>priceRecordType</code> is <code>null</code>
     */

    protected void addPriceRecordType(org.osid.type.Type priceRecordType) {
        this.priceRecordTypes.add(priceRecordType);
        return;
    }


    /**
     *  Removes support for a price record type.
     *
     *  @param priceRecordType a price record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>priceRecordType</code> is <code>null</code>
     */

    protected void removePriceRecordType(org.osid.type.Type priceRecordType) {
        this.priceRecordTypes.remove(priceRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Store </code> record types. 
     *
     *  @return a list containing the supported store record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStoreRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.storeRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Store </code> record type is supported. 
     *
     *  @param  storeRecordType a <code> Type </code> indicating a <code> 
     *          Store </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> storeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStoreRecordType(org.osid.type.Type storeRecordType) {
        return (this.storeRecordTypes.contains(storeRecordType));
    }


    /**
     *  Adds support for a store record type.
     *
     *  @param storeRecordType a store record type
     *  @throws org.osid.NullArgumentException
     *  <code>storeRecordType</code> is <code>null</code>
     */

    protected void addStoreRecordType(org.osid.type.Type storeRecordType) {
        this.storeRecordTypes.add(storeRecordType);
        return;
    }


    /**
     *  Removes support for a store record type.
     *
     *  @param storeRecordType a store record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>storeRecordType</code> is <code>null</code>
     */

    protected void removeStoreRecordType(org.osid.type.Type storeRecordType) {
        this.storeRecordTypes.remove(storeRecordType);
        return;
    }


    /**
     *  Gets the supported store search record types. 
     *
     *  @return a list containing the supported store search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStoreSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.storeSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given store search record type is supported. 
     *
     *  @param  storeSearchRecordType a <code> Type </code> indicating a store 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> storeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStoreSearchRecordType(org.osid.type.Type storeSearchRecordType) {
        return (this.storeSearchRecordTypes.contains(storeSearchRecordType));
    }


    /**
     *  Adds support for a store search record type.
     *
     *  @param storeSearchRecordType a store search record type
     *  @throws org.osid.NullArgumentException
     *  <code>storeSearchRecordType</code> is <code>null</code>
     */

    protected void addStoreSearchRecordType(org.osid.type.Type storeSearchRecordType) {
        this.storeSearchRecordTypes.add(storeSearchRecordType);
        return;
    }


    /**
     *  Removes support for a store search record type.
     *
     *  @param storeSearchRecordType a store search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>storeSearchRecordType</code> is <code>null</code>
     */

    protected void removeStoreSearchRecordType(org.osid.type.Type storeSearchRecordType) {
        this.storeSearchRecordTypes.remove(storeSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service for the authenticated agent. 
     *
     *  @return a <code> My </code> Order <code> Session </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyOrder() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.MyOrderSession getMyOrderSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getMyOrderSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service for the authenticated agent. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> My </code> Order <code> Session </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyOrder() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.MyOrderSession getMyOrderSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getMyOrderSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service for the authenticated agent for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> MyOrderSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyOrder() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.MyOrderSession getMyOrderSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getMyOrderSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service for the authenticated agent for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MyOrderSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyOrder() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.MyOrderSession getMyOrderSessionForStore(org.osid.id.Id storeId, 
                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getMyOrderSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service. 
     *
     *  @return an <code> OrderLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderLookupSession getOrderLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderLookupSession getOrderLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return an <code> OrderLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderLookupSession getOrderLookupSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderLookupSession getOrderLookupSessionForStore(org.osid.id.Id storeId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order query 
     *  service. 
     *
     *  @return an <code> OrderQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuerySession getOrderQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuerySession getOrderQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order query 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return an <code> OrderQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuerySession getOrderQuerySessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderQuerySessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order query 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuerySession getOrderQuerySessionForStore(org.osid.id.Id storeId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderQuerySessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order search 
     *  service. 
     *
     *  @return an <code> OrderSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSearchSession getOrderSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSearchSession getOrderSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order search 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return an <code> OrderSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSearchSession getOrderSearchSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderSearchSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order search 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSearchSession getOrderSearchSessionForStore(org.osid.id.Id storeId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderSearchSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  administration service. 
     *
     *  @return an <code> OrderAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderAdminSession getOrderAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderAdminSession getOrderAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return an <code> OrderAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderAdminSession getOrderAdminSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderAdminSession getOrderAdminSessionForStore(org.osid.id.Id storeId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  notification service. 
     *
     *  @param  orderReceiver the receiver 
     *  @return an <code> OrderNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> orderReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderNotificationSession getOrderNotificationSession(org.osid.ordering.OrderReceiver orderReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  notification service. 
     *
     *  @param  orderReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> OrderNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> orderReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderNotificationSession getOrderNotificationSession(org.osid.ordering.OrderReceiver orderReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  notification service for the given store. 
     *
     *  @param  orderReceiver the receiver 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return an <code> OrderNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> orderReceiver </code> or 
     *          <code> storeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderNotificationSession getOrderNotificationSessionForStore(org.osid.ordering.OrderReceiver orderReceiver, 
                                                                                          org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderNotificationSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  notification service for the given store. 
     *
     *  @param  orderReceiver the receiver 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> orderReceiver, storeId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderNotificationSession getOrderNotificationSessionForStore(org.osid.ordering.OrderReceiver orderReceiver, 
                                                                                          org.osid.id.Id storeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderNotificationSessionForStore not implemented");
    }


    /**
     *  Gets the session for retrieving odrer to store mappings. 
     *
     *  @return an <code> OrderStoreSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderStore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderStoreSession getOrderStoreSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderStoreSession not implemented");
    }


    /**
     *  Gets the session for retrieving odrer to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderStoreSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderStore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderStoreSession getOrderStoreSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderStoreSession not implemented");
    }


    /**
     *  Gets the session for assigning order to store mappings. 
     *
     *  @return an <code> OrderStoreAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderStoreAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderStoreAssignmentSession getOrderStoreAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderStoreAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning order to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderStoreAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderStoreAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderStoreAssignmentSession getOrderStoreAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderStoreAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the order smart store for the given 
     *  store. 
     *
     *  @param  storeId the <code> Id </code> of the store 
     *  @return an <code> OrderSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException <code> storeId </code> not found 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderSmartStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSmartStoreSession getOrderSmartStoreSession(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderSmartStoreSession not implemented");
    }


    /**
     *  Gets the session associated with the order smart store for the given 
     *  store. 
     *
     *  @param  storeId the <code> Id </code> of the store 
     *  @param  proxy a proxy 
     *  @return an <code> OrderSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException <code> storeId </code> not found 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderSmartStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSmartStoreSession getOrderSmartStoreSession(org.osid.id.Id storeId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderSmartStoreSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  adminsitartive service. 
     *
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemAdminSession getItemAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getItemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  adminsitartive service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemAdminSession getItemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getItemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item admin 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemAdminSession getItemAdminSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getItemAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item admin 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemAdminSession getItemAdminSessionForStore(org.osid.id.Id storeId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getItemAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product lookup 
     *  service. 
     *
     *  @return a <code> ProductLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductLookupSession getProductLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductLookupSession getProductLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product lookup 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> ProductLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductLookupSession getProductLookupSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product lookup 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductLookupSession getProductLookupSessionForStore(org.osid.id.Id storeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product query 
     *  service. 
     *
     *  @return a <code> ProductQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuerySession getProductQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuerySession getProductQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product query 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> ProductQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuerySession getProductQuerySessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductQuerySessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product query 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuerySession getProductQuerySessionForStore(org.osid.id.Id storeId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductQuerySessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product search 
     *  service. 
     *
     *  @return a <code> ProductSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSearchSession getProductSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSearchSession getProductSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product search 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> ProductSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSearchSession getProductSearchSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductSearchSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product search 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSearchSession getProductSearchSessionForStore(org.osid.id.Id storeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductSearchSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  administrative service. 
     *
     *  @return a <code> ProductAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductAdminSession getProductAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductAdminSession getProductAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  administrative service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> ProductAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductAdminSession getProductAdminSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  administrative service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductAdminSession getProductAdminSessionForStore(org.osid.id.Id storeId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  notification service. 
     *
     *  @param  productReceiver the receiver 
     *  @return a <code> ProductNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> producteReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductNotificationSession getProductNotificationSession(org.osid.ordering.ProductReceiver productReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  notification service. 
     *
     *  @param  productReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProductNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> producteReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductNotificationSession getProductNotificationSession(org.osid.ordering.ProductReceiver productReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  notification service for the given store. 
     *
     *  @param  productReceiver the receiver 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> ProductNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> productReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductNotificationSession getProductNotificationSessionForStore(org.osid.ordering.ProductReceiver productReceiver, 
                                                                                              org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductNotificationSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  notification service for the given store. 
     *
     *  @param  productReceiver the receiver 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> productReceiver, storeId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductNotificationSession getProductNotificationSessionForStore(org.osid.ordering.ProductReceiver productReceiver, 
                                                                                              org.osid.id.Id storeId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductNotificationSessionForStore not implemented");
    }


    /**
     *  Gets the session for retrieving product to store mappings. 
     *
     *  @return a <code> ProductStoreSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductStore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductStoreSession getProductStoreSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductStoreSession not implemented");
    }


    /**
     *  Gets the session for retrieving product to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductStoreSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductStore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductStoreSession getProductStoreSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductStoreSession not implemented");
    }


    /**
     *  Gets the session for assigning product to store mappings. 
     *
     *  @return a <code> ProductStoreAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductStoreAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductStoreAssignmentSession gerProductStoreAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.gerProductStoreAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning product to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductStoreAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductStoreAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductStoreAssignmentSession gerProductStoreAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.gerProductStoreAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the product smart store for the given 
     *  store. 
     *
     *  @param  storeId the <code> Id </code> of the store 
     *  @return a <code> ProductSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException <code> storeId </code> not found 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductSmartStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSmartStoreSession getProductSmartStoreSession(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getProductSmartStoreSession not implemented");
    }


    /**
     *  Gets the session associated with the product smart store for the given 
     *  store. 
     *
     *  @param  storeId the <code> Id </code> of the store 
     *  @param  proxy a proxy 
     *  @return a <code> ProductSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException <code> storeId </code> not found 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductSmartStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSmartStoreSession getProductSmartStoreSession(org.osid.id.Id storeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getProductSmartStoreSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  lookup service. 
     *
     *  @return a <code> PriceScheduleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleLookupSession getPriceScheduleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleLookupSession getPriceScheduleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  lookup service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceScheduleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleLookupSession getPriceScheduleLookupSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  lookup service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleLookupSession getPriceScheduleLookupSessionForStore(org.osid.id.Id storeId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  query service. 
     *
     *  @return a <code> PriceScheduleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuerySession getPriceScheduleQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuerySession getPriceScheduleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  query service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceScheduleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuerySession getPriceScheduleQuerySessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleQuerySessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  query service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuerySession getPriceScheduleQuerySessionForStore(org.osid.id.Id storeId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleQuerySessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  search service. 
     *
     *  @return a <code> PriceScheduleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSearchSession getPriceScheduleSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSearchSession getPriceScheduleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  search service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceScheduleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSearchSession getPriceScheduleSearchSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleSearchSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  search service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSearchSession getPriceScheduleSearchSessionForStore(org.osid.id.Id storeId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleSearchSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  administrative service. 
     *
     *  @return a <code> PriceScheduleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleAdminSession getPriceScheduleAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleAdminSession getPriceScheduleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  administrative service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceScheduleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleAdminSession getPriceScheduleAdminSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  administrative service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleAdminSession getPriceScheduleAdminSessionForStore(org.osid.id.Id storeId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  notification service. 
     *
     *  @param  priceScheduleReceiver the receiver 
     *  @return a <code> PriceScheduleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> priceScheduleReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleNotificationSession getPriceScheduleNotificationSession(org.osid.ordering.PriceScheduleReceiver priceScheduleReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  notification service. 
     *
     *  @param  priceScheduleReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> priceScheduleReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleNotificationSession getPriceScheduleNotificationSession(org.osid.ordering.PriceScheduleReceiver priceScheduleReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  notification service for the given store. 
     *
     *  @param  priceScheduleReceiver the receiver 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceScheduleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> priceScheduleReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleNotificationSession getPriceScheduleNotificationSessionForStore(org.osid.ordering.PriceScheduleReceiver priceScheduleReceiver, 
                                                                                                          org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleNotificationSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  notification service for the given store. 
     *
     *  @param  priceScheduleReceiver the receiver 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> priceScheduleReceiver, 
     *          storeId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleNotificationSession getPriceScheduleNotificationSessionForStore(org.osid.ordering.PriceScheduleReceiver priceScheduleReceiver, 
                                                                                                          org.osid.id.Id storeId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleNotificationSessionForStore not implemented");
    }


    /**
     *  Gets the session for retrieving price schedule to store mappings. 
     *
     *  @return a <code> PriceScheduleStoreSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleStoreSession getPriceScheduleStoreSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleStoreSession not implemented");
    }


    /**
     *  Gets the session for retrieving price schedule to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleStoreSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleStoreSession getPriceScheduleStoreSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleStoreSession not implemented");
    }


    /**
     *  Gets the session for assigning price schedule to store mappings. 
     *
     *  @return a <code> PriceScheduleStoreAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleStoreAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleStoreAssignmentSession getPriceScheduleStoreAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleStoreAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning price schedule to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleStoreAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleStoreAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleStoreAssignmentSession getPriceScheduleStoreAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleStoreAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the price schedule smart store for 
     *  the given store. 
     *
     *  @param  storeId the <code> Id </code> of the store 
     *  @return a <code> PriceScheduleSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException <code> storeId </code> not found 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSmartStore() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSmartStoreSession getPriceScheduleSmartStoreSession(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getPriceScheduleSmartStoreSession not implemented");
    }


    /**
     *  Gets the session associated with the price schedule smart store for 
     *  the given store. 
     *
     *  @param  storeId the <code> Id </code> of the store 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException <code> storeId </code> not found 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSmartStore() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSmartStoreSession getPriceScheduleSmartStoreSession(org.osid.id.Id storeId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getPriceScheduleSmartStoreSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store lookup 
     *  service. 
     *
     *  @return a <code> StoreLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreLookupSession getStoreLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getStoreLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreLookupSession getStoreLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getStoreLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store query 
     *  service. 
     *
     *  @return a <code> StoreQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuerySession getStoreQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getStoreQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuerySession getStoreQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getStoreQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store search 
     *  service. 
     *
     *  @return a <code> StoreSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreSearchSession getStoreSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getStoreSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreSearchSession getStoreSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getStoreSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  administrative service. 
     *
     *  @return a <code> StoreAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreAdminSession getStoreAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getStoreAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreAdminSession getStoreAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getStoreAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  notification service. 
     *
     *  @param  storeReceiver the receiver 
     *  @return a <code> StoreNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> storeReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreNotificationSession getStoreNotificationSession(org.osid.ordering.StoreReceiver storeReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getStoreNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  notification service. 
     *
     *  @param  storeReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> StoreNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> storeReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreNotificationSession getStoreNotificationSession(org.osid.ordering.StoreReceiver storeReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getStoreNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  hierarchy service. 
     *
     *  @return a <code> StoreHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreHierarchySession getStoreHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getStoreHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreHierarchySession getStoreHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getStoreHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  hierarchy design service. 
     *
     *  @return a <code> StoreHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreHierarchyDesignSession getStoreHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getStoreHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreHierarchyDesignSession getStoreHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getStoreHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OrderingBatchManager. </code> 
     *
     *  @return an <code> OrderingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.OrderingBatchManager getOrderingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderingBatchManager not implemented");
    }


    /**
     *  Gets the <code> OrderingBatchProxyManager. </code> 
     *
     *  @return an <code> OrderingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.OrderingBatchProxyManager getOrderingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderingBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> OrderingRulesManager. </code> 
     *
     *  @return an <code> OrderingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.OrderingRulesManager getOrderingRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingManager.getOrderingRulesManager not implemented");
    }


    /**
     *  Gets the <code> OrderingRulesProxyManager. </code> 
     *
     *  @return an <code> OrderingRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.OrderingRulesProxyManager getOrderingRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.OrderingProxyManager.getOrderingRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.orderRecordTypes.clear();
        this.orderRecordTypes.clear();

        this.orderSearchRecordTypes.clear();
        this.orderSearchRecordTypes.clear();

        this.itemRecordTypes.clear();
        this.itemRecordTypes.clear();

        this.itemSearchRecordTypes.clear();
        this.itemSearchRecordTypes.clear();

        this.productRecordTypes.clear();
        this.productRecordTypes.clear();

        this.productSearchRecordTypes.clear();
        this.productSearchRecordTypes.clear();

        this.priceScheduleRecordTypes.clear();
        this.priceScheduleRecordTypes.clear();

        this.priceScheduleSearchRecordTypes.clear();
        this.priceScheduleSearchRecordTypes.clear();

        this.priceRecordTypes.clear();
        this.priceRecordTypes.clear();

        this.storeRecordTypes.clear();
        this.storeRecordTypes.clear();

        this.storeSearchRecordTypes.clear();
        this.storeSearchRecordTypes.clear();

        return;
    }
}
