//
// AbstractFederatingCanonicalUnitEnablerLookupSession.java
//
//     An abstract federating adapter for a CanonicalUnitEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CanonicalUnitEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCanonicalUnitEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.offering.rules.CanonicalUnitEnablerLookupSession>
    implements org.osid.offering.rules.CanonicalUnitEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();


    /**
     *  Constructs a new <code>AbstractFederatingCanonicalUnitEnablerLookupSession</code>.
     */

    protected AbstractFederatingCanonicalUnitEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.offering.rules.CanonicalUnitEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>CanonicalUnitEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCanonicalUnitEnablers() {
        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            if (session.canLookupCanonicalUnitEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>CanonicalUnitEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCanonicalUnitEnablerView() {
        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            session.useComparativeCanonicalUnitEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>CanonicalUnitEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCanonicalUnitEnablerView() {
        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            session.usePlenaryCanonicalUnitEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit enablers in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            session.useFederatedCatalogueView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            session.useIsolatedCatalogueView();
        }

        return;
    }


    /**
     *  Only active canonical unit enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitEnablerView() {
        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            session.useActiveCanonicalUnitEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive canonical unit enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitEnablerView() {
        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            session.useAnyStatusCanonicalUnitEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>CanonicalUnitEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CanonicalUnitEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CanonicalUnitEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, canonical unit enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical unit enablers are returned.
     *
     *  @param  canonicalUnitEnablerId <code>Id</code> of the
     *          <code>CanonicalUnitEnabler</code>
     *  @return the canonical unit enabler
     *  @throws org.osid.NotFoundException <code>canonicalUnitEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnabler getCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            try {
                return (session.getCanonicalUnitEnabler(canonicalUnitEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(canonicalUnitEnablerId + " not found");
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnitEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>CanonicalUnitEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, canonical unit enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical unit enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCanonicalUnitEnablers()</code>.
     *
     *  @param canonicalUnitEnablerIds the list of <code>Ids</code> to
     *         retrieve
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByIds(org.osid.id.IdList canonicalUnitEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.offering.rules.canonicalunitenabler.MutableCanonicalUnitEnablerList ret = new net.okapia.osid.jamocha.offering.rules.canonicalunitenabler.MutableCanonicalUnitEnablerList();

        try (org.osid.id.IdList ids = canonicalUnitEnablerIds) {
            while (ids.hasNext()) {
                ret.addCanonicalUnitEnabler(getCanonicalUnitEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> corresponding to
     *  the given canonical unit enabler genus <code>Type</code> which
     *  does not include canonical unit enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the
     *  returned list may contain only those canonical unit enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, canonical unit enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical unit enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCanonicalUnitEnablers()</code>.
     *
     *  @param  canonicalUnitEnablerGenusType a canonicalUnitEnabler genus type 
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByGenusType(org.osid.type.Type canonicalUnitEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitenabler.FederatingCanonicalUnitEnablerList ret = getCanonicalUnitEnablerList();

        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            ret.addCanonicalUnitEnablerList(session.getCanonicalUnitEnablersByGenusType(canonicalUnitEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> corresponding to
     *  the given canonical unit enabler genus <code>Type</code> and
     *  include any additional canonical unit enablers with genus
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the
     *  returned list may contain only those canonical unit enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, canonical unit enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical unit enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCanonicalUnitEnablers()</code>.
     *
     *  @param canonicalUnitEnablerGenusType a canonicalUnitEnabler
     *         genus type
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerGenusType</code> is
     *          <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByParentGenusType(org.osid.type.Type canonicalUnitEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitenabler.FederatingCanonicalUnitEnablerList ret = getCanonicalUnitEnablerList();

        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            ret.addCanonicalUnitEnablerList(session.getCanonicalUnitEnablersByParentGenusType(canonicalUnitEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> containing the
     *  given canonical unit enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the
     *  returned list may contain only those canonical unit enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, canonical unit enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical unit enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCanonicalUnitEnablers()</code>.
     *
     *  @param  canonicalUnitEnablerRecordType a canonicalUnitEnabler record type 
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByRecordType(org.osid.type.Type canonicalUnitEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitenabler.FederatingCanonicalUnitEnablerList ret = getCanonicalUnitEnablerList();

        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            ret.addCanonicalUnitEnablerList(session.getCanonicalUnitEnablersByRecordType(canonicalUnitEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the
     *  returned list may contain only those canonical unit enablers
     *  that are accessible through this session.
     *  
     *  In active mode, canonical unit enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical unit enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CanonicalUnitEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitenabler.FederatingCanonicalUnitEnablerList ret = getCanonicalUnitEnablerList();

        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            ret.addCanonicalUnitEnablerList(session.getCanonicalUnitEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>CanonicalUnitEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the
     *  returned list may contain only those canonical unit enablers
     *  that are accessible through this session.
     *
     *  In active mode, canonical unit enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical unit enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                                    org.osid.calendaring.DateTime from,
                                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitenabler.FederatingCanonicalUnitEnablerList ret = getCanonicalUnitEnablerList();

        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            ret.addCanonicalUnitEnablerList(session.getCanonicalUnitEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>CanonicalUnitEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the
     *  returned list may contain only those canonical unit enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, canonical unit enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical unit enablers are returned.
     *
     *  @return a list of <code>CanonicalUnitEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitenabler.FederatingCanonicalUnitEnablerList ret = getCanonicalUnitEnablerList();

        for (org.osid.offering.rules.CanonicalUnitEnablerLookupSession session : getSessions()) {
            ret.addCanonicalUnitEnablerList(session.getCanonicalUnitEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitenabler.FederatingCanonicalUnitEnablerList getCanonicalUnitEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitenabler.ParallelCanonicalUnitEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitenabler.CompositeCanonicalUnitEnablerList());
        }
    }
}
