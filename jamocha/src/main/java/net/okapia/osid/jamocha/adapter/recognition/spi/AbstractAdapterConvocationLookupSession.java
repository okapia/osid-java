//
// AbstractAdapterConvocationLookupSession.java
//
//    A Convocation lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recognition.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Convocation lookup session adapter.
 */

public abstract class AbstractAdapterConvocationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recognition.ConvocationLookupSession {

    private final org.osid.recognition.ConvocationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterConvocationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterConvocationLookupSession(org.osid.recognition.ConvocationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Academy/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Academy Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.session.getAcademyId());
    }


    /**
     *  Gets the {@code Academy} associated with this session.
     *
     *  @return the {@code Academy} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAcademy());
    }


    /**
     *  Tests if this user can perform {@code Convocation} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupConvocations() {
        return (this.session.canLookupConvocations());
    }


    /**
     *  A complete view of the {@code Convocation} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeConvocationView() {
        this.session.useComparativeConvocationView();
        return;
    }


    /**
     *  A complete view of the {@code Convocation} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryConvocationView() {
        this.session.usePlenaryConvocationView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include convocations in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.session.useFederatedAcademyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        this.session.useIsolatedAcademyView();
        return;
    }
    

    /**
     *  Only active convocations are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveConvocationView() {
        this.session.useActiveConvocationView();
        return;
    }


    /**
     *  Active and inactive convocations are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusConvocationView() {
        this.session.useAnyStatusConvocationView();
        return;
    }
    
     
    /**
     *  Gets the {@code Convocation} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Convocation} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Convocation} and
     *  retained for compatibility.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param convocationId {@code Id} of the {@code Convocation}
     *  @return the convocation
     *  @throws org.osid.NotFoundException {@code convocationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code convocationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Convocation getConvocation(org.osid.id.Id convocationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConvocation(convocationId));
    }


    /**
     *  Gets a {@code ConvocationList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  convocations specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Convocations} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Convocation} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code convocationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByIds(org.osid.id.IdList convocationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConvocationsByIds(convocationIds));
    }


    /**
     *  Gets a {@code ConvocationList} corresponding to the given
     *  convocation genus {@code Type} which does not include
     *  convocations of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationGenusType a convocation genus type 
     *  @return the returned {@code Convocation} list
     *  @throws org.osid.NullArgumentException
     *          {@code convocationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByGenusType(org.osid.type.Type convocationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConvocationsByGenusType(convocationGenusType));
    }


    /**
     *  Gets a {@code ConvocationList} corresponding to the given
     *  convocation genus {@code Type} and include any additional
     *  convocations with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationGenusType a convocation genus type 
     *  @return the returned {@code Convocation} list
     *  @throws org.osid.NullArgumentException
     *          {@code convocationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByParentGenusType(org.osid.type.Type convocationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConvocationsByParentGenusType(convocationGenusType));
    }


    /**
     *  Gets a {@code ConvocationList} containing the given
     *  convocation record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationRecordType a convocation record type 
     *  @return the returned {@code Convocation} list
     *  @throws org.osid.NullArgumentException
     *          {@code convocationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByRecordType(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConvocationsByRecordType(convocationRecordType));
    }


    /**
     *  Gets a {@code ConvocationList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Convocation} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConvocationsByProvider(resourceId));
    }

    
    /**
     *  Gets a list of convocations with a date within the given date
     *  range inclusive. In plenary mode, the returned list contains
     *  all known convocations or an error results. Otherwise, the
     *  returned list may contain only those convocations that are
     *  accessible through this session.
     *
     *  @param  from the starting date 
     *  @param  to the ending date 
     *  @return the returned <code> ConvocationList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByDate(org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConvocationsByDate(from, to));
    }


    /**
     *  Gets a list of all convocations corresponding to an award
     *  <code> Id.  </code> In plenary mode, the returned list
     *  contains all known convocations or an error
     *  results. Otherwise, the returned list may contain only those
     *  convocations that are accessible through this session.
     *
     *  @param  awardId the <code> Id </code> of the award 
     *  @return the returned <code> ConvocationList </code> 
     *  @throws org.osid.NullArgumentException <code> awardId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByAward(org.osid.id.Id awardId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getConvocationsByAward(awardId));
    }


    /**
     *  Gets a list of all convocations corresponding to a time period
     *  <code> Id. </code> In plenary mode, the returned list contains
     *  all known convocations or an error results. Otherwise, the
     *  returned list may contain only those convocations that are
     *  accessible through this session.
     *
     *  @param  timePeriodId the <code> Id </code> of the time period 
     *  @return the returned <code> ConvocationList </code> 
     *  @throws org.osid.NullArgumentException <code> timePeriod </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByTimePeriod(org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getConvocationsByTimePeriod(timePeriodId));
    }
    
    
    /**
     *  Gets all {@code Convocations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @return a list of {@code Convocations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConvocations());
    }
}
