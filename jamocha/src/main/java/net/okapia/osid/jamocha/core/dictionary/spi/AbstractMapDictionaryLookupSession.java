//
// AbstractMapDictionaryLookupSession
//
//    A simple framework for providing a Dictionary lookup service
//    backed by a fixed collection of dictionaries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Dictionary lookup service backed by a
 *  fixed collection of dictionaries. The dictionaries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Dictionaries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDictionaryLookupSession
    extends net.okapia.osid.jamocha.dictionary.spi.AbstractDictionaryLookupSession
    implements org.osid.dictionary.DictionaryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.dictionary.Dictionary> dictionaries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.dictionary.Dictionary>());


    /**
     *  Makes a <code>Dictionary</code> available in this session.
     *
     *  @param  dictionary a dictionary
     *  @throws org.osid.NullArgumentException <code>dictionary<code>
     *          is <code>null</code>
     */

    protected void putDictionary(org.osid.dictionary.Dictionary dictionary) {
        this.dictionaries.put(dictionary.getId(), dictionary);
        return;
    }


    /**
     *  Makes an array of dictionaries available in this session.
     *
     *  @param  dictionaries an array of dictionaries
     *  @throws org.osid.NullArgumentException <code>dictionaries<code>
     *          is <code>null</code>
     */

    protected void putDictionaries(org.osid.dictionary.Dictionary[] dictionaries) {
        putDictionaries(java.util.Arrays.asList(dictionaries));
        return;
    }


    /**
     *  Makes a collection of dictionaries available in this session.
     *
     *  @param  dictionaries a collection of dictionaries
     *  @throws org.osid.NullArgumentException <code>dictionaries<code>
     *          is <code>null</code>
     */

    protected void putDictionaries(java.util.Collection<? extends org.osid.dictionary.Dictionary> dictionaries) {
        for (org.osid.dictionary.Dictionary dictionary : dictionaries) {
            this.dictionaries.put(dictionary.getId(), dictionary);
        }

        return;
    }


    /**
     *  Removes a Dictionary from this session.
     *
     *  @param  dictionaryId the <code>Id</code> of the dictionary
     *  @throws org.osid.NullArgumentException <code>dictionaryId<code> is
     *          <code>null</code>
     */

    protected void removeDictionary(org.osid.id.Id dictionaryId) {
        this.dictionaries.remove(dictionaryId);
        return;
    }


    /**
     *  Gets the <code>Dictionary</code> specified by its <code>Id</code>.
     *
     *  @param  dictionaryId <code>Id</code> of the <code>Dictionary</code>
     *  @return the dictionary
     *  @throws org.osid.NotFoundException <code>dictionaryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>dictionaryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Dictionary getDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.dictionary.Dictionary dictionary = this.dictionaries.get(dictionaryId);
        if (dictionary == null) {
            throw new org.osid.NotFoundException("dictionary not found: " + dictionaryId);
        }

        return (dictionary);
    }


    /**
     *  Gets all <code>Dictionaries</code>. In plenary mode, the returned
     *  list contains all known dictionaries or an error
     *  results. Otherwise, the returned list may contain only those
     *  dictionaries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Dictionaries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionaries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.dictionary.dictionary.ArrayDictionaryList(this.dictionaries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.dictionaries.clear();
        super.close();
        return;
    }
}
