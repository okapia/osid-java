//
// AbstractAssessmentEntrySearch.java
//
//     A template for making an AssessmentEntry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.assessmententry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing assessment entry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAssessmentEntrySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.chronicle.AssessmentEntrySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.AssessmentEntrySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.chronicle.AssessmentEntrySearchOrder assessmentEntrySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of assessment entries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  assessmentEntryIds list of assessment entries
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAssessmentEntries(org.osid.id.IdList assessmentEntryIds) {
        while (assessmentEntryIds.hasNext()) {
            try {
                this.ids.add(assessmentEntryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAssessmentEntries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of assessment entry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAssessmentEntryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  assessmentEntrySearchOrder assessment entry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntrySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>assessmentEntrySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAssessmentEntryResults(org.osid.course.chronicle.AssessmentEntrySearchOrder assessmentEntrySearchOrder) {
	this.assessmentEntrySearchOrder = assessmentEntrySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.chronicle.AssessmentEntrySearchOrder getAssessmentEntrySearchOrder() {
	return (this.assessmentEntrySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given assessment entry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an assessment entry implementing the requested record.
     *
     *  @param assessmentEntrySearchRecordType an assessment entry search record
     *         type
     *  @return the assessment entry search record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentEntrySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AssessmentEntrySearchRecord getAssessmentEntrySearchRecord(org.osid.type.Type assessmentEntrySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.chronicle.records.AssessmentEntrySearchRecord record : this.records) {
            if (record.implementsRecordType(assessmentEntrySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment entry search. 
     *
     *  @param assessmentEntrySearchRecord assessment entry search record
     *  @param assessmentEntrySearchRecordType assessmentEntry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentEntrySearchRecord(org.osid.course.chronicle.records.AssessmentEntrySearchRecord assessmentEntrySearchRecord, 
                                           org.osid.type.Type assessmentEntrySearchRecordType) {

        addRecordType(assessmentEntrySearchRecordType);
        this.records.add(assessmentEntrySearchRecord);        
        return;
    }
}
