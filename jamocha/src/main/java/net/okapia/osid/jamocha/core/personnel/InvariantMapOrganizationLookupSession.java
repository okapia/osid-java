//
// InvariantMapOrganizationLookupSession
//
//    Implements an Organization lookup service backed by a fixed collection of
//    organizations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements an Organization lookup service backed by a fixed
 *  collection of organizations. The organizations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapOrganizationLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractMapOrganizationLookupSession
    implements org.osid.personnel.OrganizationLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapOrganizationLookupSession</code> with no
     *  organizations.
     *  
     *  @param realm the realm
     *  @throws org.osid.NullArgumnetException {@code realm} is
     *          {@code null}
     */

    public InvariantMapOrganizationLookupSession(org.osid.personnel.Realm realm) {
        setRealm(realm);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapOrganizationLookupSession</code> with a single
     *  organization.
     *  
     *  @param realm the realm
     *  @param organization an single organization
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code organization} is <code>null</code>
     */

      public InvariantMapOrganizationLookupSession(org.osid.personnel.Realm realm,
                                               org.osid.personnel.Organization organization) {
        this(realm);
        putOrganization(organization);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapOrganizationLookupSession</code> using an array
     *  of organizations.
     *  
     *  @param realm the realm
     *  @param organizations an array of organizations
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code organizations} is <code>null</code>
     */

      public InvariantMapOrganizationLookupSession(org.osid.personnel.Realm realm,
                                               org.osid.personnel.Organization[] organizations) {
        this(realm);
        putOrganizations(organizations);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapOrganizationLookupSession</code> using a
     *  collection of organizations.
     *
     *  @param realm the realm
     *  @param organizations a collection of organizations
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code organizations} is <code>null</code>
     */

      public InvariantMapOrganizationLookupSession(org.osid.personnel.Realm realm,
                                               java.util.Collection<? extends org.osid.personnel.Organization> organizations) {
        this(realm);
        putOrganizations(organizations);
        return;
    }
}
