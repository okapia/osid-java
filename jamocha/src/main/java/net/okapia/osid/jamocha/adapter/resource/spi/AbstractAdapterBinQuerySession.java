//
// AbstractQueryBinLookupSession.java
//
//    A BinQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BinQuerySession adapter.
 */

public abstract class AbstractAdapterBinQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resource.BinQuerySession {

    private final org.osid.resource.BinQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterBinQuerySession.
     *
     *  @param session the underlying bin query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBinQuerySession(org.osid.resource.BinQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@codeBin</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchBins() {
        return (this.session.canSearchBins());
    }

      
    /**
     *  Gets a bin query. The returned query will not have an
     *  extension query.
     *
     *  @return the bin query 
     */
      
    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        return (this.session.getBinQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  binQuery the bin query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code binQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code binQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByQuery(org.osid.resource.BinQuery binQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getBinsByQuery(binQuery));
    }
}
