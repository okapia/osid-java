//
// AbstractQueueQueryInspector.java
//
//     A template for making a QueueQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.queue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for queues.
 */

public abstract class AbstractQueueQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQueryInspector
    implements org.osid.provisioning.QueueQueryInspector {

    private final java.util.Collection<org.osid.provisioning.records.QueueQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrokerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Gets the request <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the request query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQueryInspector[] getRequestTerms() {
        return (new org.osid.provisioning.RequestQueryInspector[0]);
    }


    /**
     *  Gets the size query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getSizeTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the ewa query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getEWATerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the can request provisionables query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCanSpecifyProvisionableTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given queue query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a queue implementing the requested record.
     *
     *  @param queueRecordType a queue record type
     *  @return the queue query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.QueueQueryInspectorRecord getQueueQueryInspectorRecord(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.QueueQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(queueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue query. 
     *
     *  @param queueQueryInspectorRecord queue query inspector
     *         record
     *  @param queueRecordType queue record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueQueryInspectorRecord(org.osid.provisioning.records.QueueQueryInspectorRecord queueQueryInspectorRecord, 
                                                   org.osid.type.Type queueRecordType) {

        addRecordType(queueRecordType);
        nullarg(queueRecordType, "queue record type");
        this.records.add(queueQueryInspectorRecord);        
        return;
    }
}
