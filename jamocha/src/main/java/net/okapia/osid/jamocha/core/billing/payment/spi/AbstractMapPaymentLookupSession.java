//
// AbstractMapPaymentLookupSession
//
//    A simple framework for providing a Payment lookup service
//    backed by a fixed collection of payments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Payment lookup service backed by a
 *  fixed collection of payments. The payments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Payments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPaymentLookupSession
    extends net.okapia.osid.jamocha.billing.payment.spi.AbstractPaymentLookupSession
    implements org.osid.billing.payment.PaymentLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.billing.payment.Payment> payments = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.billing.payment.Payment>());


    /**
     *  Makes a <code>Payment</code> available in this session.
     *
     *  @param  payment a payment
     *  @throws org.osid.NullArgumentException <code>payment<code>
     *          is <code>null</code>
     */

    protected void putPayment(org.osid.billing.payment.Payment payment) {
        this.payments.put(payment.getId(), payment);
        return;
    }


    /**
     *  Makes an array of payments available in this session.
     *
     *  @param  payments an array of payments
     *  @throws org.osid.NullArgumentException <code>payments<code>
     *          is <code>null</code>
     */

    protected void putPayments(org.osid.billing.payment.Payment[] payments) {
        putPayments(java.util.Arrays.asList(payments));
        return;
    }


    /**
     *  Makes a collection of payments available in this session.
     *
     *  @param  payments a collection of payments
     *  @throws org.osid.NullArgumentException <code>payments<code>
     *          is <code>null</code>
     */

    protected void putPayments(java.util.Collection<? extends org.osid.billing.payment.Payment> payments) {
        for (org.osid.billing.payment.Payment payment : payments) {
            this.payments.put(payment.getId(), payment);
        }

        return;
    }


    /**
     *  Removes a Payment from this session.
     *
     *  @param  paymentId the <code>Id</code> of the payment
     *  @throws org.osid.NullArgumentException <code>paymentId<code> is
     *          <code>null</code>
     */

    protected void removePayment(org.osid.id.Id paymentId) {
        this.payments.remove(paymentId);
        return;
    }


    /**
     *  Gets the <code>Payment</code> specified by its <code>Id</code>.
     *
     *  @param  paymentId <code>Id</code> of the <code>Payment</code>
     *  @return the payment
     *  @throws org.osid.NotFoundException <code>paymentId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>paymentId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.Payment getPayment(org.osid.id.Id paymentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.billing.payment.Payment payment = this.payments.get(paymentId);
        if (payment == null) {
            throw new org.osid.NotFoundException("payment not found: " + paymentId);
        }

        return (payment);
    }


    /**
     *  Gets all <code>Payments</code>. In plenary mode, the returned
     *  list contains all known payments or an error
     *  results. Otherwise, the returned list may contain only those
     *  payments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Payments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPayments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.payment.payment.ArrayPaymentList(this.payments.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.payments.clear();
        super.close();
        return;
    }
}
