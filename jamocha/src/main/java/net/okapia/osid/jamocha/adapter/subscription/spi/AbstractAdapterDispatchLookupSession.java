//
// AbstractAdapterDispatchLookupSession.java
//
//    A Dispatch lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.subscription.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Dispatch lookup session adapter.
 */

public abstract class AbstractAdapterDispatchLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.subscription.DispatchLookupSession {

    private final org.osid.subscription.DispatchLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDispatchLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDispatchLookupSession(org.osid.subscription.DispatchLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Publisher/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Publisher Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.session.getPublisherId());
    }


    /**
     *  Gets the {@code Publisher} associated with this session.
     *
     *  @return the {@code Publisher} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPublisher());
    }


    /**
     *  Tests if this user can perform {@code Dispatch} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDispatches() {
        return (this.session.canLookupDispatches());
    }


    /**
     *  A complete view of the {@code Dispatch} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDispatchView() {
        this.session.useComparativeDispatchView();
        return;
    }


    /**
     *  A complete view of the {@code Dispatch} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDispatchView() {
        this.session.usePlenaryDispatchView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include dispatches in publishers which are children
     *  of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.session.useFederatedPublisherView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.session.useIsolatedPublisherView();
        return;
    }
    

    /**
     *  Only active dispatches are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveDispatchView() {
        this.session.useActiveDispatchView();
        return;
    }


    /**
     *  Active and inactive dispatches are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusDispatchView() {
        this.session.useAnyStatusDispatchView();
        return;
    }
    
     
    /**
     *  Gets the {@code Dispatch} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Dispatch} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Dispatch} and
     *  retained for compatibility.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @param dispatchId {@code Id} of the {@code Dispatch}
     *  @return the dispatch
     *  @throws org.osid.NotFoundException {@code dispatchId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code dispatchId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Dispatch getDispatch(org.osid.id.Id dispatchId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDispatch(dispatchId));
    }


    /**
     *  Gets a {@code DispatchList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  dispatches specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Dispatches} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @param  dispatchIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Dispatch} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code dispatchIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByIds(org.osid.id.IdList dispatchIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDispatchesByIds(dispatchIds));
    }


    /**
     *  Gets a {@code DispatchList} corresponding to the given
     *  dispatch genus {@code Type} which does not include
     *  dispatches of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @param  dispatchGenusType a dispatch genus type 
     *  @return the returned {@code Dispatch} list
     *  @throws org.osid.NullArgumentException
     *          {@code dispatchGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByGenusType(org.osid.type.Type dispatchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDispatchesByGenusType(dispatchGenusType));
    }


    /**
     *  Gets a {@code DispatchList} corresponding to the given
     *  dispatch genus {@code Type} and include any additional
     *  dispatches with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @param  dispatchGenusType a dispatch genus type 
     *  @return the returned {@code Dispatch} list
     *  @throws org.osid.NullArgumentException
     *          {@code dispatchGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByParentGenusType(org.osid.type.Type dispatchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDispatchesByParentGenusType(dispatchGenusType));
    }


    /**
     *  Gets a {@code DispatchList} containing the given
     *  dispatch record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @param  dispatchRecordType a dispatch record type 
     *  @return the returned {@code Dispatch} list
     *  @throws org.osid.NullArgumentException
     *          {@code dispatchRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByRecordType(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDispatchesByRecordType(dispatchRecordType));
    }


    /**
     *  Gets a {@code DispatchList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Dispatch} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDispatchesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Dispatches}. 
     *
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @return a list of {@code Dispatches} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatches()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDispatches());
    }
}
