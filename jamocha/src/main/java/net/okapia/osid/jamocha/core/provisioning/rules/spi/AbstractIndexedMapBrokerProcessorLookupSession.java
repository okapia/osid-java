//
// AbstractIndexedMapBrokerProcessorLookupSession.java
//
//    A simple framework for providing a BrokerProcessor lookup service
//    backed by a fixed collection of broker processors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a BrokerProcessor lookup service backed by a
 *  fixed collection of broker processors. The broker processors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some broker processors may be compatible
 *  with more types than are indicated through these broker processor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BrokerProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBrokerProcessorLookupSession
    extends AbstractMapBrokerProcessorLookupSession
    implements org.osid.provisioning.rules.BrokerProcessorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.BrokerProcessor> brokerProcessorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.BrokerProcessor>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.BrokerProcessor> brokerProcessorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.BrokerProcessor>());


    /**
     *  Makes a <code>BrokerProcessor</code> available in this session.
     *
     *  @param  brokerProcessor a broker processor
     *  @throws org.osid.NullArgumentException <code>brokerProcessor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBrokerProcessor(org.osid.provisioning.rules.BrokerProcessor brokerProcessor) {
        super.putBrokerProcessor(brokerProcessor);

        this.brokerProcessorsByGenus.put(brokerProcessor.getGenusType(), brokerProcessor);
        
        try (org.osid.type.TypeList types = brokerProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokerProcessorsByRecord.put(types.getNextType(), brokerProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a broker processor from this session.
     *
     *  @param brokerProcessorId the <code>Id</code> of the broker processor
     *  @throws org.osid.NullArgumentException <code>brokerProcessorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBrokerProcessor(org.osid.id.Id brokerProcessorId) {
        org.osid.provisioning.rules.BrokerProcessor brokerProcessor;
        try {
            brokerProcessor = getBrokerProcessor(brokerProcessorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.brokerProcessorsByGenus.remove(brokerProcessor.getGenusType());

        try (org.osid.type.TypeList types = brokerProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokerProcessorsByRecord.remove(types.getNextType(), brokerProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBrokerProcessor(brokerProcessorId);
        return;
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> corresponding to the given
     *  broker processor genus <code>Type</code> which does not include
     *  broker processors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known broker processors or an error results. Otherwise,
     *  the returned list may contain only those broker processors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  brokerProcessorGenusType a broker processor genus type 
     *  @return the returned <code>BrokerProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByGenusType(org.osid.type.Type brokerProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.ArrayBrokerProcessorList(this.brokerProcessorsByGenus.get(brokerProcessorGenusType)));
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> containing the given
     *  broker processor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known broker processors or an error
     *  results. Otherwise, the returned list may contain only those
     *  broker processors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  brokerProcessorRecordType a broker processor record type 
     *  @return the returned <code>brokerProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByRecordType(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.ArrayBrokerProcessorList(this.brokerProcessorsByRecord.get(brokerProcessorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.brokerProcessorsByGenus.clear();
        this.brokerProcessorsByRecord.clear();

        super.close();

        return;
    }
}
