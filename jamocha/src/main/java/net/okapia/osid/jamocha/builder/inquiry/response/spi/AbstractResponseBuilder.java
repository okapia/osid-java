//
// AbstractResponse.java
//
//     Defines a Response builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inquiry.response.spi;


/**
 *  Defines a <code>Response</code> builder.
 */

public abstract class AbstractResponseBuilder<T extends AbstractResponseBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.inquiry.response.ResponseMiter response;


    /**
     *  Constructs a new <code>AbstractResponseBuilder</code>.
     *
     *  @param response the response to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractResponseBuilder(net.okapia.osid.jamocha.builder.inquiry.response.ResponseMiter response) {
        super(response);
        this.response = response;
        return;
    }


    /**
     *  Builds the response.
     *
     *  @return the new response
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>response</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.inquiry.Response build() {
        (new net.okapia.osid.jamocha.builder.validator.inquiry.response.ResponseValidator(getValidations())).validate(this.response);
        return (new net.okapia.osid.jamocha.builder.inquiry.response.ImmutableResponse(this.response));
    }


    /**
     *  Gets the response. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new response
     */

    @Override
    public net.okapia.osid.jamocha.builder.inquiry.response.ResponseMiter getMiter() {
        return (this.response);
    }


    /**
     *  Sets the inquiry.
     *
     *  @param inquiry an inquiry
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>inquiry</code> is <code>null</code>
     */

    public T inquiry(org.osid.inquiry.Inquiry inquiry) {
        getMiter().setInquiry(inquiry);
        return (self());
    }


    /**
     *  Sets the responder.
     *
     *  @param responder a responder
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>responder</code> is <code>null</code>
     */

    public T responder(org.osid.resource.Resource responder) {
        getMiter().setResponder(responder);
        return (self());
    }


    /**
     *  Sets the responding agent.
     *
     *  @param respondingAgent a responding agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>respondingAgent</code> is <code>null</code>
     */

    public T respondingAgent(org.osid.authentication.Agent respondingAgent) {
        getMiter().setRespondingAgent(respondingAgent);
        return (self());
    }


    /**
     *  Sets the affirmative flag.
     *
     *  @param affirmative {@code true} if a positive response, {@code
     *         false} if a negative response
     */

    public T setAffirmative(boolean affirmative) {
        getMiter().setAffirmative(affirmative);
        return (self());
    }


    /**
     *  Adds a Response record.
     *
     *  @param record a response record
     *  @param recordType the type of response record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.inquiry.records.ResponseRecord record, org.osid.type.Type recordType) {
        getMiter().addResponseRecord(record, recordType);
        return (self());
    }
}       


