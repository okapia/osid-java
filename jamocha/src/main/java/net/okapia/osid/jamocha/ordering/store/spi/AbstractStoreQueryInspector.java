//
// AbstractStoreQueryInspector.java
//
//     A template for making a StoreQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.store.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for stores.
 */

public abstract class AbstractStoreQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.ordering.StoreQueryInspector {

    private final java.util.Collection<org.osid.ordering.records.StoreQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the order <code> Id </code> terms. 
     *
     *  @return the order <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrderIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the order terms. 
     *
     *  @return the order terms 
     */

    @OSID @Override
    public org.osid.ordering.OrderQueryInspector[] getOrderTerms() {
        return (new org.osid.ordering.OrderQueryInspector[0]);
    }


    /**
     *  Gets the product <code> Id </code> terms. 
     *
     *  @return the product <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProductIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the product terms. 
     *
     *  @return the product terms 
     */

    @OSID @Override
    public org.osid.ordering.ProductQueryInspector[] getProductTerms() {
        return (new org.osid.ordering.ProductQueryInspector[0]);
    }


    /**
     *  Gets the price schedule <code> Id </code> terms. 
     *
     *  @return the price schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPriceScheduleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the price schedule terms. 
     *
     *  @return the price schedule terms 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQueryInspector[] getPriceScheduleTerms() {
        return (new org.osid.ordering.PriceScheduleQueryInspector[0]);
    }


    /**
     *  Gets the ancestor store <code> Id </code> terms. 
     *
     *  @return the ancestor store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorStoreIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor store terms. 
     *
     *  @return the ancestor store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getAncestorStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the descendant store <code> Id </code> terms. 
     *
     *  @return the descendant store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantStoreIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant store terms. 
     *
     *  @return the descendant store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getDescendantStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given store query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a store implementing the requested record.
     *
     *  @param storeRecordType a store record type
     *  @return the store query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>storeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(storeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.StoreQueryInspectorRecord getStoreQueryInspectorRecord(org.osid.type.Type storeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.StoreQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(storeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(storeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this store query. 
     *
     *  @param storeQueryInspectorRecord store query inspector
     *         record
     *  @param storeRecordType store record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStoreQueryInspectorRecord(org.osid.ordering.records.StoreQueryInspectorRecord storeQueryInspectorRecord, 
                                                   org.osid.type.Type storeRecordType) {

        addRecordType(storeRecordType);
        nullarg(storeRecordType, "store record type");
        this.records.add(storeQueryInspectorRecord);        
        return;
    }
}
