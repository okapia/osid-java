//
// SingleAssessmentOfferedBatchFormList.java
//
//     Implements an AssessmentOfferedBatchFormList based on a single AssessmentOfferedBatchForm.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.batch.assessmentofferedbatchform;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements an ObjectList based on a single AssessmentOfferedBatchForm.
 */

public final class SingleAssessmentOfferedBatchFormList
    extends net.okapia.osid.jamocha.assessment.batch.assessmentofferedbatchform.spi.AbstractAssessmentOfferedBatchFormList
    implements org.osid.assessment.batch.AssessmentOfferedBatchFormList {

    private final org.osid.assessment.batch.AssessmentOfferedBatchForm assessmentOfferedBatchForm;
    private boolean read = false;


    /**
     *  Creates a new <code>SingleAssessmentOfferedBatchFormList</code>.
     *
     *  @param assessmentOfferedBatchForm an assessmentofferedbatchform
     *  @throws org.osid.NullArgumentException <code>assessmentOfferedBatchForm</code>
     *          is <code>null</code>
     */

    public SingleAssessmentOfferedBatchFormList(org.osid.assessment.batch.AssessmentOfferedBatchForm assessmentOfferedBatchForm) {
        nullarg(assessmentOfferedBatchForm, "assessmentofferedbatchform");
        this.assessmentOfferedBatchForm = assessmentOfferedBatchForm;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code>true</code> if more elements are available in
     *          this list, <code>false</code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.read) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code>AssessmentOfferedBatchForm</code> in this list. 
     *
     *  @return the next <code>AssessmentOfferedBatchForm</code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code>AssessmentOfferedBatchForm</code> is available before
     *          calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.batch.AssessmentOfferedBatchForm getNextAssessmentOfferedBatchForm()
        throws org.osid.OperationFailedException {

        if (hasNext()) {
            this.read = true;
            return (this.assessmentOfferedBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in assessmentofferedbatchform list");
        }
    }
}
