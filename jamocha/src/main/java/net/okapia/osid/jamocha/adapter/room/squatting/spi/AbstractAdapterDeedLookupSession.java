//
// AbstractAdapterDeedLookupSession.java
//
//    A Deed lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Deed lookup session adapter.
 */

public abstract class AbstractAdapterDeedLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.room.squatting.DeedLookupSession {

    private final org.osid.room.squatting.DeedLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDeedLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDeedLookupSession(org.osid.room.squatting.DeedLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Campus/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Campus Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the {@code Campus} associated with this session.
     *
     *  @return the {@code Campus} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform {@code Deed} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDeeds() {
        return (this.session.canLookupDeeds());
    }


    /**
     *  A complete view of the {@code Deed} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDeedView() {
        this.session.useComparativeDeedView();
        return;
    }


    /**
     *  A complete view of the {@code Deed} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDeedView() {
        this.session.usePlenaryDeedView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include deeds in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only deeds whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveDeedView() {
        this.session.useEffectiveDeedView();
        return;
    }
    

    /**
     *  All deeds of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveDeedView() {
        this.session.useAnyEffectiveDeedView();
        return;
    }

     
    /**
     *  Gets the {@code Deed} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Deed} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Deed} and
     *  retained for compatibility.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @param deedId {@code Id} of the {@code Deed}
     *  @return the deed
     *  @throws org.osid.NotFoundException {@code deedId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code deedId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.Deed getDeed(org.osid.id.Id deedId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeed(deedId));
    }


    /**
     *  Gets a {@code DeedList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  deeds specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Deeds} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @param  deedIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Deed} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code deedIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByIds(org.osid.id.IdList deedIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByIds(deedIds));
    }


    /**
     *  Gets a {@code DeedList} corresponding to the given
     *  deed genus {@code Type} which does not include
     *  deeds of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @param  deedGenusType a deed genus type 
     *  @return the returned {@code Deed} list
     *  @throws org.osid.NullArgumentException
     *          {@code deedGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusType(org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByGenusType(deedGenusType));
    }


    /**
     *  Gets a {@code DeedList} corresponding to the given deed genus
     *  {@code Type} and include any additional deeds with genus types
     *  derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  deedGenusType a deed genus type 
     *  @return the returned {@code Deed} list
     *  @throws org.osid.NullArgumentException
     *          {@code deedGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByParentGenusType(org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByParentGenusType(deedGenusType));
    }


    /**
     *  Gets a {@code DeedList} containing the given deed record
     *  {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  deedRecordType a deed record type 
     *  @return the returned {@code Deed} list
     *  @throws org.osid.NullArgumentException
     *          {@code deedRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByRecordType(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByRecordType(deedRecordType));
    }


    /**
     *  Gets a {@code DeedList} effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In active mode, deeds are returned that are currently
     *  active. In any status mode, active and inactive deeds are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Deed} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsOnDate(org.osid.calendaring.DateTime from, 
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsOnDate(from, to));
    }
        

    /**
     *  Gets a list of all deeds with a genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Deed} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code deedGenusType,
     *         from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeOnDate(org.osid.type.Type deedGenusType, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByGenusTypeOnDate(deedGenusType, from, to));
    }


    /**
     *  Gets a list of deeds corresponding to a building {@code Id}.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId the {@code Id} of the building
     *  @return the returned {@code DeedList}
     *  @throws org.osid.NullArgumentException {@code buildingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsForBuilding(buildingId));
    }


    /**
     *  Gets a {@code DeedList} containing the given building and genus 
     *  type.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  deedGenusType a deed genus type 
     *  @return the returned {@code Deed} list 
     *  @throws org.osid.NullArgumentException {@code buildingId} or 
     *          {@code deedGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                           org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByGenusTypeForBuilding(buildingId, deedGenusType));
    }


    /**
     *  Gets a list of deeds corresponding to a building {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId the {@code Id} of the building
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code DeedList}
     *  @throws org.osid.NullArgumentException {@code buildingId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuildingOnDate(org.osid.id.Id buildingId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsForBuildingOnDate(buildingId, from, to));
    }


    /**
     *  Gets a list of all deeds for a building with a genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Deed} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code buildingId,
     *          deedGenusType, from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                 org.osid.type.Type deedGenusType, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByGenusTypeForBuildingOnDate(buildingId, deedGenusType, from, to));
    }


    /**
     *  Gets a list of deeds corresponding to a owner {@code Id}.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the owner
     *  @return the returned {@code DeedList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForOwner(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsForOwner(resourceId));
    }

    
    /**
     *  Gets a {@code DeedList} containing the given owner and genus 
     *  type.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  deedGenusType a deed genus type 
     *  @return the returned {@code Deed} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code deedGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForOwner(org.osid.id.Id resourceId, 
                                                                        org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByGenusTypeForOwner(resourceId, deedGenusType));
    }


    /**
     *  Gets a list of deeds corresponding to a owner {@code Id} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the owner
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code DeedList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForOwnerOnDate(org.osid.id.Id resourceId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsForOwnerOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of all deeds for a resource owner with a genus
     *  type and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, 
     *          deedGenusType, from </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForOwnerOnDate(org.osid.id.Id resourceId, 
                                                                              org.osid.type.Type deedGenusType, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByGenusTypeForOwnerOnDate(resourceId, deedGenusType, from, to));
    }


    /**
     *  Gets a list of deeds corresponding to building and owner
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are
     *  currently effective.  In any effective mode, effective
     *  deeds and those currently expired are returned.
     *
     *  @param  buildingId the {@code Id} of the building
     *  @param  resourceId the {@code Id} of the owner
     *  @return the returned {@code DeedList}
     *  @throws org.osid.NullArgumentException {@code buildingId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuildingAndOwner(org.osid.id.Id buildingId,
                                                                        org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsForBuildingAndOwner(buildingId, resourceId));
    }


    /**
     *  Gets a <code> DeedList </code> for the given buildin, owner,
     *  and genus type.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.NullArgumentException <code> buildingId, resourceId, 
     *          </code> or <code> deedGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuildingAndOwner(org.osid.id.Id buildingId, 
                                                                                   org.osid.id.Id resourceId, 
                                                                                   org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByGenusTypeForBuildingAndOwner(buildingId, resourceId, deedGenusType));
    }


    /**
     *  Gets a list of deeds corresponding to building and owner
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the owner
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code DeedList}
     *  @throws org.osid.NullArgumentException {@code buildingId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuildingAndOwnerOnDate(org.osid.id.Id buildingId,
                                                                              org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsForBuildingAndOwnerOnDate(buildingId, resourceId, from, to));
    }


    /**
     *  Gets a list of all deeds for a building and owner with a genus
     *  type and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> buildingId,
     *          resourceId, deedGenusType, from, </code> or <code> to
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuildingAndOwnerOnDate(org.osid.id.Id buildingId, 
                                                                                         org.osid.id.Id resourceId, 
                                                                                         org.osid.type.Type deedGenusType, 
                                                                                         org.osid.calendaring.DateTime from, 
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeedsByGenusTypeForBuildingAndOwnerOnDate(buildingId, resourceId, deedGenusType, from, to));
    }


    /**
     *  Gets all {@code Deeds}. 
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Deeds} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeeds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeeds());
    }
}
