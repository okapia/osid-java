//
// AbstractAvailabilityEnablerQuery.java
//
//     A template for making an AvailabilityEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.availabilityenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for availability enablers.
 */

public abstract class AbstractAvailabilityEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.resourcing.rules.AvailabilityEnablerQuery {

    private final java.util.Collection<org.osid.resourcing.rules.records.AvailabilityEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to an availability. 
     *
     *  @param  availabilityId the availability <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> availabilityId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAvailabilityId(org.osid.id.Id availabilityId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the availability <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAvailabilityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AvailabilityQuery </code> is available. 
     *
     *  @return <code> true </code> if an availability query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAvailabilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an availability. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the availability query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAvailabilityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuery getRuledAvailabilityQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAvailabilityQuery() is false");
    }


    /**
     *  Matches mapped to any availability. 
     *
     *  @param  match <code> true </code> for mapped to any availability, 
     *          <code> false </code> to match mapped to no availabilities 
     */

    @OSID @Override
    public void matchAnyRuledAvailability(boolean match) {
        return;
    }


    /**
     *  Clears the availability query terms. 
     */

    @OSID @Override
    public void clearRuledAvailabilityTerms() {
        return;
    }


    /**
     *  Matches mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given availability enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an availability enabler implementing the requested record.
     *
     *  @param availabilityEnablerRecordType an availability enabler record type
     *  @return the availability enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.AvailabilityEnablerQueryRecord getAvailabilityEnablerQueryRecord(org.osid.type.Type availabilityEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.AvailabilityEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(availabilityEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this availability enabler query. 
     *
     *  @param availabilityEnablerQueryRecord availability enabler query record
     *  @param availabilityEnablerRecordType availabilityEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAvailabilityEnablerQueryRecord(org.osid.resourcing.rules.records.AvailabilityEnablerQueryRecord availabilityEnablerQueryRecord, 
                                          org.osid.type.Type availabilityEnablerRecordType) {

        addRecordType(availabilityEnablerRecordType);
        nullarg(availabilityEnablerQueryRecord, "availability enabler query record");
        this.records.add(availabilityEnablerQueryRecord);        
        return;
    }
}
