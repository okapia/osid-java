//
// AbstractCommentingBatchManager.java
//
//     An adapter for a CommentingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.commenting.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CommentingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCommentingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.commenting.batch.CommentingBatchManager>
    implements org.osid.commenting.batch.CommentingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterCommentingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCommentingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCommentingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCommentingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of comments is available. 
     *
     *  @return <code> true </code> if a comment bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentBatchAdmin() {
        return (getAdapteeManager().supportsCommentBatchAdmin());
    }


    /**
     *  Tests if bulk administration of books is available. 
     *
     *  @return <code> true </code> if a book bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookBatchAdmin() {
        return (getAdapteeManager().supportsBookBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk comment 
     *  administration service. 
     *
     *  @return a <code> CommentBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.batch.CommentBatchAdminSession getCommentBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk comment 
     *  administration service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @return a <code> CommentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.batch.CommentBatchAdminSession getCommentBatchAdminSessionForBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentBatchAdminSessionForBook(bookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk book 
     *  administration service. 
     *
     *  @return a <code> BookBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBookBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.batch.BookBatchAdminSession getBookBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBookBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
