//
// AbstractJournalingProxyManager.java
//
//     An adapter for a JournalingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a JournalingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterJournalingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.journaling.JournalingProxyManager>
    implements org.osid.journaling.JournalingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterJournalingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterJournalingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterJournalingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterJournalingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any journal federation is exposed. Federation is exposed when 
     *  a specific journal may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  journals appears as a single journal. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a journal entry lookup service. 
     *
     *  @return <code> true </code> if journal entry lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryLookup() {
        return (getAdapteeManager().supportsJournalEntryLookup());
    }


    /**
     *  Tests if querying journal entries is available. 
     *
     *  @return <code> true </code> if journal entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryQuery() {
        return (getAdapteeManager().supportsJournalEntryQuery());
    }


    /**
     *  Tests if searching for journal entries is available. 
     *
     *  @return <code> true </code> if journal entry search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntrySearch() {
        return (getAdapteeManager().supportsJournalEntrySearch());
    }


    /**
     *  Tests if searching for journal entries is available. 
     *
     *  @return <code> true </code> if journal entry search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryAdmin() {
        return (getAdapteeManager().supportsJournalEntryAdmin());
    }


    /**
     *  Tests if journal entry notification is available. 
     *
     *  @return <code> true </code> if journal entry notification is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryNotification() {
        return (getAdapteeManager().supportsJournalEntryNotification());
    }


    /**
     *  Tests if branch lookup is supported. 
     *
     *  @return <code> true </code> if branch lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchLookup() {
        return (getAdapteeManager().supportsBranchLookup());
    }


    /**
     *  Tests if branch query is supported. 
     *
     *  @return <code> true </code> if branch query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchQuery() {
        return (getAdapteeManager().supportsBranchQuery());
    }


    /**
     *  Tests if branch search is supported. 
     *
     *  @return <code> true </code> if branch search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchSearch() {
        return (getAdapteeManager().supportsBranchSearch());
    }


    /**
     *  Tests if branch administration is supported. 
     *
     *  @return <code> true </code> if branch administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchAdmin() {
        return (getAdapteeManager().supportsBranchAdmin());
    }


    /**
     *  Tests if branch notification is supported. Messages may be sent when 
     *  branches are created, modified, or deleted. 
     *
     *  @return <code> true </code> if branch notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchNotification() {
        return (getAdapteeManager().supportsBranchNotification());
    }


    /**
     *  Tests if branch smart journals are available. 
     *
     *  @return <code> true </code> if branch smart journals are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchSmartJournal() {
        return (getAdapteeManager().supportsBranchSmartJournal());
    }


    /**
     *  Tests for the availability of an journal lookup service. 
     *
     *  @return <code> true </code> if journal lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalLookup() {
        return (getAdapteeManager().supportsJournalLookup());
    }


    /**
     *  Tests if querying journals is available. 
     *
     *  @return <code> true </code> if journal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalQuery() {
        return (getAdapteeManager().supportsJournalQuery());
    }


    /**
     *  Tests if searching for journals is available. 
     *
     *  @return <code> true </code> if journal search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalSearch() {
        return (getAdapteeManager().supportsJournalSearch());
    }


    /**
     *  Tests for the availability of a journal administrative service for 
     *  creating and deleting journals. 
     *
     *  @return <code> true </code> if journal administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalAdmin() {
        return (getAdapteeManager().supportsJournalAdmin());
    }


    /**
     *  Tests for the availability of a journal notification service. 
     *
     *  @return <code> true </code> if journal notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalNotification() {
        return (getAdapteeManager().supportsJournalNotification());
    }


    /**
     *  Tests for the availability of a journal hierarchy traversal service. 
     *
     *  @return <code> true </code> if journal hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalHierarchy() {
        return (getAdapteeManager().supportsJournalHierarchy());
    }


    /**
     *  Tests for the availability of a journal hierarchy design service. 
     *
     *  @return <code> true </code> if journal hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalHierarchyDesign() {
        return (getAdapteeManager().supportsJournalHierarchyDesign());
    }


    /**
     *  Tests for the availability of a journaling batch service. 
     *
     *  @return <code> true </code> if journaling batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalngBatch() {
        return (getAdapteeManager().supportsJournalngBatch());
    }


    /**
     *  Gets the supported <code> JournalEntry </code> record types. 
     *
     *  @return a list containing the supported journal entry record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJournalEntryRecordTypes() {
        return (getAdapteeManager().getJournalEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> JournalEntry </code> record type is 
     *  supported. 
     *
     *  @param  journalEntryRecordType a <code> Type </code> indicating a 
     *          <code> JournalEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> journalEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJournalEntryRecordType(org.osid.type.Type journalEntryRecordType) {
        return (getAdapteeManager().supportsJournalEntryRecordType(journalEntryRecordType));
    }


    /**
     *  Gets the supported journal entry search record types. 
     *
     *  @return a list containing the supported journal entry search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJournalEntrySearchRecordTypes() {
        return (getAdapteeManager().getJournalEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given journal entry search record type is supported. 
     *
     *  @param  journalEntrySearchRecordType a <code> Type </code> indicating 
     *          a journal entry record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          journalEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJournalEntrySearchRecordType(org.osid.type.Type journalEntrySearchRecordType) {
        return (getAdapteeManager().supportsJournalEntrySearchRecordType(journalEntrySearchRecordType));
    }


    /**
     *  Gets all the branch record types supported. 
     *
     *  @return the list of supported branch record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBranchRecordTypes() {
        return (getAdapteeManager().getBranchRecordTypes());
    }


    /**
     *  Tests if a given branch record type is supported. 
     *
     *  @param  branchRecordType the branch type 
     *  @return <code> true </code> if the branch record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> branchRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBranchRecordType(org.osid.type.Type branchRecordType) {
        return (getAdapteeManager().supportsBranchRecordType(branchRecordType));
    }


    /**
     *  Gets all the branch search record types supported. 
     *
     *  @return the list of supported branch search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBranchSearchRecordTypes() {
        return (getAdapteeManager().getBranchSearchRecordTypes());
    }


    /**
     *  Tests if a given branch search type is supported. 
     *
     *  @param  branchSearchRecordType the branch search type 
     *  @return <code> true </code> if the branch search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> branchSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBranchSearchRecordType(org.osid.type.Type branchSearchRecordType) {
        return (getAdapteeManager().supportsBranchSearchRecordType(branchSearchRecordType));
    }


    /**
     *  Gets the supported <code> Journal </code> record types. 
     *
     *  @return a list containing the supported journal record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJournalRecordTypes() {
        return (getAdapteeManager().getJournalRecordTypes());
    }


    /**
     *  Tests if the given <code> Journal </code> record type is supported. 
     *
     *  @param  journalRecordType a <code> Type </code> indicating a <code> 
     *          Journal </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> journalRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJournalRecordType(org.osid.type.Type journalRecordType) {
        return (getAdapteeManager().supportsJournalRecordType(journalRecordType));
    }


    /**
     *  Gets the supported journal search record types. 
     *
     *  @return a list containing the supported journal search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJournalSearchRecordTypes() {
        return (getAdapteeManager().getJournalSearchRecordTypes());
    }


    /**
     *  Tests if the given journal search record type is supported. 
     *
     *  @param  journalSearchRecordType a <code> Type </code> indicating a 
     *          journal record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          journalEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJournalSearchRecordType(org.osid.type.Type journalSearchRecordType) {
        return (getAdapteeManager().supportsJournalSearchRecordType(journalSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryLookupSession getJournalEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  lookup service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryLookupSession getJournalEntryLookupSessionForJournal(org.osid.id.Id journalId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryLookupSessionForJournal(journalId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuerySession getJournalEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  query service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuerySession getJournalEntryQuerySessionForJournal(org.osid.id.Id journalId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryQuerySessionForJournal(journalId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchSession getJournalEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  search service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchSession getJournalEntrySearchSessionForJournal(org.osid.id.Id journalId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntrySearchSessionForJournal(journalId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryAdminSession getJournalEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  administration service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryAdminSession getJournalEntryAdminSessionForJournal(org.osid.id.Id journalId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryAdminSessionForJournal(journalId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  notification service. 
     *
     *  @param  journalEntryReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> journalEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryNotificationSession getJournalEntryNotificationSession(org.osid.journaling.JournalEntryReceiver journalEntryReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryNotificationSession(journalEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  notification service for the given journal. 
     *
     *  @param  journalEntryReceiver the receiver 
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalEntryReceiver, 
     *          journalId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryNotificationSession getJournalEntryNotificationSessionForJournal(org.osid.journaling.JournalEntryReceiver journalEntryReceiver, 
                                                                                                            org.osid.id.Id journalId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryNotificationSessionForJournal(journalEntryReceiver, journalId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branch lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BranchLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchLookupSession getBranchLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branch lookup 
     *  service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy <code> a proxy </code> 
     *  @return <code> a BranchLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsBranchLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchLookupSession getBranchLookupSessionForJournal(org.osid.id.Id journalId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchLookupSessionForJournal(journalId, proxy));
    }


    /**
     *  Gets a branch query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BranchQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuerySession getBranchQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchQuerySession(proxy));
    }


    /**
     *  Gets a branch query session for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return <code> a BranchQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuerySession getBranchQuerySessionForJournal(org.osid.id.Id journalId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchQuerySessionForJournal(journalId, proxy));
    }


    /**
     *  Gets a branch search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BranchSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSearchSession getBranchSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchSearchSession(proxy));
    }


    /**
     *  Gets a branch search session for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return <code> a BranchSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsBranchSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSearchSession getBranchSearchSessionForJournal(org.osid.id.Id journalId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchSearchSessionForJournal(journalId, proxy));
    }


    /**
     *  Gets a branch administration session for creating, updating and 
     *  deleting branches. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BranchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchAdminSession getBranchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchAdminSession(proxy));
    }


    /**
     *  Gets a branch administration session for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return <code> a BranchAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchAdminSession getBranchAdminSessionForJournal(org.osid.id.Id journalId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchAdminSessionForJournal(journalId, proxy));
    }


    /**
     *  Gets the branch notification session for the given journal. 
     *
     *  @param  branchReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a BranchNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> branchReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchNotificationSession getBranchNotificationSession(org.osid.journaling.BranchReceiver branchReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchNotificationSession(branchReceiver, proxy));
    }


    /**
     *  Gets the branch notification session for the given journal. 
     *
     *  @param  branchReceiver notification callback 
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return <code> a BranchNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> branchReceiver, 
     *          journalId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchNotificationSession getBranchNotificationSessionForJournal(org.osid.journaling.BranchReceiver branchReceiver, 
                                                                                                org.osid.id.Id journalId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchNotificationSessionForJournal(branchReceiver, journalId, proxy));
    }


    /**
     *  Gets the session for managing dynamic branch journals. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return a <code> BranchSmartJournalSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchSmartJournal() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSmartJournalSession getBranchSmartJournalSession(org.osid.id.Id journalId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchSmartJournalSession(journalId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalLookupSession getJournalLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalQuerySession getJournalQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalSearchSession getJournalSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalAdminSession getJournalAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  notification service. 
     *
     *  @param  journalReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> JournalNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> journalReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalNotificationSession getJournalNotificationSession(org.osid.journaling.JournalReceiver journalReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalNotificationSession(journalReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalHierarchySession getJournalHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalHierarchyDesignSession getJournalHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> JournalingBatchProxyManager. </code> 
     *
     *  @return a <code> JournalingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalingBatchProxyManager getJournalingBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalingBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
