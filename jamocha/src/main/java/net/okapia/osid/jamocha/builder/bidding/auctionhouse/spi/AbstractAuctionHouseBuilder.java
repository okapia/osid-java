//
// AbstractAuctionHouse.java
//
//     Defines an AuctionHouse builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.auctionhouse.spi;


/**
 *  Defines an <code>AuctionHouse</code> builder.
 */

public abstract class AbstractAuctionHouseBuilder<T extends AbstractAuctionHouseBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCatalogBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.bidding.auctionhouse.AuctionHouseMiter auctionHouse;


    /**
     *  Constructs a new <code>AbstractAuctionHouseBuilder</code>.
     *
     *  @param auctionHouse the auction house to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAuctionHouseBuilder(net.okapia.osid.jamocha.builder.bidding.auctionhouse.AuctionHouseMiter auctionHouse) {
        super(auctionHouse);
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Builds the auction house.
     *
     *  @return the new auction house
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.bidding.AuctionHouse build() {
        (new net.okapia.osid.jamocha.builder.validator.bidding.auctionhouse.AuctionHouseValidator(getValidations())).validate(this.auctionHouse);
        return (new net.okapia.osid.jamocha.builder.bidding.auctionhouse.ImmutableAuctionHouse(this.auctionHouse));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the auctionHouse miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.bidding.auctionhouse.AuctionHouseMiter getMiter() {
        return (this.auctionHouse);
    }


    /**
     *  Adds an AuctionHouse record.
     *
     *  @param record an auction house record
     *  @param recordType the type of auction house record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.bidding.records.AuctionHouseRecord record, org.osid.type.Type recordType) {
        getMiter().addAuctionHouseRecord(record, recordType);
        return (self());
    }
}       


