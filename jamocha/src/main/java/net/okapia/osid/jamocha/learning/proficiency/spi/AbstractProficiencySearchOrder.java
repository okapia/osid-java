//
// AbstractProficiencySearchOdrer.java
//
//     Defines a ProficiencySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.proficiency.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ProficiencySearchOrder}.
 */

public abstract class AbstractProficiencySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.learning.ProficiencySearchOrder {

    private final java.util.Collection<org.osid.learning.records.ProficiencySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets a resource search order. 
     *
     *  @return a resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the objective. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByObjective(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an objective search order is available. 
     *
     *  @return <code> true </code> if an objective search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSearchOrder() {
        return (false);
    }


    /**
     *  Gets an objective search order. 
     *
     *  @return an objective search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchOrder getObjectiveSearchOrder() {
        throw new org.osid.UnimplementedException("supportsObjectiveSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the completion. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompletion(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the level. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLevel(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLevelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getLevelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLevelSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  proficiencyRecordType a proficiency record type 
     *  @return {@code true} if the proficiencyRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code proficiencyRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type proficiencyRecordType) {
        for (org.osid.learning.records.ProficiencySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  proficiencyRecordType the proficiency record type 
     *  @return the proficiency search order record
     *  @throws org.osid.NullArgumentException
     *          {@code proficiencyRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(proficiencyRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencySearchOrderRecord getProficiencySearchOrderRecord(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ProficiencySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proficiencyRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this proficiency. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param proficiencyRecord the proficiency search odrer record
     *  @param proficiencyRecordType proficiency record type
     *  @throws org.osid.NullArgumentException
     *          {@code proficiencyRecord} or
     *          {@code proficiencyRecordTypeproficiency} is
     *          {@code null}
     */
            
    protected void addProficiencyRecord(org.osid.learning.records.ProficiencySearchOrderRecord proficiencySearchOrderRecord, 
                                     org.osid.type.Type proficiencyRecordType) {

        addRecordType(proficiencyRecordType);
        this.records.add(proficiencySearchOrderRecord);
        
        return;
    }
}
