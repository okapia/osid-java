//
// AbstractRouteLookupSession.java
//
//    A starter implementation framework for providing a Route
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Route
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRoutes(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRouteLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.mapping.route.RouteLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();
    

    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>Route</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRoutes() {
        return (true);
    }


    /**
     *  A complete view of the <code>Route</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRouteView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Route</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRouteView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include routes in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only routes whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveRouteView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All routes of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRouteView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Route</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Route</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Route</code> and
     *  retained for compatibility.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and
     *  those currently expired are returned.
     *
     *  @param  routeId <code>Id</code> of the
     *          <code>Route</code>
     *  @return the route
     *  @throws org.osid.NotFoundException <code>routeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>routeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.Route getRoute(org.osid.id.Id routeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.mapping.route.RouteList routes = getRoutes()) {
            while (routes.hasNext()) {
                org.osid.mapping.route.Route route = routes.getNextRoute();
                if (route.getId().equals(routeId)) {
                    return (route);
                }
            }
        } 

        throw new org.osid.NotFoundException(routeId + " not found");
    }


    /**
     *  Gets a <code>RouteList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  routes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Routes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, routes are returned that are currently effective.
     *  In any effective mode, effective routes and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRoutes()</code>.
     *
     *  @param  routeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Route</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>routeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByIds(org.osid.id.IdList routeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.mapping.route.Route> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = routeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRoute(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("route " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.mapping.route.route.LinkedRouteList(ret));
    }


    /**
     *  Gets a <code>RouteList</code> corresponding to the given route
     *  genus <code>Type</code> which does not include routes of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRoutes()</code>.
     *
     *  @param  routeGenusType a route genus type 
     *  @return the returned <code>Route</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>routeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByGenusType(org.osid.type.Type routeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.route.route.RouteGenusFilterList(getRoutes(), routeGenusType));
    }


    /**
     *  Gets a <code>RouteList</code> corresponding to the given
     *  route genus <code>Type</code> and include any additional
     *  routes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRoutes()</code>.
     *
     *  @param  routeGenusType a route genus type 
     *  @return the returned <code>Route</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>routeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByParentGenusType(org.osid.type.Type routeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRoutesByGenusType(routeGenusType));
    }


    /**
     *  Gets a <code>RouteList</code> containing the given
     *  route record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRoutes()</code>.
     *
     *  @param  routeRecordType a route record type 
     *  @return the returned <code>Route</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByRecordType(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.route.route.RouteRecordFilterList(getRoutes(), routeRecordType));
    }


    /**
     *  Gets a <code>RouteList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *  
     *  In active mode, routes are returned that are currently
     *  active. In any status mode, active and inactive routes are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Route</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.route.route.TemporalRouteFilterList(getRoutes(), from, to));
    }
        

    /**
     *  Gets a list of routes corresponding to a starting location
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible
     *  through this session.
     *
     *  In effective mode, routes are returned that are
     *  currently effective.  In any effective mode, effective
     *  routes and those currently expired are returned.
     *
     *  @param  startingLocationId the <code>Id</code> of the starting location
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException <code>startingLocationId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.mapping.route.RouteList getRoutesForStartingLocation(org.osid.id.Id startingLocationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.route.route.RouteFilterList(new StartingLocationFilter(startingLocationId), getRoutes()));
    }


    /**
     *  Gets a list of routes corresponding to a starting location
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible
     *  through this session.
     *
     *  In effective mode, routes are returned that are
     *  currently effective.  In any effective mode, effective
     *  routes and those currently expired are returned.
     *
     *  @param  startingLocationId the <code>Id</code> of the starting location
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException <code>startingLocationId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingLocationOnDate(org.osid.id.Id startingLocationId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.route.route.TemporalRouteFilterList(getRoutesForStartingLocation(startingLocationId), from, to));
    }


    /**
     *  Gets a list of routes corresponding to a ending location
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  endingLocationId the <code>Id</code> of the ending location
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>endingLocationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.mapping.route.RouteList getRoutesForEndingLocation(org.osid.id.Id endingLocationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.route.route.RouteFilterList(new EndingLocationFilter(endingLocationId), getRoutes()));
    }


    /**
     *  Gets a list of routes corresponding to a ending location
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  endingLocationId the <code>Id</code> of the ending location
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>endingLocationId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForEndingLocationOnDate(org.osid.id.Id endingLocationId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.route.route.TemporalRouteFilterList(getRoutesForEndingLocation(endingLocationId), from, to));
    }


    /**
     *  Gets a list of routes corresponding to starting location and
     *  ending location <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  startingLocationId the <code>Id</code> of the starting location
     *  @param  endingLocationId the <code>Id</code> of the ending location
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingLocationId</code>,
     *          <code>endingLocationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingAndEndingLocation(org.osid.id.Id startingLocationId,
                                                                                  org.osid.id.Id endingLocationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.route.route.RouteFilterList(new EndingLocationFilter(endingLocationId), getRoutesForStartingLocation(startingLocationId)));
    }


    /**
     *  Gets a list of routes corresponding to starting location and
     *  ending location <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  endingLocationId the <code>Id</code> of the ending location
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingLocationId</code>,
     *          <code>endingLocationId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingAndEndingLocationOnDate(org.osid.id.Id startingLocationId,
                                                                                        org.osid.id.Id endingLocationId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.route.route.TemporalRouteFilterList(getRoutesForStartingAndEndingLocation(startingLocationId, endingLocationId), from, to));
    }


    /**
     *  Gets a <code>RouteList</code> connected to all the given
     *  <code>Locations</code> .
     *  
     *  In plenary mode, the returned list contains all of the routes
     *  along the locations, or an error results if a route connected
     *  to the location is not found or inaccessible. Otherwise,
     *  inaccessible <code>Routes</code> may be omitted from the list.
     *  
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  This method should be implemmented.
     *
     *  @param  locationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code> Route </code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>locationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesAlongLocations(org.osid.id.IdList locationIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.nil.mapping.route.route.EmptyRouteList());
    }


    /**
     *  Gets all <code>Routes</code>. 
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Routes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.mapping.route.RouteList getRoutes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the route list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of routes
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.mapping.route.RouteList filterRoutesOnViews(org.osid.mapping.route.RouteList list)
        throws org.osid.OperationFailedException {

        org.osid.mapping.route.RouteList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.mapping.route.route.EffectiveRouteFilterList(ret);
        }

        return (ret);
    }

    public static class StartingLocationFilter
        implements net.okapia.osid.jamocha.inline.filter.mapping.route.route.RouteFilter {
         
        private final org.osid.id.Id startingLocationId;
         
         
        /**
         *  Constructs a new <code>StartingLocationFilter</code>.
         *
         *  @param startingLocationId the starting location to filter
         *  @throws org.osid.NullArgumentException
         *          <code>startingLocationId</code> is <code>null</code>
         */
        
        public StartingLocationFilter(org.osid.id.Id startingLocationId) {
            nullarg(startingLocationId, "starting location Id");
            this.startingLocationId = startingLocationId;
            return;
        }

         
        /**
         *  Used by the RouteFilterList to filter the 
         *  route list based on starting location.
         *
         *  @param route the route
         *  @return <code>true</code> to pass the route,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.mapping.route.Route route) {
            return (route.getStartingLocationId().equals(this.startingLocationId));
        }
    }


    public static class EndingLocationFilter
        implements net.okapia.osid.jamocha.inline.filter.mapping.route.route.RouteFilter {
         
        private final org.osid.id.Id endingLocationId;
         
         
        /**
         *  Constructs a new <code>EndingLocationFilter</code>.
         *
         *  @param endingLocationId the ending location to filter
         *  @throws org.osid.NullArgumentException
         *          <code>endingLocationId</code> is <code>null</code>
         */
        
        public EndingLocationFilter(org.osid.id.Id endingLocationId) {
            nullarg(endingLocationId, "ending location Id");
            this.endingLocationId = endingLocationId;
            return;
        }

         
        /**
         *  Used by the RouteFilterList to filter the 
         *  route list based on ending location.
         *
         *  @param route the route
         *  @return <code>true</code> to pass the route,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.mapping.route.Route route) {
            return (route.getEndingLocationId().equals(this.endingLocationId));
        }
    }
}
