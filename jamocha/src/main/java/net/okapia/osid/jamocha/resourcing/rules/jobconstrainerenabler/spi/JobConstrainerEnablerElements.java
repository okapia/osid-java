//
// JobConstrainerEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobconstrainerenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class JobConstrainerEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the JobConstrainerEnablerElement Id.
     *
     *  @return the job constrainer enabler element Id
     */

    public static org.osid.id.Id getJobConstrainerEnablerEntityId() {
        return (makeEntityId("osid.resourcing.rules.JobConstrainerEnabler"));
    }


    /**
     *  Gets the RuledJobConstrainerId element Id.
     *
     *  @return the RuledJobConstrainerId element Id
     */

    public static org.osid.id.Id getRuledJobConstrainerId() {
        return (makeQueryElementId("osid.resourcing.rules.jobconstrainerenabler.RuledJobConstrainerId"));
    }


    /**
     *  Gets the RuledJobConstrainer element Id.
     *
     *  @return the RuledJobConstrainer element Id
     */

    public static org.osid.id.Id getRuledJobConstrainer() {
        return (makeQueryElementId("osid.resourcing.rules.jobconstrainerenabler.RuledJobConstrainer"));
    }


    /**
     *  Gets the FoundryId element Id.
     *
     *  @return the FoundryId element Id
     */

    public static org.osid.id.Id getFoundryId() {
        return (makeQueryElementId("osid.resourcing.rules.jobconstrainerenabler.FoundryId"));
    }


    /**
     *  Gets the Foundry element Id.
     *
     *  @return the Foundry element Id
     */

    public static org.osid.id.Id getFoundry() {
        return (makeQueryElementId("osid.resourcing.rules.jobconstrainerenabler.Foundry"));
    }
}
