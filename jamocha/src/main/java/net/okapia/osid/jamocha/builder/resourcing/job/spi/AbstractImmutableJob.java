//
// AbstractImmutableJob.java
//
//     Wraps a mutable Job to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.job.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Job</code> to hide modifiers. This
 *  wrapper provides an immutized Job from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying job whose state changes are visible.
 */

public abstract class AbstractImmutableJob
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.resourcing.Job {

    private final org.osid.resourcing.Job job;


    /**
     *  Constructs a new <code>AbstractImmutableJob</code>.
     *
     *  @param job the job to immutablize
     *  @throws org.osid.NullArgumentException <code>job</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableJob(org.osid.resourcing.Job job) {
        super(job);
        this.job = job;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the competencies. 
     *
     *  @return the competency <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCompetencyIds() {
        return (this.job.getCompetencyIds());
    }


    /**
     *  Gets the competencies. 
     *
     *  @return the competencies 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetencies()
        throws org.osid.OperationFailedException {

        return (this.job.getCompetencies());
    }


    /**
     *  Gets the job record corresponding to the given <code> Job </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> jobRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(jobRecordType) </code> is <code> true </code> . 
     *
     *  @param  jobRecordType the type of job record to retrieve 
     *  @return the job record 
     *  @throws org.osid.NullArgumentException <code> jobRecordType </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(jobRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.records.JobRecord getJobRecord(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException {

        return (this.job.getJobRecord(jobRecordType));
    }
}

