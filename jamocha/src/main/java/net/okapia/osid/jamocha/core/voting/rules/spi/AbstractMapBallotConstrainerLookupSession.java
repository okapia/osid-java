//
// AbstractMapBallotConstrainerLookupSession
//
//    A simple framework for providing a BallotConstrainer lookup service
//    backed by a fixed collection of ballot constrainers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a BallotConstrainer lookup service backed by a
 *  fixed collection of ballot constrainers. The ballot constrainers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BallotConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBallotConstrainerLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractBallotConstrainerLookupSession
    implements org.osid.voting.rules.BallotConstrainerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.rules.BallotConstrainer> ballotConstrainers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.rules.BallotConstrainer>());


    /**
     *  Makes a <code>BallotConstrainer</code> available in this session.
     *
     *  @param  ballotConstrainer a ballot constrainer
     *  @throws org.osid.NullArgumentException <code>ballotConstrainer<code>
     *          is <code>null</code>
     */

    protected void putBallotConstrainer(org.osid.voting.rules.BallotConstrainer ballotConstrainer) {
        this.ballotConstrainers.put(ballotConstrainer.getId(), ballotConstrainer);
        return;
    }


    /**
     *  Makes an array of ballot constrainers available in this session.
     *
     *  @param  ballotConstrainers an array of ballot constrainers
     *  @throws org.osid.NullArgumentException <code>ballotConstrainers<code>
     *          is <code>null</code>
     */

    protected void putBallotConstrainers(org.osid.voting.rules.BallotConstrainer[] ballotConstrainers) {
        putBallotConstrainers(java.util.Arrays.asList(ballotConstrainers));
        return;
    }


    /**
     *  Makes a collection of ballot constrainers available in this session.
     *
     *  @param  ballotConstrainers a collection of ballot constrainers
     *  @throws org.osid.NullArgumentException <code>ballotConstrainers<code>
     *          is <code>null</code>
     */

    protected void putBallotConstrainers(java.util.Collection<? extends org.osid.voting.rules.BallotConstrainer> ballotConstrainers) {
        for (org.osid.voting.rules.BallotConstrainer ballotConstrainer : ballotConstrainers) {
            this.ballotConstrainers.put(ballotConstrainer.getId(), ballotConstrainer);
        }

        return;
    }


    /**
     *  Removes a BallotConstrainer from this session.
     *
     *  @param  ballotConstrainerId the <code>Id</code> of the ballot constrainer
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerId<code> is
     *          <code>null</code>
     */

    protected void removeBallotConstrainer(org.osid.id.Id ballotConstrainerId) {
        this.ballotConstrainers.remove(ballotConstrainerId);
        return;
    }


    /**
     *  Gets the <code>BallotConstrainer</code> specified by its <code>Id</code>.
     *
     *  @param  ballotConstrainerId <code>Id</code> of the <code>BallotConstrainer</code>
     *  @return the ballotConstrainer
     *  @throws org.osid.NotFoundException <code>ballotConstrainerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainer getBallotConstrainer(org.osid.id.Id ballotConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.rules.BallotConstrainer ballotConstrainer = this.ballotConstrainers.get(ballotConstrainerId);
        if (ballotConstrainer == null) {
            throw new org.osid.NotFoundException("ballotConstrainer not found: " + ballotConstrainerId);
        }

        return (ballotConstrainer);
    }


    /**
     *  Gets all <code>BallotConstrainers</code>. In plenary mode, the returned
     *  list contains all known ballotConstrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  ballotConstrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>BallotConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.ballotconstrainer.ArrayBallotConstrainerList(this.ballotConstrainers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.ballotConstrainers.clear();
        super.close();
        return;
    }
}
