//
// AbstractAdapterRaceProcessorEnablerLookupSession.java
//
//    A RaceProcessorEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A RaceProcessorEnabler lookup session adapter.
 */

public abstract class AbstractAdapterRaceProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.rules.RaceProcessorEnablerLookupSession {

    private final org.osid.voting.rules.RaceProcessorEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRaceProcessorEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRaceProcessorEnablerLookupSession(org.osid.voting.rules.RaceProcessorEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Polls/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@code RaceProcessorEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRaceProcessorEnablers() {
        return (this.session.canLookupRaceProcessorEnablers());
    }


    /**
     *  A complete view of the {@code RaceProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceProcessorEnablerView() {
        this.session.useComparativeRaceProcessorEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code RaceProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceProcessorEnablerView() {
        this.session.usePlenaryRaceProcessorEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race processor enablers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active race processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRaceProcessorEnablerView() {
        this.session.useActiveRaceProcessorEnablerView();
        return;
    }


    /**
     *  Active and inactive race processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceProcessorEnablerView() {
        this.session.useAnyStatusRaceProcessorEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code RaceProcessorEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code RaceProcessorEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code RaceProcessorEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, race processor enablers are returned that are currently
     *  active. In any status mode, active and inactive race processor enablers
     *  are returned.
     *
     *  @param raceProcessorEnablerId {@code Id} of the {@code RaceProcessorEnabler}
     *  @return the race processor enabler
     *  @throws org.osid.NotFoundException {@code raceProcessorEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code raceProcessorEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnabler getRaceProcessorEnabler(org.osid.id.Id raceProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorEnabler(raceProcessorEnablerId));
    }


    /**
     *  Gets a {@code RaceProcessorEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  raceProcessorEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code RaceProcessorEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, race processor enablers are returned that are currently
     *  active. In any status mode, active and inactive race processor enablers
     *  are returned.
     *
     *  @param  raceProcessorEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code RaceProcessorEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByIds(org.osid.id.IdList raceProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorEnablersByIds(raceProcessorEnablerIds));
    }


    /**
     *  Gets a {@code RaceProcessorEnablerList} corresponding to the given
     *  race processor enabler genus {@code Type} which does not include
     *  race processor enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  race processor enablers or an error results. Otherwise, the returned list
     *  may contain only those race processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processor enablers are returned that are currently
     *  active. In any status mode, active and inactive race processor enablers
     *  are returned.
     *
     *  @param  raceProcessorEnablerGenusType a raceProcessorEnabler genus type 
     *  @return the returned {@code RaceProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByGenusType(org.osid.type.Type raceProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorEnablersByGenusType(raceProcessorEnablerGenusType));
    }


    /**
     *  Gets a {@code RaceProcessorEnablerList} corresponding to the given
     *  race processor enabler genus {@code Type} and include any additional
     *  race processor enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  race processor enablers or an error results. Otherwise, the returned list
     *  may contain only those race processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processor enablers are returned that are currently
     *  active. In any status mode, active and inactive race processor enablers
     *  are returned.
     *
     *  @param  raceProcessorEnablerGenusType a raceProcessorEnabler genus type 
     *  @return the returned {@code RaceProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByParentGenusType(org.osid.type.Type raceProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorEnablersByParentGenusType(raceProcessorEnablerGenusType));
    }


    /**
     *  Gets a {@code RaceProcessorEnablerList} containing the given
     *  race processor enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  race processor enablers or an error results. Otherwise, the returned list
     *  may contain only those race processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processor enablers are returned that are currently
     *  active. In any status mode, active and inactive race processor enablers
     *  are returned.
     *
     *  @param  raceProcessorEnablerRecordType a raceProcessorEnabler record type 
     *  @return the returned {@code RaceProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByRecordType(org.osid.type.Type raceProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorEnablersByRecordType(raceProcessorEnablerRecordType));
    }


    /**
     *  Gets a {@code RaceProcessorEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  race processor enablers or an error results. Otherwise, the returned list
     *  may contain only those race processor enablers that are accessible
     *  through this session.
     *  
     *  In active mode, race processor enablers are returned that are currently
     *  active. In any status mode, active and inactive race processor enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RaceProcessorEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code RaceProcessorEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  race processor enablers or an error results. Otherwise, the returned list
     *  may contain only those race processor enablers that are accessible
     *  through this session.
     *
     *  In active mode, race processor enablers are returned that are currently
     *  active. In any status mode, active and inactive race processor enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code RaceProcessorEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getRaceProcessorEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code RaceProcessorEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  race processor enablers or an error results. Otherwise, the returned list
     *  may contain only those race processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processor enablers are returned that are currently
     *  active. In any status mode, active and inactive race processor enablers
     *  are returned.
     *
     *  @return a list of {@code RaceProcessorEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorEnablers());
    }
}
