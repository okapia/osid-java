//
// AbstractMutableProgramOffering.java
//
//     Defines a mutable ProgramOffering.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.programoffering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>ProgramOffering</code>.
 */

public abstract class AbstractMutableProgramOffering
    extends net.okapia.osid.jamocha.course.program.programoffering.spi.AbstractProgramOffering
    implements org.osid.course.program.ProgramOffering,
               net.okapia.osid.jamocha.builder.course.program.programoffering.ProgramOfferingMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this program offering. 
     *
     *  @param record program offering record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addProgramOfferingRecord(org.osid.course.program.records.ProgramOfferingRecord record, org.osid.type.Type recordType) {
        super.addProgramOfferingRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this program offering is
     *  effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this program offering ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }

    
    /**
     *  Sets the display name for this program offering.
     *
     *  @param displayName the name for this program offering
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this program offering.
     *
     *  @param description the description of this program offering
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this program offering
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    @Override
    public void setProgram(org.osid.course.program.Program program) {
        super.setProgram(program);
        return;
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    @Override
    public void setTerm(org.osid.course.Term term) {
        super.setTerm(term);
        return;
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    @Override
    public void setTitle(org.osid.locale.DisplayText title) {
        super.setTitle(title);
        return;
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    @Override
    public void setNumber(String number) {
        super.setNumber(number);
        return;
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    @Override
    public void addSponsor(org.osid.resource.Resource sponsor) {
        super.addSponsor(sponsor);
        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    @Override
    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        super.setSponsors(sponsors);
        return;
    }


    /**
     *  Sets the completion requirements info.
     *
     *  @param info a completion requirements info
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    @Override
    public void setCompletionRequirementsInfo(org.osid.locale.DisplayText info) {
        super.setCompletionRequirementsInfo(info);
        return;
    }


    /**
     *  Adds a completion requirement.
     *
     *  @param requirement a completion requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    @Override
    public void addCompletionRequirement(org.osid.course.requisite.Requisite requirement) {
        super.addCompletionRequirement(requirement);
        return;
    }


    /**
     *  Sets all the completion requirements.
     *
     *  @param requirements a collection of completion requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    @Override
    public void setCompletionRequirements(java.util.Collection<org.osid.course.requisite.Requisite> requirements) {
        super.setCompletionRequirements(requirements);
        return;
    }


    /**
     *  Adds a credential.
     *
     *  @param credential a credential
     *  @throws org.osid.NullArgumentException <code>credential</code>
     *          is <code>null</code>
     */

    @Override
    public void addCredential(org.osid.course.program.Credential credential) {
        super.addCredential(credential);
        return;
    }


    /**
     *  Sets all the credentials.
     *
     *  @param credentials a collection of credentials
     *  @throws org.osid.NullArgumentException
     *          <code>credentials</code> is <code>null</code>
     */

    @Override
    public void setCredentials(java.util.Collection<org.osid.course.program.Credential> credentials) {
        super.setCredentials(credentials);
        return;
    }


    /**
     *  Sets the requires registration flag.
     *
     *  @param requires <code> true </code> if this program offering
     *          requires advance registration, <code> false </code>
     *          otherwise
     */

    @Override
    public void setRequiresRegistration(boolean requires) {
        super.setRequiresRegistration(requires);
        return;
    }


    /**
     *  Sets the minimum seats.
     *
     *  @param seats the minimum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    @Override
    public void setMinimumSeats(long seats) {
        super.setMinimumSeats(seats);
        return;
    }


    /**
     *  Sets the maximum seats.
     *
     *  @param seats the maximum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    @Override
    public void setMaximumSeats(long seats) {
        super.setMaximumSeats(seats);
        return;
    }


    /**
     *  Sets the URL.
     *
     *  @param url a URL
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    @Override
    public void setURL(String url) {
        super.setURL(url);
        return;
    }
}

