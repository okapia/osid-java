//
// AbstractAssemblyStockQuery.java
//
//     A StockQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inventory.stock.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StockQuery that stores terms.
 */

public abstract class AbstractAssemblyStockQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.inventory.StockQuery,
               org.osid.inventory.StockQueryInspector,
               org.osid.inventory.StockSearchOrder {

    private final java.util.Collection<org.osid.inventory.records.StockQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.StockQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.StockSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyStockQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyStockQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches a sku number. 
     *
     *  @param  sku a sku 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sku </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSKU(String sku, org.osid.type.Type stringMatchType, 
                         boolean match) {
        getAssembler().addStringTerm(getSKUColumn(), sku, stringMatchType, match);
        return;
    }


    /**
     *  Matches items that have any sku. 
     *
     *  @param  match <code> true </code> to match items with any sku, <code> 
     *          false </code> to match items with no sku 
     */

    @OSID @Override
    public void matchAnySKU(boolean match) {
        getAssembler().addStringWildcardTerm(getSKUColumn(), match);
        return;
    }


    /**
     *  Clears the sku terms. 
     */

    @OSID @Override
    public void clearSKUTerms() {
        getAssembler().clearTerms(getSKUColumn());
        return;
    }


    /**
     *  Gets the SKU query terms. 
     *
     *  @return the SKU query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSKUTerms() {
        return (getAssembler().getStringTerms(getSKUColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the sku. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySKU(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSKUColumn(), style);
        return;
    }


    /**
     *  Gets the SKU column name.
     *
     * @return the column name
     */

    protected String getSKUColumn() {
        return ("s_ku");
    }


    /**
     *  Sets the model <code> Id </code> for this query. 
     *
     *  @param  modelId a model <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> modelId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModelId(org.osid.id.Id modelId, boolean match) {
        getAssembler().addIdTerm(getModelIdColumn(), modelId, match);
        return;
    }


    /**
     *  Clears the model <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModelIdTerms() {
        getAssembler().clearTerms(getModelIdColumn());
        return;
    }


    /**
     *  Gets the model <code> Id </code> query terms. 
     *
     *  @return the model <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModelIdTerms() {
        return (getAssembler().getIdTerms(getModelIdColumn()));
    }


    /**
     *  Gets the ModelId column name.
     *
     * @return the column name
     */

    protected String getModelIdColumn() {
        return ("model_id");
    }


    /**
     *  Tests if a <code> ModelQuery </code> is available. 
     *
     *  @return <code> true </code> if a model query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inventory. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the model query 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuery getModelQuery() {
        throw new org.osid.UnimplementedException("supportsModelQuery() is false");
    }


    /**
     *  Matches any related model. 
     *
     *  @param  match <code> true </code> to match stocks with any model, 
     *          <code> false </code> to match stocks with no models 
     */

    @OSID @Override
    public void matchAnyModel(boolean match) {
        getAssembler().addIdWildcardTerm(getModelColumn(), match);
        return;
    }


    /**
     *  Clears the model terms. 
     */

    @OSID @Override
    public void clearModelTerms() {
        getAssembler().clearTerms(getModelColumn());
        return;
    }


    /**
     *  Gets the model query terms. 
     *
     *  @return the model query terms 
     */

    @OSID @Override
    public org.osid.inventory.ModelQueryInspector[] getModelTerms() {
        return (new org.osid.inventory.ModelQueryInspector[0]);
    }


    /**
     *  Gets the Model column name.
     *
     * @return the column name
     */

    protected String getModelColumn() {
        return ("model");
    }


    /**
     *  Matches a location description. 
     *
     *  @param  location a location string 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        getAssembler().addStringTerm(getLocationDescriptionColumn(), location, stringMatchType, match);
        return;
    }


    /**
     *  Matches items that have any location description. 
     *
     *  @param  match <code> true </code> to match stocks with any location 
     *          string, <code> false </code> to match stocks with no location 
     *          string 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        getAssembler().addStringWildcardTerm(getLocationDescriptionColumn(), match);
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        getAssembler().clearTerms(getLocationDescriptionColumn());
        return;
    }


    /**
     *  Gets the location description query terms. 
     *
     *  @return the location description query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLocationDescriptionTerms() {
        return (getAssembler().getStringTerms(getLocationDescriptionColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the location 
     *  description. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocationDescription(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLocationDescriptionColumn(), style);
        return;
    }


    /**
     *  Gets the LocationDescription column name.
     *
     * @return the column name
     */

    protected String getLocationDescriptionColumn() {
        return ("location_description");
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        getAssembler().clearTerms(getLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the location <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (getAssembler().getIdTerms(getLocationIdColumn()));
    }


    /**
     *  Gets the LocationId column name.
     *
     * @return the column name
     */

    protected String getLocationIdColumn() {
        return ("location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches items that have any location. 
     *
     *  @param  match <code> true </code> to match stocks with any location, 
     *          <code> false </code> to match stocks with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getLocationColumn(), match);
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        getAssembler().clearTerms(getLocationColumn());
        return;
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the location query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the Location column name.
     *
     * @return the column name
     */

    protected String getLocationColumn() {
        return ("location");
    }


    /**
     *  Sets the stock <code> Id </code> for this query to match stocks that 
     *  have the specified stock as an ancestor. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorStockId(org.osid.id.Id stockId, boolean match) {
        getAssembler().addIdTerm(getAncestorStockIdColumn(), stockId, match);
        return;
    }


    /**
     *  Clears the ancestor stock <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorStockIdTerms() {
        getAssembler().clearTerms(getAncestorStockIdColumn());
        return;
    }


    /**
     *  Gets the ancestor stock <code> Id </code> query terms. 
     *
     *  @return the ancestor stock <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorStockIdTerms() {
        return (getAssembler().getIdTerms(getAncestorStockIdColumn()));
    }


    /**
     *  Gets the AncestorStockId column name.
     *
     * @return the column name
     */

    protected String getAncestorStockIdColumn() {
        return ("ancestor_stock_id");
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorStockQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getAncestorStockQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorStockQuery() is false");
    }


    /**
     *  Matches stocks with any stock ancestor. 
     *
     *  @param  match <code> true </code> to match stocks with any ancestor, 
     *          <code> false </code> to match root stocks 
     */

    @OSID @Override
    public void matchAnyAncestorStock(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorStockColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor stock query terms. 
     */

    @OSID @Override
    public void clearAncestorStockTerms() {
        getAssembler().clearTerms(getAncestorStockColumn());
        return;
    }


    /**
     *  Gets the ancestor stock query terms. 
     *
     *  @return the ancestor stock terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getAncestorStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Gets the AncestorStock column name.
     *
     * @return the column name
     */

    protected String getAncestorStockColumn() {
        return ("ancestor_stock");
    }


    /**
     *  Sets the stock <code> Id </code> for this query to match stocks that 
     *  have the specified stock as an descendant. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantStockId(org.osid.id.Id stockId, boolean match) {
        getAssembler().addIdTerm(getDescendantStockIdColumn(), stockId, match);
        return;
    }


    /**
     *  Clears the descendant stock <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantStockIdTerms() {
        getAssembler().clearTerms(getDescendantStockIdColumn());
        return;
    }


    /**
     *  Gets the descendant stock <code> Id </code> query terms. 
     *
     *  @return the descendant stock <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantStockIdTerms() {
        return (getAssembler().getIdTerms(getDescendantStockIdColumn()));
    }


    /**
     *  Gets the DescendantStockId column name.
     *
     * @return the column name
     */

    protected String getDescendantStockIdColumn() {
        return ("descendant_stock_id");
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantStockQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getDescendantStockQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantStockQuery() is false");
    }


    /**
     *  Matches stocks with any descendant stock. 
     *
     *  @param  match <code> true </code> to match stocks with any descendant, 
     *          <code> false </code> to match leaf stocks 
     */

    @OSID @Override
    public void matchAnyDescendantStock(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantStockColumn(), match);
        return;
    }


    /**
     *  Clears the descendant stock query terms. 
     */

    @OSID @Override
    public void clearDescendantStockTerms() {
        getAssembler().clearTerms(getDescendantStockColumn());
        return;
    }


    /**
     *  Gets the descendant stock query terms. 
     *
     *  @return the descendant stock terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getDescendantStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Gets the DescendantStock column name.
     *
     * @return the column name
     */

    protected String getDescendantStockColumn() {
        return ("descendant_stock");
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match stocks 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        getAssembler().addIdTerm(getWarehouseIdColumn(), warehouseId, match);
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        getAssembler().clearTerms(getWarehouseIdColumn());
        return;
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (getAssembler().getIdTerms(getWarehouseIdColumn()));
    }


    /**
     *  Gets the WarehouseId column name.
     *
     * @return the column name
     */

    protected String getWarehouseIdColumn() {
        return ("warehouse_id");
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        getAssembler().clearTerms(getWarehouseColumn());
        return;
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }


    /**
     *  Gets the Warehouse column name.
     *
     * @return the column name
     */

    protected String getWarehouseColumn() {
        return ("warehouse");
    }


    /**
     *  Tests if this stock supports the given record
     *  <code>Type</code>.
     *
     *  @param  stockRecordType a stock record type 
     *  @return <code>true</code> if the stockRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type stockRecordType) {
        for (org.osid.inventory.records.StockQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stockRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  stockRecordType the stock record type 
     *  @return the stock query record 
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stockRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.StockQueryRecord getStockQueryRecord(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.StockQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stockRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  stockRecordType the stock record type 
     *  @return the stock query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stockRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.StockQueryInspectorRecord getStockQueryInspectorRecord(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.StockQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(stockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stockRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param stockRecordType the stock record type
     *  @return the stock search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stockRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.StockSearchOrderRecord getStockSearchOrderRecord(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.StockSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(stockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stockRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this stock. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param stockQueryRecord the stock query record
     *  @param stockQueryInspectorRecord the stock query inspector
     *         record
     *  @param stockSearchOrderRecord the stock search order record
     *  @param stockRecordType stock record type
     *  @throws org.osid.NullArgumentException
     *          <code>stockQueryRecord</code>,
     *          <code>stockQueryInspectorRecord</code>,
     *          <code>stockSearchOrderRecord</code> or
     *          <code>stockRecordTypestock</code> is
     *          <code>null</code>
     */
            
    protected void addStockRecords(org.osid.inventory.records.StockQueryRecord stockQueryRecord, 
                                      org.osid.inventory.records.StockQueryInspectorRecord stockQueryInspectorRecord, 
                                      org.osid.inventory.records.StockSearchOrderRecord stockSearchOrderRecord, 
                                      org.osid.type.Type stockRecordType) {

        addRecordType(stockRecordType);

        nullarg(stockQueryRecord, "stock query record");
        nullarg(stockQueryInspectorRecord, "stock query inspector record");
        nullarg(stockSearchOrderRecord, "stock search odrer record");

        this.queryRecords.add(stockQueryRecord);
        this.queryInspectorRecords.add(stockQueryInspectorRecord);
        this.searchOrderRecords.add(stockSearchOrderRecord);
        
        return;
    }
}
