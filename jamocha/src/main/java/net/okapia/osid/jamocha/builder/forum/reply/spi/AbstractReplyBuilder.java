//
// AbstractReply.java
//
//     Defines a Reply builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.forum.reply.spi;


/**
 *  Defines a <code>Reply</code> builder.
 */

public abstract class AbstractReplyBuilder<T extends AbstractReplyBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractContainableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.forum.reply.ReplyMiter reply;


    /**
     *  Constructs a new <code>AbstractReplyBuilder</code>.
     *
     *  @param reply the reply to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractReplyBuilder(net.okapia.osid.jamocha.builder.forum.reply.ReplyMiter reply) {
        super(reply);
        this.reply = reply;
        return;
    }


    /**
     *  Builds the reply.
     *
     *  @return the new reply
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.forum.Reply build() {
        (new net.okapia.osid.jamocha.builder.validator.forum.reply.ReplyValidator(getValidations())).validate(this.reply);
        return (new net.okapia.osid.jamocha.builder.forum.reply.ImmutableReply(this.reply));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the reply miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.forum.reply.ReplyMiter getMiter() {
        return (this.reply);
    }


    /**
     *  Sets the post.
     *
     *  @param post a post
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>post</code> is <code>null</code>
     */

    public T post(org.osid.forum.Post post) {
        getMiter().setPost(post);
        return (self());
    }


    /**
     *  Adds a reply.
     *
     *  @param reply a reply
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>reply</code> is
     *          <code>null</code>
     */

    public T reply(org.osid.forum.Reply reply) {
        getMiter().addReply(reply);
        return (self());
    }


    /**
     *  Sets all the replies.
     *
     *  @param replies a collection of replies
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>replies</code> is
     *          <code>null</code>
     */

    public T replies(java.util.Collection<org.osid.forum.Reply> replies) {
        getMiter().setReplies(replies);
        return (self());
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    public T timestamp(org.osid.calendaring.DateTime timestamp) {
        getMiter().setTimestamp(timestamp);
        return (self());
    }


    /**
     *  Sets the poster.
     *
     *  @param poster a poster
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>poster</code> is
     *          <code>null</code>
     */

    public T poster(org.osid.resource.Resource poster) {
        getMiter().setPoster(poster);
        return (self());
    }


    /**
     *  Sets the posting agent.
     *
     *  @param agent a posting agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T postingAgent(org.osid.authentication.Agent agent) {
        getMiter().setPostingAgent(agent);
        return (self());
    }


    /**
     *  Sets the subject line.
     *
     *  @param subjectLine a subject line
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>subjectLine</code> is <code>null</code>
     */

    public T subjectLine(org.osid.locale.DisplayText subjectLine) {
        getMiter().setSubjectLine(subjectLine);
        return (self());
    }


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    public T text(org.osid.locale.DisplayText text) {
        getMiter().setText(text);
        return (self());
    }


    /**
     *  Adds a Reply record.
     *
     *  @param record a reply record
     *  @param recordType the type of reply record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.forum.records.ReplyRecord record, org.osid.type.Type recordType) {
        getMiter().addReplyRecord(record, recordType);
        return (self());
    }
}       


