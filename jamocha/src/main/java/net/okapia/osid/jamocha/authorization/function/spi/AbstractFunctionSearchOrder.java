//
// AbstractFunctionSearchOdrer.java
//
//     Defines a FunctionSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.function.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code FunctionSearchOrder}.
 */

public abstract class AbstractFunctionSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.authorization.FunctionSearchOrder {

    private final java.util.Collection<org.osid.authorization.records.FunctionSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the qualifier 
     *  hierarchy. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQualifierHierarchy(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> HierarchySearchOrder </code> interface is available. 
     *
     *  @return <code> true </code> if a qualifier hierarchy search order 
     *          interface is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order interface for a qualifier hierarchy. 
     *
     *  @return the hierarchy search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySearchOrder getQualifierHierarchySearchOrder() {
        throw new org.osid.UnimplementedException("supportsQualifierHierarchySearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  functionRecordType a function record type 
     *  @return {@code true} if the functionRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code functionRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type functionRecordType) {
        for (org.osid.authorization.records.FunctionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(functionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  functionRecordType the function record type 
     *  @return the function search order record
     *  @throws org.osid.NullArgumentException
     *          {@code functionRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(functionRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionSearchOrderRecord getFunctionSearchOrderRecord(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.FunctionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(functionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(functionRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this function. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param functionRecord the function search odrer record
     *  @param functionRecordType function record type
     *  @throws org.osid.NullArgumentException
     *          {@code functionRecord} or
     *          {@code functionRecordTypefunction} is
     *          {@code null}
     */
            
    protected void addFunctionRecord(org.osid.authorization.records.FunctionSearchOrderRecord functionSearchOrderRecord, 
                                     org.osid.type.Type functionRecordType) {

        addRecordType(functionRecordType);
        this.records.add(functionSearchOrderRecord);
        
        return;
    }
}
