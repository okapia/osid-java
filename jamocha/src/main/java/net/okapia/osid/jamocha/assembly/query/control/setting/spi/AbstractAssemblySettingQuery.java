//
// AbstractAssemblySettingQuery.java
//
//     A SettingQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.setting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SettingQuery that stores terms.
 */

public abstract class AbstractAssemblySettingQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.control.SettingQuery,
               org.osid.control.SettingQueryInspector,
               org.osid.control.SettingSearchOrder {

    private final java.util.Collection<org.osid.control.records.SettingQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.SettingQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.SettingSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySettingQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySettingQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the controller <code> Id </code> for this query. 
     *
     *  @param  controllerId the controller <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchControllerId(org.osid.id.Id controllerId, boolean match) {
        getAssembler().addIdTerm(getControllerIdColumn(), controllerId, match);
        return;
    }


    /**
     *  Clears the controller <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearControllerIdTerms() {
        getAssembler().clearTerms(getControllerIdColumn());
        return;
    }


    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getControllerIdTerms() {
        return (getAssembler().getIdTerms(getControllerIdColumn()));
    }


    /**
     *  Orders the results by controller. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByController(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getControllerColumn(), style);
        return;
    }


    /**
     *  Gets the ControllerId column name.
     *
     * @return the column name
     */

    protected String getControllerIdColumn() {
        return ("controller_id");
    }


    /**
     *  Tests if a <code> ControllerQuery </code> is available. 
     *
     *  @return <code> true </code> if a controller query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> InputQntry. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the controller query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuery getControllerQuery() {
        throw new org.osid.UnimplementedException("supportsControllerQuery() is false");
    }


    /**
     *  Clears the controller query terms. 
     */

    @OSID @Override
    public void clearControllerTerms() {
        getAssembler().clearTerms(getControllerColumn());
        return;
    }


    /**
     *  Gets the controller query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getControllerTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Tests if a controller search order is available. 
     *
     *  @return <code> true </code> if a controller search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the controller search order. 
     *
     *  @return the controller search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsControllerSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchOrder getControllerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsControllerSearchOrder() is false");
    }


    /**
     *  Gets the Controller column name.
     *
     * @return the column name
     */

    protected String getControllerColumn() {
        return ("controller");
    }


    /**
     *  Matches on settings. 
     *
     *  @param match <code> true </code> for a positive match, <code>
     *         false </code> for a negative match
     */

    @OSID @Override
    public void matchOn(boolean match) {
        getAssembler().addBooleanTerm(getOnColumn(), match);
        return;
    }


    /**
     *  Clears the on query terms. 
     */

    @OSID @Override
    public void clearOnTerms() {
        getAssembler().clearTerms(getOnColumn());
        return;
    }


    /**
     *  Gets the on query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOnTerms() {
        return (getAssembler().getBooleanTerms(getOnColumn()));
    }


    /**
     *  Orders the results by on status. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOn(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOnColumn(), style);
        return;
    }


    /**
     *  Gets the On column name.
     *
     * @return the column name
     */

    protected String getOnColumn() {
        return ("on");
    }


    /**
     *  Matches off settings. 
     *
     *  @param match <code> true </code> for a positive match, <code>
     *         false </code> for a negative match
     */

    @OSID @Override
    public void matchOff(boolean match) {
        getAssembler().addBooleanTerm(getOffColumn(), match);
        return;
    }


    /**
     *  Clears the off query terms. 
     */

    @OSID @Override
    public void clearOffTerms() {
        getAssembler().clearTerms(getOffColumn());
        return;
    }


    /**
     *  Gets the off query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOffTerms() {
        return (getAssembler().getBooleanTerms(getOffColumn()));
    }


    /**
     *  Orders the results by off status, 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOff(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOffColumn(), style);
        return;
    }


    /**
     *  Gets the Off column name.
     *
     * @return the column name
     */

    protected String getOffColumn() {
        return ("off");
    }


    /**
     *  Matches variable percentages between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVariablePercentage(java.math.BigDecimal start, 
                                        java.math.BigDecimal end, 
                                        boolean match) {
        getAssembler().addDecimalRangeTerm(getVariablePercentageColumn(), start, end, match);
        return;
    }


    /**
     *  Matches any variable percentages. 
     *
     *  @param  match <code> true </code> to match settings with variable 
     *          percentages, <code> false </code> to match settings with no 
     *          variable percentages 
     */

    @OSID @Override
    public void matchAnyVariablePercentage(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getVariablePercentageColumn(), match);
        return;
    }


    /**
     *  Clears the variable percentages query terms. 
     */

    @OSID @Override
    public void clearVariablePercentageTerms() {
        getAssembler().clearTerms(getVariablePercentageColumn());
        return;
    }


    /**
     *  Gets the variable percentage query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getVariablePercentageTerms() {
        return (getAssembler().getDecimalRangeTerms(getVariablePercentageColumn()));
    }


    /**
     *  Orders the results by variable percentage. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariablePercentage(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVariablePercentageColumn(), style);
        return;
    }


    /**
     *  Gets the VariablePercentage column name.
     *
     * @return the column name
     */

    protected String getVariablePercentageColumn() {
        return ("variable_percentage");
    }


    /**
     *  Matches variable amount between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVariableAmount(java.math.BigDecimal start, 
                                    java.math.BigDecimal end, boolean match) {
        getAssembler().addDecimalRangeTerm(getVariableAmountColumn(), start, end, match);
        return;
    }


    /**
     *  Matches any variable amount. 
     *
     *  @param  match <code> true </code> to match settings with variable 
     *          amounts, <code> false </code> to match settings with no 
     *          variable amounts 
     */

    @OSID @Override
    public void matchAnyVariableAmount(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getVariableAmountColumn(), match);
        return;
    }


    /**
     *  Clears the variable amount query terms. 
     */

    @OSID @Override
    public void clearVariableAmountTerms() {
        getAssembler().clearTerms(getVariableAmountColumn());
        return;
    }


    /**
     *  Gets the variable amount query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getVariableAmountTerms() {
        return (getAssembler().getDecimalRangeTerms(getVariableAmountColumn()));
    }


    /**
     *  Orders the results by variable amount. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariableAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVariableAmountColumn(), style);
        return;
    }


    /**
     *  Gets the VariableAmount column name.
     *
     * @return the column name
     */

    protected String getVariableAmountColumn() {
        return ("variable_amount");
    }


    /**
     *  Sets the state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDiscreetStateId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getDiscreetStateIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears the state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateIdTerms() {
        getAssembler().clearTerms(getDiscreetStateIdColumn());
        return;
    }


    /**
     *  Gets the discreet state <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDiscreetStateIdTerms() {
        return (getAssembler().getIdTerms(getDiscreetStateIdColumn()));
    }


    /**
     *  Orders the results by variable discreet state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDiscreetState(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDiscreetStateColumn(), style);
        return;
    }


    /**
     *  Gets the DiscreetStateId column name.
     *
     * @return the column name
     */

    protected String getDiscreetStateIdColumn() {
        return ("discreet_state_id");
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> State. </code> Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDiscreetStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getDiscreetStateQuery() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateQuery() is false");
    }


    /**
     *  Matches any discreet states. 
     *
     *  @param  match <code> true </code> to match settings with discreet 
     *          states, <code> false </code> to match settings with no 
     *          discreet states 
     */

    @OSID @Override
    public void matchAnyDiscreetState(boolean match) {
        getAssembler().addIdWildcardTerm(getDiscreetStateColumn(), match);
        return;
    }


    /**
     *  Clears the state query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateTerms() {
        getAssembler().clearTerms(getDiscreetStateColumn());
        return;
    }


    /**
     *  Gets the discreet state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getDiscreetStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Tests if a state search order is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the state search order. 
     *
     *  @return the state search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsDiscreetStateSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getDiscreetStateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateSearchOrder() is false");
    }


    /**
     *  Gets the DiscreetState column name.
     *
     * @return the column name
     */

    protected String getDiscreetStateColumn() {
        return ("discreet_state");
    }


    /**
     *  Matches ramp rates between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRampRate(org.osid.calendaring.Duration start, 
                              org.osid.calendaring.Duration end, boolean match) {
        getAssembler().addDurationRangeTerm(getRampRateColumn(), start, end, match);
        return;
    }


    /**
     *  Matches any ramp rate. 
     *
     *  @param  match <code> true </code> to match settings with ramp rates, 
     *          <code> false </code> to match settings with no ramp rates 
     */

    @OSID @Override
    public void matchAnyRampRate(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getRampRateColumn(), match);
        return;
    }


    /**
     *  Clears the ramp rate query terms. 
     */

    @OSID @Override
    public void clearRampRateTerms() {
        getAssembler().clearTerms(getRampRateColumn());
        return;
    }


    /**
     *  Gets the ramp rate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getRampRateTerms() {
        return (getAssembler().getDurationRangeTerms(getRampRateColumn()));
    }


    /**
     *  Orders the results by ramp rate. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRampRate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRampRateColumn(), style);
        return;
    }


    /**
     *  Gets the RampRate column name.
     *
     * @return the column name
     */

    protected String getRampRateColumn() {
        return ("ramp_rate");
    }


    /**
     *  Sets the system <code> Id </code> for this query to match controllers 
     *  assigned to systems. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this setting supports the given record
     *  <code>Type</code>.
     *
     *  @param  settingRecordType a setting record type 
     *  @return <code>true</code> if the settingRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type settingRecordType) {
        for (org.osid.control.records.SettingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(settingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  settingRecordType the setting record type 
     *  @return the setting query record 
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(settingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SettingQueryRecord getSettingQueryRecord(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SettingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(settingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(settingRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  settingRecordType the setting record type 
     *  @return the setting query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(settingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SettingQueryInspectorRecord getSettingQueryInspectorRecord(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SettingQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(settingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(settingRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param settingRecordType the setting record type
     *  @return the setting search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(settingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SettingSearchOrderRecord getSettingSearchOrderRecord(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SettingSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(settingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(settingRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this setting. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param settingQueryRecord the setting query record
     *  @param settingQueryInspectorRecord the setting query inspector
     *         record
     *  @param settingSearchOrderRecord the setting search order record
     *  @param settingRecordType setting record type
     *  @throws org.osid.NullArgumentException
     *          <code>settingQueryRecord</code>,
     *          <code>settingQueryInspectorRecord</code>,
     *          <code>settingSearchOrderRecord</code> or
     *          <code>settingRecordTypesetting</code> is
     *          <code>null</code>
     */
            
    protected void addSettingRecords(org.osid.control.records.SettingQueryRecord settingQueryRecord, 
                                      org.osid.control.records.SettingQueryInspectorRecord settingQueryInspectorRecord, 
                                      org.osid.control.records.SettingSearchOrderRecord settingSearchOrderRecord, 
                                      org.osid.type.Type settingRecordType) {

        addRecordType(settingRecordType);

        nullarg(settingQueryRecord, "setting query record");
        nullarg(settingQueryInspectorRecord, "setting query inspector record");
        nullarg(settingSearchOrderRecord, "setting search odrer record");

        this.queryRecords.add(settingQueryRecord);
        this.queryInspectorRecords.add(settingQueryInspectorRecord);
        this.searchOrderRecords.add(settingSearchOrderRecord);
        
        return;
    }
}
