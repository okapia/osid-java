//
// AbstractProgramOffering.java
//
//     Defines a ProgramOffering builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.programoffering.spi;


/**
 *  Defines a <code>ProgramOffering</code> builder.
 */

public abstract class AbstractProgramOfferingBuilder<T extends AbstractProgramOfferingBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.program.programoffering.ProgramOfferingMiter programOffering;


    /**
     *  Constructs a new <code>AbstractProgramOfferingBuilder</code>.
     *
     *  @param programOffering the program offering to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProgramOfferingBuilder(net.okapia.osid.jamocha.builder.course.program.programoffering.ProgramOfferingMiter programOffering) {
        super(programOffering);
        this.programOffering = programOffering;
        return;
    }


    /**
     *  Builds the program offering.
     *
     *  @return the new program offering
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.program.ProgramOffering build() {
        (new net.okapia.osid.jamocha.builder.validator.course.program.programoffering.ProgramOfferingValidator(getValidations())).validate(this.programOffering);
        return (new net.okapia.osid.jamocha.builder.course.program.programoffering.ImmutableProgramOffering(this.programOffering));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the program offering miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.program.programoffering.ProgramOfferingMiter getMiter() {
        return (this.programOffering);
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    public T program(org.osid.course.program.Program program) {
        getMiter().setProgram(program);
        return (self());
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public T term(org.osid.course.Term term) {
        getMiter().setTerm(term);
        return (self());
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public T title(org.osid.locale.DisplayText title) {
        getMiter().setTitle(title);
        return (self());
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T number(String number) {
        getMiter().setNumber(number);
        return (self());
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public T sponsor(org.osid.resource.Resource sponsor) {
        getMiter().addSponsor(sponsor);
        return (self());
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public T sponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        getMiter().setSponsors(sponsors);
        return (self());
    }


    /**
     *  Sets the completion requirements info.
     *
     *  @param info a completion requirements info
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    public T completionRequirementsInfo(org.osid.locale.DisplayText info) {
        getMiter().setCompletionRequirementsInfo(info);
        return (self());
    }


    /**
     *  Adds a completion requirement.
     *
     *  @param requirement a completion requirement
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public T completionRequirement(org.osid.course.requisite.Requisite requirement) {
        getMiter().addCompletionRequirement(requirement);
        return (self());
    }


    /**
     *  Sets all the completion requirements.
     *
     *  @param requirements a collection of completion requirements
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public T completionRequirements(java.util.Collection<org.osid.course.requisite.Requisite> requirements) {
        getMiter().setCompletionRequirements(requirements);
        return (self());
    }


    /**
     *  Adds a credential.
     *
     *  @param credential a credential
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credential</code>
     *          is <code>null</code>
     */

    public T credential(org.osid.course.program.Credential credential) {
        getMiter().addCredential(credential);
        return (self());
    }


    /**
     *  Sets all the credentials.
     *
     *  @param credentials a collection of credentials
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>credentials</code> is <code>null</code>
     */

    public T credentials(java.util.Collection<org.osid.course.program.Credential> credentials) {
        getMiter().setCredentials(credentials);
        return (self());
    }


    /**
     *  Marks this offering to require registration.
     *
     *  @return the builder
     */

    public T requireRegistration() {
        getMiter().setRequiresRegistration(true);
        return (self());
    }


    /**
     *  Marks this offering to not require registration.
     *
     *  @return the builder
     */

    public T notRequireRegistration() {
        getMiter().setRequiresRegistration(false);
        return (self());
    }


    /**
     *  Sets the minimum seats.
     *
     *  @param seats the minimum seats
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    public T minimumSeats(long seats) {
        getMiter().setMinimumSeats(seats);
        return (self());
    }


    /**
     *  Sets the maximum seats.
     *
     *  @param seats the maximum seats
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    public T maximumSeats(long seats) {
        getMiter().setMaximumSeats(seats);
        return (self());
    }


    /**
     *  Sets the URL.
     *
     *  @param url a URL
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public T url(String url) {
        getMiter().setURL(url);
        return (self());
    }


    /**
     *  Adds a ProgramOffering record.
     *
     *  @param record a program offering record
     *  @param recordType the type of program offering record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.program.records.ProgramOfferingRecord record, org.osid.type.Type recordType) {
        getMiter().addProgramOfferingRecord(record, recordType);
        return (self());
    }
}       


