//
// ShallowCommunicationProxyManager.java
//
//     A nothing CommunicationProxyManager adapter.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.communication;


/**
 *  A nothing CommunicationProxyManager adapter.
 */

public final class ShallowAdapterCommunicationProxyManager
    extends net.okapia.osid.jamocha.adapter.communication.spi.AbstractAdapterCommunicationProxyManager
    implements org.osid.communication.CommunicationProxyManager {


    /**
     *  Constructs a new {@code ShallowAdapterCommunicationProxyManager} using
     *  the underlying provider.
     *
     *  @param manager the manager to adapt
     *  @throws org.osid.NullArgumentException {@code manager} is
     *          {@code null}
     */

    public ShallowAdapterCommunicationProxyManager(org.osid.communication.CommunicationProxyManager manager) {
        setAdapteeManager(manager);
        return;
    }


    /**
     *  Constructs a new {@code ShallowAdapterCommunicationProxyManager}.
     *
     *  @param manager the manager to adapt
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code manager} or
     *          {@code provider} is {@code null}
     */

    public ShallowAdapterCommunicationProxyManager(org.osid.communication.CommunicationProxyManager manager,
                                               net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        setAdapteeManager(manager);
        return;
    }
}
