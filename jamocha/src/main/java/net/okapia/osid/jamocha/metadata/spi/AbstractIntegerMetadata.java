//
// AbstractIntegerMetadata.java
//
//     Defines an integer Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an integer Metadata.
 */

public abstract class AbstractIntegerMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private long minimum = Long.MIN_VALUE;
    private long maximum = Long.MAX_VALUE;

    private final java.util.Collection<Long> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<Long> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<Long> existing = new java.util.LinkedHashSet<>();    


    /**
     *  Constructs a new {@code AbstractIntegerMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractIntegerMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.INTEGER, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractIntegerMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractIntegerMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.INTEGER, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the minimum integer value. 
     *
     *  @return the minimum integer 
     *  @throws org.osid.IllegalStateException syntax is not a <code> INTEGER 
     *          </code> 
     */

    @OSID @Override
    public long getMinimumInteger() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum integer value. 
     *
     *  @return the maximum integer 
     *  @throws org.osid.IllegalStateException syntax is not a <code> INTEGER 
     *          </code> 
     */

    @OSID @Override
    public long getMaximumInteger() {
        return (this.maximum);
    }

    
    /**
     *  Sets the integer range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max}
     */

    protected void setIntegerRange(long min, long max) {
        if (min > max) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.minimum = min;
        this.maximum = max;

        return;
    }


    /**
     *  Gets the set of acceptable integer values. 
     *
     *  @return a set of integers or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          INTEGER </code>
     */

    @OSID @Override
    public long[] getIntegerSet() {
        Long[] longs = this.set.toArray(new Long[this.set.size()]);
        long[] ret = new long[longs.length];
        for (int i = 0; i < longs.length; i++) {
            ret[i] = longs[i];
        }

        return (ret);
    }

    
    /**
     *  Sets the integer set.
     *
     *  @param values a collection of accepted integer values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setIntegerSet(java.util.Collection<Long> values) {
        this.set.clear();
        addToIntegerSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the integer set.
     *
     *  @param values a collection of accepted integer values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToIntegerSet(java.util.Collection<Long> values) {
        nullarg(values, "integer set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the integer set.
     *
     *  @param value an integer value
     */

    protected void addToIntegerSet(long value) {
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the integer set.
     *
     *  @param value an integer value
     */

    protected void removeFromIntegerSet(long value) {
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the integer set.
     */

    protected void clearIntegerSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default integer values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default integer values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          INTEGER </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public long[] getDefaultIntegerValues() {
        Long[] longs = this.set.toArray(new Long[this.defvals.size()]);
        long[] ret = new long[longs.length];
        for (int i = 0; i < longs.length; i++) {
            ret[i] = longs[i];
        }

        return (ret);
    }


    /**
     *  Values the default integer values.
     *
     *  @param values a collection of default integer values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultIntegerValues(java.util.Collection<Long> values) {
        clearDefaultIntegerValues();
        addDefaultIntegerValues(values);
        return;
    }

    
    /**
     *  Adds a collection of default integer values.
     *
     *  @param values a collection of default integer values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultIntegerValues(java.util.Collection<Long> values) {
        nullarg(values, "default integer values");
        
        for (long value : values) {
            addDefaultIntegerValue(value);
        }

        return;
    }


    /**
     *  Adds a default integer value.
     *
     *  @param value a integer value
     */

    protected void addDefaultIntegerValue(long value) {
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default integer value.
     *
     *  @param value a integer value
     */

    protected void removeFromIntegerValues(long value) {
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default integer values.
     */

    protected void clearDefaultIntegerValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing integer values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing integer values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> INTEGER 
     *          </code> or <code> isValueKnown() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public long[] getExistingIntegerValues() {
        Long[] longs = this.existing.toArray(new Long[this.existing.size()]);
        long[] ret = new long[longs.length];
        for (int i = 0; i < longs.length; i++) {
            ret[i] = longs[i];
        }

        return (ret);
    }


    /**
     *  Valuess the existing integer values.
     *
     *  @param values a collection of existing integer values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingIntegerValues(java.util.Collection<Long> values) {
        clearExistingIntegerValues();
        addExistingIntegerValues(values);
        return;
    }


    /**
     *  Adds a collection of existing integer values.
     *
     *  @param values a collection of existing integer values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingIntegerValues(java.util.Collection<Long> values) {
        nullarg(values, "existing integer values");
        
        for (long value : values) {
            addExistingIntegerValue(value);
        }

        return;
    }


    /**
     *  Adds a existing integer value.
     *
     *  @param value a integer value
     */

    protected void addExistingIntegerValue(long value) {
        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing integer value.
     *
     *  @param value a integer value
     */

    protected void removeExistingIntegerValue(long value) {
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing integer values.
     */

    protected void clearExistingIntegerValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }    
}