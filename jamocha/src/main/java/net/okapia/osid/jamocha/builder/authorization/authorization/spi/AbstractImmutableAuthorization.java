//
// AbstractImmutableAuthorization.java
//
//     Wraps a mutable Authorization to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.authorization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Authorization</code> to hide modifiers. This
 *  wrapper provides an immutized Authorization from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying authorization whose state changes are visible.
 */

public abstract class AbstractImmutableAuthorization
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.authorization.Authorization {

    private final org.osid.authorization.Authorization authorization;


    /**
     *  Constructs a new <code>AbstractImmutableAuthorization</code>.
     *
     *  @param authorization the authorization to immutablize
     *  @throws org.osid.NullArgumentException <code>authorization</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAuthorization(org.osid.authorization.Authorization authorization) {
        super(authorization);
        this.authorization = authorization;
        return;
    }


    /**
     *  Tests if this authorization is implicit. 
     *
     *  @return <code> true </code> if this authorization is implicit, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.authorization.isImplicit());
    }


    /**
     *  Tests if this authorization has a <code> Resource. </code> 
     *
     *  @return <code> true </code> if this authorization has a <code> 
     *          Resource, </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasResource() {
        return (this.authorization.hasResource());
    }


    /**
     *  Gets the <code> resource Id </code> for this authorization. 
     *
     *  @return the <code> Resource Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.authorization.getResourceId());
    }


    /**
     *  Gets the <code> Resource </code> for this authorization. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.authorization.getResource());
    }


    /**
     *  Tests if this authorization has a <code> Trust. </code> 
     *
     *  @return <code> true </code> if this authorization has a <code> Trust, 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasTrust() {
        return (this.authorization.hasTrust());
    }


    /**
     *  Gets the <code> Trust </code> <code> Id </code> for this 
     *  authorization. 
     *
     *  @return the trust <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasTrust() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTrustId() {
        return (this.authorization.getTrustId());
    }


    /**
     *  Gets the <code> Trust </code> for this authorization. 
     *
     *  @return the <code> Trust </code> 
     *  @throws org.osid.IllegalStateException <code> hasTrust() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.process.Trust getTrust()
        throws org.osid.OperationFailedException {

        return (this.authorization.getTrust());
    }


    /**
     *  Tests if this authorization has an <code> Agent. </code> An implied 
     *  authorization may have an <code> Agent </code> in addition to a 
     *  specified <code> Resource. </code> 
     *
     *  @return <code> true </code> if this authorization has an <code> Agent, 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAgent() {
        return (this.authorization.hasAgent());
    }


    /**
     *  Gets the <code> Agent Id </code> for this authorization. 
     *
     *  @return the <code> Agent Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.authorization.getAgentId());
    }


    /**
     *  Gets the <code> Agent </code> for this authorization. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.authorization.getAgent());
    }


    /**
     *  Gets the <code> Function Id </code> for this authorization. 
     *
     *  @return the function <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFunctionId() {
        return (this.authorization.getFunctionId());
    }


    /**
     *  Gets the <code> Function </code> for this authorization. 
     *
     *  @return the function 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authorization.Function getFunction()
        throws org.osid.OperationFailedException {

        return (this.authorization.getFunction());
    }


    /**
     *  Gets the <code> Qualifier Id </code> for this authorization. 
     *
     *  @return the qualifier <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQualifierId() {
        return (this.authorization.getQualifierId());
    }


    /**
     *  Gets the qualifier for this authorization. 
     *
     *  @return the qualifier 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authorization.Qualifier getQualifier()
        throws org.osid.OperationFailedException {

        return (this.authorization.getQualifier());
    }


    /**
     *  Gets the authorization record corresponding to the given <code> 
     *  Authorization </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  authorizationRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(authorizationRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  authorizationRecordType the type of the record to retrieve 
     *  @return the authorization record 
     *  @throws org.osid.NullArgumentException <code> authorizationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(authorizationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.records.AuthorizationRecord getAuthorizationRecord(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException {

        return (this.authorization.getAuthorizationRecord(authorizationRecordType));
    }
}

