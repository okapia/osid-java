//
// AbstractChecklistQuery.java
//
//     A template for making a Checklist Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for checklists.
 */

public abstract class AbstractChecklistQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.checklist.ChecklistQuery {

    private final java.util.Collection<org.osid.checklist.records.ChecklistQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the todo <code> Id </code> for this query to match todos assigned 
     *  to checklists. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTodoId(org.osid.id.Id todoId, boolean match) {
        return;
    }


    /**
     *  Clears the todo <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTodoIdTerms() {
        return;
    }


    /**
     *  Tests if a todo query is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo. 
     *
     *  @return the todo query 
     *  @throws org.osid.UnimplementedException <code> supportsTodoQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getTodoQuery() {
        throw new org.osid.UnimplementedException("supportsTodoQuery() is false");
    }


    /**
     *  Matches checklists with any todo. 
     *
     *  @param  match <code> true </code> to match checklists with any todo, 
     *          <code> false </code> to match checklists with no todos 
     */

    @OSID @Override
    public void matchAnyTodo(boolean match) {
        return;
    }


    /**
     *  Clears the todo terms. 
     */

    @OSID @Override
    public void clearTodoTerms() {
        return;
    }


    /**
     *  Sets the checklist <code> Id </code> for this query to match 
     *  checklists that have the specified checklist as an ancestor. 
     *
     *  @param  checklistId a checklist <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, a <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorChecklistId(org.osid.id.Id checklistId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the ancestor checklist <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorChecklistIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ChecklistQuery </code> is available. 
     *
     *  @return <code> true </code> if a checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorChecklistQuery() {
        return (false);
    }


    /**
     *  Gets the query for a checklist. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorChecklistQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuery getAncestorChecklistQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorChecklistQuery() is false");
    }


    /**
     *  Matches checklists with any ancestor. 
     *
     *  @param  match <code> true </code> to match checklists with any 
     *          ancestor, <code> false </code> to match root checklists 
     */

    @OSID @Override
    public void matchAnyAncestorChecklist(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor checklist terms. 
     */

    @OSID @Override
    public void clearAncestorChecklistTerms() {
        return;
    }


    /**
     *  Sets the checklist <code> Id </code> for this query to match 
     *  checklists that have the specified checklist as a descendant. 
     *
     *  @param  checklistId a checklist <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantChecklistId(org.osid.id.Id checklistId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the descendant checklist <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantChecklistIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ChecklistQuery </code> is available. 
     *
     *  @return <code> true </code> if a checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantChecklistQuery() {
        return (false);
    }


    /**
     *  Gets the query for a checklist. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantChecklistQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuery getDescendantChecklistQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantChecklistQuery() is false");
    }


    /**
     *  Matches checklists with any descendant. 
     *
     *  @param  match <code> true </code> to match checklists with any 
     *          descendant, <code> false </code> to match leaf checklists 
     */

    @OSID @Override
    public void matchAnyDescendantChecklist(boolean match) {
        return;
    }


    /**
     *  Clears the descendant checklist terms. 
     */

    @OSID @Override
    public void clearDescendantChecklistTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given checklist query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a checklist implementing the requested record.
     *
     *  @param checklistRecordType a checklist record type
     *  @return the checklist query record
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checklistRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.ChecklistQueryRecord getChecklistQueryRecord(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.ChecklistQueryRecord record : this.records) {
            if (record.implementsRecordType(checklistRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checklistRecordType + " is not supported");
    }


    /**
     *  Adds a record to this checklist query. 
     *
     *  @param checklistQueryRecord checklist query record
     *  @param checklistRecordType checklist record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addChecklistQueryRecord(org.osid.checklist.records.ChecklistQueryRecord checklistQueryRecord, 
                                          org.osid.type.Type checklistRecordType) {

        addRecordType(checklistRecordType);
        nullarg(checklistQueryRecord, "checklist query record");
        this.records.add(checklistQueryRecord);        
        return;
    }
}
