//
// AbstractSchedule.java
//
//     Defines a Schedule.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.schedule.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Schedule</code>.
 */

public abstract class AbstractSchedule
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.calendaring.Schedule {

    private org.osid.calendaring.ScheduleSlot scheduleSlot;
    private org.osid.calendaring.TimePeriod timePeriod;
    private org.osid.calendaring.DateTime scheduleStart;
    private org.osid.calendaring.DateTime scheduleEnd;
    private long limit = -1;
    private org.osid.calendaring.Duration duration;
    private org.osid.locale.DisplayText locationDescription;
    private org.osid.mapping.Location location;

    private final java.util.Collection<org.osid.calendaring.records.ScheduleRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the schedule slot. 
     *
     *  @return the schedule slot <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScheduleSlotId() {
        return (this.scheduleSlot.getId());
    }


    /**
     *  Gets the schedule slot included inside this one. 
     *
     *  @return the schedule slot 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlot getScheduleSlot()
        throws org.osid.OperationFailedException {

        return (this.scheduleSlot);
    }


    /**
     *  Sets the schedule slot.
     *
     *  @param slot a schedule slot
     *  @throws org.osid.NullArgumentException <code>slot</code> is
     *          <code>null</code>
     */

    protected void setScheduleSlot(org.osid.calendaring.ScheduleSlot slot) {
        nullarg(slot, "schedule slot");
        this.scheduleSlot = slot;
        return;
    }


    /**
     *  Tests if a <code> TimePeriod </code> is associated with this schedule. 
     *  The time period determines the start and end time of the recurring 
     *  series. 
     *
     *  @return <code> true </code> if there is an associated <code> 
     *          TimePeriod, </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasTimePeriod() {
        return (this.timePeriod != null);
    }


    /**
     *  Gets the <code> TimePeriod Id </code> for this recurring event. A 
     *  <code> Schedule </code> with an associated <code> TimePeriod </code> 
     *  overrides any start or end date set. 
     *
     *  @return the time period <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimePeriodId() {
        if (!hasTimePeriod()) {
            throw new org.osid.IllegalStateException("hasTimePeriod() is false");
        }

        return (this.timePeriod.getId());
    }


    /**
     *  Gets the <code> TimePeriod </code> for this recurring event. A <code> 
     *  Schedule </code> with an associated <code> TimePeriod </code> 
     *  overrides any start or end date set. 
     *
     *  @return the time period 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod()
        throws org.osid.OperationFailedException {

        if (!hasTimePeriod()) {
            throw new org.osid.IllegalStateException("hasTimePeriod() is false");
        }

        return (this.timePeriod);
    }


    /**
     *  Sets the time period.
     *
     *  @param period a time period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    protected void setTimePeriod(org.osid.calendaring.TimePeriod period) {
        nullarg(period, "time period");
        this.timePeriod = period;
        return;
    }


    /**
     *  Gets the start date of this schedule. If <code>
     *  hasTimePeriod() </code> is <code> true, </code> the start date
     *  is the start date of the associated <code> TimePeriod. </code>
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getScheduleStart() {
        return (this.scheduleStart);
    }


    /**
     *  Sets the schedule start.
     *
     *  @param start a schedule start
     *  @throws org.osid.NullArgumentException <code>start</code> is
     *          <code>null</code>
     */

    protected void setScheduleStart(org.osid.calendaring.DateTime start) {
        nullarg(start, "schedule start");
        this.scheduleStart = start;
        return;
    }


    /**
     *  Gets the end date of this schedule. If <code> hasTimePeriod()
     *  </code> is <code> true, </code> the end date is the end date
     *  of the associated <code> TimePeriod. </code>
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getScheduleEnd() {
        return (this.scheduleEnd);
    }


    /**
     *  Sets the schedule end.
     *
     *  @param end a schedule end
     *  @throws org.osid.NullArgumentException <code>end</code> is
     *          <code>null</code>
     */

    protected void setScheduleEnd(org.osid.calendaring.DateTime end) {
        nullarg(end, "schedule end");
        this.scheduleEnd = end;
        return;
    }


    /**
     *  Tests if this schedule has a limit on the number of occurrences. 
     *
     *  @return <code> true </code> if there is a limit <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean hasLimit() {
        return (this.limit > 0);
    }


    /**
     *  Gets the limit of the number of occurences of this schedule. 
     *
     *  @return the limit 
     *  @throws org.osid.IllegalStateException <code> hasLimitl() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getLimit() {
        if (!hasLimit()) {
            throw new org.osid.IllegalStateException("hasLimit() is false");
        }

        return (this.limit);
    }


    /**
     *  Sets the limit.
     *
     *  @param limit a limit
     *  @throws org.osid.InvalieArgumentException <code>limit</code>
     *          is negative
     */

    protected void setLimit(long limit) {
        cardinalarg(limit, "limit");
        this.limit = limit;
        return;
    }


    /**
     *  Gets the total duration of the schedule. 
     *
     *  @return the total duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalDuration() {
        return (this.duration);
    }


    /**
     *  Sets the total duration.
     *
     *  @param duration the total duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setTotalDuration(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.duration = duration;
        return;
    }


    /**
     *  Gets a descriptive location. 
     *
     *  @return the location 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.locationDescription);
    }


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    protected void setLocationDescription(org.osid.locale.DisplayText description) {
        nullarg(description, "location description");
        this.locationDescription = description;
        return;
    }


    /**
     *  Tests if a location is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated location, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.location != null);
    }


    /**
     *  Gets the location <code> Id </code>. 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location.getId());
    }


    /**
     *  Gets the <code> Location. </code> 
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location);
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    protected void setLocation(org.osid.mapping.Location location) {
        nullarg(location, "location");
        this.location = location;
        return;
    }

    /**
     *  Tests if this schedule supports the given record
     *  <code>Type</code>.
     *
     *  @param  scheduleRecordType a schedule record type 
     *  @return <code>true</code> if the scheduleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type scheduleRecordType) {
        for (org.osid.calendaring.records.ScheduleRecord record : this.records) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Schedule</code> record <code>Type</code>.
     *
     *  @param  scheduleRecordType the schedule record type 
     *  @return the schedule record 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleRecord getScheduleRecord(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleRecord record : this.records) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this schedule. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param scheduleRecord the schedule record
     *  @param scheduleRecordType schedule record type
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecord</code> or
     *          <code>scheduleRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addScheduleRecord(org.osid.calendaring.records.ScheduleRecord scheduleRecord, 
                                     org.osid.type.Type scheduleRecordType) {

        nullarg(scheduleRecord, "schedule record");
        addRecordType(scheduleRecordType);
        this.records.add(scheduleRecord);
        
        return;
    }
}
