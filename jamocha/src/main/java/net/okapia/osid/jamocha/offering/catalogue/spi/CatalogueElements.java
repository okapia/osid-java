//
// CatalogueElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.catalogue.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CatalogueElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the CatalogueElement Id.
     *
     *  @return the catalogue element Id
     */

    public static org.osid.id.Id getCatalogueEntityId() {
        return (makeEntityId("osid.offering.Catalogue"));
    }


    /**
     *  Gets the CanonicalUnitId element Id.
     *
     *  @return the CanonicalUnitId element Id
     */

    public static org.osid.id.Id getCanonicalUnitId() {
        return (makeQueryElementId("osid.offering.catalogue.CanonicalUnitId"));
    }


    /**
     *  Gets the CanonicalUnit element Id.
     *
     *  @return the CanonicalUnit element Id
     */

    public static org.osid.id.Id getCanonicalUnit() {
        return (makeQueryElementId("osid.offering.catalogue.CanonicalUnit"));
    }


    /**
     *  Gets the OfferingId element Id.
     *
     *  @return the OfferingId element Id
     */

    public static org.osid.id.Id getOfferingId() {
        return (makeQueryElementId("osid.offering.catalogue.OfferingId"));
    }


    /**
     *  Gets the Offering element Id.
     *
     *  @return the Offering element Id
     */

    public static org.osid.id.Id getOffering() {
        return (makeQueryElementId("osid.offering.catalogue.Offering"));
    }


    /**
     *  Gets the ParticipantId element Id.
     *
     *  @return the ParticipantId element Id
     */

    public static org.osid.id.Id getParticipantId() {
        return (makeQueryElementId("osid.offering.catalogue.ParticipantId"));
    }


    /**
     *  Gets the Participant element Id.
     *
     *  @return the Participant element Id
     */

    public static org.osid.id.Id getParticipant() {
        return (makeQueryElementId("osid.offering.catalogue.Participant"));
    }


    /**
     *  Gets the ResultId element Id.
     *
     *  @return the ResultId element Id
     */

    public static org.osid.id.Id getResultId() {
        return (makeQueryElementId("osid.offering.catalogue.ResultId"));
    }


    /**
     *  Gets the Result element Id.
     *
     *  @return the Result element Id
     */

    public static org.osid.id.Id getResult() {
        return (makeQueryElementId("osid.offering.catalogue.Result"));
    }


    /**
     *  Gets the AncestorCatalogueId element Id.
     *
     *  @return the AncestorCatalogueId element Id
     */

    public static org.osid.id.Id getAncestorCatalogueId() {
        return (makeQueryElementId("osid.offering.catalogue.AncestorCatalogueId"));
    }


    /**
     *  Gets the AncestorCatalogue element Id.
     *
     *  @return the AncestorCatalogue element Id
     */

    public static org.osid.id.Id getAncestorCatalogue() {
        return (makeQueryElementId("osid.offering.catalogue.AncestorCatalogue"));
    }


    /**
     *  Gets the DescendantCatalogueId element Id.
     *
     *  @return the DescendantCatalogueId element Id
     */

    public static org.osid.id.Id getDescendantCatalogueId() {
        return (makeQueryElementId("osid.offering.catalogue.DescendantCatalogueId"));
    }


    /**
     *  Gets the DescendantCatalogue element Id.
     *
     *  @return the DescendantCatalogue element Id
     */

    public static org.osid.id.Id getDescendantCatalogue() {
        return (makeQueryElementId("osid.offering.catalogue.DescendantCatalogue"));
    }
}
