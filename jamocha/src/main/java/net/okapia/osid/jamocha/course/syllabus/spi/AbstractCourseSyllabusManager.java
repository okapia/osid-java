//
// AbstractCourseSyllabusManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCourseSyllabusManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.course.syllabus.CourseSyllabusManager,
               org.osid.course.syllabus.CourseSyllabusProxyManager {

    private final Types syllabusRecordTypes                = new TypeRefSet();
    private final Types syllabusSearchRecordTypes          = new TypeRefSet();

    private final Types moduleRecordTypes                  = new TypeRefSet();
    private final Types moduleSearchRecordTypes            = new TypeRefSet();

    private final Types docetRecordTypes                   = new TypeRefSet();
    private final Types docetSearchRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCourseSyllabusManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCourseSyllabusManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any docet federation is exposed. Federation is exposed when a 
     *  specific docet may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of 
     *  docetsappears as a single docet. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a syllabus lookup service. 
     *
     *  @return <code> true </code> if syllabus lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusLookup() {
        return (false);
    }


    /**
     *  Tests if querying syllabi is available. 
     *
     *  @return <code> true </code> if syllabus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusQuery() {
        return (false);
    }


    /**
     *  Tests if searching for syllabi is available. 
     *
     *  @return <code> true </code> if syllabus search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusSearch() {
        return (false);
    }


    /**
     *  Tests if searching for syllabi is available. 
     *
     *  @return <code> true </code> if syllabus search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusAdmin() {
        return (false);
    }


    /**
     *  Tests if syllabus notification is available. 
     *
     *  @return <code> true </code> if syllabus notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusNotification() {
        return (false);
    }


    /**
     *  Tests if a syllabus to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if syllabus course catalog lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a syllabus to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if syllabus course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a syllabus smart course catalog session is available. 
     *
     *  @return <code> true </code> if syllabus smart course catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests for the availability of a module service for getting available 
     *  modules for a resource. 
     *
     *  @return <code> true </code> if module is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModule() {
        return (false);
    }


    /**
     *  Tests for the availability of a module lookup service. 
     *
     *  @return <code> true </code> if module lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleLookup() {
        return (false);
    }


    /**
     *  Tests if querying modules is available. 
     *
     *  @return <code> true </code> if module query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleQuery() {
        return (false);
    }


    /**
     *  Tests if searching for modules is available. 
     *
     *  @return <code> true </code> if module search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleSearch() {
        return (false);
    }


    /**
     *  Tests if searching for modules is available. 
     *
     *  @return <code> true </code> if module search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleAdmin() {
        return (false);
    }


    /**
     *  Tests if module notification is available. 
     *
     *  @return <code> true </code> if module notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleNotification() {
        return (false);
    }


    /**
     *  Tests if a module to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if module course catalog lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a module to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if module course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a module smart course catalog session is available. 
     *
     *  @return <code> true </code> if module smart course catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests for the availability of a docet lookup service. 
     *
     *  @return <code> true </code> if docet lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetLookup() {
        return (false);
    }


    /**
     *  Tests if querying docetsis available. 
     *
     *  @return <code> true </code> if docet query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetQuery() {
        return (false);
    }


    /**
     *  Tests if searching for docets is available. 
     *
     *  @return <code> true </code> if docet search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a docet administrative service for 
     *  creating and deleting docets. 
     *
     *  @return <code> true </code> if docet administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a docet notification service. 
     *
     *  @return <code> true </code> if docet notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetNotification() {
        return (false);
    }


    /**
     *  Tests if a docet to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if docet course catalog lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a docet to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if docet course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a docet smart course catalog session is available. 
     *
     *  @return <code> true </code> if docet smart course catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course syllabus batch service is available. 
     *
     *  @return <code> true </code> if a course syllabus batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSyllabusBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Syllabus </code> record types. 
     *
     *  @return a list containing the supported syllabus record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSyllabusRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.syllabusRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Syllabus </code> record type is supported. 
     *
     *  @param  syllabusRecordType a <code> Type </code> indicating a <code> 
     *          Syllabus </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> syllabusRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSyllabusRecordType(org.osid.type.Type syllabusRecordType) {
        return (this.syllabusRecordTypes.contains(syllabusRecordType));
    }


    /**
     *  Adds support for a syllabus record type.
     *
     *  @param syllabusRecordType a syllabus record type
     *  @throws org.osid.NullArgumentException
     *  <code>syllabusRecordType</code> is <code>null</code>
     */

    protected void addSyllabusRecordType(org.osid.type.Type syllabusRecordType) {
        this.syllabusRecordTypes.add(syllabusRecordType);
        return;
    }


    /**
     *  Removes support for a syllabus record type.
     *
     *  @param syllabusRecordType a syllabus record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>syllabusRecordType</code> is <code>null</code>
     */

    protected void removeSyllabusRecordType(org.osid.type.Type syllabusRecordType) {
        this.syllabusRecordTypes.remove(syllabusRecordType);
        return;
    }


    /**
     *  Gets the supported syllabus search record types. 
     *
     *  @return a list containing the supported syllabus search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSyllabusSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.syllabusSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given syllabus search record type is supported. 
     *
     *  @param  syllabusSearchRecordType a <code> Type </code> indicating a 
     *          syllabus record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> syllabusSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSyllabusSearchRecordType(org.osid.type.Type syllabusSearchRecordType) {
        return (this.syllabusSearchRecordTypes.contains(syllabusSearchRecordType));
    }


    /**
     *  Adds support for a syllabus search record type.
     *
     *  @param syllabusSearchRecordType a syllabus search record type
     *  @throws org.osid.NullArgumentException
     *  <code>syllabusSearchRecordType</code> is <code>null</code>
     */

    protected void addSyllabusSearchRecordType(org.osid.type.Type syllabusSearchRecordType) {
        this.syllabusSearchRecordTypes.add(syllabusSearchRecordType);
        return;
    }


    /**
     *  Removes support for a syllabus search record type.
     *
     *  @param syllabusSearchRecordType a syllabus search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>syllabusSearchRecordType</code> is <code>null</code>
     */

    protected void removeSyllabusSearchRecordType(org.osid.type.Type syllabusSearchRecordType) {
        this.syllabusSearchRecordTypes.remove(syllabusSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Module </code> record types. 
     *
     *  @return a list containing the supported module record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getModuleRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.moduleRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Module </code> record type is supported. 
     *
     *  @param  moduleRecordType a <code> Type </code> indicating a <code> 
     *          Module </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> moduleRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsModuleRecordType(org.osid.type.Type moduleRecordType) {
        return (this.moduleRecordTypes.contains(moduleRecordType));
    }


    /**
     *  Adds support for a module record type.
     *
     *  @param moduleRecordType a module record type
     *  @throws org.osid.NullArgumentException
     *  <code>moduleRecordType</code> is <code>null</code>
     */

    protected void addModuleRecordType(org.osid.type.Type moduleRecordType) {
        this.moduleRecordTypes.add(moduleRecordType);
        return;
    }


    /**
     *  Removes support for a module record type.
     *
     *  @param moduleRecordType a module record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>moduleRecordType</code> is <code>null</code>
     */

    protected void removeModuleRecordType(org.osid.type.Type moduleRecordType) {
        this.moduleRecordTypes.remove(moduleRecordType);
        return;
    }


    /**
     *  Gets the supported module search record types. 
     *
     *  @return a list containing the supported module search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getModuleSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.moduleSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given module search record type is supported. 
     *
     *  @param  moduleSearchRecordType a <code> Type </code> indicating a 
     *          module record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> moduleSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsModuleSearchRecordType(org.osid.type.Type moduleSearchRecordType) {
        return (this.moduleSearchRecordTypes.contains(moduleSearchRecordType));
    }


    /**
     *  Adds support for a module search record type.
     *
     *  @param moduleSearchRecordType a module search record type
     *  @throws org.osid.NullArgumentException
     *  <code>moduleSearchRecordType</code> is <code>null</code>
     */

    protected void addModuleSearchRecordType(org.osid.type.Type moduleSearchRecordType) {
        this.moduleSearchRecordTypes.add(moduleSearchRecordType);
        return;
    }


    /**
     *  Removes support for a module search record type.
     *
     *  @param moduleSearchRecordType a module search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>moduleSearchRecordType</code> is <code>null</code>
     */

    protected void removeModuleSearchRecordType(org.osid.type.Type moduleSearchRecordType) {
        this.moduleSearchRecordTypes.remove(moduleSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Docet </code> record types. 
     *
     *  @return a list containing the supported docet record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDocetRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.docetRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Docet </code> record type is supported. 
     *
     *  @param  docetRecordType a <code> Type </code> indicating a <code> 
     *          Docet </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> docetRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDocetRecordType(org.osid.type.Type docetRecordType) {
        return (this.docetRecordTypes.contains(docetRecordType));
    }


    /**
     *  Adds support for a docet record type.
     *
     *  @param docetRecordType a docet record type
     *  @throws org.osid.NullArgumentException
     *  <code>docetRecordType</code> is <code>null</code>
     */

    protected void addDocetRecordType(org.osid.type.Type docetRecordType) {
        this.docetRecordTypes.add(docetRecordType);
        return;
    }


    /**
     *  Removes support for a docet record type.
     *
     *  @param docetRecordType a docet record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>docetRecordType</code> is <code>null</code>
     */

    protected void removeDocetRecordType(org.osid.type.Type docetRecordType) {
        this.docetRecordTypes.remove(docetRecordType);
        return;
    }


    /**
     *  Gets the supported docet search record types. 
     *
     *  @return a list containing the supported docet search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDocetSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.docetSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given docet search record type is supported. 
     *
     *  @param  docetSearchRecordType a <code> Type </code> indicating a docet 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> docetSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDocetSearchRecordType(org.osid.type.Type docetSearchRecordType) {
        return (this.docetSearchRecordTypes.contains(docetSearchRecordType));
    }


    /**
     *  Adds support for a docet search record type.
     *
     *  @param docetSearchRecordType a docet search record type
     *  @throws org.osid.NullArgumentException
     *  <code>docetSearchRecordType</code> is <code>null</code>
     */

    protected void addDocetSearchRecordType(org.osid.type.Type docetSearchRecordType) {
        this.docetSearchRecordTypes.add(docetSearchRecordType);
        return;
    }


    /**
     *  Removes support for a docet search record type.
     *
     *  @param docetSearchRecordType a docet search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>docetSearchRecordType</code> is <code>null</code>
     */

    protected void removeDocetSearchRecordType(org.osid.type.Type docetSearchRecordType) {
        this.docetSearchRecordTypes.remove(docetSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  lookup service. 
     *
     *  @return a <code> SyllabusLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusLookupSession getSyllabusLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusLookupSession getSyllabusLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusLookupSession getSyllabusLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusLookupSession getSyllabusLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus query 
     *  service. 
     *
     *  @return a <code> SyllabusQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuerySession getSyllabusQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuerySession getSyllabusQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuerySession getSyllabusQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuerySession getSyllabusQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  search service. 
     *
     *  @return a <code> SyllabusSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSearchSession getSyllabusSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSearchSession getSyllabusSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSearchSession getSyllabusSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSearchSession getSyllabusSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  administration service. 
     *
     *  @return a <code> SyllabusAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusAdminSession getSyllabusAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusAdminSession getSyllabusAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusAdminSession getSyllabusAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusAdminSession getSyllabusAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  notification service. 
     *
     *  @param  syllabusReceiver the receiver 
     *  @return a <code> SyllabusNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> syllabusReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusNotificationSession getSyllabusNotificationSession(org.osid.course.syllabus.SyllabusReceiver syllabusReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  notification service. 
     *
     *  @param  syllabusReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> syllabusReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusNotificationSession getSyllabusNotificationSession(org.osid.course.syllabus.SyllabusReceiver syllabusReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  notification service for the given course catalog. 
     *
     *  @param  syllabusReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> syllabusReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusNotificationSession getSyllabusNotificationSessionForCourseCatalog(org.osid.course.syllabus.SyllabusReceiver syllabusReceiver, 
                                                                                                               org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  notification service for the given course catalog. 
     *
     *  @param  syllabusReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> syllabusReceiver, 
     *          courseCatalogId, </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusNotificationSession getSyllabusNotificationSessionForCourseCatalog(org.osid.course.syllabus.SyllabusReceiver syllabusReceiver, 
                                                                                                               org.osid.id.Id courseCatalogId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the session for retrieving syllabus to course catalog mappings. 
     *
     *  @return a <code> SyllabusCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusCourseCatalogSession getSyllabusCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for retrieving syllabus to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusCourseCatalogSession getSyllabusCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for assigning syllabus to course catalog mappings. 
     *
     *  @return a <code> SyllabusCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusCourseCatalogAssignmentSession getSyllabusCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning syllabus to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusCourseCatalogAssignmentSession getSyllabusCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the syllabus smart course catalog for 
     *  the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> SyllabusSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSmartCourseCatalogSession getSyllabusSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getSyllabusSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session associated with the syllabus smart course catalog for 
     *  the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> SyllabusSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCtalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSmartCourseCatalogSession getSyllabusSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getSyllabusSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module lookup 
     *  service. 
     *
     *  @return a <code> ModuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleLookupSession getModuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ModuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleLookupSession getModuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleLookupSession getModuleLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ModuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleLookupSession getModuleLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module query 
     *  service. 
     *
     *  @return a <code> ModuleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuerySession getModuleQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ModuleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuerySession getModuleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuerySession getModuleQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ModuleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuerySession getModuleQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module search 
     *  service. 
     *
     *  @return a <code> ModuleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSearchSession getModuleSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ModuleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSearchSession getModuleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSearchSession getModuleSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ModuleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSearchSession getModuleSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  administration service. 
     *
     *  @return a <code> ModuleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleAdminSession getModuleAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ModuleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleAdminSession getModuleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleAdminSession getModuleAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ModuleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleAdminSession getModuleAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  notification service. 
     *
     *  @param  moduleReceiver the receiver 
     *  @return a <code> ModuleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> moduleReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleNotificationSession getModuleNotificationSession(org.osid.course.syllabus.ModuleReceiver moduleReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  notification service. 
     *
     *  @param  moduleReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ModuleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> moduleReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleNotificationSession getModuleNotificationSession(org.osid.course.syllabus.ModuleReceiver moduleReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  notification service for the given course catalog. 
     *
     *  @param  moduleReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> moduleReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleNotificationSession getModuleNotificationSessionForCourseCatalog(org.osid.course.syllabus.ModuleReceiver moduleReceiver, 
                                                                                                           org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  notification service for the given course catalog. 
     *
     *  @param  moduleReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ModuleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> moduleReceiver, 
     *          courseCatalogId, </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleNotificationSession getModuleNotificationSessionForCourseCatalog(org.osid.course.syllabus.ModuleReceiver moduleReceiver, 
                                                                                                           org.osid.id.Id courseCatalogId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the session for retrieving module to course catalog mappings. 
     *
     *  @return a <code> ModuleCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleCourseCatalogSession getModuleCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for retrieving module to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ModuleCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleCourseCatalogSession getModuleCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for assigning module to course catalog mappings. 
     *
     *  @return a <code> ModuleCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleCourseCatalogAssignmentSession getModuleCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning module to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ModuleCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleCourseCatalogAssignmentSession getModuleCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the module smart course catalog for 
     *  the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> ModuleSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSmartCourseCatalogSession getModuleSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getModuleSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session associated with the module smart course catalog for 
     *  the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> ModuleSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSmartCourseCatalogSession getModuleSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getModuleSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet lookup 
     *  service. 
     *
     *  @return a <code> DocetLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetLookupSession getDocetLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DocetLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetLookupSession getDocetLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Docet 
     *          </code> 
     *  @return a <code> DocetLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetLookupSession getDocetLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DocetLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetLookupSession getDocetLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet query 
     *  service. 
     *
     *  @return a <code> DocetQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuerySession getDocetQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DocetQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuerySession getDocetQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Docet 
     *          </code> 
     *  @return a <code> DocetQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuerySession getDocetQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DocetQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Docet </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuerySession getDocetQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet search 
     *  service. 
     *
     *  @return a <code> DocetSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSearchSession getDocetSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DocetSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSearchSession getDocetSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Docet 
     *          </code> 
     *  @return a <code> DocetSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSearchSession getDocetSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DocetSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Docet </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSearchSession getDocetSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  administrative service. 
     *
     *  @return a <code> DocetAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetAdminSession getDocetAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DocetAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetAdminSession getDocetAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  administrative service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Docet 
     *          </code> 
     *  @return a <code> DocetAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetAdminSession getDocetAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DocetAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Docet </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetAdminSession getDocetAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  notification service. 
     *
     *  @param  docetReceiver the receiver 
     *  @return a <code> DocetNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> docetReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetNotificationSession getDocetNotificationSession(org.osid.course.syllabus.DocetReceiver docetReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  notification service. 
     *
     *  @param  docetReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> DocetNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> docetReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetNotificationSession getDocetNotificationSession(org.osid.course.syllabus.DocetReceiver docetReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  notification service for the given course catalog. 
     *
     *  @param  docetReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> DocetNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> docetReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetNotificationSession getDocetNotificationSessionForCourseCatalog(org.osid.course.syllabus.DocetReceiver docetReceiver, 
                                                                                                         org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  notification service for the given course catalog. 
     *
     *  @param  docetReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DocetNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Docet </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> docetReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetNotificationSession getDocetNotificationSessionForCourseCatalog(org.osid.course.syllabus.DocetReceiver docetReceiver, 
                                                                                                         org.osid.id.Id courseCatalogId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the session for retrieving docet to course catalog mappings. 
     *
     *  @return a <code> DocetCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetCourseCatalogSession getDocetCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for retrieving docet to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DocetCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetCourseCatalogSession getDocetCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for assigning docet to course catalog mappings. 
     *
     *  @return a <code> DocetCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetCourseCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetCourseCatalogAssignmentSession getDocetCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning docet to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DocetCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetCourseCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetCourseCatalogAssignmentSession getDocetCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the docet smart course catalog for 
     *  the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> DocetSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSmartCourseCatalogSession getDocetSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getDocetSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic docet course catalogs for the 
     *  given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of a course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> DocetSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSmartCourseCatalogSession getDocetSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getDocetSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets a <code> CourseSyllabusBatchManager. </code> 
     *
     *  @return a <code> CourseSyllabusBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSyllabusBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.batch.CourseSyllabusBatchManager getCourseSyllabusBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusManager.getCourseSyllabusBatchManager not implemented");
    }


    /**
     *  Gets a <code> CourseSyllabusBatchProxyManager. </code> 
     *
     *  @return a <code> CourseSyllabusBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSyllabusBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.batch.CourseSyllabusBatchProxyManager getCourseSyllabusBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.syllabus.CourseSyllabusProxyManager.getCourseSyllabusBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.syllabusRecordTypes.clear();
        this.syllabusRecordTypes.clear();

        this.syllabusSearchRecordTypes.clear();
        this.syllabusSearchRecordTypes.clear();

        this.moduleRecordTypes.clear();
        this.moduleRecordTypes.clear();

        this.moduleSearchRecordTypes.clear();
        this.moduleSearchRecordTypes.clear();

        this.docetRecordTypes.clear();
        this.docetRecordTypes.clear();

        this.docetSearchRecordTypes.clear();
        this.docetSearchRecordTypes.clear();

        return;
    }
}
