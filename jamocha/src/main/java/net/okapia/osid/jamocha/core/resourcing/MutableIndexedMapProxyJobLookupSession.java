//
// MutableIndexedMapProxyJobLookupSession
//
//    Implements a Job lookup service backed by a collection of
//    jobs indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Job lookup service backed by a collection of
 *  jobs. The jobs are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some jobs may be compatible
 *  with more types than are indicated through these job
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of jobs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyJobLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractIndexedMapJobLookupSession
    implements org.osid.resourcing.JobLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyJobLookupSession} with
     *  no job.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyJobLookupSession(org.osid.resourcing.Foundry foundry,
                                                       org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyJobLookupSession} with
     *  a single job.
     *
     *  @param foundry the foundry
     *  @param  job an job
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code job}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyJobLookupSession(org.osid.resourcing.Foundry foundry,
                                                       org.osid.resourcing.Job job, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putJob(job);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyJobLookupSession} using
     *  an array of jobs.
     *
     *  @param foundry the foundry
     *  @param  jobs an array of jobs
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobs}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyJobLookupSession(org.osid.resourcing.Foundry foundry,
                                                       org.osid.resourcing.Job[] jobs, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putJobs(jobs);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyJobLookupSession} using
     *  a collection of jobs.
     *
     *  @param foundry the foundry
     *  @param  jobs a collection of jobs
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobs}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyJobLookupSession(org.osid.resourcing.Foundry foundry,
                                                       java.util.Collection<? extends org.osid.resourcing.Job> jobs,
                                                       org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putJobs(jobs);
        return;
    }

    
    /**
     *  Makes a {@code Job} available in this session.
     *
     *  @param  job a job
     *  @throws org.osid.NullArgumentException {@code job{@code 
     *          is {@code null}
     */

    @Override
    public void putJob(org.osid.resourcing.Job job) {
        super.putJob(job);
        return;
    }


    /**
     *  Makes an array of jobs available in this session.
     *
     *  @param  jobs an array of jobs
     *  @throws org.osid.NullArgumentException {@code jobs{@code 
     *          is {@code null}
     */

    @Override
    public void putJobs(org.osid.resourcing.Job[] jobs) {
        super.putJobs(jobs);
        return;
    }


    /**
     *  Makes collection of jobs available in this session.
     *
     *  @param  jobs a collection of jobs
     *  @throws org.osid.NullArgumentException {@code job{@code 
     *          is {@code null}
     */

    @Override
    public void putJobs(java.util.Collection<? extends org.osid.resourcing.Job> jobs) {
        super.putJobs(jobs);
        return;
    }


    /**
     *  Removes a Job from this session.
     *
     *  @param jobId the {@code Id} of the job
     *  @throws org.osid.NullArgumentException {@code jobId{@code  is
     *          {@code null}
     */

    @Override
    public void removeJob(org.osid.id.Id jobId) {
        super.removeJob(jobId);
        return;
    }    
}
