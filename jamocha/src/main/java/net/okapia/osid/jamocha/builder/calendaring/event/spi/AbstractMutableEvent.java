//
// AbstractMutableEvent.java
//
//     Defines a mutable Event.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.event.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Event</code>.
 */

public abstract class AbstractMutableEvent
    extends net.okapia.osid.jamocha.calendaring.event.spi.AbstractEvent
    implements org.osid.calendaring.Event,
               net.okapia.osid.jamocha.builder.calendaring.event.EventMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    @Override
    public void setSequestered(boolean sequestered) {
        super.setSequestered(sequestered);
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this event. 
     *
     *  @param record event record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addEventRecord(org.osid.calendaring.records.EventRecord record, org.osid.type.Type recordType) {
        super.addEventRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this event is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this event ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this event.
     *
     *  @param displayName the name for this event
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this event.
     *
     *  @param description the description of this event
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the implicit flag.
     *
     *  @param implicit <code> true </code> if this event is implicit,
     *          <code> false </code> if explicitly defined
     */

    @Override
    public void setImplicit(boolean implicit) {
        super.setImplicit(implicit);
        return;
    }


    /**
     *  Sets the in recurring series flag.
     *
     *  @param recurring <code> true </code> if this event is part of
     *         a recurring series, <code> false </code> otherwise
     */

    @Override
    public void setInRecurringSeries(boolean recurring) {
        super.setInRecurringSeries(recurring);
        return;
    }

    
    /**
     *  Sets the superseding event flag.
     *
     *  @param superseding <code> true </code> if this event is
     *          superseding event, <code> false </code> otherwise
     */

    @Override
    public void setSupersedingEvent(boolean superseding) {
        super.setSupersedingEvent(superseding);
        return;
    }


    /**
     *  Sets the offset event flag.
     *
     *  @param offset <code> true </code> if this event is an offset
     *          event, <code> false </code> otherwise
     */

    @Override
    public void setOffsetEvent(boolean offset) {
        super.setOffsetEvent(offset);
        return;
    }


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    @Override
    public void setDuration(org.osid.calendaring.Duration duration) {
        super.setDuration(duration);
        return;
    }


    /**
     *  Sets the location description.
     *
     *  @param locationDescription a location description
     *  @throws org.osid.NullArgumentException
     *          <code>locationDescription</code> is <code>null</code>
     */

    @Override
    public void setLocationDescription(org.osid.locale.DisplayText locationDescription) {
        super.setLocationDescription(locationDescription);
        return;
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    @Override
    public void setLocation(org.osid.mapping.Location location) {
        super.setLocation(location);
        return;
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    @Override
    public void addSponsor(org.osid.resource.Resource sponsor) {
        super.addSponsor(sponsor);
        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    @Override
    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        super.setSponsors(sponsors);
        return;
    }
}

