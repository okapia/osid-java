//
// AbstractFederatingPoolConstrainerEnablerLookupSession.java
//
//     An abstract federating adapter for a PoolConstrainerEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PoolConstrainerEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPoolConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession>
    implements org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Constructs a new
     *  <code>AbstractFederatingPoolConstrainerEnablerLookupSession</code>.
     */

    protected AbstractFederatingPoolConstrainerEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform
     *  <code>PoolConstrainerEnabler</code> lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPoolConstrainerEnablers() {
        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            if (session.canLookupPoolConstrainerEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>PoolConstrainerEnabler</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePoolConstrainerEnablerView() {
        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            session.useComparativePoolConstrainerEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>PoolConstrainerEnabler</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPoolConstrainerEnablerView() {
        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            session.usePlenaryPoolConstrainerEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pool constrainer enablers in distributors
     *  which are children of this distributor in the distributor
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            session.useFederatedDistributorView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            session.useIsolatedDistributorView();
        }

        return;
    }


    /**
     *  Only active pool constrainer enablers are returned by methods
     *  in this session.
     */
     
    @OSID @Override
    public void useActivePoolConstrainerEnablerView() {
        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            session.useActivePoolConstrainerEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive pool constrainer enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolConstrainerEnablerView() {
        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            session.useAnyStatusPoolConstrainerEnablerView();
        }

        return;
    }

     
    /**
     *  Gets the <code>PoolConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PoolConstrainerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PoolConstrainerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerId <code>Id</code> of the
     *          <code>PoolConstrainerEnabler</code>
     *  @return the pool constrainer enabler
     *  @throws org.osid.NotFoundException <code>poolConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnabler getPoolConstrainerEnabler(org.osid.id.Id poolConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            try {
                return (session.getPoolConstrainerEnabler(poolConstrainerEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(poolConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  poolConstrainerEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>PoolConstrainerEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByIds(org.osid.id.IdList poolConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.rules.poolconstrainerenabler.MutablePoolConstrainerEnablerList ret = new net.okapia.osid.jamocha.provisioning.rules.poolconstrainerenabler.MutablePoolConstrainerEnablerList();

        try (org.osid.id.IdList ids = poolConstrainerEnablerIds) {
            while (ids.hasNext()) {
                ret.addPoolConstrainerEnabler(getPoolConstrainerEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> corresponding
     *  to the given pool constrainer enabler genus <code>Type</code>
     *  which does not include pool constrainer enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those pool constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerGenusType a poolConstrainerEnabler genus type 
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByGenusType(org.osid.type.Type poolConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainerenabler.FederatingPoolConstrainerEnablerList ret = getPoolConstrainerEnablerList();

        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            ret.addPoolConstrainerEnablerList(session.getPoolConstrainerEnablersByGenusType(poolConstrainerEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> corresponding
     *  to the given pool constrainer enabler genus <code>Type</code>
     *  and include any additional pool constrainer enablers with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those pool constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerGenusType a poolConstrainerEnabler genus type 
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByParentGenusType(org.osid.type.Type poolConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainerenabler.FederatingPoolConstrainerEnablerList ret = getPoolConstrainerEnablerList();

        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            ret.addPoolConstrainerEnablerList(session.getPoolConstrainerEnablersByParentGenusType(poolConstrainerEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> containing the given
     *  pool constrainer enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known pool
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those pool constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerRecordType a poolConstrainerEnabler record type 
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByRecordType(org.osid.type.Type poolConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainerenabler.FederatingPoolConstrainerEnablerList ret = getPoolConstrainerEnablerList();

        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            ret.addPoolConstrainerEnablerList(session.getPoolConstrainerEnablersByRecordType(poolConstrainerEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  pool constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>PoolConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainerenabler.FederatingPoolConstrainerEnablerList ret = getPoolConstrainerEnablerList();

        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            ret.addPoolConstrainerEnablerList(session.getPoolConstrainerEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>PoolConstrainerEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainerenabler.FederatingPoolConstrainerEnablerList ret = getPoolConstrainerEnablerList();

        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            ret.addPoolConstrainerEnablerList(session.getPoolConstrainerEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>PoolConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those pool constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @return a list of <code>PoolConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainerenabler.FederatingPoolConstrainerEnablerList ret = getPoolConstrainerEnablerList();

        for (org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session : getSessions()) {
            ret.addPoolConstrainerEnablerList(session.getPoolConstrainerEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainerenabler.FederatingPoolConstrainerEnablerList getPoolConstrainerEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainerenabler.ParallelPoolConstrainerEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainerenabler.CompositePoolConstrainerEnablerList());
        }
    }
}
