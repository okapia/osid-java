//
// AbstractParameterLookupSession.java
//
//    A starter implementation framework for providing a Parameter
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Parameter
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getParameters(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractParameterLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.configuration.ParameterLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();
    

    /**
     *  Gets the <code>Configuration/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the <code>Configuration</code>.
     *
     *  @param  configuration the configuration for this session
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this user can perform <code>Parameter</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParameters() {
        return (true);
    }


    /**
     *  A complete view of the <code>Parameter</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParameterView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Parameter</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParameterView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameters in configurations which are
     *  children of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active parameters are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveParameterView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive parameters are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusParameterView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Parameter</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Parameter</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Parameter</code> and
     *  retained for compatibility.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterId <code>Id</code> of the
     *          <code>Parameter</code>
     *  @return the parameter
     *  @throws org.osid.NotFoundException <code>parameterId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>parameterId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Parameter getParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.configuration.ParameterList parameters = getParameters()) {
            while (parameters.hasNext()) {
                org.osid.configuration.Parameter parameter = parameters.getNextParameter();
                if (parameter.getId().equals(parameterId)) {
                    return (parameter);
                }
            }
        } 

        throw new org.osid.NotFoundException(parameterId + " not found");
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  parameters specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Parameters</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getParameters()</code>.
     *
     *  @param  parameterIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByIds(org.osid.id.IdList parameterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.configuration.Parameter> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = parameterIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getParameter(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("parameter " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.configuration.parameter.LinkedParameterList(ret));
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  parameter genus <code>Type</code> which does not include
     *  parameters of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getParameters()</code>.
     *
     *  @param  parameterGenusType a parameter genus type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByGenusType(org.osid.type.Type parameterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.parameter.ParameterGenusFilterList(getParameters(), parameterGenusType));
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  parameter genus <code>Type</code> and include any additional
     *  parameters with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getParameters()</code>.
     *
     *  @param  parameterGenusType a parameter genus type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByParentGenusType(org.osid.type.Type parameterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getParametersByGenusType(parameterGenusType));
    }


    /**
     *  Gets a <code>ParameterList</code> containing the given
     *  parameter record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getParameters()</code>.
     *
     *  @param  parameterRecordType a parameter record type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByRecordType(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.parameter.ParameterRecordFilterList(getParameters(), parameterRecordType));
    }


    /**
     *  Gets all <code>Parameters</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @return a list of <code>Parameters</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.configuration.ParameterList getParameters()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the parameter list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of parameters
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.configuration.ParameterList filterParametersOnViews(org.osid.configuration.ParameterList list)
        throws org.osid.OperationFailedException {

        org.osid.configuration.ParameterList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.configuration.parameter.ActiveParameterFilterList(ret);
        }

        return (ret);
    }
}
