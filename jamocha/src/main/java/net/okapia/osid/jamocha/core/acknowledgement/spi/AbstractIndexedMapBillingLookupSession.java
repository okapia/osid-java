//
// AbstractIndexedMapBillingLookupSession.java
//
//    A simple framework for providing a Billing lookup service
//    backed by a fixed collection of billings with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Billing lookup service backed by a
 *  fixed collection of billings. The billings are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some billings may be compatible
 *  with more types than are indicated through these billing
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Billings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBillingLookupSession
    extends AbstractMapBillingLookupSession
    implements org.osid.acknowledgement.BillingLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.acknowledgement.Billing> billingsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.acknowledgement.Billing>());
    private final MultiMap<org.osid.type.Type, org.osid.acknowledgement.Billing> billingsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.acknowledgement.Billing>());


    /**
     *  Makes a <code>Billing</code> available in this session.
     *
     *  @param  billing a billing
     *  @throws org.osid.NullArgumentException <code>billing<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBilling(org.osid.acknowledgement.Billing billing) {
        super.putBilling(billing);

        this.billingsByGenus.put(billing.getGenusType(), billing);
        
        try (org.osid.type.TypeList types = billing.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.billingsByRecord.put(types.getNextType(), billing);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a billing from this session.
     *
     *  @param billingId the <code>Id</code> of the billing
     *  @throws org.osid.NullArgumentException <code>billingId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBilling(org.osid.id.Id billingId) {
        org.osid.acknowledgement.Billing billing;
        try {
            billing = getBilling(billingId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.billingsByGenus.remove(billing.getGenusType());

        try (org.osid.type.TypeList types = billing.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.billingsByRecord.remove(types.getNextType(), billing);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBilling(billingId);
        return;
    }


    /**
     *  Gets a <code>BillingList</code> corresponding to the given
     *  billing genus <code>Type</code> which does not include
     *  billings of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known billings or an error results. Otherwise,
     *  the returned list may contain only those billings that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  billingGenusType a billing genus type 
     *  @return the returned <code>Billing</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>billingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByGenusType(org.osid.type.Type billingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.acknowledgement.billing.ArrayBillingList(this.billingsByGenus.get(billingGenusType)));
    }


    /**
     *  Gets a <code>BillingList</code> containing the given
     *  billing record <code>Type</code>. In plenary mode, the
     *  returned list contains all known billings or an error
     *  results. Otherwise, the returned list may contain only those
     *  billings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  billingRecordType a billing record type 
     *  @return the returned <code>billing</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>billingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByRecordType(org.osid.type.Type billingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.acknowledgement.billing.ArrayBillingList(this.billingsByRecord.get(billingRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.billingsByGenus.clear();
        this.billingsByRecord.clear();

        super.close();

        return;
    }
}
