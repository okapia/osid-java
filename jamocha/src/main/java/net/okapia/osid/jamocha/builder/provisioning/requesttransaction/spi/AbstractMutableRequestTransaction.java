//
// AbstractMutableRequestTransaction.java
//
//     Defines a mutable RequestTransaction.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.requesttransaction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>RequestTransaction</code>.
 */

public abstract class AbstractMutableRequestTransaction
    extends net.okapia.osid.jamocha.provisioning.requesttransaction.spi.AbstractRequestTransaction
    implements org.osid.provisioning.RequestTransaction,
               net.okapia.osid.jamocha.builder.provisioning.requesttransaction.RequestTransactionMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this request transaction. 
     *
     *  @param record request transaction record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addRequestTransactionRecord(org.osid.provisioning.records.RequestTransactionRecord record, org.osid.type.Type recordType) {
        super.addRequestTransactionRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this request transaction is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this request transaction ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this request transaction.
     *
     *  @param displayName the name for this request transaction
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this request transaction.
     *
     *  @param description the description of this request transaction
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this request transaction
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException <code>broker</code> is
     *          <code>null</code>
     */

    @Override
    public void setBroker(org.osid.provisioning.Broker broker) {
        super.setBroker(broker);
        return;
    }


    /**
     *  Sets the submit date.
     *
     *  @param date a submit date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setSubmitDate(org.osid.calendaring.DateTime date) {
        super.setSubmitDate(date);
        return;
    }


    /**
     *  Sets the submitter.
     *
     *  @param submitter a submitter
     *  @throws org.osid.NullArgumentException <code>submitter</code>
     *          is <code>null</code>
     */

    @Override
    public void setSubmitter(org.osid.resource.Resource submitter) {
        super.setSubmitter(submitter);
        return;
    }


    /**
     *  Sets the submitting agent.
     *
     *  @param agent a submitting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setSubmittingAgent(org.osid.authentication.Agent agent) {
        super.setSubmittingAgent(agent);
        return;
    }


    /**
     *  Adds a request.
     *
     *  @param request a request
     *  @throws org.osid.NullArgumentException <code>request</code> is
     *          <code>null</code>
     */

    @Override
    public void addRequest(org.osid.provisioning.Request request) {
        super.addRequest(request);
        return;
    }


    /**
     *  Sets all the requests.
     *
     *  @param requests a collection of requests
     *  @throws org.osid.NullArgumentException <code>requests</code>
     *          is <code>null</code>
     */

    @Override
    public void setRequests(java.util.Collection<org.osid.provisioning.Request> requests) {
        super.setRequests(requests);
        return;
    }
}

