//
// AbstractAsset.java
//
//     Defines an Asset.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.asset.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Asset</code>.
 */

public abstract class AbstractAsset
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObject
    implements org.osid.repository.Asset {

    private org.osid.locale.DisplayText title;
    private org.osid.locale.DisplayText copyright;
    private String copyrightRegistration = "";

    private boolean copyrightStatusKnown      = false;
    private boolean publicDomain              = false;
    private boolean canDistributeVerbatim     = false;
    private boolean canDistributeAlterations  = false;
    private boolean canDistributeCompositions = false;

    private org.osid.resource.Resource source;
    private final java.util.Collection<org.osid.resource.Resource> providerLinks = new java.util.LinkedHashSet<>();
    private org.osid.calendaring.DateTime createdDate;
    private org.osid.calendaring.DateTime publishedDate;
    private org.osid.locale.DisplayText principalCreditString;
    private final java.util.Collection<org.osid.repository.AssetContent> assetContents = new java.util.LinkedHashSet<>();
    private org.osid.repository.Composition composition;

    private final java.util.Collection<org.osid.repository.records.AssetRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the proper title of this asset. This may be the same as the 
     *  display name or the display name may be used for a less formal label. 
     *
     *  @return the title of this asset 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.title);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    protected void setTitle(org.osid.locale.DisplayText title) {
        nullarg(title, "title");
        this.title = title;
        return;
    }


    /**
     *  Tests if the copyright status is known. 
     *
     *  @return <code> true </code> if the copyright status of this
     *          asset is known, <code> false </code> otherwise. If
     *          <code> false, isPublicDomain(), </code> <code>
     *          canDistributeVerbatim(), canDistributeAlterations()
     *          and canDistributeCompositions() </code> may also be
     *          <code> false. </code>
     */

    @OSID @Override
    public boolean isCopyrightStatusKnown() {
        return (this.copyrightStatusKnown);
    }


    /**
     *  Sets the copyright status known flag.
     *
     *  @param status <code> true </code> if the copyright status of
     *         this asset is known, <code> false </code> otherwise. If
     *         <code> false, isPublicDomain(), </code> <code>
     *         canDistributeVerbatim(), canDistributeAlterations() and
     *         canDistributeCompositions() </code> may also be <code>
     *         false. </code>
     */

    protected void setCopyrightStatusKnown(boolean status) {
        this.copyrightStatusKnown = status;
        return;
    }


    /**
     *  Tests if this asset is in the public domain. An asset is in
     *  the public domain if copyright is not applicable, the
     *  copyright has expired, or the copyright owner has expressly
     *  relinquished the copyright.
     *
     *  @return <code> true </code> if this asset is in the public
     *          domain, <code> false </code> otherwise. If <code>
     *          true, </code> <code> canDistributeVerbatim(),
     *          canDistributeAlterations() and
     *          canDistributeCompositions() </code> must also be
     *          <code> true.  </code>
     *  @throws org.osid.IllegalStateException <code>
     *          isCopyrightStatusKnown() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public boolean isPublicDomain() {
        if (!isCopyrightStatusKnown()) {
            throw new org.osid.IllegalStateException("isCopyrightStatusKnown() is false");
        }

        return (this.publicDomain);
    }


    /**
     *  Sets the public domain flag and enables all the distribution
     *  rights if <code>true</code>.
     *
     *  @param publicDomain <code> true </code> if this asset is in
     *         the public domain, <code> false </code> otherwise. If
     *         <code> true, </code> <code> canDistributeVerbatim(),
     *         canDistributeAlterations() and
     *         canDistributeCompositions() </code> must also be <code>
     *         true.  </code>
     */

    protected void setPublicDomain(boolean publicDomain) {
        if (publicDomain == true) {
            this.canDistributeVerbatim     = true;
            this.canDistributeAlterations  = true;
            this.canDistributeCompositions = true;
        }

        this.publicDomain = publicDomain;
        return;
    }


    /**
     *  Gets the copyright statement and of this asset which
     *  identifies the current copyright holder. For an asset in the
     *  public domain, this method may return the original copyright
     *  statement although it may be no longer valid.
     *
     *  @return the copyright statement or an empty string if none
     *          available.  An empty string does not imply the asset
     *          is not protected by copyright.
     *  @throws org.osid.IllegalStateException <code>
     *          isCopyrightStatusKnown() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCopyright() {
        if (!isCopyrightStatusKnown()) {
            throw new org.osid.IllegalStateException("isCopyrightStatusKnown() is false");
        }

        return (this.copyright);
    }


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @throws org.osid.NullArgumentException
     *          <code>copyright</code> is <code>null</code>
     */

    protected void setCopyright(org.osid.locale.DisplayText copyright) {
        nullarg(copyright, "copyright");
        this.copyright = copyright;
        return;
    }


    /**
     *  Gets the copyright registration information for this asset. 
     *
     *  @return the copyright registration. An empty string means the 
     *          registration isn't known. 
     *  @throws org.osid.IllegalStateException <code>
     *          isCopyrightStatusKnown() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public String getCopyrightRegistration() {
        if (!isCopyrightStatusKnown()) {
            throw new org.osid.IllegalStateException("isCopyrightStatusKnown() is false");
        }

        return (this.copyrightRegistration);
    }


    /**
     *  Sets the copyright registration.
     *
     *  @param registration a copyright registration
     *  @throws org.osid.NullArgumentException
     *          <code>registration</code> is <code>null</code>
     */

    protected void setCopyrightRegistration(String registration) {
        nullarg(registration, "copyright registration");
        this.copyrightRegistration = registration;
        return;
    }


    /**
     *  Tests if there are any license restrictions on this asset that
     *  restrict the distribution, re-publication or public display of
     *  this asset, commercial or otherwise, without modification,
     *  alteration, or inclusion in other works. This method is
     *  intended to offer consumers a means of filtering out search
     *  results that restrict distribution for any purpose. The scope
     *  of this method does not include licensing that describes
     *  warranty disclaimers or attribution requirements. This method
     *  is intended for informational purposes only and does not
     *  replace or override the terms specified in a license agreement
     *  which may specify exceptions or additional restrictions.
     *
     *  @return <code> true </code> if the asset can be distributed
     *          verbatim, <code> false </code> otherwise.
     *  @throws org.osid.IllegalStateException <code>
     *          isCopyrightStatusKnown() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public boolean canDistributeVerbatim() {
        if (!isCopyrightStatusKnown()) {
            throw new org.osid.IllegalStateException("isCopyrightStatusKnown() is false");
        }

        return (this.canDistributeVerbatim);
    }


    /**
     *  Sets the can distribute verbatim flag.
     *
     *  @param status <code> true </code> if the asset can be
     *         distributed verbatim, <code> false </code> otherwise.
     */

    protected void setCanDistributeVerbatim(boolean status) {
        this.canDistributeVerbatim = status;
        return;
    }


    /**
     *  Tests if there are any license restrictions on this asset that
     *  restrict the distribution, re-publication or public display of
     *  any alterations or modifications to this asset, commercial or
     *  otherwise, for any purpose. This method is intended to offer
     *  consumers a means of filtering out search results that
     *  restrict the distribution or public display of any
     *  modification or alteration of the content or its metadata of
     *  any kind, including editing, translation, resampling, resizing
     *  and cropping. The scope of this method does not include
     *  licensing that describes warranty disclaimers or attribution
     *  requirements. This method is intended for informational
     *  purposes only and does not replace or override the terms
     *  specified in a license agreement which may specify exceptions
     *  or additional restrictions.
     *
     *  @return <code> true </code> if the asset can be modified,
     *          <code> false </code> otherwise. If <code> true,
     *          </code> <code> canDistributeVerbatim() </code> must
     *          also be <code> true.  </code>
     *  @throws org.osid.IllegalStateException <code>
     *          isCopyrightStatusKnown() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public boolean canDistributeAlterations() {
        if (!isCopyrightStatusKnown()) {
            throw new org.osid.IllegalStateException("isCopyrightStatusKnown() is false");
        }

        return (this.canDistributeAlterations);
    }


    /**
     *  Sets the can distribute alterations flag.
     *
     *  @param status <code> true </code> if the asset can be
     *         modified, <code> false </code> otherwise. If <code>
     *         true, </code> <code> canDistributeVerbatim() </code>
     *         must also be <code> true.  </code>
     */

    protected void setCanDistributeAlterations(boolean status) {
        this.canDistributeAlterations = status;
        return;
    }


    /**
     *  Tests if there are any license restrictions on this asset that
     *  restrict the distribution, re-publication or public display of
     *  this asset as an inclusion within other content or
     *  composition, commercial or otherwise, for any purpose,
     *  including restrictions upon the distribution or license of the
     *  resulting composition. This method is intended to offer
     *  consumers a means of filtering out search results that
     *  restrict the use of this asset within compositions. The scope
     *  of this method does not include licensing that describes
     *  warranty disclaimers or attribution requirements. This method
     *  is intended for informational purposes only and does not
     *  replace or override the terms specified in a license agreement
     *  which may specify exceptions or additional restrictions.
     *
     *  @return <code> true </code> if the asset can be part of a
     *          larger composition <code> false </code> otherwise. If
     *          <code> true, </code> <code> canDistributeVerbatim()
     *          </code> must also be <code> true. </code>
     *  @throws org.osid.IllegalStateException <code>
     *          isCopyrightStatusKnown() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public boolean canDistributeCompositions() {
        if (!isCopyrightStatusKnown()) {
            throw new org.osid.IllegalStateException("isCopyrightStatusKnown() is false");
        }

        return (this.canDistributeCompositions);
    }


    /**
     *  Sets the can distribute compositions flag.
     *
     *  @param status <code> true </code> if the asset can be part of
     *         a larger composition <code> false </code> otherwise. If
     *         <code> true, </code> <code> canDistributeVerbatim()
     *         </code> must also be <code> true. </code>
     */

    protected void setCanDistributeCompositions(boolean status) {
        this.canDistributeCompositions = status;
        return;
    }


    /**
     *  Sets all distribution rights.
     * 
     *  @param canDistributeVerbatim <code> true </code> if the asset
     *         can be distributed verbatim, <code> false </code>
     *         otherwise.
     *  @param canDistributeAlterations <code> true </code> if the
     *         asset can be modified, <code> false </code>
     *         otherwise. If <code> true, </code> <code>
     *         canDistributeVerbatim() </code> must also be <code>
     *         true.  </code>
     *  @param canDistributeCompositions status <code> true </code> if
     *         the asset can be part of a larger composition <code>
     *         false </code> otherwise. If <code> true, </code> <code>
     *         canDistributeVerbatim() </code> must also be <code>
     *         true. </code>
     *  @throws org.osid.BadLogicException 
     */

    protected void setDistributionRights(boolean canDistributeVerbatim,
                                         boolean canDistributeAlterations,
                                         boolean canDistributeCompositions) {
        
        if (canDistributeAlterations && !(canDistributeVerbatim)) {
            throw new org.osid.BadLogicException("canDistributeVerbatim must be true");
        }

        if (canDistributeCompositions && !(canDistributeVerbatim)) {
            throw new org.osid.BadLogicException("canDistributeVerbatim must be true");
        }

        setCanDistributeVerbatim(canDistributeVerbatim);
        setCanDistributeAlterations(canDistributeAlterations);
        setCanDistributeCompositions(canDistributeCompositions);

        return;
    }
        

    /**
     *  Gets the <code> Resource Id </code> of the source of this
     *  asset. The source is the original owner of the copyright of
     *  this asset and may differ from the creator of this asset. The
     *  source for a published book written by Margaret Mitchell would
     *  be Macmillan. The source for an unpublished painting by Arthur
     *  Goodwin would be Arthur Goodwin.
     *  
     *  An <code> Asset </code> is <code> Sourceable </code> and also
     *  contains a provider identity. The provider is the entity that
     *  makes this digital asset available in this repository but may
     *  or may not be the publisher of the contents depicted in the
     *  asset. For example, a map published by Ticknor and Fields in
     *  1848 may have a provider of Library of Congress and a source
     *  of Ticknor and Fields. If copied from a repository at
     *  Middlebury College, the provider would be Middlebury College
     *  and a source of Ticknor and Fields.
     *
     *  @return the source <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceId() {
        return (this.source.getId());
    }


    /**
     *  Gets the <code> Resource </code> of the source of this
     *  asset. The source is the original owner of the copyright of
     *  this asset and may differ from the creator of this asset. The
     *  source for a published book written by Margaret Mitchell would
     *  be Macmillan. The source for an unpublished painting by Arthur
     *  Goodwin would be Arthur Goodwin.
     *
     *  @return the source 
     */

    @OSID @Override
    public org.osid.resource.Resource getSource() {
        return (this.source);
    }


    /**
     *  Sets the source.
     *
     *  @param source a source
     *  @throws org.osid.NullArgumentException
     *          <code>source</code> is <code>null</code>
     */

    protected void setSource(org.osid.resource.Resource source) {
        nullarg(source, "source");
        this.source = source;
        return;
    }


    /**
     *  Gets the resource <code> Ids </code> representing the source
     *  of this asset in order from the most recent provider to the
     *  originating source.
     *
     *  @return the provider <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getProviderLinkIds() {
        try {
            org.osid.resource.ResourceList providerLinks = getProviderLinks();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(providerLinks));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> Resources </code> representing the source of
     *  this asset order from the most recent provider to the
     *  originating source.
     *
     *  @return the provider chain 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getProviderLinks()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.providerLinks));
    }


    /**
     *  Adds a provider to the chain.
     *
     *  @param provider a provider link
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected void addProviderLink(org.osid.resource.Resource provider) {
        nullarg(provider, "provider");
        this.providerLinks.add(provider);
        return;
    }


    /**
     *  Sets the entire provider chain.
     *
     *  @param providers a collection of provider links
     *  @throws org.osid.NullArgumentException <code>providers</code>
     *          is <code>null</code>
     */

    protected void setProviderLinks(java.util.Collection<org.osid.resource.Resource> providers) {
        nullarg(providers, "providers");

        this.providerLinks.clear();
        this.providerLinks.addAll(providers);

        return;
    }


    /**
     *  Gets the created date of this asset, which is generally not
     *  related to when the object representing the asset was
     *  created. The date returned may indicate that not much is
     *  known.
     *
     *  @return the created date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreatedDate() {
        return (this.createdDate);
    }


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setCreatedDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "created date");
        this.createdDate = date;
        return;
    }


    /**
     *  Tests if this asset has been published. Not all assets
     *  viewable in this repository may have been published. The
     *  source of a published asset indicates the publisher.
     *
     *  @return true if this asset has been published, <code> false </code> if 
     *          unpublished or its published status is not known 
     */

    @OSID @Override
    public boolean isPublished() {
        return (this.publishedDate != null);
    }


    /**
     *  Gets the published date of this asset. Unpublished assets have
     *  no published date. A published asset has a date available,
     *  however the date returned may indicate that not much is known.
     *
     *  @return the published date 
     *  @throws org.osid.IllegalStateException <code> isPublished() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getPublishedDate() {
        return (this.publishedDate);
    }


    /**
     *  Sets the published date.
     *
     *  @param date a published date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setPublishedDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "published date");
        this.publishedDate = date;
        return;
    }


    /**
     *  Gets the credits of the principal people involved in the
     *  production of this asset as a display string.
     *
     *  @return the principal credits 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getPrincipalCreditString() {
        return (this.principalCreditString);
    }


    /**
     *  Sets the principal credit string.
     *
     *  @param credit a principal credit string
     *  @throws org.osid.NullArgumentException
     *          <code>credit</code> is
     *          <code>null</code>
     */

    protected void setPrincipalCreditString(org.osid.locale.DisplayText credit) {
        nullarg(credit, "credit");
        this.principalCreditString = credit;
        return;
    }


    /**
     *  Gets the content <code> Ids </code> of this asset. 
     *
     *  @return the asset content <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetContentIds() {
        try {
            org.osid.repository.AssetContentList assetContents = getAssetContents();
            return (new net.okapia.osid.jamocha.adapter.converter.repository.assetcontent.AssetContentToIdList(assetContents));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the content of this asset. 
     *
     *  @return the asset contents 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetContentList getAssetContents()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.repository.assetcontent.ArrayAssetContentList(this.assetContents));
    }


    /**
     *  Adds an asset content.
     *
     *  @param content an asset content
     *  @throws org.osid.NullArgumentException
     *          <code>content</code> is <code>null</code>
     */

    protected void addAssetContent(org.osid.repository.AssetContent content) {
        nullarg(content, "asset content");
        this.assetContents.add(content);
        return;
    }


    /**
     *  Sets all the asset contents.
     *
     *  @param contents a collection of asset contents
     *  @throws org.osid.NullArgumentException <code>contents</code>
     *          is <code>null</code>
     */

    protected void setAssetContents(java.util.Collection<org.osid.repository.AssetContent> contents) {
        nullarg(contents, "asset contents");
        this.assetContents.clear();
        this.assetContents.addAll(contents);
        return;
    }


    /**
     *  Tetss if this asset is a representation of a composition of assets. 
     *
     *  @return true if this asset is a composition, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean isComposition() {
        return (this.composition != null);
    }


    /**
     *  Gets the <code> Composition </code> <code> Id </code> corresponding to 
     *  this asset. 
     *
     *  @return the composiiton <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isComposition() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCompositionId() {
        return (this.composition.getId());
    }


    /**
     *  Gets the Composition corresponding to this asset. 
     *
     *  @return the composiiton 
     *  @throws org.osid.IllegalStateException <code> isComposition()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.Composition getComposition()
        throws org.osid.OperationFailedException {

        return (this.composition);
    }


    /**
     *  Sets the composition.
     *
     *  @param composition a composition
     *  @throws org.osid.NullArgumentException
     *          <code>composition</code> is <code>null</code>
     */

    protected void setComposition(org.osid.repository.Composition composition) {
        nullarg(composition, "composition");
        this.composition = composition;
        return;
    }


    /**
     *  Tests if this asset supports the given record
     *  <code>Type</code>.
     *
     *  @param  assetRecordType an asset record type 
     *  @return <code>true</code> if the assetRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assetRecordType) {
        for (org.osid.repository.records.AssetRecord record : this.records) {
            if (record.implementsRecordType(assetRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Asset</code>
     *  record <code>Type</code>.
     *
     *  @param  assetRecordType the asset record type 
     *  @return the asset record 
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetRecord getAssetRecord(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetRecord record : this.records) {
            if (record.implementsRecordType(assetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetRecordType + " is not supported");
    }


    /**
     *  Adds a record to this asset. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assetRecord the asset record
     *  @param assetRecordType asset record type
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecord</code> or
     *          <code>assetRecordTypeasset</code> is
     *          <code>null</code>
     */
            
    protected void addAssetRecord(org.osid.repository.records.AssetRecord assetRecord, 
                                  org.osid.type.Type assetRecordType) {
        
        nullarg(assetRecord, "asset record");
        addRecordType(assetRecordType);
        this.records.add(assetRecord);
        
        return;
    }
}
