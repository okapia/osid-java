//
// AbstractParticipantLookupSession.java
//
//    A starter implementation framework for providing a Participant
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Participant
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getParticipants(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractParticipantLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.offering.ParticipantLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();
    

    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>Participant</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParticipants() {
        return (true);
    }


    /**
     *  A complete view of the <code>Participant</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParticipantView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Participant</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParticipantView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include participants in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only participants whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveParticipantView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All participants of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveParticipantView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Participant</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Participant</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Participant</code> and
     *  retained for compatibility.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  participantId <code>Id</code> of the
     *          <code>Participant</code>
     *  @return the participant
     *  @throws org.osid.NotFoundException <code>participantId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>participantId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Participant getParticipant(org.osid.id.Id participantId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.offering.ParticipantList participants = getParticipants()) {
            while (participants.hasNext()) {
                org.osid.offering.Participant participant = participants.getNextParticipant();
                if (participant.getId().equals(participantId)) {
                    return (participant);
                }
            }
        } 

        throw new org.osid.NotFoundException(participantId + " not found");
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  participants specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Participants</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getParticipants()</code>.
     *
     *  @param  participantIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>participantIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByIds(org.osid.id.IdList participantIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.offering.Participant> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = participantIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getParticipant(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("participant " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.offering.participant.LinkedParticipantList(ret));
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  participant genus <code>Type</code> which does not include
     *  participants of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getParticipants()</code>.
     *
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusType(org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantGenusFilterList(getParticipants(), participantGenusType));
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  participant genus <code>Type</code> and include any additional
     *  participants with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getParticipants()</code>.
     *
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByParentGenusType(org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getParticipantsByGenusType(participantGenusType));
    }


    /**
     *  Gets a <code>ParticipantList</code> containing the given
     *  participant record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getParticipants()</code>.
     *
     *  @param  participantRecordType a participant record type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByRecordType(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantRecordFilterList(getParticipants(), participantRecordType));
    }


    /**
     *  Gets a <code>ParticipantList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In active mode, participants are returned that are currently
     *  active. In any status mode, active and inactive participants
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Participant</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsOnDate(org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.TemporalParticipantFilterList(getParticipants(), from, to));
    }
        

    /**
     *  Gets a <code>ParticipantList</code> by genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In active mode, participants are returned that are currently
     *  active. In any status mode, active and inactive participants
     *  are returned.
     *
     *  @param participantGenusType an offering genus type
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Participant</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>participantGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeOnDate(org.osid.type.Type participantGenusType, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.TemporalParticipantFilterList(getParticipantsByGenusType(participantGenusType), from, to));
    }
        

    /**
     *  Gets a list of participants corresponding to an offering
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsForOffering(org.osid.id.Id offeringId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new OfferingFilter(offeringId), getParticipants()));
    }


    /**
     *  Gets a list of participants by genus type corresponding to an
     *  offering <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param participantGenusType an offering genus type
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>
     *          or <code>participantGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOffering(org.osid.id.Id offeringId,
                                                                                    org.osid.type.Type participantGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new OfferingFilter(offeringId), getParticipantsByGenusType(participantGenusType)));
    }
    

    /**
     *  Gets a list of participants corresponding to an offering
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>offeringId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingOnDate(org.osid.id.Id offeringId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.TemporalParticipantFilterList(getParticipantsForOffering(offeringId), from, to));
    }


    /**
     *  Gets a list of participants by genus type corresponding to an
     *  offering <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param participantGenusType a participant genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>offeringId</code>,
     *          <code>participantGenusType</code>, <code>from</code>,
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOfferingOnDate(org.osid.id.Id offeringId,
                                                                                         org.osid.type.Type participantGenusType,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.TemporalParticipantFilterList(getParticipantsByGenusTypeForOffering(offeringId, participantGenusType), from, to));
    }


    /**
     *  Gets a <code> ParticipantList </code> for the given offering
     *  in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param offeringId an offering <code> Id </code>
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> offeringId
     *          </code> or <code> timePeriodId </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOffering(org.osid.id.Id offeringId, 
                                                                                    org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsForOffering(offeringId)));
    }


    /**
     *  Gets a <code> ParticipantList </code> by genus type for the
     *  given offering in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          timePeriodId, </code> or <code> participantGenusType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOffering(org.osid.id.Id offeringId, 
                                                                                                org.osid.id.Id timePeriodId, 
                                                                                                org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsByGenusTypeForOffering(offeringId, participantGenusType)));
    }


    /**
     *  Gets a <code> ParticipantList </code> for an offering in a
     *  time period and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          timePeriodId, from, </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOfferingOnDate(org.osid.id.Id offeringId, 
                                                                                          org.osid.id.Id timePeriodId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsForOfferingOnDate(offeringId, from, to)));
    }


    /**
     *  Gets a <code> ParticipantList </code> by genus type for an
     *  offering in a time period and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          timePeriodId, participantGenusType, from, </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOfferingOnDate(org.osid.id.Id offeringId, 
                                                                                                      org.osid.id.Id timePeriodId, 
                                                                                                      org.osid.type.Type participantGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsByGenusTypeForOfferingOnDate(offeringId, participantGenusType, from, to)));
    }


    /**
     *  Gets a list of participants corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new ResourceFilter(resourceId), getParticipants()));
    }


    /**
     *  Gets a list of participants by genus type corresponding to a
     *  resource <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param participantGenusType an resource genus type
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>participantGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsByGenusTypeForResource(org.osid.id.Id resourceId,
                                                                                    org.osid.type.Type participantGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new ResourceFilter(resourceId), getParticipantsByGenusType(participantGenusType)));
    }


    /**
     *  Gets a list of participants corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForResourceOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.TemporalParticipantFilterList(getParticipantsForResource(resourceId), from, to));
    }


    /**
     *  Gets a list of participants by genus type corresponding to a
     *  resource <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param participantGenusType a participant genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>,
     *          <code>participantGenusType</code>, <code>from</code>,
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId,
                                                                                         org.osid.type.Type participantGenusType,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.TemporalParticipantFilterList(getParticipantsByGenusTypeForResource(resourceId, participantGenusType), from, to));
    }


    /**
     *  Gets a <code> ParticipantList </code> for the given resource
     *  in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or 
     *          <code> timePeriodId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForResource(org.osid.id.Id resourceId, 
                                                                                    org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsForResource(resourceId)));
    }


    /**
     *  Gets a <code> ParticipantList </code> by genus type for the
     *  given resource in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          timePeriodId, </code> or <code> participantGenusType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForResource(org.osid.id.Id resourceId, 
                                                                                                org.osid.id.Id timePeriodId, 
                                                                                                org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsByGenusTypeForResource(resourceId, participantGenusType)));
    }



    /**
     *  Gets a <code> ParticipantList </code> for a resource in a time
     *  period and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          timePeriodId, from, </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                          org.osid.id.Id timePeriodId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsForResourceOnDate(resourceId, from, to)));
    }


    /**
     *  Gets a <code> ParticipantList </code> by genus type for a
     *  resource in a time period and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          timePeriodId, participantGenusType, from, </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                                      org.osid.id.Id timePeriodId, 
                                                                                                      org.osid.type.Type participantGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsByGenusTypeForResourceOnDate(resourceId, participantGenusType, from, to)));
    }


    /**
     *  Gets a list of participants corresponding to offering and
     *  resource <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingAndResource(org.osid.id.Id offeringId,
                                                                                   org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new ResourceFilter(resourceId), getParticipantsForOffering(offeringId)));
    }


    /**
     *  Gets a list of participants by genus type corresponding to
     *  offering and resource <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param participantGenusType an offering genus type
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>offeringId</code>, <code>resourceId</code>, or,
     *          <code>participantGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOfferingAndResource(org.osid.id.Id offeringId,
                                                                                              org.osid.id.Id resourceId,
                                                                                              org.osid.type.Type participantGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new ResourceFilter(resourceId), getParticipantsByGenusTypeForOffering(offeringId, participantGenusType)));
    }


    /**
     *  Gets a list of participants corresponding to offering and
     *  resource <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingAndResourceOnDate(org.osid.id.Id offeringId,
                                                                                         org.osid.id.Id resourceId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.TemporalParticipantFilterList(getParticipantsForOfferingAndResource(offeringId, resourceId), from, to));
    }


    /**
     *  Gets a list of participants by genus type corresponding to
     *  offering and resource <code>Ids</code> and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  participantGenusType an offering genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>offeringId</code>, <code>resourceId</code>,
     *          <code>participantGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOfferingAndResourceOnDate(org.osid.id.Id offeringId,
                                                                                                    org.osid.id.Id resourceId,
                                                                                                    org.osid.type.Type participantGenusType,
                                                                                                    org.osid.calendaring.DateTime from,
                                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.TemporalParticipantFilterList(getParticipantsByGenusTypeForOfferingAndResource(offeringId, resourceId, participantGenusType), from, to));
    }


    /**
     *  Gets a <code> ParticipantList </code> for the given offering
     *  and resource in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          resourceId, </code> or <code> timePeriodId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOfferingAndResource(org.osid.id.Id offeringId, 
                                                                                               org.osid.id.Id resourceId, 
                                                                                               org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsForOfferingAndResource(offeringId, resourceId)));
    }

    
    /**
     *  Gets a <code> ParticipantList </code> by genus type for the
     *  given offering and resource in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          resourceId, </code> <code> timePeriodId </code> or
     *          <code> participantGenusType </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOfferingAndResource(org.osid.id.Id offeringId, 
                                                                                                           org.osid.id.Id resourceId, 
                                                                                                           org.osid.id.Id timePeriodId, 
                                                                                                           org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsByGenusTypeForOfferingAndResource(offeringId, resourceId, participantGenusType)));
    }


    /**
     *  Gets a <code> ParticipantList </code> for an offering and
     *  resource in a time period and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          resourceId, timePeriodId, from, </code> or <code> to
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOfferingAndResourceOnDate(org.osid.id.Id offeringId, 
                                                                                                     org.osid.id.Id resourceId, 
                                                                                                     org.osid.id.Id timePeriodId, 
                                                                                                     org.osid.calendaring.DateTime from, 
                                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsForOfferingAndResourceOnDate(offeringId, resourceId, from, to)));
    }



    /**
     *  Gets a <code> ParticipantList </code> by genus type for an
     *  offering and resource in a time period and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          resourceId, timePeriodId,participantGenusType, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOfferingAndResourceOnDate(org.osid.id.Id offeringId, 
                                                                                                                 org.osid.id.Id resourceId, 
                                                                                                                 org.osid.id.Id timePeriodId, 
                                                                                                                 org.osid.type.Type participantGenusType, 
                                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilterList(new TimePeriodFilter(timePeriodId), getParticipantsByGenusTypeForOfferingAndResourceOnDate(offeringId, resourceId, participantGenusType, from, to)));
    }

    
    /**
     *  Gets all <code>Participants</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Participants</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.offering.ParticipantList getParticipants()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the participant list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of participants
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.offering.ParticipantList filterParticipantsOnViews(org.osid.offering.ParticipantList list)
        throws org.osid.OperationFailedException {

        org.osid.offering.ParticipantList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.offering.participant.EffectiveParticipantFilterList(ret);
        }

        return (ret);
    }

    public static class OfferingFilter
        implements net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilter {
         
        private final org.osid.id.Id offeringId;
         
         
        /**
         *  Constructs a new <code>OfferingFilter</code>.
         *
         *  @param offeringId the offering to filter
         *  @throws org.osid.NullArgumentException
         *          <code>offeringId</code> is <code>null</code>
         */
        
        public OfferingFilter(org.osid.id.Id offeringId) {
            nullarg(offeringId, "offering Id");
            this.offeringId = offeringId;
            return;
        }

         
        /**
         *  Used by the ParticipantFilterList to filter the 
         *  participant list based on offering.
         *
         *  @param participant the participant
         *  @return <code>true</code> to pass the participant,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.offering.Participant participant) {
            return (participant.getOfferingId().equals(this.offeringId));
        }
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ParticipantFilterList to filter the 
         *  participant list based on resource.
         *
         *  @param participant the participant
         *  @return <code>true</code> to pass the participant,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.offering.Participant participant) {
            return (participant.getResourceId().equals(this.resourceId));
        }
    }


    public static class TimePeriodFilter
        implements net.okapia.osid.jamocha.inline.filter.offering.participant.ParticipantFilter {
         
        private final org.osid.id.Id timePeriodId;
         
         
        /**
         *  Constructs a new <code>TimePeriodFilter</code>.
         *
         *  @param timePeriodId the time period to filter
         *  @throws org.osid.NullArgumentException
         *          <code>timePeriodId</code> is <code>null</code>
         */
        
        public TimePeriodFilter(org.osid.id.Id timePeriodId) {
            nullarg(timePeriodId, "time period Id");
            this.timePeriodId = timePeriodId;
            return;
        }

         
        /**
         *  Used by the ParticipantFilterList to filter the 
         *  participant list based on time period.
         *
         *  @param participant the participant
         *  @return <code>true</code> to pass the participant,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.offering.Participant participant) {
            return (participant.getTimePeriodId().equals(this.timePeriodId));
        }
    }
}
