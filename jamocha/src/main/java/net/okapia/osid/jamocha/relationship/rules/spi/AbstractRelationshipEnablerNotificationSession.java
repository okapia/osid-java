//
// AbstractRelationshipEnablerNotificationSession.java
//
//     A template for making RelationshipEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code RelationshipEnabler} objects. This session
 *  is intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code RelationshipEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for relationship enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRelationshipEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.relationship.rules.RelationshipEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.relationship.Family family = new net.okapia.osid.jamocha.nil.relationship.family.UnknownFamily();


    /**
     *  Gets the {@code Family} {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Family Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.family.getId());
    }

    
    /**
     *  Gets the {@code Family} associated with this session.
     *
     *  @return the {@code Family} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.family);
    }


    /**
     *  Sets the {@code Family}.
     *
     *  @param family the family for this session
     *  @throws org.osid.NullArgumentException {@code family}
     *          is {@code null}
     */

    protected void setFamily(org.osid.relationship.Family family) {
        nullarg(family, "family");
        this.family = family;
        return;
    }


    /**
     *  Tests if this user can register for {@code
     *  RelationshipEnabler} notifications.  A return of true does not
     *  guarantee successful authorization. A return of false
     *  indicates that it is known all methods in this session will
     *  result in a {@code PERMISSION_DENIED}. This is intended as a
     *  hint to an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRelationshipEnablerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeRelationshipEnablerNotification() </code>.
     */

    @OSID @Override
    public void reliableRelationshipEnablerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode, 
     *  notifications do not need to be acknowledged. 
     */

    @OSID @Override
    public void unreliableRelationshipEnablerNotifications() {
        return;
    }


    /**
     *  Acknowledge a relationship enabler notification. 
     *
     *  @param  notificationId the <code> Id </code> of the notification 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void acknowledgeRelationshipEnablerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationships in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this family only.
     */

    @OSID @Override
    public void useIsolatedFamilyView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new relationship
     *  enablers. {@code
     *  RelationshipEnablerReceiver.newRelationshipEnabler()} is
     *  invoked when a new {@code RelationshipEnabler} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRelationshipEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated relationship
     *  enablers. {@code
     *  RelationshipEnablerReceiver.changedRelationshipEnabler()} is
     *  invoked when a relationship enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRelationshipEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated relationship
     *  enabler. {@code
     *  RelationshipEnablerReceiver.changedRelationshipEnabler()} is
     *  invoked when the specified relationship enabler is changed.
     *
     *  @param relationshipEnablerId the {@code Id} of the {@code RelationshipEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code relationshipEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRelationshipEnabler(org.osid.id.Id relationshipEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted relationship
     *  enablers. {@code
     *  RelationshipEnablerReceiver.deletedRelationshipEnabler()} is
     *  invoked when a relationship enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRelationshipEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted relationship
     *  enabler. {@code
     *  RelationshipEnablerReceiver.deletedRelationshipEnabler()} is
     *  invoked when the specified relationship enabler is deleted.
     *
     *  @param relationshipEnablerId the {@code Id} of the
     *          {@code RelationshipEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code relationshipEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRelationshipEnabler(org.osid.id.Id relationshipEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
