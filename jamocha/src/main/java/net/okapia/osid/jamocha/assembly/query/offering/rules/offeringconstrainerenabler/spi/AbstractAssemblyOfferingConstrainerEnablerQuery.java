//
// AbstractAssemblyOfferingConstrainerEnablerQuery.java
//
//     An OfferingConstrainerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.offering.rules.offeringconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OfferingConstrainerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyOfferingConstrainerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.offering.rules.OfferingConstrainerEnablerQuery,
               org.osid.offering.rules.OfferingConstrainerEnablerQueryInspector,
               org.osid.offering.rules.OfferingConstrainerEnablerSearchOrder {

    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyOfferingConstrainerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOfferingConstrainerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the offering constrainer. 
     *
     *  @param  offeringConstrainerId the offering constrainer <code> Id 
     *          </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> offeringConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledOfferingConstrainerId(org.osid.id.Id offeringConstrainerId, 
                                                boolean match) {
        getAssembler().addIdTerm(getRuledOfferingConstrainerIdColumn(), offeringConstrainerId, match);
        return;
    }


    /**
     *  Clears the offering constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledOfferingConstrainerIdTerms() {
        getAssembler().clearTerms(getRuledOfferingConstrainerIdColumn());
        return;
    }


    /**
     *  Gets the offering constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledOfferingConstrainerIdTerms() {
        return (getAssembler().getIdTerms(getRuledOfferingConstrainerIdColumn()));
    }


    /**
     *  Gets the RuledOfferingConstrainerId column name.
     *
     * @return the column name
     */

    protected String getRuledOfferingConstrainerIdColumn() {
        return ("ruled_offering_constrainer_id");
    }


    /**
     *  Tests if an <code> OfferingConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if an offering constrainer query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledOfferingConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offering constrainer. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the offering constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledOfferingConstrainerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQuery getRuledOfferingConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledOfferingConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any offering constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any offering 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no offering constrainers 
     */

    @OSID @Override
    public void matchAnyRuledOfferingConstrainer(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledOfferingConstrainerColumn(), match);
        return;
    }


    /**
     *  Clears the offering constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledOfferingConstrainerTerms() {
        getAssembler().clearTerms(getRuledOfferingConstrainerColumn());
        return;
    }


    /**
     *  Gets the offering constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQueryInspector[] getRuledOfferingConstrainerTerms() {
        return (new org.osid.offering.rules.OfferingConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the RuledOfferingConstrainer column name.
     *
     * @return the column name
     */

    protected String getRuledOfferingConstrainerColumn() {
        return ("ruled_offering_constrainer");
    }


    /**
     *  Matches enablers mapped to the catalogue. 
     *
     *  @param  catalogueId the catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        getAssembler().addIdTerm(getCatalogueIdColumn(), catalogueId, match);
        return;
    }


    /**
     *  Clears the catalogue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        getAssembler().clearTerms(getCatalogueIdColumn());
        return;
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (getAssembler().getIdTerms(getCatalogueIdColumn()));
    }


    /**
     *  Gets the CatalogueId column name.
     *
     * @return the column name
     */

    protected String getCatalogueIdColumn() {
        return ("catalogue_id");
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears the catalogue query terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        getAssembler().clearTerms(getCatalogueColumn());
        return;
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the Catalogue column name.
     *
     * @return the column name
     */

    protected String getCatalogueColumn() {
        return ("catalogue");
    }


    /**
     *  Tests if this offeringConstrainerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  offeringConstrainerEnablerRecordType an offering constrainer enabler record type 
     *  @return <code>true</code> if the offeringConstrainerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type offeringConstrainerEnablerRecordType) {
        for (org.osid.offering.rules.records.OfferingConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(offeringConstrainerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  offeringConstrainerEnablerRecordType the offering constrainer enabler record type 
     *  @return the offering constrainer enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerEnablerQueryRecord getOfferingConstrainerEnablerQueryRecord(org.osid.type.Type offeringConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(offeringConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  offeringConstrainerEnablerRecordType the offering constrainer enabler record type 
     *  @return the offering constrainer enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerEnablerQueryInspectorRecord getOfferingConstrainerEnablerQueryInspectorRecord(org.osid.type.Type offeringConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(offeringConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param offeringConstrainerEnablerRecordType the offering constrainer enabler record type
     *  @return the offering constrainer enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerEnablerSearchOrderRecord getOfferingConstrainerEnablerSearchOrderRecord(org.osid.type.Type offeringConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(offeringConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this offering constrainer enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param offeringConstrainerEnablerQueryRecord the offering constrainer enabler query record
     *  @param offeringConstrainerEnablerQueryInspectorRecord the offering constrainer enabler query inspector
     *         record
     *  @param offeringConstrainerEnablerSearchOrderRecord the offering constrainer enabler search order record
     *  @param offeringConstrainerEnablerRecordType offering constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerQueryRecord</code>,
     *          <code>offeringConstrainerEnablerQueryInspectorRecord</code>,
     *          <code>offeringConstrainerEnablerSearchOrderRecord</code> or
     *          <code>offeringConstrainerEnablerRecordTypeofferingConstrainerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addOfferingConstrainerEnablerRecords(org.osid.offering.rules.records.OfferingConstrainerEnablerQueryRecord offeringConstrainerEnablerQueryRecord, 
                                      org.osid.offering.rules.records.OfferingConstrainerEnablerQueryInspectorRecord offeringConstrainerEnablerQueryInspectorRecord, 
                                      org.osid.offering.rules.records.OfferingConstrainerEnablerSearchOrderRecord offeringConstrainerEnablerSearchOrderRecord, 
                                      org.osid.type.Type offeringConstrainerEnablerRecordType) {

        addRecordType(offeringConstrainerEnablerRecordType);

        nullarg(offeringConstrainerEnablerQueryRecord, "offering constrainer enabler query record");
        nullarg(offeringConstrainerEnablerQueryInspectorRecord, "offering constrainer enabler query inspector record");
        nullarg(offeringConstrainerEnablerSearchOrderRecord, "offering constrainer enabler search odrer record");

        this.queryRecords.add(offeringConstrainerEnablerQueryRecord);
        this.queryInspectorRecords.add(offeringConstrainerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(offeringConstrainerEnablerSearchOrderRecord);
        
        return;
    }
}
