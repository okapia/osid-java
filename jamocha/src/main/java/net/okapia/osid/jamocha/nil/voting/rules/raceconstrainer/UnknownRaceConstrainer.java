//
// UnknownRaceConstrainer.java
//
//     Defines an unknown RaceConstrainer.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.voting.rules.raceconstrainer;


/**
 *  Defines an unknown <code>RaceConstrainer</code>.
 */

public final class UnknownRaceConstrainer
    extends net.okapia.osid.jamocha.nil.voting.rules.raceconstrainer.spi.AbstractUnknownRaceConstrainer
    implements org.osid.voting.rules.RaceConstrainer {


    /**
     *  Constructs a new <code>UnknownRaceConstrainer</code>.
     */

    public UnknownRaceConstrainer() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownRaceConstrainer</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownRaceConstrainer(boolean optional) {
        super(optional);
        addRaceConstrainerRecord(new RaceConstrainerRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown RaceConstrainer.
     *
     *  @return an unknown RaceConstrainer
     */

    public static org.osid.voting.rules.RaceConstrainer create() {
        return (net.okapia.osid.jamocha.builder.validator.voting.rules.raceconstrainer.RaceConstrainerValidator.validateRaceConstrainer(new UnknownRaceConstrainer()));
    }


    public class RaceConstrainerRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.voting.rules.records.RaceConstrainerRecord {

        
        protected RaceConstrainerRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
