//
// AbstractJournalEntry.java
//
//     Defines a JournalEntry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.journaling.journalentry.spi;


/**
 *  Defines a <code>JournalEntry</code> builder.
 */

public abstract class AbstractJournalEntryBuilder<T extends AbstractJournalEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.journaling.journalentry.JournalEntryMiter journalEntry;


    /**
     *  Constructs a new <code>AbstractJournalEntryBuilder</code>.
     *
     *  @param journalEntry the journal entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractJournalEntryBuilder(net.okapia.osid.jamocha.builder.journaling.journalentry.JournalEntryMiter journalEntry) {
        super(journalEntry);
        this.journalEntry = journalEntry;
        return;
    }


    /**
     *  Builds the journal entry.
     *
     *  @return the new journal entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.journaling.JournalEntry build() {
        (new net.okapia.osid.jamocha.builder.validator.journaling.journalentry.JournalEntryValidator(getValidations())).validate(this.journalEntry);
        return (new net.okapia.osid.jamocha.builder.journaling.journalentry.ImmutableJournalEntry(this.journalEntry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the journalEntry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.journaling.journalentry.JournalEntryMiter getMiter() {
        return (this.journalEntry);
    }


    /**
     *  Sets the branch.
     *
     *  @param branch a branch
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>branch</code> is
     *          <code>null</code>
     */

    public T branch(org.osid.journaling.Branch branch) {
        getMiter().setBranch(branch);
        return (self());
    }


    /**
     *  Sets the source id.
     *
     *  @param sourceId a source id
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sourceId</code>
     *          is <code>null</code>
     */

    public T sourceId(org.osid.id.Id sourceId) {
        getMiter().setSourceId(sourceId);
        return (self());
    }


    /**
     *  Sets the version id.
     *
     *  @param versionId a version id
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>versionId</code>
     *          is <code>null</code>
     */

    public T versionId(org.osid.id.Id versionId) {
        getMiter().setVersionId(versionId);
        return (self());
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    public T timestamp(org.osid.calendaring.DateTime timestamp) {
        getMiter().setTimestamp(timestamp);
        return (self());
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setAgent(agent);
        return (self());
    }


    /**
     *  Adds a JournalEntry record.
     *
     *  @param record a journal entry record
     *  @param recordType the type of journal entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.journaling.records.JournalEntryRecord record, org.osid.type.Type recordType) {
        getMiter().addJournalEntryRecord(record, recordType);
        return (self());
    }
}       


