//
// InvariantMapProxyAwardLookupSession
//
//    Implements an Award lookup service backed by a fixed
//    collection of awards. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition;


/**
 *  Implements an Award lookup service backed by a fixed
 *  collection of awards. The awards are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAwardLookupSession
    extends net.okapia.osid.jamocha.core.recognition.spi.AbstractMapAwardLookupSession
    implements org.osid.recognition.AwardLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAwardLookupSession} with no
     *  awards.
     *
     *  @param academy the academy
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAwardLookupSession(org.osid.recognition.Academy academy,
                                                  org.osid.proxy.Proxy proxy) {
        setAcademy(academy);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAwardLookupSession} with a single
     *  award.
     *
     *  @param academy the academy
     *  @param award an single award
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code award} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAwardLookupSession(org.osid.recognition.Academy academy,
                                                  org.osid.recognition.Award award, org.osid.proxy.Proxy proxy) {

        this(academy, proxy);
        putAward(award);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAwardLookupSession} using
     *  an array of awards.
     *
     *  @param academy the academy
     *  @param awards an array of awards
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code awards} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAwardLookupSession(org.osid.recognition.Academy academy,
                                                  org.osid.recognition.Award[] awards, org.osid.proxy.Proxy proxy) {

        this(academy, proxy);
        putAwards(awards);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAwardLookupSession} using a
     *  collection of awards.
     *
     *  @param academy the academy
     *  @param awards a collection of awards
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code awards} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAwardLookupSession(org.osid.recognition.Academy academy,
                                                  java.util.Collection<? extends org.osid.recognition.Award> awards,
                                                  org.osid.proxy.Proxy proxy) {

        this(academy, proxy);
        putAwards(awards);
        return;
    }
}
