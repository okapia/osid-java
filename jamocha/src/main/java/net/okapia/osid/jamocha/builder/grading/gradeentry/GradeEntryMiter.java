//
// GradeEntryMiter.java
//
//     Defines a GradeEntry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradeentry;


/**
 *  Defines a <code>GradeEntry</code> miter for use with the builders.
 */

public interface GradeEntryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.grading.GradeEntry {


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    public void setGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn);


    /**
     *  Sets the key resource.
     *
     *  @param keyResource a key resource
     *  @throws org.osid.NullArgumentException
     *          <code>keyResource</code> is <code>null</code>
     */

    public void setKeyResource(org.osid.resource.Resource keyResource);


    /**
     *  Sets the overridden calculated entry.
     *
     *  @param overriddenCalculatedEntry an overridden calculated entry
     *  @throws org.osid.NullArgumentException
     *          <code>overriddenCalculatedEntry</code> is <code>null</code>
     */

    public void setOverriddenCalculatedEntry(org.osid.grading.GradeEntry overriddenCalculatedEntry);


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException
     *          <code>grade</code> is <code>null</code>
     */

    public void setGrade(org.osid.grading.Grade grade);


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @throws org.osid.NullArgumentException
     *          <code>score</code> is <code>null</code>
     */

    public void setScore(java.math.BigDecimal score);


    /**
     *  Sets the time graded.
     *
     *  @param timeGraded a time graded
     *  @throws org.osid.NullArgumentException
     *          <code>timeGraded</code> is <code>null</code>
     */

    public void setTimeGraded(org.osid.calendaring.DateTime timeGraded);


    /**
     *  Sets the grader.
     *
     *  @param grader a grader
     *  @throws org.osid.NullArgumentException
     *          <code>grader</code> is <code>null</code>
     */

    public void setGrader(org.osid.resource.Resource grader);


    /**
     *  Sets the grading agent.
     *
     *  @param gradingAgent a grading agent
     *  @throws org.osid.NullArgumentException
     *          <code>gradingAgent</code> is <code>null</code>
     */

    public void setGradingAgent(org.osid.authentication.Agent gradingAgent);


    /**
     *  Adds a GradeEntry record.
     *
     *  @param record a gradeEntry record
     *  @param recordType the type of gradeEntry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addGradeEntryRecord(org.osid.grading.records.GradeEntryRecord record, org.osid.type.Type recordType);
}       


