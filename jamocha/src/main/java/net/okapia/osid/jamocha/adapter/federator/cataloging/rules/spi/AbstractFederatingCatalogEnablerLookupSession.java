//
// AbstractFederatingCatalogEnablerLookupSession.java
//
//     An abstract federating adapter for a CatalogEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.cataloging.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CatalogEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCatalogEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.cataloging.rules.CatalogEnablerLookupSession>
    implements org.osid.cataloging.rules.CatalogEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.cataloging.Catalog catalog = new net.okapia.osid.jamocha.nil.cataloging.catalog.UnknownCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingCatalogEnablerLookupSession</code>.
     */

    protected AbstractFederatingCatalogEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.cataloging.rules.CatalogEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Catalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogId() {
        return (this.catalog.getId());
    }


    /**
     *  Gets the <code>Catalog</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.Catalog getCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalog);
    }


    /**
     *  Sets the <code>Catalog</code>.
     *
     *  @param  catalog the catalog for this session
     *  @throws org.osid.NullArgumentException <code>catalog</code>
     *          is <code>null</code>
     */

    protected void setCatalog(org.osid.cataloging.Catalog catalog) {
        nullarg(catalog, "catalog");
        this.catalog = catalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>CatalogEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCatalogEnablers() {
        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            if (session.canLookupCatalogEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>CatalogEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCatalogEnablerView() {
        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            session.useComparativeCatalogEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>CatalogEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCatalogEnablerView() {
        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            session.usePlenaryCatalogEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include catalog enablers in catalogs which are children
     *  of this catalog in the catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogView() {
        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            session.useFederatedCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalog only.
     */

    @OSID @Override
    public void useIsolatedCatalogView() {
        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            session.useIsolatedCatalogView();
        }

        return;
    }


    /**
     *  Only active catalog enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCatalogEnablerView() {
        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            session.useActiveCatalogEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive catalog enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCatalogEnablerView() {
        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            session.useAnyStatusCatalogEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>CatalogEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CatalogEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CatalogEnabler</code> and retained for compatibility.
     *
     *  In active mode, catalog enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  catalog enablers are returned.
     *
     *  @param  catalogEnablerId <code>Id</code> of the
     *          <code>CatalogEnabler</code>
     *  @return the catalog enabler
     *  @throws org.osid.NotFoundException <code>catalogEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>catalogEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnabler getCatalogEnabler(org.osid.id.Id catalogEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            try {
                return (session.getCatalogEnabler(catalogEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(catalogEnablerId + " not found");
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  catalogEnablers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>CatalogEnablers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, catalog enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  catalog enablers are returned.
     *
     *  @param  catalogEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByIds(org.osid.id.IdList catalogEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.cataloging.rules.catalogenabler.MutableCatalogEnablerList ret = new net.okapia.osid.jamocha.cataloging.rules.catalogenabler.MutableCatalogEnablerList();

        try (org.osid.id.IdList ids = catalogEnablerIds) {
            while (ids.hasNext()) {
                ret.addCatalogEnabler(getCatalogEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> corresponding to the given
     *  catalog enabler genus <code>Type</code> which does not include
     *  catalog enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known catalog
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those catalog enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, catalog enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  catalog enablers are returned.
     *
     *  @param  catalogEnablerGenusType a catalogEnabler genus type 
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByGenusType(org.osid.type.Type catalogEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.cataloging.rules.catalogenabler.FederatingCatalogEnablerList ret = getCatalogEnablerList();

        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            ret.addCatalogEnablerList(session.getCatalogEnablersByGenusType(catalogEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> corresponding to the
     *  given catalog enabler genus <code>Type</code> and include any
     *  additional catalog enablers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known catalog
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those catalog enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, catalog enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  catalog enablers are returned.
     *
     *  @param  catalogEnablerGenusType a catalogEnabler genus type 
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByParentGenusType(org.osid.type.Type catalogEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.cataloging.rules.catalogenabler.FederatingCatalogEnablerList ret = getCatalogEnablerList();

        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            ret.addCatalogEnablerList(session.getCatalogEnablersByParentGenusType(catalogEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> containing the given
     *  catalog enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known catalog
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those catalog enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, catalog enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  catalog enablers are returned.
     *
     *  @param  catalogEnablerRecordType a catalogEnabler record type 
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByRecordType(org.osid.type.Type catalogEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.cataloging.rules.catalogenabler.FederatingCatalogEnablerList ret = getCatalogEnablerList();

        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            ret.addCatalogEnablerList(session.getCatalogEnablersByRecordType(catalogEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known catalog
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those catalog enablers that are accessible
     *  through this session.
     *  
     *  In active mode, catalog enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  catalog enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CatalogEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.cataloging.rules.catalogenabler.FederatingCatalogEnablerList ret = getCatalogEnablerList();

        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            ret.addCatalogEnablerList(session.getCatalogEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>CatalogEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined to
     *  the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known catalog
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those catalog enablers that are accessible
     *  through this session.
     *
     *  In active mode, catalog enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  catalog enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.cataloging.rules.catalogenabler.FederatingCatalogEnablerList ret = getCatalogEnablerList();

        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            ret.addCatalogEnablerList(session.getCatalogEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>CatalogEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known catalog
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those catalog enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, catalog enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  catalog enablers are returned.
     *
     *  @return a list of <code>CatalogEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.cataloging.rules.catalogenabler.FederatingCatalogEnablerList ret = getCatalogEnablerList();

        for (org.osid.cataloging.rules.CatalogEnablerLookupSession session : getSessions()) {
            ret.addCatalogEnablerList(session.getCatalogEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.cataloging.rules.catalogenabler.FederatingCatalogEnablerList getCatalogEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.cataloging.rules.catalogenabler.ParallelCatalogEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.cataloging.rules.catalogenabler.CompositeCatalogEnablerList());
        }
    }
}
