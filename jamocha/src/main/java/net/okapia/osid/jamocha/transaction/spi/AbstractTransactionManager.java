//
// AbstractTransactionManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transaction.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTransactionManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.transaction.TransactionManager,
               org.osid.transaction.TransactionProxyManager {


    /**
     *  Constructs a new <code>AbstractTransactionManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTransactionManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if transactions are supported. 
     *
     *  @return <code> true </code> if transactions are supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransactions() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the transaction 
     *  service. 
     *
     *  @return a transaction session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnimplementedException <code> supportsTransactions() 
     *          </code> is false 
     */

    @OSID @Override
    public org.osid.transaction.TransactionSession getTransactionSession()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.UnimplementedException("org.osid.transaction.TransactionManager.getTransactionSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the transaction 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a transaction session 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnimplementedException <code> supportsTransactions() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transaction.TransactionSession getTransactionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.UnimplementedException("org.osid.transaction.TransactionProxyManager.getTransactionSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
