//
// AbstractImmutablePostEntry.java
//
//     Wraps a mutable PostEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.posting.postentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>PostEntry</code> to hide modifiers. This
 *  wrapper provides an immutized PostEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying postEntry whose state changes are visible.
 */

public abstract class AbstractImmutablePostEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.financials.posting.PostEntry {

    private final org.osid.financials.posting.PostEntry postEntry;


    /**
     *  Constructs a new <code>AbstractImmutablePostEntry</code>.
     *
     *  @param postEntry the post entry to immutablize
     *  @throws org.osid.NullArgumentException <code>postEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePostEntry(org.osid.financials.posting.PostEntry postEntry) {
        super(postEntry);
        this.postEntry = postEntry;
        return;
    }


    /**
     *  Gets the post <code> Id </code> to which this entry belongs. 
     *
     *  @return the payer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPostId() {
        return (this.postEntry.getPostId());
    }


    /**
     *  Gets the post to which this entry belongs. 
     *
     *  @return the post 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.posting.Post getPost()
        throws org.osid.OperationFailedException {

        return (this.postEntry.getPost());
    }


    /**
     *  Gets the G/L account <code> Id </code> to which this entry applies. 
     *
     *  @return the account <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAccountId() {
        return (this.postEntry.getAccountId());
    }


    /**
     *  Gets the G/L account to which this entry applies. 
     *
     *  @return the account 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount()
        throws org.osid.OperationFailedException {

        return (this.postEntry.getAccount());
    }


    /**
     *  Gets the financial activity <code> Id </code> to which this entry 
     *  applies. 
     *
     *  @return the activity <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        return (this.postEntry.getActivityId());
    }


    /**
     *  Gets the financial activity to which this entry applies. 
     *
     *  @return the activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Activity getActivity()
        throws org.osid.OperationFailedException {

        return (this.postEntry.getActivity());
    }


    /**
     *  Gets the amount. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.postEntry.getAmount());
    }


    /**
     *  Tests if the amount is a debit or a credit. 
     *
     *  @return <code> true </code> if this entry amount is a debit, <code> 
     *          false </code> if it is a credit 
     */

    @OSID @Override
    public boolean isDebit() {
        return (this.postEntry.isDebit());
    }


    /**
     *  Gets the post entry record corresponding to the given <code> PostEntry 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  postEntryRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(postEntryRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  postEntryRecordType the type of post entry record to retrieve 
     *  @return the post entry record 
     *  @throws org.osid.NullArgumentException <code> postEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(postEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostEntryRecord getPostEntryRecord(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.postEntry.getPostEntryRecord(postEntryRecordType));
    }
}

