//
// AbstractSpeedZoneSearchOdrer.java
//
//     Defines a SpeedZoneSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.speedzone.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code SpeedZoneSearchOrder}.
 */

public abstract class AbstractSpeedZoneSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.mapping.path.SpeedZoneSearchOrder {

    private final java.util.Collection<org.osid.mapping.path.records.SpeedZoneSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPath(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a path search order is available. 
     *
     *  @return <code> true </code> if a path search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearchOrder() {
        return (false);
    }


    /**
     *  Gets a path search order. 
     *
     *  @return the path search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchOrder getPathSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPathSearchOrder() is false");
    }


    /**
     *  Orders the results by the length of the zone. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLength(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the results by speed limit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySpeedLimit(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  speedZoneRecordType a speed zone record type 
     *  @return {@code true} if the speedZoneRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type speedZoneRecordType) {
        for (org.osid.mapping.path.records.SpeedZoneSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  speedZoneRecordType the speed zone record type 
     *  @return the speed zone search order record
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(speedZoneRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneSearchOrderRecord getSpeedZoneSearchOrderRecord(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SpeedZoneSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this speed zone. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param speedZoneRecord the speed zone search odrer record
     *  @param speedZoneRecordType speed zone record type
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneRecord} or
     *          {@code speedZoneRecordTypespeedZone} is
     *          {@code null}
     */
            
    protected void addSpeedZoneRecord(org.osid.mapping.path.records.SpeedZoneSearchOrderRecord speedZoneSearchOrderRecord, 
                                     org.osid.type.Type speedZoneRecordType) {

        addRecordType(speedZoneRecordType);
        this.records.add(speedZoneSearchOrderRecord);
        
        return;
    }
}
