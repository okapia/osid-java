//
// AbstractInquirySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractInquirySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inquiry.InquirySearchResults {

    private org.osid.inquiry.InquiryList inquiries;
    private final org.osid.inquiry.InquiryQueryInspector inspector;
    private final java.util.Collection<org.osid.inquiry.records.InquirySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractInquirySearchResults.
     *
     *  @param inquiries the result set
     *  @param inquiryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>inquiries</code>
     *          or <code>inquiryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractInquirySearchResults(org.osid.inquiry.InquiryList inquiries,
                                            org.osid.inquiry.InquiryQueryInspector inquiryQueryInspector) {
        nullarg(inquiries, "inquiries");
        nullarg(inquiryQueryInspector, "inquiry query inspectpr");

        this.inquiries = inquiries;
        this.inspector = inquiryQueryInspector;

        return;
    }


    /**
     *  Gets the inquiry list resulting from a search.
     *
     *  @return an inquiry list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiries() {
        if (this.inquiries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inquiry.InquiryList inquiries = this.inquiries;
        this.inquiries = null;
	return (inquiries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inquiry.InquiryQueryInspector getInquiryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  inquiry search record <code> Type. </code> This method must
     *  be used to retrieve an inquiry implementing the requested
     *  record.
     *
     *  @param inquirySearchRecordType an inquiry search 
     *         record type 
     *  @return the inquiry search
     *  @throws org.osid.NullArgumentException
     *          <code>inquirySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(inquirySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquirySearchResultsRecord getInquirySearchResultsRecord(org.osid.type.Type inquirySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inquiry.records.InquirySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(inquirySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(inquirySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record inquiry search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addInquiryRecord(org.osid.inquiry.records.InquirySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "inquiry record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
