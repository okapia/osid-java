//
// AbstractFederatingRecipeLookupSession.java
//
//     An abstract federating adapter for a RecipeLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RecipeLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRecipeLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.recipe.RecipeLookupSession>
    implements org.osid.recipe.RecipeLookupSession {

    private boolean parallel = false;
    private org.osid.recipe.Cookbook cookbook = new net.okapia.osid.jamocha.nil.recipe.cookbook.UnknownCookbook();


    /**
     *  Constructs a new <code>AbstractFederatingRecipeLookupSession</code>.
     */

    protected AbstractFederatingRecipeLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.recipe.RecipeLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Cookbook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Cookbook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.cookbook.getId());
    }


    /**
     *  Gets the <code>Cookbook</code> associated with this 
     *  session.
     *
     *  @return the <code>Cookbook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.cookbook);
    }


    /**
     *  Sets the <code>Cookbook</code>.
     *
     *  @param  cookbook the cookbook for this session
     *  @throws org.osid.NullArgumentException <code>cookbook</code>
     *          is <code>null</code>
     */

    protected void setCookbook(org.osid.recipe.Cookbook cookbook) {
        nullarg(cookbook, "cookbook");
        this.cookbook = cookbook;
        return;
    }


    /**
     *  Tests if this user can perform <code>Recipe</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRecipes() {
        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            if (session.canLookupRecipes()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Recipe</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRecipeView() {
        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            session.useComparativeRecipeView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Recipe</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRecipeView() {
        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            session.usePlenaryRecipeView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include recipes in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            session.useFederatedCookbookView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this cookbook only.
     */

    @OSID @Override
    public void useIsolatedCookbookView() {
        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            session.useIsolatedCookbookView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Recipe</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Recipe</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Recipe</code> and
     *  retained for compatibility.
     *
     *  @param  recipeId <code>Id</code> of the
     *          <code>Recipe</code>
     *  @return the recipe
     *  @throws org.osid.NotFoundException <code>recipeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>recipeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Recipe getRecipe(org.osid.id.Id recipeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            try {
                return (session.getRecipe(recipeId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(recipeId + " not found");
    }


    /**
     *  Gets a <code>RecipeList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  recipes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Recipes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  recipeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Recipe</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>recipeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByIds(org.osid.id.IdList recipeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.recipe.recipe.MutableRecipeList ret = new net.okapia.osid.jamocha.recipe.recipe.MutableRecipeList();

        try (org.osid.id.IdList ids = recipeIds) {
            while (ids.hasNext()) {
                ret.addRecipe(getRecipe(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RecipeList</code> corresponding to the given
     *  recipe genus <code>Type</code> which does not include
     *  recipes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recipeGenusType a recipe genus type 
     *  @return the returned <code>Recipe</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recipeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByGenusType(org.osid.type.Type recipeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recipe.recipe.FederatingRecipeList ret = getRecipeList();

        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            ret.addRecipeList(session.getRecipesByGenusType(recipeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RecipeList</code> corresponding to the given
     *  recipe genus <code>Type</code> and include any additional
     *  recipes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recipeGenusType a recipe genus type 
     *  @return the returned <code>Recipe</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recipeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByParentGenusType(org.osid.type.Type recipeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recipe.recipe.FederatingRecipeList ret = getRecipeList();

        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            ret.addRecipeList(session.getRecipesByParentGenusType(recipeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RecipeList</code> containing the given
     *  recipe record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recipeRecordType a recipe record type 
     *  @return the returned <code>Recipe</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByRecordType(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recipe.recipe.FederatingRecipeList ret = getRecipeList();

        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            ret.addRecipeList(session.getRecipesByRecordType(recipeRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RecipeList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known recipes or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  recipes that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Recipe</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.recipe.recipe.FederatingRecipeList ret = getRecipeList();

        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            ret.addRecipeList(session.getRecipesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Recipes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Recipes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recipe.recipe.FederatingRecipeList ret = getRecipeList();

        for (org.osid.recipe.RecipeLookupSession session : getSessions()) {
            ret.addRecipeList(session.getRecipes());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.recipe.recipe.FederatingRecipeList getRecipeList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.recipe.recipe.ParallelRecipeList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.recipe.recipe.CompositeRecipeList());
        }
    }
}
