//
// MutableIndexedMapProxyRegistrationLookupSession
//
//    Implements a Registration lookup service backed by a collection of
//    registrations indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration;


/**
 *  Implements a Registration lookup service backed by a collection of
 *  registrations. The registrations are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some registrations may be compatible
 *  with more types than are indicated through these registration
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of registrations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyRegistrationLookupSession
    extends net.okapia.osid.jamocha.core.course.registration.spi.AbstractIndexedMapRegistrationLookupSession
    implements org.osid.course.registration.RegistrationLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRegistrationLookupSession} with
     *  no registration.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRegistrationLookupSession} with
     *  a single registration.
     *
     *  @param courseCatalog the course catalog
     *  @param  registration an registration
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code registration}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.registration.Registration registration, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putRegistration(registration);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRegistrationLookupSession} using
     *  an array of registrations.
     *
     *  @param courseCatalog the course catalog
     *  @param  registrations an array of registrations
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code registrations}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.registration.Registration[] registrations, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putRegistrations(registrations);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRegistrationLookupSession} using
     *  a collection of registrations.
     *
     *  @param courseCatalog the course catalog
     *  @param  registrations a collection of registrations
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code registrations}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       java.util.Collection<? extends org.osid.course.registration.Registration> registrations,
                                                       org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putRegistrations(registrations);
        return;
    }

    
    /**
     *  Makes a {@code Registration} available in this session.
     *
     *  @param  registration a registration
     *  @throws org.osid.NullArgumentException {@code registration{@code 
     *          is {@code null}
     */

    @Override
    public void putRegistration(org.osid.course.registration.Registration registration) {
        super.putRegistration(registration);
        return;
    }


    /**
     *  Makes an array of registrations available in this session.
     *
     *  @param  registrations an array of registrations
     *  @throws org.osid.NullArgumentException {@code registrations{@code 
     *          is {@code null}
     */

    @Override
    public void putRegistrations(org.osid.course.registration.Registration[] registrations) {
        super.putRegistrations(registrations);
        return;
    }


    /**
     *  Makes collection of registrations available in this session.
     *
     *  @param  registrations a collection of registrations
     *  @throws org.osid.NullArgumentException {@code registration{@code 
     *          is {@code null}
     */

    @Override
    public void putRegistrations(java.util.Collection<? extends org.osid.course.registration.Registration> registrations) {
        super.putRegistrations(registrations);
        return;
    }


    /**
     *  Removes a Registration from this session.
     *
     *  @param registrationId the {@code Id} of the registration
     *  @throws org.osid.NullArgumentException {@code registrationId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRegistration(org.osid.id.Id registrationId) {
        super.removeRegistration(registrationId);
        return;
    }    
}
