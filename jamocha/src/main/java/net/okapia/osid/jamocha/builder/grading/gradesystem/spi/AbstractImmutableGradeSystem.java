//
// AbstractImmutableGradeSystem.java
//
//     Wraps a mutable GradeSystem to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradesystem.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>GradeSystem</code> to hide modifiers. This
 *  wrapper provides an immutized GradeSystem from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying gradeSystem whose state changes are visible.
 */

public abstract class AbstractImmutableGradeSystem
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.grading.GradeSystem {

    private final org.osid.grading.GradeSystem gradeSystem;


    /**
     *  Constructs a new <code>AbstractImmutableGradeSystem</code>.
     *
     *  @param gradeSystem the grade system to immutablize
     *  @throws org.osid.NullArgumentException <code>gradeSystem</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableGradeSystem(org.osid.grading.GradeSystem gradeSystem) {
        super(gradeSystem);
        this.gradeSystem = gradeSystem;
        return;
    }


    /**
     *  Tests if the grading system is based on grades. 
     *
     *  @return true if the grading system is based on grades, <code> false 
     *          </code> if the system is a numeric score 
     */

    @OSID @Override
    public boolean isBasedOnGrades() {
        return (this.gradeSystem.isBasedOnGrades());
    }


    /**
     *  Gets the grade <code> Ids </code> in this system ranked from
     *  highest to lowest.
     *
     *  @return the list of grades <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getGradeIds() {
        return (this.gradeSystem.getGradeIds());
    }


    /**
     *  Gets the grades in this system ranked from highest to lowest. 
     *
     *  @return the list of grades 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeList getGrades()
        throws org.osid.OperationFailedException {

        return (this.gradeSystem.getGrades());
    }


    /**
     *  Gets the lowest number in a numeric grading system. 
     *
     *  @return the lowest number 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getLowestNumericScore() {
        return (this.gradeSystem.getLowestNumericScore());
    }


    /**
     *  Gets the incremental step. 
     *
     *  @return the increment 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getNumericScoreIncrement() {
        return (this.gradeSystem.getNumericScoreIncrement());
    }


    /**
     *  Gets the highest number in a numeric grading system. 
     *
     *  @return the highest number 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getHighestNumericScore() {
        return (this.gradeSystem.getHighestNumericScore());
    }


    /**
     *  Gets the grade system record corresponding to the given <code> 
     *  GradeSystem </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  gradeSystemRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(gradeSystemRecordType) </code> is <code> true </code> . 
     *
     *  @param  gradeSystemRecordType the type of the record to retrieve 
     *  @return the grade system record 
     *  @throws org.osid.NullArgumentException <code> gradeSystemRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(gradeSystemRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemRecord getGradeSystemRecord(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException {

        return (this.gradeSystem.getGradeSystemRecord(gradeSystemRecordType));
    }
}

