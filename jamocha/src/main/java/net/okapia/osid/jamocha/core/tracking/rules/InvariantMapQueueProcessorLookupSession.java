//
// InvariantMapQueueProcessorLookupSession
//
//    Implements a QueueProcessor lookup service backed by a fixed collection of
//    queueProcessors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.rules;


/**
 *  Implements a QueueProcessor lookup service backed by a fixed
 *  collection of queue processors. The queue processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapQueueProcessorLookupSession
    extends net.okapia.osid.jamocha.core.tracking.rules.spi.AbstractMapQueueProcessorLookupSession
    implements org.osid.tracking.rules.QueueProcessorLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorLookupSession</code> with no
     *  queue processors.
     *  
     *  @param frontOffice the front office
     *  @throws org.osid.NullArgumnetException {@code frontOffice} is
     *          {@code null}
     */

    public InvariantMapQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice) {
        setFrontOffice(frontOffice);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorLookupSession</code> with a single
     *  queue processor.
     *  
     *  @param frontOffice the front office
     *  @param queueProcessor a single queue processor
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueProcessor} is <code>null</code>
     */

      public InvariantMapQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                               org.osid.tracking.rules.QueueProcessor queueProcessor) {
        this(frontOffice);
        putQueueProcessor(queueProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorLookupSession</code> using an array
     *  of queue processors.
     *  
     *  @param frontOffice the front office
     *  @param queueProcessors an array of queue processors
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueProcessors} is <code>null</code>
     */

      public InvariantMapQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                               org.osid.tracking.rules.QueueProcessor[] queueProcessors) {
        this(frontOffice);
        putQueueProcessors(queueProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorLookupSession</code> using a
     *  collection of queue processors.
     *
     *  @param frontOffice the front office
     *  @param queueProcessors a collection of queue processors
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueProcessors} is <code>null</code>
     */

      public InvariantMapQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                               java.util.Collection<? extends org.osid.tracking.rules.QueueProcessor> queueProcessors) {
        this(frontOffice);
        putQueueProcessors(queueProcessors);
        return;
    }
}
