//
// Validator.java
//
//     Validates metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


public class Validator {
    
    /**
     *  Validates an element against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    public void validate(Object element, org.osid.Metadata metadata) {
        nullarg(metadata, "metadata");
        nullarg(element, metadata.getElementLabel().getText());
        
        java.util.Collection<String> errors = new java.util.ArrayList<>(0);

        switch (metadata.getSyntax()) {
        case CARDINAL:
            if (metadata.isArray()) {
                errors.addAll(validateCardinalArray(element, metadata));
            } else {
                errors.addAll(validateCardinal(element, metadata));
            }

            break;
        case COORDINATE:
            if (metadata.isArray()) {
                errors.addAll(validateCoordinateArray(element, metadata));
            } else {
                errors.addAll(validateCoordinate(element, metadata));
            }            

            break;
        case CURRENCY:
            if (metadata.isArray()) {
                errors.addAll(validateCurrencyArray(element, metadata));
            } else {
                errors.addAll(validateCurrency(element, metadata));
            }

            break;
        case DATETIME:
            if (metadata.isArray()) {
                errors.addAll(validateDateTimeArray(element, metadata));
            } else {
                errors.addAll(validateDateTime(element, metadata));
            }

            break;
        case DECIMAL:
            if (metadata.isArray()) {
                errors.addAll(validateDecimalArray(element, metadata));
            } else {
                errors.addAll(validateDecimal(element, metadata));
            }

            break;
        case DISTANCE:
            if (metadata.isArray()) {
                errors.addAll(validateDistanceArray(element, metadata));
            } else {
                errors.addAll(validateDistance(element, metadata));
            }

            break;
        case DURATION:
            if (metadata.isArray()) {
                errors.addAll(validateDurationArray(element, metadata));
            } else {
                errors.addAll(validateDuration(element, metadata));
            }

            break;
        case HEADING:
            if (metadata.isArray()) {
                errors.addAll(validateHeadingArray(element, metadata));
            } else {
                errors.addAll(validateHeading(element, metadata));
            }

            break;
        case ID:
            if (metadata.isArray()) {
                errors.addAll(validateIdArray(element, metadata));
            } else {
                errors.addAll(validateId(element, metadata));
            }

            break;
        case INTEGER:
            if (metadata.isArray()) {
                errors.addAll(validateIntegerArray(element, metadata));
            } else {
                errors.addAll(validateInteger(element, metadata));
            }

            break;
        case OBJECT:
            if (metadata.isArray()) {
                errors.addAll(validateObjectArray(element, metadata));
            } else {
                errors.addAll(validateObject(element, metadata));
            }

            break;
        case SPATIALUNIT:
            if (metadata.isArray()) {
                errors.addAll(validateSpatialUnitArray(element, metadata));
            } else {
                errors.addAll(validateSpatialUnit(element, metadata));
            }

            break;
        case SPEED:
            if (metadata.isArray()) {
                errors.addAll(validateSpeedArray(element, metadata));
            } else {
                errors.addAll(validateSpeed(element, metadata));
            }

            break;
        case STRING:
            if (metadata.isArray()) {
                errors.addAll(validateStringArray(element, metadata));
            } else {
                errors.addAll(validateString(element, metadata));
            }

            break;
        case TIME:
            if (metadata.isArray()) {
                errors.addAll(validateTimeArray(element, metadata));
            } else {
                errors.addAll(validateTime(element, metadata));
            }

            break;
        case TYPE:
            if (metadata.isArray()) {
                errors.addAll(validateTypeArray(element, metadata));
            } else {
                errors.addAll(validateType(element, metadata));
            }

            break;
        case VERSION:
            if (metadata.isArray()) {
                errors.addAll(validateVersionArray(element, metadata));
            } else {
                errors.addAll(validateVersion(element, metadata));
            }

            break;
        default:
            throw new org.osid.UnsupportedException(metadata.getSyntax() + " not supported");
        }

        if (errors.size() == 0) {
            return;
        }

        if (errors.size() == 1) {
            for (String s : errors) {
                throw new org.osid.InvalidArgumentException(s);
            }
        }

        StringBuilder msg = new StringBuilder();
        msg.append(metadata.getElementLabel() + " is invalid:");
        int line = 1;
        for (String s : errors) {
            msg.append("\n    ");
            msg.append(line);
            msg.append(". ");
            msg.append(s);
        }

        throw new org.osid.InvalidArgumentException(msg.toString());
    }


    /**
     *  Validates a cardinal array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a cardinal array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateCardinalArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.CARDINAL) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a cardinal");
        }

        if (!(element instanceof Long[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a cardinal");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateCardinal(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a cardinal against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a cardinal
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateCardinal(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.CARDINAL) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a cardinal");
        }

        if (!(element instanceof Long)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a cardinal");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        long[] set = metadata.getCardinalSet();
        Long[] longs = new Long[set.length];
        for (int i = 0; i < set.length; i++) {
            longs[i] = set[i];
        }

        errors.addAll(validateCommon(element, metadata, longs));

        long c = (long) element;

        if (c < metadata.getMinimumCardinal()) {
            errors.add("cannot be less than " + metadata.getMinimumCardinal());
        }

        if (c > metadata.getMaximumCardinal()) {
            errors.add("cannot be greater than " + metadata.getMaximumCardinal());
        }

        return (errors);
    }


    /**
     *  Validates a coordinate array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a coordinate array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateCoordinateArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.COORDINATE) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a coordinate");
        }

        if (!(element instanceof org.osid.mapping.Coordinate[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a coordinate");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateCoordinate(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a coordinate against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a coordinate
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateCoordinate(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.COORDINATE) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a coordinate");
        }

        if (!(element instanceof org.osid.mapping.Coordinate)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a coordinate");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getCoordinateSet()));

        org.osid.mapping.Coordinate c = (org.osid.mapping.Coordinate) element;
        if (!metadata.supportsCoordinateType(c.getCoordinateType())) {
            errors.add("coordinate type " + c.getCoordinateType() + " not supported");
        }

        if (metadata.getAxesForCoordinateType(c.getCoordinateType()) != c.getDimensions()) {
            errors.add("coordinate type requires " + metadata.getAxesForCoordinateType(c.getCoordinateType()) + " axes");
        }

        int i = 0;
        for (java.math.BigDecimal value : metadata.getMinimumCoordinateValues(c.getCoordinateType())) {
            if (value.compareTo(c.getValues()[i++]) > 0) {
                errors.add("value cannot be less than " + value);
            }
        }

        i = 0;
        for (java.math.BigDecimal value : metadata.getMaximumCoordinateValues(c.getCoordinateType())) {
            if (value.compareTo(c.getValues()[i++]) > 0) {
                errors.add("value cannot be less than " + value);
            }
        }

        return (errors);
    }


    /**
     *  Validates a currency array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a currency array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateCurrencyArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.CURRENCY) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a currency");
        }

        if (!(element instanceof org.osid.financials.Currency[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a currency");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateCurrency(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a currency against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a currency
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateCurrency(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.CURRENCY) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a currency");
        }

        if (!(element instanceof org.osid.financials.Currency)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a currency");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getCurrencySet()));

        org.osid.financials.Currency c = (org.osid.financials.Currency) element;
        if (!metadata.supportsCurrencyType(c.getCurrencyType())) {
            errors.add("currency type " + c.getCurrencyType() + " not supported");
        }

        if (metadata.getMinimumCurrency().isLarger(c)) {
            errors.add("currency must be at least " + metadata.getMinimumCurrency());
        }

        if (metadata.getMaximumCurrency().isSmaller(c)) {
            errors.add("currency cannot exceed " + metadata.getMinimumCurrency());
        }
            
        return (errors);
    }


    /**
     *  Validates a datetime array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a datetime array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateDateTimeArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.DATETIME) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a datetime");
        }

        if (!(element instanceof org.osid.calendaring.DateTime[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a datetime");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateDateTime(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a datetime against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a datetime
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateDateTime(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.DATETIME) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a datetime");
        }

        if (!(element instanceof org.osid.calendaring.DateTime)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a datetime");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getDateTimeSet()));

        org.osid.calendaring.DateTime dt = (org.osid.calendaring.DateTime) element;
        if (dt.getGranularity().ordinal() < metadata.getDateTimeResolution().ordinal()) {
            errors.add("resolution cannot exceed " + metadata.getDateTimeResolution());
        }

        if (!metadata.supportsCalendarType(dt.getCalendarType())) {
            errors.add("calendar type not supported");
        }

        if (!metadata.supportsTimeType(dt.getTimeType())) {
            errors.add("time type not supported");
        }

        if (metadata.getMinimumDateTime().isGreater(dt)) {
            errors.add("datetime must be at least " + metadata.getMinimumDateTime());
        }

        if (metadata.getMaximumDateTime().isLess(dt)) {
            errors.add("datetime cannot exceed " + metadata.getMinimumDateTime());
        }
                    
        return (errors);
    }


    /**
     *  Validates a decimal array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a decimal array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateDecimalArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.DECIMAL) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a decimal");
        }

        if (!(element instanceof java.math.BigDecimal[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a decimal");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateDecimal(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a decimal against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a decimal
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateDecimal(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.DECIMAL) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a decimal");
        }

        if (!(element instanceof java.math.BigDecimal)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a decimal");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getDecimalSet()));

        java.math.BigDecimal d = (java.math.BigDecimal) element;
        
        if (metadata.getMinimumDecimal().compareTo(d) > 0) {
            errors.add("cannot be less than " + metadata.getMinimumDecimal());
        }

        if (metadata.getMaximumDecimal().compareTo(d) < 0) {
            errors.add("cannot be greater than " + metadata.getMaximumDecimal());
        }

        if (d.scale() > metadata.getDecimalScale()) {
            errors.add("only " + metadata.getDecimalScale() + " allowed after decimal point");
        }

        return (errors);
    }


    /**
     *  Validates a distance array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a distance array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateDistanceArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.DISTANCE) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a distance");
        }

        if (!(element instanceof org.osid.mapping.Distance[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a distance");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateDistance(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a distance against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a distance
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateDistance(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.DISTANCE) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a distance");
        }

        if (!(element instanceof org.osid.mapping.Distance)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a distance");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getDistanceSet()));

        org.osid.mapping.Distance d = (org.osid.mapping.Distance) element;

        if (d.getGranularity().ordinal() < metadata.getDistanceResolution().ordinal()) {
            errors.add("resolution cannot exceed " + metadata.getDistanceResolution());
        }

        if (metadata.getMinimumDistance().isGreater(d)) {
            errors.add("value cannot be less than " + metadata.getMinimumDistance());
        }

        if (metadata.getMaximumDistance().isLess(d)) {
            errors.add("value cannot exceed " + metadata.getMinimumDistance());
        }

        return (errors);
    }


    /**
     *  Validates a duration array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a duration array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateDurationArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.DURATION) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a duration");
        }

        if (!(element instanceof org.osid.calendaring.Duration[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a duration");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateDuration(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a duration against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a duration
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateDuration(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.DURATION) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a duration");
        }

        if (!(element instanceof org.osid.calendaring.Duration)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a duration");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getDurationSet()));

        org.osid.calendaring.Duration d = (org.osid.calendaring.Duration) element;

        if (d.getGranularity().ordinal() < metadata.getDateTimeResolution().ordinal()) {
            errors.add("resolution cannot exceed " + metadata.getDateTimeResolution());
        }

        if (metadata.getMinimumDuration().isGreater(d)) {
            errors.add("value cannot be less than " + metadata.getMinimumDuration());
        }

        if (metadata.getMaximumDuration().isLess(d)) {
            errors.add("value cannot exceed " + metadata.getMinimumDuration());
        }

        return (errors);
    }


    /**
     *  Validates a heading array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a heading array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateHeadingArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.HEADING) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a heading");
        }

        if (!(element instanceof org.osid.mapping.Heading[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a heading");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateHeading(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a heading against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a heading
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateHeading(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.HEADING) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a heading");
        }

        if (!(element instanceof org.osid.mapping.Heading)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a heading");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getHeadingSet()));

        org.osid.mapping.Heading h = (org.osid.mapping.Heading) element;

        if (!metadata.supportsHeadingType(h.getHeadingType())) {
            errors.add("type not supported");
        }

        if (metadata.getAxesForHeadingType(h.getHeadingType()) != h.getDimensions()) {
            errors.add("heading type requires " + metadata.getAxesForHeadingType(h.getHeadingType()) + " axes");
        }            

        int i = 0;
        for (java.math.BigDecimal value : metadata.getMinimumHeadingValues(h.getHeadingType())) {
            if (value.compareTo(h.getValues()[i++]) > 0) {
                errors.add("value cannot be less than " + value);
            }
        }

        i = 0;
        for (java.math.BigDecimal value : metadata.getMaximumHeadingValues(h.getHeadingType())) {
            if (value.compareTo(h.getValues()[i++]) > 0) {
                errors.add("value cannot be less than " + value);
            }
        }

        return (errors);
    }


    /**
     *  Validates an Id array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not an Id array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateIdArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.ID) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not an Id");
        }

        if (!(element instanceof org.osid.id.Id[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not an Id");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateId(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates an Id against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not an Id
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateId(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.ID) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not an Id");
        }

        if (!(element instanceof org.osid.id.Id)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not an Id");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getIdSet()));

        return (errors);
    }


    /**
     *  Validates a integer array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a integer array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateIntegerArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.INTEGER) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a integer");
        }

        if (!(element instanceof Long[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a integer");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateInteger(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a integer against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a integer
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateInteger(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.INTEGER) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a integer");
        }

        if (!(element instanceof Long)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a integer");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        long[] set = metadata.getIntegerSet();
        Long[] longs = new Long[set.length];
        for (int i = 0; i < set.length; i++) {
            longs[i] = set[i];
        }

        errors.addAll(validateCommon(element, metadata, longs));

        Integer i = (Integer) element;

        if (i < metadata.getMinimumInteger()) {
            errors.add("cannot be less than " + metadata.getMinimumInteger());
        }

        if (i > metadata.getMaximumInteger()) {
            errors.add("cannot be greater than " + metadata.getMaximumInteger());
        }

        return (errors);
    }


    /**
     *  Validates an object array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not an object array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateObjectArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.OBJECT) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not an object");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateObject(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates an object against its metadata. This method should
     *  be overridden to check for specific object types.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not an object
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateObject(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.OBJECT) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not an object");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        return (errors);
    }


    /**
     *  Validates a spatial unit array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a spatial unit array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateSpatialUnitArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.SPATIALUNIT) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a spatial unit");
        }

        if (!(element instanceof org.osid.mapping.SpatialUnit[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a spatial unit");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateSpatialUnit(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a spatial unit against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a spatial unit
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateSpatialUnit(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.SPATIALUNIT) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a spatial unit");
        }

        if (!(element instanceof org.osid.mapping.SpatialUnit)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a spatial unit");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getSpatialUnitSet()));

        org.osid.mapping.SpatialUnit su = (org.osid.mapping.SpatialUnit) element;

        boolean supports = false;
        try (org.osid.type.TypeList types = su.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    if (metadata.supportsSpatialUnitRecordType(types.getNextType())) {
                        supports = true;
                        break;
                    }
                } catch (org.osid.OperationFailedException oe) {}
            }
        }

        if (!supports) {
            errors.add("type not supported");
        }

        return (errors);
    }


    /**
     *  Validates a speed array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a speed array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateSpeedArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.SPEED) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a speed");
        }

        if (!(element instanceof org.osid.mapping.Speed[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a speed");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateSpeed(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a speed against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a speed
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateSpeed(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.SPEED) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a speed");
        }

        if (!(element instanceof org.osid.mapping.Speed)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a speed");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getSpeedSet()));

        org.osid.mapping.Speed d = (org.osid.mapping.Speed) element;

        if (metadata.getMinimumSpeed().isGreater(d)) {
            errors.add("value cannot be less than " + metadata.getMinimumSpeed());
        }

        if (metadata.getMaximumSpeed().isLess(d)) {
            errors.add("value cannot exceed " + metadata.getMinimumSpeed());
        }

        return (errors);
    }


    /**
     *  Validates a string array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a string array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateStringArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.STRING) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a string");
        }

        if (!(element instanceof String[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a string");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateString(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a string against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a string
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateString(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.STRING) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a string");
        }

        if (!(element instanceof String)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a string");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getStringSet()));

        String s = (String) element;

        if (s.length() < metadata.getMinimumStringLength()) {
            errors.add(metadata.getMinimumStringLength() + " characters required.");
        }

        if (s.length() > metadata.getMaximumStringLength()) {
            errors.add("Exceeds " + metadata.getMaximumStringLength() + " characters.");
        }

        if (metadata.supportsStringMatchType(net.okapia.osid.primordium.types.search.StringMatchTypes.REGEX.getType())) {
            if (!s.matches(metadata.getStringExpression(net.okapia.osid.primordium.types.search.StringMatchTypes.REGEX.getType()))) {
                errors.add("Invalid format (" + metadata.getStringExpression(net.okapia.osid.primordium.types.search.StringMatchTypes.REGEX.getType()) + ").");
            }
        }

        return (errors);
    }


    /**
     *  Validates a time array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a time array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateTimeArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.TIME) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a time");
        }

        if (!(element instanceof org.osid.calendaring.Time[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a time");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateTime(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a time against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a time
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateTime(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.TIME) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a time");
        }

        if (!(element instanceof org.osid.calendaring.Time)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a time");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getTimeSet()));

        org.osid.calendaring.Time t = (org.osid.calendaring.Time) element;
        if (t.getGranularity().ordinal() < metadata.getDateTimeResolution().ordinal()) {
            errors.add("resolution cannot exceed " + metadata.getDateTimeResolution());
        }

        if (!metadata.supportsTimeType(t.getTimeType())) {
            errors.add("time type not supported");
        }

        if (metadata.getMinimumTime().isGreater(t)) {
            errors.add("time must be at least " + metadata.getMinimumTime());
        }

        if (metadata.getMaximumTime().isLess(t)) {
            errors.add("time cannot exceed " + metadata.getMinimumTime());
        }
                    
        return (errors);
    }


    /**
     *  Validates a type array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a type array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateTypeArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.TYPE) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a type");
        }

        if (!(element instanceof org.osid.type.Type[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a type");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateType(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a type against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a type
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateType(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.TYPE) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a type");
        }

        if (!(element instanceof org.osid.type.Type)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a type");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getTypeSet()));
        return (errors);
    }


    /**
     *  Validates a version array against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a version array
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateVersionArray(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.VERSION) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a version");
        }

        if (!(element instanceof org.osid.installation.Version[])) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a version");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        for (Object e : (Object[]) element) {
            errors.addAll(validateVersion(e, metadata));
        }

        return (errors);
    }


    /**
     *  Validates a version against its metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid or is not a version
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */

    protected java.util.Collection<String> validateVersion(Object element, org.osid.Metadata metadata) {
        if (metadata.getSyntax() != org.osid.Syntax.VERSION) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a version");
        }

        if (!(element instanceof org.osid.installation.Version)) {
            throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " not a version");
        }

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);
        errors.addAll(validateCommon(element, metadata, metadata.getVersionSet()));

        org.osid.installation.Version v = (org.osid.installation.Version) element;

        if (!metadata.supportsVersionType(v.getScheme())) {
            errors.add("version type not supported");
        }

        if (metadata.getMinimumVersion().isNewer(v)) {
            errors.add("version must be at least " + metadata.getMinimumVersion());
        }

        if (metadata.getMaximumVersion().isOlder(v)) {
            errors.add("version cannot exceed " + metadata.getMinimumVersion());
        }
                    
        return (errors);
    }


    /**
     *  Validates an element against common metadata.
     *
     *  @param element the element
     *  @param metadata element metadata
     *  @throws org.osid.InvalidArgumentException {@code element} is
     *          invalid
     *  @throws org.osid.NullArgumentException {@code element} or
     *          {@code metadata} is {@code null}
     *  @throws org.osid.NoAccessException element is read only
     */
    
    protected java.util.Collection<String> validateCommon(Object element, org.osid.Metadata metadata, Object[] set) {
        nullarg(set, metadata.getElementLabel() + " set");

        java.util.Collection<String> errors = new java.util.ArrayList<>(0);

        if (metadata.isReadOnly()) {
            throw new org.osid.NoAccessException(metadata.getElementLabel() + " is read only");
        }

        if (element.getClass().isArray() != metadata.isArray()) {
                throw new org.osid.InvalidArgumentException(metadata.getElementLabel() + " array mismatch");
        }   
         
        if (element.getClass().isArray()) {
            Object[] elements = (Object[]) element;
            if (elements.length < metadata.getMinimumElements()) {
                errors.add("Does not contain at least " + metadata.getMinimumElements() + " elements.");
            }

            if (elements.length > metadata.getMaximumElements()) {
                errors.add("Contains more than " + metadata.getMaximumElements() + " elements.");
            }

            if (set.length > 0) {
                for (Object o : elements) {
                    boolean found = false;
                    for (Object e : set) {
                        if (e.equals(o)) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        errors.add("Contains an unallowed element.");
                    }
                }
            }
        } else {
            if (set.length > 0) {
                boolean found = false;
                for (Object e : set) {
                    if (e.equals(element)) {
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    errors.add("Contains an unallowed element.");
                }
            }
        }
            
        return (errors);
    }
}