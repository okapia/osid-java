//
// AbstractAssessmentLookupSession.java
//
//    A starter implementation framework for providing an Assessment
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Assessment
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAssessments(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAssessmentLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.assessment.AssessmentLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();
    

    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }

    /**
     *  Tests if this user can perform <code>Assessment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessments() {
        return (true);
    }


    /**
     *  A complete view of the <code>Assessment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Assessment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessment entries in banks which are
     *  children of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Assessment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code>Assessment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate <code> Id
     *  </code> was assigned to a <code>Assessment</code> and retained
     *  for compatibility.
     *
     *  @param  assessmentId <code>Id</code> of the
     *          <code>Assessment</code>
     *  @return the assessment
     *  @throws org.osid.NotFoundException <code>assessmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment(org.osid.id.Id assessmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.assessment.AssessmentList assessments = getAssessments()) {
            while (assessments.hasNext()) {
                org.osid.assessment.Assessment assessment = assessments.getNextAssessment();
                if (assessment.getId().equals(assessmentId)) {
                    return (assessment);
                }
            }
        } 

        throw new org.osid.NotFoundException(assessmentId + " not found");
    }


    /**
     *  Gets an <code>AssessmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Assessments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAssessments()</code>.
     *
     *  @param  assessmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Assessment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByIds(org.osid.id.IdList assessmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.Assessment> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = assessmentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAssessment(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("assessment " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.assessment.assessment.LinkedAssessmentList(ret));
    }


    /**
     *  Gets an <code>AssessmentList</code> corresponding to the given
     *  assessment genus <code>Type</code> which does not include
     *  assessments of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAssessments()</code>.
     *
     *  @param  assessmentGenusType an assessment genus type 
     *  @return the returned <code>Assessment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByGenusType(org.osid.type.Type assessmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessment.AssessmentGenusFilterList(getAssessments(), assessmentGenusType));
    }


    /**
     *  Gets an <code>AssessmentList</code> corresponding to the given
     *  assessment genus <code>Type</code> and include any additional
     *  assessments with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssessments()</code>.
     *
     *  @param  assessmentGenusType an assessment genus type 
     *  @return the returned <code>Assessment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByParentGenusType(org.osid.type.Type assessmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAssessmentsByGenusType(assessmentGenusType));
    }


    /**
     *  Gets an <code>AssessmentList</code> containing the given
     *  assessment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssessments()</code>.
     *
     *  @param  assessmentRecordType an assessment record type 
     *  @return the returned <code>Assessment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByRecordType(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessment.AssessmentRecordFilterList(getAssessments(), assessmentRecordType));
    }


    /**
     *  Gets all <code>Assessments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Assessments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.assessment.AssessmentList getAssessments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the assessment list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of assessments
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.assessment.AssessmentList filterAssessmentsOnViews(org.osid.assessment.AssessmentList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }
}
