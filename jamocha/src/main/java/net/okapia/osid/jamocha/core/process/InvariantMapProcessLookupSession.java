//
// InvariantMapProcessLookupSession
//
//    Implements a Process lookup service backed by a fixed collection of
//    processes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process;


/**
 *  Implements a Process lookup service backed by a fixed
 *  collection of processes. The processes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProcessLookupSession
    extends net.okapia.osid.jamocha.core.process.spi.AbstractMapProcessLookupSession
    implements org.osid.process.ProcessLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapProcessLookupSession</code> with no
     *  processes.
     */

    public InvariantMapProcessLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProcessLookupSession</code> with a single
     *  process.
     *  
     *  @throws org.osid.NullArgumentException {@code process}
     *          is <code>null</code>
     */

    public InvariantMapProcessLookupSession(org.osid.process.Process process) {
        putProcess(process);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProcessLookupSession</code> using an array
     *  of processes.
     *  
     *  @throws org.osid.NullArgumentException {@code processes}
     *          is <code>null</code>
     */

    public InvariantMapProcessLookupSession(org.osid.process.Process[] processes) {
        putProcesses(processes);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProcessLookupSession</code> using a
     *  collection of processes.
     *
     *  @throws org.osid.NullArgumentException {@code processes}
     *          is <code>null</code>
     */

    public InvariantMapProcessLookupSession(java.util.Collection<? extends org.osid.process.Process> processes) {
        putProcesses(processes);
        return;
    }
}
