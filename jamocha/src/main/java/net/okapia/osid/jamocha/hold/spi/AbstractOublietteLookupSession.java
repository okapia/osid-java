//
// AbstractOublietteLookupSession.java
//
//    A starter implementation framework for providing an Oubliette
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing an Oubliette
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getOubliettes(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractOublietteLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.hold.OublietteLookupSession {

    private boolean pedantic = false;
    private org.osid.hold.Oubliette oubliette = new net.okapia.osid.jamocha.nil.hold.oubliette.UnknownOubliette();
    

    /**
     *  Tests if this user can perform <code>Oubliette</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOubliettes() {
        return (true);
    }


    /**
     *  A complete view of the <code>Oubliette</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOublietteView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Oubliette</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOublietteView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Oubliette</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Oubliette</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Oubliette</code> and
     *  retained for compatibility.
     *
     *  @param  oublietteId <code>Id</code> of the
     *          <code>Oubliette</code>
     *  @return the oubliette
     *  @throws org.osid.NotFoundException <code>oublietteId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>oublietteId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.hold.OublietteList oubliettes = getOubliettes()) {
            while (oubliettes.hasNext()) {
                org.osid.hold.Oubliette oubliette = oubliettes.getNextOubliette();
                if (oubliette.getId().equals(oublietteId)) {
                    return (oubliette);
                }
            }
        } 

        throw new org.osid.NotFoundException(oublietteId + " not found");
    }


    /**
     *  Gets an <code>OublietteList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  oubliettes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Oubliettes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getOubliettes()</code>.
     *
     *  @param  oublietteIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Oubliette</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByIds(org.osid.id.IdList oublietteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.hold.Oubliette> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = oublietteIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getOubliette(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("oubliette " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.hold.oubliette.LinkedOublietteList(ret));
    }


    /**
     *  Gets an <code>OublietteList</code> corresponding to the given
     *  oubliette genus <code>Type</code> which does not include
     *  oubliettes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getOubliettes()</code>.
     *
     *  @param  oublietteGenusType an oubliette genus type 
     *  @return the returned <code>Oubliette</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByGenusType(org.osid.type.Type oublietteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.oubliette.OublietteGenusFilterList(getOubliettes(), oublietteGenusType));
    }


    /**
     *  Gets an <code>OublietteList</code> corresponding to the given
     *  oubliette genus <code>Type</code> and include any additional
     *  oubliettes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOubliettes()</code>.
     *
     *  @param  oublietteGenusType an oubliette genus type 
     *  @return the returned <code>Oubliette</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByParentGenusType(org.osid.type.Type oublietteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOubliettesByGenusType(oublietteGenusType));
    }


    /**
     *  Gets an <code>OublietteList</code> containing the given
     *  oubliette record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOubliettes()</code>.
     *
     *  @param  oublietteRecordType an oubliette record type 
     *  @return the returned <code>Oubliette</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByRecordType(org.osid.type.Type oublietteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.oubliette.OublietteRecordFilterList(getOubliettes(), oublietteRecordType));
    }


    /**
     *  Gets an <code>OublietteList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Oubliette</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.hold.oubliette.OublietteProviderFilterList(getOubliettes(), resourceId));
    }


    /**
     *  Gets all <code>Oubliettes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Oubliettes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.hold.OublietteList getOubliettes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the oubliette list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of oubliettes
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.hold.OublietteList filterOubliettesOnViews(org.osid.hold.OublietteList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
