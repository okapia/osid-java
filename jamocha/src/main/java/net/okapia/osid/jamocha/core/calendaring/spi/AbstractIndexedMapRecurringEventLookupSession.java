//
// AbstractIndexedMapRecurringEventLookupSession.java
//
//    A simple framework for providing a RecurringEvent lookup service
//    backed by a fixed collection of recurring events with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a RecurringEvent lookup service backed by a
 *  fixed collection of recurring events. The recurring events are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some recurring events may be compatible
 *  with more types than are indicated through these recurring event
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RecurringEvents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRecurringEventLookupSession
    extends AbstractMapRecurringEventLookupSession
    implements org.osid.calendaring.RecurringEventLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.RecurringEvent> recurringEventsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.RecurringEvent>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.RecurringEvent> recurringEventsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.RecurringEvent>());


    /**
     *  Makes a <code>RecurringEvent</code> available in this session.
     *
     *  @param  recurringEvent a recurring event
     *  @throws org.osid.NullArgumentException <code>recurringEvent<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRecurringEvent(org.osid.calendaring.RecurringEvent recurringEvent) {
        super.putRecurringEvent(recurringEvent);

        this.recurringEventsByGenus.put(recurringEvent.getGenusType(), recurringEvent);
        
        try (org.osid.type.TypeList types = recurringEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.recurringEventsByRecord.put(types.getNextType(), recurringEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a recurring event from this session.
     *
     *  @param recurringEventId the <code>Id</code> of the recurring event
     *  @throws org.osid.NullArgumentException <code>recurringEventId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRecurringEvent(org.osid.id.Id recurringEventId) {
        org.osid.calendaring.RecurringEvent recurringEvent;
        try {
            recurringEvent = getRecurringEvent(recurringEventId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.recurringEventsByGenus.remove(recurringEvent.getGenusType());

        try (org.osid.type.TypeList types = recurringEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.recurringEventsByRecord.remove(types.getNextType(), recurringEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRecurringEvent(recurringEventId);
        return;
    }


    /**
     *  Gets a <code>RecurringEventList</code> corresponding to the given
     *  recurring event genus <code>Type</code> which does not include
     *  recurring events of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known recurring events or an error results. Otherwise,
     *  the returned list may contain only those recurring events that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  recurringEventGenusType a recurring event genus type 
     *  @return the returned <code>RecurringEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByGenusType(org.osid.type.Type recurringEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.recurringevent.ArrayRecurringEventList(this.recurringEventsByGenus.get(recurringEventGenusType)));
    }


    /**
     *  Gets a <code>RecurringEventList</code> containing the given
     *  recurring event record <code>Type</code>. In plenary mode, the
     *  returned list contains all known recurring events or an error
     *  results. Otherwise, the returned list may contain only those
     *  recurring events that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  recurringEventRecordType a recurring event record type 
     *  @return the returned <code>recurringEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByRecordType(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.recurringevent.ArrayRecurringEventList(this.recurringEventsByRecord.get(recurringEventRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.recurringEventsByGenus.clear();
        this.recurringEventsByRecord.clear();

        super.close();

        return;
    }
}
