//
// AbstractAgendaSearch.java
//
//     A template for making an Agenda Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.agenda.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing agenda searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAgendaSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.rules.check.AgendaSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.rules.check.records.AgendaSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.rules.check.AgendaSearchOrder agendaSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of agendas. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  agendaIds list of agendas
     *  @throws org.osid.NullArgumentException
     *          <code>agendaIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAgendas(org.osid.id.IdList agendaIds) {
        while (agendaIds.hasNext()) {
            try {
                this.ids.add(agendaIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAgendas</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of agenda Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAgendaIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  agendaSearchOrder agenda search order 
     *  @throws org.osid.NullArgumentException
     *          <code>agendaSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>agendaSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAgendaResults(org.osid.rules.check.AgendaSearchOrder agendaSearchOrder) {
	this.agendaSearchOrder = agendaSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.rules.check.AgendaSearchOrder getAgendaSearchOrder() {
	return (this.agendaSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given agenda search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an agenda implementing the requested record.
     *
     *  @param agendaSearchRecordType an agenda search record
     *         type
     *  @return the agenda search record
     *  @throws org.osid.NullArgumentException
     *          <code>agendaSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agendaSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.AgendaSearchRecord getAgendaSearchRecord(org.osid.type.Type agendaSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.rules.check.records.AgendaSearchRecord record : this.records) {
            if (record.implementsRecordType(agendaSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agendaSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this agenda search. 
     *
     *  @param agendaSearchRecord agenda search record
     *  @param agendaSearchRecordType agenda search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAgendaSearchRecord(org.osid.rules.check.records.AgendaSearchRecord agendaSearchRecord, 
                                           org.osid.type.Type agendaSearchRecordType) {

        addRecordType(agendaSearchRecordType);
        this.records.add(agendaSearchRecord);        
        return;
    }
}
