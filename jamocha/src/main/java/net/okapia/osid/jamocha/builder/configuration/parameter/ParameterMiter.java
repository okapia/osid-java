//
// ParameterMiter.java
//
//     Defines a Parameter miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.parameter;


/**
 *  Defines a <code>Parameter</code> miter for use with the builders.
 */

public interface ParameterMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.configuration.Parameter {


    /**
     *  Sets the value syntax.
     *
     *  @param syntax a value syntax
     *  @throws org.osid.NullArgumentException <code>syntax</code> is
     *          <code>null</code>
     */

    public void setValueSyntax(org.osid.Syntax syntax);


    /**
     *  Sets the value coordinate type.
     *
     *  @param coordinateType a value coordinate type
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateType</code> is <code>null</code>
     */

    public void setValueCoordinateType(org.osid.type.Type coordinateType);


    /**
     *  Sets the value heading type.
     *
     *  @param headingType a value heading type
     *  @throws org.osid.NullArgumentException
     *          <code>headingType</code> is <code>null</code>
     */

    public void setValueHeadingType(org.osid.type.Type headingType);


    /**
     *  Sets the value spatial unit record type.
     *
     *  @param spatialUnitRecordType a value spatial unit record type
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnitRecordType</code> is
     *          <code>null</code>
     */

    public void setValueSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType);


    /**
     *  Sets the value object type.
     *
     *  @param objectType a value object type
     *  @throws org.osid.NullArgumentException <code>objectType</code>
     *          is <code>null</code>
     */

    public void setValueObjectType(org.osid.type.Type objectType);


    /**
     *  Sets the version scheme.
     *
     *  @param versionType an version scheme
     *  @throws org.osid.NullArgumentException
     *          <code>versionType</code> is <code>null</code>
     */
    
    public void setValueVersionScheme(org.osid.type.Type versionType);


    /**
     *  Sets the shuffled flag.
     *
     *  @param shuffled <code> true </code> if the values are
     *         shuffled, <code> false </code> otherwise
     */

    public void setShuffled(boolean shuffled);


    /**
     *  Adds a Parameter record.
     *
     *  @param record a parameter record
     *  @param recordType the type of parameter record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addParameterRecord(org.osid.configuration.records.ParameterRecord record, org.osid.type.Type recordType);
}       


