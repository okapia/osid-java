//
// AbstractFederatingGradeEntryLookupSession.java
//
//     An abstract federating adapter for a GradeEntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  GradeEntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingGradeEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.grading.GradeEntryLookupSession>
    implements org.osid.grading.GradeEntryLookupSession {

    private boolean parallel = false;
    private org.osid.grading.Gradebook gradebook = new net.okapia.osid.jamocha.nil.grading.gradebook.UnknownGradebook();


    /**
     *  Constructs a new <code>AbstractFederatingGradeEntryLookupSession</code>.
     */

    protected AbstractFederatingGradeEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.grading.GradeEntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Gradebook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Gradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.gradebook.getId());
    }


    /**
     *  Gets the <code>Gradebook</code> associated with this 
     *  session.
     *
     *  @return the <code>Gradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.gradebook);
    }


    /**
     *  Sets the <code>Gradebook</code>.
     *
     *  @param  gradebook the gradebook for this session
     *  @throws org.osid.NullArgumentException <code>gradebook</code>
     *          is <code>null</code>
     */

    protected void setGradebook(org.osid.grading.Gradebook gradebook) {
        nullarg(gradebook, "gradebook");
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Tests if this user can perform <code>GradeEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradeEntries() {
        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            if (session.canLookupGradeEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>GradeEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradeEntryView() {
        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            session.useComparativeGradeEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>GradeEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradeEntryView() {
        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            session.usePlenaryGradeEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade entries in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            session.useFederatedGradebookView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            session.useIsolatedGradebookView();
        }

        return;
    }


    /**
     *  Only grade entries whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveGradeEntryView() {
        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            session.useEffectiveGradeEntryView();
        }

        return;
    }


    /**
     *  All grade entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveGradeEntryView() {
        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            session.useAnyEffectiveGradeEntryView();
        }

        return;
    }

     
    /**
     *  Gets the <code>GradeEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradeEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>GradeEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param  gradeEntryId <code>Id</code> of the
     *          <code>GradeEntry</code>
     *  @return the grade entry
     *  @throws org.osid.NotFoundException <code>gradeEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradeEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntry getGradeEntry(org.osid.id.Id gradeEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            try {
                return (session.getGradeEntry(gradeEntryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(gradeEntryId + " not found");
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>GradeEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, grade entries are returned that are currently effective.
     *  In any effective mode, effective grade entries and those currently expired
     *  are returned.
     *
     *  @param  gradeEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByIds(org.osid.id.IdList gradeEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.grading.gradeentry.MutableGradeEntryList ret = new net.okapia.osid.jamocha.grading.gradeentry.MutableGradeEntryList();

        try (org.osid.id.IdList ids = gradeEntryIds) {
            while (ids.hasNext()) {
                ret.addGradeEntry(getGradeEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  grade entry genus <code>Type</code> which does not include
     *  grade entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently effective.
     *  In any effective mode, effective grade entries and those currently expired
     *  are returned.
     *
     *  @param  gradeEntryGenusType a gradeEntry genus type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByGenusType(org.osid.type.Type gradeEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesByGenusType(gradeEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradeEntryList</code> corresponding to the given
     *  grade entry genus <code>Type</code> and include any additional
     *  grade entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param  gradeEntryGenusType a gradeEntry genus type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByParentGenusType(org.osid.type.Type gradeEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesByParentGenusType(gradeEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradeEntryList</code> containing the given grade
     *  entry record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradeEntryRecordType a gradeEntry record type 
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByRecordType(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesByRecordType(gradeEntryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradeEntryList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *  
     *  In active mode, grade entries are returned that are currently
     *  active. In any status mode, active and inactive grade entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>GradeEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of grade entries corresponding to a gradebook
     *  column <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the <code>Id</code> of the gradebook column
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumn(org.osid.id.Id gradebookColumnId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesForGradebookColumn(gradebookColumnId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of grade entries corresponding to a gradebook
     *  column <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the <code>Id</code> of the gradebook column
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnOnDate(org.osid.id.Id gradebookColumnId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesForGradebookColumnOnDate(gradebookColumnId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of grade entries corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.grading.GradeEntryList getGradeEntriesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of grade entries corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForResourceOnDate(org.osid.id.Id resourceId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesForResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of grade entries corresponding to gradebook column
     *  and resource <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the <code>Id</code> of the gradebook column
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnAndResource(org.osid.id.Id gradebookColumnId,
                                                                                        org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesForGradebookColumnAndResource(gradebookColumnId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of grade entries corresponding to gradebook column
     *  and resource <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>GradeEntryList</code>
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnAndResourceOnDate(org.osid.id.Id gradebookColumnId,
                                                                                              org.osid.id.Id resourceId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesForGradebookColumnAndResourceOnDate(gradebookColumnId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradeEntryList</code> for the given grader.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective. In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId an resource <code>Id</code>
     *  @return the returned <code>GradeEntry</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByGrader(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntriesByGrader(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>GradeEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>GradeEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList ret = getGradeEntryList();

        for (org.osid.grading.GradeEntryLookupSession session : getSessions()) {
            ret.addGradeEntryList(session.getGradeEntries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.FederatingGradeEntryList getGradeEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.ParallelGradeEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.grading.gradeentry.CompositeGradeEntryList());
        }
    }
}
