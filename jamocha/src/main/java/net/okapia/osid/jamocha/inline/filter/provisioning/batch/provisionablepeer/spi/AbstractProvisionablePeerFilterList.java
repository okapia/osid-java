//
// AbstractProvisionablePeerList
//
//     Implements a filter for a ProvisionablePeerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.provisioning.batch.provisionablepeer.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a ProvisionablePeerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedProvisionablePeerList
 *  to improve performance.
 */

public abstract class AbstractProvisionablePeerFilterList
    extends net.okapia.osid.jamocha.provisioning.batch.provisionablepeer.spi.AbstractProvisionablePeerList
    implements org.osid.provisioning.batch.ProvisionablePeerList,
               net.okapia.osid.jamocha.inline.filter.provisioning.batch.provisionablepeer.ProvisionablePeerFilter {

    private org.osid.provisioning.batch.ProvisionablePeer provisionablePeer;
    private final org.osid.provisioning.batch.ProvisionablePeerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractProvisionablePeerFilterList</code>.
     *
     *  @param provisionablePeerList a <code>ProvisionablePeerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>provisionablePeerList</code> is <code>null</code>
     */

    protected AbstractProvisionablePeerFilterList(org.osid.provisioning.batch.ProvisionablePeerList provisionablePeerList) {
        nullarg(provisionablePeerList, "provisionable peer list");
        this.list = provisionablePeerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.provisionablePeer == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> ProvisionablePeer </code> in this list. 
     *
     *  @return the next <code> ProvisionablePeer </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> ProvisionablePeer </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionablePeer getNextProvisionablePeer()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.provisioning.batch.ProvisionablePeer provisionablePeer = this.provisionablePeer;
            this.provisionablePeer = null;
            return (provisionablePeer);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in provisionable peer list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.provisionablePeer = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters ProvisionablePeers.
     *
     *  @param provisionablePeer the provisionable peer to filter
     *  @return <code>true</code> if the provisionable peer passes the filter,
     *          <code>false</code> if the provisionable peer should be filtered
     */

    public abstract boolean pass(org.osid.provisioning.batch.ProvisionablePeer provisionablePeer);


    protected void prime() {
        if (this.provisionablePeer != null) {
            return;
        }

        org.osid.provisioning.batch.ProvisionablePeer provisionablePeer = null;

        while (this.list.hasNext()) {
            try {
                provisionablePeer = this.list.getNextProvisionablePeer();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(provisionablePeer)) {
                this.provisionablePeer = provisionablePeer;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
