//
// AbstractAdapterStoreLookupSession.java
//
//    A Store lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ordering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Store lookup session adapter.
 */

public abstract class AbstractAdapterStoreLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ordering.StoreLookupSession {

    private final org.osid.ordering.StoreLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterStoreLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStoreLookupSession(org.osid.ordering.StoreLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Store} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupStores() {
        return (this.session.canLookupStores());
    }


    /**
     *  A complete view of the {@code Store} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStoreView() {
        this.session.useComparativeStoreView();
        return;
    }


    /**
     *  A complete view of the {@code Store} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStoreView() {
        this.session.usePlenaryStoreView();
        return;
    }

     
    /**
     *  Gets the {@code Store} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Store} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Store} and
     *  retained for compatibility.
     *
     *  @param storeId {@code Id} of the {@code Store}
     *  @return the store
     *  @throws org.osid.NotFoundException {@code storeId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code storeId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStore(storeId));
    }


    /**
     *  Gets a {@code StoreList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stores specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Stores} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  storeIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Store} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code storeIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByIds(org.osid.id.IdList storeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStoresByIds(storeIds));
    }


    /**
     *  Gets a {@code StoreList} corresponding to the given
     *  store genus {@code Type} which does not include
     *  stores of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  storeGenusType a store genus type 
     *  @return the returned {@code Store} list
     *  @throws org.osid.NullArgumentException
     *          {@code storeGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByGenusType(org.osid.type.Type storeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStoresByGenusType(storeGenusType));
    }


    /**
     *  Gets a {@code StoreList} corresponding to the given
     *  store genus {@code Type} and include any additional
     *  stores with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  storeGenusType a store genus type 
     *  @return the returned {@code Store} list
     *  @throws org.osid.NullArgumentException
     *          {@code storeGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByParentGenusType(org.osid.type.Type storeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStoresByParentGenusType(storeGenusType));
    }


    /**
     *  Gets a {@code StoreList} containing the given
     *  store record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  storeRecordType a store record type 
     *  @return the returned {@code Store} list
     *  @throws org.osid.NullArgumentException
     *          {@code storeRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByRecordType(org.osid.type.Type storeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStoresByRecordType(storeRecordType));
    }


    /**
     *  Gets a {@code StoreList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Store} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStoresByProvider(resourceId));
    }


    /**
     *  Gets all {@code Stores}. 
     *
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Stores} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStores()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStores());
    }
}
