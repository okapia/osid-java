//
// MutableMapProxyBlockLookupSession
//
//    Implements a Block lookup service backed by a collection of
//    blocks that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold;


/**
 *  Implements a Block lookup service backed by a collection of
 *  blocks. The blocks are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of blocks can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyBlockLookupSession
    extends net.okapia.osid.jamocha.core.hold.spi.AbstractMapBlockLookupSession
    implements org.osid.hold.BlockLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyBlockLookupSession}
     *  with no blocks.
     *
     *  @param oubliette the oubliette
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyBlockLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.proxy.Proxy proxy) {
        setOubliette(oubliette);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBlockLookupSession} with a
     *  single block.
     *
     *  @param oubliette the oubliette
     *  @param block a block
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code block}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBlockLookupSession(org.osid.hold.Oubliette oubliette,
                                                org.osid.hold.Block block, org.osid.proxy.Proxy proxy) {
        this(oubliette, proxy);
        putBlock(block);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBlockLookupSession} using an
     *  array of blocks.
     *
     *  @param oubliette the oubliette
     *  @param blocks an array of blocks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code blocks}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBlockLookupSession(org.osid.hold.Oubliette oubliette,
                                                org.osid.hold.Block[] blocks, org.osid.proxy.Proxy proxy) {
        this(oubliette, proxy);
        putBlocks(blocks);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBlockLookupSession} using a
     *  collection of blocks.
     *
     *  @param oubliette the oubliette
     *  @param blocks a collection of blocks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code blocks}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBlockLookupSession(org.osid.hold.Oubliette oubliette,
                                                java.util.Collection<? extends org.osid.hold.Block> blocks,
                                                org.osid.proxy.Proxy proxy) {
   
        this(oubliette, proxy);
        setSessionProxy(proxy);
        putBlocks(blocks);
        return;
    }

    
    /**
     *  Makes a {@code Block} available in this session.
     *
     *  @param block an block
     *  @throws org.osid.NullArgumentException {@code block{@code 
     *          is {@code null}
     */

    @Override
    public void putBlock(org.osid.hold.Block block) {
        super.putBlock(block);
        return;
    }


    /**
     *  Makes an array of blocks available in this session.
     *
     *  @param blocks an array of blocks
     *  @throws org.osid.NullArgumentException {@code blocks{@code 
     *          is {@code null}
     */

    @Override
    public void putBlocks(org.osid.hold.Block[] blocks) {
        super.putBlocks(blocks);
        return;
    }


    /**
     *  Makes collection of blocks available in this session.
     *
     *  @param blocks
     *  @throws org.osid.NullArgumentException {@code block{@code 
     *          is {@code null}
     */

    @Override
    public void putBlocks(java.util.Collection<? extends org.osid.hold.Block> blocks) {
        super.putBlocks(blocks);
        return;
    }


    /**
     *  Removes a Block from this session.
     *
     *  @param blockId the {@code Id} of the block
     *  @throws org.osid.NullArgumentException {@code blockId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBlock(org.osid.id.Id blockId) {
        super.removeBlock(blockId);
        return;
    }    
}
