//
// AbstractImmutableRecipe.java
//
//     Wraps a mutable Recipe to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recipe.recipe.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Recipe</code> to hide modifiers. This
 *  wrapper provides an immutized Recipe from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying recipe whose state changes are visible.
 */

public abstract class AbstractImmutableRecipe
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableSourceableOsidObject
    implements org.osid.recipe.Recipe {

    private final org.osid.recipe.Recipe recipe;


    /**
     *  Constructs a new <code>AbstractImmutableRecipe</code>.
     *
     *  @param recipe the recipe to immutablize
     *  @throws org.osid.NullArgumentException <code>recipe</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRecipe(org.osid.recipe.Recipe recipe) {
        super(recipe);
        this.recipe = recipe;
        return;
    }


    /**
     *  Gets the overall time required for this recipe. 
     *
     *  @return the estimated duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalEstimatedDuration() {
        return (this.recipe.getTotalEstimatedDuration());
    }


    /**
     *  Gets any asset <code> Ids </code> for the goal of the recipe. 
     *
     *  @return the asset <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetIds() {
        return (this.recipe.getAssetIds());
    }


    /**
     *  Gets any assets for the goal of the recipe. 
     *
     *  @return the assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException {

        return (this.recipe.getAssets());
    }


    /**
     *  Gets the recipe record corresponding to the given <code> Recipe 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> recipeRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(recipeRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  recipeRecordType the type of recipe record to retrieve 
     *  @return the recipe record 
     *  @throws org.osid.NullArgumentException <code> recipeRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(recipeRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.records.RecipeRecord getRecipeRecord(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.recipe.getRecipeRecord(recipeRecordType));
    }
}

