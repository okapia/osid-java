//
// AbstractScheduleSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.schedule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractScheduleSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.ScheduleSearchResults {

    private org.osid.calendaring.ScheduleList schedules;
    private final org.osid.calendaring.ScheduleQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.records.ScheduleSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractScheduleSearchResults.
     *
     *  @param schedules the result set
     *  @param scheduleQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>schedules</code>
     *          or <code>scheduleQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractScheduleSearchResults(org.osid.calendaring.ScheduleList schedules,
                                            org.osid.calendaring.ScheduleQueryInspector scheduleQueryInspector) {
        nullarg(schedules, "schedules");
        nullarg(scheduleQueryInspector, "schedule query inspectpr");

        this.schedules = schedules;
        this.inspector = scheduleQueryInspector;

        return;
    }


    /**
     *  Gets the schedule list resulting from a search.
     *
     *  @return a schedule list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules() {
        if (this.schedules == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.ScheduleList schedules = this.schedules;
        this.schedules = null;
	return (schedules);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.ScheduleQueryInspector getScheduleQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  schedule search record <code> Type. </code> This method must
     *  be used to retrieve a schedule implementing the requested
     *  record.
     *
     *  @param scheduleSearchRecordType a schedule search 
     *         record type 
     *  @return the schedule search
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(scheduleSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSearchResultsRecord getScheduleSearchResultsRecord(org.osid.type.Type scheduleSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.records.ScheduleSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(scheduleSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(scheduleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record schedule search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addScheduleRecord(org.osid.calendaring.records.ScheduleSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "schedule record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
