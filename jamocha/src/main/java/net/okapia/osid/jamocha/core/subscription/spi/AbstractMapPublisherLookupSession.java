//
// AbstractMapPublisherLookupSession
//
//    A simple framework for providing a Publisher lookup service
//    backed by a fixed collection of publishers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Publisher lookup service backed by a
 *  fixed collection of publishers. The publishers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Publishers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPublisherLookupSession
    extends net.okapia.osid.jamocha.subscription.spi.AbstractPublisherLookupSession
    implements org.osid.subscription.PublisherLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.subscription.Publisher> publishers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.subscription.Publisher>());


    /**
     *  Makes a <code>Publisher</code> available in this session.
     *
     *  @param  publisher a publisher
     *  @throws org.osid.NullArgumentException <code>publisher<code>
     *          is <code>null</code>
     */

    protected void putPublisher(org.osid.subscription.Publisher publisher) {
        this.publishers.put(publisher.getId(), publisher);
        return;
    }


    /**
     *  Makes an array of publishers available in this session.
     *
     *  @param  publishers an array of publishers
     *  @throws org.osid.NullArgumentException <code>publishers<code>
     *          is <code>null</code>
     */

    protected void putPublishers(org.osid.subscription.Publisher[] publishers) {
        putPublishers(java.util.Arrays.asList(publishers));
        return;
    }


    /**
     *  Makes a collection of publishers available in this session.
     *
     *  @param  publishers a collection of publishers
     *  @throws org.osid.NullArgumentException <code>publishers<code>
     *          is <code>null</code>
     */

    protected void putPublishers(java.util.Collection<? extends org.osid.subscription.Publisher> publishers) {
        for (org.osid.subscription.Publisher publisher : publishers) {
            this.publishers.put(publisher.getId(), publisher);
        }

        return;
    }


    /**
     *  Removes a Publisher from this session.
     *
     *  @param  publisherId the <code>Id</code> of the publisher
     *  @throws org.osid.NullArgumentException <code>publisherId<code> is
     *          <code>null</code>
     */

    protected void removePublisher(org.osid.id.Id publisherId) {
        this.publishers.remove(publisherId);
        return;
    }


    /**
     *  Gets the <code>Publisher</code> specified by its <code>Id</code>.
     *
     *  @param  publisherId <code>Id</code> of the <code>Publisher</code>
     *  @return the publisher
     *  @throws org.osid.NotFoundException <code>publisherId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>publisherId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.subscription.Publisher publisher = this.publishers.get(publisherId);
        if (publisher == null) {
            throw new org.osid.NotFoundException("publisher not found: " + publisherId);
        }

        return (publisher);
    }


    /**
     *  Gets all <code>Publishers</code>. In plenary mode, the returned
     *  list contains all known publishers or an error
     *  results. Otherwise, the returned list may contain only those
     *  publishers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Publishers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.publisher.ArrayPublisherList(this.publishers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.publishers.clear();
        super.close();
        return;
    }
}
