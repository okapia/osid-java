//
// AbstractMapProvisionLookupSession
//
//    A simple framework for providing a Provision lookup service
//    backed by a fixed collection of provisions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Provision lookup service backed by a
 *  fixed collection of provisions. The provisions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Provisions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProvisionLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractProvisionLookupSession
    implements org.osid.provisioning.ProvisionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.Provision> provisions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.Provision>());


    /**
     *  Makes a <code>Provision</code> available in this session.
     *
     *  @param  provision a provision
     *  @throws org.osid.NullArgumentException <code>provision<code>
     *          is <code>null</code>
     */

    protected void putProvision(org.osid.provisioning.Provision provision) {
        this.provisions.put(provision.getId(), provision);
        return;
    }


    /**
     *  Makes an array of provisions available in this session.
     *
     *  @param  provisions an array of provisions
     *  @throws org.osid.NullArgumentException <code>provisions<code>
     *          is <code>null</code>
     */

    protected void putProvisions(org.osid.provisioning.Provision[] provisions) {
        putProvisions(java.util.Arrays.asList(provisions));
        return;
    }


    /**
     *  Makes a collection of provisions available in this session.
     *
     *  @param  provisions a collection of provisions
     *  @throws org.osid.NullArgumentException <code>provisions<code>
     *          is <code>null</code>
     */

    protected void putProvisions(java.util.Collection<? extends org.osid.provisioning.Provision> provisions) {
        for (org.osid.provisioning.Provision provision : provisions) {
            this.provisions.put(provision.getId(), provision);
        }

        return;
    }


    /**
     *  Removes a Provision from this session.
     *
     *  @param  provisionId the <code>Id</code> of the provision
     *  @throws org.osid.NullArgumentException <code>provisionId<code> is
     *          <code>null</code>
     */

    protected void removeProvision(org.osid.id.Id provisionId) {
        this.provisions.remove(provisionId);
        return;
    }


    /**
     *  Gets the <code>Provision</code> specified by its <code>Id</code>.
     *
     *  @param  provisionId <code>Id</code> of the <code>Provision</code>
     *  @return the provision
     *  @throws org.osid.NotFoundException <code>provisionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>provisionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Provision getProvision(org.osid.id.Id provisionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.Provision provision = this.provisions.get(provisionId);
        if (provision == null) {
            throw new org.osid.NotFoundException("provision not found: " + provisionId);
        }

        return (provision);
    }


    /**
     *  Gets all <code>Provisions</code>. In plenary mode, the returned
     *  list contains all known provisions or an error
     *  results. Otherwise, the returned list may contain only those
     *  provisions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Provisions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.provision.ArrayProvisionList(this.provisions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.provisions.clear();
        super.close();
        return;
    }
}
