//
// AbstractAccount.java
//
//     Defines an Account.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.account.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Account</code>.
 */

public abstract class AbstractAccount
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.financials.Account {

    private boolean credit = false;
    private String code;
    private final java.util.Collection<org.osid.financials.records.AccountRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this account is a credit or debit account. 
     *
     *  @return <code> true </code> if credits increase the balance,
     *          <code> false </code> if credits decrease the balance
     */

    @OSID @Override
    public boolean isCreditBalance() {
        return (this.credit);
    }


    /**
     *  Sets the credit balance flag.
     *
     *  @param credit <code> true </code> if credits increase the
     *         balance, <code> false </code> if credits decrease the
     *         balance
     */

    protected void setCreditBalance(boolean credit) {
        this.credit = credit;
        return;
    }


    /**
     *  Gets a code for this G/L account. 
     *
     *  @return an account code 
     */

    @OSID @Override
    public String getCode() {
        return (this.code);
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     */

    protected void setCode(String code) {
        nullarg(code, "code");
        this.code = code;
        return;
    }


    /**
     *  Tests if this account supports the given record
     *  <code>Type</code>.
     *
     *  @param  accountRecordType an account record type 
     *  @return <code>true</code> if the accountRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type accountRecordType) {
        for (org.osid.financials.records.AccountRecord record : this.records) {
            if (record.implementsRecordType(accountRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Account</code> record <code>Type</code>.
     *
     *  @param  accountRecordType the account record type 
     *  @return the account record 
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(accountRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.AccountRecord getAccountRecord(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.AccountRecord record : this.records) {
            if (record.implementsRecordType(accountRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(accountRecordType + " is not supported");
    }


    /**
     *  Adds a record to this account. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param accountRecord the account record
     *  @param accountRecordType account record type
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecord</code> or
     *          <code>accountRecordTypeaccount</code> is
     *          <code>null</code>
     */
            
    protected void addAccountRecord(org.osid.financials.records.AccountRecord accountRecord, 
                                    org.osid.type.Type accountRecordType) {
        
        nullarg(accountRecord, "account record");
        addRecordType(accountRecordType);
        this.records.add(accountRecord);
        
        return;
    }
}
