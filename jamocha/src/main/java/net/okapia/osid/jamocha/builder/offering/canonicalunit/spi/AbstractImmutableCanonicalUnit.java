//
// AbstractImmutableCanonicalUnit.java
//
//     Wraps a mutable CanonicalUnit to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.canonicalunit.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>CanonicalUnit</code> to hide modifiers. This
 *  wrapper provides an immutized CanonicalUnit from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying canonicalUnit whose state changes are visible.
 */

public abstract class AbstractImmutableCanonicalUnit
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOperableOsidObject
    implements org.osid.offering.CanonicalUnit {

    private final org.osid.offering.CanonicalUnit canonicalUnit;


    /**
     *  Constructs a new <code>AbstractImmutableCanonicalUnit</code>.
     *
     *  @param canonicalUnit the canonical unit to immutablize
     *  @throws org.osid.NullArgumentException <code>canonicalUnit</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCanonicalUnit(org.osid.offering.CanonicalUnit canonicalUnit) {
        super(canonicalUnit);
        this.canonicalUnit = canonicalUnit;
        return;
    }


    /**
     *  Gets the title for this canonical unit. 
     *
     *  @return the title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.canonicalUnit.getTitle());
    }


    /**
     *  Gets the code for this canonical unit. 
     *
     *  @return the code 
     */

    @OSID @Override
    public String getCode() {
        return (this.canonicalUnit.getCode());
    }


    /**
     *  Gets the cyclic period <code> Ids </code> in which this CU can be 
     *  offered. If there are no time cycles, then it can be offered any time. 
     *
     *  @return a list of cyclic period <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getOfferedCyclicTimePeriodIds() {
        return (this.canonicalUnit.getOfferedCyclicTimePeriodIds());
    }


    /**
     *  Gets the cyclic periods in which this CU can be offered. If there are 
     *  no time cycles, then it can be offered any time. 
     *
     *  @return a list of cyclic periods 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getOfferedCyclicTimePeriods()
        throws org.osid.OperationFailedException {

        return (this.canonicalUnit.getOfferedCyclicTimePeriods());
    }


    /**
     *  Tests if this canonical has results when offered. 
     *
     *  @return <code> true </code> if this canonical has results, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasResults() {
        return (this.canonicalUnit.hasResults());
    }


    /**
     *  Gets the various result option <code> Ids </code> allowed for
     *  results.
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasResults()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.IdList getResultOptionIds() {
        return (this.canonicalUnit.getResultOptionIds());
    }


    /**
     *  Gets the various result options allowed for this canonical
     *  unit.
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> hasResults()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getResultOptions()
        throws org.osid.OperationFailedException {

        return (this.canonicalUnit.getResultOptions());
    }


    /**
     *  Tests if this canonical has sponsors. 
     *
     *  @return <code> true </code> if this canonical has sponsors, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.canonicalUnit.hasSponsors());
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        return (this.canonicalUnit.getSponsorIds());
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        return (this.canonicalUnit.getSponsors());
    }


    /**
     *  Gets the record corresponding to the given <code> CanonicalUnit 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  canonicalUnitRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(canonicalUnitRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  canonicalUnitRecordType the type of canonical unit record to 
     *          retrieve 
     *  @return the canonical unit record 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(canonicalUnitRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.records.CanonicalUnitRecord getCanonicalUnitRecord(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException {

        return (this.canonicalUnit.getCanonicalUnitRecord(canonicalUnitRecordType));
    }
}

