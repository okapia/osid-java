//
// CourseOfferingMiter.java
//
//     Defines a CourseOffering miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.courseoffering;


/**
 *  Defines a <code>CourseOffering</code> miter for use with the builders.
 */

public interface CourseOfferingMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.CourseOffering {


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public void setCourse(org.osid.course.Course course);


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public void setTerm(org.osid.course.Term term);


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public void setTitle(org.osid.locale.DisplayText title);


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setNumber(String number);


    /**
     *  Adds an instructor.
     *
     *  @param instructor an instructor
     *  @throws org.osid.NullArgumentException <code>instructor</code>
     *          is <code>null</code>
     */

    public void addInstructor(org.osid.resource.Resource instructor);


    /**
     *  Sets all the instructors.
     *
     *  @param instructors a collection of instructors
     *  @throws org.osid.NullArgumentException
     *          <code>instructors</code> is <code>null</code>
     */

    public void setInstructors(java.util.Collection<org.osid.resource.Resource> instructors);


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public void addSponsor(org.osid.resource.Resource sponsor);


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors);


    /**
     *  Adds a credit smount.
     *
     *  @param credit a credit amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void addCreditAmount(org.osid.grading.Grade amount);


    /**
     *  Sets all the credit amount options.
     *
     *  @param amounts a collection of credit amounts
     *  @throws org.osid.NullArgumentException <code>amounts</code>
     *          is <code>null</code>
     */

    public void setCreditAmounts(java.util.Collection<org.osid.grading.Grade> amounts);


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public void addGradingOption(org.osid.grading.GradeSystem option);


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public void setGradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options);


    /**
     *  Sets the requires registration.
     *
     *  @param requires <code> true </code> if this course requires
     *         advance registration, <code> false </code> otherwise
     */

    public void setRequiresRegistration(boolean requires);


    /**
     *  Sets the minimum seats.
     *
     *  @param seats a minimum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code> is
     *          negative
     */

    public void setMinimumSeats(long seats);


    /**
     *  Sets the maximum seats.
     *
     *  @param seats a maximum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code> is
     *          negative
     */

    public void setMaximumSeats(long seats);


    /**
     *  Sets the URL.
     *
     *  @param url the URL
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public void setURL(String url);


    /**
     *  Sets the schedule info.
     *
     *  @param info a schedule info
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    public void setScheduleInfo(org.osid.locale.DisplayText info);


    /**
     *  Sets the event.
     *
     *  @param event an event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public void setEvent(org.osid.calendaring.Event event);


    /**
     *  Adds a CourseOffering record.
     *
     *  @param record a courseOffering record
     *  @param recordType the type of courseOffering record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCourseOfferingRecord(org.osid.course.records.CourseOfferingRecord record, org.osid.type.Type recordType);
}       


