//
// AbstractUnknownResourceVelocity.java
//
//     Defines an unknown ResourceVelocity.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.mapping.path.resourcevelocity.spi;


/**
 *  Defines an unknown <code>ResourceVelocity</code>.
 */

public abstract class AbstractUnknownResourceVelocity
    extends net.okapia.osid.jamocha.mapping.path.resourcevelocity.spi.AbstractResourceVelocity
    implements org.osid.mapping.path.ResourceVelocity {

    protected static final String OBJECT = "osid.mapping.path.ResourceVelocity";


    /**
     *  Constructs a new <code>AbstractUnknownResourceVelocity</code>.
     */

    public AbstractUnknownResourceVelocity() {
        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setResource(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());

        // setSpeed();
        // setPosition()
        // setHeading() 
        
        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownResourceVelocity</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownResourceVelocity(boolean optional) {
        this();
        setPath(new net.okapia.osid.jamocha.nil.mapping.path.path.UnknownPath());
        return;
    }
}
