//
// AbstractAdapterPaymentLookupSession.java
//
//    A Payment lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Payment lookup session adapter.
 */

public abstract class AbstractAdapterPaymentLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.billing.payment.PaymentLookupSession {

    private final org.osid.billing.payment.PaymentLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPaymentLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPaymentLookupSession(org.osid.billing.payment.PaymentLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code Payment} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPayments() {
        return (this.session.canLookupPayments());
    }


    /**
     *  A complete view of the {@code Payment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePaymentView() {
        this.session.useComparativePaymentView();
        return;
    }


    /**
     *  A complete view of the {@code Payment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPaymentView() {
        this.session.usePlenaryPaymentView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include payments in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the {@code Payment} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Payment} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Payment} and
     *  retained for compatibility.
     *
     *  @param paymentId {@code Id} of the {@code Payment}
     *  @return the payment
     *  @throws org.osid.NotFoundException {@code paymentId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code paymentId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.Payment getPayment(org.osid.id.Id paymentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayment(paymentId));
    }


    /**
     *  Gets a {@code PaymentList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  payments specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Payments} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *
     *  @param  paymentIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Payment} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code paymentIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByIds(org.osid.id.IdList paymentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsByIds(paymentIds));
    }


    /**
     *  Gets a {@code PaymentList} corresponding to the given
     *  payment genus {@code Type} which does not include
     *  payments of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  paymentGenusType a payment genus type 
     *  @return the returned {@code Payment} list
     *  @throws org.osid.NullArgumentException
     *          {@code paymentGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByGenusType(org.osid.type.Type paymentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsByGenusType(paymentGenusType));
    }


    /**
     *  Gets a {@code PaymentList} corresponding to the given
     *  payment genus {@code Type} and include any additional
     *  payments with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  paymentGenusType a payment genus type 
     *  @return the returned {@code Payment} list
     *  @throws org.osid.NullArgumentException
     *          {@code paymentGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByParentGenusType(org.osid.type.Type paymentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsByParentGenusType(paymentGenusType));
    }


    /**
     *  Gets a {@code PaymentList} containing the given
     *  payment record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  paymentRecordType a payment record type 
     *  @return the returned {@code Payment} list
     *  @throws org.osid.NullArgumentException
     *          {@code paymentRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByRecordType(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsByRecordType(paymentRecordType));
    }


    /**
     *  Gets a {@code PaymentList} in the given period. In
     *  plenary mode, the returned list contains all known payments or
     *  an error results. Otherwise, the returned list may contain
     *  only those payments that are accessible through this session.
     *
     *  @param  periodId a period {@code Id} 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.NullArgumentException {@code periodId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriod(org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsByPeriod(periodId));
    }


    /**
     *  Gets a {@code PaymentList} for the given payer. In plenary
     *  mode, the returned list contains all known payments or an
     *  error results. Otherwise, the returned list may contain only
     *  those payments that are accessible through this session.
     *
     *  @param  payerId a payer {@code Id} 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.NullArgumentException {@code payerId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayer(org.osid.id.Id payerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsForPayer(payerId));
    }


    /**
     *  Gets a {@code PaymentList} for the given payer in a billing
     *  period. In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list may
     *  contain only those payments that are accessible through this
     *  session.
     *
     *  @param  payerId a payer {@code Id} 
     *  @param  periodId a period {@code Id} 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.NullArgumentException {@code payerId} or 
     *          {@code periodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriodForPayer(org.osid.id.Id payerId, 
                                                                            org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsByPeriodForPayer(payerId, periodId));
    }

    
    /**
     *  Gets a {@code PaymentList} for the given payer made within the
     *  given date range inclusive. In plenary mode, the returned list
     *  contains all known payments or an error results. Otherwise,
     *  the returned list may contain only those payments that are
     *  accessible through this session.
     *
     *  @param  payerId a payer {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code payerId} or 
     *          {@code periodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayerOnDate(org.osid.id.Id payerId, 
                                                                          org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsForPayerOnDate(payerId, from, to));
    }


    /**
     *  Gets a {@code PaymentList} for the given customer. In plenary
     *  mode, the returned list contains all known payments or an
     *  error results. Otherwise, the returned list may contain only
     *  those payments that are accessible through this session.
     *
     *  @param  customerId a customer {@code Id} 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.NullArgumentException {@code customerId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForCustomer(org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsForCustomer(customerId));
    }


    /**
     *  Gets a {@code PaymentList} for the given customer and billing
     *  period. In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list may
     *  contain only those payments that are accessible through this
     *  session.
     *
     *  @param  customerId a customer {@code Id} 
     *  @param  periodId a period {@code Id} 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.NullArgumentException {@code customerId} or
     *          {@code periodId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriodForCustomer(org.osid.id.Id customerId, 
                                                                               org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsByPeriodForCustomer(customerId, periodId));
    }


    /**
     *  Gets a {@code PaymentList} for the given customer made within
     *  the given date range inclusive. In plenary mode, the returned
     *  list contains all known payments or an error
     *  results. Otherwise, the returned list may contain only those
     *  payments that are accessible through this session.
     *
     *  @param  customerId a customer {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code customerId} or 
     *          {@code periodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForCustomerOnDate(org.osid.id.Id customerId, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsForCustomerOnDate(customerId, from, to));
    }


    /**
     *  Gets a {@code PaymentList} for the given payer and
     *  customer. In plenary mode, the returned list contains all
     *  known payments or an error results. Otherwise, the returned
     *  list may contain only those payments that are accessible
     *  through this session.
     *
     *  @param  payerId a payer {@code Id} 
     *  @param  customerId a customerId {@code Id} 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.NullArgumentException {@code payerId} or 
     *          {@code customerId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayerAndCustomer(org.osid.id.Id payerId, 
                                                                               org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsForPayerAndCustomer(payerId, customerId));
    }

    
    /**
     *  Gets a {@code PaymentList} for the given payer and customer in
     *  a billing period. In plenary mode, the returned list contains
     *  all known payments or an error results. Otherwise, the
     *  returned list may contain only those payments that are
     *  accessible through this session.
     *
     *  @param  payerId a payer {@code Id} 
     *  @param  customerId a customerId {@code Id} 
     *  @param  periodId a period {@code Id} 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.NullArgumentException {@code payerId, customerId, 
     *         } or {@code periodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriodForPayerAndCustomer(org.osid.id.Id payerId, 
                                                                                       org.osid.id.Id customerId, 
                                                                                       org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsByPeriodForPayerAndCustomer(payerId, customerId, periodId));
    }


    /**
     *  Gets a {@code PaymentList} for the given customer and payer
     *  made within the given date range inclusive. In plenary mode,
     *  the returned list contains all known payments or an error
     *  results.  Otherwise, the returned list may contain only those
     *  payments that are accessible through this session.
     *
     *  @param  payerId a payer {@code Id} 
     *  @param  customerId a customerId {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Payment} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code payerId,
     *         customerId}, or {@code periodId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayerAndCustomerOnDate(org.osid.id.Id payerId, 
                                                                                     org.osid.id.Id customerId, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaymentsForPayerAndCustomerOnDate(payerId, customerId, from, to));
    }

    
    /**
     *  Gets all {@code Payments}. 
     *
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Payments} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPayments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayments());
    }
}
