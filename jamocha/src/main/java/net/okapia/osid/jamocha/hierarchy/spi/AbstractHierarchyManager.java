//
// AbstractHierarchyManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractHierarchyManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.hierarchy.HierarchyManager,
               org.osid.hierarchy.HierarchyProxyManager {

    private final Types hierarchyRecordTypes               = new TypeRefSet();
    private final Types hierarchySearchRecordTypes         = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractHierarchyManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractHierarchyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. Visible federation allows for 
     *  selecting among multiple hierarchies. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if hierarchy traversal is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyTraversal() {
        return (false);
    }


    /**
     *  Tests if hierarchy design is supported. 
     *
     *  @return <code> true </code> if hierarchy design is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if hierarchy sequencing is supported. 
     *
     *  @return <code> true </code> if hierarchy sequencing is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchySequencing() {
        return (false);
    }


    /**
     *  Tests if hierarchy structure notification is supported. 
     *
     *  @return <code> true </code> if hierarchy structure notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyStructureNotification() {
        return (false);
    }


    /**
     *  Tests if a hierarchy lookup is supported. 
     *
     *  @return <code> true </code> if hierarchy lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyLookup() {
        return (false);
    }


    /**
     *  Tests if a hierarchy query is supported. 
     *
     *  @return <code> true </code> if hierarchy query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyQuery() {
        return (false);
    }


    /**
     *  Tests if a hierarchy search is supported. 
     *
     *  @return <code> true </code> if hierarchy search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchySearch() {
        return (false);
    }


    /**
     *  Tests if a hierarchy administration is supported. 
     *
     *  @return <code> true </code> if hierarchy administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyAdmin() {
        return (false);
    }


    /**
     *  Tests if hierarchy notification is supported. Messages may be sent 
     *  when hierarchies are created, modified, or deleted. 
     *
     *  @return <code> true </code> if hierarchy notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyNotification() {
        return (false);
    }


    /**
     *  Gets the supported <code> Hierarchy </code> types. 
     *
     *  @return a list containing the supported <code> Hierarchy </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHierarchyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.hierarchyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Hierarchy </code> record type is supported. 
     *
     *  @param  hierarchyRecordType a <code> Type </code> indicating a <code> 
     *          Hierarchy </code> record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> hierarchyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHierarchyRecordType(org.osid.type.Type hierarchyRecordType) {
        return (this.hierarchyRecordTypes.contains(hierarchyRecordType));
    }


    /**
     *  Adds support for a hierarchy record type.
     *
     *  @param hierarchyRecordType a hierarchy record type
     *  @throws org.osid.NullArgumentException
     *  <code>hierarchyRecordType</code> is <code>null</code>
     */

    protected void addHierarchyRecordType(org.osid.type.Type hierarchyRecordType) {
        this.hierarchyRecordTypes.add(hierarchyRecordType);
        return;
    }


    /**
     *  Removes support for a hierarchy record type.
     *
     *  @param hierarchyRecordType a hierarchy record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>hierarchyRecordType</code> is <code>null</code>
     */

    protected void removeHierarchyRecordType(org.osid.type.Type hierarchyRecordType) {
        this.hierarchyRecordTypes.remove(hierarchyRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Hierarchy </code> search record types. 
     *
     *  @return a list containing the supported <code> Hierarchy </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHierarchySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.hierarchySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Hierarchy </code> search record type is 
     *  supported. 
     *
     *  @param  hierarchySearchRecordType a <code> Type </code> indicating a 
     *          <code> Hierarchy </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          hierarchySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHierarchySearchRecordType(org.osid.type.Type hierarchySearchRecordType) {
        return (this.hierarchySearchRecordTypes.contains(hierarchySearchRecordType));
    }


    /**
     *  Adds support for a hierarchy search record type.
     *
     *  @param hierarchySearchRecordType a hierarchy search record type
     *  @throws org.osid.NullArgumentException
     *  <code>hierarchySearchRecordType</code> is <code>null</code>
     */

    protected void addHierarchySearchRecordType(org.osid.type.Type hierarchySearchRecordType) {
        this.hierarchySearchRecordTypes.add(hierarchySearchRecordType);
        return;
    }


    /**
     *  Removes support for a hierarchy search record type.
     *
     *  @param hierarchySearchRecordType a hierarchy search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>hierarchySearchRecordType</code> is <code>null</code>
     */

    protected void removeHierarchySearchRecordType(org.osid.type.Type hierarchySearchRecordType) {
        this.hierarchySearchRecordTypes.remove(hierarchySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  traversal service. 
     *
     *  @return a <code> HierarchyTraversalSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyTraversal() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyTraversalSession getHierarchyTraversalSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyTraversalSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  traversal service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyTraversalSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyTraversal() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyTraversalSession getHierarchyTraversalSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyTraversalSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  traversal service for the given hierarchy. 
     *
     *  @param  hierarchyId the <code> Id </code> of the hierarchy 
     *  @return the new <code> HierarchyTraversalSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> hierarchyid </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyTraversal() </code> or <code> 
     *          supportsVisibleFedaration() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyTraversalSession getHierarchyTraversalSessionForHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyTraversalSessionForHierarchy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  traversal service for the given hierarchy. 
     *
     *  @param  hierarchyId the <code> Id </code> of the hierarchy 
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyTraversalSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyid </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> hierarchyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyTraversal() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyTraversalSession getHierarchyTraversalSessionForHierarchy(org.osid.id.Id hierarchyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyTraversalSessionForHierarchy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyDesignSession getHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyDesignSession getHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  design service using for the given hierarchy. 
     *
     *  @param  hierarchyId the <code> Id </code> of the graph 
     *  @return a <code> HierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> hierarchyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyDesignSession getHierarchyDesignSessionForHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyDesignSessionForHierarchy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  design service using for the given hierarchy. 
     *
     *  @param  hierarchyId the <code> Id </code> of the hierarchy 
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> hierarchyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyDesignSession getHierarchyDesignSessionForHierarchy(org.osid.id.Id hierarchyId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyDesignSessionForHierarchy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  sequencing service. 
     *
     *  @return a <code> HierarchySequencingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchySequencing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySequencingSession getHierarchySequencingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchySequencingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  sequencing service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchySequencingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchySequencing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySequencingSession getHierarchySequencingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchySequencingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequencing 
     *  design service using for the given hierarchy. 
     *
     *  @param  hierarchyId the <code> Id </code> of the graph 
     *  @return a <code> HierarchySequencingSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> hierarchyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchySequencing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySequencingSession getHierarchySequencingSessionForHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchySequencingSessionForHierarchy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequencing 
     *  design service using for the given hierarchy. 
     *
     *  @param  hierarchyId the <code> Id </code> of the graph 
     *  @param  proxy a proxy 
     *  @return a <code> HierarchySequencingSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> hierarchyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchySequencing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySequencingSession getHierarchySequencingSessionForHierarchy(org.osid.id.Id hierarchyId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchySequencingSessionForHierarchy not implemented");
    }


    /**
     *  Gets the session for subscribing to notifications of changes within a 
     *  hierarchy structure. 
     *
     *  @param  hierarchyStructureReceiver a receiver 
     *  @return a <code> HierarchyStructureNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          hierarchyStructureReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyStructureNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyStructureNotificationSession getHierarchyStructureNotificationSession(org.osid.hierarchy.HierarchyStructureReceiver hierarchyStructureReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyStructureNotificationSession not implemented");
    }


    /**
     *  Gets the session for subscribing to notifications of changes within a 
     *  hierarchy structure. 
     *
     *  @param  hierarchyStructureReceiver a receiver 
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyStructureNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          hierarchyStructureReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyStructureNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyStructureNotificationSession getHierarchyStructureNotificationSession(org.osid.hierarchy.HierarchyStructureReceiver hierarchyStructureReceiver, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyStructureNotificationSession not implemented");
    }


    /**
     *  Gets the session for subscribing to notifications of changes within a 
     *  hierarchy structure for the given hierarchy. 
     *
     *  @param  hierarchyStructureReceiver a receiver 
     *  @param  hierarchyId the <code> Id </code> of the graph 
     *  @return a <code> HierarchyStructureNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> 
     *          hierarchyStructureReceiver </code> or <code> hierarchyId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyStructureNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyStructureNotificationSession getHierarchyStructureNotificationSessionForHierarchy(org.osid.hierarchy.HierarchyStructureReceiver hierarchyStructureReceiver, 
                                                                                                                         org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyStructureNotificationSessionForHierarchy not implemented");
    }


    /**
     *  Gets the session for subscribing to notifications of changes within a 
     *  hierarchy structure for the given hierarchy. 
     *
     *  @param  hierarchyStructureReceiver a receiver 
     *  @param  hierarchyId the <code> Id </code> of the hierarchy 
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyStructureNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> 
     *          hierarchyStructureReceiver, hierarchyId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyStructureNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyStructureNotificationSession getHierarchyStructureNotificationSessionForHierarchy(org.osid.hierarchy.HierarchyStructureReceiver hierarchyStructureReceiver, 
                                                                                                                         org.osid.id.Id hierarchyId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyStructureNotificationSessionForHierarchy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  lookup service. 
     *
     *  @return a <code> HierarchyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyLookupSession getHierarchyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyLookupSession getHierarchyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  query service. 
     *
     *  @return a <code> HierarchyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQuerySession getHierarchyQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQuerySession getHierarchyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  search service. 
     *
     *  @return a <code> HierarchySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySearchSession getHierarchySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySearchSession getHierarchySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchySearchSession not implemented");
    }


    /**
     *  Gets the hierarchy administrative session. 
     *
     *  @return a <code> HierarchyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyAdminSession getHierarchyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyAdminSession not implemented");
    }


    /**
     *  Gets the hierarchy administrative session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyAdminSession getHierarchyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyAdminSession not implemented");
    }


    /**
     *  Gets a hierarchy notification session. 
     *
     *  @param  hierarchyReceiver notification callback 
     *  @return a <code> HierarchyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> hierarchyReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyNotificationSession getHierarchyNotificationSession(org.osid.hierarchy.HierarchyReceiver hierarchyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyManager.getHierarchyNotificationSession not implemented");
    }


    /**
     *  Gets the hierarchy notification session. 
     *
     *  @param  hierarchyReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> hierarchyReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyNotificationSession getHierarchyNotificationSession(org.osid.hierarchy.HierarchyReceiver hierarchyReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hierarchy.HierarchyProxyManager.getHierarchyNotificationSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.hierarchyRecordTypes.clear();
        this.hierarchyRecordTypes.clear();

        this.hierarchySearchRecordTypes.clear();
        this.hierarchySearchRecordTypes.clear();

        return;
    }
}
