//
// MutableMapOfficeLookupSession
//
//    Implements an Office lookup service backed by a collection of
//    offices that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow;


/**
 *  Implements an Office lookup service backed by a collection of
 *  offices. The offices are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of offices can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapOfficeLookupSession
    extends net.okapia.osid.jamocha.core.workflow.spi.AbstractMapOfficeLookupSession
    implements org.osid.workflow.OfficeLookupSession {


    /**
     *  Constructs a new {@code MutableMapOfficeLookupSession}
     *  with no offices.
     */

    public MutableMapOfficeLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOfficeLookupSession} with a
     *  single office.
     *  
     *  @param office an office
     *  @throws org.osid.NullArgumentException {@code office}
     *          is {@code null}
     */

    public MutableMapOfficeLookupSession(org.osid.workflow.Office office) {
        putOffice(office);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOfficeLookupSession}
     *  using an array of offices.
     *
     *  @param offices an array of offices
     *  @throws org.osid.NullArgumentException {@code offices}
     *          is {@code null}
     */

    public MutableMapOfficeLookupSession(org.osid.workflow.Office[] offices) {
        putOffices(offices);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOfficeLookupSession}
     *  using a collection of offices.
     *
     *  @param offices a collection of offices
     *  @throws org.osid.NullArgumentException {@code offices}
     *          is {@code null}
     */

    public MutableMapOfficeLookupSession(java.util.Collection<? extends org.osid.workflow.Office> offices) {
        putOffices(offices);
        return;
    }

    
    /**
     *  Makes an {@code Office} available in this session.
     *
     *  @param office an office
     *  @throws org.osid.NullArgumentException {@code office{@code  is
     *          {@code null}
     */

    @Override
    public void putOffice(org.osid.workflow.Office office) {
        super.putOffice(office);
        return;
    }


    /**
     *  Makes an array of offices available in this session.
     *
     *  @param offices an array of offices
     *  @throws org.osid.NullArgumentException {@code offices{@code 
     *          is {@code null}
     */

    @Override
    public void putOffices(org.osid.workflow.Office[] offices) {
        super.putOffices(offices);
        return;
    }


    /**
     *  Makes collection of offices available in this session.
     *
     *  @param offices a collection of offices
     *  @throws org.osid.NullArgumentException {@code offices{@code  is
     *          {@code null}
     */

    @Override
    public void putOffices(java.util.Collection<? extends org.osid.workflow.Office> offices) {
        super.putOffices(offices);
        return;
    }


    /**
     *  Removes an Office from this session.
     *
     *  @param officeId the {@code Id} of the office
     *  @throws org.osid.NullArgumentException {@code officeId{@code 
     *          is {@code null}
     */

    @Override
    public void removeOffice(org.osid.id.Id officeId) {
        super.removeOffice(officeId);
        return;
    }    
}
