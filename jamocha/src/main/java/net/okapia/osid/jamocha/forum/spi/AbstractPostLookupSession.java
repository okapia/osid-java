//
// AbstractPostLookupSession.java
//
//    A starter implementation framework for providing a Post
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Post
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPosts(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPostLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.forum.PostLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.forum.Forum forum = new net.okapia.osid.jamocha.nil.forum.forum.UnknownForum();
    

    /**
     *  Gets the <code>Forum/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Forum Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getForumId() {
        return (this.forum.getId());
    }


    /**
     *  Gets the <code>Forum</code> associated with this 
     *  session.
     *
     *  @return the <code>Forum</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.forum);
    }


    /**
     *  Sets the <code>Forum</code>.
     *
     *  @param  forum the forum for this session
     *  @throws org.osid.NullArgumentException <code>forum</code>
     *          is <code>null</code>
     */

    protected void setForum(org.osid.forum.Forum forum) {
        nullarg(forum, "forum");
        this.forum = forum;
        return;
    }


    /**
     *  Tests if this user can perform <code>Post</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPosts() {
        return (true);
    }


    /**
     *  A complete view of the <code>Post</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePostView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Post</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPostView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include posts in forums which are children
     *  of this forum in the forum hierarchy.
     */

    @OSID @Override
    public void useFederatedForumView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this forum only.
     */

    @OSID @Override
    public void useIsolatedForumView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Post</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Post</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Post</code> and
     *  retained for compatibility.
     *
     *  @param  postId <code>Id</code> of the
     *          <code>Post</code>
     *  @return the post
     *  @throws org.osid.NotFoundException <code>postId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>postId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Post getPost(org.osid.id.Id postId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.forum.PostList posts = getPosts()) {
            while (posts.hasNext()) {
                org.osid.forum.Post post = posts.getNextPost();
                if (post.getId().equals(postId)) {
                    return (post);
                }
            }
        } 

        throw new org.osid.NotFoundException(postId + " not found");
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  posts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Posts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPosts()</code>.
     *
     *  @param  postIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>postIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByIds(org.osid.id.IdList postIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.forum.Post> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = postIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPost(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("post " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.forum.post.LinkedPostList(ret));
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  post genus <code>Type</code> which does not include
     *  posts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPosts()</code>.
     *
     *  @param  postGenusType a post genus type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByGenusType(org.osid.type.Type postGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.post.PostGenusFilterList(getPosts(), postGenusType));
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  post genus <code>Type</code> and include any additional
     *  posts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPosts()</code>.
     *
     *  @param  postGenusType a post genus type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByParentGenusType(org.osid.type.Type postGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPostsByGenusType(postGenusType));
    }


    /**
     *  Gets a <code>PostList</code> containing the given
     *  post record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPosts()</code>.
     *
     *  @param  postRecordType a post record type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByRecordType(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.post.PostRecordFilterList(getPosts(), postRecordType));
    }


    /**
     *  Gets a <code>PostList</code> in the given date range
     *  inclusive.  In plenary mode, the returned list contains all
     *  known posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through this
     *  session.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Post</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByDate(org.osid.calendaring.DateTime from, 
                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.post.PostFilterList(new TimestampFilter(from, to), getPosts()));
    }


    /**
     *  Gets a <code>PostList</code> for the given poster. In plenary
     *  mode, the returned list contains all known posts or an error
     *  results. Otherwise, the returned list may contain only those
     *  posts that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Post</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsForPoster(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.post.PostFilterList(new PosterFilter(resourceId), getPosts()));
    }

    
    /**
     *  Gets a <code>PostList</code> by the given poster and in the
     *  given date range inclusive. In plenary mode, the returned list
     *  contains all known posts or an error results. Otherwise, the
     *  returned list may contain only those posts that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Post</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByDateForPoster(org.osid.id.Id resourceId, 
                                                           org.osid.calendaring.DateTime from, 
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.post.PostFilterList(new TimestampFilter(from, to), getPostsForPoster(resourceId)));
    }

    
    /**
     *  Gets all <code>Posts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Posts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.forum.PostList getPosts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the post list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of posts
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.forum.PostList filterPostsOnViews(org.osid.forum.PostList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class PosterFilter
        implements net.okapia.osid.jamocha.inline.filter.forum.post.PostFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>PosterFilterList</code>.
         *
         *  @param resourceId the poster to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */

        public PosterFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "poster Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the PostEntryFilterList to filter the post list
         *  based on poster.
         *
         *  @param post the post
         *  @return <code>true</code> to pass the post,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.forum.Post post) {
            return (post.getPosterId().equals(this.resourceId));
        }
    }


    public static class TimestampFilter
        implements net.okapia.osid.jamocha.inline.filter.forum.post.PostFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;

        
        /**
         *  Constructs a new <code>TimestampFilter</code>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        public TimestampFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to = to;

            return;
        }


        /**
         *  Used by the PostFilterList to filter the post list
         *  based on date.
         *
         *  @param post the post
         *  @return <code>true</code> to pass the post,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.forum.Post post) {
            if (post.getTimestamp().isLess(this.from)) {
                return (false);
            }

            if (post.getTimestamp().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }    
}
