//
// UnknownDeviceEnabler.java
//
//     Defines an unknown DeviceEnabler.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.control.rules.deviceenabler;


/**
 *  Defines an unknown <code>DeviceEnabler</code>.
 */

public final class UnknownDeviceEnabler
    extends net.okapia.osid.jamocha.nil.control.rules.deviceenabler.spi.AbstractUnknownDeviceEnabler
    implements org.osid.control.rules.DeviceEnabler {


    /**
     *  Constructs a new Unknown<code>DeviceEnabler</code>.
     */

    public UnknownDeviceEnabler() {
        return;
    }


    /**
     *  Constructs a new Unknown<code>DeviceEnabler</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownDeviceEnabler(boolean optional) {
        super(optional);
        addDeviceEnablerRecord(new DeviceEnablerRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown DeviceEnabler.
     *
     *  @return an unknown DeviceEnabler
     */

    public static org.osid.control.rules.DeviceEnabler create() {
        return (net.okapia.osid.jamocha.builder.validator.control.rules.deviceenabler.DeviceEnablerValidator.validateDeviceEnabler(new UnknownDeviceEnabler()));
    }


    public class DeviceEnablerRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.control.rules.records.DeviceEnablerRecord {

        
        protected DeviceEnablerRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
