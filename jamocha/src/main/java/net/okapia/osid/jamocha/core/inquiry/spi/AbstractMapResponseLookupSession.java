//
// AbstractMapResponseLookupSession
//
//    A simple framework for providing a Response lookup service
//    backed by a fixed collection of responses.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Response lookup service backed by a
 *  fixed collection of responses. The responses are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Responses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapResponseLookupSession
    extends net.okapia.osid.jamocha.inquiry.spi.AbstractResponseLookupSession
    implements org.osid.inquiry.ResponseLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inquiry.Response> responses = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inquiry.Response>());


    /**
     *  Makes a <code>Response</code> available in this session.
     *
     *  @param  response a response
     *  @throws org.osid.NullArgumentException <code>response<code>
     *          is <code>null</code>
     */

    protected void putResponse(org.osid.inquiry.Response response) {
        this.responses.put(response.getId(), response);
        return;
    }


    /**
     *  Makes an array of responses available in this session.
     *
     *  @param  responses an array of responses
     *  @throws org.osid.NullArgumentException <code>responses<code>
     *          is <code>null</code>
     */

    protected void putResponses(org.osid.inquiry.Response[] responses) {
        putResponses(java.util.Arrays.asList(responses));
        return;
    }


    /**
     *  Makes a collection of responses available in this session.
     *
     *  @param  responses a collection of responses
     *  @throws org.osid.NullArgumentException <code>responses<code>
     *          is <code>null</code>
     */

    protected void putResponses(java.util.Collection<? extends org.osid.inquiry.Response> responses) {
        for (org.osid.inquiry.Response response : responses) {
            this.responses.put(response.getId(), response);
        }

        return;
    }


    /**
     *  Removes a Response from this session.
     *
     *  @param  responseId the <code>Id</code> of the response
     *  @throws org.osid.NullArgumentException <code>responseId<code> is
     *          <code>null</code>
     */

    protected void removeResponse(org.osid.id.Id responseId) {
        this.responses.remove(responseId);
        return;
    }


    /**
     *  Gets the <code>Response</code> specified by its <code>Id</code>.
     *
     *  @param  responseId <code>Id</code> of the <code>Response</code>
     *  @return the response
     *  @throws org.osid.NotFoundException <code>responseId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>responseId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Response getResponse(org.osid.id.Id responseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inquiry.Response response = this.responses.get(responseId);
        if (response == null) {
            throw new org.osid.NotFoundException("response not found: " + responseId);
        }

        return (response);
    }


    /**
     *  Gets all <code>Responses</code>. In plenary mode, the returned
     *  list contains all known responses or an error
     *  results. Otherwise, the returned list may contain only those
     *  responses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Responses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.response.ArrayResponseList(this.responses.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.responses.clear();
        super.close();
        return;
    }
}
