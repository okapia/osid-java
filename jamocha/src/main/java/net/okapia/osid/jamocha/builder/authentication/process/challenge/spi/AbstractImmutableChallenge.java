//
// AbstractImmutableChallenge.java
//
//     Wraps a mutable Challenge to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.process.challenge.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Challenge</code> to hide modifiers. This
 *  wrapper provides an immutized Challenge from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying challenge whose state changes are visible.
 */

public abstract class AbstractImmutableChallenge
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.authentication.process.Challenge {

    private final org.osid.authentication.process.Challenge challenge;


    /**
     *  Constructs a new <code>AbstractImmutableChallenge</code>.
     *
     *  @param challenge the challenge to immutablize
     *  @throws org.osid.NullArgumentException <code>challenge</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableChallenge(org.osid.authentication.process.Challenge challenge) {
        super(challenge);
        this.challenge = challenge;
        return;
    }


    /**
     *  Gets the challenge record corresponding to the given challenge record <code> 
     *  Type. </code> This method is used to retrieve an object implementing 
     *  the requested. The <code> challengeRecordType </code> may be the <code> 
     *  Type </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(challengeRecordType) </code> is <code> true </code> . 
     *
     *  @param  challengeRecordType the type of challenge record to retrieve 
     *  @return the challenge record 
     *  @throws org.osid.NullArgumentException <code> challengeRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(challengeRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.records.ChallengeRecord getChallengeRecord(org.osid.type.Type challengeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.challenge.getChallengeRecord(challengeRecordType));
    }
}

