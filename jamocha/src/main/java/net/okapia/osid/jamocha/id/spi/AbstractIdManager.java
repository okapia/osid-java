//
// AbstractIdManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.id.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractIdManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.id.IdManager,
               org.osid.id.IdProxyManager {


    /**
     *  Constructs a new <code>AbstractIdManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractIdManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if <code> Id </code> lookup is supported. 
     *
     *  @return <code> true </code> if <code> Id </code> lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdLookup() {
        return (false);
    }


    /**
     *  Tests if an <code> Id </code> issue service is supported. 
     *
     *  @return <code> true </code> if <code> Id </code> issuing is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdIssue() {
        return (false);
    }


    /**
     *  Tests if an <code> Id </code> administrative service is supported. 
     *
     *  @return <code> true </code> if <code> Id </code> administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of an Id batch service. 
     *
     *  @return <code> true </code> if an Id batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdBatch() {
        return (false);
    }


    /**
     *  Gets the session associated with the id lookup service. 
     *
     *  @return an <code> IdLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdLookupSession getIdLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.id.IdManager.getIdLookupSession not implemented");
    }


    /**
     *  Gets the session associated with the id lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IdLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdLookupSession getIdLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.id.IdProxyManager.getIdLookupSession not implemented");
    }


    /**
     *  Gets the session associated with the id issue service. 
     *
     *  @return an <code> IdIssueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdIssue() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdIssueSession getIdIssueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.id.IdManager.getIdIssueSession not implemented");
    }


    /**
     *  Gets the session associated with the id issue service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IdIssueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsIdIssue() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdIssueSession getIdIssueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.id.IdProxyManager.getIdIssueSession not implemented");
    }


    /**
     *  Gets the session associated with the id admin service. 
     *
     *  @return an <code> IdAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdAdminSession getIdAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.id.IdManager.getIdAdminSession not implemented");
    }


    /**
     *  Gets the session associated with the id administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IdAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsIdAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdAdminSession getIdAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.id.IdProxyManager.getIdAdminSession not implemented");
    }


    /**
     *  Gets an <code> IdBatchManager. </code> 
     *
     *  @return an <code> IdBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.batch.IdBatchManager getIdBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.id.IdManager.getIdBatchManager not implemented");
    }


    /**
     *  Gets an <code> IdnProxyManager. </code> 
     *
     *  @return an <code> IdBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.batch.IdBatchProxyManager getIdBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.id.IdProxyManager.getIdBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
