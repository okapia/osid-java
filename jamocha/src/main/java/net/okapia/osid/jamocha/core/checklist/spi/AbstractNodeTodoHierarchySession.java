//
// AbstractNodeTodoHierarchySession.java
//
//     Defines a Todo hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a todo hierarchy session for delivering a hierarchy
 *  of todos using the TodoNode interface.
 */

public abstract class AbstractNodeTodoHierarchySession
    extends net.okapia.osid.jamocha.checklist.spi.AbstractTodoHierarchySession
    implements org.osid.checklist.TodoHierarchySession {

    private java.util.Collection<org.osid.checklist.TodoNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root todo <code> Ids </code> in this hierarchy.
     *
     *  @return the root todo <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootTodoIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.checklist.todonode.TodoNodeToIdList(this.roots));
    }


    /**
     *  Gets the root todos in the todo hierarchy. A node
     *  with no parents is an orphan. While all todo <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root todos 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getRootTodos()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.todonode.TodoNodeToTodoList(new net.okapia.osid.jamocha.checklist.todonode.ArrayTodoNodeList(this.roots)));
    }


    /**
     *  Adds a root todo node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootTodo(org.osid.checklist.TodoNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root todo nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootTodos(java.util.Collection<org.osid.checklist.TodoNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root todo node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootTodo(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.checklist.TodoNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Todo </code> has any parents. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @return <code> true </code> if the todo has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentTodos(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getTodoNode(todoId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  todo.
     *
     *  @param  id an <code> Id </code> 
     *  @param  todoId the <code> Id </code> of a todo 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> todoId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> todoId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfTodo(org.osid.id.Id id, org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.checklist.TodoNodeList parents = getTodoNode(todoId).getParentTodoNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextTodoNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given todo. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @return the parent <code> Ids </code> of the todo 
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentTodoIds(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.todo.TodoToIdList(getParentTodos(todoId)));
    }


    /**
     *  Gets the parents of the given todo. 
     *
     *  @param  todoId the <code> Id </code> to query 
     *  @return the parents of the todo 
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getParentTodos(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.todonode.TodoNodeToTodoList(getTodoNode(todoId).getParentTodoNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  todo.
     *
     *  @param  id an <code> Id </code> 
     *  @param  todoId the Id of a todo 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> todoId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfTodo(org.osid.id.Id id, org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfTodo(id, todoId)) {
            return (true);
        }

        try (org.osid.checklist.TodoList parents = getParentTodos(todoId)) {
            while (parents.hasNext()) {
                if (isAncestorOfTodo(id, parents.getNextTodo().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a todo has any children. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @return <code> true </code> if the <code> todoId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildTodos(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTodoNode(todoId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  todo.
     *
     *  @param  id an <code> Id </code> 
     *  @param todoId the <code> Id </code> of a 
     *         todo
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> todoId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> todoId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfTodo(org.osid.id.Id id, org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfTodo(todoId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  todo.
     *
     *  @param  todoId the <code> Id </code> to query 
     *  @return the children of the todo 
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildTodoIds(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.todo.TodoToIdList(getChildTodos(todoId)));
    }


    /**
     *  Gets the children of the given todo. 
     *
     *  @param  todoId the <code> Id </code> to query 
     *  @return the children of the todo 
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getChildTodos(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.todonode.TodoNodeToTodoList(getTodoNode(todoId).getChildTodoNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  todo.
     *
     *  @param  id an <code> Id </code> 
     *  @param todoId the <code> Id </code> of a 
     *         todo
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> todoId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfTodo(org.osid.id.Id id, org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfTodo(todoId, id)) {
            return (true);
        }

        try (org.osid.checklist.TodoList children = getChildTodos(todoId)) {
            while (children.hasNext()) {
                if (isDescendantOfTodo(id, children.getNextTodo().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  todo.
     *
     *  @param  todoId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified todo node 
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getTodoNodeIds(org.osid.id.Id todoId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.checklist.todonode.TodoNodeToNode(getTodoNode(todoId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given todo.
     *
     *  @param  todoId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified todo node 
     *  @throws org.osid.NotFoundException <code> todoId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> todoId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoNode getTodoNodes(org.osid.id.Id todoId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTodoNode(todoId));
    }


    /**
     *  Closes this <code>TodoHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a todo node.
     *
     *  @param todoId the id of the todo node
     *  @throws org.osid.NotFoundException <code>todoId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>todoId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.checklist.TodoNode getTodoNode(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(todoId, "todo Id");
        for (org.osid.checklist.TodoNode todo : this.roots) {
            if (todo.getId().equals(todoId)) {
                return (todo);
            }

            org.osid.checklist.TodoNode r = findTodo(todo, todoId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(todoId + " is not found");
    }


    protected org.osid.checklist.TodoNode findTodo(org.osid.checklist.TodoNode node, 
                                                            org.osid.id.Id todoId)
	throws org.osid.OperationFailedException {

        try (org.osid.checklist.TodoNodeList children = node.getChildTodoNodes()) {
            while (children.hasNext()) {
                org.osid.checklist.TodoNode todo = children.getNextTodoNode();
                if (todo.getId().equals(todoId)) {
                    return (todo);
                }
                
                todo = findTodo(todo, todoId);
                if (todo != null) {
                    return (todo);
                }
            }
        }

        return (null);
    }
}
