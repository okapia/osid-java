//
// MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession
//
//    Implements a BrokerConstrainerEnabler lookup service backed by a collection of
//    brokerConstrainerEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a BrokerConstrainerEnabler lookup service backed by a collection of
 *  brokerConstrainerEnablers. The broker constrainer enablers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some brokerConstrainerEnablers may be compatible
 *  with more types than are indicated through these brokerConstrainerEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of broker constrainer enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractIndexedMapBrokerConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession} with
     *  no broker constrainer enabler.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession} with
     *  a single broker constrainer enabler.
     *
     *  @param distributor the distributor
     *  @param  brokerConstrainerEnabler an broker constrainer enabler
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerConstrainerEnabler}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.rules.BrokerConstrainerEnabler brokerConstrainerEnabler, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putBrokerConstrainerEnabler(brokerConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession} using
     *  an array of broker constrainer enablers.
     *
     *  @param distributor the distributor
     *  @param  brokerConstrainerEnablers an array of broker constrainer enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerConstrainerEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.rules.BrokerConstrainerEnabler[] brokerConstrainerEnablers, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putBrokerConstrainerEnablers(brokerConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession} using
     *  a collection of broker constrainer enablers.
     *
     *  @param distributor the distributor
     *  @param  brokerConstrainerEnablers a collection of broker constrainer enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerConstrainerEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBrokerConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                       java.util.Collection<? extends org.osid.provisioning.rules.BrokerConstrainerEnabler> brokerConstrainerEnablers,
                                                       org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putBrokerConstrainerEnablers(brokerConstrainerEnablers);
        return;
    }

    
    /**
     *  Makes a {@code BrokerConstrainerEnabler} available in this session.
     *
     *  @param  brokerConstrainerEnabler a broker constrainer enabler
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerConstrainerEnabler(org.osid.provisioning.rules.BrokerConstrainerEnabler brokerConstrainerEnabler) {
        super.putBrokerConstrainerEnabler(brokerConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of broker constrainer enablers available in this session.
     *
     *  @param  brokerConstrainerEnablers an array of broker constrainer enablers
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerConstrainerEnablers(org.osid.provisioning.rules.BrokerConstrainerEnabler[] brokerConstrainerEnablers) {
        super.putBrokerConstrainerEnablers(brokerConstrainerEnablers);
        return;
    }


    /**
     *  Makes collection of broker constrainer enablers available in this session.
     *
     *  @param  brokerConstrainerEnablers a collection of broker constrainer enablers
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerConstrainerEnablers(java.util.Collection<? extends org.osid.provisioning.rules.BrokerConstrainerEnabler> brokerConstrainerEnablers) {
        super.putBrokerConstrainerEnablers(brokerConstrainerEnablers);
        return;
    }


    /**
     *  Removes a BrokerConstrainerEnabler from this session.
     *
     *  @param brokerConstrainerEnablerId the {@code Id} of the broker constrainer enabler
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBrokerConstrainerEnabler(org.osid.id.Id brokerConstrainerEnablerId) {
        super.removeBrokerConstrainerEnabler(brokerConstrainerEnablerId);
        return;
    }    
}
