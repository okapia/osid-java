//
// PagingCheckResultList
//
//     Implements a paging CheckResultList.
//
//
// Tom Coppeto
// Okapia
// 17 June 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.rules.check.checkresult;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a paging CheckResultList. This list can be used to honor
 *  parameters set in search interfaces where the provider doesn't
 *  support poging. 
 *
 *  The <code>skip()</code> method is used to move to the desired
 *  starting position.
 */

public final class PagingCheckResultList
    extends net.okapia.osid.jamocha.rules.check.checkresult.spi.AbstractCheckResultList
    implements org.osid.rules.check.CheckResultList {

    private long index = 0;
    private long end   = 0;
    private org.osid.rules.check.CheckResultList list;
    

    /**
     *  Creates a new <code>PagingCheckResultList</code>.
     *
     *  @param checkResultList actviity <code>CheckResultList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>checkResultList</code> is <code>null</code>
     */

    public PagingCheckResultList(org.osid.rules.check.CheckResultList checkResultList, long start, long end) {
        nullarg(checkResultList, "checkResult list");
        cardinalarg(start, "start");
        cardinalarg(end, "end");

        this.list = checkResultList;
        this.end  = end;
        this.list.skip(start);
        
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.index >= this.end) {
            return (false);
        }

        return (this.list.hasNext());
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *  
     *  This method does not imply asynchronous usage. All OSID
     *  methods may block.
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        long n = this.list.available();
        if (n > (this.index - this.end)) {
            return (this.index - this.end);
        }

        return (n);
    }


    /**
     *  Skip the specified number of elements in the list. If the
     *  number skipped is greater than the number of elements in the
     *  list, hasNext() becomes false and available() returns zero as
     *  there are no more elements to retrieve.
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        this.list.skip(n);
        this.index += n;
        return;
    }


    /**
     *  Gets the next <code> CheckResult </code> in this list. 
     *
     *  @return the next <code> CheckResult </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> CheckResult </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.CheckResult getNextCheckResult()
        throws org.osid.OperationFailedException {

        if (!hasNext()) {
            throw new org.osid.IllegalStateException("no more elements available in checkResult list");
        }

        org.osid.rules.check.CheckResult checkResult = this.list.getNextCheckResult();
        ++this.index;
        return (checkResult);
    }

        
    /**
     *  Gets the next set of <code> CheckResult </code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param  n the number of <code> CheckResult </code> elements requested which 
     *          must be less than or equal to <code> available() </code> 
     *  @return an array of <code> CheckResult </code> elements. <code> </code> The 
     *          length of the array is less than or equal to the number 
     *          specified. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.CheckResult[] getNextCheckResults(long n)
        throws org.osid.OperationFailedException {

        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        org.osid.rules.check.CheckResult[] ret = this.list.getNextCheckResults(n);
        this.index += n;
        return (ret);
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.list.close();
        return;
    }
}
