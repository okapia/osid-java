//
// AbstractFederatingPersonLookupSession.java
//
//     An abstract federating adapter for a PersonLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PersonLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPersonLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.personnel.PersonLookupSession>
    implements org.osid.personnel.PersonLookupSession {

    private boolean parallel = false;
    private org.osid.personnel.Realm realm = new net.okapia.osid.jamocha.nil.personnel.realm.UnknownRealm();


    /**
     *  Constructs a new <code>AbstractFederatingPersonLookupSession</code>.
     */

    protected AbstractFederatingPersonLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.personnel.PersonLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Realm/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Realm Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.realm.getId());
    }


    /**
     *  Gets the <code>Realm</code> associated with this 
     *  session.
     *
     *  @return the <code>Realm</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.realm);
    }


    /**
     *  Sets the <code>Realm</code>.
     *
     *  @param  realm the realm for this session
     *  @throws org.osid.NullArgumentException <code>realm</code>
     *          is <code>null</code>
     */

    protected void setRealm(org.osid.personnel.Realm realm) {
        nullarg(realm, "realm");
        this.realm = realm;
        return;
    }


    /**
     *  Tests if this user can perform <code>Person</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPersons() {
        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            if (session.canLookupPersons()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Person</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePersonView() {
        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            session.useComparativePersonView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Person</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPersonView() {
        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            session.usePlenaryPersonView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include persons in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            session.useFederatedRealmView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            session.useIsolatedRealmView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Person</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Person</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Person</code> and
     *  retained for compatibility.
     *
     *  @param  personId <code>Id</code> of the
     *          <code>Person</code>
     *  @return the person
     *  @throws org.osid.NotFoundException <code>personId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>personId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Person getPerson(org.osid.id.Id personId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            try {
                return (session.getPerson(personId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(personId + " not found");
    }


    /**
     *  Gets a <code>PersonList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  persons specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Persons</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  personIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Person</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>personIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByIds(org.osid.id.IdList personIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.personnel.person.MutablePersonList ret = new net.okapia.osid.jamocha.personnel.person.MutablePersonList();

        try (org.osid.id.IdList ids = personIds) {
            while (ids.hasNext()) {
                ret.addPerson(getPerson(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PersonList</code> corresponding to the given
     *  person genus <code>Type</code> which does not include
     *  persons of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  personGenusType a person genus type 
     *  @return the returned <code>Person</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>personGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByGenusType(org.osid.type.Type personGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.person.FederatingPersonList ret = getPersonList();

        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            ret.addPersonList(session.getPersonsByGenusType(personGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PersonList</code> corresponding to the given
     *  person genus <code>Type</code> and include any additional
     *  persons with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  personGenusType a person genus type 
     *  @return the returned <code>Person</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>personGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByParentGenusType(org.osid.type.Type personGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.person.FederatingPersonList ret = getPersonList();

        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            ret.addPersonList(session.getPersonsByParentGenusType(personGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PersonList</code> containing the given
     *  person record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  personRecordType a person record type 
     *  @return the returned <code>Person</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByRecordType(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.person.FederatingPersonList ret = getPersonList();

        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            ret.addPersonList(session.getPersonsByRecordType(personRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Persons</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Persons</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersons()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.person.FederatingPersonList ret = getPersonList();

        for (org.osid.personnel.PersonLookupSession session : getSessions()) {
            ret.addPersonList(session.getPersons());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.personnel.person.FederatingPersonList getPersonList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.personnel.person.ParallelPersonList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.personnel.person.CompositePersonList());
        }
    }
}
