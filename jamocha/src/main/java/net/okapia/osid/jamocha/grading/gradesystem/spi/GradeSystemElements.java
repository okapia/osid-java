//
// GradeSystemElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradesystem.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class GradeSystemElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the GradeSystemElement Id.
     *
     *  @return the grade system element Id
     */

    public static org.osid.id.Id getGradeSystemEntityId() {
        return (makeEntityId("osid.grading.GradeSystem"));
    }


    /**
     *  Gets the GradeIds element Id.
     *
     *  @return the GradeIds element Id
     */

    public static org.osid.id.Id getGradeIds() {
        return (makeElementId("osid.grading.gradesystem.GradeIds"));
    }


    /**
     *  Gets the Grades element Id.
     *
     *  @return the Grades element Id
     */

    public static org.osid.id.Id getGrades() {
        return (makeElementId("osid.grading.gradesystem.Grades"));
    }


    /**
     *  Gets the LowestNumericScore element Id.
     *
     *  @return the LowestNumericScore element Id
     */

    public static org.osid.id.Id getLowestNumericScore() {
        return (makeElementId("osid.grading.gradesystem.LowestNumericScore"));
    }


    /**
     *  Gets the NumericScoreIncrement element Id.
     *
     *  @return the NumericScoreIncrement element Id
     */

    public static org.osid.id.Id getNumericScoreIncrement() {
        return (makeElementId("osid.grading.gradesystem.NumericScoreIncrement"));
    }


    /**
     *  Gets the HighestNumericScore element Id.
     *
     *  @return the HighestNumericScore element Id
     */

    public static org.osid.id.Id getHighestNumericScore() {
        return (makeElementId("osid.grading.gradesystem.HighestNumericScore"));
    }


    /**
     *  Gets the BasedOnGrades element Id.
     *
     *  @return the BasedOnGradeselement Id
     */

    public static org.osid.id.Id getBasedOnGrades() {
        return (makeElementId("osid.grading.gradesystem.BasedOnGrades"));
    }


    /**
     *  Gets the GradebookColumnId element Id.
     *
     *  @return the GradebookColumnId element Id
     */

    public static org.osid.id.Id getGradebookColumnId() {
        return (makeQueryElementId("osid.grading.gradesystem.GradebookColumnId"));
    }


    /**
     *  Gets the GradebookColumn element Id.
     *
     *  @return the GradebookColumn element Id
     */

    public static org.osid.id.Id getGradebookColumn() {
        return (makeQueryElementId("osid.grading.gradesystem.GradebookColumn"));
    }


    /**
     *  Gets the GradebookId element Id.
     *
     *  @return the GradebookId element Id
     */

    public static org.osid.id.Id getGradebookId() {
        return (makeQueryElementId("osid.grading.gradesystem.GradebookId"));
    }


    /**
     *  Gets the Gradebook element Id.
     *
     *  @return the Gradebook element Id
     */

    public static org.osid.id.Id getGradebook() {
        return (makeQueryElementId("osid.grading.gradesystem.Gradebook"));
    }


    /**
     *  Gets the BaseOnGrades element Id.
     *
     *  @return the BaseOnGrades element Id
     */

    public static org.osid.id.Id getBaseOnGrades() {
        return (makeElementId("osid.grading.gradesystem.BaseOnGrades"));
    }
}
