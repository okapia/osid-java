//
// AbstractQueryCourseLookupSession.java
//
//    An inline adapter that maps a CourseLookupSession to
//    a CourseQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CourseLookupSession to
 *  a CourseQuerySession.
 */

public abstract class AbstractQueryCourseLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractCourseLookupSession
    implements org.osid.course.CourseLookupSession {

    private boolean activeonly = false;
    private final org.osid.course.CourseQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCourseLookupSession.
     *
     *  @param querySession the underlying course query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCourseLookupSession(org.osid.course.CourseQuerySession querySession) {
        nullarg(querySession, "course query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>Course</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCourses() {
        return (this.session.canSearchCourses());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include courses in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only active courses are returned by methods in this session.
     */
     
    @OSID @Override
    public void useActiveCourseView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive courses are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCourseView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */

    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Course</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Course</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Course</code> and
     *  retained for compatibility.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  courseId <code>Id</code> of the
     *          <code>Course</code>
     *  @return the course
     *  @throws org.osid.NotFoundException <code>courseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Course getCourse(org.osid.id.Id courseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseQuery query = getQuery();
        query.matchId(courseId, true);
        org.osid.course.CourseList courses = this.session.getCoursesByQuery(query);
        if (courses.hasNext()) {
            return (courses.getNextCourse());
        } 
        
        throw new org.osid.NotFoundException(courseId + " not found");
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Courses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  courseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>courseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByIds(org.osid.id.IdList courseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseQuery query = getQuery();

        try (org.osid.id.IdList ids = courseIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCoursesByQuery(query));
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  course genus <code>Type</code> which does not include
     *  courses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  courseGenusType a course genus type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByGenusType(org.osid.type.Type courseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseQuery query = getQuery();
        query.matchGenusType(courseGenusType, true);
        return (this.session.getCoursesByQuery(query));
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  course genus <code>Type</code> and include any additional
     *  courses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  courseGenusType a course genus type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByParentGenusType(org.osid.type.Type courseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseQuery query = getQuery();
        query.matchParentGenusType(courseGenusType, true);
        return (this.session.getCoursesByQuery(query));
    }


    /**
     *  Gets a <code>CourseList</code> containing the given
     *  course record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  courseRecordType a course record type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByRecordType(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseQuery query = getQuery();
        query.matchRecordType(courseRecordType, true);
        return (this.session.getCoursesByQuery(query));
    }

    
    /**
     *  Gets all <code>Courses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @return a list of <code>Courses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCourses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.course.CourseQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCoursesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.CourseQuery getQuery() {
        org.osid.course.CourseQuery query = this.session.getCourseQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
