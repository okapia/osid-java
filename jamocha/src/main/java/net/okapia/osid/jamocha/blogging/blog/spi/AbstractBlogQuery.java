//
// AbstractBlogQuery.java
//
//     A template for making a Blog Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.blog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for blogs.
 */

public abstract class AbstractBlogQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.blogging.BlogQuery {

    private final java.util.Collection<org.osid.blogging.records.BlogQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the entry <code> Id </code> for this query. 
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches blogs with any entries. 
     *
     *  @param  match <code> true </code> to match blogs with any entries, 
     *          <code> false </code> to match blogs with no entries 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        return;
    }


    /**
     *  Sets the blog <code> Id </code> for this query to match blogs that 
     *  have the specified blog as an ancestor. 
     *
     *  @param  blogId a blog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorBlogId(org.osid.id.Id blogId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor blog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBlogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BlogQuery </code> is available. 
     *
     *  @return <code> true </code> if a blog query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBlogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a blog. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the blog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBlogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogQuery getAncestorBlogQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBlogQuery() is false");
    }


    /**
     *  Matches blogs with any ancestor. 
     *
     *  @param  match <code> true </code> to match blogs with any ancestor, 
     *          <code> false </code> to match root blogs 
     */

    @OSID @Override
    public void matchAnyAncestorBlog(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor blog terms. 
     */

    @OSID @Override
    public void clearAncestorBlogTerms() {
        return;
    }


    /**
     *  Sets the blog <code> Id </code> for this query to match blogs that 
     *  have the specified blog as a descendant. 
     *
     *  @param  blogId a blog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantBlogId(org.osid.id.Id blogId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant blog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBlogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BlogQuery </code> is available. 
     *
     *  @return <code> true </code> if a blog query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBlogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a blog. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the blog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBlogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogQuery getDescendantBlogQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBlogQuery() is false");
    }


    /**
     *  Matches blogs with any descendant. 
     *
     *  @param  match <code> true </code> to match blogs with any descendant, 
     *          <code> false </code> to match leaf blogs 
     */

    @OSID @Override
    public void matchAnyDescendantBlog(boolean match) {
        return;
    }


    /**
     *  Clears the descendant blog terms. 
     */

    @OSID @Override
    public void clearDescendantBlogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given blog query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a blog implementing the requested record.
     *
     *  @param blogRecordType a blog record type
     *  @return the blog query record
     *  @throws org.osid.NullArgumentException
     *          <code>blogRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blogRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.BlogQueryRecord getBlogQueryRecord(org.osid.type.Type blogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.BlogQueryRecord record : this.records) {
            if (record.implementsRecordType(blogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blogRecordType + " is not supported");
    }


    /**
     *  Adds a record to this blog query. 
     *
     *  @param blogQueryRecord blog query record
     *  @param blogRecordType blog record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBlogQueryRecord(org.osid.blogging.records.BlogQueryRecord blogQueryRecord, 
                                          org.osid.type.Type blogRecordType) {

        addRecordType(blogRecordType);
        nullarg(blogQueryRecord, "blog query record");
        this.records.add(blogQueryRecord);        
        return;
    }
}
