//
// AbstractContactSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractContactSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.contact.ContactSearchResults {

    private org.osid.contact.ContactList contacts;
    private final org.osid.contact.ContactQueryInspector inspector;
    private final java.util.Collection<org.osid.contact.records.ContactSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractContactSearchResults.
     *
     *  @param contacts the result set
     *  @param contactQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>contacts</code>
     *          or <code>contactQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractContactSearchResults(org.osid.contact.ContactList contacts,
                                            org.osid.contact.ContactQueryInspector contactQueryInspector) {
        nullarg(contacts, "contacts");
        nullarg(contactQueryInspector, "contact query inspectpr");

        this.contacts = contacts;
        this.inspector = contactQueryInspector;

        return;
    }


    /**
     *  Gets the contact list resulting from a search.
     *
     *  @return a contact list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.contact.ContactList getContacts() {
        if (this.contacts == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.contact.ContactList contacts = this.contacts;
        this.contacts = null;
	return (contacts);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.contact.ContactQueryInspector getContactQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  contact search record <code> Type. </code> This method must
     *  be used to retrieve a contact implementing the requested
     *  record.
     *
     *  @param contactSearchRecordType a contact search 
     *         record type 
     *  @return the contact search
     *  @throws org.osid.NullArgumentException
     *          <code>contactSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(contactSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.ContactSearchResultsRecord getContactSearchResultsRecord(org.osid.type.Type contactSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.contact.records.ContactSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(contactSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(contactSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record contact search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addContactRecord(org.osid.contact.records.ContactSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "contact record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
