//
// AbstractStepConstrainerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractStepConstrainerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.workflow.rules.StepConstrainerSearchResults {

    private org.osid.workflow.rules.StepConstrainerList stepConstrainers;
    private final org.osid.workflow.rules.StepConstrainerQueryInspector inspector;
    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractStepConstrainerSearchResults.
     *
     *  @param stepConstrainers the result set
     *  @param stepConstrainerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>stepConstrainers</code>
     *          or <code>stepConstrainerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractStepConstrainerSearchResults(org.osid.workflow.rules.StepConstrainerList stepConstrainers,
                                            org.osid.workflow.rules.StepConstrainerQueryInspector stepConstrainerQueryInspector) {
        nullarg(stepConstrainers, "step constrainers");
        nullarg(stepConstrainerQueryInspector, "step constrainer query inspectpr");

        this.stepConstrainers = stepConstrainers;
        this.inspector = stepConstrainerQueryInspector;

        return;
    }


    /**
     *  Gets the step constrainer list resulting from a search.
     *
     *  @return a step constrainer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainers() {
        if (this.stepConstrainers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.workflow.rules.StepConstrainerList stepConstrainers = this.stepConstrainers;
        this.stepConstrainers = null;
	return (stepConstrainers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.workflow.rules.StepConstrainerQueryInspector getStepConstrainerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  step constrainer search record <code> Type. </code> This method must
     *  be used to retrieve a stepConstrainer implementing the requested
     *  record.
     *
     *  @param stepConstrainerSearchRecordType a stepConstrainer search 
     *         record type 
     *  @return the step constrainer search
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(stepConstrainerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerSearchResultsRecord getStepConstrainerSearchResultsRecord(org.osid.type.Type stepConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.workflow.rules.records.StepConstrainerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(stepConstrainerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(stepConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record step constrainer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addStepConstrainerRecord(org.osid.workflow.rules.records.StepConstrainerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "step constrainer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
