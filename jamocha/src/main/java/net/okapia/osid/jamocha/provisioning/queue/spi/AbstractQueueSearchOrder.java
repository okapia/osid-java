//
// AbstractQueueSearchOdrer.java
//
//     Defines a QueueSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.queue.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code QueueSearchOrder}.
 */

public abstract class AbstractQueueSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorSearchOrder
    implements org.osid.provisioning.QueueSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.QueueSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by broker. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBroker(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a broker search order is available. 
     *
     *  @return <code> true </code> if a broker search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the broker search order. 
     *
     *  @return the broker search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBrokerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchOrder getBrokerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBrokerSearchOrder() is false");
    }


    /**
     *  Orders the results by queue size. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySize(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by estimated waiting time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEWA(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the can request provisionables flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCanSpecifyProvisionable(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  queueRecordType a queue record type 
     *  @return {@code true} if the queueRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code queueRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type queueRecordType) {
        for (org.osid.provisioning.records.QueueSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(queueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  queueRecordType the queue record type 
     *  @return the queue search order record
     *  @throws org.osid.NullArgumentException
     *          {@code queueRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(queueRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.provisioning.records.QueueSearchOrderRecord getQueueSearchOrderRecord(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.QueueSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(queueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this queue. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param queueRecord the queue search odrer record
     *  @param queueRecordType queue record type
     *  @throws org.osid.NullArgumentException
     *          {@code queueRecord} or
     *          {@code queueRecordTypequeue} is
     *          {@code null}
     */
            
    protected void addQueueRecord(org.osid.provisioning.records.QueueSearchOrderRecord queueSearchOrderRecord, 
                                     org.osid.type.Type queueRecordType) {

        addRecordType(queueRecordType);
        this.records.add(queueSearchOrderRecord);
        
        return;
    }
}
