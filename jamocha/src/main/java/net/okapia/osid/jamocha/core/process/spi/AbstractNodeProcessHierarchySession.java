//
// AbstractNodeProcessHierarchySession.java
//
//     Defines a Process hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a process hierarchy session for delivering a hierarchy
 *  of processes using the ProcessNode interface.
 */

public abstract class AbstractNodeProcessHierarchySession
    extends net.okapia.osid.jamocha.process.spi.AbstractProcessHierarchySession
    implements org.osid.process.ProcessHierarchySession {

    private java.util.Collection<org.osid.process.ProcessNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root process <code> Ids </code> in this hierarchy.
     *
     *  @return the root process <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootProcessIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.process.processnode.ProcessNodeToIdList(this.roots));
    }


    /**
     *  Gets the root processes in the process hierarchy. A node
     *  with no parents is an orphan. While all process <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root processes 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getRootProcesses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.process.processnode.ProcessNodeToProcessList(new net.okapia.osid.jamocha.process.processnode.ArrayProcessNodeList(this.roots)));
    }


    /**
     *  Adds a root process node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootProcess(org.osid.process.ProcessNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root process nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootProcesses(java.util.Collection<org.osid.process.ProcessNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root process node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootProcess(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.process.ProcessNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Process </code> has any parents. 
     *
     *  @param  processId a process <code> Id </code> 
     *  @return <code> true </code> if the process has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentProcesses(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getProcessNode(processId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  process.
     *
     *  @param  id an <code> Id </code> 
     *  @param  processId the <code> Id </code> of a process 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> processId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> processId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfProcess(org.osid.id.Id id, org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.process.ProcessNodeList parents = getProcessNode(processId).getParentProcessNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextProcessNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given process. 
     *
     *  @param  processId a process <code> Id </code> 
     *  @return the parent <code> Ids </code> of the process 
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentProcessIds(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.process.process.ProcessToIdList(getParentProcesses(processId)));
    }


    /**
     *  Gets the parents of the given process. 
     *
     *  @param  processId the <code> Id </code> to query 
     *  @return the parents of the process 
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getParentProcesses(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.process.processnode.ProcessNodeToProcessList(getProcessNode(processId).getParentProcessNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  process.
     *
     *  @param  id an <code> Id </code> 
     *  @param  processId the Id of a process 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> processId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfProcess(org.osid.id.Id id, org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfProcess(id, processId)) {
            return (true);
        }

        try (org.osid.process.ProcessList parents = getParentProcesses(processId)) {
            while (parents.hasNext()) {
                if (isAncestorOfProcess(id, parents.getNextProcess().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a process has any children. 
     *
     *  @param  processId a process <code> Id </code> 
     *  @return <code> true </code> if the <code> processId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildProcesses(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProcessNode(processId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  process.
     *
     *  @param  id an <code> Id </code> 
     *  @param processId the <code> Id </code> of a 
     *         process
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> processId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> processId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfProcess(org.osid.id.Id id, org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfProcess(processId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  process.
     *
     *  @param  processId the <code> Id </code> to query 
     *  @return the children of the process 
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildProcessIds(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.process.process.ProcessToIdList(getChildProcesses(processId)));
    }


    /**
     *  Gets the children of the given process. 
     *
     *  @param  processId the <code> Id </code> to query 
     *  @return the children of the process 
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getChildProcesses(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.process.processnode.ProcessNodeToProcessList(getProcessNode(processId).getChildProcessNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  process.
     *
     *  @param  id an <code> Id </code> 
     *  @param processId the <code> Id </code> of a 
     *         process
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> processId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfProcess(org.osid.id.Id id, org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfProcess(processId, id)) {
            return (true);
        }

        try (org.osid.process.ProcessList children = getChildProcesses(processId)) {
            while (children.hasNext()) {
                if (isDescendantOfProcess(id, children.getNextProcess().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  process.
     *
     *  @param  processId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified process node 
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getProcessNodeIds(org.osid.id.Id processId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.process.processnode.ProcessNodeToNode(getProcessNode(processId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given process.
     *
     *  @param  processId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified process node 
     *  @throws org.osid.NotFoundException <code> processId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> processId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessNode getProcessNodes(org.osid.id.Id processId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProcessNode(processId));
    }


    /**
     *  Closes this <code>ProcessHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a process node.
     *
     *  @param processId the id of the process node
     *  @throws org.osid.NotFoundException <code>processId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>processId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.process.ProcessNode getProcessNode(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(processId, "process Id");
        for (org.osid.process.ProcessNode process : this.roots) {
            if (process.getId().equals(processId)) {
                return (process);
            }

            org.osid.process.ProcessNode r = findProcess(process, processId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(processId + " is not found");
    }


    protected org.osid.process.ProcessNode findProcess(org.osid.process.ProcessNode node, 
                                                       org.osid.id.Id processId) 
	throws org.osid.OperationFailedException {

        try (org.osid.process.ProcessNodeList children = node.getChildProcessNodes()) {
            while (children.hasNext()) {
                org.osid.process.ProcessNode process = children.getNextProcessNode();
                if (process.getId().equals(processId)) {
                    return (process);
                }
                
                process = findProcess(process, processId);
                if (process != null) {
                    return (process);
                }
            }
        }

        return (null);
    }
}
