//
// ResultMiter.java
//
//     Defines a Result miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.result;


/**
 *  Defines a <code>Result</code> miter for use with the builders.
 */

public interface ResultMiter
    extends net.okapia.osid.jamocha.builder.spi.TemporalOsidObjectMiter,
            org.osid.offering.Result {


    /**
     *  Sets the participant.
     *
     *  @param participant a participant
     *  @throws org.osid.NullArgumentException
     *          <code>participant</code> is <code>null</code>
     */

    public void setParticipant(org.osid.offering.Participant participant);


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException
     *          <code>grade</code> is <code>null</code>
     */

    public void setGrade(org.osid.grading.Grade grade);


    /**
     *  Sets the value.
     *
     *  @param value a value
     *  @throws org.osid.NullArgumentException
     *          <code>value</code> is <code>null</code>
     */

    public void setValue(java.math.BigDecimal value);


    /**
     *  Adds a Result record.
     *
     *  @param record a result record
     *  @param recordType the type of result record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addResultRecord(org.osid.offering.records.ResultRecord record, org.osid.type.Type recordType);
}       


