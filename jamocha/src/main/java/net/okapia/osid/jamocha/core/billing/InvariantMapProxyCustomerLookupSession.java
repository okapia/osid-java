//
// InvariantMapProxyCustomerLookupSession
//
//    Implements a Customer lookup service backed by a fixed
//    collection of customers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements a Customer lookup service backed by a fixed
 *  collection of customers. The customers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCustomerLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractMapCustomerLookupSession
    implements org.osid.billing.CustomerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCustomerLookupSession} with no
     *  customers.
     *
     *  @param business the business
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCustomerLookupSession(org.osid.billing.Business business,
                                                  org.osid.proxy.Proxy proxy) {
        setBusiness(business);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCustomerLookupSession} with a single
     *  customer.
     *
     *  @param business the business
     *  @param customer a single customer
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code customer} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCustomerLookupSession(org.osid.billing.Business business,
                                                  org.osid.billing.Customer customer, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putCustomer(customer);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCustomerLookupSession} using
     *  an array of customers.
     *
     *  @param business the business
     *  @param customers an array of customers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code customers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCustomerLookupSession(org.osid.billing.Business business,
                                                  org.osid.billing.Customer[] customers, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putCustomers(customers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCustomerLookupSession} using a
     *  collection of customers.
     *
     *  @param business the business
     *  @param customers a collection of customers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code customers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCustomerLookupSession(org.osid.billing.Business business,
                                                  java.util.Collection<? extends org.osid.billing.Customer> customers,
                                                  org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putCustomers(customers);
        return;
    }
}
