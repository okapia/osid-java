//
// AbstractIndexedMapAccountLookupSession.java
//
//    A simple framework for providing an Account lookup service
//    backed by a fixed collection of accounts with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Account lookup service backed by a
 *  fixed collection of accounts. The accounts are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some accounts may be compatible
 *  with more types than are indicated through these account
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Accounts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAccountLookupSession
    extends AbstractMapAccountLookupSession
    implements org.osid.financials.AccountLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.financials.Account> accountsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.Account>());
    private final MultiMap<org.osid.type.Type, org.osid.financials.Account> accountsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.Account>());


    /**
     *  Makes an <code>Account</code> available in this session.
     *
     *  @param  account an account
     *  @throws org.osid.NullArgumentException <code>account<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAccount(org.osid.financials.Account account) {
        super.putAccount(account);

        this.accountsByGenus.put(account.getGenusType(), account);
        
        try (org.osid.type.TypeList types = account.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.accountsByRecord.put(types.getNextType(), account);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an account from this session.
     *
     *  @param accountId the <code>Id</code> of the account
     *  @throws org.osid.NullArgumentException <code>accountId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAccount(org.osid.id.Id accountId) {
        org.osid.financials.Account account;
        try {
            account = getAccount(accountId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.accountsByGenus.remove(account.getGenusType());

        try (org.osid.type.TypeList types = account.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.accountsByRecord.remove(types.getNextType(), account);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAccount(accountId);
        return;
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  account genus <code>Type</code> which does not include
     *  accounts of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known accounts or an error results. Otherwise,
     *  the returned list may contain only those accounts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  accountGenusType an account genus type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByGenusType(org.osid.type.Type accountGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.account.ArrayAccountList(this.accountsByGenus.get(accountGenusType)));
    }


    /**
     *  Gets an <code>AccountList</code> containing the given
     *  account record <code>Type</code>. In plenary mode, the
     *  returned list contains all known accounts or an error
     *  results. Otherwise, the returned list may contain only those
     *  accounts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  accountRecordType an account record type 
     *  @return the returned <code>account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByRecordType(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.account.ArrayAccountList(this.accountsByRecord.get(accountRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.accountsByGenus.clear();
        this.accountsByRecord.clear();

        super.close();

        return;
    }
}
