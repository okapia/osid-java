//
// AbstractRequisiteNotificationSession.java
//
//     A template for making RequisiteNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Requisite} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Requisite} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for requisite entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRequisiteNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.requisite.RequisiteNotificationSession {

    private boolean federated = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }

    
    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the {@code CourseCatalog}.
     *
     *  @param courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can register for {@code Requisite}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRequisiteNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeRequisiteNotification() </code>.
     */

    @OSID @Override
    public void reliableRequisiteNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableRequisiteNotifications() {
        return;
    }


    /**
     *  Acknowledge a requisite notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeRequisiteNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for requisites in course
     *  catalogs which are children of this course catalog in the
     *  course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new requisites. {@code
     *  RequisiteReceiver.newRequisite()} is invoked when a new {@code
     *  Requisite} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRequisites()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated requisites. {@code
     *  RequisiteReceiver.changedRequisite()} is invoked when a
     *  requisite is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRequisites()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated requisite. {@code
     *  RequisiteReceiver.changedRequisite()} is invoked when the
     *  specified requisite is changed.
     *
     *  @param requisiteId the {@code Id} of the {@code Requisite} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code requisiteId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted requisites. {@code
     *  RequisiteReceiver.deletedRequisite()} is invoked when a
     *  requisite is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRequisites()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted requisite. {@code
     *  RequisiteReceiver.deletedRequisite()} is invoked when the
     *  specified requisite is deleted.
     *
     *  @param requisiteId the {@code Id} of the
     *          {@code Requisite} to monitor
     *  @throws org.osid.NullArgumentException {@code requisiteId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of new course requirements. {@code
     *  RequisiteReceiver.newCourseRequirement()} is invoked when a
     *  new {@code CourseRequirement} appears in this course catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewCourseRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated course
     *  requirements. {@code
     *  RequisiteReceiver.changedCourseRequirement()} is invoked when
     *  a course requirement in this course catalog is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedCourseRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated course
     *  requirement. {@code
     *  RequisiteReceiver.changedCourseRequirement()} is invoked when
     *  the specified course requirement in this course catalog is
     *  changed.
     *
     *  @param  courseRequirementId the {@code Id} of the {@code 
     *          CourseRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *          courseRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedCourseRequirement(org.osid.id.Id courseRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted course
     *  requirements. {@code
     *  RequisiteReceiver.deletedCourseRequirement()} is invoked when
     *  a course requirement is deleted or removed from this course
     *  catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedCourseRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted course
     *  requirement. {@code
     *  RequisiteReceiver.deletedCourseRequirement()} is invoked when
     *  the specified course requirement is deleted or removed from
     *  this course catalog.
     *
     *  @param  courseRequirementId the {@code Id} of the {@code 
     *          CourseRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *          courseRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedCourseRequirement(org.osid.id.Id courseRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new program requirements. {@code
     *  RequisiteReceiver.newProgramRequirement()} is invoked when a
     *  new {@code ProgramRequirement} appears in this course catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewProgramRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated program
     *  requirements. {@code
     *  RequisiteReceiver.changedProgramRequirement()} is invoked when
     *  a program requirement in this course catalog is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedProgramRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated program
     *  requirement. {@code
     *  RequisiteReceiver.changedProgramRequirement()} is invoked when
     *  the specified program requirement in this course catalog is
     *  changed.
     *
     *  @param  programRequirementId the {@code Id} of the {@code 
     *          ProgramRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *          programRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedProgramRequirement(org.osid.id.Id programRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted program
     *  requirements. {@code
     *  RequisiteReceiver.deletedProgramRequirement()} is invoked when
     *  a program requirement is deleted or removed from this course
     *  catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedProgramRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted program
     *  requirement. {@code
     *  RequisiteReceiver.deletedProgramRequirement()} is invoked when
     *  the specified program requirement is deleted or removed from
     *  this course catalog.
     *
     *  @param  programRequirementId the {@code Id} of the {@code 
     *          ProgramRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *          programRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedProgramRequirement(org.osid.id.Id programRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new credential
     *  requirements. {@code
     *  RequisiteReceiver.newCredentialRequirement()} is invoked when
     *  a new {@code CredentialRequirement} appears in this course
     *  catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewCredentialRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated credential
     *  requirements. {@code
     *  RequisiteReceiver.changedCredentialRequirement()} is invoked
     *  when a credential requirement in this course catalog is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedCredentialRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated credential
     *  requirement.  {@code
     *  RequisiteReceiver.changedCredentialRequirement()} is invoked
     *  when the specified credential requirement in this course
     *  catalog is changed.
     *
     *  @param  credentialRequirementId the {@code Id} of the {@code 
     *          CredentialRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *         credentialRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedCredentialRequirement(org.osid.id.Id credentialRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted credential
     *  requirements. {@code
     *  RequisiteReceiver.deletedCredentialRequirement()} is invoked
     *  when a credential requirement is deleted or removed from this
     *  course catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedCredentialRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted credential
     *  requirement. {@code
     *  RequisiteReceiver.deletedCredentialRequirement()} is invoked
     *  when the specified credential requirement is deleted or
     *  removed from this course catalog.
     *
     *  @param  credentialRequirementId the {@code Id} of the {@code 
     *          CredentialRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *         credentialRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedCredentialRequirement(org.osid.id.Id credentialRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new learning objective
     *  requirements.  {@code
     *  RequisiteReceiver.newLearningObjectiveRequirement()} is
     *  invoked when a new {@code LearningObjectiveRequirement}
     *  appears in this course catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewLearningObjectiveRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated learning objective
     *  requirements.  {@code
     *  RequisiteReceiver.changedLearningObjectiveRequirement()} is
     *  invoked when a learning objective requirement in this course
     *  catalog is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedLearningObjectiveRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated learning objective
     *  requirement. {@code
     *  RequisiteReceiver.changedLearningObjectiveRequirement()} is
     *  invoked when the specified learning objective requirement in
     *  this course catalog is changed.
     *
     *  @param learningObjectiveRequirementId the {@code Id} of the
     *          {@code LearningObjectiveRequirement} to monitor
     *  @throws org.osid.NullArgumentException {@code 
     *          learningObjectiveRequirementId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted learning objective
     *  requirements.  {@code
     *  RequisiteReceiver.deletedLearningObjectiveRequirement()} is
     *  invoked when a learning objective requirement is deleted or
     *  removed from this course catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedLearningObjectiveRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted learning objective
     *  requirement. {@code
     *  RequisiteReceiver.deletedLearningObjectiveRequirement()} is
     *  invoked when the specified learning objective requirement is
     *  deleted or removed from this course catalog.
     *
     *  @param  learningObjectiveRequirementId the {@code Id} of the 
     *          {@code LearningObjectiveRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code 
     *          learningObjectiveRequirementId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new assessment
     *  requirements. {@code
     *  RequisiteReceiver.newAssessmentRequirement()} is invoked when
     *  a new {@code AssessmentRequirement} appears in this course
     *  catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewAssessmentRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated assessment
     *  requirements. {@code
     *  RequisiteReceiver.changedAssessmentRequirement()} is invoked
     *  when an assessment requirement in this course catalog is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedAssessmentRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated assessment
     *  requirement.  {@code
     *  RequisiteReceiver.changedAssessmentRequirement()} is invoked
     *  when the specified assessment requirement in this course
     *  catalog is changed.
     *
     *  @param  assessmentRequirementId the {@code Id} of the {@code 
     *          AssessmentRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedAssessmentRequirement(org.osid.id.Id assessmentRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted assessment
     *  requirements. {@code
     *  RequisiteReceiver.deletedAssessmentRequirement()} is invoked
     *  when an assessment requirement is deleted or removed from this
     *  course catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedAssessmentRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted assessment
     *  requirement. {@code
     *  RequisiteReceiver.deletedAssessmentRequirement()} is invoked
     *  when the specified assessment requirement is deleted or
     *  removed from this course catalog.
     *
     *  @param assessmentRequirementId the {@code Id} of the {@code
     *          AssessmentRequirement} to monitor
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedAssessmentRequirement(org.osid.id.Id assessmentRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new award requirements. {@code
     *  RequisiteReceiver.newAwardRequirement()} is invoked when a new
     *  {@code AwardRequirement} appears in this course catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewAwardRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated award
     *  requirements. {@code
     *  RequisiteReceiver.changedAwardRequirement()} is invoked when
     *  an award requirement in this course catalog is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedAwardRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated award
     *  requirement. {@code
     *  RequisiteReceiver.changedAwardRequirement()} is invoked when
     *  the specified award requirement in this course catalog is
     *  changed.
     *
     *  @param  awardRequirementId the {@code Id} of the {@code 
     *          AwardRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *         awardRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedAwardRequirement(org.osid.id.Id awardRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted award
     *  requirements. {@code
     *  RequisiteReceiver.deletedAwardRequirement()} is invoked when
     *  an award requirement is deleted or removed from this course
     *  catalog.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedAwardRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted award
     *  requirement. {@code
     *  RequisiteReceiver.deletedAwardRequirement()} is invoked when
     *  the specified award requirement is deleted or removed from
     *  this course catalog.
     *
     *  @param  awardRequirementId the {@code Id} of the {@code 
     *          AwardRequirement} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *         awardRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedAwardRequirement(org.osid.id.Id awardRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }
}
