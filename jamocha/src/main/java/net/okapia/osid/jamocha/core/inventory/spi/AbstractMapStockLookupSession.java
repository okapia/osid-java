//
// AbstractMapStockLookupSession
//
//    A simple framework for providing a Stock lookup service
//    backed by a fixed collection of stocks.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Stock lookup service backed by a
 *  fixed collection of stocks. The stocks are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Stocks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapStockLookupSession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractStockLookupSession
    implements org.osid.inventory.StockLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inventory.Stock> stocks = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inventory.Stock>());


    /**
     *  Makes a <code>Stock</code> available in this session.
     *
     *  @param  stock a stock
     *  @throws org.osid.NullArgumentException <code>stock<code>
     *          is <code>null</code>
     */

    protected void putStock(org.osid.inventory.Stock stock) {
        this.stocks.put(stock.getId(), stock);
        return;
    }


    /**
     *  Makes an array of stocks available in this session.
     *
     *  @param  stocks an array of stocks
     *  @throws org.osid.NullArgumentException <code>stocks<code>
     *          is <code>null</code>
     */

    protected void putStocks(org.osid.inventory.Stock[] stocks) {
        putStocks(java.util.Arrays.asList(stocks));
        return;
    }


    /**
     *  Makes a collection of stocks available in this session.
     *
     *  @param  stocks a collection of stocks
     *  @throws org.osid.NullArgumentException <code>stocks<code>
     *          is <code>null</code>
     */

    protected void putStocks(java.util.Collection<? extends org.osid.inventory.Stock> stocks) {
        for (org.osid.inventory.Stock stock : stocks) {
            this.stocks.put(stock.getId(), stock);
        }

        return;
    }


    /**
     *  Removes a Stock from this session.
     *
     *  @param  stockId the <code>Id</code> of the stock
     *  @throws org.osid.NullArgumentException <code>stockId<code> is
     *          <code>null</code>
     */

    protected void removeStock(org.osid.id.Id stockId) {
        this.stocks.remove(stockId);
        return;
    }


    /**
     *  Gets the <code>Stock</code> specified by its <code>Id</code>.
     *
     *  @param  stockId <code>Id</code> of the <code>Stock</code>
     *  @return the stock
     *  @throws org.osid.NotFoundException <code>stockId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>stockId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inventory.Stock stock = this.stocks.get(stockId);
        if (stock == null) {
            throw new org.osid.NotFoundException("stock not found: " + stockId);
        }

        return (stock);
    }


    /**
     *  Gets all <code>Stocks</code>. In plenary mode, the returned
     *  list contains all known stocks or an error
     *  results. Otherwise, the returned list may contain only those
     *  stocks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Stocks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.stock.ArrayStockList(this.stocks.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stocks.clear();
        super.close();
        return;
    }
}
