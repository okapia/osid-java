//
// AbstractDirectionSearchOdrer.java
//
//     Defines a DirectionSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.direction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code DirectionSearchOrder}.
 */

public abstract class AbstractDirectionSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.recipe.DirectionSearchOrder {

    private final java.util.Collection<org.osid.recipe.records.DirectionSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by recipe. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecipe(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a recipe search order is available. 
     *
     *  @return <code> true </code> if a recipe search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the recipe search order. 
     *
     *  @return the recipe search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRecipeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSearchOrder getRecipeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRecipeSearchOrder() is false");
    }


    /**
     *  Orders the results by the duration. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEstimatedDuration(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  directionRecordType a direction record type 
     *  @return {@code true} if the directionRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code directionRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type directionRecordType) {
        for (org.osid.recipe.records.DirectionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(directionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  directionRecordType the direction record type 
     *  @return the direction search order record
     *  @throws org.osid.NullArgumentException
     *          {@code directionRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(directionRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionSearchOrderRecord getDirectionSearchOrderRecord(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.DirectionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(directionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directionRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this direction. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param directionRecord the direction search odrer record
     *  @param directionRecordType direction record type
     *  @throws org.osid.NullArgumentException
     *          {@code directionRecord} or
     *          {@code directionRecordTypedirection} is
     *          {@code null}
     */
            
    protected void addDirectionRecord(org.osid.recipe.records.DirectionSearchOrderRecord directionSearchOrderRecord, 
                                     org.osid.type.Type directionRecordType) {

        addRecordType(directionRecordType);
        this.records.add(directionSearchOrderRecord);
        
        return;
    }
}
