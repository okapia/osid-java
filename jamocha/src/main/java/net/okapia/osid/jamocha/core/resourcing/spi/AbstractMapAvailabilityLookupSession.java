//
// AbstractMapAvailabilityLookupSession
//
//    A simple framework for providing an Availability lookup service
//    backed by a fixed collection of availabilities.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Availability lookup service backed by a
 *  fixed collection of availabilities. The availabilities are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Availabilities</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAvailabilityLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractAvailabilityLookupSession
    implements org.osid.resourcing.AvailabilityLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.Availability> availabilities = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.Availability>());


    /**
     *  Makes an <code>Availability</code> available in this session.
     *
     *  @param  availability an availability
     *  @throws org.osid.NullArgumentException <code>availability<code>
     *          is <code>null</code>
     */

    protected void putAvailability(org.osid.resourcing.Availability availability) {
        this.availabilities.put(availability.getId(), availability);
        return;
    }


    /**
     *  Makes an array of availabilities available in this session.
     *
     *  @param  availabilities an array of availabilities
     *  @throws org.osid.NullArgumentException <code>availabilities<code>
     *          is <code>null</code>
     */

    protected void putAvailabilities(org.osid.resourcing.Availability[] availabilities) {
        putAvailabilities(java.util.Arrays.asList(availabilities));
        return;
    }


    /**
     *  Makes a collection of availabilities available in this session.
     *
     *  @param  availabilities a collection of availabilities
     *  @throws org.osid.NullArgumentException <code>availabilities<code>
     *          is <code>null</code>
     */

    protected void putAvailabilities(java.util.Collection<? extends org.osid.resourcing.Availability> availabilities) {
        for (org.osid.resourcing.Availability availability : availabilities) {
            this.availabilities.put(availability.getId(), availability);
        }

        return;
    }


    /**
     *  Removes an Availability from this session.
     *
     *  @param  availabilityId the <code>Id</code> of the availability
     *  @throws org.osid.NullArgumentException <code>availabilityId<code> is
     *          <code>null</code>
     */

    protected void removeAvailability(org.osid.id.Id availabilityId) {
        this.availabilities.remove(availabilityId);
        return;
    }


    /**
     *  Gets the <code>Availability</code> specified by its <code>Id</code>.
     *
     *  @param  availabilityId <code>Id</code> of the <code>Availability</code>
     *  @return the availability
     *  @throws org.osid.NotFoundException <code>availabilityId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>availabilityId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Availability getAvailability(org.osid.id.Id availabilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.Availability availability = this.availabilities.get(availabilityId);
        if (availability == null) {
            throw new org.osid.NotFoundException("availability not found: " + availabilityId);
        }

        return (availability);
    }


    /**
     *  Gets all <code>Availabilities</code>. In plenary mode, the returned
     *  list contains all known availabilities or an error
     *  results. Otherwise, the returned list may contain only those
     *  availabilities that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Availabilities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.availability.ArrayAvailabilityList(this.availabilities.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.availabilities.clear();
        super.close();
        return;
    }
}
