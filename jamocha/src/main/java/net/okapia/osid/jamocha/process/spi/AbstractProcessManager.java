//
// AbstractProcessManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractProcessManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.process.ProcessManager,
               org.osid.process.ProcessProxyManager {

    private final Types stateRecordTypes                   = new TypeRefSet();
    private final Types stateSearchRecordTypes             = new TypeRefSet();

    private final Types processRecordTypes                 = new TypeRefSet();
    private final Types processSearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractProcessManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractProcessManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of state and <code> Ids </code> is 
     *  supported. 
     *
     *  @return <code> true </code> if state mapping retrieval is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsState() {
        return (false);
    }


    /**
     *  Tests if managing mappings of states and <code> Ids </code> is 
     *  supported. 
     *
     *  @return <code> true </code> if state assignment is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateAssignment() {
        return (false);
    }


    /**
     *  Tests if subscirbing to state change notifications is supported. 
     *
     *  @return <code> true </code> if state change notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateChangeNotification() {
        return (false);
    }


    /**
     *  Tests if state lookup is supported. 
     *
     *  @return <code> true </code> if state lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateLookup() {
        return (false);
    }


    /**
     *  Tests if state query is supported. 
     *
     *  @return <code> true </code> if state query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateQuery() {
        return (false);
    }


    /**
     *  Tests if state search is supported. 
     *
     *  @return <code> true </code> if state search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateSearch() {
        return (false);
    }


    /**
     *  Tests if state administration is supported. 
     *
     *  @return <code> true </code> if state administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateAdmin() {
        return (false);
    }


    /**
     *  Tests if state sequencing is supported. 
     *
     *  @return <code> true </code> if state sequencing is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateSequencing() {
        return (false);
    }


    /**
     *  Tests if state notification is supported. Messages may be sent when 
     *  states are created, modified, or deleted. 
     *
     *  @return <code> true </code> if state notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateNotification() {
        return (false);
    }


    /**
     *  Tests if process lookup is supported. 
     *
     *  @return <code> true </code> if process lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessLookup() {
        return (false);
    }


    /**
     *  Tests if process query is supported. 
     *
     *  @return <code> true </code> if process query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Tests if process search is supported. 
     *
     *  @return <code> true </code> if process search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessSearch() {
        return (false);
    }


    /**
     *  Tests if process administration is supported. 
     *
     *  @return <code> true </code> if process administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessAdmin() {
        return (false);
    }


    /**
     *  Tests if process notification is supported. Messages may be sent when 
     *  <code> Process </code> objects are created, deleted or updated. 
     *  Notifications for states within processes are sent via the state 
     *  notification session. 
     *
     *  @return <code> true </code> if process notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessNotification() {
        return (false);
    }


    /**
     *  Tests if a process hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a process hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessHierarchy() {
        return (false);
    }


    /**
     *  Tests if a process hierarchy design is supported. 
     *
     *  @return <code> true </code> if a process hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessHierarchyDesign() {
        return (false);
    }


    /**
     *  Gets all the state record types supported. 
     *
     *  @return the list of supported state record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStateRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stateRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given state record type is supported. 
     *
     *  @param  stateRecordType the state type 
     *  @return <code> true </code> if the state record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stateRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStateRecordType(org.osid.type.Type stateRecordType) {
        return (this.stateRecordTypes.contains(stateRecordType));
    }


    /**
     *  Adds support for a state record type.
     *
     *  @param stateRecordType a state record type
     *  @throws org.osid.NullArgumentException
     *  <code>stateRecordType</code> is <code>null</code>
     */

    protected void addStateRecordType(org.osid.type.Type stateRecordType) {
        this.stateRecordTypes.add(stateRecordType);
        return;
    }


    /**
     *  Removes support for a state record type.
     *
     *  @param stateRecordType a state record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stateRecordType</code> is <code>null</code>
     */

    protected void removeStateRecordType(org.osid.type.Type stateRecordType) {
        this.stateRecordTypes.remove(stateRecordType);
        return;
    }


    /**
     *  Gets all the state search record types supported. 
     *
     *  @return the list of supported state search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStateSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stateSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given state search type is supported. 
     *
     *  @param  stateSearchRecordType the state search type 
     *  @return <code> true </code> if the state search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stateRecordSearchType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStateSearchRecordType(org.osid.type.Type stateSearchRecordType) {
        return (this.stateSearchRecordTypes.contains(stateSearchRecordType));
    }


    /**
     *  Adds support for a state search record type.
     *
     *  @param stateSearchRecordType a state search record type
     *  @throws org.osid.NullArgumentException
     *  <code>stateSearchRecordType</code> is <code>null</code>
     */

    protected void addStateSearchRecordType(org.osid.type.Type stateSearchRecordType) {
        this.stateSearchRecordTypes.add(stateSearchRecordType);
        return;
    }


    /**
     *  Removes support for a state search record type.
     *
     *  @param stateSearchRecordType a state search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stateSearchRecordType</code> is <code>null</code>
     */

    protected void removeStateSearchRecordType(org.osid.type.Type stateSearchRecordType) {
        this.stateSearchRecordTypes.remove(stateSearchRecordType);
        return;
    }


    /**
     *  Gets all the process record types supported. 
     *
     *  @return the list of supported process record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.processRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given process record type is supported. 
     *
     *  @param  processRecordType the process record type 
     *  @return <code> true </code> if the process record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessRecordType(org.osid.type.Type processRecordType) {
        return (this.processRecordTypes.contains(processRecordType));
    }


    /**
     *  Adds support for a process record type.
     *
     *  @param processRecordType a process record type
     *  @throws org.osid.NullArgumentException
     *  <code>processRecordType</code> is <code>null</code>
     */

    protected void addProcessRecordType(org.osid.type.Type processRecordType) {
        this.processRecordTypes.add(processRecordType);
        return;
    }


    /**
     *  Removes support for a process record type.
     *
     *  @param processRecordType a process record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>processRecordType</code> is <code>null</code>
     */

    protected void removeProcessRecordType(org.osid.type.Type processRecordType) {
        this.processRecordTypes.remove(processRecordType);
        return;
    }


    /**
     *  Gets all the process search record types supported. 
     *
     *  @return the list of supported process search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.processSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given process search record type is supported. 
     *
     *  @param  processSearchRecordType the process search record type 
     *  @return <code> true </code> if the process search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessSearchRecordType(org.osid.type.Type processSearchRecordType) {
        return (this.processSearchRecordTypes.contains(processSearchRecordType));
    }


    /**
     *  Adds support for a process search record type.
     *
     *  @param processSearchRecordType a process search record type
     *  @throws org.osid.NullArgumentException
     *  <code>processSearchRecordType</code> is <code>null</code>
     */

    protected void addProcessSearchRecordType(org.osid.type.Type processSearchRecordType) {
        this.processSearchRecordTypes.add(processSearchRecordType);
        return;
    }


    /**
     *  Removes support for a process search record type.
     *
     *  @param processSearchRecordType a process search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>processSearchRecordType</code> is <code>null</code>
     */

    protected void removeProcessSearchRecordType(org.osid.type.Type processSearchRecordType) {
        this.processSearchRecordTypes.remove(processSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state service. 
     *
     *  @return <code> a StateLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsState() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSession getStateSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsState() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSession getStateSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state service 
     *  for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @return <code> a StateStateSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsState() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateSession getStateSessionForProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateSessionForProcess not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state service 
     *  for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateStateSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsState() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateSession getStateSessionForProcess(org.osid.id.Id processId, 
                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateSessionForProcess not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state 
     *  assignment service. 
     *
     *  @return <code> a StateAssignmentLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAssignmentSession getStateAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateAssignmentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAssignmentSession getStateAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state 
     *  assignment service for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @return <code> a StateAssignmentStateSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAssignmentSession getStateAssignmentSessionForProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateAssignmentSessionForProcess not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state 
     *  assignment service for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateAssignmentStateSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAssignmentSession getStateAssignmentSessionForProcess(org.osid.id.Id processId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateAssignmentSessionForProcess not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to state 
     *  changes. 
     *
     *  @param  stateChangeReceiver the notification callback 
     *  @return <code> a StateChangeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stateChangeReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateChangeNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateChangeNotificationSession(org.osid.process.StateChangeReceiver stateChangeReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateChangeNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to state 
     *  changes. 
     *
     *  @param  stateChangeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a StateChangeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stateChangeReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateChangeNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateChangeNotificationSession(org.osid.process.StateChangeReceiver stateChangeReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateChangeNotificationSession not implemented");
    }


    /**
     *  Gets the state change notification session for the given process. 
     *
     *  @param  stateChangeReceiver the notification callback 
     *  @param  processId the <code> Id </code> of the process 
     *  @return <code> a StateChangeNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> stateChangeReceiver 
     *          </code> or <code> processId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateChangeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateChangeNotificationSessionForProcess(org.osid.process.StateChangeReceiver stateChangeReceiver, 
                                                                                                 org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateChangeNotificationSessionForProcess not implemented");
    }


    /**
     *  Gets the state change notification session for the given process. 
     *
     *  @param  stateChangeReceiver the notification callback 
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateChangeNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> stateChangeReceiver, 
     *          processId </code> , or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateChangeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateChangeNotificationSessionForProcess(org.osid.process.StateChangeReceiver stateChangeReceiver, 
                                                                                                 org.osid.id.Id processId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateChangeNotificationSessionForProcess not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state lookup 
     *  service. 
     *
     *  @return <code> a StateLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateLookupSession getStateLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateLookupSession getStateLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state lookup 
     *  service for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @return <code> a StateLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsStateLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateLookupSession getStateLookupSessionForProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateLookupSessionForProcess not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state lookup 
     *  service for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsStateLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateLookupSession getStateLookupSessionForProcess(org.osid.id.Id processId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateLookupSessionForProcess not implemented");
    }


    /**
     *  Gets a state query session. 
     *
     *  @return <code> a StatQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuerySession getStateQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateQuerySession not implemented");
    }


    /**
     *  Gets a state query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuerySession getStateQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateQuerySession not implemented");
    }


    /**
     *  Gets a state query session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @return <code> a StateQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuerySession getStateQuerySessionForProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateQuerySessionForProcess not implemented");
    }


    /**
     *  Gets a state query session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuerySession getStateQuerySessionForProcess(org.osid.id.Id processId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateQuerySessionForProcess not implemented");
    }


    /**
     *  Gets a state search session. 
     *
     *  @return <code> a StateSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchSession getStateSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateSearchSession not implemented");
    }


    /**
     *  Gets a state search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchSession getStateSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateSearchSession not implemented");
    }


    /**
     *  Gets a state search session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @return <code> a StateSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsStateSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchSession getStateSearchSessionForProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateSearchSessionForProcess not implemented");
    }


    /**
     *  Gets a state search session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsStateSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchSession getStateSearchSessionForProcess(org.osid.id.Id processId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateSearchSessionForProcess not implemented");
    }


    /**
     *  Gets a state administration session for creating, updating and 
     *  deleting states. 
     *
     *  @return <code> a StateAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAdminSession getStateAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateAdminSession not implemented");
    }


    /**
     *  Gets a state administration session for creating, updating and 
     *  deleting states. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAdminSession getStateAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateAdminSession not implemented");
    }


    /**
     *  Gets a state administration session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @return <code> a StateAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAdminSession getStateAdminSessionForProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateAdminSessionForProcess not implemented");
    }


    /**
     *  Gets a state administration session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAdminSession getStateAdminSessionForProcess(org.osid.id.Id processId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateAdminSessionForProcess not implemented");
    }


    /**
     *  Gets a state sequencing session for ordering states. 
     *
     *  @return <code> a StateSequencingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateSequencing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSequencingSession getStateSequencingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateSequencingSession not implemented");
    }


    /**
     *  Gets a state sequencing session for ordering states. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateSequencingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateSequencing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSequencingSession getStateSequencingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateSequencingSession not implemented");
    }


    /**
     *  Gets a state sequencing session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @return <code> a StateSequencingSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateSequencing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSequencingSession getStateSequencingSessionForProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateSequencingSessionForProcess not implemented");
    }


    /**
     *  Gets a state sequencing session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateSequencingSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateSequencing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSequencingSession getStateSequencingSessionForProcess(org.osid.id.Id processId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateSequencingSessionForProcess not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to state 
     *  changes. 
     *
     *  @param  stateReceiver the notification callback 
     *  @return <code> a StateNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stateReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateNotificationSession(org.osid.process.StateReceiver stateReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateNotificationSession not implemented");
    }


    /**
     *  Gets the state notification session for the given process. 
     *
     *  @param  stateReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a StateNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stateReceiver </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateNotificationSession(org.osid.process.StateReceiver stateReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateNotificationSession not implemented");
    }


    /**
     *  Gets the state notification session for the given process. 
     *
     *  @param  stateReceiver the notification callback 
     *  @param  processId the <code> Id </code> of the process 
     *  @return <code> a StateNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> stateReceiver </code> or 
     *          <code> processId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateNotificationSessionForProcess(org.osid.process.StateReceiver stateReceiver, 
                                                                                           org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getStateNotificationSessionForProcess not implemented");
    }


    /**
     *  Gets the state notification session for the given process. 
     *
     *  @param  stateReceiver notification callback 
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> stateReceiver, processId 
     *          </code> or <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateNotificationSessionForProcess(org.osid.process.StateReceiver stateReceiver, 
                                                                                           org.osid.id.Id processId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getStateNotificationSessionForProcess not implemented");
    }


    /**
     *  Gets the process lookup session. 
     *
     *  @return a <code> ProcessLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessLookupSession getProcessLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getProcessLookupSession not implemented");
    }


    /**
     *  Gets the process lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessLookupSession getProcessLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getProcessLookupSession not implemented");
    }


    /**
     *  Gets the process query session. 
     *
     *  @return a <code> ProcessQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessQuerySession getProcessQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getProcessQuerySession not implemented");
    }


    /**
     *  Gets the process query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessQuerySession getProcessQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getProcessQuerySession not implemented");
    }


    /**
     *  Gets the process search session. 
     *
     *  @return a <code> ProcessSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessSearchSession getProcessSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getProcessSearchSession not implemented");
    }


    /**
     *  Gets the process search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessSearchSession getProcessSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getProcessSearchSession not implemented");
    }


    /**
     *  Gets the process administrative session for creating, updating and 
     *  deleteing processes. 
     *
     *  @return a <code> ProcessAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessAdminSession getProcessAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getProcessAdminSession not implemented");
    }


    /**
     *  Gets the process administrative session for creating, updating and 
     *  deleteing processes. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessAdminSession getProcessAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getProcessAdminSession not implemented");
    }


    /**
     *  Gets the notification session for subscriprocessg to changes to a 
     *  process. 
     *
     *  @param  processReceiver the notification callback 
     *  @return a <code> ProcessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> processReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessNotificationSession getProcessNotificationSession(org.osid.process.ProcessReceiver processReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getProcessNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for subscriprocessg to changes to a 
     *  process. 
     *
     *  @param  processReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> processReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessNotificationSession getProcessNotificationSession(org.osid.process.ProcessReceiver processReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getProcessNotificationSession not implemented");
    }


    /**
     *  Gets the process hierarchy traversal session. 
     *
     *  @return <code> a ProcessHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessHierarchySession getProcessHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getProcessHierarchySession not implemented");
    }


    /**
     *  Gets the process hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ProcessHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessHierarchySession getProcessHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getProcessHierarchySession not implemented");
    }


    /**
     *  Gets the process hierarchy design session. 
     *
     *  @return a <code> ProcessHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessHierarchyDesignSession getProcessHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessManager.getProcessHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the process hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessHierarchyDesignSession getProcessHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.process.ProcessProxyManager.getProcessHierarchyDesignSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.stateRecordTypes.clear();
        this.stateRecordTypes.clear();

        this.stateSearchRecordTypes.clear();
        this.stateSearchRecordTypes.clear();

        this.processRecordTypes.clear();
        this.processRecordTypes.clear();

        this.processSearchRecordTypes.clear();
        this.processSearchRecordTypes.clear();

        return;
    }
}
