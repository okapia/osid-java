//
// AbstractImmutableTrigger.java
//
//     Wraps a mutable Trigger to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.trigger.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Trigger</code> to hide modifiers. This
 *  wrapper provides an immutized Trigger from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying trigger whose state changes are visible.
 */

public abstract class AbstractImmutableTrigger
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.control.Trigger {

    private final org.osid.control.Trigger trigger;


    /**
     *  Constructs a new <code>AbstractImmutableTrigger</code>.
     *
     *  @param trigger the trigger to immutablize
     *  @throws org.osid.NullArgumentException <code>trigger</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableTrigger(org.osid.control.Trigger trigger) {
        super(trigger);
        this.trigger = trigger;
        return;
    }


    /**
     *  Gets the controller <code> Id. </code> 
     *
     *  @return the controller <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getControllerId() {
        return (this.trigger.getControllerId());
    }


    /**
     *  Gets the controller. 
     *
     *  @return the controller 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Controller getController()
        throws org.osid.OperationFailedException {

        return (this.trigger.getController());
    }


    /**
     *  Tests if this trigger listens to controller ON events. 
     *
     *  @return <code> true </code> if this is an ON event listener, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean turnedOn() {
        return (this.trigger.turnedOn());
    }


    /**
     *  Tests if this trigger listens to controller OFF events. 
     *
     *  @return <code> true </code> if this is an OFF event listener, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean turnedOff() {
        return (this.trigger.turnedOff());
    }


    /**
     *  Tests if this trigger listens to changed variable amount controller 
     *  events. 
     *
     *  @return <code> true </code> if this is a change event listener, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean changedVariableAmount() {
        return (this.trigger.changedVariableAmount());
    }


    /**
     *  Tests if this trigger listens to events where the variable amount or 
     *  percentage increased to the same or above this value. 
     *
     *  @return the max threshold 
     */

    @OSID @Override
    public java.math.BigDecimal changedExceedsVariableAmount() {
        return (this.trigger.changedExceedsVariableAmount());
    }


    /**
     *  Tests if this trigger listens to events where the variable amount or 
     *  percentage decreased to the same or below this value. 
     *
     *  @return the max threshold 
     */

    @OSID @Override
    public java.math.BigDecimal changedDeceedsVariableAmount() {
        return (this.trigger.changedDeceedsVariableAmount());
    }


    /**
     *  Tests if this trigger listens to controller state changes. 
     *
     *  @return <code> true </code> if this is a state change event listener, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean changedDiscreetState() {
        return (this.trigger.changedDiscreetState());
    }


    /**
     *  Tests if this trigger listens to controller events where a state has 
     *  changed to a specific state. 
     *
     *  @return <code> true </code> if this is a state change event, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean changedToDiscreetState() {
        return (this.trigger.changedToDiscreetState());
    }


    /**
     *  Gets the discreet <code> State </code> <code> Id </code> for a changed 
     *  state event. 
     *
     *  @return the state <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> changedToDiscreetState() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDiscreetStateId() {
        return (this.trigger.getDiscreetStateId());
    }


    /**
     *  Gets the discreet <code> State </code> for a changed state event. 
     *
     *  @return the state 
     *  @throws org.osid.IllegalStateException <code> changedToDiscreetState() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getDiscreetState()
        throws org.osid.OperationFailedException {

        return (this.trigger.getDiscreetState());
    }


    /**
     *  Gets the <code> ActionGroup </code> <code> Ids </code> to execute in 
     *  this trigger. Multiple action groups can be executed in the order of 
     *  this list. 
     *
     *  @return the action group <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getActionGroupIds() {
        return (this.trigger.getActionGroupIds());
    }


    /**
     *  Gets the <code> ActionGroup </code> to execute in this
     *  trigger.  Multiple action groups can be executed in the order
     *  of this list.
     *
     *  @return the action groups
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroups()
        throws org.osid.OperationFailedException {

        return (this.trigger.getActionGroups());
    }


    /**
     *  Gets the <code> Scene </code> <code> Ids </code> to execute in
     *  this trigger. Multiple scenes can be executed in the order of
     *  this list.  This is a shortcut to defining <code> Actions
     *  </code> and <code> ActionGroups </code> which offer more
     *  control in scene execution.
     *
     *  @return the scene <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSceneIds() {
        return (this.trigger.getSceneIds());
    }


    /**
     *  Gets the <code> Scenes </code> to execute in this
     *  trigger. Multiple scenes can be executed in the order of this
     *  list. This is a shortcut to defining <code> Actions </code>
     *  and <code> ActionGroups </code> which offer more control in
     *  scene execution.
     *
     *  @return the scenes
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenes()
        throws org.osid.OperationFailedException {

        return (this.trigger.getScenes());
    }


    /**
     *  Gets the <code> Setting </code> <code> Ids </code> to execute
     *  in this trigger. Multiple settings can be executed in the
     *  order of this list.  This is a shortcut to defining <code>
     *  Settings, </code> <code> Actions </code> and <code>
     *  ActionGroups </code> which offer more control in scene and
     *  setting execution.
     *
     *  @return the setting <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSettingIds() {
        return (this.trigger.getSettingIds());
    }


    /**
     *  Gets the <code> Settings </code> to execute in this
     *  trigger. Multiple settings can be executed in the order of
     *  this list. This is a shortcut to defining <code> Settings,
     *  </code> <code> Actions </code> and <code> ActionGroups </code>
     *  which offer more control in scene and setting execution.
     *
     *  @return the settings
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettings()
        throws org.osid.OperationFailedException {

        return (this.trigger.getSettings());
    }


    /**
     *  Gets the trigger record corresponding to the given <code>
     *  Trigger </code> record <code> Type. </code> This method is
     *  used to retrieve an object implementing the requested
     *  record. The <code> triggerRecordType </code> may be the <code>
     *  Type </code> returned in <code> getRecordTypes() </code> or
     *  any of its parents in a <code> Type </code> hierarchy where
     *  <code> hasRecordType(triggerRecordType) </code> is <code> true
     *  </code>.
     *
     *  @param  triggerRecordType the type of trigger record to retrieve 
     *  @return the trigger record 
     *  @throws org.osid.NullArgumentException <code> triggerRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(triggerRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.records.TriggerRecord getTriggerRecord(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException {

        return (this.trigger.getTriggerRecord(triggerRecordType));
    }
}

