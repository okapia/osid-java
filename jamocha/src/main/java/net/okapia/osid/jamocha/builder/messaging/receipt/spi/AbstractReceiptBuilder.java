//
// AbstractReceipt.java
//
//     Defines a Receipt builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.messaging.receipt.spi;


/**
 *  Defines a <code>Receipt</code> builder.
 */

public abstract class AbstractReceiptBuilder<T extends AbstractReceiptBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.messaging.receipt.ReceiptMiter receipt;


    /**
     *  Constructs a new <code>AbstractReceiptBuilder</code>.
     *
     *  @param receipt the receipt to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractReceiptBuilder(net.okapia.osid.jamocha.builder.messaging.receipt.ReceiptMiter receipt) {
        super(receipt);
        this.receipt = receipt;
        return;
    }


    /**
     *  Builds the receipt.
     *
     *  @return the new receipt
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.messaging.Receipt build() {
        (new net.okapia.osid.jamocha.builder.validator.messaging.receipt.ReceiptValidator(getValidations())).validate(this.receipt);
        return (new net.okapia.osid.jamocha.builder.messaging.receipt.ImmutableReceipt(this.receipt));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the receipt miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.messaging.receipt.ReceiptMiter getMiter() {
        return (this.receipt);
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public T message(org.osid.messaging.Message message) {
        getMiter().setMessage(message);
        return (self());
    }


    /**
     *  Sets the received time.
     *
     *  @param time a received time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T receivedTime(org.osid.calendaring.DateTime time) {
        getMiter().setReceivedTime(time);
        return (self());
    }


    /**
     *  Sets the receiving agent.
     *
     *  @param agent a receiving agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T receivingAgent(org.osid.authentication.Agent agent) {
        getMiter().setReceivingAgent(agent);
        return (self());
    }


    /**
     *  Sets the recipient.
     *
     *  @param recipient a recipient
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>recipient</code>
     *          is <code>null</code>
     */

    public T recipient(org.osid.resource.Resource recipient) {
        getMiter().setRecipient(recipient);
        return (self());
    }


    /**
     *  Adds a Receipt record.
     *
     *  @param record a receipt record
     *  @param recordType the type of receipt record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.messaging.records.ReceiptRecord record, org.osid.type.Type recordType) {
        getMiter().addReceiptRecord(record, recordType);
        return (self());
    }
}       


