//
// AbstractMapLogEntryLookupSession
//
//    A simple framework for providing a LogEntry lookup service
//    backed by a fixed collection of log entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a LogEntry lookup service backed by a
 *  fixed collection of log entries. The log entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>LogEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapLogEntryLookupSession
    extends net.okapia.osid.jamocha.tracking.spi.AbstractLogEntryLookupSession
    implements org.osid.tracking.LogEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.tracking.LogEntry> logEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.tracking.LogEntry>());


    /**
     *  Makes a <code>LogEntry</code> available in this session.
     *
     *  @param  logEntry a log entry
     *  @throws org.osid.NullArgumentException <code>logEntry<code>
     *          is <code>null</code>
     */

    protected void putLogEntry(org.osid.tracking.LogEntry logEntry) {
        this.logEntries.put(logEntry.getId(), logEntry);
        return;
    }


    /**
     *  Makes an array of log entries available in this session.
     *
     *  @param  logEntries an array of log entries
     *  @throws org.osid.NullArgumentException <code>logEntries<code>
     *          is <code>null</code>
     */

    protected void putLogEntries(org.osid.tracking.LogEntry[] logEntries) {
        putLogEntries(java.util.Arrays.asList(logEntries));
        return;
    }


    /**
     *  Makes a collection of log entries available in this session.
     *
     *  @param  logEntries a collection of log entries
     *  @throws org.osid.NullArgumentException <code>logEntries<code>
     *          is <code>null</code>
     */

    protected void putLogEntries(java.util.Collection<? extends org.osid.tracking.LogEntry> logEntries) {
        for (org.osid.tracking.LogEntry logEntry : logEntries) {
            this.logEntries.put(logEntry.getId(), logEntry);
        }

        return;
    }


    /**
     *  Removes a LogEntry from this session.
     *
     *  @param  logEntryId the <code>Id</code> of the log entry
     *  @throws org.osid.NullArgumentException <code>logEntryId<code> is
     *          <code>null</code>
     */

    protected void removeLogEntry(org.osid.id.Id logEntryId) {
        this.logEntries.remove(logEntryId);
        return;
    }


    /**
     *  Gets the <code>LogEntry</code> specified by its <code>Id</code>.
     *
     *  @param  logEntryId <code>Id</code> of the <code>LogEntry</code>
     *  @return the logEntry
     *  @throws org.osid.NotFoundException <code>logEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>logEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntry getLogEntry(org.osid.id.Id logEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.tracking.LogEntry logEntry = this.logEntries.get(logEntryId);
        if (logEntry == null) {
            throw new org.osid.NotFoundException("logEntry not found: " + logEntryId);
        }

        return (logEntry);
    }


    /**
     *  Gets all <code>LogEntries</code>. In plenary mode, the returned
     *  list contains all known logEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  logEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>LogEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.logentry.ArrayLogEntryList(this.logEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.logEntries.clear();
        super.close();
        return;
    }
}
