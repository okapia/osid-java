//
// AbstractIndexedMapMapLookupSession.java
//
//    A simple framework for providing a Map lookup service
//    backed by a fixed collection of maps with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Map lookup service backed by a
 *  fixed collection of maps. The maps are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some maps may be compatible
 *  with more types than are indicated through these map
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Maps</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapMapLookupSession
    extends AbstractMapMapLookupSession
    implements org.osid.mapping.MapLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.Map> mapsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.Map>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.Map> mapsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.Map>());


    /**
     *  Makes a <code>Map</code> available in this session.
     *
     *  @param  map a map
     *  @throws org.osid.NullArgumentException <code>map<code> is
     *          <code>null</code>
     */

    @Override
    protected void putMap(org.osid.mapping.Map map) {
        super.putMap(map);

        this.mapsByGenus.put(map.getGenusType(), map);
        
        try (org.osid.type.TypeList types = map.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.mapsByRecord.put(types.getNextType(), map);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a map from this session.
     *
     *  @param mapId the <code>Id</code> of the map
     *  @throws org.osid.NullArgumentException <code>mapId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeMap(org.osid.id.Id mapId) {
        org.osid.mapping.Map map;
        try {
            map = getMap(mapId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.mapsByGenus.remove(map.getGenusType());

        try (org.osid.type.TypeList types = map.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.mapsByRecord.remove(types.getNextType(), map);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeMap(mapId);
        return;
    }


    /**
     *  Gets a <code>MapList</code> corresponding to the given
     *  map genus <code>Type</code> which does not include
     *  maps of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known maps or an error results. Otherwise,
     *  the returned list may contain only those maps that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  mapGenusType a map genus type 
     *  @return the returned <code>Map</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mapGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByGenusType(org.osid.type.Type mapGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.map.ArrayMapList(this.mapsByGenus.get(mapGenusType)));
    }


    /**
     *  Gets a <code>MapList</code> containing the given
     *  map record <code>Type</code>. In plenary mode, the
     *  returned list contains all known maps or an error
     *  results. Otherwise, the returned list may contain only those
     *  maps that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  mapRecordType a map record type 
     *  @return the returned <code>map</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mapRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByRecordType(org.osid.type.Type mapRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.map.ArrayMapList(this.mapsByRecord.get(mapRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.mapsByGenus.clear();
        this.mapsByRecord.clear();

        super.close();

        return;
    }
}
