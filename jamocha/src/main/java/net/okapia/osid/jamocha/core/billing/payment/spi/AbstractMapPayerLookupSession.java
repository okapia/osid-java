//
// AbstractMapPayerLookupSession
//
//    A simple framework for providing a Payer lookup service
//    backed by a fixed collection of payers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Payer lookup service backed by a
 *  fixed collection of payers. The payers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Payers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPayerLookupSession
    extends net.okapia.osid.jamocha.billing.payment.spi.AbstractPayerLookupSession
    implements org.osid.billing.payment.PayerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.billing.payment.Payer> payers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.billing.payment.Payer>());


    /**
     *  Makes a <code>Payer</code> available in this session.
     *
     *  @param  payer a payer
     *  @throws org.osid.NullArgumentException <code>payer<code>
     *          is <code>null</code>
     */

    protected void putPayer(org.osid.billing.payment.Payer payer) {
        this.payers.put(payer.getId(), payer);
        return;
    }


    /**
     *  Makes an array of payers available in this session.
     *
     *  @param  payers an array of payers
     *  @throws org.osid.NullArgumentException <code>payers<code>
     *          is <code>null</code>
     */

    protected void putPayers(org.osid.billing.payment.Payer[] payers) {
        putPayers(java.util.Arrays.asList(payers));
        return;
    }


    /**
     *  Makes a collection of payers available in this session.
     *
     *  @param  payers a collection of payers
     *  @throws org.osid.NullArgumentException <code>payers<code>
     *          is <code>null</code>
     */

    protected void putPayers(java.util.Collection<? extends org.osid.billing.payment.Payer> payers) {
        for (org.osid.billing.payment.Payer payer : payers) {
            this.payers.put(payer.getId(), payer);
        }

        return;
    }


    /**
     *  Removes a Payer from this session.
     *
     *  @param  payerId the <code>Id</code> of the payer
     *  @throws org.osid.NullArgumentException <code>payerId<code> is
     *          <code>null</code>
     */

    protected void removePayer(org.osid.id.Id payerId) {
        this.payers.remove(payerId);
        return;
    }


    /**
     *  Gets the <code>Payer</code> specified by its <code>Id</code>.
     *
     *  @param  payerId <code>Id</code> of the <code>Payer</code>
     *  @return the payer
     *  @throws org.osid.NotFoundException <code>payerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>payerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.Payer getPayer(org.osid.id.Id payerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.billing.payment.Payer payer = this.payers.get(payerId);
        if (payer == null) {
            throw new org.osid.NotFoundException("payer not found: " + payerId);
        }

        return (payer);
    }


    /**
     *  Gets all <code>Payers</code>. In plenary mode, the returned
     *  list contains all known payers or an error
     *  results. Otherwise, the returned list may contain only those
     *  payers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Payers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.payment.payer.ArrayPayerList(this.payers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.payers.clear();
        super.close();
        return;
    }
}
