//
// AbstractProficiencyQuery.java
//
//     A template for making a Proficiency Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.proficiency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for proficiencies.
 */

public abstract class AbstractProficiencyQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.learning.ProficiencyQuery {

    private final java.util.Collection<org.osid.learning.records.ProficiencyQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveId(org.osid.id.Id objectiveId, boolean match) {
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  objectives. 
     *
     *  @return <code> true </code> if an robjective query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveQuery() is false");
    }


    /**
     *  Matches an activity that has any objective assigned. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          objective, <code> false </code> to match activities with no 
     *          objective 
     */

    @OSID @Override
    public void matchAnyObjective(boolean match) {
        return;
    }


    /**
     *  Clears the objective terms. 
     */

    @OSID @Override
    public void clearObjectiveTerms() {
        return;
    }


    /**
     *  Sets the completion for this query to match completion percentages 
     *  between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     */

    @OSID @Override
    public void matchCompletion(java.math.BigDecimal start, 
                                java.math.BigDecimal end, boolean match) {
        return;
    }


    /**
     *  Clears the completion terms. 
     */

    @OSID @Override
    public void clearCompletionTerms() {
        return;
    }


    /**
     *  Sets the minimum completion for this query. 
     *
     *  @param  completion completion percentage 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumCompletion(java.math.BigDecimal completion, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the minimum completion terms. 
     */

    @OSID @Override
    public void clearMinimumCompletionTerms() {
        return;
    }


    /**
     *  Sets the level grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears all level <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches an assessment offered that has any level assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any level, 
     *          <code> false </code> to match offerings with no levsls 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        return;
    }


    /**
     *  Clears all level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        return;
    }


    /**
     *  Sets the objective bank <code> Id </code> for this query. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveBankId(org.osid.id.Id objectiveBankId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the objective bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ObjectiveBankQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if an objective bank query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the objective bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuery getObjectiveBankQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveBankQuery() is false");
    }


    /**
     *  Clears the objective bank terms. 
     */

    @OSID @Override
    public void clearObjectiveBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given proficiency query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a proficiency implementing the requested record.
     *
     *  @param proficiencyRecordType a proficiency record type
     *  @return the proficiency query record
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(proficiencyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencyQueryRecord getProficiencyQueryRecord(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ProficiencyQueryRecord record : this.records) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proficiencyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this proficiency query. 
     *
     *  @param proficiencyQueryRecord proficiency query record
     *  @param proficiencyRecordType proficiency record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProficiencyQueryRecord(org.osid.learning.records.ProficiencyQueryRecord proficiencyQueryRecord, 
                                          org.osid.type.Type proficiencyRecordType) {

        addRecordType(proficiencyRecordType);
        nullarg(proficiencyQueryRecord, "proficiency query record");
        this.records.add(proficiencyQueryRecord);        
        return;
    }
}
