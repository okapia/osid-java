//
// MutableMapCommissionEnablerLookupSession
//
//    Implements a CommissionEnabler lookup service backed by a collection of
//    commissionEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a CommissionEnabler lookup service backed by a collection of
 *  commission enablers. The commission enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of commission enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapCommissionEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapCommissionEnablerLookupSession
    implements org.osid.resourcing.rules.CommissionEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapCommissionEnablerLookupSession}
     *  with no commission enablers.
     *
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumentException {@code foundry} is
     *          {@code null}
     */

      public MutableMapCommissionEnablerLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCommissionEnablerLookupSession} with a
     *  single commissionEnabler.
     *
     *  @param foundry the foundry  
     *  @param commissionEnabler a commission enabler
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code commissionEnabler} is {@code null}
     */

    public MutableMapCommissionEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                           org.osid.resourcing.rules.CommissionEnabler commissionEnabler) {
        this(foundry);
        putCommissionEnabler(commissionEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCommissionEnablerLookupSession}
     *  using an array of commission enablers.
     *
     *  @param foundry the foundry
     *  @param commissionEnablers an array of commission enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code commissionEnablers} is {@code null}
     */

    public MutableMapCommissionEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                           org.osid.resourcing.rules.CommissionEnabler[] commissionEnablers) {
        this(foundry);
        putCommissionEnablers(commissionEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCommissionEnablerLookupSession}
     *  using a collection of commission enablers.
     *
     *  @param foundry the foundry
     *  @param commissionEnablers a collection of commission enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code commissionEnablers} is {@code null}
     */

    public MutableMapCommissionEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                           java.util.Collection<? extends org.osid.resourcing.rules.CommissionEnabler> commissionEnablers) {

        this(foundry);
        putCommissionEnablers(commissionEnablers);
        return;
    }

    
    /**
     *  Makes a {@code CommissionEnabler} available in this session.
     *
     *  @param commissionEnabler a commission enabler
     *  @throws org.osid.NullArgumentException {@code commissionEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putCommissionEnabler(org.osid.resourcing.rules.CommissionEnabler commissionEnabler) {
        super.putCommissionEnabler(commissionEnabler);
        return;
    }


    /**
     *  Makes an array of commission enablers available in this session.
     *
     *  @param commissionEnablers an array of commission enablers
     *  @throws org.osid.NullArgumentException {@code commissionEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putCommissionEnablers(org.osid.resourcing.rules.CommissionEnabler[] commissionEnablers) {
        super.putCommissionEnablers(commissionEnablers);
        return;
    }


    /**
     *  Makes collection of commission enablers available in this session.
     *
     *  @param commissionEnablers a collection of commission enablers
     *  @throws org.osid.NullArgumentException {@code commissionEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putCommissionEnablers(java.util.Collection<? extends org.osid.resourcing.rules.CommissionEnabler> commissionEnablers) {
        super.putCommissionEnablers(commissionEnablers);
        return;
    }


    /**
     *  Removes a CommissionEnabler from this session.
     *
     *  @param commissionEnablerId the {@code Id} of the commission enabler
     *  @throws org.osid.NullArgumentException {@code commissionEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeCommissionEnabler(org.osid.id.Id commissionEnablerId) {
        super.removeCommissionEnabler(commissionEnablerId);
        return;
    }    
}
