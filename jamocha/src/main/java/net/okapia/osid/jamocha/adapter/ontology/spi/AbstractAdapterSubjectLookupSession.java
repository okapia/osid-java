//
// AbstractAdapterSubjectLookupSession.java
//
//    A Subject lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ontology.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Subject lookup session adapter.
 */

public abstract class AbstractAdapterSubjectLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ontology.SubjectLookupSession {

    private final org.osid.ontology.SubjectLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSubjectLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSubjectLookupSession(org.osid.ontology.SubjectLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Ontology/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Ontology Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.session.getOntologyId());
    }


    /**
     *  Gets the {@code Ontology} associated with this session.
     *
     *  @return the {@code Ontology} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOntology());
    }


    /**
     *  Tests if this user can perform {@code Subject} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSubjects() {
        return (this.session.canLookupSubjects());
    }


    /**
     *  A complete view of the {@code Subject} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSubjectView() {
        this.session.useComparativeSubjectView();
        return;
    }


    /**
     *  A complete view of the {@code Subject} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySubjectView() {
        this.session.usePlenarySubjectView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subjects in ontologies which are children
     *  of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        this.session.useFederatedOntologyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this ontology only.
     */

    @OSID @Override
    public void useIsolatedOntologyView() {
        this.session.useIsolatedOntologyView();
        return;
    }
    
     
    /**
     *  Gets the {@code Subject} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Subject} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Subject} and
     *  retained for compatibility.
     *
     *  @param subjectId {@code Id} of the {@code Subject}
     *  @return the subject
     *  @throws org.osid.NotFoundException {@code subjectId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code subjectId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Subject getSubject(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubject(subjectId));
    }


    /**
     *  Gets a {@code SubjectList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  subjects specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Subjects} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  subjectIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Subject} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code subjectIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByIds(org.osid.id.IdList subjectIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubjectsByIds(subjectIds));
    }


    /**
     *  Gets a {@code SubjectList} corresponding to the given
     *  subject genus {@code Type} which does not include
     *  subjects of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  subjectGenusType a subject genus type 
     *  @return the returned {@code Subject} list
     *  @throws org.osid.NullArgumentException
     *          {@code subjectGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByGenusType(org.osid.type.Type subjectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubjectsByGenusType(subjectGenusType));
    }


    /**
     *  Gets a {@code SubjectList} corresponding to the given
     *  subject genus {@code Type} and include any additional
     *  subjects with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  subjectGenusType a subject genus type 
     *  @return the returned {@code Subject} list
     *  @throws org.osid.NullArgumentException
     *          {@code subjectGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByParentGenusType(org.osid.type.Type subjectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubjectsByParentGenusType(subjectGenusType));
    }


    /**
     *  Gets a {@code SubjectList} containing the given
     *  subject record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  subjectRecordType a subject record type 
     *  @return the returned {@code Subject} list
     *  @throws org.osid.NullArgumentException
     *          {@code subjectRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByRecordType(org.osid.type.Type subjectRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubjectsByRecordType(subjectRecordType));
    }


    /**
     *  Gets all {@code Subjects}. 
     *
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Subjects} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjects()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubjects());
    }
}
