//
// ResponseElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.response;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class ResponseElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ResponseElement Id.
     *
     *  @return the response element Id
     */

    public static org.osid.id.Id getResponseEntityId() {
        return (makeEntityId("osid.inquiry.Response"));
    }


    /**
     *  Gets the InquiryId element Id.
     *
     *  @return the InquiryId element Id
     */

    public static org.osid.id.Id getInquiryId() {
        return (makeElementId("osid.inquiry.response.InquiryId"));
    }


    /**
     *  Gets the Inquiry element Id.
     *
     *  @return the Inquiry element Id
     */

    public static org.osid.id.Id getInquiry() {
        return (makeElementId("osid.inquiry.response.Inquiry"));
    }


    /**
     *  Gets the ResponderId element Id.
     *
     *  @return the ResponderId element Id
     */

    public static org.osid.id.Id getResponderId() {
        return (makeElementId("osid.inquiry.response.ResponderId"));
    }


    /**
     *  Gets the Responder element Id.
     *
     *  @return the Responder element Id
     */

    public static org.osid.id.Id getResponder() {
        return (makeElementId("osid.inquiry.response.Responder"));
    }


    /**
     *  Gets the RespondingAgentId element Id.
     *
     *  @return the RespondingAgentId element Id
     */

    public static org.osid.id.Id getRespondingAgentId() {
        return (makeElementId("osid.inquiry.response.RespondingAgentId"));
    }


    /**
     *  Gets the RespondingAgent element Id.
     *
     *  @return the RespondingAgent element Id
     */

    public static org.osid.id.Id getRespondingAgent() {
        return (makeElementId("osid.inquiry.response.RespondingAgent"));
    }


    /**
     *  Gets the InquirytId element Id.
     *
     *  @return the InquirytId element Id
     */

    public static org.osid.id.Id getInquirytId() {
        return (makeQueryElementId("osid.inquiry.response.InquirytId"));
    }


    /**
     *  Gets the Affirmative element Id.
     *
     *  @return the Affirmative element Id
     */

    public static org.osid.id.Id getAffirmative() {
        return (makeQueryElementId("osid.inquiry.response.Affirmative"));
    }


    /**
     *  Gets the InquestId element Id.
     *
     *  @return the InquestId element Id
     */

    public static org.osid.id.Id getInquestId() {
        return (makeQueryElementId("osid.inquiry.response.InquestId"));
    }


    /**
     *  Gets the Inquest element Id.
     *
     *  @return the Inquest element Id
     */

    public static org.osid.id.Id getInquest() {
        return (makeQueryElementId("osid.inquiry.response.Inquest"));
    }
}
