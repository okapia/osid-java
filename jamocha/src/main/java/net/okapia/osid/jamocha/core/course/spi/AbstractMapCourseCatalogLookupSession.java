//
// AbstractMapCourseCatalogLookupSession
//
//    A simple framework for providing a CourseCatalog lookup service
//    backed by a fixed collection of course catalogs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CourseCatalog lookup service backed by a
 *  fixed collection of course catalogs. The course catalogs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CourseCatalogs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCourseCatalogLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractCourseCatalogLookupSession
    implements org.osid.course.CourseCatalogLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.CourseCatalog> courseCatalogs = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.CourseCatalog>());


    /**
     *  Makes a <code>CourseCatalog</code> available in this session.
     *
     *  @param  courseCatalog a course catalog
     *  @throws org.osid.NullArgumentException <code>courseCatalog<code>
     *          is <code>null</code>
     */

    protected void putCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        this.courseCatalogs.put(courseCatalog.getId(), courseCatalog);
        return;
    }


    /**
     *  Makes an array of course catalogs available in this session.
     *
     *  @param  courseCatalogs an array of course catalogs
     *  @throws org.osid.NullArgumentException <code>courseCatalogs<code>
     *          is <code>null</code>
     */

    protected void putCourseCatalogs(org.osid.course.CourseCatalog[] courseCatalogs) {
        putCourseCatalogs(java.util.Arrays.asList(courseCatalogs));
        return;
    }


    /**
     *  Makes a collection of course catalogs available in this session.
     *
     *  @param  courseCatalogs a collection of course catalogs
     *  @throws org.osid.NullArgumentException <code>courseCatalogs<code>
     *          is <code>null</code>
     */

    protected void putCourseCatalogs(java.util.Collection<? extends org.osid.course.CourseCatalog> courseCatalogs) {
        for (org.osid.course.CourseCatalog courseCatalog : courseCatalogs) {
            this.courseCatalogs.put(courseCatalog.getId(), courseCatalog);
        }

        return;
    }


    /**
     *  Removes a CourseCatalog from this session.
     *
     *  @param  courseCatalogId the <code>Id</code> of the course catalog
     *  @throws org.osid.NullArgumentException <code>courseCatalogId<code> is
     *          <code>null</code>
     */

    protected void removeCourseCatalog(org.osid.id.Id courseCatalogId) {
        this.courseCatalogs.remove(courseCatalogId);
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> specified by its <code>Id</code>.
     *
     *  @param  courseCatalogId <code>Id</code> of the <code>CourseCatalog</code>
     *  @return the courseCatalog
     *  @throws org.osid.NotFoundException <code>courseCatalogId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>courseCatalogId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.CourseCatalog courseCatalog = this.courseCatalogs.get(courseCatalogId);
        if (courseCatalog == null) {
            throw new org.osid.NotFoundException("courseCatalog not found: " + courseCatalogId);
        }

        return (courseCatalog);
    }


    /**
     *  Gets all <code>CourseCatalogs</code>. In plenary mode, the returned
     *  list contains all known courseCatalogs or an error
     *  results. Otherwise, the returned list may contain only those
     *  courseCatalogs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CourseCatalogs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.coursecatalog.ArrayCourseCatalogList(this.courseCatalogs.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.courseCatalogs.clear();
        super.close();
        return;
    }
}
