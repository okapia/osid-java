//
// AbstractIndexedMapJournalEntryLookupSession.java
//
//    A simple framework for providing a JournalEntry lookup service
//    backed by a fixed collection of journal entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a JournalEntry lookup service backed by a
 *  fixed collection of journal entries. The journal entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some journal entries may be compatible
 *  with more types than are indicated through these journal entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>JournalEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapJournalEntryLookupSession
    extends AbstractMapJournalEntryLookupSession
    implements org.osid.journaling.JournalEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.journaling.JournalEntry> journalEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.journaling.JournalEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.journaling.JournalEntry> journalEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.journaling.JournalEntry>());


    /**
     *  Makes a <code>JournalEntry</code> available in this session.
     *
     *  @param  journalEntry a journal entry
     *  @throws org.osid.NullArgumentException <code>journalEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putJournalEntry(org.osid.journaling.JournalEntry journalEntry) {
        super.putJournalEntry(journalEntry);

        this.journalEntriesByGenus.put(journalEntry.getGenusType(), journalEntry);
        
        try (org.osid.type.TypeList types = journalEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.journalEntriesByRecord.put(types.getNextType(), journalEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a journal entry from this session.
     *
     *  @param journalEntryId the <code>Id</code> of the journal entry
     *  @throws org.osid.NullArgumentException <code>journalEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeJournalEntry(org.osid.id.Id journalEntryId) {
        org.osid.journaling.JournalEntry journalEntry;
        try {
            journalEntry = getJournalEntry(journalEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.journalEntriesByGenus.remove(journalEntry.getGenusType());

        try (org.osid.type.TypeList types = journalEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.journalEntriesByRecord.remove(types.getNextType(), journalEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeJournalEntry(journalEntryId);
        return;
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  journal entry genus <code>Type</code> which does not include
     *  journal entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known journal entries or an error results. Otherwise,
     *  the returned list may contain only those journal entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  journalEntryGenusType a journal entry genus type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByGenusType(org.osid.type.Type journalEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.journaling.journalentry.ArrayJournalEntryList(this.journalEntriesByGenus.get(journalEntryGenusType)));
    }


    /**
     *  Gets a <code>JournalEntryList</code> containing the given
     *  journal entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  journalEntryRecordType a journal entry record type 
     *  @return the returned <code>journalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByRecordType(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.journaling.journalentry.ArrayJournalEntryList(this.journalEntriesByRecord.get(journalEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.journalEntriesByGenus.clear();
        this.journalEntriesByRecord.clear();

        super.close();

        return;
    }
}
