//
// AbstractHoldEnablerNotificationSession.java
//
//     A template for making HoldEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code HoldEnabler} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code HoldEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for hold enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractHoldEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.hold.rules.HoldEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.hold.Oubliette oubliette = new net.okapia.osid.jamocha.nil.hold.oubliette.UnknownOubliette();


    /**
     *  Gets the {@code Oubliette} {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Oubliette Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.oubliette.getId());
    }

    
    /**
     *  Gets the {@code Oubliette} associated with this session.
     *
     *  @return the {@code Oubliette} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.oubliette);
    }


    /**
     *  Sets the {@code Oubliette}.
     *
     *  @param oubliette the oubliette for this session
     *  @throws org.osid.NullArgumentException {@code oubliette}
     *          is {@code null}
     */

    protected void setOubliette(org.osid.hold.Oubliette oubliette) {
        nullarg(oubliette, "oubliette");
        this.oubliette = oubliette;
        return;
    }


    /**
     *  Tests if this user can register for {@code HoldEnabler}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForHoldEnablerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeHoldEnablerNotification() </code>.
     */

    @OSID @Override
    public void reliableHoldEnablerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableHoldEnablerNotifications() {
        return;
    }


    /**
     *  Acknowledge a hold enabler notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeHoldEnablerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for hold enablers in
     *  oubliettes which are children of this oubliette in the
     *  oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new hold enablers. {@code
     *  HoldEnablerReceiver.newHoldEnabler()} is invoked when a new
     *  {@code HoldEnabler} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewHoldEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated hold enablers. {@code
     *  HoldEnablerReceiver.changedHoldEnabler()} is invoked when a
     *  hold enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedHoldEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hold enabler. {@code
     *  HoldEnablerReceiver.changedHoldEnabler()} is invoked when the
     *  specified hold enabler is changed.
     *
     *  @param holdEnablerId the {@code Id} of the {@code HoldEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code holdEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedHoldEnabler(org.osid.id.Id holdEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted hold enablers. {@code
     *  HoldEnablerReceiver.deletedHoldEnabler()} is invoked when a
     *  hold enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedHoldEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted hold enabler. {@code
     *  HoldEnablerReceiver.deletedHoldEnabler()} is invoked when the
     *  specified hold enabler is deleted.
     *
     *  @param holdEnablerId the {@code Id} of the
     *          {@code HoldEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code holdEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedHoldEnabler(org.osid.id.Id holdEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
