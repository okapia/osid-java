//
// JDBCConferralPeerList
//
//     Implements a ConferralPeerList. This list creates conferralpeers from
//     the return of a JDBC query.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.jdbc.recognition.batch.conferralpeer;

import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  <p>Implements a ConferralPeerList. This list creates conferralpeers from the
 *  return of a JDBC query. The query is processed in a separate
 *  thread.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more conferralpeers to be added.</p>
 */

public final class JDBCConferralPeerList
    extends net.okapia.osid.jamocha.recognition.batch.conferralpeer.spi.AbstractMutableConferralPeerList
    implements org.osid.recognition.batch.ConferralPeerList,
               Runnable {

    private boolean running = false;
    private ConferralPeerFetcher fetcher;


    /**
     *  Creates a new <code>JDBCConferralPeerList</code>.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator a conferralPeer to parse a result row and
     *         generate a ConferralPeer
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of conferralpeers in this list
     *         buffer such that when the number of conferralpeers exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until conferralpeers are retrieved. The value
     *         of this may depend on the amount of memory a ConferralPeer
     *         consumes.
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>conferralPeer</code>
     *          is <code>null</code>
     */

    public JDBCConferralPeerList(String query, java.sql.Connection connection, JDBCConferralPeerGenerator generator,
                        boolean closeWhenDone, int bufferSize)
        throws org.osid.OperationFailedException {

        nullarg(query, "query");
        nullarg(connection, "connection");
        nullarg(generator, "generator");

        if (bufferSize <= 0) {
            throw new org.osid.InvalidArgumentException("buffer size too small");
        }

        this.fetcher = new ConferralPeerFetcher(query, connection, generator, closeWhenDone, bufferSize);
        return;
    }


    /**
     *  Creates a new <code>JDBCConferralPeerList</code> and runs it upon
     *  instantiation.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator a ConferralPeer to parse a result row and
     *         generate a ConferralPeer
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of conferralpeers in this list
     *         buffer such that when the number of conferralpeers exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until conferralpeers are retrieved. The value
     *         of this may depend on the amount of memory a ConferralPeer
     *         consumes.
     *  @param run <code>true</code> to start the fetching thread
     *         immediately
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>conferralPeer</code>
     *          is <code>null</code>
     */

    public JDBCConferralPeerList(String query, java.sql.Connection connection, JDBCConferralPeerGenerator generator,
                        boolean closeWhenDone, int bufferSize, boolean run)
        throws org.osid.OperationFailedException {

        this(query, connection, generator, closeWhenDone, bufferSize);
        if (run) {
            run();
        }
        return;
    }


    /**
     *  Starts the JDBC process.
     *
     *  @throws org.osid.IllegalStateException already started
     */

    public void run() {
        if (this.running || hasError()) {
            throw new org.osid.IllegalStateException("already started");
        }

        this.fetcher.start();
        return;
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already
     *          closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.running = false;
        return;
    }


    /**
     *  Tests if the list thread is running to populate elements from
     *  the underlying list.
     *
     *  @return <code>true</code> if the list is running,
     *          <code>false</code> otherwise.
     */

    public boolean isRunning() {
        return (this.running);
    }


    class ConferralPeerFetcher
        extends Thread {
        
        private String query;
        private java.sql.Connection connection;
        private JDBCConferralPeerGenerator generator;
        private boolean closeWhenDone;
        private int bufferSize;

        
        ConferralPeerFetcher(String query, java.sql.Connection connection, JDBCConferralPeerGenerator generator,
                        boolean closeWhenDone, int bufferSize) {

            this.query         = query;
            this.connection    = connection;
            this.generator     = generator;
            this.closeWhenDone = closeWhenDone;
            this.bufferSize    = bufferSize;

            return;
        }
            
        
        public void run() {
            java.sql.Statement statement = null;
            java.sql.ResultSet resultset = null;
            JDBCConferralPeerList.this.running = true;

            try {
                statement = this.connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
                                                            java.sql.ResultSet.CONCUR_READ_ONLY);
                resultset = statement.executeQuery(this.query);
                long length = 0;

                while (resultset.next() && JDBCConferralPeerList.this.running && !JDBCConferralPeerList.this.hasError()) {
                    JDBCConferralPeerList.this.addConferralPeer(generator.makeConferralPeer(resultset));
                    synchronized (JDBCConferralPeerList.this) {
                        JDBCConferralPeerList.this.notifyAll();
                    }

                    if (++length > this.bufferSize) {
                        length = JDBCConferralPeerList.this.available();
                        if (length > this.bufferSize) {
                            synchronized (JDBCConferralPeerList.this) {
                                try {
                                    JDBCConferralPeerList.this.wait();
                                } catch (InterruptedException ie) {}
                            }
                        }
                    } 
                }
            } catch (Exception e) {
                JDBCConferralPeerList.this.error(new org.osid.OperationFailedException("cannot generate conferralpeer", e));
                return;
            } finally {
                try {
                    if (this.closeWhenDone) {
                        this.connection.close();
                    }
                 
                    JDBCConferralPeerList.this.running = false;

                    if (statement != null) {
                        statement.close();
                    }

                    if (resultset != null) {
                        resultset.close();
                    }
                } catch (Exception e) {}
            }

            JDBCConferralPeerList.this.eol();
            return;
        }
    }
}
