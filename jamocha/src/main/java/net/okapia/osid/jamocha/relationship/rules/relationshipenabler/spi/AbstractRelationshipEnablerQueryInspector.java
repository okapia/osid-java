//
// AbstractRelationshipEnablerQueryInspector.java
//
//     A template for making a RelationshipEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.rules.relationshipenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for relationship enablers.
 */

public abstract class AbstractRelationshipEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.relationship.rules.RelationshipEnablerQueryInspector {

    private final java.util.Collection<org.osid.relationship.rules.records.RelationshipEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the relationship <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRelationshipIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the relationship query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQueryInspector[] getRuledRelationshipTerms() {
        return (new org.osid.relationship.RelationshipQueryInspector[0]);
    }


    /**
     *  Gets the family <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFamilyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the family query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQueryInspector[] getFamilyTerms() {
        return (new org.osid.relationship.FamilyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given relationship enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a relationship enabler implementing the requested record.
     *
     *  @param relationshipEnablerRecordType a relationship enabler record type
     *  @return the relationship enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.rules.records.RelationshipEnablerQueryInspectorRecord getRelationshipEnablerQueryInspectorRecord(org.osid.type.Type relationshipEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.rules.records.RelationshipEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(relationshipEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relationship enabler query. 
     *
     *  @param relationshipEnablerQueryInspectorRecord relationship enabler query inspector
     *         record
     *  @param relationshipEnablerRecordType relationshipEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelationshipEnablerQueryInspectorRecord(org.osid.relationship.rules.records.RelationshipEnablerQueryInspectorRecord relationshipEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type relationshipEnablerRecordType) {

        addRecordType(relationshipEnablerRecordType);
        nullarg(relationshipEnablerRecordType, "relationship enabler record type");
        this.records.add(relationshipEnablerQueryInspectorRecord);        
        return;
    }
}
