//
// AbstractCourseOfferingLookupSession.java
//
//    A starter implementation framework for providing a
//    CourseOffering lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CourseOffering
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCourseOfferings(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCourseOfferingLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.CourseOfferingLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private boolean effectiveonly = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>CourseOffering</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCourseOfferings() {
        return (true);
    }


    /**
     *  A complete view of the <code>CourseOffering</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCourseOfferingView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CourseOffering</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCourseOfferingView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include course offerings in course catalogs which are
     *  children of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only course offerings whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveCourseOfferingView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All course offerings of any effective dates are returned by
     *  all methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCourseOfferingView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>CourseOffering</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CourseOffering</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CourseOffering</code> and
     *  retained for compatibility.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingId <code>Id</code> of the
     *          <code>CourseOffering</code>
     *  @return the course offering
     *  @throws org.osid.NotFoundException <code>courseOfferingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.CourseOfferingList courseOfferings = getCourseOfferings()) {
            while (courseOfferings.hasNext()) {
                org.osid.course.CourseOffering courseOffering = courseOfferings.getNextCourseOffering();
                if (courseOffering.getId().equals(courseOfferingId)) {
                    return (courseOffering);
                }
            }
        } 

        throw new org.osid.NotFoundException(courseOfferingId + " not found");
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  courseOfferings specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>CourseOfferings</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCourseOfferings()</code>.
     *
     *  @param  courseOfferingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByIds(org.osid.id.IdList courseOfferingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.CourseOffering> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = courseOfferingIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCourseOffering(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("course offering " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.courseoffering.LinkedCourseOfferingList(ret));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the
     *  given course offering genus <code>Type</code> which does not
     *  include course offerings of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCourseOfferings()</code>.
     *
     *  @param  courseOfferingGenusType a courseOffering genus type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByGenusType(org.osid.type.Type courseOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.CourseOfferingGenusFilterList(getCourseOfferings(), courseOfferingGenusType));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the
     *  given course offering genus <code>Type</code> and include any
     *  additional course offerings with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourseOfferings()</code>.
     *
     *  @param  courseOfferingGenusType a courseOffering genus type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByParentGenusType(org.osid.type.Type courseOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCourseOfferingsByGenusType(courseOfferingGenusType));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> containing the given
     *  course offering record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourseOfferings()</code>.
     *
     *  @param  courseOfferingRecordType a courseOffering record type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByRecordType(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.CourseOfferingRecordFilterList(getCourseOfferings(), courseOfferingRecordType));
    }


    /**
     *  Gets a <code> CourseOfferingList </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective. In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CourseOffering </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsOnDate(org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.TemporalCourseOfferingFilterList(getCourseOfferings(), from, to));
    }
    

    /**
     *  Gets all <code> CourseOfferings </code> associated with a given <code> 
     *  Course. </code> 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseId a course <code> Id </code> 
     *  @return a list of <code> CourseOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.CourseOfferingFilterList(new CourseFilter(courseId), getCourseOfferings()));
    }


    /**
     *  Gets a <code> CourseOfferingList </code> for the given
     *  course effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective. In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param courseId the <code>Id</code> of a course
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CourseOffering </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>courseId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourseOnDate(org.osid.id.Id courseId, 
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.TemporalCourseOfferingFilterList(getCourseOfferingsForCourse(courseId), from, to));
    }


    /**
     *  Gets all <code> CourseOfferings </code> associated with a
     *  given <code> Term. </code> 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code> 
     *  @return a list of <code> CourseOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.CourseOfferingFilterList(new TermFilter(termId), getCourseOfferings()));
    }        


    /**
     *  Gets a <code> CourseOfferingList </code> for the given
     *  term effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective. In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param termId the <code>Id</code> of a term
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CourseOffering </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>termId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForTermOnDate(org.osid.id.Id termId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.TemporalCourseOfferingFilterList(getCourseOfferingsForTerm(termId), from, to));
    }


    /**
     *  Gets all <code> CourseOfferings </code> associated with a
     *  given <code>Course</code> and <code>Term</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  termId a term <code> Id </code> 
     *  @return a list of <code> CourseOfferings </code> 
     *  @throws org.osid.NullArgumentException <code>courseId</code>
     *          or <code>termId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourseAndTerm(org.osid.id.Id courseId, 
                                                                                 org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.CourseOfferingFilterList(new TermFilter(termId), getCourseOfferingsForCourse(courseId)));
    }


    /**
     *  Gets a <code> CourseOfferingList </code> for the given
     *  course, term, and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective. In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param courseId the <code>Id</code> of a course
     *  @param termId the <code>Id</code> of a term
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CourseOffering </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>courseId</code>,
     *          <code>termId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourseAndTermOnDate(org.osid.id.Id courseId, 
                                                                                                  org.osid.id.Id termId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.TemporalCourseOfferingFilterList(getCourseOfferingsForCourseAndTerm(courseId, termId), from, to));
    }

    
    /**
     *  Gets all <code> CourseOfferings ny number and </code>
     *  associated with a given <code> Term. </code> 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  number a course offering number 
     *  @return a list of <code> CourseOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> termId </code> or <code> 
     *          number </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByNumberForTerm(org.osid.id.Id termId, 
                                                                                String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.courseoffering.CourseOfferingFilterList(new NumberFilter(number), getCourseOfferingsForTerm(termId)));
    }


    /**
     *  Gets all <code>CourseOfferings</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  course offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @return a list of <code>CourseOfferings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.CourseOfferingList getCourseOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the course offering list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of course offerings
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.CourseOfferingList filterCourseOfferingsOnViews(org.osid.course.CourseOfferingList list)
        throws org.osid.OperationFailedException {

        org.osid.course.CourseOfferingList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.courseoffering.EffectiveCourseOfferingFilterList(ret);
        }

        return (ret);
    }


    public static class CourseFilter
        implements net.okapia.osid.jamocha.inline.filter.course.courseoffering.CourseOfferingFilter {

        private final org.osid.id.Id courseId;

        
        /**
         *  Constructs a new <code>CourseFilter</code>.
         *
         *  @param courseId the course to filter
         *  @throws org.osid.NullArgumentException <code>courseId</code>
         *          is <code>null</code>
         */

        public CourseFilter(org.osid.id.Id courseId) {
            nullarg(courseId, "course Id");
            this.courseId = courseId;
            return;
        }


        /**
         *  Used by the CourseOfferingFilterList to filter the course offering list
         *  based on course.
         *
         *  @param courseOffering the course offering
         *  @return <code>true</code> to pass the course offering,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.CourseOffering courseOffering) {
            return (courseOffering.getCourseId().equals(this.courseId));
        }
    }        


    public static class TermFilter
        implements net.okapia.osid.jamocha.inline.filter.course.courseoffering.CourseOfferingFilter {

        private final org.osid.id.Id termId;

        
        /**
         *  Constructs a new <code>TermFilter</code>.
         *
         *  @param termId the term to filter
         *  @throws org.osid.NullArgumentException <code>termId</code>
         *          is <code>null</code>
         */

        public TermFilter(org.osid.id.Id termId) {
            nullarg(termId, "term Id");
            this.termId = termId;
            return;
        }


        /**
         *  Used by the CourseOfferingFilterList to filter the course offering list
         *  based on term.
         *
         *  @param courseOffering the course offering
         *  @return <code>true</code> to pass the course offering,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.CourseOffering courseOffering) {
            return (courseOffering.getTermId().equals(this.termId));
        }
    }        


    public static class NumberFilter
        implements net.okapia.osid.jamocha.inline.filter.course.courseoffering.CourseOfferingFilter {

        private final String number;

        
        /**
         *  Constructs a new <code>NumberFilter</code>.
         *
         *  @param number the number to filter
         *  @throws org.osid.NullArgumentException <code>number</code>
         *          is <code>null</code>
         */

        public NumberFilter(String number) {
            nullarg(number, "course number");
            this.number = number;
            return;
        }


        /**
         *  Used by the CourseOfferingFilterList to filter the course
         *  offering list based on number.
         *
         *  @param courseOffering the course offering
         *  @return <code>true</code> to pass the course offering,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.CourseOffering courseOffering) {
            return (courseOffering.getNumber().equals(this.number));
        }
    }        
}
