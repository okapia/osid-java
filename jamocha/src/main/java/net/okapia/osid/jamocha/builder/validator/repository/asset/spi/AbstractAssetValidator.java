//
// AbstractAssetValidator.java
//
//     Validates an Asset.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.repository.asset.spi;


/**
 *  Validates an Asset.
 */

public abstract class AbstractAssetValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractSourceableOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractAssetValidator</code>.
     */

    protected AbstractAssetValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractAssetValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractAssetValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Asset.
     *
     *  @param asset an asset to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.repository.Asset asset) {
        super.validate(asset);

        test(asset.getTitle(), "getTitle()");

        testConditionalMethod(asset, "isPublicDomain", asset.isCopyrightStatusKnown(), "isCopyrightStatusKnown()");
        testConditionalMethod(asset, "getCopyright", asset.isCopyrightStatusKnown(), "isCopyrightStatusKnown()");
        testConditionalMethod(asset, "getCopyrightRegistration", asset.isCopyrightStatusKnown(), "isCopyrightStatusKnown()");
        testConditionalMethod(asset, "canDistributeVerbatim", asset.isCopyrightStatusKnown(), "isCopyrightStatusKnown()");
        testConditionalMethod(asset, "canDistributeAlterations", asset.isCopyrightStatusKnown(), "isCopyrightStatusKnown()");
        testConditionalMethod(asset, "canDistributeCompositions", asset.isCopyrightStatusKnown(), "isCopyrightStatusKnown()");

        if (asset.isPublicDomain() && !(asset.canDistributeVerbatim() && 
                                        asset.canDistributeAlterations() &&
                                        asset.canDistributeCompositions())) {
            throw new org.osid.BadLogicException("isPublicDomain() is true");
        }

        testNestedObject(asset, "getSource");
        testNestedObjects(asset, "getProviderLinkIds", "getProviderLinks");
        test(asset.getCreatedDate(), "getCreatedDate()");
       
        testConditionalMethod(asset, "getPublishedDate", asset.isPublished(), "isPublished()");
        test(asset.getPrincipalCreditString(), "getPrincipalCreditString()");
        testNestedObjects(asset, "getAssetContentIds", "getAssetContents");

        testConditionalMethod(asset, "getCompositionId", asset.isComposition(), "isComposition()"); 
        testConditionalMethod(asset, "getComposition", asset.isComposition(), "isComposition()"); 
        if (asset.isComposition()) {
            testNestedObject(asset, "getComposition");
        }

        return;
    }
}
