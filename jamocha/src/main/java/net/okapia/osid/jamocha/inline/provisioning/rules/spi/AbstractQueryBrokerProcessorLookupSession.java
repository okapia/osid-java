//
// AbstractQueryBrokerProcessorLookupSession.java
//
//    An inline adapter that maps a BrokerProcessorLookupSession to
//    a BrokerProcessorQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BrokerProcessorLookupSession to
 *  a BrokerProcessorQuerySession.
 */

public abstract class AbstractQueryBrokerProcessorLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractBrokerProcessorLookupSession
    implements org.osid.provisioning.rules.BrokerProcessorLookupSession {

    private boolean activeonly    = false;
    private final org.osid.provisioning.rules.BrokerProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBrokerProcessorLookupSession.
     *
     *  @param querySession the underlying broker processor query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBrokerProcessorLookupSession(org.osid.provisioning.rules.BrokerProcessorQuerySession querySession) {
        nullarg(querySession, "broker processor query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>BrokerProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBrokerProcessors() {
        return (this.session.canSearchBrokerProcessors());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker processors in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active broker processors are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveBrokerProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive broker processors are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>BrokerProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BrokerProcessor</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>BrokerProcessor</code> and retained for compatibility.
     *
     *  In active mode, broker processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker processors are returned.
     *
     *  @param  brokerProcessorId <code>Id</code> of the
     *          <code>BrokerProcessor</code>
     *  @return the broker processor
     *  @throws org.osid.NotFoundException <code>brokerProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>brokerProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessor getBrokerProcessor(org.osid.id.Id brokerProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorQuery query = getQuery();
        query.matchId(brokerProcessorId, true);
        org.osid.provisioning.rules.BrokerProcessorList brokerProcessors = this.session.getBrokerProcessorsByQuery(query);
        if (brokerProcessors.hasNext()) {
            return (brokerProcessors.getNextBrokerProcessor());
        } 
        
        throw new org.osid.NotFoundException(brokerProcessorId + " not found");
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  brokerProcessors specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>BrokerProcessors</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, broker processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker processors are returned.
     *
     *  @param  brokerProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BrokerProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByIds(org.osid.id.IdList brokerProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorQuery query = getQuery();

        try (org.osid.id.IdList ids = brokerProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBrokerProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> corresponding to the
     *  given broker processor genus <code>Type</code> which does not
     *  include broker processors of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, broker processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker processors are returned.
     *
     *  @param  brokerProcessorGenusType a brokerProcessor genus type 
     *  @return the returned <code>BrokerProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByGenusType(org.osid.type.Type brokerProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorQuery query = getQuery();
        query.matchGenusType(brokerProcessorGenusType, true);
        return (this.session.getBrokerProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> corresponding to the
     *  given broker processor genus <code>Type</code> and include any
     *  additional broker processors with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, broker processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker processors are returned.
     *
     *  @param  brokerProcessorGenusType a brokerProcessor genus type 
     *  @return the returned <code>BrokerProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByParentGenusType(org.osid.type.Type brokerProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorQuery query = getQuery();
        query.matchParentGenusType(brokerProcessorGenusType, true);
        return (this.session.getBrokerProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> containing the given
     *  broker processor record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known broker
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, broker processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker processors are returned.
     *
     *  @param  brokerProcessorRecordType a brokerProcessor record type 
     *  @return the returned <code>BrokerProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByRecordType(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorQuery query = getQuery();
        query.matchRecordType(brokerProcessorRecordType, true);
        return (this.session.getBrokerProcessorsByQuery(query));
    }

    
    /**
     *  Gets all <code>BrokerProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  broker processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker processors are returned that are currently
     *  active. In any status mode, active and inactive broker processors
     *  are returned.
     *
     *  @return a list of <code>BrokerProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerProcessorQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBrokerProcessorsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.rules.BrokerProcessorQuery getQuery() {
        org.osid.provisioning.rules.BrokerProcessorQuery query = this.session.getBrokerProcessorQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
