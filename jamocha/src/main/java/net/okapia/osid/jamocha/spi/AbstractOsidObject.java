//
// AbstractOsidObject.java
//
//     Defines a simple OSID object to draw from.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import net.okapia.osid.primordium.type.BasicType;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OSID object to draw from. using this abstract
 *  class requires that <code>setId()</code> must be done before
 *  passing this interface to a consumer in order to maintain
 *  compliance with the OSID specification.
 */

public abstract class AbstractOsidObject
    extends AbstractIdentifiable
    implements org.osid.OsidObject {

    private org.osid.locale.DisplayText displayName = new Plain("OsidObject");
    private org.osid.locale.DisplayText description = new Plain("An OSID Object.");
    private org.osid.type.Type genusType = BasicType.valueOf("types:none@okapia.net");
    
    private final Browsable browsable = new Browsable();


    /**
     *  Gets the preferred display name associated with this instance
     *  of this OSID object appropriate for display to the user.
     *
     *  @return the display name 
     *  @throws org.osid.IllegalStateException the display name has
     *          not been set
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayName() {
        return (this.displayName);
    }


    /**
     *  Sets the displayName for this object.
     *
     *  @param displayName the name for this object
     *  @throws org.osid.NullArgumentException
     *          <code>displayName</code> is <code>null</code>
     */

    protected void setDisplayName(org.osid.locale.DisplayText displayName) {
        nullarg(displayName, "displayName");
        this.displayName = displayName;
        return;
    }


    /**
     *  Gets the description associated with this instance of this
     *  OSID object.
     *
     *  @throws org.osid.IllegalStateException the description has not
     *          been set
     *  @return the description 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDescription() {
        return (this.description);
    }


    /**
     *  Sets the description of this object.
     *
     *  @param description the description of this object
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    protected void setDescription(org.osid.locale.DisplayText description) {
        nullarg(description, "description");
        this.description = description;
        return;
    }

        
    /**
     *  Gets the genus type of this object. 
     *
     *  @return the genus type of this object 
     */

    @OSID @Override
    public org.osid.type.Type getGenusType() {
        return (this.genusType);
    }


    /**
     *  Tests if this object is of the given genus <code>
     *  Type. </code> The given genus type may be supported by the
     *  object through the type hierarchy.
     *
     *  @param  genusType a genus type 
     *  @return <code> true </code> if this object is of the given
     *          genus <code> Type, </code> <code> false </code>
     *          otherwise
     *  @throws org.osid.NullArgumentException <code> genusType
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public boolean isOfGenusType(org.osid.type.Type genusType) {
        return (this.genusType.equals(genusType));
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    protected void setGenusType(org.osid.type.Type genusType) {
        nullarg(genusType, "genus type");
        this.genusType = genusType;
        return;
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.browsable.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code> false </code>
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.browsable.hasRecordType(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.browsable.addRecordType(recordType);
        return;
    }


    /**
     *  Gets a list of all properties of this object including those
     *  corresponding to data within this object's records. Properties
     *  provide a means for applications to display a representation
     *  of the contents of an object without understanding its record
     *  interface specifications. Applications needing to examine a
     *  specific property or perform updates should use the methods
     *  defined by the object's record <code> Type. </code>
     *
     *  @return a list of properties 
     */
    
    @OSID @Override
    public org.osid.PropertyList getProperties()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.browsable.getProperties());
    }


    /**
     *  Gets the properties by record type.
     *
     *  @param  recordType the record type corresponding to the
     *          properties set to retrieve
     *  @return a list of properties 
     *  @throws org.osid.NullArgumentException <code> recordType
     *          </code> is <code> null </code>
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(recordType) </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.PropertyList getPropertiesByRecordType(org.osid.type.Type recordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.browsable.getPropertiesByRecordType(recordType));
    }

    
    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    protected void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        this.browsable.addProperties(set, recordType);
        return;
    }


    protected class Browsable
        extends AbstractBrowsable
        implements org.osid.Browsable,
                   org.osid.Extensible {

        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
            return;
        }


        /**
         *  Adds a property set.
         *
         *  @param set set of properties
         *  @param recordType associated type
         *  @throws org.osid.NullArgumentException <code>set</code> or
         *          <code>recordType</code> is <code>null</code>
         */
        
        @Override
        protected void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
            super.addProperties(set, recordType);
            return;
        }
    }
}
