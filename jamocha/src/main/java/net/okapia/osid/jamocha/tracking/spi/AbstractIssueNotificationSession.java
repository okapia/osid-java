//
// AbstractIssueNotificationSession.java
//
//     A template for making IssueNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Issue} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Issue} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for issue entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractIssueNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.tracking.IssueNotificationSession {

    private boolean federated = false;
    private org.osid.tracking.FrontOffice frontOffice = new net.okapia.osid.jamocha.nil.tracking.frontoffice.UnknownFrontOffice();


    /**
     *  Gets the {@code FrontOffice/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code FrontOffice Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.frontOffice.getId());
    }

    
    /**
     *  Gets the {@code FrontOffice} associated with this session.
     *
     *  @return the {@code FrontOffice} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.frontOffice);
    }


    /**
     *  Sets the {@code FrontOffice}.
     *
     *  @param frontOffice the front office for this session
     *  @throws org.osid.NullArgumentException {@code frontOffice}
     *          is {@code null}
     */

    protected void setFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        nullarg(frontOffice, "front office");
        this.frontOffice = frontOffice;
        return;
    }


    /**
     *  Tests if this user can register for {@code Issue}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForIssueNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include issues in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new issues. {@code
     *  IssueReceiver.newIssue()} is invoked when an new {@code Issue}
     *  is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewIssues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new issues for the given queue
     *  {@code Id}. {@code IssueReceiver.newIssue()} is invoked when a
     *  new {@code Issue} is created.
     *
     *  @param  queueId the {@code Id} of the queue to monitor
     *  @throws org.osid.NullArgumentException {@code queueId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewIssuesForQueue(org.osid.id.Id queueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new issues for the given
     *  customer {@code Id}. {@code IssueReceiver.newIssue()} is
     *  invoked when a new {@code Issue} is created.
     *
     *  @param  resourceId the {@code Id} of the customer to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewIssuesForCustomer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of new issues for the given
     *  topic. {@code IssueReceiver.newIssue()} is invoked when a new
     *  {@code Issue} appears in this front office.
     *
     *  @param  subjectId the {@code Id} of the subject to monitor 
     *  @throws org.osid.NullArgumentException {@code subjectId is
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewIssuesByTopic(org.osid.id.Id subjectId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated issues. {@code
     *  IssueReceiver.changedIssue()} is invoked when an issue is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedIssues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated issues for the given
     *  queue {@code Id}. {@code IssueReceiver.changedIssue()} is
     *  invoked when an {@code Issue} in this front office is changed.
     *
     *  @param  queueId the {@code Id} of the queue to monitor
     *  @throws org.osid.NullArgumentException {@code queueId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedIssuesForQueue(org.osid.id.Id queueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated issues for the given
     *  customer {@code Id}. {@code IssueReceiver.changedIssue()} is
     *  invoked when an {@code Issue} in this front office is changed.
     *
     *  @param  resourceId the {@code Id} of the customer to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedIssuesForCustomer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated issues for the given
     *  topic.  {@code IssueReceiver.changedIssue()} is invoked when
     *  an issue in this front office is changed.
     *
     *  @param  subjectId the {@code Id} of the subject to monitor 
     *  @throws org.osid.NullArgumentException {@code subjectId is
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedIssuesByTopic(org.osid.id.Id subjectId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated issue. {@code
     *  IssueReceiver.changedIssue()} is invoked when the specified
     *  issue is changed.
     *
     *  @param issueId the {@code Id} of the {@code Issue} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code issueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted issues. {@code
     *  IssueReceiver.deletedIssue()} is invoked when an issue is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedIssues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted issues for the given
     *  queue {@code Id}. {@code IssueReceiver.deletedIssue()} is
     *  invoked when an {@code Issue} is deleted or removed from this
     *  front office.
     *
     *  @param  queueId the {@code Id} of the queue to monitor
     *  @throws org.osid.NullArgumentException {@code queueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedIssuesForQueue(org.osid.id.Id queueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted issues for the given
     *  customer {@code Id}. {@code IssueReceiver.deletedIssue()} is
     *  invoked when an {@code Issue} is deleted or removed from this
     *  front office.
     *
     *  @param  resourceId the {@code Id} of the customer to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedIssuesForCustomer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted issues for the given
     *  topic.  {@code IssueReceiver.deletedIssue()} is invoked when
     *  an issue is deleted or removed from this front office.
     *
     *  @param  subjectId the {@code Id} of the subject to monitor 
     *  @throws org.osid.NullArgumentException {@code subjectId is
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedIssuesByTopic(org.osid.id.Id subjectId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted issue. {@code
     *  IssueReceiver.deletedIssue()} is invoked when the specified
     *  issue is deleted.
     *
     *  @param issueId the {@code Id} of the
     *          {@code Issue} to monitor
     *  @throws org.osid.NullArgumentException {@code issueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a subtask for the given
     *  issue. {@code IssueReceiver.subtaskIssue()} is invoked when
     *  the specified issue is gets a new subtask.
     *
     *  @param issueId the {@code Id} of the {@code Issue} to monitor
     *  @throws org.osid.NullArgumentException {@code issueId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForSubtasks(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a linked issue to the given
     *  issue.  {@code IssueReceiver.linkedIssue()} is invoked when
     *  the specified issue gets a new duplicate.
     *
     *  @param issueId the {@code Id} of the {@code Issue} to monitor
     *  @throws org.osid.NullArgumentException {@code issueId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForLinkedIssues(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an unlinked issue from the given
     *  issue.  {@code IssueReceiver.unlinkedIssue()} is invoked when
     *  the specified issue loses a duplicate.
     *
     *  @param issueId the {@code Id} of the {@code Issue} to monitor
     *  @throws org.osid.NullArgumentException {@code issueId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForUnlinkedIssues(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a blocking issue to the given
     *  issue.  {@code IssueReceiver.blockedIssue()} is invoked when
     *  the specified issue is blocked by another issue.
     *
     *  @param issueId the {@code Id} of the {@code Issue} to monitor
     *  @throws org.osid.NullArgumentException {@code issueId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForBlockingIssues(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of the removal of a blocking issue
     *  to the given issue. {@code IssueReceiver.unblockedIssue()} is
     *  invoked when the specified issue is no longer blocked by
     *  another issue.
     *
     *  @param issueId the {@code Id} of the {@code Issue} to monitor
     *  @throws org.osid.NullArgumentException {@code issueId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForUnblockingIssues(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a blocked issue to the given
     *  issue.  {@code IssueReceiver.blockedIssue()} is invoked when
     *  the specified issue is blocking another issue.
     *
     *  @param issueId the {@code Id} of the {@code Issue} to monitor
     *  @throws org.osid.NullArgumentException {@code issueId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForBlockedIssues(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of the removal of a blocked issue
     *  to the given issue. {@code IssueReceiver.unblockedIssue()} is
     *  invoked when the specified issue is no longer blocking another
     *  issue.
     *
     *  @param issueId the {@code Id} of the {@code Issue} to monitor
     *  @throws org.osid.NullArgumentException {@code issueId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForUnblockedIssues(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }
}
