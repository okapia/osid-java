//
// AbstractRelevancyQueryInspector.java
//
//     A template for making a RelevancyQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.relevancy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for relevancies.
 */

public abstract class AbstractRelevancyQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.ontology.RelevancyQueryInspector {

    private final java.util.Collection<org.osid.ontology.records.RelevancyQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the subject <code> Id </code> terms. 
     *
     *  @return the subject <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubjectIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the subject terms. 
     *
     *  @return the subject terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Gets the mapped <code> Id </code> terms. 
     *
     *  @return the mapped <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMappedIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ontology <code> Id </code> terms. 
     *
     *  @return the ontology <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOntologyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ontology terms. 
     *
     *  @return the ontology terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given relevancy query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a relevancy implementing the requested record.
     *
     *  @param relevancyRecordType a relevancy record type
     *  @return the relevancy query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.RelevancyQueryInspectorRecord getRelevancyQueryInspectorRecord(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.RelevancyQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(relevancyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relevancy query. 
     *
     *  @param relevancyQueryInspectorRecord relevancy query inspector
     *         record
     *  @param relevancyRecordType relevancy record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelevancyQueryInspectorRecord(org.osid.ontology.records.RelevancyQueryInspectorRecord relevancyQueryInspectorRecord, 
                                                   org.osid.type.Type relevancyRecordType) {

        addRecordType(relevancyRecordType);
        nullarg(relevancyRecordType, "relevancy record type");
        this.records.add(relevancyQueryInspectorRecord);        
        return;
    }
}
