//
// CredentialElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.credential.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CredentialElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the CredentialElement Id.
     *
     *  @return the credential element Id
     */

    public static org.osid.id.Id getCredentialEntityId() {
        return (makeEntityId("osid.course.program.Credential"));
    }


    /**
     *  Gets the Lifetime element Id.
     *
     *  @return the Lifetime element Id
     */

    public static org.osid.id.Id getLifetime() {
        return (makeElementId("osid.course.program.credential.Lifetime"));
    }


    /**
     *  Gets the ProgramId element Id.
     *
     *  @return the ProgramId element Id
     */

    public static org.osid.id.Id getProgramId() {
        return (makeQueryElementId("osid.course.program.credential.ProgramId"));
    }


    /**
     *  Gets the Program element Id.
     *
     *  @return the Program element Id
     */

    public static org.osid.id.Id getProgram() {
        return (makeQueryElementId("osid.course.program.credential.Program"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.program.credential.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.program.credential.CourseCatalog"));
    }
}
