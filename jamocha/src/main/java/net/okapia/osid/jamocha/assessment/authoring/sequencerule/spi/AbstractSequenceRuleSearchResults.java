//
// AbstractSequenceRuleSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequencerule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSequenceRuleSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.assessment.authoring.SequenceRuleSearchResults {

    private org.osid.assessment.authoring.SequenceRuleList sequenceRules;
    private final org.osid.assessment.authoring.SequenceRuleQueryInspector inspector;
    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSequenceRuleSearchResults.
     *
     *  @param sequenceRules the result set
     *  @param sequenceRuleQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>sequenceRules</code>
     *          or <code>sequenceRuleQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSequenceRuleSearchResults(org.osid.assessment.authoring.SequenceRuleList sequenceRules,
                                            org.osid.assessment.authoring.SequenceRuleQueryInspector sequenceRuleQueryInspector) {
        nullarg(sequenceRules, "sequence rules");
        nullarg(sequenceRuleQueryInspector, "sequence rule query inspectpr");

        this.sequenceRules = sequenceRules;
        this.inspector = sequenceRuleQueryInspector;

        return;
    }


    /**
     *  Gets the sequence rule list resulting from a search.
     *
     *  @return a sequence rule list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRules() {
        if (this.sequenceRules == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.assessment.authoring.SequenceRuleList sequenceRules = this.sequenceRules;
        this.sequenceRules = null;
	return (sequenceRules);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.assessment.authoring.SequenceRuleQueryInspector getSequenceRuleQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  sequence rule search record <code> Type. </code> This method must
     *  be used to retrieve a sequenceRule implementing the requested
     *  record.
     *
     *  @param sequenceRuleSearchRecordType a sequenceRule search 
     *         record type 
     *  @return the sequence rule search
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(sequenceRuleSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleSearchResultsRecord getSequenceRuleSearchResultsRecord(org.osid.type.Type sequenceRuleSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.assessment.authoring.records.SequenceRuleSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(sequenceRuleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record sequence rule search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSequenceRuleRecord(org.osid.assessment.authoring.records.SequenceRuleSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "sequence rule record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
