//
// AbstractMapAuctionLookupSession
//
//    A simple framework for providing an Auction lookup service
//    backed by a fixed collection of auctions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Auction lookup service backed by a
 *  fixed collection of auctions. The auctions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Auctions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuctionLookupSession
    extends net.okapia.osid.jamocha.bidding.spi.AbstractAuctionLookupSession
    implements org.osid.bidding.AuctionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.bidding.Auction> auctions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.bidding.Auction>());


    /**
     *  Makes an <code>Auction</code> available in this session.
     *
     *  @param  auction an auction
     *  @throws org.osid.NullArgumentException <code>auction<code>
     *          is <code>null</code>
     */

    protected void putAuction(org.osid.bidding.Auction auction) {
        this.auctions.put(auction.getId(), auction);
        return;
    }


    /**
     *  Makes an array of auctions available in this session.
     *
     *  @param  auctions an array of auctions
     *  @throws org.osid.NullArgumentException <code>auctions<code>
     *          is <code>null</code>
     */

    protected void putAuctions(org.osid.bidding.Auction[] auctions) {
        putAuctions(java.util.Arrays.asList(auctions));
        return;
    }


    /**
     *  Makes a collection of auctions available in this session.
     *
     *  @param  auctions a collection of auctions
     *  @throws org.osid.NullArgumentException <code>auctions<code>
     *          is <code>null</code>
     */

    protected void putAuctions(java.util.Collection<? extends org.osid.bidding.Auction> auctions) {
        for (org.osid.bidding.Auction auction : auctions) {
            this.auctions.put(auction.getId(), auction);
        }

        return;
    }


    /**
     *  Removes an Auction from this session.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @throws org.osid.NullArgumentException <code>auctionId<code> is
     *          <code>null</code>
     */

    protected void removeAuction(org.osid.id.Id auctionId) {
        this.auctions.remove(auctionId);
        return;
    }


    /**
     *  Gets the <code>Auction</code> specified by its <code>Id</code>.
     *
     *  @param  auctionId <code>Id</code> of the <code>Auction</code>
     *  @return the auction
     *  @throws org.osid.NotFoundException <code>auctionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>auctionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.Auction getAuction(org.osid.id.Id auctionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.bidding.Auction auction = this.auctions.get(auctionId);
        if (auction == null) {
            throw new org.osid.NotFoundException("auction not found: " + auctionId);
        }

        return (auction);
    }


    /**
     *  Gets all <code>Auctions</code>. In plenary mode, the returned
     *  list contains all known auctions or an error
     *  results. Otherwise, the returned list may contain only those
     *  auctions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Auctions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.auction.ArrayAuctionList(this.auctions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctions.clear();
        super.close();
        return;
    }
}
