//
// AbstractUnknownIngredient.java
//
//     Defines an unknown Ingredient.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.recipe.ingredient.spi;


/**
 *  Defines an unknown <code>Ingredient</code>.
 */

public abstract class AbstractUnknownIngredient
    extends net.okapia.osid.jamocha.recipe.ingredient.spi.AbstractIngredient
    implements org.osid.recipe.Ingredient {

    protected static final String OBJECT = "osid.recipe.Ingredient";


    /**
     *  Constructs a new <code>AbstractUnknownIngredient</code>.
     */

    public AbstractUnknownIngredient() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setQuantity(new java.math.BigDecimal(0));
        setUnitType(new net.okapia.osid.primordium.type.BasicType("okapia.net", "unit", "unknown"));
        setStock(new net.okapia.osid.jamocha.nil.inventory.stock.UnknownStock());

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownIngredient</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownIngredient(boolean optional) {
        this();
        return;
    }
}
