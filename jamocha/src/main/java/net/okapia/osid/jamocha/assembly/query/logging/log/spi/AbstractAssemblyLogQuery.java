//
// AbstractAssemblyLogQuery.java
//
//     A LogQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.logging.log.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A LogQuery that stores terms.
 */

public abstract class AbstractAssemblyLogQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.logging.LogQuery,
               org.osid.logging.LogQueryInspector,
               org.osid.logging.LogSearchOrder {

    private final java.util.Collection<org.osid.logging.records.LogQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.logging.records.LogQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.logging.records.LogSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyLogQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyLogQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a log entry <code> Id. </code> 
     *
     *  @param  logEntryId a log entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLogEntryId(org.osid.id.Id logEntryId, boolean match) {
        getAssembler().addIdTerm(getLogEntryIdColumn(), logEntryId, match);
        return;
    }


    /**
     *  Clesrs the log entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLogEntryIdTerms() {
        getAssembler().clearTerms(getLogEntryIdColumn());
        return;
    }


    /**
     *  Gets the log entry <code> Id </code> query terms. 
     *
     *  @return the log entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLogEntryIdTerms() {
        return (getAssembler().getIdTerms(getLogEntryIdColumn()));
    }


    /**
     *  Gets the LogEntryId column name.
     *
     * @return the column name
     */

    protected String getLogEntryIdColumn() {
        return ("log_entry_id");
    }


    /**
     *  Tests if a log entry query is available. 
     *
     *  @return <code> true </code> if a log entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log entry. 
     *
     *  @return the log entry query 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryQuery getLogEntryQuery() {
        throw new org.osid.UnimplementedException("supportsLogEntryQuery() is false");
    }


    /**
     *  Matches logs with any log entry. 
     *
     *  @param  match <code> true </code> to match logs with any entry, <code> 
     *          false </code> to match logs with no log entries 
     */

    @OSID @Override
    public void matchAnyLogEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getLogEntryColumn(), match);
        return;
    }


    /**
     *  Clesrs the log entry terms. 
     */

    @OSID @Override
    public void clearLogEntryTerms() {
        getAssembler().clearTerms(getLogEntryColumn());
        return;
    }


    /**
     *  Gets the log entry query terms. 
     *
     *  @return the log entry terms 
     */

    @OSID @Override
    public org.osid.logging.LogEntryQueryInspector[] getLogEntryTerms() {
        return (new org.osid.logging.LogEntryQueryInspector[0]);
    }


    /**
     *  Gets the LogEntry column name.
     *
     * @return the column name
     */

    protected String getLogEntryColumn() {
        return ("log_entry");
    }


    /**
     *  Sets the log <code> Id </code> for this query to match logs that have 
     *  the specified log as an ancestor. 
     *
     *  @param  logId a log <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorLogId(org.osid.id.Id logId, boolean match) {
        getAssembler().addIdTerm(getAncestorLogIdColumn(), logId, match);
        return;
    }


    /**
     *  Clesrs the ancestor log <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorLogIdTerms() {
        getAssembler().clearTerms(getAncestorLogIdColumn());
        return;
    }


    /**
     *  Gets the ancestor <code> Id </code> query terms. 
     *
     *  @return the ancestor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorLogIdTerms() {
        return (getAssembler().getIdTerms(getAncestorLogIdColumn()));
    }


    /**
     *  Gets the AncestorLogId column name.
     *
     * @return the column name
     */

    protected String getAncestorLogIdColumn() {
        return ("ancestor_log_id");
    }


    /**
     *  Tests if a <code> LogQuery </code> is available. 
     *
     *  @return <code> true </code> if a log query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorLogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the log query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorLogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogQuery getAncestorLogQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorLogQuery() is false");
    }


    /**
     *  Matches logs with any ancestor. 
     *
     *  @param  match <code> true </code> to match logs with any ancestor, 
     *          <code> false </code> to match root logs 
     */

    @OSID @Override
    public void matchAnyAncestorLog(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorLogColumn(), match);
        return;
    }


    /**
     *  Clesrs the ancestor log terms. 
     */

    @OSID @Override
    public void clearAncestorLogTerms() {
        getAssembler().clearTerms(getAncestorLogColumn());
        return;
    }


    /**
     *  Gets the ancestor query terms. 
     *
     *  @return the ancestor terms 
     */

    @OSID @Override
    public org.osid.logging.LogQueryInspector[] getAncestorLogTerms() {
        return (new org.osid.logging.LogQueryInspector[0]);
    }


    /**
     *  Gets the AncestorLog column name.
     *
     * @return the column name
     */

    protected String getAncestorLogColumn() {
        return ("ancestor_log");
    }


    /**
     *  Sets the log <code> Id </code> for this query to match logs that have 
     *  the specified log as a descendant. 
     *
     *  @param  logId a log <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantLogId(org.osid.id.Id logId, boolean match) {
        getAssembler().addIdTerm(getDescendantLogIdColumn(), logId, match);
        return;
    }


    /**
     *  Clesrs the descendant log <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantLogIdTerms() {
        getAssembler().clearTerms(getDescendantLogIdColumn());
        return;
    }


    /**
     *  Gets the descendant log <code> Id </code> query terms. 
     *
     *  @return the descendant log <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantLogIdTerms() {
        return (getAssembler().getIdTerms(getDescendantLogIdColumn()));
    }


    /**
     *  Gets the DescendantLogId column name.
     *
     * @return the column name
     */

    protected String getDescendantLogIdColumn() {
        return ("descendant_log_id");
    }


    /**
     *  Tests if a <code> LogQuery </code> is available. 
     *
     *  @return <code> true </code> if a log query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantLogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the log query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantLogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogQuery getDescendantLogQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantLogQuery() is false");
    }


    /**
     *  Matches logs with any descendant. 
     *
     *  @param  match <code> true </code> to match logs with any descendant, 
     *          <code> false </code> to match leaf logs 
     */

    @OSID @Override
    public void matchAnyDescendantLog(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantLogColumn(), match);
        return;
    }


    /**
     *  Clesrs the descendant log terms. 
     */

    @OSID @Override
    public void clearDescendantLogTerms() {
        getAssembler().clearTerms(getDescendantLogColumn());
        return;
    }


    /**
     *  Gets the descendant log query terms. 
     *
     *  @return the descendant log terms 
     */

    @OSID @Override
    public org.osid.logging.LogQueryInspector[] getDescendantLogTerms() {
        return (new org.osid.logging.LogQueryInspector[0]);
    }


    /**
     *  Gets the DescendantLog column name.
     *
     * @return the column name
     */

    protected String getDescendantLogColumn() {
        return ("descendant_log");
    }


    /**
     *  Tests if this log supports the given record
     *  <code>Type</code>.
     *
     *  @param  logRecordType a log record type 
     *  @return <code>true</code> if the logRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>logRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type logRecordType) {
        for (org.osid.logging.records.LogQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(logRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  logRecordType the log record type 
     *  @return the log query record 
     *  @throws org.osid.NullArgumentException
     *          <code>logRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogQueryRecord getLogQueryRecord(org.osid.type.Type logRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(logRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  logRecordType the log record type 
     *  @return the log query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>logRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogQueryInspectorRecord getLogQueryInspectorRecord(org.osid.type.Type logRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(logRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param logRecordType the log record type
     *  @return the log search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>logRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogSearchOrderRecord getLogSearchOrderRecord(org.osid.type.Type logRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(logRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this log. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param logQueryRecord the log query record
     *  @param logQueryInspectorRecord the log query inspector
     *         record
     *  @param logSearchOrderRecord the log search order record
     *  @param logRecordType log record type
     *  @throws org.osid.NullArgumentException
     *          <code>logQueryRecord</code>,
     *          <code>logQueryInspectorRecord</code>,
     *          <code>logSearchOrderRecord</code> or
     *          <code>logRecordTypelog</code> is
     *          <code>null</code>
     */
            
    protected void addLogRecords(org.osid.logging.records.LogQueryRecord logQueryRecord, 
                                      org.osid.logging.records.LogQueryInspectorRecord logQueryInspectorRecord, 
                                      org.osid.logging.records.LogSearchOrderRecord logSearchOrderRecord, 
                                      org.osid.type.Type logRecordType) {

        addRecordType(logRecordType);

        nullarg(logQueryRecord, "log query record");
        nullarg(logQueryInspectorRecord, "log query inspector record");
        nullarg(logSearchOrderRecord, "log search odrer record");

        this.queryRecords.add(logQueryRecord);
        this.queryInspectorRecords.add(logQueryInspectorRecord);
        this.searchOrderRecords.add(logSearchOrderRecord);
        
        return;
    }
}
