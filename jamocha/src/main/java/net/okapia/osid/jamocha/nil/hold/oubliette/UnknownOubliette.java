//
// UnknownOubliette.java
//
//     Defines an unknown Oubliette.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.hold.oubliette;


/**
 *  Defines an unknown <code>Oubliette</code>.
 */

public final class UnknownOubliette
    extends net.okapia.osid.jamocha.nil.hold.oubliette.spi.AbstractUnknownOubliette
    implements org.osid.hold.Oubliette {


    /**
     *  Constructs a new <code>UnknownOubliette</code>.
     */

    public UnknownOubliette() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownOubliette</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownOubliette(boolean optional) {
        super(optional);
        addOublietteRecord(new OublietteRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Oubliette.
     *
     *  @return an unknown Oubliette
     */

    public static org.osid.hold.Oubliette create() {
        return (net.okapia.osid.jamocha.builder.validator.hold.oubliette.OublietteValidator.validateOubliette(new UnknownOubliette()));
    }


    public class OublietteRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.hold.records.OublietteRecord {

        
        protected OublietteRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
