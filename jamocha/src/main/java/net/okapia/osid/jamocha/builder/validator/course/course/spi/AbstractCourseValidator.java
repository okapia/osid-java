//
// AbstractCourseValidator.java
//
//     Validates a Course.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.course.spi;


/**
 *  Validates a Course.
 */

public abstract class AbstractCourseValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOperableOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractCourseValidator</code>.
     */

    protected AbstractCourseValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractCourseValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractCourseValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Course.
     *
     *  @param course a course to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>course</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.Course course) {
        super.validate(course);

        test(course.getTitle(), "getTitle()");
        test(course.getNumber(), "getNumber()");

        testConditionalMethod(course, "getSponsorIds", course.hasSponsors(), "hasSponors()");
        testConditionalMethod(course, "getSponsors", course.hasSponsors(), "hasSponors()");
        if (course.hasSponsors()) {
            testNestedObjects(course, "getSponsorIds", "getSponsors");
        }

        testNestedObjects(course, "getCreditAmountIds", "getCreditAmounts");
        test(course.getPrerequisitesInfo(), "getPrerequisitesInfo()");

        testConditionalMethod(course, "getPrerequisiteIds", course.hasPrerequisites(), "hasPrerequisites()");
        testConditionalMethod(course, "getPrerequisites", course.hasPrerequisites(), "hasPrerequisites()");
        if (course.hasPrerequisites()) {
            testNestedObjects(course, "getPrerequisiteIds", "getPrerequisites");
        }

        testNestedObjects(course, "getLevelIds", "getLevels");

        testConditionalMethod(course, "getGradingOptionIds", course.isGraded(), "isGraded()");
        testConditionalMethod(course, "getGradingOptions", course.isGraded(), "isGraded()");
        if (course.isGraded()) {
            testNestedObjects(course, "getGradingOptionIds", "getGradingOptions");
        }

        testConditionalMethod(course, "getLearningObjectiveIds", course.hasLearningObjectives(), "hasLearningObjectives()");
        testConditionalMethod(course, "getLearningObjectives", course.hasLearningObjectives(), "hasLearningObjectives()");
        if (course.hasLearningObjectives()) {
            testNestedObjects(course, "getLearningObjectiveIds", "getLearningObjectives");
        }

        return;
    }
}
