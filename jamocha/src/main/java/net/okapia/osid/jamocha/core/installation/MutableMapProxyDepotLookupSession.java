//
// MutableMapProxyDepotLookupSession
//
//    Implements a Depot lookup service backed by a collection of
//    depots that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation;


/**
 *  Implements a Depot lookup service backed by a collection of
 *  depots. The depots are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of depots can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyDepotLookupSession
    extends net.okapia.osid.jamocha.core.installation.spi.AbstractMapDepotLookupSession
    implements org.osid.installation.DepotLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDepotLookupSession} with no
     *  depots.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyDepotLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyDepotLookupSession} with a
     *  single depot.
     *
     *  @param depot a depot
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code depot} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyDepotLookupSession(org.osid.installation.Depot depot, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDepot(depot);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDepotLookupSession} using an
     *  array of depots.
     *
     *  @param depots an array of depots
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code depots} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyDepotLookupSession(org.osid.installation.Depot[] depots, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDepots(depots);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyDepotLookupSession} using
     *  a collection of depots.
     *
     *  @param depots a collection of depots
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code depots} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyDepotLookupSession(java.util.Collection<? extends org.osid.installation.Depot> depots,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDepots(depots);
        return;
    }

    
    /**
     *  Makes a {@code Depot} available in this session.
     *
     *  @param depot an depot
     *  @throws org.osid.NullArgumentException {@code depot{@code 
     *          is {@code null}
     */

    @Override
    public void putDepot(org.osid.installation.Depot depot) {
        super.putDepot(depot);
        return;
    }


    /**
     *  Makes an array of depots available in this session.
     *
     *  @param depots an array of depots
     *  @throws org.osid.NullArgumentException {@code depots{@code 
     *          is {@code null}
     */

    @Override
    public void putDepots(org.osid.installation.Depot[] depots) {
        super.putDepots(depots);
        return;
    }


    /**
     *  Makes collection of depots available in this session.
     *
     *  @param depots
     *  @throws org.osid.NullArgumentException {@code depot{@code 
     *          is {@code null}
     */

    @Override
    public void putDepots(java.util.Collection<? extends org.osid.installation.Depot> depots) {
        super.putDepots(depots);
        return;
    }


    /**
     *  Removes a Depot from this session.
     *
     *  @param depotId the {@code Id} of the depot
     *  @throws org.osid.NullArgumentException {@code depotId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDepot(org.osid.id.Id depotId) {
        super.removeDepot(depotId);
        return;
    }    
}
