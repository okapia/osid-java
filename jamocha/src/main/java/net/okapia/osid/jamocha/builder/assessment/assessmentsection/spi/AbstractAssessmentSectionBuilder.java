//
// AbstractAssessmentSection.java
//
//     Defines an AssessmentSection builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmentsection.spi;


/**
 *  Defines an <code>AssessmentSection</code> builder.
 */

public abstract class AbstractAssessmentSectionBuilder<T extends AbstractAssessmentSectionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.assessment.assessmentsection.AssessmentSectionMiter assessmentSection;


    /**
     *  Constructs a new <code>AbstractAssessmentSectionBuilder</code>.
     *
     *  @param assessmentSection the assessment section to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAssessmentSectionBuilder(net.okapia.osid.jamocha.builder.assessment.assessmentsection.AssessmentSectionMiter assessmentSection) {
        super(assessmentSection);
        this.assessmentSection = assessmentSection;
        return;
    }


    /**
     *  Builds the assessment section.
     *
     *  @return the new assessment section
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.assessment.AssessmentSection build() {
        (new net.okapia.osid.jamocha.builder.validator.assessment.assessmentsection.AssessmentSectionValidator(getValidations())).validate(this.assessmentSection);
        return (new net.okapia.osid.jamocha.builder.assessment.assessmentsection.ImmutableAssessmentSection(this.assessmentSection));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the assessment section miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.assessment.assessmentsection.AssessmentSectionMiter getMiter() {
        return (this.assessmentSection);
    }


    /**
     *  Sets the allocated time.
     *
     *  @param allocatedTime an allocated time
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>allocatedTime</code> is <code>null</code>
     */

    public T allocatedTime(org.osid.calendaring.Duration allocatedTime) {
        getMiter().setAllocatedTime(allocatedTime);
        return (self());
    }


    /**
     *  Sets the sequential flag.
     *
     *  @param sequential <code> true </code> if the items are taken
     *         sequentially, <code> false </code> if the items can be
     *         skipped and revisited
     *  @return the builder
     */

    public T itemsSequential(boolean sequential) {
        getMiter().setItemsSequential(sequential);
        return (self());
    }


    /**
     *  Sets the shuffled flag.
     *
     *  @param shuffled <code>true</code> if items are shuffled,
     *         <code>false</code> otherwise
     *  @return the builder
     */

    public T itemsShuffled(boolean shuffled) {
        getMiter().setItemsShuffled(shuffled);
        return (self());
    }


    /**
     *  Adds an AssessmentSection record.
     *
     *  @param record an assessment section record
     *  @param recordType the type of assessment section record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.assessment.records.AssessmentSectionRecord record, org.osid.type.Type recordType) {
        getMiter().addAssessmentSectionRecord(record, recordType);
        return (self());
    }
}       


