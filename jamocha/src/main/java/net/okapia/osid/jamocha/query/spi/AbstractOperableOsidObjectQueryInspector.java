//
// AbstractOperableOsidObjectQueryInspector.java
//
//     Defines an operable OsidObjectQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an operable OsidObjectQueryInspector to extend. 
 */

public abstract class AbstractOperableOsidObjectQueryInspector
    extends AbstractOsidObjectQueryInspector
    implements org.osid.OsidOperableQueryInspector,
               org.osid.OsidObjectQueryInspector {
    
    private final OsidOperableQueryInspector inspector = new OsidOperableQueryInspector();


    /**
     *  Gets the active query terms.
     *
     *  @return the active terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getActiveTerms() {
        return (this.inspector.getActiveTerms());
    }


    /**
     *  Adds an active term.
     *
     *  @param term an active term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addActiveTerm(org.osid.search.terms.BooleanTerm term) {
        this.inspector.addActiveTerm(term);
        return;
    }


    /**
     *  Adds a collection of active terms.
     *
     *  @param terms a collection of active terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addActiveTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        this.inspector.addActiveTerms(terms);
        return;
    }


    /**
     *  Adds an active term.
     *
     *  @param active the active flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addActiveTerm(boolean active, boolean match) {
        this.inspector.addActiveTerm(active, match);
        return;
    }


    /**
     *  Gets the enabled query terms.
     *
     *  @return the enabled terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEnabledTerms() {
        return (this.inspector.getEnabledTerms());
    }


    /**
     *  Adds an enabled term.
     *
     *  @param term an enabled term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEnabledTerm(org.osid.search.terms.BooleanTerm term) {
        this.inspector.addEnabledTerm(term);
        return;
    }


    /**
     *  Adds a collection of enabled terms.
     *
     *  @param terms a collection of enabled terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addEnabledTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        this.inspector.addEnabledTerms(terms);
        return;
    }


    /**
     *  Adds an enabled term.
     *
     *  @param enabled the enabled flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addEnabledTerm(boolean enabled, boolean match) {
        this.inspector.addEnabledTerm(enabled, match);
        return;
    }


    /**
     *  Gets the disabled query terms.
     *
     *  @return the disabled terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDisabledTerms() {
        return (this.inspector.getEnabledTerms());
    }


    /**
     *  Adds a disabled term.
     *
     *  @param term a disabled term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addDisabledTerm(org.osid.search.terms.BooleanTerm term) {
        this.inspector.addDisabledTerm(term);
        return;
    }


    /**
     *  Adds a collection of disabled terms.
     *
     *  @param terms a collection of disabled terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addDisabledTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        this.inspector.addDisabledTerms(terms);
        return;
    }


    /**
     *  Adds a disabled term.
     *
     *  @param disabled the disabled flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addDisabledTerm(boolean disabled, boolean match) {
        this.inspector.addDisabledTerm(disabled, match);
        return;
    }


    /**
     *  Gets the operational query terms.
     *
     *  @return the operational terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOperationalTerms() {
        return (this.inspector.getOperationalTerms());
    }


    /**
     *  Adds an operational term.
     *
     *  @param term an operational term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addOperationalTerm(org.osid.search.terms.BooleanTerm term) {
        this.inspector.addOperationalTerm(term);
        return;
    }


    /**
     *  Adds a collection of operational terms.
     *
     *  @param terms a collection of operational terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addOperationalTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        this.inspector.addOperationalTerms(terms);
        return;
    }


    /**
     *  Adds an operational term.
     *
     *  @param operational the operational flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addOperationalTerm(boolean operational, boolean match) {
        this.inspector.addOperationalTerm(operational, match);
        return;
    }


    protected class OsidOperableQueryInspector
        extends AbstractOsidOperableQueryInspector
        implements org.osid.OsidOperableQueryInspector {


        /**
         *  Adds an active term.
         *
         *  @param term an active term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addActiveTerm(org.osid.search.terms.BooleanTerm term) {
            super.addActiveTerm(term);
            return;
        }


        /**
         *  Adds a collection of active terms.
         *
         *  @param terms a collection of active terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addActiveTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
            super.addActiveTerms(terms);
            return;
        }


        /**
         *  Adds an active term.
         *
         *  @param active the active flag
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         */

        @Override
        protected void addActiveTerm(boolean active, boolean match) {
            super.addActiveTerm(active, match);
            return;
        }


        /**
         *  Adds an enabled term.
         *
         *  @param term an enabled term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addEnabledTerm(org.osid.search.terms.BooleanTerm term) {
            super.addEnabledTerm(term);
            return;
        }


        /**
         *  Adds a collection of enabled terms.
         *
         *  @param terms a collection of enabled terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addEnabledTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
            super.addEnabledTerms(terms);
            return;
        }


        /**
         *  Adds an enabled term.
         *
         *  @param enabled the enabled flag
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         */

        @Override
        protected void addEnabledTerm(boolean enabled, boolean match) {
            super.addEnabledTerm(enabled, match);
            return;
        }


        /**
         *  Adds a disabled term.
         *
         *  @param term a disabled term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addDisabledTerm(org.osid.search.terms.BooleanTerm term) {
            super.addDisabledTerm(term);
            return;
        }


        /**
         *  Adds a collection of disabled terms.
         *
         *  @param terms a collection of disabled terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addDisabledTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
            super.addDisabledTerms(terms);
            return;
        }


        /**
         *  Adds a disabled term.
         *
         *  @param disabled the disabled flag
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         */

        @Override
        protected void addDisabledTerm(boolean disabled, boolean match) {
            super.addDisabledTerm(disabled, match);
            return;
        }


        /**
         *  Adds an operational term.
         *
         *  @param term an operational term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addOperationalTerm(org.osid.search.terms.BooleanTerm term) {
            super.addOperationalTerm(term);
            return;
        }


        /**
         *  Adds a collection of operational terms.
         *
         *  @param terms a collection of operational terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addOperationalTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
            super.addOperationalTerms(terms);
            return;
        }


        /**
         *  Adds an operational term.
         *
         *  @param operational the operational flag
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         */

        @Override
        protected void addOperationalTerm(boolean operational, boolean match) {
            super.addOperationalTerm(operational, match);
            return;
        }
    }
}
