//
// AbstractFederatingFloorLookupSession.java
//
//     An abstract federating adapter for a FloorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  FloorLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingFloorLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.room.FloorLookupSession>
    implements org.osid.room.FloorLookupSession {

    private boolean parallel = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();


    /**
     *  Constructs a new <code>AbstractFederatingFloorLookupSession</code>.
     */

    protected AbstractFederatingFloorLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.room.FloorLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Floor</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFloors() {
        for (org.osid.room.FloorLookupSession session : getSessions()) {
            if (session.canLookupFloors()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Floor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFloorView() {
        for (org.osid.room.FloorLookupSession session : getSessions()) {
            session.useComparativeFloorView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Floor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFloorView() {
        for (org.osid.room.FloorLookupSession session : getSessions()) {
            session.usePlenaryFloorView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include floors in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        for (org.osid.room.FloorLookupSession session : getSessions()) {
            session.useFederatedCampusView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        for (org.osid.room.FloorLookupSession session : getSessions()) {
            session.useIsolatedCampusView();
        }

        return;
    }


    /**
     *  Only floors whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveFloorView() {
        for (org.osid.room.FloorLookupSession session : getSessions()) {
            session.useEffectiveFloorView();
        }

        return;
    }


    /**
     *  All floors of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveFloorView() {
        for (org.osid.room.FloorLookupSession session : getSessions()) {
            session.useAnyEffectiveFloorView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Floor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Floor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Floor</code> and
     *  retained for compatibility.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @param  floorId <code>Id</code> of the
     *          <code>Floor</code>
     *  @return the floor
     *  @throws org.osid.NotFoundException <code>floorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>floorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Floor getFloor(org.osid.id.Id floorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.room.FloorLookupSession session : getSessions()) {
            try {
                return (session.getFloor(floorId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(floorId + " not found");
    }


    /**
     *  Gets a <code>FloorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  floors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Floors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  @param  floorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>floorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByIds(org.osid.id.IdList floorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.room.floor.MutableFloorList ret = new net.okapia.osid.jamocha.room.floor.MutableFloorList();

        try (org.osid.id.IdList ids = floorIds) {
            while (ids.hasNext()) {
                ret.addFloor(getFloor(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>FloorList</code> corresponding to the given
     *  floor genus <code>Type</code> which does not include
     *  floors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  @param  floorGenusType a floor genus type 
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>floorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByGenusType(org.osid.type.Type floorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.floor.FederatingFloorList ret = getFloorList();

        for (org.osid.room.FloorLookupSession session : getSessions()) {
            ret.addFloorList(session.getFloorsByGenusType(floorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FloorList</code> corresponding to the given
     *  floor genus <code>Type</code> and include any additional
     *  floors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  @param  floorGenusType a floor genus type 
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>floorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByParentGenusType(org.osid.type.Type floorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.floor.FederatingFloorList ret = getFloorList();

        for (org.osid.room.FloorLookupSession session : getSessions()) {
            ret.addFloorList(session.getFloorsByParentGenusType(floorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FloorList</code> containing the given
     *  floor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  @param  floorRecordType a floor record type 
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByRecordType(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.floor.FederatingFloorList ret = getFloorList();

        for (org.osid.room.FloorLookupSession session : getSessions()) {
            ret.addFloorList(session.getFloorsByRecordType(floorRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FloorList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible
     *  through this session.
     *  
     *  In active mode, floors are returned that are currently
     *  active. In any status mode, active and inactive floors
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Floor</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.FloorList getFloorsOnDate(org.osid.calendaring.DateTime from, 
                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.floor.FederatingFloorList ret = getFloorList();

        for (org.osid.room.FloorLookupSession session : getSessions()) {
            ret.addFloorList(session.getFloorsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     * Gets a list of all floors corresponding to a building
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known floors or an
     *  error results. Otherwise, the returned list may contain only those
     *  floors that are accessible through this session.
     *
     *  In effective mode, floors are returned that are currently effective.
     *  In any effective mode, effective floors and those currently expired
     *  are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building
     *  @return the returned <code>FloorList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.floor.FederatingFloorList ret = getFloorList();

        for (org.osid.room.FloorLookupSession session : getSessions()) {
            ret.addFloorList(session.getFloorsForBuilding(buildingId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all floors corresponding to a building <code> Id
     *  </code> and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known floors or an
     *  error results. Otherwise, the returned list may contain only those
     *  floors that are accessible through this session.
     *
     *  In effective mode, floors are returned that are currently effective.
     *  In any effective mode, effective floors and those currently expired
     *  are returned.
     *
     *  @param  buildingId a building <code> Id </code>
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code> FloorList </code>
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> buildingId, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsForBuildingOnDate(org.osid.id.Id buildingId,
                                                              org.osid.calendaring.DateTime from,
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
              org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.floor.FederatingFloorList ret = getFloorList();

        for (org.osid.room.FloorLookupSession session : getSessions()) {
            ret.addFloorList(session.getFloorsForBuildingOnDate(buildingId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FloorList</code> containing of the given
     *  floor number.
     *
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session.
     *
     *  In effective mode, floors are returned that are currently
     *  effective. In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @param  number a floor number
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.floor.FederatingFloorList ret = getFloorList();

        for (org.osid.room.FloorLookupSession session : getSessions()) {
            ret.addFloorList(session.getFloorsByNumber(number));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Floors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Floors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.floor.FederatingFloorList ret = getFloorList();

        for (org.osid.room.FloorLookupSession session : getSessions()) {
            ret.addFloorList(session.getFloors());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.room.floor.FederatingFloorList getFloorList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.room.floor.ParallelFloorList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.room.floor.CompositeFloorList());
        }
    }
}
