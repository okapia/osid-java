//
// AbstractAdapterCommissionLookupSession.java
//
//    A Commission lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Commission lookup session adapter.
 */

public abstract class AbstractAdapterCommissionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.CommissionLookupSession {

    private final org.osid.resourcing.CommissionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCommissionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCommissionLookupSession(org.osid.resourcing.CommissionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code Commission} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCommissions() {
        return (this.session.canLookupCommissions());
    }


    /**
     *  A complete view of the {@code Commission} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommissionView() {
        this.session.useComparativeCommissionView();
        return;
    }


    /**
     *  A complete view of the {@code Commission} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommissionView() {
        this.session.usePlenaryCommissionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commissions in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only commissions whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCommissionView() {
        this.session.useEffectiveCommissionView();
        return;
    }
    

    /**
     *  All commissions of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCommissionView() {
        this.session.useAnyEffectiveCommissionView();
        return;
    }

     
    /**
     *  Gets the {@code Commission} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Commission} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Commission} and
     *  retained for compatibility.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param commissionId {@code Id} of the {@code Commission}
     *  @return the commission
     *  @throws org.osid.NotFoundException {@code commissionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code commissionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Commission getCommission(org.osid.id.Id commissionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommission(commissionId));
    }


    /**
     *  Gets a {@code CommissionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  commissions specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Commissions} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Commission} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code commissionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByIds(org.osid.id.IdList commissionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsByIds(commissionIds));
    }


    /**
     *  Gets a {@code CommissionList} corresponding to the given
     *  commission genus {@code Type} which does not include
     *  commissions of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionGenusType a commission genus type 
     *  @return the returned {@code Commission} list
     *  @throws org.osid.NullArgumentException
     *          {@code commissionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByGenusType(org.osid.type.Type commissionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsByGenusType(commissionGenusType));
    }


    /**
     *  Gets a {@code CommissionList} corresponding to the given
     *  commission genus {@code Type} and include any additional
     *  commissions with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionGenusType a commission genus type 
     *  @return the returned {@code Commission} list
     *  @throws org.osid.NullArgumentException
     *          {@code commissionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByParentGenusType(org.osid.type.Type commissionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsByParentGenusType(commissionGenusType));
    }


    /**
     *  Gets a {@code CommissionList} containing the given commission
     *  record {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionRecordType a commission record type 
     *  @return the returned {@code Commission} list
     *  @throws org.osid.NullArgumentException
     *          {@code commissionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByRecordType(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsByRecordType(commissionRecordType));
    }


    /**
     *  Gets a {@code CommissionList} effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *  
     *  In active mode, commissions are returned that are currently
     *  active. In any status mode, active and inactive commissions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Commission} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsOnDate(org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsOnDate(from, to));
    }
        

    /**
     *  Gets a list of commissions corresponding to a resource {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code CommissionList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsForResource(resourceId));
    }


    /**
     *  Gets a list of commissions corresponding to a resource {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CommissionList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResourceOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of commissions corresponding to a work {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  workId the {@code Id} of the work
     *  @return the returned {@code CommissionList}
     *  @throws org.osid.NullArgumentException {@code workId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForWork(org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsForWork(workId));
    }


    /**
     *  Gets a list of commissions corresponding to a work {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  workId the {@code Id} of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CommissionList}
     *  @throws org.osid.NullArgumentException {@code workId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForWorkOnDate(org.osid.id.Id workId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsForWorkOnDate(workId, from, to));
    }


    /**
     *  Gets a list of commissions corresponding to resource and work
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  workId the {@code Id} of the work
     *  @return the returned {@code CommissionList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code workId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResourceAndWork(org.osid.id.Id resourceId,
                                                                               org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsForResourceAndWork(resourceId, workId));
    }


    /**
     *  Gets a list of commissions corresponding to resource and work
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective. In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  workId the {@code Id} of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CommissionList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code workId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResourceAndWorkOnDate(org.osid.id.Id resourceId,
                                                                                     org.osid.id.Id workId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissionsForResourceAndWorkOnDate(resourceId, workId, from, to));
    }


    /**
     *  Gets all {@code Commissions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Commissions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommissions());
    }
}
