//
// AbstractKeyBatchFormList
//
//     Implements a filter for a KeyBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.authentication.keys.batch.keybatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a KeyBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedKeyBatchFormList
 *  to improve performance.
 */

public abstract class AbstractKeyBatchFormFilterList
    extends net.okapia.osid.jamocha.authentication.keys.batch.keybatchform.spi.AbstractKeyBatchFormList
    implements org.osid.authentication.keys.batch.KeyBatchFormList,
               net.okapia.osid.jamocha.inline.filter.authentication.keys.batch.keybatchform.KeyBatchFormFilter {

    private org.osid.authentication.keys.batch.KeyBatchForm keyBatchForm;
    private final org.osid.authentication.keys.batch.KeyBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractKeyBatchFormFilterList</code>.
     *
     *  @param keyBatchFormList a <code>KeyBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>keyBatchFormList</code> is <code>null</code>
     */

    protected AbstractKeyBatchFormFilterList(org.osid.authentication.keys.batch.KeyBatchFormList keyBatchFormList) {
        nullarg(keyBatchFormList, "key batch form list");
        this.list = keyBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.keyBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> KeyBatchForm </code> in this list. 
     *
     *  @return the next <code> KeyBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> KeyBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.keys.batch.KeyBatchForm getNextKeyBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.authentication.keys.batch.KeyBatchForm keyBatchForm = this.keyBatchForm;
            this.keyBatchForm = null;
            return (keyBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in key batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.keyBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters KeyBatchForms.
     *
     *  @param keyBatchForm the key batch form to filter
     *  @return <code>true</code> if the key batch form passes the filter,
     *          <code>false</code> if the key batch form should be filtered
     */

    public abstract boolean pass(org.osid.authentication.keys.batch.KeyBatchForm keyBatchForm);


    protected void prime() {
        if (this.keyBatchForm != null) {
            return;
        }

        org.osid.authentication.keys.batch.KeyBatchForm keyBatchForm = null;

        while (this.list.hasNext()) {
            try {
                keyBatchForm = this.list.getNextKeyBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(keyBatchForm)) {
                this.keyBatchForm = keyBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
