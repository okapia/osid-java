//
// VaultElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.vault.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class VaultElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the VaultElement Id.
     *
     *  @return the vault element Id
     */

    public static org.osid.id.Id getVaultEntityId() {
        return (makeEntityId("osid.authorization.Vault"));
    }


    /**
     *  Gets the FunctionId element Id.
     *
     *  @return the FunctionId element Id
     */

    public static org.osid.id.Id getFunctionId() {
        return (makeQueryElementId("osid.authorization.vault.FunctionId"));
    }


    /**
     *  Gets the Function element Id.
     *
     *  @return the Function element Id
     */

    public static org.osid.id.Id getFunction() {
        return (makeQueryElementId("osid.authorization.vault.Function"));
    }


    /**
     *  Gets the QualifierId element Id.
     *
     *  @return the QualifierId element Id
     */

    public static org.osid.id.Id getQualifierId() {
        return (makeQueryElementId("osid.authorization.vault.QualifierId"));
    }


    /**
     *  Gets the Qualifier element Id.
     *
     *  @return the Qualifier element Id
     */

    public static org.osid.id.Id getQualifier() {
        return (makeQueryElementId("osid.authorization.vault.Qualifier"));
    }


    /**
     *  Gets the AuthorizationId element Id.
     *
     *  @return the AuthorizationId element Id
     */

    public static org.osid.id.Id getAuthorizationId() {
        return (makeQueryElementId("osid.authorization.vault.AuthorizationId"));
    }


    /**
     *  Gets the Authorization element Id.
     *
     *  @return the Authorization element Id
     */

    public static org.osid.id.Id getAuthorization() {
        return (makeQueryElementId("osid.authorization.vault.Authorization"));
    }


    /**
     *  Gets the AncestorVaultId element Id.
     *
     *  @return the AncestorVaultId element Id
     */

    public static org.osid.id.Id getAncestorVaultId() {
        return (makeQueryElementId("osid.authorization.vault.AncestorVaultId"));
    }


    /**
     *  Gets the AncestorVault element Id.
     *
     *  @return the AncestorVault element Id
     */

    public static org.osid.id.Id getAncestorVault() {
        return (makeQueryElementId("osid.authorization.vault.AncestorVault"));
    }


    /**
     *  Gets the DescendantVaultId element Id.
     *
     *  @return the DescendantVaultId element Id
     */

    public static org.osid.id.Id getDescendantVaultId() {
        return (makeQueryElementId("osid.authorization.vault.DescendantVaultId"));
    }


    /**
     *  Gets the DescendantVault element Id.
     *
     *  @return the DescendantVault element Id
     */

    public static org.osid.id.Id getDescendantVault() {
        return (makeQueryElementId("osid.authorization.vault.DescendantVault"));
    }
}
