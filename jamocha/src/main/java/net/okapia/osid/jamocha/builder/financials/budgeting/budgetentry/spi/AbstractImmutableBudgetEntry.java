//
// AbstractImmutableBudgetEntry.java
//
//     Wraps a mutable BudgetEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.budgeting.budgetentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>BudgetEntry</code> to hide modifiers. This
 *  wrapper provides an immutized BudgetEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying budgetEntry whose state changes are visible.
 */

public abstract class AbstractImmutableBudgetEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.financials.budgeting.BudgetEntry {

    private final org.osid.financials.budgeting.BudgetEntry budgetEntry;


    /**
     *  Constructs a new <code>AbstractImmutableBudgetEntry</code>.
     *
     *  @param budgetEntry the budget entry to immutablize
     *  @throws org.osid.NullArgumentException <code>budgetEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBudgetEntry(org.osid.financials.budgeting.BudgetEntry budgetEntry) {
        super(budgetEntry);
        this.budgetEntry = budgetEntry;
        return;
    }


    /**
     *  Gets the budget <code> Id. </code> 
     *
     *  @return the budget <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBudgetId() {
        return (this.budgetEntry.getBudgetId());
    }


    /**
     *  Gets the budget. 
     *
     *  @return the budget 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.budgeting.Budget getBudget()
        throws org.osid.OperationFailedException {

        return (this.budgetEntry.getBudget());
    }


    /**
     *  Gets the account <code> Id. </code> 
     *
     *  @return the account <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAccountId() {
        return (this.budgetEntry.getAccountId());
    }


    /**
     *  Gets the account. 
     *
     *  @return the account 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount()
        throws org.osid.OperationFailedException {

        return (this.budgetEntry.getAccount());
    }


    /**
     *  Gets the amount of this budget entries. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.budgetEntry.getAmount());
    }


    /**
     *  Tests if the budgeted amount is to be debited or a credited to this 
     *  activity. 
     *
     *  @return <code> true </code> if this item amount is a debit, <code> 
     *          false </code> if it is a credit 
     */

    @OSID @Override
    public boolean isDebit() {
        return (this.budgetEntry.isDebit());
    }


    /**
     *  Gets the budget entry record corresponding to the given <code> 
     *  BudgetEntry </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  budgetEntryRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(budgetEntryRecordType) </code> is <code> true </code> . 
     *
     *  @param  budgetEntryRecordType the type of budget entry record to 
     *          retrieve 
     *  @return the budget entry record 
     *  @throws org.osid.NullArgumentException <code> budgetEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(budgetEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetEntryRecord getBudgetEntryRecord(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.budgetEntry.getBudgetEntryRecord(budgetEntryRecordType));
    }
}

