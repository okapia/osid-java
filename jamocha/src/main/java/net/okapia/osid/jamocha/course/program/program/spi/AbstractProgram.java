//
// AbstractProgram.java
//
//     Defines a Program.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Program</code>.
 */

public abstract class AbstractProgram
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObject
    implements org.osid.course.program.Program {

    private org.osid.locale.DisplayText title;
    private String number;
    private org.osid.locale.DisplayText completionRequirementsInfo;

    private boolean hasSponsors = false;
    private boolean hasCompletionRequirements = false;
    private boolean earnsCredentials          = false;

    private final java.util.Collection<org.osid.resource.Resource> sponsors = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.Requisite> completionRequirements = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.Credential> credentials = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.records.ProgramRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the formal title of this program. It may be the same as the 
     *  display name or it may be used to more formally label the program. 
     *
     *  @return the program title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.title);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    protected void setTitle(org.osid.locale.DisplayText title) {
        nullarg(title, "title");
        this.title = title;
        return;
    }


    /**
     *  Gets the program number which is a label generally used to
     *  index the program in a catalog, such as "16c."
     *
     *  @return the program number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.number);
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    protected void setNumber(String number) {
        nullarg(number, "program number");
        this.number = number;
        return;
    }


    /**
     *  Tests if this program has a sponsor. 
     *
     *  @return <code> true </code> if this program has sponsors, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.hasSponsors);
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        try {
            org.osid.resource.ResourceList sponsors = getSponsors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(sponsors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.sponsors));
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    protected void addSponsor(org.osid.resource.Resource sponsor) {
        nullarg(sponsor, "sponsor");

        this.sponsors.add(sponsor);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    protected void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        nullarg(sponsors, "sponsors");

        this.sponsors.clear();
        this.sponsors.addAll(sponsors);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Gets the an informational string for the program completion. 
     *
     *  @return the program completion 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCompletionRequirementsInfo() {
        return (this.completionRequirementsInfo);
    }


    /**
     *  Sets the completion requirements info.
     *
     *  @param info a completion requirements info
     *  @throws org.osid.NullArgumentException
     *          <code>info</code> is <code>null</code>
     */

    protected void setCompletionRequirementsInfo(org.osid.locale.DisplayText info) {
        nullarg(info, "completion requirements info");
        this.completionRequirementsInfo = info;
        return;
    }


    /**
     *  Tests if this program has a rule for the program completion. 
     *
     *  @return <code> true </code> if this program has a completion rule, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCompletionRequirements() {
        return (this.hasCompletionRequirements);
    }


    /**
     *  Gets the <code> Requisite </code> <code> Ids </code> for the program 
     *  completion. 
     *
     *  @return the completion requisite <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasCompletionRequirements() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCompletionRequirementIds() {
        if (!hasCompletionRequirements()) {
            throw new org.osid.IllegalStateException("hasCompletionRequirements() is false");
        }

        try {
            org.osid.course.requisite.RequisiteList completionRequirements = getCompletionRequirements();
            return (new net.okapia.osid.jamocha.adapter.converter.course.requisite.requisite.RequisiteToIdList(completionRequirements));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the requisites for the program completion. Each <code> Requisite 
     *  </code> is an <code> AND </code> term and must be true for the 
     *  requirements to be satisifed. 
     *
     *  @return the completion requisites 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasCompletionRequirements() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getCompletionRequirements()
        throws org.osid.OperationFailedException {

        if (!hasCompletionRequirements()) {
            throw new org.osid.IllegalStateException("hasCompletionRequirements() is false");
        }

        return (new net.okapia.osid.jamocha.course.requisite.requisite.ArrayRequisiteList(this.completionRequirements));
    }


    /**
     *  Adds a completion requirement.
     *
     *  @param requirement a completion requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    protected void addCompletionRequirement(org.osid.course.requisite.Requisite requirement) {
        nullarg(requirement, "completion requirement");

        this.completionRequirements.add(requirement);
        this.hasCompletionRequirements = true;

        return;
    }


    /**
     *  Sets all the completion requirements.
     *
     *  @param requirements a collection of completion requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    protected void setCompletionRequirements(java.util.Collection<org.osid.course.requisite.Requisite> requirements) {
        nullarg(requirements, "completion requirements");

        this.completionRequirements.clear();
        this.completionRequirements.addAll(requirements);
        this.hasCompletionRequirements = true;

        return;
    }


    /**
     *  Tests if completion of this program results in credentials awarded. 
     *
     *  @return <code> true </code> if this program earns credentials, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean earnsCredentials() {
        return (this.earnsCredentials);
    }


    /**
     *  Gets the awarded credential <code> Ids. </code> 
     *
     *  @return the returned list of credential <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> earnsCredentials() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCredentialIds() {
        if (!earnsCredentials()) {
            throw new org.osid.IllegalStateException("earnsCredentials() is false");
        }

        try {
            org.osid.course.program.CredentialList credentials = getCredentials();
            return (new net.okapia.osid.jamocha.adapter.converter.course.program.credential.CredentialToIdList(credentials));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the awarded credentials. 
     *
     *  @return the returned list of credentials 
     *  @throws org.osid.IllegalStateException <code> earnsCredentials() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentials()
        throws org.osid.OperationFailedException {

        if (!earnsCredentials()) {
            throw new org.osid.IllegalStateException("earnsCredentials() is false");
        }

        return (new net.okapia.osid.jamocha.course.program.credential.ArrayCredentialList(this.credentials));
    }


    /**
     *  Adds a credential.
     *
     *  @param credential a credential
     *  @throws org.osid.NullArgumentException <code>credential</code>
     *          is <code>null</code>
     */

    protected void addCredential(org.osid.course.program.Credential credential) {
        nullarg(credential, "credential");

        this.credentials.add(credential);
        this.earnsCredentials = true;

        return;
    }


    /**
     *  Sets all the credentials.
     *
     *  @param credentials a collection of credentials
     *  @throws org.osid.NullArgumentException
     *          <code>credentials</code> is <code>null</code>
     */

    protected void setCredentials(java.util.Collection<org.osid.course.program.Credential> credentials) {
        nullarg(credentials, "credentials");

        this.credentials.clear();
        this.credentials.addAll(credentials);
        this.earnsCredentials = true;

        return;
    }


    /**
     *  Tests if this program supports the given record
     *  <code>Type</code>.
     *
     *  @param  programRecordType a program record type 
     *  @return <code>true</code> if the programRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type programRecordType) {
        for (org.osid.course.program.records.ProgramRecord record : this.records) {
            if (record.implementsRecordType(programRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Program</code> record <code>Type</code>.
     *
     *  @param  programRecordType the program record type 
     *  @return the program record 
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramRecord getProgramRecord(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramRecord record : this.records) {
            if (record.implementsRecordType(programRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param programRecord the program record
     *  @param programRecordType program record type
     *  @throws org.osid.NullArgumentException
     *          <code>programRecord</code> or
     *          <code>programRecordTypeprogram</code> is
     *          <code>null</code>
     */
            
    protected void addProgramRecord(org.osid.course.program.records.ProgramRecord programRecord, 
                                    org.osid.type.Type programRecordType) {

        nullarg(programRecord, "program record");
        addRecordType(programRecordType);
        this.records.add(programRecord);
        
        return;
    }
}
