//
// AbstractCredentialSearch.java
//
//     A template for making a Credential Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.credential.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing credential searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCredentialSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.program.CredentialSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.program.records.CredentialSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.program.CredentialSearchOrder credentialSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of credentials. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  credentialIds list of credentials
     *  @throws org.osid.NullArgumentException
     *          <code>credentialIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCredentials(org.osid.id.IdList credentialIds) {
        while (credentialIds.hasNext()) {
            try {
                this.ids.add(credentialIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCredentials</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of credential Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCredentialIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  credentialSearchOrder credential search order 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>credentialSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCredentialResults(org.osid.course.program.CredentialSearchOrder credentialSearchOrder) {
	this.credentialSearchOrder = credentialSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.program.CredentialSearchOrder getCredentialSearchOrder() {
	return (this.credentialSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given credential search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a credential implementing the requested record.
     *
     *  @param credentialSearchRecordType a credential search record
     *         type
     *  @return the credential search record
     *  @throws org.osid.NullArgumentException
     *          <code>credentialSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.CredentialSearchRecord getCredentialSearchRecord(org.osid.type.Type credentialSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.program.records.CredentialSearchRecord record : this.records) {
            if (record.implementsRecordType(credentialSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credential search. 
     *
     *  @param credentialSearchRecord credential search record
     *  @param credentialSearchRecordType credential search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCredentialSearchRecord(org.osid.course.program.records.CredentialSearchRecord credentialSearchRecord, 
                                           org.osid.type.Type credentialSearchRecordType) {

        addRecordType(credentialSearchRecordType);
        this.records.add(credentialSearchRecord);        
        return;
    }
}
