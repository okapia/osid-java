//
// AbstractAssemblyFunctionQuery.java
//
//     A FunctionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.authorization.function.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A FunctionQuery that stores terms.
 */

public abstract class AbstractAssemblyFunctionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.authorization.FunctionQuery,
               org.osid.authorization.FunctionQueryInspector,
               org.osid.authorization.FunctionSearchOrder {

    private final java.util.Collection<org.osid.authorization.records.FunctionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.records.FunctionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.records.FunctionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyFunctionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyFunctionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the qualifier hierarchy <code> Id </code> for this query. 
     *
     *  @param  qualifierHierarchyId a hierarchy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchQualifierHierarchyId(org.osid.id.Id qualifierHierarchyId, 
                                          boolean match) {
        getAssembler().addIdTerm(getQualifierHierarchyIdColumn(), qualifierHierarchyId, match);
        return;
    }


    /**
     *  Clears the qualifier hierarchy <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQualifierHierarchyIdTerms() {
        getAssembler().clearTerms(getQualifierHierarchyIdColumn());
        return;
    }


    /**
     *  Gets the qualifier hierarchy <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualifierHierarchyIdTerms() {
        return (getAssembler().getIdTerms(getQualifierHierarchyIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the qualifier 
     *  hierarchy. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQualifierHierarchy(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQualifierHierarchyColumn(), style);
        return;
    }


    /**
     *  Gets the QualifierHierarchyId column name.
     *
     * @return the column name
     */

    protected String getQualifierHierarchyIdColumn() {
        return ("qualifier_hierarchy_id");
    }


    /**
     *  Tests if a <code> HierarchyQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier hierarchy query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier hierarchy. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the qualifier hierarchy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQuery getQualifierHierarchyQuery() {
        throw new org.osid.UnimplementedException("supportsQualifierHierarchyQuery() is false");
    }


    /**
     *  Matches functions that have any qualifier hierarchy. 
     *
     *  @param  match <code> true </code> to match functions with any 
     *          qualifier hierarchy, <code> false </code> to match functions 
     *          with no qualifier hierarchy 
     */

    @OSID @Override
    public void matchAnyQualifierHierarchy(boolean match) {
        getAssembler().addIdWildcardTerm(getQualifierHierarchyColumn(), match);
        return;
    }


    /**
     *  Clears the qualifier hierarchy query terms. 
     */

    @OSID @Override
    public void clearQualifierHierarchyTerms() {
        getAssembler().clearTerms(getQualifierHierarchyColumn());
        return;
    }


    /**
     *  Gets the qualifier hierarchy query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQueryInspector[] getQualifierHierarchyTerms() {
        return (new org.osid.hierarchy.HierarchyQueryInspector[0]);
    }


    /**
     *  Tests if a <code> HierarchySearchOrder </code> interface is available. 
     *
     *  @return <code> true </code> if a qualifier hierarchy search order 
     *          interface is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order interface for a qualifier hierarchy. 
     *
     *  @return the hierarchy search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySearchOrder getQualifierHierarchySearchOrder() {
        throw new org.osid.UnimplementedException("supportsQualifierHierarchySearchOrder() is false");
    }


    /**
     *  Gets the QualifierHierarchy column name.
     *
     * @return the column name
     */

    protected String getQualifierHierarchyColumn() {
        return ("qualifier_hierarchy");
    }


    /**
     *  Sets the authorization <code> Id </code> for this query. 
     *
     *  @param  authorizationId an authorization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> authorizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuthorizationId(org.osid.id.Id authorizationId, 
                                     boolean match) {
        getAssembler().addIdTerm(getAuthorizationIdColumn(), authorizationId, match);
        return;
    }


    /**
     *  Clears the authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuthorizationIdTerms() {
        getAssembler().clearTerms(getAuthorizationIdColumn());
        return;
    }


    /**
     *  Gets the authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuthorizationIdTerms() {
        return (getAssembler().getIdTerms(getAuthorizationIdColumn()));
    }


    /**
     *  Gets the AuthorizationId column name.
     *
     * @return the column name
     */

    protected String getAuthorizationIdColumn() {
        return ("authorization_id");
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an authorization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the authorization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getAuthorizationQuery() {
        throw new org.osid.UnimplementedException("supportsAuthorizationQuery() is false");
    }


    /**
     *  Matches functions that have any authorization mapping. 
     *
     *  @param  match <code> true </code> to match functions with any 
     *          authorization mapping, <code> false </code> to match functions 
     *          with no authorization mapping 
     */

    @OSID @Override
    public void matchAnyAuthorization(boolean match) {
        getAssembler().addIdWildcardTerm(getAuthorizationColumn(), match);
        return;
    }


    /**
     *  Clears the authorization query terms. 
     */

    @OSID @Override
    public void clearAuthorizationTerms() {
        getAssembler().clearTerms(getAuthorizationColumn());
        return;
    }


    /**
     *  Gets the authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the Authorization column name.
     *
     * @return the column name
     */

    protected String getAuthorizationColumn() {
        return ("authorization");
    }


    /**
     *  Sets the vault <code> Id </code> for this query. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVaultId(org.osid.id.Id vaultId, boolean match) {
        getAssembler().addIdTerm(getVaultIdColumn(), vaultId, match);
        return;
    }


    /**
     *  Clears the vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVaultIdTerms() {
        getAssembler().clearTerms(getVaultIdColumn());
        return;
    }


    /**
     *  Gets the vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVaultIdTerms() {
        return (getAssembler().getIdTerms(getVaultIdColumn()));
    }


    /**
     *  Gets the VaultId column name.
     *
     * @return the column name
     */

    protected String getVaultIdColumn() {
        return ("vault_id");
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getVaultQuery() {
        throw new org.osid.UnimplementedException("supportsVaultQuery() is false");
    }


    /**
     *  Clears the vault query terms. 
     */

    @OSID @Override
    public void clearVaultTerms() {
        getAssembler().clearTerms(getVaultColumn());
        return;
    }


    /**
     *  Gets the vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }


    /**
     *  Gets the Vault column name.
     *
     * @return the column name
     */

    protected String getVaultColumn() {
        return ("vault");
    }


    /**
     *  Tests if this function supports the given record
     *  <code>Type</code>.
     *
     *  @param  functionRecordType a function record type 
     *  @return <code>true</code> if the functionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type functionRecordType) {
        for (org.osid.authorization.records.FunctionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(functionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  functionRecordType the function record type 
     *  @return the function query record 
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(functionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionQueryRecord getFunctionQueryRecord(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.FunctionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(functionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(functionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  functionRecordType the function record type 
     *  @return the function query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(functionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionQueryInspectorRecord getFunctionQueryInspectorRecord(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.FunctionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(functionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(functionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param functionRecordType the function record type
     *  @return the function search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(functionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionSearchOrderRecord getFunctionSearchOrderRecord(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.FunctionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(functionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(functionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this function. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param functionQueryRecord the function query record
     *  @param functionQueryInspectorRecord the function query inspector
     *         record
     *  @param functionSearchOrderRecord the function search order record
     *  @param functionRecordType function record type
     *  @throws org.osid.NullArgumentException
     *          <code>functionQueryRecord</code>,
     *          <code>functionQueryInspectorRecord</code>,
     *          <code>functionSearchOrderRecord</code> or
     *          <code>functionRecordTypefunction</code> is
     *          <code>null</code>
     */
            
    protected void addFunctionRecords(org.osid.authorization.records.FunctionQueryRecord functionQueryRecord, 
                                      org.osid.authorization.records.FunctionQueryInspectorRecord functionQueryInspectorRecord, 
                                      org.osid.authorization.records.FunctionSearchOrderRecord functionSearchOrderRecord, 
                                      org.osid.type.Type functionRecordType) {

        addRecordType(functionRecordType);

        nullarg(functionQueryRecord, "function query record");
        nullarg(functionQueryInspectorRecord, "function query inspector record");
        nullarg(functionSearchOrderRecord, "function search odrer record");

        this.queryRecords.add(functionQueryRecord);
        this.queryInspectorRecords.add(functionQueryInspectorRecord);
        this.searchOrderRecords.add(functionSearchOrderRecord);
        
        return;
    }
}
