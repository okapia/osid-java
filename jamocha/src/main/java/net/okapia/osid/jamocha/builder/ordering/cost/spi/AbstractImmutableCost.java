//
// AbstractImmutableCost.java
//
//     Wraps a mutable Cost to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.cost.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Cost</code> to hide modifiers. This
 *  wrapper provides an immutized Cost from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying cost whose state changes are visible.
 */

public abstract class AbstractImmutableCost
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutable
    implements org.osid.ordering.Cost {

    private final org.osid.ordering.Cost cost;


    /**
     *  Constructs a new <code>AbstractImmutableCost</code>.
     *
     *  @param cost the cost to immutablize
     *  @throws org.osid.NullArgumentException <code>cost</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCost(org.osid.ordering.Cost cost) {
        super(cost);
        this.cost = cost;
        return;
    }


    /**
     *  Gets the price schedule <code> Id </code> to which this price
     *  belongs.
     *
     *  @return the price schedule <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPriceScheduleId() {
        return (this.cost.getPriceScheduleId());
    }


    /**
     *  Gets the price schedule to which this price belongs. 
     *
     *  @return the price schedule 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceSchedule getPriceSchedule()
        throws org.osid.OperationFailedException {

        return (this.cost.getPriceSchedule());
    }


    /**
     *  Gets the cost. 
     *
     *  @return the cost 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.cost.getAmount());
    }
}
