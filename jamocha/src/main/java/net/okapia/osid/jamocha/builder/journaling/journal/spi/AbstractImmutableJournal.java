//
// AbstractImmutableJournal.java
//
//     Wraps a mutable Journal to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.journaling.journal.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Journal</code> to hide modifiers. This
 *  wrapper provides an immutized Journal from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying journal whose state changes are visible.
 */

public abstract class AbstractImmutableJournal
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.journaling.Journal {

    private final org.osid.journaling.Journal journal;


    /**
     *  Constructs a new <code>AbstractImmutableJournal</code>.
     *
     *  @param journal the journal to immutablize
     *  @throws org.osid.NullArgumentException <code>journal</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableJournal(org.osid.journaling.Journal journal) {
        super(journal);
        this.journal = journal;
        return;
    }


    /**
     *  Gets the journal record corresponding to the given <code> Journal 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> journalRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(journalRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  journalRecordType the type of journal record to retrieve 
     *  @return the journal record 
     *  @throws org.osid.NullArgumentException <code> journalRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(journalRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.records.JournalRecord getJournalRecord(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException {

        return (this.journal.getJournalRecord(journalRecordType));
    }
}

