//
// AbstractAssemblyInputQuery.java
//
//     An InputQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.input.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InputQuery that stores terms.
 */

public abstract class AbstractAssemblyInputQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.control.InputQuery,
               org.osid.control.InputQueryInspector,
               org.osid.control.InputSearchOrder {

    private final java.util.Collection<org.osid.control.records.InputQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.InputQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.InputSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyInputQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyInputQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the device <code> Id </code> for this query. 
     *
     *  @param  deviceId the device <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> deviceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDeviceId(org.osid.id.Id deviceId, boolean match) {
        getAssembler().addIdTerm(getDeviceIdColumn(), deviceId, match);
        return;
    }


    /**
     *  Clears the device <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDeviceIdTerms() {
        getAssembler().clearTerms(getDeviceIdColumn());
        return;
    }


    /**
     *  Gets the device <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDeviceIdTerms() {
        return (getAssembler().getIdTerms(getDeviceIdColumn()));
    }


    /**
     *  Orders the results by device. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDevice(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDeviceColumn(), style);
        return;
    }


    /**
     *  Gets the DeviceId column name.
     *
     * @return the column name
     */

    protected String getDeviceIdColumn() {
        return ("device_id");
    }


    /**
     *  Tests if a <code> DeviceQuery </code> is available. 
     *
     *  @return <code> true </code> if a device query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a device. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the device query 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuery getDeviceQuery() {
        throw new org.osid.UnimplementedException("supportsDeviceQuery() is false");
    }


    /**
     *  Clears the device query terms. 
     */

    @OSID @Override
    public void clearDeviceTerms() {
        getAssembler().clearTerms(getDeviceColumn());
        return;
    }


    /**
     *  Gets the device query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.DeviceQueryInspector[] getDeviceTerms() {
        return (new org.osid.control.DeviceQueryInspector[0]);
    }


    /**
     *  Tests if a device search order is available. 
     *
     *  @return <code> true </code> if a device search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the device search order. 
     *
     *  @return the device search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsDeviceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSearchOrder getDeviceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDeviceSearchOrder() is false");
    }


    /**
     *  Gets the Device column name.
     *
     * @return the column name
     */

    protected String getDeviceColumn() {
        return ("device");
    }


    /**
     *  Sets the controller <code> Id </code> for this query. 
     *
     *  @param  controllerId the controller <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchControllerId(org.osid.id.Id controllerId, boolean match) {
        getAssembler().addIdTerm(getControllerIdColumn(), controllerId, match);
        return;
    }


    /**
     *  Clears the controller <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearControllerIdTerms() {
        getAssembler().clearTerms(getControllerIdColumn());
        return;
    }


    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getControllerIdTerms() {
        return (getAssembler().getIdTerms(getControllerIdColumn()));
    }


    /**
     *  Orders the results by controller. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByController(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getControllerColumn(), style);
        return;
    }


    /**
     *  Gets the ControllerId column name.
     *
     * @return the column name
     */

    protected String getControllerIdColumn() {
        return ("controller_id");
    }


    /**
     *  Tests if a <code> ControllerQuery </code> is available. 
     *
     *  @return <code> true </code> if a controller query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> ControllerQuery. </code> Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the controller query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuery getControllerQuery() {
        throw new org.osid.UnimplementedException("supportsControllerQuery() is false");
    }


    /**
     *  Clears the controller query terms. 
     */

    @OSID @Override
    public void clearControllerTerms() {
        getAssembler().clearTerms(getControllerColumn());
        return;
    }


    /**
     *  Gets the controller query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getControllerTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Tests if a controller search order is available. 
     *
     *  @return <code> true </code> if a controller search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the controller search order. 
     *
     *  @return the controller search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsControllerSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchOrder getControllerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsControllerSearchOrder() is false");
    }


    /**
     *  Gets the Controller column name.
     *
     * @return the column name
     */

    protected String getControllerColumn() {
        return ("controller");
    }


    /**
     *  Sets the system <code> Id </code> for this query to match inputs 
     *  assigned to systems. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sustemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this input supports the given record
     *  <code>Type</code>.
     *
     *  @param  inputRecordType an input record type 
     *  @return <code>true</code> if the inputRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inputRecordType) {
        for (org.osid.control.records.InputQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inputRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  inputRecordType the input record type 
     *  @return the input query record 
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.InputQueryRecord getInputQueryRecord(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.InputQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inputRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  inputRecordType the input record type 
     *  @return the input query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.InputQueryInspectorRecord getInputQueryInspectorRecord(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.InputQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(inputRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param inputRecordType the input record type
     *  @return the input search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.InputSearchOrderRecord getInputSearchOrderRecord(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.InputSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(inputRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this input. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inputQueryRecord the input query record
     *  @param inputQueryInspectorRecord the input query inspector
     *         record
     *  @param inputSearchOrderRecord the input search order record
     *  @param inputRecordType input record type
     *  @throws org.osid.NullArgumentException
     *          <code>inputQueryRecord</code>,
     *          <code>inputQueryInspectorRecord</code>,
     *          <code>inputSearchOrderRecord</code> or
     *          <code>inputRecordTypeinput</code> is
     *          <code>null</code>
     */
            
    protected void addInputRecords(org.osid.control.records.InputQueryRecord inputQueryRecord, 
                                      org.osid.control.records.InputQueryInspectorRecord inputQueryInspectorRecord, 
                                      org.osid.control.records.InputSearchOrderRecord inputSearchOrderRecord, 
                                      org.osid.type.Type inputRecordType) {

        addRecordType(inputRecordType);

        nullarg(inputQueryRecord, "input query record");
        nullarg(inputQueryInspectorRecord, "input query inspector record");
        nullarg(inputSearchOrderRecord, "input search odrer record");

        this.queryRecords.add(inputQueryRecord);
        this.queryInspectorRecords.add(inputQueryInspectorRecord);
        this.searchOrderRecords.add(inputSearchOrderRecord);
        
        return;
    }
}
