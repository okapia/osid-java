//
// AbstractActivityBundleQueryInspector.java
//
//     A template for making an ActivityBundleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activitybundle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for activity bundles.
 */

public abstract class AbstractActivityBundleQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.course.registration.ActivityBundleQueryInspector {

    private final java.util.Collection<org.osid.course.registration.records.ActivityBundleQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the course offering <code> Id </code> query terms. 
     *
     *  @return the course offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course offering query terms. 
     *
     *  @return the course offering query terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the credits query terms. 
     *
     *  @return the credits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getCreditsTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradingOptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradingOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given activity bundle query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an activity bundle implementing the requested record.
     *
     *  @param activityBundleRecordType an activity bundle record type
     *  @return the activity bundle query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityBundleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityBundleQueryInspectorRecord getActivityBundleQueryInspectorRecord(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityBundleQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(activityBundleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityBundleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity bundle query. 
     *
     *  @param activityBundleQueryInspectorRecord activity bundle query inspector
     *         record
     *  @param activityBundleRecordType activityBundle record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityBundleQueryInspectorRecord(org.osid.course.registration.records.ActivityBundleQueryInspectorRecord activityBundleQueryInspectorRecord, 
                                                   org.osid.type.Type activityBundleRecordType) {

        addRecordType(activityBundleRecordType);
        nullarg(activityBundleRecordType, "activity bundle record type");
        this.records.add(activityBundleQueryInspectorRecord);        
        return;
    }
}
