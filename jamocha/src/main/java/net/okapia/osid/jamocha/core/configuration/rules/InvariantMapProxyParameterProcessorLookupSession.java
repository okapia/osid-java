//
// InvariantMapProxyParameterProcessorLookupSession
//
//    Implements a ParameterProcessor lookup service backed by a fixed
//    collection of parameterProcessors. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules;


/**
 *  Implements a ParameterProcessor lookup service backed by a fixed
 *  collection of parameter processors. The parameter processors are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyParameterProcessorLookupSession
    extends net.okapia.osid.jamocha.core.configuration.rules.spi.AbstractMapParameterProcessorLookupSession
    implements org.osid.configuration.rules.ParameterProcessorLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyParameterProcessorLookupSession} with no
     *  parameter processors.
     *
     *  @param configuration the configuration
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.proxy.Proxy proxy) {
        setConfiguration(configuration);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyParameterProcessorLookupSession} with a single
     *  parameter processor.
     *
     *  @param configuration the configuration
     *  @param parameterProcessor a single parameter processor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameterProcessor} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.rules.ParameterProcessor parameterProcessor, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameterProcessor(parameterProcessor);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyParameterProcessorLookupSession} using
     *  an array of parameter processors.
     *
     *  @param configuration the configuration
     *  @param parameterProcessors an array of parameter processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameterProcessors} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.rules.ParameterProcessor[] parameterProcessors, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameterProcessors(parameterProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyParameterProcessorLookupSession} using a
     *  collection of parameter processors.
     *
     *  @param configuration the configuration
     *  @param parameterProcessors a collection of parameter processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameterProcessors} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                                  java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessor> parameterProcessors,
                                                  org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameterProcessors(parameterProcessors);
        return;
    }
}
