//
// AbstractImmutableResourceRelationship.java
//
//     Wraps a mutable ResourceRelationship to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.resourcerelationship.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ResourceRelationship</code> to hide modifiers. This
 *  wrapper provides an immutized ResourceRelationship from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying resourceRelationship whose state changes are visible.
 */

public abstract class AbstractImmutableResourceRelationship
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.resource.ResourceRelationship {

    private final org.osid.resource.ResourceRelationship resourceRelationship;


    /**
     *  Constructs a new <code>AbstractImmutableResourceRelationship</code>.
     *
     *  @param resourceRelationship the resource relationship to immutablize
     *  @throws org.osid.NullArgumentException <code>resourceRelationship</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableResourceRelationship(org.osid.resource.ResourceRelationship resourceRelationship) {
        super(resourceRelationship);
        this.resourceRelationship = resourceRelationship;
        return;
    }


    /**
     *  Gets the source resource <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceResourceId() {
        return (this.resourceRelationship.getSourceResourceId());
    }


    /**
     *  Gets the <code> Resource. </code> 
     *
     *  @return the source resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSourceResource()
        throws org.osid.OperationFailedException {

        return (this.resourceRelationship.getSourceResource());
    }


    /**
     *  Gets the destination resource <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDestinationResourceId() {
        return (this.resourceRelationship.getDestinationResourceId());
    }


    /**
     *  Gets the <code> Resource. </code> 
     *
     *  @return the destination resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getDestinationResource()
        throws org.osid.OperationFailedException {

        return (this.resourceRelationship.getDestinationResource());
    }


    /**
     *  Gets the resource relationship record corresponding to the given 
     *  <code> ResourceRelationship </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> resourceRelationshipRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(resourceRelationshipRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  resourceRelationshipRecordType the type of the record to 
     *          retrieve 
     *  @return the resource relationship record 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(resourceRelationshipRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRelationshipRecord getResourceRelationshipRecord(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException {

        return (this.resourceRelationship.getResourceRelationshipRecord(resourceRelationshipRecordType));
    }
}

