//
// AbstractMappingManager.java
//
//     An adapter for a MappingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MappingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMappingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.mapping.MappingManager>
    implements org.osid.mapping.MappingManager {


    /**
     *  Constructs a new {@code AbstractAdapterMappingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMappingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMappingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMappingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any map federation is exposed. Federation is exposed when a 
     *  specific map may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of maps appears 
     *  as a single map. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up locations is supported. 
     *
     *  @return <code> true </code> if location lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationLookup() {
        return (getAdapteeManager().supportsLocationLookup());
    }


    /**
     *  Tests if querying locations is supported. 
     *
     *  @return <code> true </code> if location query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (getAdapteeManager().supportsLocationQuery());
    }


    /**
     *  Tests if searching locations is supported. 
     *
     *  @return <code> true </code> if location search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSearch() {
        return (getAdapteeManager().supportsLocationSearch());
    }


    /**
     *  Tests if location <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if location administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationAdmin() {
        return (getAdapteeManager().supportsLocationAdmin());
    }


    /**
     *  Tests if a location <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if location notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationNotification() {
        return (getAdapteeManager().supportsLocationNotification());
    }


    /**
     *  Tests if a location <code> </code> hierarchy service is supported. 
     *
     *  @return <code> true </code> if location hierarchy is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationHierarchy() {
        return (getAdapteeManager().supportsLocationHierarchy());
    }


    /**
     *  Tests if a location hierarchy design service is supported. 
     *
     *  @return <code> true </code> if location hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationHierarchyDesign() {
        return (getAdapteeManager().supportsLocationHierarchyDesign());
    }


    /**
     *  Tests if a location map lookup service is supported. 
     *
     *  @return <code> true </code> if a location map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationMap() {
        return (getAdapteeManager().supportsLocationMap());
    }


    /**
     *  Tests if a location map assignment service is supported. 
     *
     *  @return <code> true </code> if a location to map assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationMapAssignment() {
        return (getAdapteeManager().supportsLocationMapAssignment());
    }


    /**
     *  Tests if a location smart map service is supported. 
     *
     *  @return <code> true </code> if a location smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSmartMap() {
        return (getAdapteeManager().supportsLocationSmartMap());
    }


    /**
     *  Tests if a location adjacency service is supported. 
     *
     *  @return <code> true </code> if a location adjacency service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationAdjacency() {
        return (getAdapteeManager().supportsLocationAdjacency());
    }


    /**
     *  Tests if a location spatial service is supported. 
     *
     *  @return <code> true </code> if a location spatial service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSpatial() {
        return (getAdapteeManager().supportsLocationSpatial());
    }


    /**
     *  Tests if a resource location service is supported. 
     *
     *  @return <code> true </code> if a resource location service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceLocation() {
        return (getAdapteeManager().supportsResourceLocation());
    }


    /**
     *  Tests if a resource location update service is supported. 
     *
     *  @return <code> true </code> if a resource location update service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceLocationUpdate() {
        return (getAdapteeManager().supportsResourceLocationUpdate());
    }


    /**
     *  Tests if a resource location notification service is supported. 
     *
     *  @return <code> true </code> if a resource location notification 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceLocationNotification() {
        return (getAdapteeManager().supportsResourceLocationNotification());
    }


    /**
     *  Tests if a resource position notification service is supported. 
     *
     *  @return <code> true </code> if a resource position notification 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcePositionNotification() {
        return (getAdapteeManager().supportsResourcePositionNotification());
    }


    /**
     *  Tests if a location service is supported for the current agent. 
     *
     *  @return <code> true </code> if my location is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyLocation() {
        return (getAdapteeManager().supportsMyLocation());
    }


    /**
     *  Tests if looking up maps is supported. 
     *
     *  @return <code> true </code> if map lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapLookup() {
        return (getAdapteeManager().supportsMapLookup());
    }


    /**
     *  Tests if querying maps is supported. 
     *
     *  @return <code> true </code> if a map query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (getAdapteeManager().supportsMapQuery());
    }


    /**
     *  Tests if searching maps is supported. 
     *
     *  @return <code> true </code> if map search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapSearch() {
        return (getAdapteeManager().supportsMapSearch());
    }


    /**
     *  Tests if map administrative service is supported. 
     *
     *  @return <code> true </code> if map administration is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapAdmin() {
        return (getAdapteeManager().supportsMapAdmin());
    }


    /**
     *  Tests if a map <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if map notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapNotification() {
        return (getAdapteeManager().supportsMapNotification());
    }


    /**
     *  Tests for the availability of a map hierarchy traversal service. 
     *
     *  @return <code> true </code> if map hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapHierarchy() {
        return (getAdapteeManager().supportsMapHierarchy());
    }


    /**
     *  Tests for the availability of a map hierarchy design service. 
     *
     *  @return <code> true </code> if map hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapHierarchyDesign() {
        return (getAdapteeManager().supportsMapHierarchyDesign());
    }


    /**
     *  Tests if the mapping batch service is supported. 
     *
     *  @return <code> true </code> if maping batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingBatch() {
        return (getAdapteeManager().supportsMappingBatch());
    }


    /**
     *  Tests if the mapping path service is supported. 
     *
     *  @return <code> true </code> if maping path service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingPath() {
        return (getAdapteeManager().supportsMappingPath());
    }


    /**
     *  Tests if the mapping route service is supported. 
     *
     *  @return <code> true </code> if maping route service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingRoute() {
        return (getAdapteeManager().supportsMappingRoute());
    }


    /**
     *  Gets the supported <code> Location </code> record types. 
     *
     *  @return a list containing the supported <code> Location </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLocationRecordTypes() {
        return (getAdapteeManager().getLocationRecordTypes());
    }


    /**
     *  Tests if the given <code> Location </code> record type is supported. 
     *
     *  @param  locationRecordType a <code> Type </code> indicating a <code> 
     *          Location </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> locationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLocationRecordType(org.osid.type.Type locationRecordType) {
        return (getAdapteeManager().supportsLocationRecordType(locationRecordType));
    }


    /**
     *  Gets the supported <code> Location </code> search types. 
     *
     *  @return a list containing the supported <code> Location </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLocationSearchRecordTypes() {
        return (getAdapteeManager().getLocationSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Location </code> search type is supported. 
     *
     *  @param  locationSearchRecordType a <code> Type </code> indicating a 
     *          <code> Location </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> locationSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLocationSearchRecordType(org.osid.type.Type locationSearchRecordType) {
        return (getAdapteeManager().supportsLocationSearchRecordType(locationSearchRecordType));
    }


    /**
     *  Gets the supported <code> Map </code> record types. 
     *
     *  @return a list containing the supported <code> Map </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMapRecordTypes() {
        return (getAdapteeManager().getMapRecordTypes());
    }


    /**
     *  Tests if the given <code> Map </code> record type is supported. 
     *
     *  @param  mapRecordType a <code> Type </code> indicating a <code> Map 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mapRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMapRecordType(org.osid.type.Type mapRecordType) {
        return (getAdapteeManager().supportsMapRecordType(mapRecordType));
    }


    /**
     *  Gets the supported <code> Map </code> search record types. 
     *
     *  @return a list containing the supported <code> Map </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMapSearchRecordTypes() {
        return (getAdapteeManager().getMapSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Map </code> search record type is supported. 
     *
     *  @param  mapSearchRecordType a <code> Type </code> indicating a <code> 
     *          Map </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mapSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMapSearchRecordType(org.osid.type.Type mapSearchRecordType) {
        return (getAdapteeManager().supportsMapSearchRecordType(mapSearchRecordType));
    }


    /**
     *  Gets the supported <code> ResourceLocation </code> record types. 
     *
     *  @return a list containing the supported <code> ResourceLocation 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceLocationRecordTypes() {
        return (getAdapteeManager().getResourceLocationRecordTypes());
    }


    /**
     *  Tests if the given <code> ResourceLocationRecord </code> record type 
     *  is supported. 
     *
     *  @param  resourceLocationRecordType a <code> Type </code> indicating a 
     *          <code> ResourceLocation </code> type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceLocationRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResourceLocationRecordType(org.osid.type.Type resourceLocationRecordType) {
        return (getAdapteeManager().supportsResourceLocationRecordType(resourceLocationRecordType));
    }


    /**
     *  Gets the supported <code> Coordinate </code> types. 
     *
     *  @return a list containing the supported <code> Coordinate </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateTypes() {
        return (getAdapteeManager().getCoordinateTypes());
    }


    /**
     *  Tests if the given <code> Coordinate </code> type is supported. 
     *
     *  @param  coordinateType a <code> Type </code> indicating a <code> 
     *          Coordinate </code> type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateType(org.osid.type.Type coordinateType) {
        return (getAdapteeManager().supportsCoordinateType(coordinateType));
    }


    /**
     *  Gets the supported <code> Heading </code> types. 
     *
     *  @return a list containing the supported <code> Heading </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHeadingTypes() {
        return (getAdapteeManager().getHeadingTypes());
    }


    /**
     *  Tests if the given <code> Heading </code> type is supported. 
     *
     *  @param  headingType a <code> Type </code> indicating a <code> Heading 
     *          </code> type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> headingType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHeadingType(org.osid.type.Type headingType) {
        return (getAdapteeManager().supportsHeadingType(headingType));
    }


    /**
     *  Gets the supported <code> SpatialUnit </code> record types. 
     *
     *  @return a list containing the supported <code> SpatialUnit </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpatialUnitRecordTypes() {
        return (getAdapteeManager().getSpatialUnitRecordTypes());
    }


    /**
     *  Tests if the given <code> SpatialUnit </code> record type is 
     *  supported. 
     *
     *  @param  spatialUnitRecordType a <code> Type </code> indicating a 
     *          <code> SpatialUnit </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        return (getAdapteeManager().supportsSpatialUnitRecordType(spatialUnitRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  lookup service. 
     *
     *  @return a <code> LocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> LocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationLookupSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location query 
     *  service. 
     *
     *  @return a <code> LocationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuerySession getLocationQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationQuerySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuerySession getLocationQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationQuerySessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  search service. 
     *
     *  @return a <code> LocationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchSession getLocationSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchSession getLocationSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationSearchSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  administration service. 
     *
     *  @return a <code> LocationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdminSession getLocationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdminSession getLocationAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationAdminSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  notification service. 
     *
     *  @param  locationReceiver the notification callback 
     *  @return a <code> LocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> locationReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationNotificationSession getLocationNotificationSession(org.osid.mapping.LocationReceiver locationReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationNotificationSession(locationReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  notification service for the given map. 
     *
     *  @param  locationReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> locationReceiver </code> 
     *          or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationNotificationSession getLocationNotificationSessionForMap(org.osid.mapping.LocationReceiver locationReceiver, 
                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationNotificationSessionForMap(locationReceiver, mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy service. 
     *
     *  @return a <code> LocationHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchySession getLocationHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationHierarchySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchySession getLocationHierarchySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationHierarchySessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy design service. 
     *
     *  @return a <code> LocationHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchyDesignSession getLocationHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationHierarchyDesignSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy design service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationHierarchySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchyDesignSession getLocationHierarchyDesignSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationHierarchyDesignSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup location/map mappings. 
     *
     *  @return a <code> LocationMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationMapSession getLocationMapSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationMapSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  locations to maps. 
     *
     *  @return a <code> LocationMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationMapAssignmentSession getLocationMapAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationMapAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSmartMapSession getLocationSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationSmartMapSession(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  adjacency service. 
     *
     *  @return a <code> LocationAdjacencySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationAdjacency() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdjacencySession getLocationAdjacencySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationAdjacencySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  adjacency service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationAdjacencySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationAdjacency() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdjacencySession getLocationAdjacencySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationAdjacencySessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  spatial service. 
     *
     *  @return a <code> LocationSpatialSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSpatial() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSpatialSession getLocationSpatialSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationSpatialSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  spatial service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationSpatialSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSpatial() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSpatialSession getLocationSpatialSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLocationSpatialSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location service. 
     *
     *  @return a <code> ResourceLocationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationSession getResourceLocationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceLocationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceLocationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocation() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationSession getResourceLocationSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceLocationSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location update service. 
     *
     *  @return a <code> ResourceLocationUpdateSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationUpdate() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationUpdateSession getResourceLocationUpdateSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceLocationUpdateSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location update service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceLocationUpdateSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationUpdate() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationUpdateSession getResourceLocationUpdateSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceLocationUpdateSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location notification service. 
     *
     *  @param  resourceLocationReceiver the notification callback 
     *  @return a <code> ResourceLocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceLocationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationNotificationSession getResourceLocationNotificationSession(org.osid.mapping.ResourceLocationReceiver resourceLocationReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceLocationNotificationSession(resourceLocationReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location notification service for the given map. 
     *
     *  @param  resourceLocationReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceLocationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourceLocationReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationNotificationSession getResourceLocationNotificationSessionForMap(org.osid.mapping.ResourceLocationReceiver resourceLocationReceiver, 
                                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceLocationNotificationSessionForMap(resourceLocationReceiver, mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  position notification service. 
     *
     *  @param  resourcePositionReceiver the notification callback 
     *  @return a <code> ResourcePositionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePositionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePositionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourcePositionNotificationSession getResourcePositionNotificationSession(org.osid.mapping.ResourcePositionReceiver resourcePositionReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourcePositionNotificationSession(resourcePositionReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  position notification service for the given map. 
     *
     *  @param  resourcePositionReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourcePositionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePositionReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePositionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourcePositionNotificationSession getResourcePositionNotificationSessionForMap(org.osid.mapping.ResourcePositionReceiver resourcePositionReceiver, 
                                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourcePositionNotificationSessionForMap(resourcePositionReceiver, mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my location 
     *  service. 
     *
     *  @return a <code> MyLocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MyLocationSession getMyLocationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyLocationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my location 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> MyLocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MyLocationSession getMyLocationSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyLocationSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map lookup 
     *  service. 
     *
     *  @return a <code> MapLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapLookupSession getMapLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMapLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map query 
     *  service. 
     *
     *  @return a <code> MapQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuerySession getMapQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMapQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map search 
     *  service. 
     *
     *  @return a <code> MapSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapSearchSession getMapSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMapSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map 
     *  administrative service. 
     *
     *  @return a <code> MapAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapAdminSession getMapAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMapAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map 
     *  notification service. 
     *
     *  @param  mapReceiver the notification callback 
     *  @return a <code> MapNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> mapReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMapNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapNotificationSession getMapNotificationSession(org.osid.mapping.MapReceiver mapReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMapNotificationSession(mapReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map hierarchy 
     *  service. 
     *
     *  @return a <code> MapHierarchySession </code> for maps 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapHierarchySession getMapHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMapHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map hierarchy 
     *  design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for maps 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMapHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapHierarchyDesignSession getMapHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMapHierarchyDesignSession());
    }


    /**
     *  Gets the mapping batch manager. 
     *
     *  @return a <code> MappingBatchManager </code> for paths 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMappingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.batch.MappingBatchManager getMappingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMappingBatchManager());
    }


    /**
     *  Gets the mapping path manager. 
     *
     *  @return a <code> MappingPathManager </code> for paths 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMappingPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.MappingPathManager getMappingPathManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMappingPathManager());
    }


    /**
     *  Gets the mapping route manager. 
     *
     *  @return a <code> MappingRouteManager </code> for routes 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMappingRoute() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.MappingRouteManager getMappingRouteManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMappingRouteManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
