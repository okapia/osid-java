//
// InvariantMapMessageLookupSession
//
//    Implements a Message lookup service backed by a fixed collection of
//    messages.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging;


/**
 *  Implements a Message lookup service backed by a fixed
 *  collection of messages. The messages are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapMessageLookupSession
    extends net.okapia.osid.jamocha.core.messaging.spi.AbstractMapMessageLookupSession
    implements org.osid.messaging.MessageLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapMessageLookupSession</code> with no
     *  messages.
     *  
     *  @param mailbox the mailbox
     *  @throws org.osid.NullArgumnetException {@code mailbox} is
     *          {@code null}
     */

    public InvariantMapMessageLookupSession(org.osid.messaging.Mailbox mailbox) {
        setMailbox(mailbox);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapMessageLookupSession</code> with a single
     *  message.
     *  
     *  @param mailbox the mailbox
     *  @param message a single message
     *  @throws org.osid.NullArgumentException {@code mailbox} or
     *          {@code message} is <code>null</code>
     */

      public InvariantMapMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                               org.osid.messaging.Message message) {
        this(mailbox);
        putMessage(message);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapMessageLookupSession</code> using an array
     *  of messages.
     *  
     *  @param mailbox the mailbox
     *  @param messages an array of messages
     *  @throws org.osid.NullArgumentException {@code mailbox} or
     *          {@code messages} is <code>null</code>
     */

      public InvariantMapMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                               org.osid.messaging.Message[] messages) {
        this(mailbox);
        putMessages(messages);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapMessageLookupSession</code> using a
     *  collection of messages.
     *
     *  @param mailbox the mailbox
     *  @param messages a collection of messages
     *  @throws org.osid.NullArgumentException {@code mailbox} or
     *          {@code messages} is <code>null</code>
     */

      public InvariantMapMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                               java.util.Collection<? extends org.osid.messaging.Message> messages) {
        this(mailbox);
        putMessages(messages);
        return;
    }
}
