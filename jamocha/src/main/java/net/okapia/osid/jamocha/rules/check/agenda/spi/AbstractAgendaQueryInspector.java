//
// AbstractAgendaQueryInspector.java
//
//     A template for making an AgendaQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.agenda.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for agendas.
 */

public abstract class AbstractAgendaQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.rules.check.AgendaQueryInspector {

    private final java.util.Collection<org.osid.rules.check.records.AgendaQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the instruction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstructionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the instruction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQueryInspector[] getInstructionTerms() {
        return (new org.osid.rules.check.InstructionQueryInspector[0]);
    }


    /**
     *  Gets the engine <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEngineIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the engine query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.EngineQueryInspector[] getEngineTerms() {
        return (new org.osid.rules.EngineQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given agenda query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an agenda implementing the requested record.
     *
     *  @param agendaRecordType an agenda record type
     *  @return the agenda query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agendaRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.AgendaQueryInspectorRecord getAgendaQueryInspectorRecord(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.AgendaQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(agendaRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agendaRecordType + " is not supported");
    }


    /**
     *  Adds a record to this agenda query. 
     *
     *  @param agendaQueryInspectorRecord agenda query inspector
     *         record
     *  @param agendaRecordType agenda record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAgendaQueryInspectorRecord(org.osid.rules.check.records.AgendaQueryInspectorRecord agendaQueryInspectorRecord, 
                                                   org.osid.type.Type agendaRecordType) {

        addRecordType(agendaRecordType);
        nullarg(agendaRecordType, "agenda record type");
        this.records.add(agendaQueryInspectorRecord);        
        return;
    }
}
