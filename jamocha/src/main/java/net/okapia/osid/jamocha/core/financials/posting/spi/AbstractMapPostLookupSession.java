//
// AbstractMapPostLookupSession
//
//    A simple framework for providing a Post lookup service
//    backed by a fixed collection of posts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Post lookup service backed by a
 *  fixed collection of posts. The posts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Posts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPostLookupSession
    extends net.okapia.osid.jamocha.financials.posting.spi.AbstractPostLookupSession
    implements org.osid.financials.posting.PostLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.financials.posting.Post> posts = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.financials.posting.Post>());


    /**
     *  Makes a <code>Post</code> available in this session.
     *
     *  @param  post a post
     *  @throws org.osid.NullArgumentException <code>post<code>
     *          is <code>null</code>
     */

    protected void putPost(org.osid.financials.posting.Post post) {
        this.posts.put(post.getId(), post);
        return;
    }


    /**
     *  Makes an array of posts available in this session.
     *
     *  @param  posts an array of posts
     *  @throws org.osid.NullArgumentException <code>posts<code>
     *          is <code>null</code>
     */

    protected void putPosts(org.osid.financials.posting.Post[] posts) {
        putPosts(java.util.Arrays.asList(posts));
        return;
    }


    /**
     *  Makes a collection of posts available in this session.
     *
     *  @param  posts a collection of posts
     *  @throws org.osid.NullArgumentException <code>posts<code>
     *          is <code>null</code>
     */

    protected void putPosts(java.util.Collection<? extends org.osid.financials.posting.Post> posts) {
        for (org.osid.financials.posting.Post post : posts) {
            this.posts.put(post.getId(), post);
        }

        return;
    }


    /**
     *  Removes a Post from this session.
     *
     *  @param  postId the <code>Id</code> of the post
     *  @throws org.osid.NullArgumentException <code>postId<code> is
     *          <code>null</code>
     */

    protected void removePost(org.osid.id.Id postId) {
        this.posts.remove(postId);
        return;
    }


    /**
     *  Gets the <code>Post</code> specified by its <code>Id</code>.
     *
     *  @param  postId <code>Id</code> of the <code>Post</code>
     *  @return the post
     *  @throws org.osid.NotFoundException <code>postId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>postId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.Post getPost(org.osid.id.Id postId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.financials.posting.Post post = this.posts.get(postId);
        if (post == null) {
            throw new org.osid.NotFoundException("post not found: " + postId);
        }

        return (post);
    }


    /**
     *  Gets all <code>Posts</code>. In plenary mode, the returned
     *  list contains all known posts or an error
     *  results. Otherwise, the returned list may contain only those
     *  posts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Posts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostList getPosts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.posting.post.ArrayPostList(this.posts.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.posts.clear();
        super.close();
        return;
    }
}
