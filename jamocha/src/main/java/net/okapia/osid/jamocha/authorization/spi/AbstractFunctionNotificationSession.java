//
// AbstractFunctionNotificationSession.java
//
//     A template for making FunctionNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Function} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Function} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for function entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractFunctionNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.authorization.FunctionNotificationSession {

    private boolean federated = false;
    private org.osid.authorization.Vault vault = new net.okapia.osid.jamocha.nil.authorization.vault.UnknownVault();


    /**
     *  Gets the {@code Vault/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Vault Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.vault.getId());
    }

    
    /**
     *  Gets the {@code Vault} associated with this 
     *  session.
     *
     *  @return the {@code Vault} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.vault);
    }


    /**
     *  Sets the {@code Vault}.
     *
     *  @param  vault the vault for this session
     *  @throws org.osid.NullArgumentException {@code vault}
     *          is {@code null}
     */

    protected void setVault(org.osid.authorization.Vault vault) {
        nullarg(vault, "vault");
        this.vault = vault;
        return;
    }


    /**
     *  Tests if this user can register for {@code Function}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForFunctionNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeFunctionNotification() </code>.
     */

    @OSID @Override
    public void reliableFunctionNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableFunctionNotifications() {
        return;
    }


    /**
     *  Acknowledge a function notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeFunctionNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }



    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for functions in vaults
     *  which are children of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new functions. {@code
     *  FunctionReceiver.newFunction()} is invoked when a new {@code
     *  Function} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewFunctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated functions. {@code
     *  FunctionReceiver.changedFunction()} is invoked when a function
     *  is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedFunctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated function. {@code
     *  FunctionReceiver.changedFunction()} is invoked when the
     *  specified function is changed.
     *
     *  @param functionId the {@code Id} of the {@code Function} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code functionId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedFunction(org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted functions. {@code
     *  FunctionReceiver.deletedFunction()} is invoked when a function
     *  is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedFunctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted function. {@code
     *  FunctionReceiver.deletedFunction()} is invoked when the
     *  specified function is deleted.
     *
     *  @param functionId the {@code Id} of the
     *          {@code Function} to monitor
     *  @throws org.osid.NullArgumentException {@code functionId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedFunction(org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
