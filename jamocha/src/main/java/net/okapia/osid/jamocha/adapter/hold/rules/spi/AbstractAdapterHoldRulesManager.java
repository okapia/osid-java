//
// AbstractHoldRulesManager.java
//
//     An adapter for a HoldRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a HoldRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterHoldRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.hold.rules.HoldRulesManager>
    implements org.osid.hold.rules.HoldRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterHoldRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterHoldRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterHoldRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterHoldRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up hold enablers is supported. 
     *
     *  @return <code> true </code> if hold enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerLookup() {
        return (getAdapteeManager().supportsHoldEnablerLookup());
    }


    /**
     *  Tests if querying hold enablers is supported. 
     *
     *  @return <code> true </code> if hold enabler query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerQuery() {
        return (getAdapteeManager().supportsHoldEnablerQuery());
    }


    /**
     *  Tests if searching hold enablers is supported. 
     *
     *  @return <code> true </code> if hold enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerSearch() {
        return (getAdapteeManager().supportsHoldEnablerSearch());
    }


    /**
     *  Tests if a hold enabler administrative service is supported. 
     *
     *  @return <code> true </code> if hold enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerAdmin() {
        return (getAdapteeManager().supportsHoldEnablerAdmin());
    }


    /**
     *  Tests if a hold enabler notification service is supported. 
     *
     *  @return <code> true </code> if hold enabler notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerNotification() {
        return (getAdapteeManager().supportsHoldEnablerNotification());
    }


    /**
     *  Tests if a hold enabler oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if an oubliette enabler hold lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerOubliette() {
        return (getAdapteeManager().supportsHoldEnablerOubliette());
    }


    /**
     *  Tests if a hold enabler oubliette service is supported. 
     *
     *  @return <code> true </code> if hold enabler oubliette assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerOublietteAssignment() {
        return (getAdapteeManager().supportsHoldEnablerOublietteAssignment());
    }


    /**
     *  Tests if a hold enabler hold lookup service is supported. 
     *
     *  @return <code> true </code> if a hold enabler hold service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerSmartHold() {
        return (getAdapteeManager().supportsHoldEnablerSmartHold());
    }


    /**
     *  Tests if a hold enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a hold enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerRuleLookup() {
        return (getAdapteeManager().supportsHoldEnablerRuleLookup());
    }


    /**
     *  Tests if a hold enabler rule application service is supported. 
     *
     *  @return <code> true </code> if hold enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerRuleApplication() {
        return (getAdapteeManager().supportsHoldEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> HoldEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> HoldEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHoldEnablerRecordTypes() {
        return (getAdapteeManager().getHoldEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> HoldEnabler </code> record type is 
     *  supported. 
     *
     *  @param  holdEnablerRecordType a <code> Type </code> indicating a 
     *          <code> HoldEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> holdEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHoldEnablerRecordType(org.osid.type.Type holdEnablerRecordType) {
        return (getAdapteeManager().supportsHoldEnablerRecordType(holdEnablerRecordType));
    }


    /**
     *  Gets the supported <code> HoldEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> HoldEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHoldEnablerSearchRecordTypes() {
        return (getAdapteeManager().getHoldEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> HoldEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  holdEnablerSearchRecordType a <code> Type </code> indicating a 
     *          <code> HoldEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          holdEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHoldEnablerSearchRecordType(org.osid.type.Type holdEnablerSearchRecordType) {
        return (getAdapteeManager().supportsHoldEnablerSearchRecordType(holdEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  lookup service. 
     *
     *  @return a <code> HoldEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerLookupSession getHoldEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  lookup service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerLookupSession getHoldEnablerLookupSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerLookupSessionForOubliette(oublietteId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  query service. 
     *
     *  @return a <code> HoldEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerQuerySession getHoldEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  query service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerQuerySession getHoldEnablerQuerySessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerQuerySessionForOubliette(oublietteId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  search service. 
     *
     *  @return a <code> HoldEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerSearchSession getHoldEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enablers 
     *  earch service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerSearchSession getHoldEnablerSearchSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerSearchSessionForOubliette(oublietteId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  administration service. 
     *
     *  @return a <code> HoldEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerAdminSession getHoldEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerAdminSession getHoldEnablerAdminSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerAdminSessionForOubliette(oublietteId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  notification service. 
     *
     *  @param  holdEnablerReceiver the notification callback 
     *  @return a <code> HoldEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> holdEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerNotificationSession getHoldEnablerNotificationSession(org.osid.hold.rules.HoldEnablerReceiver holdEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerNotificationSession(holdEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  notification service for the given oubliette. 
     *
     *  @param  holdEnablerReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> holdEnablerReceiver 
     *          </code> or <code> oublietteId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerNotificationSession getHoldEnablerNotificationSessionForOubliette(org.osid.hold.rules.HoldEnablerReceiver holdEnablerReceiver, 
                                                                                                            org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerNotificationSessionForOubliette(holdEnablerReceiver, oublietteId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup hold enableroubliette 
     *  mappings. 
     *
     *  @return a <code> HoldEnablerOublietteSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerOublietteSession getHoldEnablerOublietteSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerOublietteSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning hold 
     *  enablers to oubliettes. 
     *
     *  @return a <code> HoldEnablerOublietteAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerOublietteAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerOublietteAssignmentSession getHoldEnablerOublietteAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerOublietteAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage hold enabler smart 
     *  oubliettes. 
     *
     *  @param  oublietteId the Id of the <code> Oubliette </code> 
     *  @return a <code> HoldEnablerSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerSmartOubliette() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerSmartOublietteSession getHoldEnablerSmartOublietteSession(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerSmartOublietteSession(oublietteId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  mapping lookup service for looking up the rules applied to the hold. 
     *
     *  @return a <code> HoldEnablerRuleSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleLookupSession getHoldEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  mapping lookup service for the given hold for looking up rules applied 
     *  to a hold. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleLookupSession getHoldEnablerRuleLookupSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerRuleLookupSessionForOubliette(oublietteId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  assignment service to apply enablers to holds. 
     *
     *  @return a <code> HoldEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleApplicationSession getHoldEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  assignment service for the given hold to apply enablers to holds. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleApplicationSession getHoldEnablerRuleApplicationSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldEnablerRuleApplicationSessionForOubliette(oublietteId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
