//
// AbstractBlogQueryInspector.java
//
//     A template for making a BlogQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.blog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for blogs.
 */

public abstract class AbstractBlogQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.blogging.BlogQueryInspector {

    private final java.util.Collection<org.osid.blogging.records.BlogQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the entry <code> Id </code> terms. 
     *
     *  @return the entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the entry terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.blogging.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.blogging.EntryQueryInspector[0]);
    }


    /**
     *  Gets the ancestor blog <code> Id </code> terms. 
     *
     *  @return the ancestor blog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBlogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor blog terms. 
     *
     *  @return the ancestor blog terms 
     */

    @OSID @Override
    public org.osid.blogging.BlogQueryInspector[] getAncestorBlogTerms() {
        return (new org.osid.blogging.BlogQueryInspector[0]);
    }


    /**
     *  Gets the descendant blog <code> Id </code> terms. 
     *
     *  @return the descendant blog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBlogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant blog terms. 
     *
     *  @return the descendant blog terms 
     */

    @OSID @Override
    public org.osid.blogging.BlogQueryInspector[] getDescendantBlogTerms() {
        return (new org.osid.blogging.BlogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given blog query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a blog implementing the requested record.
     *
     *  @param blogRecordType a blog record type
     *  @return the blog query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>blogRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blogRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.BlogQueryInspectorRecord getBlogQueryInspectorRecord(org.osid.type.Type blogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.BlogQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(blogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blogRecordType + " is not supported");
    }


    /**
     *  Adds a record to this blog query. 
     *
     *  @param blogQueryInspectorRecord blog query inspector
     *         record
     *  @param blogRecordType blog record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBlogQueryInspectorRecord(org.osid.blogging.records.BlogQueryInspectorRecord blogQueryInspectorRecord, 
                                                   org.osid.type.Type blogRecordType) {

        addRecordType(blogRecordType);
        nullarg(blogRecordType, "blog record type");
        this.records.add(blogQueryInspectorRecord);        
        return;
    }
}
