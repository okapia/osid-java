//
// AbstractResourceRelationshipLookupSession.java
//
//    A starter implementation framework for providing a ResourceRelationship
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a
 *  ResourceRelationship lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getResourceRelationships(), this other methods may need to be
 *  overridden for better performance.
 */

public abstract class AbstractResourceRelationshipLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resource.ResourceRelationshipLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.resource.Bin bin = new net.okapia.osid.jamocha.nil.resource.bin.UnknownBin();
    

    /**
     *  Gets the <code>Bin/code> <code>Id</code> associated with this
     *  session.
     *
     *  @return the <code>Bin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.bin.getId());
    }


    /**
     *  Gets the <code>Bin</code> associated with this session.
     *
     *  @return the <code>Bin</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bin);
    }


    /**
     *  Sets the <code>Bin</code>.
     *
     *  @param  bin the bin for this session
     *  @throws org.osid.NullArgumentException <code>bin</code>
     *          is <code>null</code>
     */

    protected void setBin(org.osid.resource.Bin bin) {
        nullarg(bin, "bin");
        this.bin = bin;
        return;
    }


    /**
     *  Tests if this user can perform
     *  <code>ResourceRelationship</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResourceRelationships() {
        return (true);
    }


    /**
     *  A complete view of the <code>ResourceRelationship</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResourceRelationshipView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ResourceRelationship</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResourceRelationshipView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resource relationships in bins which are
     *  children of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only resource relationships whose effective dates are current
     *  are returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveResourceRelationshipView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All resource relationships of any effective dates are returned
     *  by all methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveResourceRelationshipView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>ResourceRelationship</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ResourceRelationship</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>ResourceRelationship</code> and retained for
     *  compatibility.
     *
     *  In effective mode, resource relationships are returned that are currently
     *  effective.  In any effective mode, effective resource relationships and
     *  those currently expired are returned.
     *
     *  @param  resourceRelationshipId <code>Id</code> of the
     *          <code>ResourceRelationship</code>
     *  @return the resource relationship
     *  @throws org.osid.NotFoundException <code>resourceRelationshipId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>resourceRelationshipId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationship getResourceRelationship(org.osid.id.Id resourceRelationshipId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resource.ResourceRelationshipList resourceRelationships = getResourceRelationships()) {
            while (resourceRelationships.hasNext()) {
                org.osid.resource.ResourceRelationship resourceRelationship = resourceRelationships.getNextResourceRelationship();
                if (resourceRelationship.getId().equals(resourceRelationshipId)) {
                    return (resourceRelationship);
                }
            }
        } 

        throw new org.osid.NotFoundException(resourceRelationshipId + " not found");
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  resourceRelationships specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>ResourceRelationships</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getResourceRelationships()</code>.
     *
     *  @param  resourceRelationshipIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ResourceRelationship</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByIds(org.osid.id.IdList resourceRelationshipIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resource.ResourceRelationship> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = resourceRelationshipIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getResourceRelationship(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("resource relationship " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resource.resourcerelationship.LinkedResourceRelationshipList(ret));
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> corresponding to
     *  the given resource relationship genus <code>Type</code> which
     *  does not include resource relationships of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getResourceRelationships()</code>.
     *
     *  @param  resourceRelationshipGenusType a resourceRelationship genus type 
     *  @return the returned <code>ResourceRelationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusType(org.osid.type.Type resourceRelationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipGenusFilterList(getResourceRelationships(), resourceRelationshipGenusType));
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> corresponding to
     *  the given resource relationship genus <code>Type</code> and
     *  include any additional resource relationships with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResourceRelationships()</code>.
     *
     *  @param  resourceRelationshipGenusType a resourceRelationship genus type 
     *  @return the returned <code>ResourceRelationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByParentGenusType(org.osid.type.Type resourceRelationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getResourceRelationshipsByGenusType(resourceRelationshipGenusType));
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> containing the
     *  given resource relationship record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResourceRelationships()</code>.
     *
     *  @param  resourceRelationshipRecordType a resourceRelationship record type 
     *  @return the returned <code>ResourceRelationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByRecordType(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipRecordFilterList(getResourceRelationships(), resourceRelationshipRecordType));
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In active mode, resource relationships are returned that are
     *  currently active. In any status mode, active and inactive
     *  resource relationships are returned.
     *
     *  @param from start of date range
     *  @param  to end of date range 
     *  @return the returned <code>ResourceRelationship</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsOnDate(org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.TemporalResourceRelationshipFilterList(getResourceRelationships(), from, to));
    }
        

    /**
     *  Gets a list of resource relationships corresponding to a
     *  source resource <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param sourceResourceId the <code>Id</code> of the source
     *  resource
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceResourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForSourceResource(org.osid.id.Id sourceResourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipFilterList(new SourceResourceFilter(sourceResourceId), getResourceRelationships()));
    }


    /**
     *  Gets a list of resource relationships corresponding to a
     *  source resource <code>Id</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  sourceResourceId the <code>Id</code> of the source resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceResourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForSourceResourceOnDate(org.osid.id.Id sourceResourceId,
                                                                                                      org.osid.calendaring.DateTime from,
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.TemporalResourceRelationshipFilterList(getResourceRelationshipsForSourceResource(sourceResourceId), from, to));
    }


    /**
     *  Gets the <code>ResourceRelationships</code> of a resource of
     *  relationship genus type that includes any genus type derived
     *  from the given one.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective. In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param sourceResourceId <code>Id</code> of a
     *          <code>Resource</code>
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException
     *          <code>sourceResourceId</code> or
     *          <code>relationshipGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForSourceResource(org.osid.id.Id sourceResourceId, 
                                                                                                           org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipGenusFilterList(getResourceRelationshipsForSourceResource(sourceResourceId), relationshipGenusType));
    }


    /**
     *  Gets a list of resource relationships of a given genus type
     *  for a resource and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective in addition to being effective during
     *  the given dates. In any effective mode, effective resource
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceResourceId a resource <code>Id</code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceResourceId</code>,
     *          <code>relationshipGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForSourceResourceOnDate(org.osid.id.Id sourceResourceId, 
                                                                                                                 org.osid.type.Type relationshipGenusType, 
                                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.TemporalResourceRelationshipFilterList(getResourceRelationshipsByGenusTypeForSourceResource(sourceResourceId, relationshipGenusType), from, to));
    }


    /**
     *  Gets a list of resource relationships corresponding to a
     *  destination resource <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param destinationResourceId the <code>Id</code> of the
     *  destination resource
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>destinationResourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForDestinationResource(org.osid.id.Id destinationResourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipFilterList(new DestinationResourceFilter(destinationResourceId), getResourceRelationships()));
    }


    /**
     *  Gets a list of resource relationships corresponding to a
     *  destination resource <code>Id</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param destinationResourceId the <code>Id</code> of the
     *         destination resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>destinationResourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForDestinationResourceOnDate(org.osid.id.Id destinationResourceId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.TemporalResourceRelationshipFilterList(getResourceRelationshipsForDestinationResource(destinationResourceId), from, to));
    }


    /**
     *  Gets the <code>ResourceRelationships</code> of a resource of
     *  relationship genus type that includes any genus type derived
     *  from the given one.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective. In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param destinationResourceId <code>Id</code> of a
     *         <code>Resource</code>
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException
     *          <code>destinationResourceId</code> or
     *          <code>relationshipGenusType </code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForDestinationResource(org.osid.id.Id destinationResourceId, 
                                                                                                                org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipGenusFilterList(getResourceRelationshipsForDestinationResource(destinationResourceId), relationshipGenusType));
    }


    /**
     *  Gets a list of resource relationships of a given genus type
     *  for a resource and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective in addition to being effective during
     *  the given dates. In any effective mode, effective resource
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationResourceId a resource <code>Id</code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>destinationResourceId</code>,
     *          <code>relationshipGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForDestinationResourceOnDate(org.osid.id.Id destinationResourceId, 
                                                                                                                      org.osid.type.Type relationshipGenusType, 
                                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.TemporalResourceRelationshipFilterList(getResourceRelationshipsByGenusTypeForDestinationResource(destinationResourceId, relationshipGenusType), from, to));
    }


    /**
     *  Gets a list of resource relationships corresponding to source
     *  resource and destination resource <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param sourceResourceId the <code>Id</code> of the source
     *  resource
     *  @param  destinationResourceId the <code>Id</code> of the destination resource
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceResourceId</code>,
     *          <code>destinationResourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForResources(org.osid.id.Id sourceResourceId,
                                                                                           org.osid.id.Id destinationResourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipFilterList(new DestinationResourceFilter(destinationResourceId), getResourceRelationshipsForSourceResource(sourceResourceId)));
    }


    /**
     *  Gets a list of resource relationships corresponding to source
     *  resource and destination resource <code>Ids</code> and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  destinationResourceId the <code>Id</code> of the destination resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceResourceId</code>,
     *          <code>destinationResourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForResourcesOnDate(org.osid.id.Id sourceResourceId,
                                                                                                 org.osid.id.Id destinationResourceId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.TemporalResourceRelationshipFilterList(getResourceRelationshipsForResources(sourceResourceId, destinationResourceId), from, to));
    }


    /**
     *  Gets the <code>ResourceRelationships</code> given two
     *  resources and a relationship genus type which includes any
     *  genus types derived from the given genus type.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective. In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param sourceResourceId <code>Id</code> of a
     *         <code>Resource</code>
     *  @param destinationResourceId <code>Id</code> of another
     *         <code>Resource</code>
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException
     *          <code>sourceResourceId</code>,
     *          <code>destinationResourceId</code>, </code>or<code>
     *          <code>relatonshipGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForResources(org.osid.id.Id sourceResourceId, 
                                                                                                      org.osid.id.Id destinationResourceId, 
                                                                                                      org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipGenusFilterList(getResourceRelationshipsForResources(sourceResourceId, destinationResourceId), relationshipGenusType));
    }


    /**
     *  Gets a list of resource relationships of a given genus type
     *  for a two peer resources and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective in addition to being effective during
     *  the given dates. In any effective mode, effective resource
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceResourceId a resource <code>Id</code> 
     *  @param destinationResourceId <code>Id</code> of another
     *         <code>Resource</code>
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>sourceResourceId</code>,
     *          <code>destinationResourceId</code>,
     *          <code>relationshipGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForResourcesOnDate(org.osid.id.Id sourceResourceId, 
                                                                                                            org.osid.id.Id destinationResourceId, 
                                                                                                            org.osid.type.Type relationshipGenusType, 
                                                                                                            org.osid.calendaring.DateTime from, 
                                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.TemporalResourceRelationshipFilterList(getResourceRelationshipsByGenusTypeForResources(sourceResourceId, destinationResourceId, relationshipGenusType), from, to));
    }


    /**
     *  Gets all <code>ResourceRelationships</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @return a list of <code>ResourceRelationships</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resource.ResourceRelationshipList getResourceRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the resource relationship list for active and
     *  effective views. Should be called by <code>getObjects()</code>
     *  if no filtering is already performed.
     *
     *  @param list the list of resource relationships
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resource.ResourceRelationshipList filterResourceRelationshipsOnViews(org.osid.resource.ResourceRelationshipList list)
        throws org.osid.OperationFailedException {

        org.osid.resource.ResourceRelationshipList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.EffectiveResourceRelationshipFilterList(ret);
        }

        return (ret);
    }


    public static class SourceResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipFilter {
         
        private final org.osid.id.Id sourceResourceId;
         
         
        /**
         *  Constructs a new <code>SourceResourceFilter</code>.
         *
         *  @param sourceResourceId the source resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>sourceResourceId</code> is <code>null</code>
         */
        
        public SourceResourceFilter(org.osid.id.Id sourceResourceId) {
            nullarg(sourceResourceId, "source resource Id");
            this.sourceResourceId = sourceResourceId;
            return;
        }

         
        /**
         *  Used by the ResourceRelationshipFilterList to filter the 
         *  resource relationship list based on source resource.
         *
         *  @param resourceRelationship the resource relationship
         *  @return <code>true</code> to pass the resource relationship,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.resource.ResourceRelationship resourceRelationship) {
            return (resourceRelationship.getSourceResourceId().equals(this.sourceResourceId));
        }
    }


    public static class DestinationResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.resource.resourcerelationship.ResourceRelationshipFilter {
         
        private final org.osid.id.Id destinationResourceId;
         
         
        /**
         *  Constructs a new <code>DestinationResourceFilter</code>.
         *
         *  @param destinationResourceId the destination resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>destinationResourceId</code> is <code>null</code>
         */
        
        public DestinationResourceFilter(org.osid.id.Id destinationResourceId) {
            nullarg(destinationResourceId, "destination resource Id");
            this.destinationResourceId = destinationResourceId;
            return;
        }

         
        /**
         *  Used by the ResourceRelationshipFilterList to filter the 
         *  resource relationship list based on destination resource.
         *
         *  @param resourceRelationship the resource relationship
         *  @return <code>true</code> to pass the resource relationship,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.resource.ResourceRelationship resourceRelationship) {
            return (resourceRelationship.getDestinationResourceId().equals(this.destinationResourceId));
        }
    }
}
