//
// AbstractIndexedMapCredentialEntryLookupSession.java
//
//    A simple framework for providing a CredentialEntry lookup service
//    backed by a fixed collection of credential entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CredentialEntry lookup service backed by a
 *  fixed collection of credential entries. The credential entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some credential entries may be compatible
 *  with more types than are indicated through these credential entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CredentialEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCredentialEntryLookupSession
    extends AbstractMapCredentialEntryLookupSession
    implements org.osid.course.chronicle.CredentialEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.CredentialEntry> credentialEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.CredentialEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.CredentialEntry> credentialEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.CredentialEntry>());


    /**
     *  Makes a <code>CredentialEntry</code> available in this session.
     *
     *  @param  credentialEntry a credential entry
     *  @throws org.osid.NullArgumentException <code>credentialEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCredentialEntry(org.osid.course.chronicle.CredentialEntry credentialEntry) {
        super.putCredentialEntry(credentialEntry);

        this.credentialEntriesByGenus.put(credentialEntry.getGenusType(), credentialEntry);
        
        try (org.osid.type.TypeList types = credentialEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.credentialEntriesByRecord.put(types.getNextType(), credentialEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a credential entry from this session.
     *
     *  @param credentialEntryId the <code>Id</code> of the credential entry
     *  @throws org.osid.NullArgumentException <code>credentialEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCredentialEntry(org.osid.id.Id credentialEntryId) {
        org.osid.course.chronicle.CredentialEntry credentialEntry;
        try {
            credentialEntry = getCredentialEntry(credentialEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.credentialEntriesByGenus.remove(credentialEntry.getGenusType());

        try (org.osid.type.TypeList types = credentialEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.credentialEntriesByRecord.remove(types.getNextType(), credentialEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCredentialEntry(credentialEntryId);
        return;
    }


    /**
     *  Gets a <code>CredentialEntryList</code> corresponding to the given
     *  credential entry genus <code>Type</code> which does not include
     *  credential entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known credential entries or an error results. Otherwise,
     *  the returned list may contain only those credential entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  credentialEntryGenusType a credential entry genus type 
     *  @return the returned <code>CredentialEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByGenusType(org.osid.type.Type credentialEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.credentialentry.ArrayCredentialEntryList(this.credentialEntriesByGenus.get(credentialEntryGenusType)));
    }


    /**
     *  Gets a <code>CredentialEntryList</code> containing the given
     *  credential entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known credential entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  credential entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  credentialEntryRecordType a credential entry record type 
     *  @return the returned <code>credentialEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByRecordType(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.credentialentry.ArrayCredentialEntryList(this.credentialEntriesByRecord.get(credentialEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.credentialEntriesByGenus.clear();
        this.credentialEntriesByRecord.clear();

        super.close();

        return;
    }
}
