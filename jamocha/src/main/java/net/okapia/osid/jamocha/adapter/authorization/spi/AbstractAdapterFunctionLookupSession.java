//
// AbstractAdapterFunctionLookupSession.java
//
//    A Function lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authorization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Function lookup session adapter.
 */

public abstract class AbstractAdapterFunctionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authorization.FunctionLookupSession {

    private final org.osid.authorization.FunctionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterFunctionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterFunctionLookupSession(org.osid.authorization.FunctionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Vault/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Vault Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.session.getVaultId());
    }


    /**
     *  Gets the {@code Vault} associated with this session.
     *
     *  @return the {@code Vault} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getVault());
    }


    /**
     *  Tests if this user can perform {@code Function} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupFunctions() {
        return (this.session.canLookupFunctions());
    }


    /**
     *  A complete view of the {@code Function} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFunctionView() {
        this.session.useComparativeFunctionView();
        return;
    }


    /**
     *  A complete view of the {@code Function} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFunctionView() {
        this.session.usePlenaryFunctionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include functions in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.session.useFederatedVaultView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.session.useIsolatedVaultView();
        return;
    }
    

    /**
     *  Only active functions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveFunctionView() {
        this.session.useActiveFunctionView();
        return;
    }


    /**
     *  Active and inactive functions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusFunctionView() {
        this.session.useAnyStatusFunctionView();
        return;
    }
    
     
    /**
     *  Gets the {@code Function} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Function} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Function} and
     *  retained for compatibility.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param functionId {@code Id} of the {@code Function}
     *  @return the function
     *  @throws org.osid.NotFoundException {@code functionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code functionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Function getFunction(org.osid.id.Id functionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFunction(functionId));
    }


    /**
     *  Gets a {@code FunctionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  functions specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Functions} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *
     *  @param  functionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Function} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code functionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByIds(org.osid.id.IdList functionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFunctionsByIds(functionIds));
    }


    /**
     *  Gets a {@code FunctionList} corresponding to the given
     *  function genus {@code Type} which does not include
     *  functions of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionGenusType a function genus type 
     *  @return the returned {@code Function} list
     *  @throws org.osid.NullArgumentException
     *          {@code functionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByGenusType(org.osid.type.Type functionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFunctionsByGenusType(functionGenusType));
    }


    /**
     *  Gets a {@code FunctionList} corresponding to the given
     *  function genus {@code Type} and include any additional
     *  functions with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionGenusType a function genus type 
     *  @return the returned {@code Function} list
     *  @throws org.osid.NullArgumentException
     *          {@code functionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByParentGenusType(org.osid.type.Type functionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFunctionsByParentGenusType(functionGenusType));
    }


    /**
     *  Gets a {@code FunctionList} containing the given
     *  function record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionRecordType a function record type 
     *  @return the returned {@code Function} list
     *  @throws org.osid.NullArgumentException
     *          {@code functionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByRecordType(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFunctionsByRecordType(functionRecordType));
    }


    /**
     *  Gets all {@code Functions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @return a list of {@code Functions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFunctions());
    }
}
