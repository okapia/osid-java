//
// AbstractCourseChronicleBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCourseChronicleBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.course.chronicle.batch.CourseChronicleBatchManager,
               org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager {


    /**
     *  Constructs a new
     *  <code>AbstractCourseChronicleBatchManager</code>.
     *
     *  @param provider the provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCourseChronicleBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of program entries is available. 
     *
     *  @return <code> true </code> if a program entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of course entries is available. 
     *
     *  @return <code> true </code> if a course entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of credential entries is available. 
     *
     *  @return <code> true </code> if a credential entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of credential entries is available. 
     *
     *  @return <code> true </code> if an assessment entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of award entries is available. 
     *
     *  @return <code> true </code> if an award entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk program 
     *  entry administration service. 
     *
     *  @return a <code> ProgramEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.ProgramEntryBatchAdminSession getProgramEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getProgramEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk program 
     *  entry administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProgramEntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.ProgramEntryBatchAdminSession getProgramEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getProgramEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk program 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.ProgramEntryBatchAdminSession getProgramEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getProgramEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk program 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProgramEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.ProgramEntryBatchAdminSession getProgramEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getProgramEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk course 
     *  entry administration service. 
     *
     *  @return a <code> CourseEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CourseEntryBatchAdminSession getCourseEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getCourseEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk course 
     *  entry administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CourseEntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CourseEntryBatchAdminSession getCourseEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getCourseEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk course 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CourseEntryBatchAdminSession getCourseEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getCourseEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk course 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CourseEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CourseEntryBatchAdminSession getCourseEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getCourseEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  credential entryadministration service. 
     *
     *  @return a <code> CredentialEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CredentialEntryBatchAdminSession getCredentialEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getCredentialEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  credential entry administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CredentialEntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CredentialEntryBatchAdminSession getCredentialEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getCredentialEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  credential entryadministration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CredentialEntryBatchAdminSession getCredentialEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getCredentialEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  credential entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CredentialEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CredentialEntryBatchAdminSession getCredentialEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getCredentialEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment entry administration service. 
     *
     *  @return an <code> AssessmentEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AssessmentEntryBatchAdminSession getAssessmentEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getAssessmentEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment entry administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentEntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AssessmentEntryBatchAdminSession getAssessmentEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getAssessmentEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AssessmentEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AssessmentEntryBatchAdminSession getAssessmentEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getAssessmentEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AssessmentEntryBatchAdminSession getAssessmentEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getAssessmentEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk award 
     *  entry administration service. 
     *
     *  @return an <code> AwardEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AwardEntryBatchAdminSession getAwardEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getAwardEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk award 
     *  entry administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AwardEntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AwardEntryBatchAdminSession getAwardEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getAwardEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk award 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> AwardEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AwardEntryBatchAdminSession getAwardEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchManager.getAwardEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk award 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AwardEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AwardEntryBatchAdminSession getAwardEntryBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager.getAwardEntryBatchAdminSessionForCourseCatalog not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
