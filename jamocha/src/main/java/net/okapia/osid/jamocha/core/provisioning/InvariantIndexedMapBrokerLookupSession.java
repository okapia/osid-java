//
// InvariantIndexedMapBrokerLookupSession
//
//    Implements a Broker lookup service backed by a fixed
//    collection of brokers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Broker lookup service backed by a fixed
 *  collection of brokers. The brokers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some brokers may be compatible
 *  with more types than are indicated through these broker
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapBrokerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractIndexedMapBrokerLookupSession
    implements org.osid.provisioning.BrokerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapBrokerLookupSession} using an
     *  array of brokers.
     *
     *  @param distributor the distributor
     *  @param brokers an array of brokers
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapBrokerLookupSession(org.osid.provisioning.Distributor distributor,
                                                    org.osid.provisioning.Broker[] brokers) {

        setDistributor(distributor);
        putBrokers(brokers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapBrokerLookupSession} using a
     *  collection of brokers.
     *
     *  @param distributor the distributor
     *  @param brokers a collection of brokers
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapBrokerLookupSession(org.osid.provisioning.Distributor distributor,
                                                    java.util.Collection<? extends org.osid.provisioning.Broker> brokers) {

        setDistributor(distributor);
        putBrokers(brokers);
        return;
    }
}
