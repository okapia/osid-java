//
// AbstractAuctionHouseSearch.java
//
//     A template for making an AuctionHouse Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auctionhouse.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing auction house searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAuctionHouseSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.bidding.AuctionHouseSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.bidding.records.AuctionHouseSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.bidding.AuctionHouseSearchOrder auctionHouseSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of auction houses. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  auctionHouseIds list of auction houses
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAuctionHouses(org.osid.id.IdList auctionHouseIds) {
        while (auctionHouseIds.hasNext()) {
            try {
                this.ids.add(auctionHouseIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAuctionHouses</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of auction house Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAuctionHouseIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  auctionHouseSearchOrder auction house search order 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>auctionHouseSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAuctionHouseResults(org.osid.bidding.AuctionHouseSearchOrder auctionHouseSearchOrder) {
	this.auctionHouseSearchOrder = auctionHouseSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.bidding.AuctionHouseSearchOrder getAuctionHouseSearchOrder() {
	return (this.auctionHouseSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given auction house search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction house implementing the requested record.
     *
     *  @param auctionHouseSearchRecordType an auction house search record
     *         type
     *  @return the auction house search record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionHouseSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionHouseSearchRecord getAuctionHouseSearchRecord(org.osid.type.Type auctionHouseSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.bidding.records.AuctionHouseSearchRecord record : this.records) {
            if (record.implementsRecordType(auctionHouseSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionHouseSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction house search. 
     *
     *  @param auctionHouseSearchRecord auction house search record
     *  @param auctionHouseSearchRecordType auctionHouse search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionHouseSearchRecord(org.osid.bidding.records.AuctionHouseSearchRecord auctionHouseSearchRecord, 
                                           org.osid.type.Type auctionHouseSearchRecordType) {

        addRecordType(auctionHouseSearchRecordType);
        this.records.add(auctionHouseSearchRecord);        
        return;
    }
}
