//
// AbstractTodo.java
//
//     Defines a Todo builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.checklist.todo.spi;


/**
 *  Defines a <code>Todo</code> builder.
 */

public abstract class AbstractTodoBuilder<T extends AbstractTodoBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.checklist.todo.TodoMiter todo;


    /**
     *  Constructs a new <code>AbstractTodoBuilder</code>.
     *
     *  @param todo the todo to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractTodoBuilder(net.okapia.osid.jamocha.builder.checklist.todo.TodoMiter todo) {
        super(todo);
        this.todo = todo;
        return;
    }


    /**
     *  Builds the todo.
     *
     *  @return the new todo
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.checklist.Todo build() {
        (new net.okapia.osid.jamocha.builder.validator.checklist.todo.TodoValidator(getValidations())).validate(this.todo);
        return (new net.okapia.osid.jamocha.builder.checklist.todo.ImmutableTodo(this.todo));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the todo miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.checklist.todo.TodoMiter getMiter() {
        return (this.todo);
    }


    /**
     *  Sets the sequestered flag.
     */

    public T sequestered() {
        getMiter().setSequestered(true);
        return (self());
    }


    /**
     *  Sets the unsequestered flag.
     */

    public T unsequestered() {
        getMiter().setSequestered(false);
        return (self());
    }


    /**
     *  Sets the complete flag.
     *
     *  @return the builder
     */

    public T complete() {
        getMiter().setComplete(true);
        return (self());
    }


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>priority</code>
     *          is <code>null</code>
     */

    public T priority(org.osid.type.Type priority) {
        getMiter().setPriority(priority);
        return (self());
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T dueDate(org.osid.calendaring.DateTime date) {
        getMiter().setDueDate(date);
        return (self());
    }


    /**
     *  Adds a dependency.
     *
     *  @param dependency a dependency
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>dependency</code>
     *          is <code>null</code>
     */

    public T dependency(org.osid.checklist.Todo dependency) {
        getMiter().addDependency(dependency);
        return (self());
    }


    /**
     *  Sets all the dependencies.
     *
     *  @param dependencies a collection of dependencies
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>dependencies</code> is <code>null</code>
     */

    public T dependencies(java.util.Collection<org.osid.checklist.Todo> dependencies) {
        getMiter().setDependencies(dependencies);
        return (self());
    }


    /**
     *  Adds a Todo record.
     *
     *  @param record a todo record
     *  @param recordType the type of todo record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.checklist.records.TodoRecord record, org.osid.type.Type recordType) {
        getMiter().addTodoRecord(record, recordType);
        return (self());
    }
}       


