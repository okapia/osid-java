//
// AbstractGraphSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.graph.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractGraphSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.topology.GraphSearchResults {

    private org.osid.topology.GraphList graphs;
    private final org.osid.topology.GraphQueryInspector inspector;
    private final java.util.Collection<org.osid.topology.records.GraphSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractGraphSearchResults.
     *
     *  @param graphs the result set
     *  @param graphQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>graphs</code>
     *          or <code>graphQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractGraphSearchResults(org.osid.topology.GraphList graphs,
                                            org.osid.topology.GraphQueryInspector graphQueryInspector) {
        nullarg(graphs, "graphs");
        nullarg(graphQueryInspector, "graph query inspectpr");

        this.graphs = graphs;
        this.inspector = graphQueryInspector;

        return;
    }


    /**
     *  Gets the graph list resulting from a search.
     *
     *  @return a graph list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphs() {
        if (this.graphs == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.topology.GraphList graphs = this.graphs;
        this.graphs = null;
	return (graphs);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.topology.GraphQueryInspector getGraphQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  graph search record <code> Type. </code> This method must
     *  be used to retrieve a graph implementing the requested
     *  record.
     *
     *  @param graphSearchRecordType a graph search 
     *         record type 
     *  @return the graph search
     *  @throws org.osid.NullArgumentException
     *          <code>graphSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(graphSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.GraphSearchResultsRecord getGraphSearchResultsRecord(org.osid.type.Type graphSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.topology.records.GraphSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(graphSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(graphSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record graph search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addGraphRecord(org.osid.topology.records.GraphSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "graph record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
