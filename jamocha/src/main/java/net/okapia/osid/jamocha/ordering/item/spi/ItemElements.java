//
// ItemElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.item.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ItemElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ItemElement Id.
     *
     *  @return the item element Id
     */

    public static org.osid.id.Id getItemEntityId() {
        return (makeEntityId("osid.ordering.Item"));
    }


    /**
     *  Gets the OrderId element Id.
     *
     *  @return the OrderId element Id
     */

    public static org.osid.id.Id getOrderId() {
        return (makeElementId("osid.ordering.item.OrderId"));
    }


    /**
     *  Gets the Order element Id.
     *
     *  @return the Order element Id
     */

    public static org.osid.id.Id getOrder() {
        return (makeElementId("osid.ordering.item.Order"));
    }


    /**
     *  Gets the ProductId element Id.
     *
     *  @return the ProductId element Id
     */

    public static org.osid.id.Id getProductId() {
        return (makeElementId("osid.ordering.item.ProductId"));
    }


    /**
     *  Gets the Product element Id.
     *
     *  @return the Product element Id
     */

    public static org.osid.id.Id getProduct() {
        return (makeElementId("osid.ordering.item.Product"));
    }


    /**
     *  Gets the UnitPriceIds element Id.
     *
     *  @return the UnitPriceIds element Id
     */

    public static org.osid.id.Id getUnitPriceIds() {
        return (makeElementId("osid.ordering.item.UnitPriceIds"));
    }


    /**
     *  Gets the UnitPrices element Id.
     *
     *  @return the UnitPrices element Id
     */

    public static org.osid.id.Id getUnitPrices() {
        return (makeElementId("osid.ordering.item.UnitPrices"));
    }


    /**
     *  Gets the Quantity element Id.
     *
     *  @return the Quantity element Id
     */

    public static org.osid.id.Id getQuantity() {
        return (makeElementId("osid.ordering.item.Quantity"));
    }


    /**
     *  Gets the Costs element Id.
     *
     *  @return the Costs element Id
     */

    public static org.osid.id.Id getCosts() {
        return (makeElementId("osid.ordering.item.Costs"));
    }


    /**
     *  Gets the Derived element Id.
     *
     *  @return the Derived element Id
     */

    public static org.osid.id.Id getDerived() {
        return (makeElementId("osid.ordering.item.Derived"));
    }


    /**
     *  Gets the MinimumCost element Id.
     *
     *  @return the MinimumCost element Id
     */

    public static org.osid.id.Id getMinimumCost() {
        return (makeQueryElementId("osid.ordering.item.MinimumCost"));
    }


    /**
     *  Gets the StoreId element Id.
     *
     *  @return the StoreId element Id
     */

    public static org.osid.id.Id getStoreId() {
        return (makeQueryElementId("osid.ordering.item.StoreId"));
    }


    /**
     *  Gets the Store element Id.
     *
     *  @return the Store element Id
     */

    public static org.osid.id.Id getStore() {
        return (makeQueryElementId("osid.ordering.item.Store"));
    }
}
