//
// AbstractIndexedMapAuctionLookupSession.java
//
//    A simple framework for providing an Auction lookup service
//    backed by a fixed collection of auctions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Auction lookup service backed by a
 *  fixed collection of auctions. The auctions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some auctions may be compatible
 *  with more types than are indicated through these auction
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Auctions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuctionLookupSession
    extends AbstractMapAuctionLookupSession
    implements org.osid.bidding.AuctionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.bidding.Auction> auctionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.Auction>());
    private final MultiMap<org.osid.type.Type, org.osid.bidding.Auction> auctionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.Auction>());


    /**
     *  Makes an <code>Auction</code> available in this session.
     *
     *  @param  auction an auction
     *  @throws org.osid.NullArgumentException <code>auction<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAuction(org.osid.bidding.Auction auction) {
        super.putAuction(auction);

        this.auctionsByGenus.put(auction.getGenusType(), auction);
        
        try (org.osid.type.TypeList types = auction.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionsByRecord.put(types.getNextType(), auction);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an auction from this session.
     *
     *  @param auctionId the <code>Id</code> of the auction
     *  @throws org.osid.NullArgumentException <code>auctionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAuction(org.osid.id.Id auctionId) {
        org.osid.bidding.Auction auction;
        try {
            auction = getAuction(auctionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.auctionsByGenus.remove(auction.getGenusType());

        try (org.osid.type.TypeList types = auction.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionsByRecord.remove(types.getNextType(), auction);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAuction(auctionId);
        return;
    }


    /**
     *  Gets an <code>AuctionList</code> corresponding to the given
     *  auction genus <code>Type</code> which does not include
     *  auctions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known auctions or an error results. Otherwise,
     *  the returned list may contain only those auctions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  auctionGenusType an auction genus type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByGenusType(org.osid.type.Type auctionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.auction.ArrayAuctionList(this.auctionsByGenus.get(auctionGenusType)));
    }


    /**
     *  Gets an <code>AuctionList</code> containing the given
     *  auction record <code>Type</code>. In plenary mode, the
     *  returned list contains all known auctions or an error
     *  results. Otherwise, the returned list may contain only those
     *  auctions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  auctionRecordType an auction record type 
     *  @return the returned <code>auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByRecordType(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.auction.ArrayAuctionList(this.auctionsByRecord.get(auctionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionsByGenus.clear();
        this.auctionsByRecord.clear();

        super.close();

        return;
    }
}
