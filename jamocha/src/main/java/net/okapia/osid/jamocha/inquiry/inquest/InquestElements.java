//
// InquestElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquest;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class InquestElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the InquestElement Id.
     *
     *  @return the inquest element Id
     */

    public static org.osid.id.Id getInquestEntityId() {
        return (makeEntityId("osid.inquiry.Inquest"));
    }


    /**
     *  Gets the InquiryId element Id.
     *
     *  @return the InquiryId element Id
     */

    public static org.osid.id.Id getInquiryId() {
        return (makeQueryElementId("osid.inquiry.inquest.InquiryId"));
    }


    /**
     *  Gets the Inquiry element Id.
     *
     *  @return the Inquiry element Id
     */

    public static org.osid.id.Id getInquiry() {
        return (makeQueryElementId("osid.inquiry.inquest.Inquiry"));
    }


    /**
     *  Gets the AuditId element Id.
     *
     *  @return the AuditId element Id
     */

    public static org.osid.id.Id getAuditId() {
        return (makeQueryElementId("osid.inquiry.inquest.AuditId"));
    }


    /**
     *  Gets the Audit element Id.
     *
     *  @return the Audit element Id
     */

    public static org.osid.id.Id getAudit() {
        return (makeQueryElementId("osid.inquiry.inquest.Audit"));
    }


    /**
     *  Gets the ResponseId element Id.
     *
     *  @return the ResponseId element Id
     */

    public static org.osid.id.Id getResponseId() {
        return (makeQueryElementId("osid.inquiry.inquest.ResponseId"));
    }


    /**
     *  Gets the Response element Id.
     *
     *  @return the Response element Id
     */

    public static org.osid.id.Id getResponse() {
        return (makeQueryElementId("osid.inquiry.inquest.Response"));
    }


    /**
     *  Gets the AncestorInquestId element Id.
     *
     *  @return the AncestorInquestId element Id
     */

    public static org.osid.id.Id getAncestorInquestId() {
        return (makeQueryElementId("osid.inquiry.inquest.AncestorInquestId"));
    }


    /**
     *  Gets the AncestorInquest element Id.
     *
     *  @return the AncestorInquest element Id
     */

    public static org.osid.id.Id getAncestorInquest() {
        return (makeQueryElementId("osid.inquiry.inquest.AncestorInquest"));
    }


    /**
     *  Gets the DescendantInquestId element Id.
     *
     *  @return the DescendantInquestId element Id
     */

    public static org.osid.id.Id getDescendantInquestId() {
        return (makeQueryElementId("osid.inquiry.inquest.DescendantInquestId"));
    }


    /**
     *  Gets the DescendantInquest element Id.
     *
     *  @return the DescendantInquest element Id
     */

    public static org.osid.id.Id getDescendantInquest() {
        return (makeQueryElementId("osid.inquiry.inquest.DescendantInquest"));
    }
}
