//
// AbstractParameterQuery.java
//
//     A template for making a Parameter Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.parameter.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for parameters.
 */

public abstract class AbstractParameterQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.configuration.ParameterQuery {

    private final java.util.Collection<org.osid.configuration.records.ParameterQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Adds a match for parameters of a given value syntax. Multiple matches 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  syntax the parameter value syntax 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> syntax </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchValueSyntax(org.osid.Syntax syntax, boolean match) {
        return;
    }


    /**
     *  Clears the value syntax terms. 
     */

    @OSID @Override
    public void clearValueSyntaxTerms() {
        return;
    }


    /**
     *  Adds a match for parameters with a given coordinate type for a 
     *  coordinate value. Multiple matches can be added to perform a boolean 
     *  <code> OR </code> among them. 
     *
     *  @param  coordinateType the coordinate type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchValueCoordinateType(org.osid.type.Type coordinateType, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the coordinate type terms. 
     */

    @OSID @Override
    public void clearValueCoordinateTypeTerms() {
        return;
    }


    /**
     *  Adds a match for parameters with a given heading type for a coordinate 
     *  value. Multiple matches can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  headingType the heading type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> headingType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchValueHeadingType(org.osid.type.Type headingType, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the coorheadingdinate record type terms. 
     */

    @OSID @Override
    public void clearValueHeadingTypeTerms() {
        return;
    }


    /**
     *  Adds a match for parameters with a given object type for an object 
     *  value. Multiple matches can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  objectType the object type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchValueObjectType(org.osid.type.Type objectType, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the object value type terms. 
     */

    @OSID @Override
    public void clearValueObjectTypeTerms() {
        return;
    }


    /**
     *  Adds a match for parameters with a given spatial unit record type for 
     *  a coordinate value. Multiple matches can be added to perform a boolean 
     *  <code> OR </code> among them. 
     *
     *  @param  spatialUnitRecordType the spatial unit record type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchValueSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType, 
                                                boolean match) {
        return;
    }


    /**
     *  Clears the spatial unit record type terms. 
     */

    @OSID @Override
    public void clearValueSpatialUnitRecordTypeTerms() {
        return;
    }


    /**
     *  Adds a match for parameters with a given version type for a version 
     *  value. Multiple matches can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  versionType the version type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> versionType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchValueVersionScheme(org.osid.type.Type versionType, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the value type terms. 
     */

    @OSID @Override
    public void clearValueVersionSchemeTerms() {
        return;
    }


    /**
     *  Tests if a <code> ValueQuery </code> is available. 
     *
     *  @return <code> true </code> if a value query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a value. 
     *
     *  @return the value query 
     *  @throws org.osid.UnimplementedException <code> supportsValueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuery getValueQuery() {
        throw new org.osid.UnimplementedException("supportsValueQuery() is false");
    }


    /**
     *  Matches parameters that have any value. 
     *
     *  @param  match <code> true </code> to match parameters with any value, 
     *          <code> false </code> to match parameters with no value 
     */

    @OSID @Override
    public void matchAnyValue(boolean match) {
        return;
    }


    /**
     *  Clears the value terms. 
     */

    @OSID @Override
    public void clearValueTerms() {
        return;
    }


    /**
     *  Matches shuffle order. 
     *
     *  @param  shuffle <code> true </code> to match shuffle by weight, false 
     *          to match order by index 
     */

    @OSID @Override
    public void matchValuesShuffled(boolean shuffle) {
        return;
    }


    /**
     *  Matches parameters that have any shuffle value. 
     *
     *  @param  match <code> true </code> to match parameters with any shuffle 
     *          value, <code> false </code> to match parameters with no 
     *          shuffle value 
     */

    @OSID @Override
    public void matchAnyValuesShuffled(boolean match) {
        return;
    }


    /**
     *  Clears the shuffle terms. 
     */

    @OSID @Override
    public void clearValuesShuffledTerms() {
        return;
    }


    /**
     *  Sets the configuration <code> Id </code> for this query. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        return;
    }


    /**
     *  Gets the record corresponding to the given parameter query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a parameter implementing the requested record.
     *
     *  @param parameterRecordType a parameter record type
     *  @return the parameter query record
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ParameterQueryRecord getParameterQueryRecord(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ParameterQueryRecord record : this.records) {
            if (record.implementsRecordType(parameterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter query. 
     *
     *  @param parameterQueryRecord parameter query record
     *  @param parameterRecordType parameter record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParameterQueryRecord(org.osid.configuration.records.ParameterQueryRecord parameterQueryRecord, 
                                          org.osid.type.Type parameterRecordType) {

        addRecordType(parameterRecordType);
        nullarg(parameterQueryRecord, "parameter query record");
        this.records.add(parameterQueryRecord);        
        return;
    }
}
