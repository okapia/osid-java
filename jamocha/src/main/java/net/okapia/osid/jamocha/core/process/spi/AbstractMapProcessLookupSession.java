//
// AbstractMapProcessLookupSession
//
//    A simple framework for providing a Process lookup service
//    backed by a fixed collection of processes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Process lookup service backed by a
 *  fixed collection of processes. The processes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Processes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProcessLookupSession
    extends net.okapia.osid.jamocha.process.spi.AbstractProcessLookupSession
    implements org.osid.process.ProcessLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.process.Process> processes = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.process.Process>());


    /**
     *  Makes a <code>Process</code> available in this session.
     *
     *  @param  process a process
     *  @throws org.osid.NullArgumentException <code>process<code>
     *          is <code>null</code>
     */

    protected void putProcess(org.osid.process.Process process) {
        this.processes.put(process.getId(), process);
        return;
    }


    /**
     *  Makes an array of processes available in this session.
     *
     *  @param  processes an array of processes
     *  @throws org.osid.NullArgumentException <code>processes<code>
     *          is <code>null</code>
     */

    protected void putProcesses(org.osid.process.Process[] processes) {
        putProcesses(java.util.Arrays.asList(processes));
        return;
    }


    /**
     *  Makes a collection of processes available in this session.
     *
     *  @param  processes a collection of processes
     *  @throws org.osid.NullArgumentException <code>processes<code>
     *          is <code>null</code>
     */

    protected void putProcesses(java.util.Collection<? extends org.osid.process.Process> processes) {
        for (org.osid.process.Process process : processes) {
            this.processes.put(process.getId(), process);
        }

        return;
    }


    /**
     *  Removes a Process from this session.
     *
     *  @param  processId the <code>Id</code> of the process
     *  @throws org.osid.NullArgumentException <code>processId<code> is
     *          <code>null</code>
     */

    protected void removeProcess(org.osid.id.Id processId) {
        this.processes.remove(processId);
        return;
    }


    /**
     *  Gets the <code>Process</code> specified by its <code>Id</code>.
     *
     *  @param  processId <code>Id</code> of the <code>Process</code>
     *  @return the process
     *  @throws org.osid.NotFoundException <code>processId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>processId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.Process getProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.process.Process process = this.processes.get(processId);
        if (process == null) {
            throw new org.osid.NotFoundException("process not found: " + processId);
        }

        return (process);
    }


    /**
     *  Gets all <code>Processes</code>. In plenary mode, the returned
     *  list contains all known processes or an error
     *  results. Otherwise, the returned list may contain only those
     *  processes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Processes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcesses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.process.process.ArrayProcessList(this.processes.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.processes.clear();
        super.close();
        return;
    }
}
