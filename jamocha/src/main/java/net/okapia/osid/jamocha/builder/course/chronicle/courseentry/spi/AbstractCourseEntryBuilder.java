//
// AbstractCourseEntry.java
//
//     Defines a CourseEntry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.courseentry.spi;


/**
 *  Defines a <code>CourseEntry</code> builder.
 */

public abstract class AbstractCourseEntryBuilder<T extends AbstractCourseEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.chronicle.courseentry.CourseEntryMiter courseEntry;


    /**
     *  Constructs a new <code>AbstractCourseEntryBuilder</code>.
     *
     *  @param courseEntry the course entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCourseEntryBuilder(net.okapia.osid.jamocha.builder.course.chronicle.courseentry.CourseEntryMiter courseEntry) {
        super(courseEntry);
        this.courseEntry = courseEntry;
        return;
    }


    /**
     *  Builds the course entry.
     *
     *  @return the new course entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.chronicle.CourseEntry build() {
        (new net.okapia.osid.jamocha.builder.validator.course.chronicle.courseentry.CourseEntryValidator(getValidations())).validate(this.courseEntry);
        return (new net.okapia.osid.jamocha.builder.course.chronicle.courseentry.ImmutableCourseEntry(this.courseEntry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the course entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.chronicle.courseentry.CourseEntryMiter getMiter() {
        return (this.courseEntry);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public T student(org.osid.resource.Resource student) {
        getMiter().setStudent(student);
        return (self());
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public T course(org.osid.course.Course course) {
        getMiter().setCourse(course);
        return (self());
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public T term(org.osid.course.Term term) {
        getMiter().setTerm(term);
        return (self());
    }


    /**
     *  Marks the course as completed.
     *
     *  @return the builder
     */

    public T complete() {
        getMiter().setComplete(true);
        return (self());
    }


    /**
     *  Marks the course as inomplete.
     *
     *  @return the builder
     */

    public T incomplete() {
        getMiter().setComplete(false);
        return (self());
    }


    /**
     *  Sets the credit scale.
     *
     *  @param scale a credit scale
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    public T creditScale(org.osid.grading.GradeSystem scale) {
        getMiter().setCreditScale(scale);
        return (self());
    }


    /**
     *  Sets the credits earned.
     *
     *  @param credits a credits earned
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public T creditsEarned(java.math.BigDecimal credits) {
        getMiter().setCreditsEarned(credits);
        return (self());
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public T grade(org.osid.grading.Grade grade) {
        getMiter().setGrade(grade);
        return (self());
    }


    /**
     *  Sets the score scale.
     *
     *  @param scale a score scale
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    public T scoreScale(org.osid.grading.GradeSystem scale) {
        getMiter().setScoreScale(scale);
        return (self());
    }


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public T score(java.math.BigDecimal score) {
        getMiter().setScore(score);
        return (self());
    }


    /**
     *  Adds a registration.
     *
     *  @param registration a registration
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>registration</code> is <code>null</code>
     */

    public T registration(org.osid.course.registration.Registration registration) {
        getMiter().addRegistration(registration);
        return (self());
    }


    /**
     *  Sets all the registrations.
     *
     *  @param registrations a collection of registrations
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>registrations</code> is <code>null</code>
     */

    public T registrations(java.util.Collection<org.osid.course.registration.Registration> registrations) {
        getMiter().setRegistrations(registrations);
        return (self());
    }


    /**
     *  Adds a CourseEntry record.
     *
     *  @param record a course entry record
     *  @param recordType the type of course entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.chronicle.records.CourseEntryRecord record, org.osid.type.Type recordType) {
        getMiter().addCourseEntryRecord(record, recordType);
        return (self());
    }
}       


