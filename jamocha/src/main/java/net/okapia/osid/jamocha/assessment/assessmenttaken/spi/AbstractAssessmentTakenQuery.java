//
// AbstractAssessmentTakenQuery.java
//
//     A template for making an AssessmentTaken Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmenttaken.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for assessments taken.
 */

public abstract class AbstractAssessmentTakenQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.assessment.AssessmentTakenQuery {

    private final java.util.Collection<org.osid.assessment.records.AssessmentTakenQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the assessment offered <code> Id </code> for this query. 
     *
     *  @param  assessmentOfferedId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentOfferedId(org.osid.id.Id assessmentOfferedId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears all assessment offered <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentOfferedQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment offered query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment offered query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuery getAssessmentOfferedQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentOfferedQuery() is false");
    }


    /**
     *  Clears all assessment offered terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTakerId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears all resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTakerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTakerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsTakerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getTakerQuery() {
        throw new org.osid.UnimplementedException("supportsTakerQuery() is false");
    }


    /**
     *  Clears all resource terms. 
     */

    @OSID @Override
    public void clearTakerTerms() {
        return;
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTakingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears all agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTakingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTakingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTakingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getTakingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsTakingAgentQuery() is false");
    }


    /**
     *  Clears all taking agent terms. 
     */

    @OSID @Override
    public void clearTakingAgentTerms() {
        return;
    }


    /**
     *  Matches assessments whose start time falls between the specified range 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchActualStartTime(org.osid.calendaring.DateTime start, 
                                     org.osid.calendaring.DateTime end, 
                                     boolean match) {
        return;
    }


    /**
     *  Matches taken assessments that have begun.
     *
     *  @param match <code> true </code> to match assessments taken
     *         started, <code> false </code> to match assessments
     *         taken that have not begun
     */

    @OSID @Override
    public void matchAnyActualStartTime(boolean match) {
        return;
    }


    /**
     *  Clears all start time terms. 
     */

    @OSID @Override
    public void clearActualStartTimeTerms() {
        return;
    }


    /**
     *  Matches assessments whose completion time falls between the specified 
     *  range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCompletionTime(org.osid.calendaring.DateTime start, 
                                    org.osid.calendaring.DateTime end, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches completed taken assessments.
     *
     *  @param match <code> true </code> to match assessments with any
     *          completion time, <code> false </code> to match
     *          assessments incomplete
     */

    @OSID @Override
    public void matchAnyCompletionTime(boolean match) {
        return;
    }


    /**
     *  Clears all in completion time terms. 
     */

    @OSID @Override
    public void clearCompletionTimeTerms() {
        return;
    }


    /**
     *  Matches assessments where the time spent falls between the specified 
     *  range inclusive. 
     *
     *  @param  low start of duration range 
     *  @param  high end of duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeSpent(org.osid.calendaring.Duration low, 
                               org.osid.calendaring.Duration high, 
                               boolean match) {
        return;
    }


    /**
     *  Clears all in time spent terms. 
     */

    @OSID @Override
    public void clearTimeSpentTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true for a positive match, false for a negative 
     *          match </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScoreSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        return;
    }


    /**
     *  Clears all grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScoreSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getScoreSystemQuery() {
        throw new org.osid.UnimplementedException("supportsScoreSystemQuery() is false");
    }


    /**
     *  Matches taken assessments that have any grade system assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any grade 
     *          system, <code> false </code> to match assessments with no 
     *          grade system 
     */

    @OSID @Override
    public void matchAnyScoreSystem(boolean match) {
        return;
    }


    /**
     *  Clears all grade system terms. 
     */

    @OSID @Override
    public void clearScoreSystemTerms() {
        return;
    }


    /**
     *  Matches assessments whose score falls between the specified range 
     *  inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchScore(java.math.BigDecimal low, java.math.BigDecimal high, 
                           boolean match) {
        return;
    }


    /**
     *  Matches taken assessments that have any score assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any score, 
     *          <code> false </code> to match assessments with no score 
     */

    @OSID @Override
    public void matchAnyScore(boolean match) {
        return;
    }


    /**
     *  Clears all score terms. 
     */

    @OSID @Override
    public void clearScoreTerms() {
        return;
    }


    /**
     *  Sets the grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true for a positive match, false for a negative 
     *          match </code> 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears all grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches taken assessments that have any grade assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any grade, 
     *          <code> false </code> to match assessments with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        return;
    }


    /**
     *  Clears all grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        return;
    }


    /**
     *  Sets the comment string for this query. 
     *
     *  @param  comments comment string 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> comments is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> comments </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchFeedback(String comments, 
                              org.osid.type.Type stringMatchType, 
                              boolean match) {
        return;
    }


    /**
     *  Matches taken assessments that have any comments. 
     *
     *  @param  match <code> true </code> to match assessments with any 
     *          comments, <code> false </code> to match assessments with no 
     *          comments 
     */

    @OSID @Override
    public void matchAnyFeedback(boolean match) {
        return;
    }


    /**
     *  Clears all comment terms. 
     */

    @OSID @Override
    public void clearFeedbackTerms() {
        return;
    }


    /**
     *  Sets the rubric assessment taken <code> Id </code> for this query. 
     *
     *  @param  assessmentTakenId an assessment taken <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentTakenId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRubricId(org.osid.id.Id assessmentTakenId, boolean match) {
        return;
    }


    /**
     *  Clears all rubric assessment taken <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRubricIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentTakenQuery </code> is available. 
     *
     *  @return <code> true </code> if a rubric assessment taken query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricQuery() {
        return (false);
    }


    /**
     *  Gets the query for a rubric assessment. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment taken query 
     *  @throws org.osid.UnimplementedException <code> supportsRubricQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQuery getRubricQuery() {
        throw new org.osid.UnimplementedException("supportsRubricQuery() is false");
    }


    /**
     *  Matches an assessment taken that has any rubric assessment assigned. 
     *
     *  @param  match <code> true </code> to match assessments taken with any 
     *          rubric, <code> false </code> to match assessments taken with 
     *          no rubric 
     */

    @OSID @Override
    public void matchAnyRubric(boolean match) {
        return;
    }


    /**
     *  Clears all rubric assessment taken terms. 
     */

    @OSID @Override
    public void clearRubricTerms() {
        return;
    }


    /**
     *  Sets the bank <code> Id </code> for this query. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        return;
    }


    /**
     *  Clears all bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears all bank terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given assessment taken query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an assessment taken implementing the requested record.
     *
     *  @param assessmentTakenRecordType an assessment taken record type
     *  @return the assessment taken query record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentTakenRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenQueryRecord getAssessmentTakenQueryRecord(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentTakenQueryRecord record : this.records) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentTakenRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment taken query. 
     *
     *  @param assessmentTakenQueryRecord assessment taken query record
     *  @param assessmentTakenRecordType assessmentTaken record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentTakenQueryRecord(org.osid.assessment.records.AssessmentTakenQueryRecord assessmentTakenQueryRecord, 
                                          org.osid.type.Type assessmentTakenRecordType) {

        addRecordType(assessmentTakenRecordType);
        nullarg(assessmentTakenQueryRecord, "assessment taken query record");
        this.records.add(assessmentTakenQueryRecord);        
        return;
    }
}
