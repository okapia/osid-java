//
// InvariantMapProxyMapLookupSession
//
//    Implements a Map lookup service backed by a fixed
//    collection of maps. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping;


/**
 *  Implements a Map lookup service backed by a fixed
 *  collection of maps. The maps are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyMapLookupSession
    extends net.okapia.osid.jamocha.core.mapping.spi.AbstractMapMapLookupSession
    implements org.osid.mapping.MapLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyMapLookupSession} with no
     *  maps.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyMapLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyMapLookupSession} with a
     *  single map.
     *
     *  @param map a single map
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyMapLookupSession(org.osid.mapping.Map map, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putMap(map);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyMapLookupSession} using
     *  an array of maps.
     *
     *  @param maps an array of maps
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code maps} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyMapLookupSession(org.osid.mapping.Map[] maps, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putMaps(maps);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyMapLookupSession} using a
     *  collection of maps.
     *
     *  @param maps a collection of maps
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code maps} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyMapLookupSession(java.util.Collection<? extends org.osid.mapping.Map> maps,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putMaps(maps);
        return;
    }
}
