//
// AbstractProfileEntrySearch.java
//
//     A template for making a ProfileEntry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profileentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing profile entry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProfileEntrySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.profile.ProfileEntrySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.profile.records.ProfileEntrySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.profile.ProfileEntrySearchOrder profileEntrySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of profile entries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  profileEntryIds list of profile entries
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProfileEntries(org.osid.id.IdList profileEntryIds) {
        while (profileEntryIds.hasNext()) {
            try {
                this.ids.add(profileEntryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProfileEntries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of profile entry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProfileEntryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  profileEntrySearchOrder profile entry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntrySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>profileEntrySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProfileEntryResults(org.osid.profile.ProfileEntrySearchOrder profileEntrySearchOrder) {
	this.profileEntrySearchOrder = profileEntrySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.profile.ProfileEntrySearchOrder getProfileEntrySearchOrder() {
	return (this.profileEntrySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given profile entry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a profile entry implementing the requested record.
     *
     *  @param profileEntrySearchRecordType a profile entry search record
     *         type
     *  @return the profile entry search record
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntrySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntrySearchRecord getProfileEntrySearchRecord(org.osid.type.Type profileEntrySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.profile.records.ProfileEntrySearchRecord record : this.records) {
            if (record.implementsRecordType(profileEntrySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile entry search. 
     *
     *  @param profileEntrySearchRecord profile entry search record
     *  @param profileEntrySearchRecordType profileEntry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileEntrySearchRecord(org.osid.profile.records.ProfileEntrySearchRecord profileEntrySearchRecord, 
                                           org.osid.type.Type profileEntrySearchRecordType) {

        addRecordType(profileEntrySearchRecordType);
        this.records.add(profileEntrySearchRecord);        
        return;
    }
}
