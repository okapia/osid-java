//
// AbstractModuleSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.module.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractModuleSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.syllabus.ModuleSearchResults {

    private org.osid.course.syllabus.ModuleList modules;
    private final org.osid.course.syllabus.ModuleQueryInspector inspector;
    private final java.util.Collection<org.osid.course.syllabus.records.ModuleSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractModuleSearchResults.
     *
     *  @param modules the result set
     *  @param moduleQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>modules</code>
     *          or <code>moduleQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractModuleSearchResults(org.osid.course.syllabus.ModuleList modules,
                                            org.osid.course.syllabus.ModuleQueryInspector moduleQueryInspector) {
        nullarg(modules, "modules");
        nullarg(moduleQueryInspector, "module query inspectpr");

        this.modules = modules;
        this.inspector = moduleQueryInspector;

        return;
    }


    /**
     *  Gets the module list resulting from a search.
     *
     *  @return a module list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModules() {
        if (this.modules == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.syllabus.ModuleList modules = this.modules;
        this.modules = null;
	return (modules);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.syllabus.ModuleQueryInspector getModuleQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  module search record <code> Type. </code> This method must
     *  be used to retrieve a module implementing the requested
     *  record.
     *
     *  @param moduleSearchRecordType a module search 
     *         record type 
     *  @return the module search
     *  @throws org.osid.NullArgumentException
     *          <code>moduleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(moduleSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.ModuleSearchResultsRecord getModuleSearchResultsRecord(org.osid.type.Type moduleSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.syllabus.records.ModuleSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(moduleSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(moduleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record module search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addModuleRecord(org.osid.course.syllabus.records.ModuleSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "module record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
