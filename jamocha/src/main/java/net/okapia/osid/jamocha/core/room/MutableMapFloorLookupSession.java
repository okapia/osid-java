//
// MutableMapFloorLookupSession
//
//    Implements a Floor lookup service backed by a collection of
//    floors that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room;


/**
 *  Implements a Floor lookup service backed by a collection of
 *  floors. The floors are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of floors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapFloorLookupSession
    extends net.okapia.osid.jamocha.core.room.spi.AbstractMapFloorLookupSession
    implements org.osid.room.FloorLookupSession {


    /**
     *  Constructs a new {@code MutableMapFloorLookupSession}
     *  with no floors.
     *
     *  @param campus the campus
     *  @throws org.osid.NullArgumentException {@code campus} is
     *          {@code null}
     */

      public MutableMapFloorLookupSession(org.osid.room.Campus campus) {
        setCampus(campus);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapFloorLookupSession} with a
     *  single floor.
     *
     *  @param campus the campus  
     *  @param floor a floor
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code floor} is {@code null}
     */

    public MutableMapFloorLookupSession(org.osid.room.Campus campus,
                                           org.osid.room.Floor floor) {
        this(campus);
        putFloor(floor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapFloorLookupSession}
     *  using an array of floors.
     *
     *  @param campus the campus
     *  @param floors an array of floors
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code floors} is {@code null}
     */

    public MutableMapFloorLookupSession(org.osid.room.Campus campus,
                                           org.osid.room.Floor[] floors) {
        this(campus);
        putFloors(floors);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapFloorLookupSession}
     *  using a collection of floors.
     *
     *  @param campus the campus
     *  @param floors a collection of floors
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code floors} is {@code null}
     */

    public MutableMapFloorLookupSession(org.osid.room.Campus campus,
                                           java.util.Collection<? extends org.osid.room.Floor> floors) {

        this(campus);
        putFloors(floors);
        return;
    }

    
    /**
     *  Makes a {@code Floor} available in this session.
     *
     *  @param floor a floor
     *  @throws org.osid.NullArgumentException {@code floor{@code  is
     *          {@code null}
     */

    @Override
    public void putFloor(org.osid.room.Floor floor) {
        super.putFloor(floor);
        return;
    }


    /**
     *  Makes an array of floors available in this session.
     *
     *  @param floors an array of floors
     *  @throws org.osid.NullArgumentException {@code floors{@code 
     *          is {@code null}
     */

    @Override
    public void putFloors(org.osid.room.Floor[] floors) {
        super.putFloors(floors);
        return;
    }


    /**
     *  Makes collection of floors available in this session.
     *
     *  @param floors a collection of floors
     *  @throws org.osid.NullArgumentException {@code floors{@code  is
     *          {@code null}
     */

    @Override
    public void putFloors(java.util.Collection<? extends org.osid.room.Floor> floors) {
        super.putFloors(floors);
        return;
    }


    /**
     *  Removes a Floor from this session.
     *
     *  @param floorId the {@code Id} of the floor
     *  @throws org.osid.NullArgumentException {@code floorId{@code 
     *          is {@code null}
     */

    @Override
    public void removeFloor(org.osid.id.Id floorId) {
        super.removeFloor(floorId);
        return;
    }    
}
