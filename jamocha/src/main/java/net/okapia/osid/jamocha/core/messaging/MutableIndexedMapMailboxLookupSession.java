//
// MutableIndexedMapMailboxLookupSession
//
//    Implements a Mailbox lookup service backed by a collection of
//    mailboxes indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging;


/**
 *  Implements a Mailbox lookup service backed by a collection of
 *  mailboxes. The mailboxes are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some mailboxes may be compatible
 *  with more types than are indicated through these mailbox
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of mailboxes can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapMailboxLookupSession
    extends net.okapia.osid.jamocha.core.messaging.spi.AbstractIndexedMapMailboxLookupSession
    implements org.osid.messaging.MailboxLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapMailboxLookupSession} with no
     *  mailboxes.
     */

    public MutableIndexedMapMailboxLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapMailboxLookupSession} with a
     *  single mailbox.
     *  
     *  @param  mailbox a single mailbox
     *  @throws org.osid.NullArgumentException {@code mailbox}
     *          is {@code null}
     */

    public MutableIndexedMapMailboxLookupSession(org.osid.messaging.Mailbox mailbox) {
        putMailbox(mailbox);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapMailboxLookupSession} using an
     *  array of mailboxes.
     *
     *  @param  mailboxes an array of mailboxes
     *  @throws org.osid.NullArgumentException {@code mailboxes}
     *          is {@code null}
     */

    public MutableIndexedMapMailboxLookupSession(org.osid.messaging.Mailbox[] mailboxes) {
        putMailboxes(mailboxes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapMailboxLookupSession} using a
     *  collection of mailboxes.
     *
     *  @param  mailboxes a collection of mailboxes
     *  @throws org.osid.NullArgumentException {@code mailboxes} is
     *          {@code null}
     */

    public MutableIndexedMapMailboxLookupSession(java.util.Collection<? extends org.osid.messaging.Mailbox> mailboxes) {
        putMailboxes(mailboxes);
        return;
    }
    

    /**
     *  Makes a {@code Mailbox} available in this session.
     *
     *  @param  mailbox a mailbox
     *  @throws org.osid.NullArgumentException {@code mailbox{@code  is
     *          {@code null}
     */

    @Override
    public void putMailbox(org.osid.messaging.Mailbox mailbox) {
        super.putMailbox(mailbox);
        return;
    }


    /**
     *  Makes an array of mailboxes available in this session.
     *
     *  @param  mailboxes an array of mailboxes
     *  @throws org.osid.NullArgumentException {@code mailboxes{@code 
     *          is {@code null}
     */

    @Override
    public void putMailboxes(org.osid.messaging.Mailbox[] mailboxes) {
        super.putMailboxes(mailboxes);
        return;
    }


    /**
     *  Makes collection of mailboxes available in this session.
     *
     *  @param  mailboxes a collection of mailboxes
     *  @throws org.osid.NullArgumentException {@code mailbox{@code  is
     *          {@code null}
     */

    @Override
    public void putMailboxes(java.util.Collection<? extends org.osid.messaging.Mailbox> mailboxes) {
        super.putMailboxes(mailboxes);
        return;
    }


    /**
     *  Removes a Mailbox from this session.
     *
     *  @param mailboxId the {@code Id} of the mailbox
     *  @throws org.osid.NullArgumentException {@code mailboxId{@code  is
     *          {@code null}
     */

    @Override
    public void removeMailbox(org.osid.id.Id mailboxId) {
        super.removeMailbox(mailboxId);
        return;
    }    
}
