//
// AbstractFederatingGraphLookupSession.java
//
//     An abstract federating adapter for a GraphLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.topology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  GraphLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingGraphLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.topology.GraphLookupSession>
    implements org.osid.topology.GraphLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingGraphLookupSession</code>.
     */

    protected AbstractFederatingGraphLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.topology.GraphLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Graph</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGraphs() {
        for (org.osid.topology.GraphLookupSession session : getSessions()) {
            if (session.canLookupGraphs()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Graph</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGraphView() {
        for (org.osid.topology.GraphLookupSession session : getSessions()) {
            session.useComparativeGraphView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Graph</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGraphView() {
        for (org.osid.topology.GraphLookupSession session : getSessions()) {
            session.usePlenaryGraphView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Graph</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Graph</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Graph</code> and
     *  retained for compatibility.
     *
     *  @param  graphId <code>Id</code> of the
     *          <code>Graph</code>
     *  @return the graph
     *  @throws org.osid.NotFoundException <code>graphId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>graphId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.topology.GraphLookupSession session : getSessions()) {
            try {
                return (session.getGraph(graphId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(graphId + " not found");
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  graphs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Graphs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  graphIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>graphIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByIds(org.osid.id.IdList graphIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.topology.graph.MutableGraphList ret = new net.okapia.osid.jamocha.topology.graph.MutableGraphList();

        try (org.osid.id.IdList ids = graphIds) {
            while (ids.hasNext()) {
                ret.addGraph(getGraph(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given
     *  graph genus <code>Type</code> which does not include
     *  graphs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  graphGenusType a graph genus type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByGenusType(org.osid.type.Type graphGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.graph.FederatingGraphList ret = getGraphList();

        for (org.osid.topology.GraphLookupSession session : getSessions()) {
            ret.addGraphList(session.getGraphsByGenusType(graphGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given
     *  graph genus <code>Type</code> and include any additional
     *  graphs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  graphGenusType a graph genus type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByParentGenusType(org.osid.type.Type graphGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.graph.FederatingGraphList ret = getGraphList();

        for (org.osid.topology.GraphLookupSession session : getSessions()) {
            ret.addGraphList(session.getGraphsByParentGenusType(graphGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GraphList</code> containing the given
     *  graph record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  graphRecordType a graph record type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByRecordType(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.graph.FederatingGraphList ret = getGraphList();

        for (org.osid.topology.GraphLookupSession session : getSessions()) {
            ret.addGraphList(session.getGraphsByRecordType(graphRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GraphList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known graphs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  graphs that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Graph</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.topology.graph.FederatingGraphList ret = getGraphList();

        for (org.osid.topology.GraphLookupSession session : getSessions()) {
            ret.addGraphList(session.getGraphsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Graphs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Graphs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.graph.FederatingGraphList ret = getGraphList();

        for (org.osid.topology.GraphLookupSession session : getSessions()) {
            ret.addGraphList(session.getGraphs());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.topology.graph.FederatingGraphList getGraphList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.graph.ParallelGraphList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.graph.CompositeGraphList());
        }
    }
}
