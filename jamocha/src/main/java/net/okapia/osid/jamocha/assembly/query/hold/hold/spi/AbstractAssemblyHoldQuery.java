//
// AbstractAssemblyHoldQuery.java
//
//     A HoldQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.hold.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A HoldQuery that stores terms.
 */

public abstract class AbstractAssemblyHoldQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.hold.HoldQuery,
               org.osid.hold.HoldQueryInspector,
               org.osid.hold.HoldSearchOrder {

    private final java.util.Collection<org.osid.hold.records.HoldQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.records.HoldQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.records.HoldSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyHoldQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyHoldQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches holds that have any resource defined. 
     *
     *  @param  match <code> true </code> to match holds with any resources, 
     *          <code> false </code> to match holds with no resources 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        getAssembler().addIdWildcardTerm(getResourceColumn(), match);
        return;
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Orders the results by agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAgentColumn(), style);
        return;
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Matches holds that have any agent defined. 
     *
     *  @param  match <code> true </code> to match holds with any agents, 
     *          <code> false </code> to match holds with no agents 
     */

    @OSID @Override
    public void matchAnyAgent(boolean match) {
        getAssembler().addIdWildcardTerm(getAgentColumn(), match);
        return;
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Sets the issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        getAssembler().clearTerms(getIssueIdColumn());
        return;
    }


    /**
     *  Gets the issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIssueIdTerms() {
        return (getAssembler().getIdTerms(getIssueIdColumn()));
    }


    /**
     *  Orders the results by issue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByIssue(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getIssueColumn(), style);
        return;
    }


    /**
     *  Gets the IssueId column name.
     *
     * @return the column name
     */

    protected String getIssueIdColumn() {
        return ("issue_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if an issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for an issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        getAssembler().clearTerms(getIssueColumn());
        return;
    }


    /**
     *  Gets the issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.IssueQueryInspector[] getIssueTerms() {
        return (new org.osid.hold.IssueQueryInspector[0]);
    }


    /**
     *  Tests if an issue search order is available. 
     *
     *  @return <code> true </code> if an issue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the issue search order. 
     *
     *  @return the issue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsIssueSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSearchOrder getIssueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsIssueSearchOrder() is false");
    }


    /**
     *  Gets the Issue column name.
     *
     * @return the column name
     */

    protected String getIssueColumn() {
        return ("issue");
    }


    /**
     *  Sets the oubliette <code> Id </code> for this query to match holds 
     *  assigned to foundries. 
     *
     *  @param  oublietteId the oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOublietteId(org.osid.id.Id oublietteId, boolean match) {
        getAssembler().addIdTerm(getOublietteIdColumn(), oublietteId, match);
        return;
    }


    /**
     *  Clears the oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOublietteIdTerms() {
        getAssembler().clearTerms(getOublietteIdColumn());
        return;
    }


    /**
     *  Gets the oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOublietteIdTerms() {
        return (getAssembler().getIdTerms(getOublietteIdColumn()));
    }


    /**
     *  Gets the OublietteId column name.
     *
     * @return the column name
     */

    protected String getOublietteIdColumn() {
        return ("oubliette_id");
    }


    /**
     *  Tests if a <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsOublietteQuery() is false");
    }


    /**
     *  Clears the oubliette query terms. 
     */

    @OSID @Override
    public void clearOublietteTerms() {
        getAssembler().clearTerms(getOublietteColumn());
        return;
    }


    /**
     *  Gets the oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }


    /**
     *  Gets the Oubliette column name.
     *
     * @return the column name
     */

    protected String getOublietteColumn() {
        return ("oubliette");
    }


    /**
     *  Tests if this hold supports the given record
     *  <code>Type</code>.
     *
     *  @param  holdRecordType a hold record type 
     *  @return <code>true</code> if the holdRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type holdRecordType) {
        for (org.osid.hold.records.HoldQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(holdRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  holdRecordType the hold record type 
     *  @return the hold query record 
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.HoldQueryRecord getHoldQueryRecord(org.osid.type.Type holdRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.HoldQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(holdRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  holdRecordType the hold record type 
     *  @return the hold query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.HoldQueryInspectorRecord getHoldQueryInspectorRecord(org.osid.type.Type holdRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.HoldQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(holdRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param holdRecordType the hold record type
     *  @return the hold search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.HoldSearchOrderRecord getHoldSearchOrderRecord(org.osid.type.Type holdRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.HoldSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(holdRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this hold. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param holdQueryRecord the hold query record
     *  @param holdQueryInspectorRecord the hold query inspector
     *         record
     *  @param holdSearchOrderRecord the hold search order record
     *  @param holdRecordType hold record type
     *  @throws org.osid.NullArgumentException
     *          <code>holdQueryRecord</code>,
     *          <code>holdQueryInspectorRecord</code>,
     *          <code>holdSearchOrderRecord</code> or
     *          <code>holdRecordTypehold</code> is
     *          <code>null</code>
     */
            
    protected void addHoldRecords(org.osid.hold.records.HoldQueryRecord holdQueryRecord, 
                                      org.osid.hold.records.HoldQueryInspectorRecord holdQueryInspectorRecord, 
                                      org.osid.hold.records.HoldSearchOrderRecord holdSearchOrderRecord, 
                                      org.osid.type.Type holdRecordType) {

        addRecordType(holdRecordType);

        nullarg(holdQueryRecord, "hold query record");
        nullarg(holdQueryInspectorRecord, "hold query inspector record");
        nullarg(holdSearchOrderRecord, "hold search odrer record");

        this.queryRecords.add(holdQueryRecord);
        this.queryInspectorRecords.add(holdQueryInspectorRecord);
        this.searchOrderRecords.add(holdSearchOrderRecord);
        
        return;
    }
}
