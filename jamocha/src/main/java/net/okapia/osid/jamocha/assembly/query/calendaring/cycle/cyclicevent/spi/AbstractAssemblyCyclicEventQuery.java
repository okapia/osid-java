//
// AbstractAssemblyCyclicEventQuery.java
//
//     A CyclicEventQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.cycle.cyclicevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CyclicEventQuery that stores terms.
 */

public abstract class AbstractAssemblyCyclicEventQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.calendaring.cycle.CyclicEventQuery,
               org.osid.calendaring.cycle.CyclicEventQueryInspector,
               org.osid.calendaring.cycle.CyclicEventSearchOrder {

    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicEventQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicEventQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicEventSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCyclicEventQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCyclicEventQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches cyclic events that are related to the given event. 
     *
     *  @param  eventId an <code> Id </code> for an event 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        getAssembler().clearTerms(getEventIdColumn());
        return;
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (getAssembler().getIdTerms(getEventIdColumn()));
    }


    /**
     *  Gets the EventId column name.
     *
     * @return the column name
     */

    protected String getEventIdColumn() {
        return ("event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Matches a cyclic event that has any related event. 
     *
     *  @param  match <code> true </code> to match cyclic events with any 
     *          event, <code> false </code> to match cyclic events with no 
     *          related events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getEventColumn(), match);
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        getAssembler().clearTerms(getEventColumn());
        return;
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the Event column name.
     *
     * @return the column name
     */

    protected String getEventColumn() {
        return ("event");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this cyclicEvent supports the given record
     *  <code>Type</code>.
     *
     *  @param  cyclicEventRecordType a cyclic event record type 
     *  @return <code>true</code> if the cyclicEventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type cyclicEventRecordType) {
        for (org.osid.calendaring.cycle.records.CyclicEventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(cyclicEventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  cyclicEventRecordType the cyclic event record type 
     *  @return the cyclic event query record 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicEventQueryRecord getCyclicEventQueryRecord(org.osid.type.Type cyclicEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.cycle.records.CyclicEventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(cyclicEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicEventRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  cyclicEventRecordType the cyclic event record type 
     *  @return the cyclic event query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicEventQueryInspectorRecord getCyclicEventQueryInspectorRecord(org.osid.type.Type cyclicEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.cycle.records.CyclicEventQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(cyclicEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicEventRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param cyclicEventRecordType the cyclic event record type
     *  @return the cyclic event search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicEventSearchOrderRecord getCyclicEventSearchOrderRecord(org.osid.type.Type cyclicEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.cycle.records.CyclicEventSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(cyclicEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicEventRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this cyclic event. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param cyclicEventQueryRecord the cyclic event query record
     *  @param cyclicEventQueryInspectorRecord the cyclic event query inspector
     *         record
     *  @param cyclicEventSearchOrderRecord the cyclic event search order record
     *  @param cyclicEventRecordType cyclic event record type
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventQueryRecord</code>,
     *          <code>cyclicEventQueryInspectorRecord</code>,
     *          <code>cyclicEventSearchOrderRecord</code> or
     *          <code>cyclicEventRecordTypecyclicEvent</code> is
     *          <code>null</code>
     */
            
    protected void addCyclicEventRecords(org.osid.calendaring.cycle.records.CyclicEventQueryRecord cyclicEventQueryRecord, 
                                      org.osid.calendaring.cycle.records.CyclicEventQueryInspectorRecord cyclicEventQueryInspectorRecord, 
                                      org.osid.calendaring.cycle.records.CyclicEventSearchOrderRecord cyclicEventSearchOrderRecord, 
                                      org.osid.type.Type cyclicEventRecordType) {

        addRecordType(cyclicEventRecordType);

        nullarg(cyclicEventQueryRecord, "cyclic event query record");
        nullarg(cyclicEventQueryInspectorRecord, "cyclic event query inspector record");
        nullarg(cyclicEventSearchOrderRecord, "cyclic event search odrer record");

        this.queryRecords.add(cyclicEventQueryRecord);
        this.queryInspectorRecords.add(cyclicEventQueryInspectorRecord);
        this.searchOrderRecords.add(cyclicEventSearchOrderRecord);
        
        return;
    }
}
