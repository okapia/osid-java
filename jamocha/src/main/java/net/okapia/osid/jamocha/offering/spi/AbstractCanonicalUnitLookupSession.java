//
// AbstractCanonicalUnitLookupSession.java
//
//    A starter implementation framework for providing a CanonicalUnit
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CanonicalUnit
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCanonicalUnits(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCanonicalUnitLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.offering.CanonicalUnitLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();
    

    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>CanonicalUnit</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCanonicalUnits() {
        return (true);
    }


    /**
     *  A complete view of the <code>CanonicalUnit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCanonicalUnitView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CanonicalUnit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCanonicalUnitView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical units in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active canonical units are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive canonical units are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CanonicalUnit</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CanonicalUnit</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CanonicalUnit</code> and retained for compatibility.
     *
     *  In active mode, canonical units are returned that are
     *  currently active. In any status mode, active and inactive
     *  canonical units are returned.
     *
     *  @param  canonicalUnitId <code>Id</code> of the
     *          <code>CanonicalUnit</code>
     *  @return the canonical unit
     *  @throws org.osid.NotFoundException <code>canonicalUnitId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnit getCanonicalUnit(org.osid.id.Id canonicalUnitId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.offering.CanonicalUnitList canonicalUnits = getCanonicalUnits()) {
            while (canonicalUnits.hasNext()) {
                org.osid.offering.CanonicalUnit canonicalUnit = canonicalUnits.getNextCanonicalUnit();
                if (canonicalUnit.getId().equals(canonicalUnitId)) {
                    return (canonicalUnit);
                }
            }
        } 

        throw new org.osid.NotFoundException(canonicalUnitId + " not found");
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnits specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CanonicalUnits</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCanonicalUnits()</code>.
     *
     *  @param  canonicalUnitIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByIds(org.osid.id.IdList canonicalUnitIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.offering.CanonicalUnit> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = canonicalUnitIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCanonicalUnit(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("canonical unit " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.offering.canonicalunit.LinkedCanonicalUnitList(ret));
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the given
     *  canonical unit genus <code>Type</code> which does not include
     *  canonical units of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCanonicalUnits()</code>.
     *
     *  @param  canonicalUnitGenusType a canonicalUnit genus type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByGenusType(org.osid.type.Type canonicalUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.canonicalunit.CanonicalUnitGenusFilterList(getCanonicalUnits(), canonicalUnitGenusType));
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the given
     *  canonical unit genus <code>Type</code> and include any additional
     *  canonical units with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCanonicalUnits()</code>.
     *
     *  @param  canonicalUnitGenusType a canonicalUnit genus type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByParentGenusType(org.osid.type.Type canonicalUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCanonicalUnitsByGenusType(canonicalUnitGenusType));
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> containing the given
     *  canonical unit record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCanonicalUnits()</code>.
     *
     *  @param  canonicalUnitRecordType a canonicalUnit record type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByRecordType(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.canonicalunit.CanonicalUnitRecordFilterList(getCanonicalUnits(), canonicalUnitRecordType));
    }


    /**
     *  Gets canonical units by code. 
     *  
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned
     *  list may contain only those canonical units that are
     *  accessible through this session.
     *  
     *  In active mode, canonicals are returned that are currently
     *  active. In any status mode, active and inactive canonicals are
     *  returned.
     *
     *  @param  code a code 
     *  @return a list of canonical units 
     *  @throws org.osid.NullArgumentException <code> code </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByCode(String code)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.canonicalunit.CanonicalUnitFilterList(new CodeFilter(code), getCanonicalUnits()));
    }


    /**
     *  Gets all <code>CanonicalUnits</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @return a list of <code>CanonicalUnits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.offering.CanonicalUnitList getCanonicalUnits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the canonical unit list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of canonical units
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.offering.CanonicalUnitList filterCanonicalUnitsOnViews(org.osid.offering.CanonicalUnitList list)
        throws org.osid.OperationFailedException {

        org.osid.offering.CanonicalUnitList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.offering.canonicalunit.ActiveCanonicalUnitFilterList(ret);
        }

        return (ret);
    }


    public static class CodeFilter
        implements net.okapia.osid.jamocha.inline.filter.offering.canonicalunit.CanonicalUnitFilter {
         
        private final String code;
         
         
        /**
         *  Constructs a new <code>CodeFilter</code>.
         *
         *  @param code the code to filter
         *  @throws org.osid.NullArgumentException
         *          <code>code</code> is <code>null</code>
         */
        
        public CodeFilter(String code) {
            nullarg(code, "code");
            this.code = code;
            return;
        }

         
        /**
         *  Used by the CanonicalUnitFilterList to filter the 
         *  canonical unit list based on code.
         *
         *  @param canonicalUnit the canonical unit
         *  @return <code>true</code> to pass the canonical unit,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.offering.CanonicalUnit canonicalUnit) {
            return (canonicalUnit.getCode().equals(this.code));
        }
    }
}
