//
// AbstractQueryCanonicalUnitProcessorLookupSession.java
//
//    A CanonicalUnitProcessorQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CanonicalUnitProcessorQuerySession adapter.
 */

public abstract class AbstractAdapterCanonicalUnitProcessorQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.rules.CanonicalUnitProcessorQuerySession {

    private final org.osid.offering.rules.CanonicalUnitProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterCanonicalUnitProcessorQuerySession.
     *
     *  @param session the underlying canonical unit processor query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCanonicalUnitProcessorQuerySession(org.osid.offering.rules.CanonicalUnitProcessorQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeCatalogue</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeCatalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@codeCatalogue</code> associated with this 
     *  session.
     *
     *  @return the {@codeCatalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@codeCanonicalUnitProcessor</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchCanonicalUnitProcessors() {
        return (this.session.canSearchCanonicalUnitProcessors());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit processors in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this catalogue only.
     */
    
    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    
      
    /**
     *  Gets a canonical unit processor query. The returned query will not have an
     *  extension query.
     *
     *  @return the canonical unit processor query 
     */
      
    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQuery getCanonicalUnitProcessorQuery() {
        return (this.session.getCanonicalUnitProcessorQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  canonicalUnitProcessorQuery the canonical unit processor query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code canonicalUnitProcessorQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByQuery(org.osid.offering.rules.CanonicalUnitProcessorQuery canonicalUnitProcessorQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getCanonicalUnitProcessorsByQuery(canonicalUnitProcessorQuery));
    }
}
