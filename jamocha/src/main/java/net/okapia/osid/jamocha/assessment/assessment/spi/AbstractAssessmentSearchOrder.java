//
// AbstractAssessmentSearchOdrer.java
//
//     Defines an AssessmentSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AssessmentSearchOrder}.
 */

public abstract class AbstractAssessmentSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.assessment.AssessmentSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.AssessmentSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the level of 
     *  difficulty. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLevel(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLevelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getLevelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLevelSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the rubric 
     *  assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRubric(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an assessment search order is available. 
     *
     *  @return <code> true </code> if an assessment search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment search order. 
     *
     *  @return a rubric assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRubricSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getRubricSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRubricSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  assessmentRecordType an assessment record type 
     *  @return {@code true} if the assessmentRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentRecordType) {
        for (org.osid.assessment.records.AssessmentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  assessmentRecordType the assessment record type 
     *  @return the assessment search order record
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(assessmentRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentSearchOrderRecord getAssessmentSearchOrderRecord(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this assessment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentRecord the assessment search odrer record
     *  @param assessmentRecordType assessment record type
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentRecord} or
     *          {@code assessmentRecordTypeassessment} is
     *          {@code null}
     */
            
    protected void addAssessmentRecord(org.osid.assessment.records.AssessmentSearchOrderRecord assessmentSearchOrderRecord, 
                                     org.osid.type.Type assessmentRecordType) {

        addRecordType(assessmentRecordType);
        this.records.add(assessmentSearchOrderRecord);
        
        return;
    }
}
