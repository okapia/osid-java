//
// AbstractCalendaringProxyManager.java
//
//     An adapter for a CalendaringProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CalendaringProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCalendaringProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.calendaring.CalendaringProxyManager>
    implements org.osid.calendaring.CalendaringProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCalendaringProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCalendaringProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if an event lookup service is supported. An event lookup service 
     *  defines methods to access events. 
     *
     *  @return true if event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsEventLookup() {
        return (getAdapteeManager().supportsEventLookup());
    }


    /**
     *  Tests if an event query service is supported. 
     *
     *  @return <code> true </code> if event query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (getAdapteeManager().supportsEventQuery());
    }


    /**
     *  Tests if an event search service is supported. 
     *
     *  @return <code> true </code> if event search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventSearch() {
        return (getAdapteeManager().supportsEventSearch());
    }


    /**
     *  Tests if an event administrative service is supported. 
     *
     *  @return <code> true </code> if event admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventAdmin() {
        return (getAdapteeManager().supportsEventAdmin());
    }


    /**
     *  Tests if event notification is supported. Messages may be sent when 
     *  events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if event notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventNotification() {
        return (getAdapteeManager().supportsEventNotification());
    }


    /**
     *  Tests if an event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if event calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventCalendar() {
        return (getAdapteeManager().supportsEventCalendar());
    }


    /**
     *  Tests if an event to calendar assignment session is available. 
     *
     *  @return <code> true </code> if event calendar assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventCalendarAssignment() {
        return (getAdapteeManager().supportsEventCalendarAssignment());
    }


    /**
     *  Tests if event smart calendaring is available. 
     *
     *  @return <code> true </code> if event smart calendaring is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventSmartCalendar() {
        return (getAdapteeManager().supportsEventSmartCalendar());
    }


    /**
     *  Tests if a recurring event lookup service is supported. 
     *
     *  @return true if recurring event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventLookup() {
        return (getAdapteeManager().supportsRecurringEventLookup());
    }


    /**
     *  Tests if a recurring event unravelling service is supported. 
     *
     *  @return true if recurring event unravelling is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventUnravelling() {
        return (getAdapteeManager().supportsRecurringEventUnravelling());
    }


    /**
     *  Tests if a recurring event query service is supported. 
     *
     *  @return <code> true </code> if recurring event query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventQuery() {
        return (getAdapteeManager().supportsRecurringEventQuery());
    }


    /**
     *  Tests if a recurring event search service is supported. 
     *
     *  @return <code> true </code> if recurring event search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventSearch() {
        return (getAdapteeManager().supportsRecurringEventSearch());
    }


    /**
     *  Tests if a recurring event administrative service is supported. 
     *
     *  @return <code> true </code> if recurring event admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventAdmin() {
        return (getAdapteeManager().supportsRecurringEventAdmin());
    }


    /**
     *  Tests if recurring event notification is supported. Messages may be 
     *  sent when recurring events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if recurring event notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventNotification() {
        return (getAdapteeManager().supportsRecurringEventNotification());
    }


    /**
     *  Tests if a recurring event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if recurring event calendar lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventCalendar() {
        return (getAdapteeManager().supportsRecurringEventCalendar());
    }


    /**
     *  Tests if a recurring event to calendar assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if recurring event calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventCalendarAssignment() {
        return (getAdapteeManager().supportsRecurringEventCalendarAssignment());
    }


    /**
     *  Tests if recurring event smart calendaring is available. 
     *
     *  @return <code> true </code> if recurring event smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventSmartCalendar() {
        return (getAdapteeManager().supportsRecurringEventSmartCalendar());
    }


    /**
     *  Tests if a superseding event lookup service is supported. A 
     *  superseding event lookup service defines methods to access superseding 
     *  events. 
     *
     *  @return true if superseding event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventLookup() {
        return (getAdapteeManager().supportsSupersedingEventLookup());
    }


    /**
     *  Tests if a superseding event query service is supported. 
     *
     *  @return <code> true </code> if superseding event query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventQuery() {
        return (getAdapteeManager().supportsSupersedingEventQuery());
    }


    /**
     *  Tests if a superseding event search service is supported. 
     *
     *  @return <code> true </code> if superseding event search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventSearch() {
        return (getAdapteeManager().supportsSupersedingEventSearch());
    }


    /**
     *  Tests if a superseding event administrative service is supported. 
     *
     *  @return <code> true </code> if superseding event admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventAdmin() {
        return (getAdapteeManager().supportsSupersedingEventAdmin());
    }


    /**
     *  Tests if superseding event notification is supported. Messages may be 
     *  sent when supsreding events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if superseding event notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventNotification() {
        return (getAdapteeManager().supportsSupersedingEventNotification());
    }


    /**
     *  Tests if superseding event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if superseding event calendar lookup 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventCalendar() {
        return (getAdapteeManager().supportsSupersedingEventCalendar());
    }


    /**
     *  Tests if a superseding event to calendar assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if superseding event calendar assignment 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventCalendarAssignment() {
        return (getAdapteeManager().supportsSupersedingEventCalendarAssignment());
    }


    /**
     *  Tests if supsreding event smart calendaring is available. 
     *
     *  @return <code> true </code> if superseding smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventSmartCalendar() {
        return (getAdapteeManager().supportsSupersedingEventSmartCalendar());
    }


    /**
     *  Tests if an offset event lookup service is supported. An offset event 
     *  lookup service defines methods to access events. 
     *
     *  @return true if offset event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventLookup() {
        return (getAdapteeManager().supportsOffsetEventLookup());
    }


    /**
     *  Tests if an offset event query service is supported. 
     *
     *  @return <code> true </code> if offset event query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventQuery() {
        return (getAdapteeManager().supportsOffsetEventQuery());
    }


    /**
     *  Tests if an offset event search service is supported. 
     *
     *  @return <code> true </code> if offset event search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventSearch() {
        return (getAdapteeManager().supportsOffsetEventSearch());
    }


    /**
     *  Tests if an offset event administrative service is supported. 
     *
     *  @return <code> true </code> if offset event admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventAdmin() {
        return (getAdapteeManager().supportsOffsetEventAdmin());
    }


    /**
     *  Tests if offset event notification is supported. Messages may be sent 
     *  when events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if offset event notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventNotification() {
        return (getAdapteeManager().supportsOffsetEventNotification());
    }


    /**
     *  Tests if an offset event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if event calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventCalendar() {
        return (getAdapteeManager().supportsOffsetEventCalendar());
    }


    /**
     *  Tests if an offset event to calendar assignment session is available. 
     *
     *  @return <code> true </code> if offset event calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventCalendarAssignment() {
        return (getAdapteeManager().supportsOffsetEventCalendarAssignment());
    }


    /**
     *  Tests if offset event smart calendaring is available. 
     *
     *  @return <code> true </code> if offset event smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventSmartCalendar() {
        return (getAdapteeManager().supportsOffsetEventSmartCalendar());
    }


    /**
     *  Tests if a schedule lookup service is supported. A schedule lookup 
     *  service defines methods to access schedules. 
     *
     *  @return true if schedule lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleLookup() {
        return (getAdapteeManager().supportsScheduleLookup());
    }


    /**
     *  Tests if a schedule query service is supported. 
     *
     *  @return <code> true </code> if schedule query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleQuery() {
        return (getAdapteeManager().supportsScheduleQuery());
    }


    /**
     *  Tests if a schedule search service is supported. 
     *
     *  @return <code> true </code> if schedule search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSearch() {
        return (getAdapteeManager().supportsScheduleSearch());
    }


    /**
     *  Tests if a schedule administrative service is supported. 
     *
     *  @return <code> true </code> if schedule admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleAdmin() {
        return (getAdapteeManager().supportsScheduleAdmin());
    }


    /**
     *  Tests if schedule notification is supported. Messages may be sent when 
     *  schedules are created, modified, or deleted. 
     *
     *  @return <code> true </code> if schedule notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleNotification() {
        return (getAdapteeManager().supportsScheduleNotification());
    }


    /**
     *  Tests if a schedule to calendar lookup session is available. 
     *
     *  @return <code> true </code> if schedule calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleCalendar() {
        return (getAdapteeManager().supportsScheduleCalendar());
    }


    /**
     *  Tests if a schedule to calendar assignment session is available. 
     *
     *  @return <code> true </code> if schedule calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleCalendarAssignment() {
        return (getAdapteeManager().supportsScheduleCalendarAssignment());
    }


    /**
     *  Tests if schedule smart calendaring is available. 
     *
     *  @return <code> true </code> if schedule smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSmartCalendar() {
        return (getAdapteeManager().supportsScheduleSmartCalendar());
    }


    /**
     *  Tests if a schedule slot lookup service is supported. A schedule sot 
     *  lookup service defines methods to access schedule slots. 
     *
     *  @return true if schedule slot lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotLookup() {
        return (getAdapteeManager().supportsScheduleSlotLookup());
    }


    /**
     *  Tests if a schedule slot query service is supported. 
     *
     *  @return <code> true </code> if schedule slot query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotQuery() {
        return (getAdapteeManager().supportsScheduleSlotQuery());
    }


    /**
     *  Tests if a schedule slot search service is supported. 
     *
     *  @return <code> true </code> if schedule slots search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotSearch() {
        return (getAdapteeManager().supportsScheduleSlotSearch());
    }


    /**
     *  Tests if a schedule slot administrative service is supported. 
     *
     *  @return <code> true </code> if schedule slot admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotAdmin() {
        return (getAdapteeManager().supportsScheduleSlotAdmin());
    }


    /**
     *  Tests if schedule slot notification is supported. Messages may be sent 
     *  when schedule slots are created, modified, or deleted. 
     *
     *  @return <code> true </code> if schedule slot notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotNotification() {
        return (getAdapteeManager().supportsScheduleSlotNotification());
    }


    /**
     *  Tests if a schedule slot to calendar lookup session is available. 
     *
     *  @return <code> true </code> if schedule slot calendar lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotCalendar() {
        return (getAdapteeManager().supportsScheduleSlotCalendar());
    }


    /**
     *  Tests if a schedule slot to calendar assignment session is available. 
     *
     *  @return <code> true </code> if schedule slot calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotCalendarAssignment() {
        return (getAdapteeManager().supportsScheduleSlotCalendarAssignment());
    }


    /**
     *  Tests if schedule slot smart calendaring is available. 
     *
     *  @return <code> true </code> if schedule slot smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotSmartCalendar() {
        return (getAdapteeManager().supportsScheduleSlotSmartCalendar());
    }


    /**
     *  Tests if an event commitment lookup service is supported. 
     *
     *  @return <code> true </code> if commitment lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentLookup() {
        return (getAdapteeManager().supportsCommitmentLookup());
    }


    /**
     *  Tests if a commitment query service is supported. 
     *
     *  @return <code> true </code> if commitment query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentQuery() {
        return (getAdapteeManager().supportsCommitmentQuery());
    }


    /**
     *  Tests if a commitment search service is supported. 
     *
     *  @return <code> true </code> if commitment search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentSearch() {
        return (getAdapteeManager().supportsCommitmentSearch());
    }


    /**
     *  Tests if a commitment administrative service is supported. 
     *
     *  @return <code> true </code> if commitment admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentAdmin() {
        return (getAdapteeManager().supportsCommitmentAdmin());
    }


    /**
     *  Tests if commitment notification is supported. Messages may be sent 
     *  when commitments are created, modified, or deleted. 
     *
     *  @return <code> true </code> if commitment notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentNotification() {
        return (getAdapteeManager().supportsCommitmentNotification());
    }


    /**
     *  Tests if a commitment to calendar lookup session is available. 
     *
     *  @return <code> true </code> if commitment calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentCalendar() {
        return (getAdapteeManager().supportsCommitmentCalendar());
    }


    /**
     *  Tests if a commitment to calendar assignment session is available. 
     *
     *  @return <code> true </code> if commitment calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentCalendarAssignment() {
        return (getAdapteeManager().supportsCommitmentCalendarAssignment());
    }


    /**
     *  Tests if commitment smart calendaring is available. 
     *
     *  @return <code> true </code> if commitment smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentSmartCalendar() {
        return (getAdapteeManager().supportsCommitmentSmartCalendar());
    }


    /**
     *  Tests if a time period lookup service is supported. 
     *
     *  @return <code> true </code> if time period lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodLookup() {
        return (getAdapteeManager().supportsTimePeriodLookup());
    }


    /**
     *  Tests if a time period search service is supported. 
     *
     *  @return <code> true </code> if time period search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearch() {
        return (getAdapteeManager().supportsTimePeriodSearch());
    }


    /**
     *  Tests if a time period administrative service is supported. 
     *
     *  @return <code> true </code> if time period admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodAdmin() {
        return (getAdapteeManager().supportsTimePeriodAdmin());
    }


    /**
     *  Tests if time period notification is supported. Messages may be sent 
     *  when time periods are created, modified, or deleted. 
     *
     *  @return <code> true </code> if time period notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodNotification() {
        return (getAdapteeManager().supportsTimePeriodNotification());
    }


    /**
     *  Tests if a time period to calendar lookup session is available. 
     *
     *  @return <code> true </code> if time period calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodCalendar() {
        return (getAdapteeManager().supportsTimePeriodCalendar());
    }


    /**
     *  Tests if a time period to calendar assignment session is available. 
     *
     *  @return <code> true </code> if time period calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodCalendarAssignment() {
        return (getAdapteeManager().supportsTimePeriodCalendarAssignment());
    }


    /**
     *  Tests if time period smart calendaring is available. 
     *
     *  @return <code> true </code> if time period smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSmartCalendar() {
        return (getAdapteeManager().supportsTimePeriodSmartCalendar());
    }


    /**
     *  Tests if a calendar lookup service is supported. 
     *
     *  @return <code> true </code> if calendar lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarLookup() {
        return (getAdapteeManager().supportsCalendarLookup());
    }


    /**
     *  Tests if a calendar search service is supported. 
     *
     *  @return <code> true </code> if calendar search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarSearch() {
        return (getAdapteeManager().supportsCalendarSearch());
    }


    /**
     *  Tests if a calendar administrative service is supported. 
     *
     *  @return <code> true </code> if calendar admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarAdmin() {
        return (getAdapteeManager().supportsCalendarAdmin());
    }


    /**
     *  Tests if calendar notification is supported. Messages may be sent when 
     *  calendars are created, modified, or deleted. 
     *
     *  @return <code> true </code> if calendar notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarNotification() {
        return (getAdapteeManager().supportsCalendarNotification());
    }


    /**
     *  Tests if a calendar hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a calendar hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarHierarchy() {
        return (getAdapteeManager().supportsCalendarHierarchy());
    }


    /**
     *  Tests if calendar hierarchy design is supported. 
     *
     *  @return <code> true </code> if a calendar hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarHierarchyDesign() {
        return (getAdapteeManager().supportsCalendarHierarchyDesign());
    }


    /**
     *  Tests if a calendaring batch subpackage is supported. 
     *
     *  @return <code> true </code> if a calendar batch package is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringBatch() {
        return (getAdapteeManager().supportsCalendaringBatch());
    }


    /**
     *  Tests if a calendaring cycle subpackage is supported. 
     *
     *  @return <code> true </code> if a calendar cycle package is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringCycle() {
        return (getAdapteeManager().supportsCalendaringCycle());
    }


    /**
     *  Tests if a calendaring rules subpackage is supported. 
     *
     *  @return <code> true </code> if a calendar rules package is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringRules() {
        return (getAdapteeManager().supportsCalendaringRules());
    }


    /**
     *  Gets the supported <code> Event </code> record types. 
     *
     *  @return a list containing the supported <code> Event </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEventRecordTypes() {
        return (getAdapteeManager().getEventRecordTypes());
    }


    /**
     *  Tests if the given <code> Event </code> record type is supported. 
     *
     *  @param  eventRecordType a <code> Type </code> indicating an <code> 
     *          Event </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> eventRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEventRecordType(org.osid.type.Type eventRecordType) {
        return (getAdapteeManager().supportsEventRecordType(eventRecordType));
    }


    /**
     *  Gets the supported <code> Event </code> search record types. 
     *
     *  @return a list containing the supported <code> Event </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEventSearchRecordTypes() {
        return (getAdapteeManager().getEventSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Event </code> search record type is 
     *  supported. 
     *
     *  @param  eventSearchRecordType a <code> Type </code> indicating an 
     *          <code> Event </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> eventSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEventSearchRecordType(org.osid.type.Type eventSearchRecordType) {
        return (getAdapteeManager().supportsEventSearchRecordType(eventSearchRecordType));
    }


    /**
     *  Gets the supported <code> RecurringEvent </code> record types. 
     *
     *  @return a list containing the supported <code> RecurringEvent </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecurringEventRecordTypes() {
        return (getAdapteeManager().getRecurringEventRecordTypes());
    }


    /**
     *  Tests if the given <code> RecurringEvent </code> record type is 
     *  supported. 
     *
     *  @param  recurringEventRecordType a <code> Type </code> indicating a 
     *          <code> RecurringEvent </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> recurringEventRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecurringEventRecordType(org.osid.type.Type recurringEventRecordType) {
        return (getAdapteeManager().supportsRecurringEventRecordType(recurringEventRecordType));
    }


    /**
     *  Gets the supported <code> RecurringEvent </code> search record types. 
     *
     *  @return a list containing the supported <code> RecurringEvent </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecurringEventSearchRecordTypes() {
        return (getAdapteeManager().getRecurringEventSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> RecurringEvent </code> search record type is 
     *  supported. 
     *
     *  @param  recurringEventSearchRecordType a <code> Type </code> 
     *          indicating a <code> RecurringEvent </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecurringEventSearchRecordType(org.osid.type.Type recurringEventSearchRecordType) {
        return (getAdapteeManager().supportsRecurringEventSearchRecordType(recurringEventSearchRecordType));
    }


    /**
     *  Gets the supported <code> Superseding </code> record types. 
     *
     *  @return a list containing the supported <code> SupersedingEvent 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSupersedingEventRecordTypes() {
        return (getAdapteeManager().getSupersedingEventRecordTypes());
    }


    /**
     *  Tests if the given <code> SupersedingEvent </code> record type is 
     *  supported. 
     *
     *  @param  supersedingEventRecordType a <code> Type </code> indicating a 
     *          <code> SupersedingEvent </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSupersedingEventRecordType(org.osid.type.Type supersedingEventRecordType) {
        return (getAdapteeManager().supportsSupersedingEventRecordType(supersedingEventRecordType));
    }


    /**
     *  Gets the supported <code> SupersedingEvent </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> SupersedingEvent 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSupersedingEventSearchRecordTypes() {
        return (getAdapteeManager().getSupersedingEventSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> SupersedingEvent </code> search record type 
     *  is supported. 
     *
     *  @param  supersedingEventSearchRecordType a <code> Type </code> 
     *          indicating a <code> SupersedingEvent </code> search record 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSupersedingEventSearchRecordType(org.osid.type.Type supersedingEventSearchRecordType) {
        return (getAdapteeManager().supportsSupersedingEventSearchRecordType(supersedingEventSearchRecordType));
    }


    /**
     *  Gets the supported <code> OffsetEvent </code> record types. 
     *
     *  @return a list containing the supported <code> OffsetEvent </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOffsetEventRecordTypes() {
        return (getAdapteeManager().getOffsetEventRecordTypes());
    }


    /**
     *  Tests if the given <code> OffsetEvent </code> record type is 
     *  supported. 
     *
     *  @param  offsetEventRecordType a <code> Type </code> indicating a 
     *          <code> OffsetEvent </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> offsetEventRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOffsetEventRecordType(org.osid.type.Type offsetEventRecordType) {
        return (getAdapteeManager().supportsOffsetEventRecordType(offsetEventRecordType));
    }


    /**
     *  Gets the supported <code> OffsetEvent </code> search record types. 
     *
     *  @return a list containing the supported <code> OffsetEvent </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOffsetEventSearchRecordTypes() {
        return (getAdapteeManager().getOffsetEventSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> OffsetEvent </code> search record type is 
     *  supported. 
     *
     *  @param  offsetEventSearchRecordType a <code> Type </code> indicating a 
     *          <code> OffsetEvent </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOffsetEventSearchRecordType(org.osid.type.Type offsetEventSearchRecordType) {
        return (getAdapteeManager().supportsOffsetEventSearchRecordType(offsetEventSearchRecordType));
    }


    /**
     *  Gets the supported <code> Schedule </code> record types. 
     *
     *  @return a list containing the supported <code> Schedule </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getScheduleRecordTypes() {
        return (getAdapteeManager().getScheduleRecordTypes());
    }


    /**
     *  Tests if the given <code> Schedule </code> record type is supported. 
     *
     *  @param  scheduleRecordType a <code> Type </code> indicating a <code> 
     *          Schedule </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> scheduleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsScheduleRecordType(org.osid.type.Type scheduleRecordType) {
        return (getAdapteeManager().supportsScheduleRecordType(scheduleRecordType));
    }


    /**
     *  Gets the supported <code> Schedule </code> search record types. 
     *
     *  @return a list containing the supported <code> Schedule </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getScheduleSearchRecordTypes() {
        return (getAdapteeManager().getScheduleSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Schedule </code> search record type is 
     *  supported. 
     *
     *  @param  scheduleSearchRecordType a <code> Type </code> indicating a 
     *          <code> Schedule </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> scheduleSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsScheduleSearchRecordType(org.osid.type.Type scheduleSearchRecordType) {
        return (getAdapteeManager().supportsScheduleSearchRecordType(scheduleSearchRecordType));
    }


    /**
     *  Gets the supported <code> ScheduleSlot </code> record types. 
     *
     *  @return a list containing the supported <code> ScheduleSlot </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getScheduleSlotRecordTypes() {
        return (getAdapteeManager().getScheduleSlotRecordTypes());
    }


    /**
     *  Tests if the given <code> ScheduleSlot </code> record type is 
     *  supported. 
     *
     *  @param  scheduleSlotRecordType a <code> Type </code> indicating a 
     *          <code> ScheduleSlot </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsScheduleSlotRecordType(org.osid.type.Type scheduleSlotRecordType) {
        return (getAdapteeManager().supportsScheduleSlotRecordType(scheduleSlotRecordType));
    }


    /**
     *  Gets the supported <code> ScheduleSlot </code> search record types. 
     *
     *  @return a list containing the supported <code> ScheduleSlot </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getScheduleSlotSearchRecordTypes() {
        return (getAdapteeManager().getScheduleSlotSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ScheduleSlot </code> search record type is 
     *  supported. 
     *
     *  @param  scheduleSlotSearchRecordType a <code> Type </code> indicating 
     *          a <code> ScheduleSlot </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          scheduleSlotSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsScheduleSlotSearchRecordType(org.osid.type.Type scheduleSlotSearchRecordType) {
        return (getAdapteeManager().supportsScheduleSlotSearchRecordType(scheduleSlotSearchRecordType));
    }


    /**
     *  Gets the supported <code> TimePeriod </code> record types. 
     *
     *  @return a list containing the supported <code> TimePeriod </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimePeriodRecordTypes() {
        return (getAdapteeManager().getTimePeriodRecordTypes());
    }


    /**
     *  Tests if the given <code> TimePeriod </code> record type is supported. 
     *
     *  @param  timePeriodRecordType a <code> Type </code> indicating a <code> 
     *          TimePeriod </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> timePeriodRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimePeriodRecordType(org.osid.type.Type timePeriodRecordType) {
        return (getAdapteeManager().supportsTimePeriodRecordType(timePeriodRecordType));
    }


    /**
     *  Gets the supported <code> TimePeriod </code> search record types. 
     *
     *  @return a list containing the supported <code> TimePeriod </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimePeriodSearchRecordTypes() {
        return (getAdapteeManager().getTimePeriodSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> TimePeriod </code> search record type is 
     *  supported. 
     *
     *  @param  timePeriodSearchRecordType a <code> Type </code> indicating a 
     *          <code> TimePeriod </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          timePeriodSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearchRecordType(org.osid.type.Type timePeriodSearchRecordType) {
        return (getAdapteeManager().supportsTimePeriodSearchRecordType(timePeriodSearchRecordType));
    }


    /**
     *  Gets the supported <code> Commitment </code> record types. 
     *
     *  @return a list containing the supported <code> Commitment </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommitmentRecordTypes() {
        return (getAdapteeManager().getCommitmentRecordTypes());
    }


    /**
     *  Tests if the given <code> Commitment </code> record type is supported. 
     *
     *  @param  commitmentRecordType a <code> Type </code> indicating a <code> 
     *          Commitment </code> type 
     *  @return <code> true </code> if the given commitment record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> commitmentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommitmentRecordType(org.osid.type.Type commitmentRecordType) {
        return (getAdapteeManager().supportsCommitmentRecordType(commitmentRecordType));
    }


    /**
     *  Gets the supported commitment search record types. 
     *
     *  @return a list containing the supported <code> Commitment </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommitmentSearchRecordTypes() {
        return (getAdapteeManager().getCommitmentSearchRecordTypes());
    }


    /**
     *  Tests if the given commitment search record type is supported. 
     *
     *  @param  commitmentSearchRecordType a <code> Type </code> indicating a 
     *          <code> Commitment </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommitmentSearchRecordType(org.osid.type.Type commitmentSearchRecordType) {
        return (getAdapteeManager().supportsCommitmentSearchRecordType(commitmentSearchRecordType));
    }


    /**
     *  Gets the supported <code> Calendar </code> record types. 
     *
     *  @return a list containing the supported <code> Calendar </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarRecordTypes() {
        return (getAdapteeManager().getCalendarRecordTypes());
    }


    /**
     *  Tests if the given <code> Calendar </code> record type is supported. 
     *
     *  @param  calendarRecordType a <code> Type </code> indicating a <code> 
     *          Calendar </code> type 
     *  @return <code> true </code> if the given calendar record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> calendarRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarRecordType(org.osid.type.Type calendarRecordType) {
        return (getAdapteeManager().supportsCalendarRecordType(calendarRecordType));
    }


    /**
     *  Gets the supported calendar search record types. 
     *
     *  @return a list containing the supported <code> Calendar </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarSearchRecordTypes() {
        return (getAdapteeManager().getCalendarSearchRecordTypes());
    }


    /**
     *  Tests if the given calendar search record type is supported. 
     *
     *  @param  calendarSearchRecordType a <code> Type </code> indicating a 
     *          <code> Calendar </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> calendarSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarSearchRecordType(org.osid.type.Type calendarSearchRecordType) {
        return (getAdapteeManager().supportsCalendarSearchRecordType(calendarSearchRecordType));
    }


    /**
     *  Gets all the spatial unit record types supported. 
     *
     *  @return the list of supported spatial unit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpatialUnitRecordTypes() {
        return (getAdapteeManager().getSpatialUnitRecordTypes());
    }


    /**
     *  Tests if a given spatial unit record type is supported. 
     *
     *  @param  spatialUnitRecordType the spatial unit record type 
     *  @return <code> true </code> if the spatial unit record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        return (getAdapteeManager().supportsSpatialUnitRecordType(spatialUnitRecordType));
    }


    /**
     *  Gets all the coordinate record types supported. 
     *
     *  @return the list of supported coordinate record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateRecordTypes() {
        return (getAdapteeManager().getCoordinateRecordTypes());
    }


    /**
     *  Tests if a given coordinate record type is supported. 
     *
     *  @param  coordinateRecordType the coordinate domain type 
     *  @return <code> true </code> if the coordinate record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> coordinateRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateRecordType(org.osid.type.Type coordinateRecordType) {
        return (getAdapteeManager().supportsCoordinateRecordType(coordinateRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEventLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEventQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchSession getEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event search 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchSession getEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEventSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventAdminSession getEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event admin 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventAdminSession getEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEventAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to event 
     *  changes. 
     *
     *  @param  eventReceiver the event receiver 
     *  @param  proxy a proxy 
     *  @return an <code> EventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> eventReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventNotificationSession getEventNotificationSession(org.osid.calendaring.EventReceiver eventReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventNotificationSession(eventReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event 
     *  notification service for the given calendar. 
     *
     *  @param  eventReceiver the event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> an EventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> eventReceiver, </code> 
     *          <code> calendarId </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventNotificationSession getEventNotificationSessionForCalendar(org.osid.calendaring.EventReceiver eventReceiver, 
                                                                                                org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEventNotificationSessionForCalendar(eventReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventCalendar() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventCalendarSession getEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventCalendarAssignmentSession getEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the event smart calendar for the 
     *  given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventSmartCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSmartCalendarSession getEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEventSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventLookupSession getRecurringEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventLookupSession getRecurringEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuerySession getRecurringEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuerySession getRecurringEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSearchSession getRecurringEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSearchSession getRecurringEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventAdminSession getRecurringEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventAdminSession getRecurringEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  recurring event changes. 
     *
     *  @param  recurringEventReceiver the recurring event receiver 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> recurringEventReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventNotificationSession getRecurringEventNotificationSession(org.osid.calendaring.RecurringEventReceiver recurringEventReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventNotificationSession(recurringEventReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event notification service for the given calendar. 
     *
     *  @param  recurringEventReceiver the recurring event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> recurringEventReceiver, 
     *          </code> <code> calendarId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventNotificationSession getRecurringEventNotificationSessionForCalendar(org.osid.calendaring.RecurringEventReceiver recurringEventReceiver, 
                                                                                                                  org.osid.id.Id calendarId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventNotificationSessionForCalendar(recurringEventReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving recurring event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventCalendarSession getRecurringEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning recurring event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventCalendarAssignmentSession getRecurringEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the recurring event smart calendar 
     *  for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSmartCalendarSession getRecurringEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventLookupSession getSupersedingEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventLookupSession getSupersedingEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuerySession getSupersedingEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuerySession getSupersedingEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSearchSession getSupersedingEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSearchSession getSupersedingEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventAdminSession getSupersedingEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventAdminSession getSupersedingEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  superseding event changes. 
     *
     *  @param  supersedingEventReceiver the superseding event receiver 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> supersedingEventReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventNotificationSession getSupersedingEventNotificationSession(org.osid.calendaring.SupersedingEventReceiver supersedingEventReceiver, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventNotificationSession(supersedingEventReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event notification service for the given calendar. 
     *
     *  @param  supersedingEventReceiver the superseding event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventReceiver, calendarId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventNotificationSession getSupersedingEventNotificationSessionForCalendar(org.osid.calendaring.SupersedingEventReceiver supersedingEventReceiver, 
                                                                                                                      org.osid.id.Id calendarId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventNotificationSessionForCalendar(supersedingEventReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving superseding event to calendar 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventCalendarSession getSupersedingEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning superseding event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventCalendarAssignmentSession getSupersedingEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the superseding event smart calendar 
     *  for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSmartCalendarSession getSupersedingEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventLookupSession getOffsetEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventLookupSession getOffsetEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuerySession getOffsetEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuerySession getOffsetEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSearchSession getOffsetEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSearchSession getOffsetEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventAdminSession getOffsetEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventAdminSession getOffsetEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to offset 
     *  event changes. 
     *
     *  @param  offsetEventReceiver the offset event receiver 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> offsetEventReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventNotificationSession getOffsetEventNotificationSession(org.osid.calendaring.OffsetEventReceiver offsetEventReceiver, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventNotificationSession(offsetEventReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  notification service for the given calendar. 
     *
     *  @param  offsetEventReceiver the offset event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> offsetEventReceiver, 
     *          calendarId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventNotificationSession getOffsetEventNotificationSessionForCalendar(org.osid.calendaring.OffsetEventReceiver offsetEventReceiver, 
                                                                                                            org.osid.id.Id calendarId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventNotificationSessionForCalendar(offsetEventReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving offset event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventCalendarSession getOffsetEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning offset event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventCalendarAssignmentSession getOffsetEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the offset event smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSmartCalendarSession getOffsetEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleLookupSession getScheduleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleLookupSession getScheduleLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuerySession getScheduleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule query 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuerySession getScheduleQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSearchSession getScheduleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSearchSession getScheduleSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleAdminSession getScheduleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule admin 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleAdminSession getScheduleAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to schedule 
     *  changes. 
     *
     *  @param  scheduleReceiver the schedule receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> scheduleReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleNotificationSession getScheduleNotificationSession(org.osid.calendaring.ScheduleReceiver scheduleReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleNotificationSession(scheduleReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  notification service for the given calendar. 
     *
     *  @param  scheduleReceiver the schedule receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> scheduleReceiver, 
     *          calendarId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleNotificationSession getScheduleNotificationSessionForCalendar(org.osid.calendaring.ScheduleReceiver scheduleReceiver, 
                                                                                                      org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleNotificationSessionForCalendar(scheduleReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving schedule to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleCalendarSession getScheduleCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning schedule to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleCalendarAssignmentSession getScheduleCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the schedule smart calendar for the 
     *  given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSmartCalendarSession getScheduleSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotLookupSession getScheduleSlotLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotLookupSession getScheduleSlotLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuerySession getScheduleSlotQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuerySession getScheduleSlotQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSearchSession getScheduleSlotSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSearchSession getScheduleSlotSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotAdminSession getScheduleSlotAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotAdminSession getScheduleSlotAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to schedule 
     *  slot changes. 
     *
     *  @param  scheduleSlotReceiver the schedule slot receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotNotificationSession getScheduleSlotNotificationSession(org.osid.calendaring.ScheduleSlotReceiver scheduleSlotReceiver, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotNotificationSession(scheduleSlotReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  notification service for the given calendar. 
     *
     *  @param  scheduleSlotReceiver the schedule slot receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotReceiver, 
     *          calendarId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotNotificationSession getScheduleSlotNotificationSessionForCalendar(org.osid.calendaring.ScheduleSlotReceiver scheduleSlotReceiver, 
                                                                                                              org.osid.id.Id calendarId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotNotificationSessionForCalendar(scheduleSlotReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving schedule slot to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotCalendarSession getScheduleSlotCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning schedule slot to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotCalendarAssignmentSession getScheduleSlotCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the schedule slot smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSmartCalendarSession getScheduleSlotSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentLookupSession getCommitmentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> a CommitmentLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentLookupSession getCommitmentLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuerySession getCommitmentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuerySession getCommitmentQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentSearchSession getCommitmentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentSearchSession getCommitmentSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentAdminSession getCommitmentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmenttAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentAdminSession getCommitmentAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  commitment changes. 
     *
     *  @param  commitmentReceiver the commitment receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> commitmentReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentNotificationSession getCommitmentNotificationSession(org.osid.calendaring.CommitmentReceiver commitmentReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentNotificationSession(commitmentReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  notification service for the given calendar. 
     *
     *  @param  commitmentReceiver the commitment receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> commitmentReceiver, 
     *          calendarId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentNotificationSession getCommitmentNotificationSessionForCalendar(org.osid.calendaring.CommitmentReceiver commitmentReceiver, 
                                                                                                          org.osid.id.Id calendarId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentNotificationSessionForCalendar(commitmentReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving commitment to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentCalendarSession getCommitmentCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning commitment to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentCalendarAssignmentSession getCommitmentCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the commitment smart calendar for the 
     *  given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentSmartCalendarSession getCommitmentSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodLookupSession getTimePeriodLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodLookupSession getTimePeriodLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuerySession getTimePeriodQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuerySession getTimePeriodQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchSession getTimePeriodSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchSession getTimePeriodSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodAdminSession getTimePeriodAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> calendarId </code> not found 
     *  @throws org.osid.NotFoundException a <code> TimePeriodAdminSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodAdminSession getTimePeriodAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to time 
     *  period changes. 
     *
     *  @param  timePeriodReceiver the time period receiver 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> timePeriodReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodNotificationSession getTimePeriodNotificationSession(org.osid.calendaring.TimePeriodReceiver timePeriodReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodNotificationSession(timePeriodReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  notification service for the given calendar. 
     *
     *  @param  timePeriodReceiver the time period receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> a TimePeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> timePeriodReceiver 
     *          </code> or <code> calendarId </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodNotificationSession getTimePeriodNotificationSessionForCalendar(org.osid.calendaring.TimePeriodReceiver timePeriodReceiver, 
                                                                                                          org.osid.id.Id calendarId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodNotificationSessionForCalendar(timePeriodReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving time period to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodCalendarSession getTimePeriodCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning time period to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodCalendarAssignmentSession getTimePeriodCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the time period smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSmartCalendarSession getTimePeriodSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the OsidSession associated with the calendar lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarLookupSession getCalendarLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarLookupSession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the calendar search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarSearch() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarSearchSession getCalendarSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarSearchSession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the calendar administration 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarAdmin() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarAdminSession getCalendarAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarAdminSession(proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to calendar 
     *  service changes. 
     *
     *  @param  calendarReceiver the calendar receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CalendarNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarNotificationSession getCalendarNotificationSession(org.osid.calendaring.CalendarReceiver calendarReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarNotificationSession(calendarReceiver, proxy));
    }


    /**
     *  Gets the session traversing calendar hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarHierarchySession getCalendarHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarHierarchySession(proxy));
    }


    /**
     *  Gets the session designing calendar hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarHierarchyDesignSession getCalendarHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the calendaring batch proxy manager. 
     *
     *  @return a <code> CalendaringBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.CalendaringBatchProxyManager getCalandaringBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalandaringBatchProxyManager());
    }


    /**
     *  Gets the calendaring cycle proxy manager. 
     *
     *  @return a <code> CalendaringCycleProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringCycle() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CalendaringCycleProxyManager getCalandaringCycleProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalandaringCycleProxyManager());
    }


    /**
     *  Gets the calendaring rules proxy manager. 
     *
     *  @return a <code> CalendaringRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CalendaringRulesProxyManager getCalandaringRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalandaringRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
