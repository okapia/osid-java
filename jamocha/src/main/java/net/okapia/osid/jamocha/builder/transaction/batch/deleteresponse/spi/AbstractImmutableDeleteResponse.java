//
// AbstractImmutableDeleteResponse.java
//
//     Wraps a mutable DeleteResponse to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.transaction.batch.deleteresponse.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>DeleteResponse</code> to hide modifiers. This
 *  wrapper provides an immutized DeleteResponse from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying deleteResponse whose state changes are visible.
 */

public abstract class AbstractImmutableDeleteResponse
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutable
    implements org.osid.transaction.batch.DeleteResponse {

    private final org.osid.transaction.batch.DeleteResponse deleteResponse;


    /**
     *  Constructs a new <code>AbstractImmutableDeleteResponse</code>.
     *
     *  @param deleteResponse the delete response to immutablize
     *  @throws org.osid.NullArgumentException <code>deleteResponse</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDeleteResponse(org.osid.transaction.batch.DeleteResponse deleteResponse) {
        super(deleteResponse);
        this.deleteResponse = deleteResponse;
        return;
    }


    /**
     *  Gets the reference <code> Id </code> of the object on which this item 
     *  within the delete operation operated. 
     *
     *  @return the reference object <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDeletedId() {
        return (this.deleteResponse.getDeletedId());
    }


    /**
     *  Tests if this item within the delete operation was successful. 
     *
     *  @return <code> true </code> if the delete operation was successful, 
     *          <code> false </code> if it was not successful 
     */

    @OSID @Override
    public boolean isSuccessful() {
        return (this.deleteResponse.isSuccessful());
    }


    /**
     *  Gets the error message for an unsuccessful item within the delete 
     *  operation. 
     *
     *  @return the message 
     *  @throws org.osid.IllegalStateException <code> isSuccessful() </code> 
     *          is <code> true </code> 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getErrorMessage() {
        return (this.deleteResponse.getErrorMessage());
    }
}

