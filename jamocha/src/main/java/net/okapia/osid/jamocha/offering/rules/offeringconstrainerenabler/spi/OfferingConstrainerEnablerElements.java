//
// OfferingConstrainerEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainerenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class OfferingConstrainerEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the OfferingConstrainerEnablerElement Id.
     *
     *  @return the offering constrainer enabler element Id
     */

    public static org.osid.id.Id getOfferingConstrainerEnablerEntityId() {
        return (makeEntityId("osid.offering.rules.OfferingConstrainerEnabler"));
    }


    /**
     *  Gets the RuledOfferingConstrainerId element Id.
     *
     *  @return the RuledOfferingConstrainerId element Id
     */

    public static org.osid.id.Id getRuledOfferingConstrainerId() {
        return (makeQueryElementId("osid.offering.rules.offeringconstrainerenabler.RuledOfferingConstrainerId"));
    }


    /**
     *  Gets the RuledOfferingConstrainer element Id.
     *
     *  @return the RuledOfferingConstrainer element Id
     */

    public static org.osid.id.Id getRuledOfferingConstrainer() {
        return (makeQueryElementId("osid.offering.rules.offeringconstrainerenabler.RuledOfferingConstrainer"));
    }


    /**
     *  Gets the CatalogueId element Id.
     *
     *  @return the CatalogueId element Id
     */

    public static org.osid.id.Id getCatalogueId() {
        return (makeQueryElementId("osid.offering.rules.offeringconstrainerenabler.CatalogueId"));
    }


    /**
     *  Gets the Catalogue element Id.
     *
     *  @return the Catalogue element Id
     */

    public static org.osid.id.Id getCatalogue() {
        return (makeQueryElementId("osid.offering.rules.offeringconstrainerenabler.Catalogue"));
    }
}
