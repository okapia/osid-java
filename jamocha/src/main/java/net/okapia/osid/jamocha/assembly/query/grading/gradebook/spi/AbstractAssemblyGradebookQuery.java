//
// AbstractAssemblyGradebookQuery.java
//
//     A GradebookQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.grading.gradebook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A GradebookQuery that stores terms.
 */

public abstract class AbstractAssemblyGradebookQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.grading.GradebookQuery,
               org.osid.grading.GradebookQueryInspector,
               org.osid.grading.GradebookSearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradebookQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradebookQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradebookSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyGradebookQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyGradebookQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getGradeSystemIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeSystemIdTerms() {
        getAssembler().clearTerms(getGradeSystemIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeSystemIdTerms() {
        return (getAssembler().getIdTerms(getGradeSystemIdColumn()));
    }


    /**
     *  Gets the GradeSystemId column name.
     *
     * @return the column name
     */

    protected String getGradeSystemIdColumn() {
        return ("grade_system_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradeSystemQuery() {
        throw new org.osid.UnimplementedException("supportsGradeSystemQuery() is false");
    }


    /**
     *  Matches gradebooks that have any grade system. 
     *
     *  @param  match <code> true </code> to match gradebooks with any grade 
     *          system, <code> false </code> to match gradebooks with no grade 
     *          system 
     */

    @OSID @Override
    public void matchAnyGradeSystem(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeSystemColumn(), match);
        return;
    }


    /**
     *  Clears the grade system terms. 
     */

    @OSID @Override
    public void clearGradeSystemTerms() {
        getAssembler().clearTerms(getGradeSystemColumn());
        return;
    }


    /**
     *  Gets the grade system terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradeSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the GradeSystem column name.
     *
     * @return the column name
     */

    protected String getGradeSystemColumn() {
        return ("grade_system");
    }


    /**
     *  Sets the grade entry <code> Id </code> for this query. 
     *
     *  @param  gradeEntryId a grade entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeEntryId(org.osid.id.Id gradeEntryId, boolean match) {
        getAssembler().addIdTerm(getGradeEntryIdColumn(), gradeEntryId, match);
        return;
    }


    /**
     *  Clears the grade entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeEntryIdTerms() {
        getAssembler().clearTerms(getGradeEntryIdColumn());
        return;
    }


    /**
     *  Gets the grade entry <code> Id </code> terms. 
     *
     *  @return the grade entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeEntryIdTerms() {
        return (getAssembler().getIdTerms(getGradeEntryIdColumn()));
    }


    /**
     *  Gets the GradeEntryId column name.
     *
     * @return the column name
     */

    protected String getGradeEntryIdColumn() {
        return ("grade_entry_id");
    }


    /**
     *  Tests if a <code> GradeEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuery getGradeEntryQuery() {
        throw new org.osid.UnimplementedException("supportsGradeEntryQuery() is false");
    }


    /**
     *  Matches gradebooks that have any grade entry. 
     *
     *  @param  match <code> true </code> to match gradebooks with any grade 
     *          entry, <code> false </code> to match gradebooks with no grade 
     *          entry 
     */

    @OSID @Override
    public void matchAnyGradeEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeEntryColumn(), match);
        return;
    }


    /**
     *  Clears the grade entry terms. 
     */

    @OSID @Override
    public void clearGradeEntryTerms() {
        getAssembler().clearTerms(getGradeEntryColumn());
        return;
    }


    /**
     *  Gets the grade entry terms. 
     *
     *  @return the grade entry terms 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQueryInspector[] getGradeEntryTerms() {
        return (new org.osid.grading.GradeEntryQueryInspector[0]);
    }


    /**
     *  Gets the GradeEntry column name.
     *
     * @return the column name
     */

    protected String getGradeEntryColumn() {
        return ("grade_entry");
    }


    /**
     *  Sets the gradebook column <code> Id </code> for this query. 
     *
     *  @param  gradebookColumnId a gradebook column <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookColumnId(org.osid.id.Id gradebookColumnId, 
                                       boolean match) {
        getAssembler().addIdTerm(getGradebookColumnIdColumn(), gradebookColumnId, match);
        return;
    }


    /**
     *  Clears the gradebook column <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookColumnIdTerms() {
        getAssembler().clearTerms(getGradebookColumnIdColumn());
        return;
    }


    /**
     *  Gets the gradebook column <code> Id </code> terms. 
     *
     *  @return the gradebook column <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookColumnIdTerms() {
        return (getAssembler().getIdTerms(getGradebookColumnIdColumn()));
    }


    /**
     *  Gets the GradebookColumnId column name.
     *
     * @return the column name
     */

    protected String getGradebookColumnIdColumn() {
        return ("gradebook_column_id");
    }


    /**
     *  Tests if a <code> GradebookColumnQuery </code> is available. 
     *
     *  @return <code> true </code> if a gradebook column query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook column. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the gradebook column query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuery getGradebookColumnQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnQuery() is false");
    }


    /**
     *  Matches gradebooks that have any column. 
     *
     *  @param  match <code> true </code> to match gradebooks with any column, 
     *          <code> false </code> to match gradebooks with no column 
     */

    @OSID @Override
    public void matchAnyGradebookColumn(boolean match) {
        getAssembler().addIdWildcardTerm(getGradebookColumnColumn(), match);
        return;
    }


    /**
     *  Clears the gradebook column terms. 
     */

    @OSID @Override
    public void clearGradebookColumnTerms() {
        getAssembler().clearTerms(getGradebookColumnColumn());
        return;
    }


    /**
     *  Gets the gradebook column terms. 
     *
     *  @return the gradebook column terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQueryInspector[] getGradebookColumnTerms() {
        return (new org.osid.grading.GradebookColumnQueryInspector[0]);
    }


    /**
     *  Gets the GradebookColumn column name.
     *
     * @return the column name
     */

    protected String getGradebookColumnColumn() {
        return ("gradebook_column");
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query to match 
     *  gradebooks that have the specified gradebook as an ancestor. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorGradebookId(org.osid.id.Id gradebookId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAncestorGradebookIdColumn(), gradebookId, match);
        return;
    }


    /**
     *  Clears the ancestor gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorGradebookIdTerms() {
        getAssembler().clearTerms(getAncestorGradebookIdColumn());
        return;
    }


    /**
     *  Gets the ancestor gradebook <code> Id </code> terms. 
     *
     *  @return the ancestor gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorGradebookIdTerms() {
        return (getAssembler().getIdTerms(getAncestorGradebookIdColumn()));
    }


    /**
     *  Gets the AncestorGradebookId column name.
     *
     * @return the column name
     */

    protected String getAncestorGradebookIdColumn() {
        return ("ancestor_gradebook_id");
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available. 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorGradebookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getAncestorGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorGradebookQuery() is false");
    }


    /**
     *  Matches gradebook with any ancestor. 
     *
     *  @param  match <code> true </code> to match gradebooks with any 
     *          ancestor, <code> false </code> to match root gradebooks 
     */

    @OSID @Override
    public void matchAnyAncestorGradebook(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorGradebookColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor gradebook terms. 
     */

    @OSID @Override
    public void clearAncestorGradebookTerms() {
        getAssembler().clearTerms(getAncestorGradebookColumn());
        return;
    }


    /**
     *  Gets the ancestor gradebook terms. 
     *
     *  @return the ancestor gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getAncestorGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }


    /**
     *  Gets the AncestorGradebook column name.
     *
     * @return the column name
     */

    protected String getAncestorGradebookColumn() {
        return ("ancestor_gradebook");
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query to match 
     *  gradebooks that have the specified gradebook as a descendant. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantGradebookId(org.osid.id.Id gradebookId, 
                                           boolean match) {
        getAssembler().addIdTerm(getDescendantGradebookIdColumn(), gradebookId, match);
        return;
    }


    /**
     *  Clears the descendant gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantGradebookIdTerms() {
        getAssembler().clearTerms(getDescendantGradebookIdColumn());
        return;
    }


    /**
     *  Gets the descendant gradebook <code> Id </code> terms. 
     *
     *  @return the descendant gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantGradebookIdTerms() {
        return (getAssembler().getIdTerms(getDescendantGradebookIdColumn()));
    }


    /**
     *  Gets the DescendantGradebookId column name.
     *
     * @return the column name
     */

    protected String getDescendantGradebookIdColumn() {
        return ("descendant_gradebook_id");
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available. 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantGradebookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getDescendantGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantGradebookQuery() is false");
    }


    /**
     *  Matches gradebook with any descendant. 
     *
     *  @param  match <code> true </code> to match gradebooks with any 
     *          descendant, <code> false </code> to match leaf gradebooks 
     */

    @OSID @Override
    public void matchAnyDescendantGradebook(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantGradebookColumn(), match);
        return;
    }


    /**
     *  Clears the descendant gradebook terms. 
     */

    @OSID @Override
    public void clearDescendantGradebookTerms() {
        getAssembler().clearTerms(getDescendantGradebookColumn());
        return;
    }


    /**
     *  Gets the descendant gradebook terms. 
     *
     *  @return the descendant gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getDescendantGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }


    /**
     *  Gets the DescendantGradebook column name.
     *
     * @return the column name
     */

    protected String getDescendantGradebookColumn() {
        return ("descendant_gradebook");
    }


    /**
     *  Tests if this gradebook supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradebookRecordType a gradebook record type 
     *  @return <code>true</code> if the gradebookRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradebookRecordType) {
        for (org.osid.grading.records.GradebookQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradebookRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  gradebookRecordType the gradebook record type 
     *  @return the gradebook query record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookQueryRecord getGradebookQueryRecord(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradebookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  gradebookRecordType the gradebook record type 
     *  @return the gradebook query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookQueryInspectorRecord getGradebookQueryInspectorRecord(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(gradebookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param gradebookRecordType the gradebook record type
     *  @return the gradebook search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookSearchOrderRecord getGradebookSearchOrderRecord(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(gradebookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this gradebook. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradebookQueryRecord the gradebook query record
     *  @param gradebookQueryInspectorRecord the gradebook query inspector
     *         record
     *  @param gradebookSearchOrderRecord the gradebook search order record
     *  @param gradebookRecordType gradebook record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookQueryRecord</code>,
     *          <code>gradebookQueryInspectorRecord</code>,
     *          <code>gradebookSearchOrderRecord</code> or
     *          <code>gradebookRecordTypegradebook</code> is
     *          <code>null</code>
     */
            
    protected void addGradebookRecords(org.osid.grading.records.GradebookQueryRecord gradebookQueryRecord, 
                                      org.osid.grading.records.GradebookQueryInspectorRecord gradebookQueryInspectorRecord, 
                                      org.osid.grading.records.GradebookSearchOrderRecord gradebookSearchOrderRecord, 
                                      org.osid.type.Type gradebookRecordType) {

        addRecordType(gradebookRecordType);

        nullarg(gradebookQueryRecord, "gradebook query record");
        nullarg(gradebookQueryInspectorRecord, "gradebook query inspector record");
        nullarg(gradebookSearchOrderRecord, "gradebook search odrer record");

        this.queryRecords.add(gradebookQueryRecord);
        this.queryInspectorRecords.add(gradebookQueryInspectorRecord);
        this.searchOrderRecords.add(gradebookSearchOrderRecord);
        
        return;
    }
}
