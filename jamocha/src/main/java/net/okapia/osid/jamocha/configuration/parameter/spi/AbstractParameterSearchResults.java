//
// AbstractParameterSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.parameter.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractParameterSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.configuration.ParameterSearchResults {

    private org.osid.configuration.ParameterList parameters;
    private final org.osid.configuration.ParameterQueryInspector inspector;
    private final java.util.Collection<org.osid.configuration.records.ParameterSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractParameterSearchResults.
     *
     *  @param parameters the result set
     *  @param parameterQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>parameters</code>
     *          or <code>parameterQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractParameterSearchResults(org.osid.configuration.ParameterList parameters,
                                            org.osid.configuration.ParameterQueryInspector parameterQueryInspector) {
        nullarg(parameters, "parameters");
        nullarg(parameterQueryInspector, "parameter query inspectpr");

        this.parameters = parameters;
        this.inspector = parameterQueryInspector;

        return;
    }


    /**
     *  Gets the parameter list resulting from a search.
     *
     *  @return a parameter list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParameters() {
        if (this.parameters == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.configuration.ParameterList parameters = this.parameters;
        this.parameters = null;
	return (parameters);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.configuration.ParameterQueryInspector getParameterQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  parameter search record <code> Type. </code> This method must
     *  be used to retrieve a parameter implementing the requested
     *  record.
     *
     *  @param parameterSearchRecordType a parameter search 
     *         record type 
     *  @return the parameter search
     *  @throws org.osid.NullArgumentException
     *          <code>parameterSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(parameterSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ParameterSearchResultsRecord getParameterSearchResultsRecord(org.osid.type.Type parameterSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.configuration.records.ParameterSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(parameterSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(parameterSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record parameter search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addParameterRecord(org.osid.configuration.records.ParameterSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "parameter record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
