//
// AbstractMapCustomerLookupSession
//
//    A simple framework for providing a Customer lookup service
//    backed by a fixed collection of customers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Customer lookup service backed by a
 *  fixed collection of customers. The customers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Customers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCustomerLookupSession
    extends net.okapia.osid.jamocha.billing.spi.AbstractCustomerLookupSession
    implements org.osid.billing.CustomerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.billing.Customer> customers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.billing.Customer>());


    /**
     *  Makes a <code>Customer</code> available in this session.
     *
     *  @param  customer a customer
     *  @throws org.osid.NullArgumentException <code>customer<code>
     *          is <code>null</code>
     */

    protected void putCustomer(org.osid.billing.Customer customer) {
        this.customers.put(customer.getId(), customer);
        return;
    }


    /**
     *  Makes an array of customers available in this session.
     *
     *  @param  customers an array of customers
     *  @throws org.osid.NullArgumentException <code>customers<code>
     *          is <code>null</code>
     */

    protected void putCustomers(org.osid.billing.Customer[] customers) {
        putCustomers(java.util.Arrays.asList(customers));
        return;
    }


    /**
     *  Makes a collection of customers available in this session.
     *
     *  @param  customers a collection of customers
     *  @throws org.osid.NullArgumentException <code>customers<code>
     *          is <code>null</code>
     */

    protected void putCustomers(java.util.Collection<? extends org.osid.billing.Customer> customers) {
        for (org.osid.billing.Customer customer : customers) {
            this.customers.put(customer.getId(), customer);
        }

        return;
    }


    /**
     *  Removes a Customer from this session.
     *
     *  @param  customerId the <code>Id</code> of the customer
     *  @throws org.osid.NullArgumentException <code>customerId<code> is
     *          <code>null</code>
     */

    protected void removeCustomer(org.osid.id.Id customerId) {
        this.customers.remove(customerId);
        return;
    }


    /**
     *  Gets the <code>Customer</code> specified by its <code>Id</code>.
     *
     *  @param  customerId <code>Id</code> of the <code>Customer</code>
     *  @return the customer
     *  @throws org.osid.NotFoundException <code>customerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>customerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer(org.osid.id.Id customerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.billing.Customer customer = this.customers.get(customerId);
        if (customer == null) {
            throw new org.osid.NotFoundException("customer not found: " + customerId);
        }

        return (customer);
    }


    /**
     *  Gets all <code>Customers</code>. In plenary mode, the returned
     *  list contains all known customers or an error
     *  results. Otherwise, the returned list may contain only those
     *  customers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Customers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.customer.ArrayCustomerList(this.customers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.customers.clear();
        super.close();
        return;
    }
}
