//
// AbstractChainSearchOdrer.java
//
//     Defines a ChainSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.chain.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ChainSearchOrder}.
 */

public abstract class AbstractChainSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.sequencing.ChainSearchOrder {

    private final java.util.Collection<org.osid.sequencing.records.ChainSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by the fifo flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFifo(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  chainRecordType a chain record type 
     *  @return {@code true} if the chainRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code chainRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type chainRecordType) {
        for (org.osid.sequencing.records.ChainSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(chainRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  chainRecordType the chain record type 
     *  @return the chain search order record
     *  @throws org.osid.NullArgumentException
     *          {@code chainRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(chainRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainSearchOrderRecord getChainSearchOrderRecord(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.ChainSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(chainRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(chainRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this chain. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param chainRecord the chain search odrer record
     *  @param chainRecordType chain record type
     *  @throws org.osid.NullArgumentException
     *          {@code chainRecord} or
     *          {@code chainRecordTypechain} is
     *          {@code null}
     */
            
    protected void addChainRecord(org.osid.sequencing.records.ChainSearchOrderRecord chainSearchOrderRecord, 
                                     org.osid.type.Type chainRecordType) {

        addRecordType(chainRecordType);
        this.records.add(chainSearchOrderRecord);
        
        return;
    }
}
