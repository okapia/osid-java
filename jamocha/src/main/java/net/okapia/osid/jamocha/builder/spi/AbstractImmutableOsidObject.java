//
// AbstractImmutableOsidObject
//
//     Defines an immutable wrapper for an OSID Object.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an immutable wrapper for an OSID Object.
 */

public abstract class AbstractImmutableOsidObject
    extends AbstractImmutableIdentifiable
    implements org.osid.OsidObject {

    private final org.osid.OsidObject object;
    private final org.osid.Extensible extensible;
    private final org.osid.Browsable browsable;


    /**
     *  Constructs a new <code>AbstractImmutableOsidObject</code>.
     *
     *  @param object
     *  @throws org.osid.NullArgumentException <code>object</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOsidObject(org.osid.OsidObject object) {
        super(object);

        this.object     = object;
        this.extensible = new ImmutableExtensible(object);
        this.browsable  = new ImmutableBrowsable(object);

        return;
    }


    /**
     *  Gets the preferred display name associated with this instance
     *  of this OSID object appropriate for display to the user.
     *
     *  @return the display name 
     *  @throws org.osid.IllegalStateException the display name has
     *          not been set
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayName() {
        return (this.object.getDisplayName());
    }


    /**
     *  Gets the description associated with this instance of this
     *  OSID object.
     *
     *  @throws org.osid.IllegalStateException the description has not
     *          been set
     *  @return the description 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDescription() {
        return (this.object.getDescription());
    }

        
    /**
     *  Gets the genus type of this object. 
     *
     *  @return the genus type of this object 
     */

    @OSID @Override
    public org.osid.type.Type getGenusType() {
        return (this.object.getGenusType());
    }


    /**
     *  Tests if this object is of the given genus <code>
     *  Type. </code> The given genus type may be supported by the
     *  object through the type hierarchy.
     *
     *  @param  genusType a genus type 
     *  @return <code> true </code> if this object is of the given
     *          genus <code> Type, </code> <code> false </code>
     *          otherwise
     *  @throws org.osid.NullArgumentException <code> genusType
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public boolean isOfGenusType(org.osid.type.Type genusType) {
        return (this.object.isOfGenusType(genusType));
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.extensible.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code> false </code>
     *  @throws org.osid.NullArgumentException <code> recordType
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.extensible.hasRecordType(recordType));
    }


    /**
     *  Gets a list of all properties of this object including those
     *  corresponding to data within this object's records. Properties
     *  provide a means for applications to display a representation
     *  of the contents of an object without understanding its record
     *  interface specifications. Applications needing to examine a
     *  specific property or perform updates should use the methods
     *  defined by the object's record <code> Type. </code>
     *
     *  @return a list of properties 
     */
    
    @OSID @Override
    public org.osid.PropertyList getProperties()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.browsable.getProperties());
    }


    /**
     *  Gets the properties by record type.
     *
     *  @param  recordType the record type corresponding to the properties set 
     *          to retrieve 
     *  @return a list of properties 
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> hasRecordType(recordType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.PropertyList getPropertiesByRecordType(org.osid.type.Type recordType)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        return (this.browsable.getPropertiesByRecordType(recordType));
    }


    protected class ImmutableExtensible
        extends AbstractImmutableExtensible
        implements org.osid.Extensible {


        /**
         *  Constructs a new <code>ImmutableExtensible</code>.
         *
         *  @param object
         *  @throws org.osid.NullArgumentException <code>object</code>
         *          is <code>null</code>
         */
        
        protected ImmutableExtensible(org.osid.Extensible object) {
            super(object);
            return;
        }
    }


    protected class ImmutableBrowsable
        extends AbstractImmutableBrowsable
        implements org.osid.Browsable {


        /**
         *  Constructs a new <code>ImmutableBrowsable</code>.
         *
         *  @param object
         *  @throws org.osid.NullArgumentException <code>object</code>
         *          is <code>null</code>
         */
        
        protected ImmutableBrowsable(org.osid.Extensible object) {
            super(object);
            return;
        }
    }
}
