//
// AbstractAssemblyBallotQuery.java
//
//     A BallotQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.ballot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BallotQuery that stores terms.
 */

public abstract class AbstractAssemblyBallotQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.voting.BallotQuery,
               org.osid.voting.BallotQueryInspector,
               org.osid.voting.BallotSearchOrder {

    private final java.util.Collection<org.osid.voting.records.BallotQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.records.BallotQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.records.BallotSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBallotQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBallotQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches ballots that allow vote modification. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRevote(boolean match) {
        getAssembler().addBooleanTerm(getRevoteColumn(), match);
        return;
    }


    /**
     *  Clears the revote ballot terms. 
     */

    @OSID @Override
    public void clearRevoteTerms() {
        getAssembler().clearTerms(getRevoteColumn());
        return;
    }


    /**
     *  Gets the revote query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRevoteTerms() {
        return (getAssembler().getBooleanTerms(getRevoteColumn()));
    }


    /**
     *  Orders by the revote flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRevote(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRevoteColumn(), style);
        return;
    }


    /**
     *  Gets the Revote column name.
     *
     * @return the column name
     */

    protected String getRevoteColumn() {
        return ("revote");
    }


    /**
     *  Sets the race <code> Id </code> for this query. 
     *
     *  @param  raceId the race <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRaceId(org.osid.id.Id raceId, boolean match) {
        getAssembler().addIdTerm(getRaceIdColumn(), raceId, match);
        return;
    }


    /**
     *  Clears the race <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRaceIdTerms() {
        getAssembler().clearTerms(getRaceIdColumn());
        return;
    }


    /**
     *  Gets the race <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRaceIdTerms() {
        return (getAssembler().getIdTerms(getRaceIdColumn()));
    }


    /**
     *  Gets the RaceId column name.
     *
     * @return the column name
     */

    protected String getRaceIdColumn() {
        return ("race_id");
    }


    /**
     *  Tests if a <code> RaceQuery </code> is available. 
     *
     *  @return <code> true </code> if a race query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the race query 
     *  @throws org.osid.UnimplementedException <code> supportsRaceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuery getRaceQuery() {
        throw new org.osid.UnimplementedException("supportsRaceQuery() is false");
    }


    /**
     *  Matches ballots with any race. 
     *
     *  @param  match <code> true </code> to match any race, <code> false 
     *          </code> to match ballots with no races 
     */

    @OSID @Override
    public void matchAnyRace(boolean match) {
        getAssembler().addIdWildcardTerm(getRaceColumn(), match);
        return;
    }


    /**
     *  Clears the race terms. 
     */

    @OSID @Override
    public void clearRaceTerms() {
        getAssembler().clearTerms(getRaceColumn());
        return;
    }


    /**
     *  Gets the race query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.RaceQueryInspector[] getRaceTerms() {
        return (new org.osid.voting.RaceQueryInspector[0]);
    }


    /**
     *  Gets the Race column name.
     *
     * @return the column name
     */

    protected String getRaceColumn() {
        return ("race");
    }


    /**
     *  Sets the polls <code> Id </code> for this query. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        getAssembler().addIdTerm(getPollsIdColumn(), pollsId, match);
        return;
    }


    /**
     *  Clears the polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        getAssembler().clearTerms(getPollsIdColumn());
        return;
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (getAssembler().getIdTerms(getPollsIdColumn()));
    }


    /**
     *  Gets the PollsId column name.
     *
     * @return the column name
     */

    protected String getPollsIdColumn() {
        return ("polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        getAssembler().clearTerms(getPollsColumn());
        return;
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the Polls column name.
     *
     * @return the column name
     */

    protected String getPollsColumn() {
        return ("polls");
    }


    /**
     *  Tests if this ballot supports the given record
     *  <code>Type</code>.
     *
     *  @param  ballotRecordType a ballot record type 
     *  @return <code>true</code> if the ballotRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type ballotRecordType) {
        for (org.osid.voting.records.BallotQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  ballotRecordType the ballot record type 
     *  @return the ballot query record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.BallotQueryRecord getBallotQueryRecord(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.BallotQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  ballotRecordType the ballot record type 
     *  @return the ballot query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.BallotQueryInspectorRecord getBallotQueryInspectorRecord(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.BallotQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param ballotRecordType the ballot record type
     *  @return the ballot search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.BallotSearchOrderRecord getBallotSearchOrderRecord(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.BallotSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this ballot. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param ballotQueryRecord the ballot query record
     *  @param ballotQueryInspectorRecord the ballot query inspector
     *         record
     *  @param ballotSearchOrderRecord the ballot search order record
     *  @param ballotRecordType ballot record type
     *  @throws org.osid.NullArgumentException
     *          <code>ballotQueryRecord</code>,
     *          <code>ballotQueryInspectorRecord</code>,
     *          <code>ballotSearchOrderRecord</code> or
     *          <code>ballotRecordTypeballot</code> is
     *          <code>null</code>
     */
            
    protected void addBallotRecords(org.osid.voting.records.BallotQueryRecord ballotQueryRecord, 
                                      org.osid.voting.records.BallotQueryInspectorRecord ballotQueryInspectorRecord, 
                                      org.osid.voting.records.BallotSearchOrderRecord ballotSearchOrderRecord, 
                                      org.osid.type.Type ballotRecordType) {

        addRecordType(ballotRecordType);

        nullarg(ballotQueryRecord, "ballot query record");
        nullarg(ballotQueryInspectorRecord, "ballot query inspector record");
        nullarg(ballotSearchOrderRecord, "ballot search odrer record");

        this.queryRecords.add(ballotQueryRecord);
        this.queryInspectorRecords.add(ballotQueryInspectorRecord);
        this.searchOrderRecords.add(ballotSearchOrderRecord);
        
        return;
    }
}
