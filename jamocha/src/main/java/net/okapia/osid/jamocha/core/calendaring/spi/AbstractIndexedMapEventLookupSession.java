//
// AbstractIndexedMapEventLookupSession.java
//
//    A simple framework for providing an Event lookup service
//    backed by a fixed collection of events with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Event lookup service backed by a
 *  fixed collection of events. The events are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some events may be compatible
 *  with more types than are indicated through these event
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Events</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapEventLookupSession
    extends AbstractMapEventLookupSession
    implements org.osid.calendaring.EventLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.Event> eventsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.Event>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.Event> eventsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.Event>());


    /**
     *  Makes an <code>Event</code> available in this session.
     *
     *  @param  event an event
     *  @throws org.osid.NullArgumentException <code>event<code> is
     *          <code>null</code>
     */

    @Override
    protected void putEvent(org.osid.calendaring.Event event) {
        super.putEvent(event);

        this.eventsByGenus.put(event.getGenusType(), event);
        
        try (org.osid.type.TypeList types = event.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.eventsByRecord.put(types.getNextType(), event);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an event from this session.
     *
     *  @param eventId the <code>Id</code> of the event
     *  @throws org.osid.NullArgumentException <code>eventId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeEvent(org.osid.id.Id eventId) {
        org.osid.calendaring.Event event;
        try {
            event = getEvent(eventId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.eventsByGenus.remove(event.getGenusType());

        try (org.osid.type.TypeList types = event.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.eventsByRecord.remove(types.getNextType(), event);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeEvent(eventId);
        return;
    }


    /**
     *  Gets an <code>EventList</code> corresponding to the given
     *  event genus <code>Type</code> which does not include
     *  events of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known events or an error results. Otherwise,
     *  the returned list may contain only those events that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  eventGenusType an event genus type 
     *  @return the returned <code>Event</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>eventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByGenusType(org.osid.type.Type eventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.event.ArrayEventList(this.eventsByGenus.get(eventGenusType)));
    }


    /**
     *  Gets an <code>EventList</code> containing the given
     *  event record <code>Type</code>. In plenary mode, the
     *  returned list contains all known events or an error
     *  results. Otherwise, the returned list may contain only those
     *  events that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  eventRecordType an event record type 
     *  @return the returned <code>event</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByRecordType(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.event.ArrayEventList(this.eventsByRecord.get(eventRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.eventsByGenus.clear();
        this.eventsByRecord.clear();

        super.close();

        return;
    }
}
