//
// AbstractMapBudgetEntryLookupSession
//
//    A simple framework for providing a BudgetEntry lookup service
//    backed by a fixed collection of budget entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a BudgetEntry lookup service backed by a
 *  fixed collection of budget entries. The budget entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BudgetEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBudgetEntryLookupSession
    extends net.okapia.osid.jamocha.financials.budgeting.spi.AbstractBudgetEntryLookupSession
    implements org.osid.financials.budgeting.BudgetEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.financials.budgeting.BudgetEntry> budgetEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.financials.budgeting.BudgetEntry>());


    /**
     *  Makes a <code>BudgetEntry</code> available in this session.
     *
     *  @param  budgetEntry a budget entry
     *  @throws org.osid.NullArgumentException <code>budgetEntry<code>
     *          is <code>null</code>
     */

    protected void putBudgetEntry(org.osid.financials.budgeting.BudgetEntry budgetEntry) {
        this.budgetEntries.put(budgetEntry.getId(), budgetEntry);
        return;
    }


    /**
     *  Makes an array of budget entries available in this session.
     *
     *  @param  budgetEntries an array of budget entries
     *  @throws org.osid.NullArgumentException <code>budgetEntries<code>
     *          is <code>null</code>
     */

    protected void putBudgetEntries(org.osid.financials.budgeting.BudgetEntry[] budgetEntries) {
        putBudgetEntries(java.util.Arrays.asList(budgetEntries));
        return;
    }


    /**
     *  Makes a collection of budget entries available in this session.
     *
     *  @param  budgetEntries a collection of budget entries
     *  @throws org.osid.NullArgumentException <code>budgetEntries<code>
     *          is <code>null</code>
     */

    protected void putBudgetEntries(java.util.Collection<? extends org.osid.financials.budgeting.BudgetEntry> budgetEntries) {
        for (org.osid.financials.budgeting.BudgetEntry budgetEntry : budgetEntries) {
            this.budgetEntries.put(budgetEntry.getId(), budgetEntry);
        }

        return;
    }


    /**
     *  Removes a BudgetEntry from this session.
     *
     *  @param  budgetEntryId the <code>Id</code> of the budget entry
     *  @throws org.osid.NullArgumentException <code>budgetEntryId<code> is
     *          <code>null</code>
     */

    protected void removeBudgetEntry(org.osid.id.Id budgetEntryId) {
        this.budgetEntries.remove(budgetEntryId);
        return;
    }


    /**
     *  Gets the <code>BudgetEntry</code> specified by its <code>Id</code>.
     *
     *  @param  budgetEntryId <code>Id</code> of the <code>BudgetEntry</code>
     *  @return the budgetEntry
     *  @throws org.osid.NotFoundException <code>budgetEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>budgetEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntry getBudgetEntry(org.osid.id.Id budgetEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.financials.budgeting.BudgetEntry budgetEntry = this.budgetEntries.get(budgetEntryId);
        if (budgetEntry == null) {
            throw new org.osid.NotFoundException("budgetEntry not found: " + budgetEntryId);
        }

        return (budgetEntry);
    }


    /**
     *  Gets all <code>BudgetEntries</code>. In plenary mode, the returned
     *  list contains all known budgetEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  budgetEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>BudgetEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.budgeting.budgetentry.ArrayBudgetEntryList(this.budgetEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.budgetEntries.clear();
        super.close();
        return;
    }
}
