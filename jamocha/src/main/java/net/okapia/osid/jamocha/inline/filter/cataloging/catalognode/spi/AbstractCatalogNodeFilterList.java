//
// AbstractCatalogNodeList
//
//     Implements a filter for a CatalogNodeList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.cataloging.catalognode.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a CatalogNodeList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedCatalogNodeList
 *  to improve performance.
 */

public abstract class AbstractCatalogNodeFilterList
    extends net.okapia.osid.jamocha.cataloging.catalognode.spi.AbstractCatalogNodeList
    implements org.osid.cataloging.CatalogNodeList,
               net.okapia.osid.jamocha.inline.filter.cataloging.catalognode.CatalogNodeFilter {

    private org.osid.cataloging.CatalogNode catalogNode;
    private final org.osid.cataloging.CatalogNodeList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractCatalogNodeFilterList</code>.
     *
     *  @param catalogNodeList a <code>CatalogNodeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>catalogNodeList</code> is <code>null</code>
     */

    protected AbstractCatalogNodeFilterList(org.osid.cataloging.CatalogNodeList catalogNodeList) {
        nullarg(catalogNodeList, "catalog node list");
        this.list = catalogNodeList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.catalogNode == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> CatalogNode </code> in this list. 
     *
     *  @return the next <code> CatalogNode </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> CatalogNode </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogNode getNextCatalogNode()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.cataloging.CatalogNode catalogNode = this.catalogNode;
            this.catalogNode = null;
            return (catalogNode);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in catalog node list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.catalogNode = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters CatalogNodes.
     *
     *  @param catalogNode the catalog node to filter
     *  @return <code>true</code> if the catalog node passes the filter,
     *          <code>false</code> if the catalog node should be filtered
     */

    public abstract boolean pass(org.osid.cataloging.CatalogNode catalogNode);


    protected void prime() {
        if (this.catalogNode != null) {
            return;
        }

        org.osid.cataloging.CatalogNode catalogNode = null;

        while (this.list.hasNext()) {
            try {
                catalogNode = this.list.getNextCatalogNode();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(catalogNode)) {
                this.catalogNode = catalogNode;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
