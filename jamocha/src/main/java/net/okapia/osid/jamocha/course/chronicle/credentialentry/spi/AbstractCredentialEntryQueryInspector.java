//
// AbstractCredentialEntryQueryInspector.java
//
//     A template for making a CredentialEntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.credentialentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for credential entries.
 */

public abstract class AbstractCredentialEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.course.chronicle.CredentialEntryQueryInspector {

    private final java.util.Collection<org.osid.course.chronicle.records.CredentialEntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the credential <code> Id </code> query terms. 
     *
     *  @return the credential <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCredentialIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the credential query terms. 
     *
     *  @return the credential terms 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQueryInspector[] getCredentialTerms() {
        return (new org.osid.course.program.CredentialQueryInspector[0]);
    }


    /**
     *  Gets the award date query terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateAwardedTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given credential entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a credential entry implementing the requested record.
     *
     *  @param credentialEntryRecordType a credential entry record type
     *  @return the credential entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CredentialEntryQueryInspectorRecord getCredentialEntryQueryInspectorRecord(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CredentialEntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(credentialEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credential entry query. 
     *
     *  @param credentialEntryQueryInspectorRecord credential entry query inspector
     *         record
     *  @param credentialEntryRecordType credentialEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCredentialEntryQueryInspectorRecord(org.osid.course.chronicle.records.CredentialEntryQueryInspectorRecord credentialEntryQueryInspectorRecord, 
                                                   org.osid.type.Type credentialEntryRecordType) {

        addRecordType(credentialEntryRecordType);
        nullarg(credentialEntryRecordType, "credential entry record type");
        this.records.add(credentialEntryQueryInspectorRecord);        
        return;
    }
}
