//
// AbstractFederatingOfficeLookupSession.java
//
//     An abstract federating adapter for an OfficeLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  OfficeLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingOfficeLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.workflow.OfficeLookupSession>
    implements org.osid.workflow.OfficeLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingOfficeLookupSession</code>.
     */

    protected AbstractFederatingOfficeLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.workflow.OfficeLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Office</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOffices() {
        for (org.osid.workflow.OfficeLookupSession session : getSessions()) {
            if (session.canLookupOffices()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Office</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOfficeView() {
        for (org.osid.workflow.OfficeLookupSession session : getSessions()) {
            session.useComparativeOfficeView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Office</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOfficeView() {
        for (org.osid.workflow.OfficeLookupSession session : getSessions()) {
            session.usePlenaryOfficeView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Office</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Office</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Office</code> and
     *  retained for compatibility.
     *
     *  @param  officeId <code>Id</code> of the
     *          <code>Office</code>
     *  @return the office
     *  @throws org.osid.NotFoundException <code>officeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>officeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.workflow.OfficeLookupSession session : getSessions()) {
            try {
                return (session.getOffice(officeId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(officeId + " not found");
    }


    /**
     *  Gets an <code>OfficeList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the offices
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Offices</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  officeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Office</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>officeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByIds(org.osid.id.IdList officeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.workflow.office.MutableOfficeList ret = new net.okapia.osid.jamocha.workflow.office.MutableOfficeList();

        try (org.osid.id.IdList ids = officeIds) {
            while (ids.hasNext()) {
                ret.addOffice(getOffice(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>OfficeList</code> corresponding to the given
     *  office genus <code>Type</code> which does not include offices
     *  of types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known offices
     *  or an error results. Otherwise, the returned list may contain
     *  only those offices that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @param  officeGenusType an office genus type 
     *  @return the returned <code>Office</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>officeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByGenusType(org.osid.type.Type officeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.office.FederatingOfficeList ret = getOfficeList();

        for (org.osid.workflow.OfficeLookupSession session : getSessions()) {
            ret.addOfficeList(session.getOfficesByGenusType(officeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>OfficeList</code> corresponding to the given
     *  office genus <code>Type</code> and include any additional
     *  offices with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known offices
     *  or an error results. Otherwise, the returned list may contain
     *  only those offices that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @param  officeGenusType an office genus type 
     *  @return the returned <code>Office</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>officeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByParentGenusType(org.osid.type.Type officeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.office.FederatingOfficeList ret = getOfficeList();

        for (org.osid.workflow.OfficeLookupSession session : getSessions()) {
            ret.addOfficeList(session.getOfficesByParentGenusType(officeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>OfficeList</code> containing the given office
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known offices
     *  or an error results. Otherwise, the returned list may contain
     *  only those offices that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @param  officeRecordType an office record type 
     *  @return the returned <code>Office</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>officeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByRecordType(org.osid.type.Type officeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.office.FederatingOfficeList ret = getOfficeList();

        for (org.osid.workflow.OfficeLookupSession session : getSessions()) {
            ret.addOfficeList(session.getOfficesByRecordType(officeRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>OfficeList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known offices or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  offices that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Office</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.workflow.office.FederatingOfficeList ret = getOfficeList();

        for (org.osid.workflow.OfficeLookupSession session : getSessions()) {
            ret.addOfficeList(session.getOfficesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Offices</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  offices or an error results. Otherwise, the returned list
     *  may contain only those offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Offices</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.office.FederatingOfficeList ret = getOfficeList();

        for (org.osid.workflow.OfficeLookupSession session : getSessions()) {
            ret.addOfficeList(session.getOffices());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.workflow.office.FederatingOfficeList getOfficeList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.office.ParallelOfficeList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.office.CompositeOfficeList());
        }
    }
}
