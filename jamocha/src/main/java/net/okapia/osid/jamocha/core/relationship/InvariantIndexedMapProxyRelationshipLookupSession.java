//
// InvariantIndexedMapProxyRelationshipLookupSession
//
//    Implements a Relationship lookup service backed by a fixed
//    collection of relationships indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship;


/**
 *  Implements a Relationship lookup service backed by a fixed
 *  collection of relationships. The relationships are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some relationships may be compatible
 *  with more types than are indicated through these relationship
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyRelationshipLookupSession
    extends net.okapia.osid.jamocha.core.relationship.spi.AbstractIndexedMapRelationshipLookupSession
    implements org.osid.relationship.RelationshipLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyRelationshipLookupSession}
     *  using an array of relationships.
     *
     *  @param family the family
     *  @param relationships an array of relationships
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code family},
     *          {@code relationships} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyRelationshipLookupSession(org.osid.relationship.Family family,
                                                         org.osid.relationship.Relationship[] relationships, 
                                                         org.osid.proxy.Proxy proxy) {

        setFamily(family);
        setSessionProxy(proxy);
        putRelationships(relationships);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyRelationshipLookupSession}
     *  using a collection of relationships.
     *
     *  @param family the family
     *  @param relationships a collection of relationships
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code family},
     *          {@code relationships} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyRelationshipLookupSession(org.osid.relationship.Family family,
                                                         java.util.Collection<? extends org.osid.relationship.Relationship> relationships,
                                                         org.osid.proxy.Proxy proxy) {

        setFamily(family);
        setSessionProxy(proxy);
        putRelationships(relationships);

        return;
    }
}
