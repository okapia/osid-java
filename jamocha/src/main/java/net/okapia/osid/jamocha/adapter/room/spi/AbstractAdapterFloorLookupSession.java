//
// AbstractAdapterFloorLookupSession.java
//
//    A Floor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Floor lookup session adapter.
 */

public abstract class AbstractAdapterFloorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.room.FloorLookupSession {

    private final org.osid.room.FloorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterFloorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterFloorLookupSession(org.osid.room.FloorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Campus/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Campus Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the {@code Campus} associated with this session.
     *
     *  @return the {@code Campus} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform {@code Floor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupFloors() {
        return (this.session.canLookupFloors());
    }


    /**
     *  A complete view of the {@code Floor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFloorView() {
        this.session.useComparativeFloorView();
        return;
    }


    /**
     *  A complete view of the {@code Floor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFloorView() {
        this.session.usePlenaryFloorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include floors in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only floors whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveFloorView() {
        this.session.useEffectiveFloorView();
        return;
    }
    

    /**
     *  All floors of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveFloorView() {
        this.session.useAnyEffectiveFloorView();
        return;
    }

     
    /**
     *  Gets the {@code Floor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Floor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Floor} and
     *  retained for compatibility.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @param floorId {@code Id} of the {@code Floor}
     *  @return the floor
     *  @throws org.osid.NotFoundException {@code floorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code floorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Floor getFloor(org.osid.id.Id floorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFloor(floorId));
    }


    /**
     *  Gets a {@code FloorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  floors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Floors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @param  floorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Floor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code floorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByIds(org.osid.id.IdList floorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFloorsByIds(floorIds));
    }


    /**
     *  Gets a {@code FloorList} corresponding to the given
     *  floor genus {@code Type} which does not include
     *  floors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @param  floorGenusType a floor genus type 
     *  @return the returned {@code Floor} list
     *  @throws org.osid.NullArgumentException
     *          {@code floorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByGenusType(org.osid.type.Type floorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFloorsByGenusType(floorGenusType));
    }


    /**
     *  Gets a {@code FloorList} corresponding to the given
     *  floor genus {@code Type} and include any additional
     *  floors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @param  floorGenusType a floor genus type 
     *  @return the returned {@code Floor} list
     *  @throws org.osid.NullArgumentException
     *          {@code floorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByParentGenusType(org.osid.type.Type floorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFloorsByParentGenusType(floorGenusType));
    }


    /**
     *  Gets a {@code FloorList} containing the given
     *  floor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @param  floorRecordType a floor record type 
     *  @return the returned {@code Floor} list
     *  @throws org.osid.NullArgumentException
     *          {@code floorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByRecordType(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFloorsByRecordType(floorRecordType));
    }


    /**
     *  Gets a {@code FloorList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible
     *  through this session.
     *  
     *  In active mode, floors are returned that are currently
     *  active. In any status mode, active and inactive floors
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Floor} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.FloorList getFloorsOnDate(org.osid.calendaring.DateTime from, 
                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFloorsOnDate(from, to));
    }
        

    /**
     *  Gets a list of all floors corresponding to a building {@code
     *  Id}.
     *  
     *  In plenary mode, the returned list contains all known floors or
     *  an error results. Otherwise, the returned list may contain
     *  only those floors that are accessible through this session.
     *  
     *  In effective mode, floors are returned that are currently
     *  effective. In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  @param  buildingId the {@code Id} of the building 
     *  @return the returned {@code FloorList} 
     *  @throws org.osid.NullArgumentException {@code buildingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFloorsForBuilding(buildingId));
    }


    /**
     *  Gets a list of all floors corresponding to a building {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known floors or
     *  an error results. Otherwise, the returned list may contain
     *  only those floors that are accessible through this session.
     *  
     *  In effective mode, floors are returned that are currently
     *  effective. In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code FloorList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code buildingId,
     *          from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsForBuildingOnDate(org.osid.id.Id buildingId, 
                                                            org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFloorsForBuildingOnDate(buildingId, from, to));
    }


    /**
     *  Gets a {@code FloorList} containing of the given floor number.
     *  
     *  In plenary mode, the returned list contains all known floors
     *  or an error results. Otherwise, the returned list may contain
     *  only those floors that are accessible through this session.
     *  
     *  In effective mode, floors are returned that are currently
     *  effective. In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  @param  number a floor number 
     *  @return the returned {@code Floor} list 
     *  @throws org.osid.NullArgumentException {@code number} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFloorsByNumber(number));
    }


    /**
     *  Gets all {@code Floors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Floors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFloors());
    }
}
