//
// JobConstrainerEnablerValidator.java
//
//     Validates a JobConstrainerEnabler.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.resourcing.rules.jobconstrainerenabler;


/**
 *  Validates a JobConstrainerEnabler.
 */

public final class JobConstrainerEnablerValidator
    extends net.okapia.osid.jamocha.builder.validator.resourcing.rules.jobconstrainerenabler.spi.AbstractJobConstrainerEnablerValidator {


    /**
     *  Constructs a new <code>JobConstrainerEnablerValidator</code>.
     */

    public JobConstrainerEnablerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>JobConstrainerEnablerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public JobConstrainerEnablerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a JobConstrainerEnabler with a default validation.
     *
     *  @param jobConstrainerEnabler a job constrainer enabler to validate
     *  @return the job constrainer enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnabler</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.resourcing.rules.JobConstrainerEnabler validateJobConstrainerEnabler(org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler) {
        JobConstrainerEnablerValidator validator = new JobConstrainerEnablerValidator();
        validator.validate(jobConstrainerEnabler);
        return (jobConstrainerEnabler);
    }


    /**
     *  Validates a JobConstrainerEnabler for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param jobConstrainerEnabler a job constrainer enabler to validate
     *  @return the job constrainer enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>jobConstrainerEnabler</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.resourcing.rules.JobConstrainerEnabler validateJobConstrainerEnabler(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler) {

        JobConstrainerEnablerValidator validator = new JobConstrainerEnablerValidator(validation);
        validator.validate(jobConstrainerEnabler);
        return (jobConstrainerEnabler);
    }
}
