//
// AbstractImmutableAssessmentOffered.java
//
//     Wraps a mutable AssessmentOffered to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmentoffered.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AssessmentOffered</code> to hide modifiers. This
 *  wrapper provides an immutized AssessmentOffered from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying assessmentOffered whose state changes are visible.
 */

public abstract class AbstractImmutableAssessmentOffered
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.assessment.AssessmentOffered {

    private final org.osid.assessment.AssessmentOffered assessmentOffered;


    /**
     *  Constructs a new <code>AbstractImmutableAssessmentOffered</code>.
     *
     *  @param assessmentOffered the assessment offered to immutablize
     *  @throws org.osid.NullArgumentException <code>assessmentOffered</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAssessmentOffered(org.osid.assessment.AssessmentOffered assessmentOffered) {
        super(assessmentOffered);
        this.assessmentOffered = assessmentOffered;
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> corresponding to this assessment 
     *  offering. 
     *
     *  @return the assessment id 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.assessmentOffered.getAssessmentId());
    }


    /**
     *  Gets the assessment corresponding to this assessment offereng. 
     *
     *  @return the assessment 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.assessmentOffered.getAssessment());
    }


    /**
     *  Gets the <code> Id </code> of a <code> Grade </code> corresponding to 
     *  the assessment difficulty. 
     *
     *  @return a grade id 
     */

    @OSID @Override
    public org.osid.id.Id getLevelId() {
        return (this.assessmentOffered.getLevelId());
    }


    /**
     *  Gets the <code> Grade </code> corresponding to the assessment 
     *  difficulty. 
     *
     *  @return the level 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getLevel()
        throws org.osid.OperationFailedException {

        return (this.assessmentOffered.getLevel());
    }


    /**
     *  Tests if the items or parts in this assessment are taken sequentially. 
     *
     *  @return <code> true </code> if the items are taken sequentially, 
     *          <code> false </code> if the items can be skipped and revisited 
     */

    @OSID @Override
    public boolean areItemsSequential() {
        return (this.assessmentOffered.areItemsSequential());
    }


    /**
     *  Tests if the items or parts appear in a random order. 
     *
     *  @return <code> true </code> if the items appear in a random order, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean areItemsShuffled() {
        return (this.assessmentOffered.areItemsShuffled());
    }


    /**
     *  Tests if there is a fixed start time for this assessment. 
     *
     *  @return <code> true </code> if there is a fixed start time, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasStartTime() {
        return (this.assessmentOffered.hasStartTime());
    }


    /**
     *  Gets the start time for this assessment. 
     *
     *  @return the designated start time 
     *  @throws org.osid.IllegalStateException <code> hasStartTime() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartTime() {
        return (this.assessmentOffered.getStartTime());
    }


    /**
     *  Tests if there is a fixed end time for this assessment. 
     *
     *  @return <code> true </code> if there is a fixed end time, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasDeadline() {
        return (this.assessmentOffered.hasDeadline());
    }


    /**
     *  Gets the end time for this assessment. 
     *
     *  @return the designated end time 
     *  @throws org.osid.IllegalStateException <code> hasDeadline() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDeadline() {
        return (this.assessmentOffered.getDeadline());
    }


    /**
     *  Tests if there is a fixed duration for this assessment. 
     *
     *  @return <code> true </code> if there is a fixed duration, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasDuration() {
        return (this.assessmentOffered.hasDuration());
    }


    /**
     *  Gets the duration for this assessment. 
     *
     *  @return the duration 
     *  @throws org.osid.IllegalStateException <code> hasDuration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDuration() {
        return (this.assessmentOffered.getDuration());
    }


    /**
     *  Tests if this assessment will be scored. 
     *
     *  @return <code> true </code> if this assessment will be scored <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isScored() {
        return (this.assessmentOffered.isScored());
    }


    /**
     *  Gets the grade system Id for the score. 
     *
     *  @return the grade system Id
     *  @throws org.osid.IllegalStateException <code> isScored() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScoreSystemId() {
        return (this.assessmentOffered.getScoreSystemId());
    }


    /**
     *  Gets the grade system for the score. 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> isScored() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getScoreSystem()
        throws org.osid.OperationFailedException {

        return (this.assessmentOffered.getScoreSystem());
    }


    /**
     *  Tests if this assessment will be graded. 
     *
     *  @return <code> true </code> if this assessment will be graded, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.assessmentOffered.isGraded());
    }


    /**
     *  Gets the grade system Id for the grade. 
     *
     *  @return the grade system Id
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeSystemId() {
        return (this.assessmentOffered.getGradeSystemId());
    }


    /**
     *  Gets the grade system for the grade. 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystem()
        throws org.osid.OperationFailedException {
        
        return (this.assessmentOffered.getGradeSystem());
    }


    /**
     *  Tests if a rubric assessment offered is associated with this
     *  assessment.
     *
     *  @return <code> true </code> if a rubric is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasRubric() {
        return (this.assessmentOffered.hasRubric());
    }


    /**
     *  Gets the <code> Id </code> of the rubric. 
     *
     *  @return an assessment offered <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRubricId() {
        return (this.assessmentOffered.getRubricId());
    }


    /**
     *  Gets the rubric. 
     *
     *  @return the assessment offered
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOffered getRubric()
        throws org.osid.OperationFailedException {

        return (this.assessmentOffered.getRubric());
    }


    /**
     *  Gets the assessment offered record corresponding to the given <code> 
     *  AssessmentOffered </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> assessmentOfferedRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(assessmentOfferedRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  assessmentTakenRecordType an assessment offered record type 
     *  @return the assessment offered record 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentOfferedRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(assessmentOfferedRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedRecord getAssessmentOfferedRecord(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException {

        return (this.assessmentOffered.getAssessmentOfferedRecord(assessmentTakenRecordType));
    }
}

