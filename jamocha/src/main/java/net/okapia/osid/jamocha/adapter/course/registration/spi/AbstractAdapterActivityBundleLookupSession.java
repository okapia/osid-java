//
// AbstractAdapterActivityBundleLookupSession.java
//
//    An ActivityBundle lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.registration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An ActivityBundle lookup session adapter.
 */

public abstract class AbstractAdapterActivityBundleLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.registration.ActivityBundleLookupSession {

    private final org.osid.course.registration.ActivityBundleLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterActivityBundleLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterActivityBundleLookupSession(org.osid.course.registration.ActivityBundleLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code ActivityBundle} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupActivityBundles() {
        return (this.session.canLookupActivityBundles());
    }


    /**
     *  A complete view of the {@code ActivityBundle} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityBundleView() {
        this.session.useComparativeActivityBundleView();
        return;
    }


    /**
     *  A complete view of the {@code ActivityBundle} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityBundleView() {
        this.session.usePlenaryActivityBundleView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity bundles in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    
     
    /**
     *  Gets the {@code ActivityBundle} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ActivityBundle} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ActivityBundle} and
     *  retained for compatibility.
     *
     *  @param activityBundleId {@code Id} of the {@code ActivityBundle}
     *  @return the activity bundle
     *  @throws org.osid.NotFoundException {@code activityBundleId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code activityBundleId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundle getActivityBundle(org.osid.id.Id activityBundleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityBundle(activityBundleId));
    }


    /**
     *  Gets an {@code ActivityBundleList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activityBundles specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ActivityBundles} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  activityBundleIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ActivityBundle} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code activityBundleIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByIds(org.osid.id.IdList activityBundleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityBundlesByIds(activityBundleIds));
    }


    /**
     *  Gets an {@code ActivityBundleList} corresponding to the given
     *  activity bundle genus {@code Type} which does not include
     *  activity bundles of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityBundleGenusType an activityBundle genus type 
     *  @return the returned {@code ActivityBundle} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityBundleGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByGenusType(org.osid.type.Type activityBundleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityBundlesByGenusType(activityBundleGenusType));
    }


    /**
     *  Gets an {@code ActivityBundleList} corresponding to the given
     *  activity bundle genus {@code Type} and include any additional
     *  activity bundles with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityBundleGenusType an activityBundle genus type 
     *  @return the returned {@code ActivityBundle} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityBundleGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByParentGenusType(org.osid.type.Type activityBundleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityBundlesByParentGenusType(activityBundleGenusType));
    }


    /**
     *  Gets an {@code ActivityBundleList} containing the given
     *  activity bundle record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityBundleRecordType an activityBundle record type 
     *  @return the returned {@code ActivityBundle} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityBundleRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByRecordType(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityBundlesByRecordType(activityBundleRecordType));
    }


    /**
     *  Gets an {@code ActivityBundleList} for a given course offering
     *  {@code .} In plenary mode, the returned list contains all
     *  known courses or an error results. Otherwise, the returned
     *  list may contain only those activity bundles that are
     *  accessible through this session.
     *
     *  @param  courseOfferingId a course offering {@code Id} 
     *  @return the returned {@code ActivityBundle} list 
     *  @throws org.osid.NullArgumentException {@code courseOfferingId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getActivityBundlesForCourseOffering(courseOfferingId));
    }


    /**
     *  Gets an {@code ActivityBundleList} for a given activity. In
     *  plenary mode, the returned list contains all known courses or
     *  an error results. Otherwise, the returned list may contain
     *  only those activity bundles that are accessible through this
     *  session.
     *
     *  @param  activityId an activity {@code Id} 
     *  @return the returned {@code ActivityBundle} list 
     *  @throws org.osid.NullArgumentException {@code activityId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityBundlesByActivity(activityId));
    }


    /**
     *  Gets all {@code ActivityBundles}. 
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code ActivityBundles} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityBundles());
    }
}
