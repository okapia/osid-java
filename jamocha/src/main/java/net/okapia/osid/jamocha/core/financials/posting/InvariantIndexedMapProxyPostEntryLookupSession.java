//
// InvariantIndexedMapProxyPostEntryLookupSession
//
//    Implements a PostEntry lookup service backed by a fixed
//    collection of postEntries indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.posting;


/**
 *  Implements a PostEntry lookup service backed by a fixed
 *  collection of postEntries. The postEntries are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some postEntries may be compatible
 *  with more types than are indicated through these postEntry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyPostEntryLookupSession
    extends net.okapia.osid.jamocha.core.financials.posting.spi.AbstractIndexedMapPostEntryLookupSession
    implements org.osid.financials.posting.PostEntryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyPostEntryLookupSession}
     *  using an array of post entries.
     *
     *  @param business the business
     *  @param postEntries an array of post entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code postEntries} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyPostEntryLookupSession(org.osid.financials.Business business,
                                                         org.osid.financials.posting.PostEntry[] postEntries, 
                                                         org.osid.proxy.Proxy proxy) {

        setBusiness(business);
        setSessionProxy(proxy);
        putPostEntries(postEntries);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyPostEntryLookupSession}
     *  using a collection of post entries.
     *
     *  @param business the business
     *  @param postEntries a collection of post entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code postEntries} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyPostEntryLookupSession(org.osid.financials.Business business,
                                                         java.util.Collection<? extends org.osid.financials.posting.PostEntry> postEntries,
                                                         org.osid.proxy.Proxy proxy) {

        setBusiness(business);
        setSessionProxy(proxy);
        putPostEntries(postEntries);

        return;
    }
}
