//
// AbstractIndexedMapRouteLookupSession.java
//
//    A simple framework for providing a Route lookup service
//    backed by a fixed collection of routes with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.route.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Route lookup service backed by a
 *  fixed collection of routes. The routes are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some routes may be compatible
 *  with more types than are indicated through these route
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Routes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRouteLookupSession
    extends AbstractMapRouteLookupSession
    implements org.osid.mapping.route.RouteLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.route.Route> routesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.route.Route>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.route.Route> routesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.route.Route>());


    /**
     *  Makes a <code>Route</code> available in this session.
     *
     *  @param  route a route
     *  @throws org.osid.NullArgumentException <code>route<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRoute(org.osid.mapping.route.Route route) {
        super.putRoute(route);

        this.routesByGenus.put(route.getGenusType(), route);
        
        try (org.osid.type.TypeList types = route.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.routesByRecord.put(types.getNextType(), route);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a route from this session.
     *
     *  @param routeId the <code>Id</code> of the route
     *  @throws org.osid.NullArgumentException <code>routeId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRoute(org.osid.id.Id routeId) {
        org.osid.mapping.route.Route route;
        try {
            route = getRoute(routeId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.routesByGenus.remove(route.getGenusType());

        try (org.osid.type.TypeList types = route.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.routesByRecord.remove(types.getNextType(), route);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRoute(routeId);
        return;
    }


    /**
     *  Gets a <code>RouteList</code> corresponding to the given
     *  route genus <code>Type</code> which does not include
     *  routes of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known routes or an error results. Otherwise,
     *  the returned list may contain only those routes that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  routeGenusType a route genus type 
     *  @return the returned <code>Route</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>routeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByGenusType(org.osid.type.Type routeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.route.route.ArrayRouteList(this.routesByGenus.get(routeGenusType)));
    }


    /**
     *  Gets a <code>RouteList</code> containing the given
     *  route record <code>Type</code>. In plenary mode, the
     *  returned list contains all known routes or an error
     *  results. Otherwise, the returned list may contain only those
     *  routes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  routeRecordType a route record type 
     *  @return the returned <code>route</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByRecordType(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.route.route.ArrayRouteList(this.routesByRecord.get(routeRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.routesByGenus.clear();
        this.routesByRecord.clear();

        super.close();

        return;
    }
}
