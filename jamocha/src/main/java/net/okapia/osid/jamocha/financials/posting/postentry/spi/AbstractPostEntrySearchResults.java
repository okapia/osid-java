//
// AbstractPostEntrySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.postentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPostEntrySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.financials.posting.PostEntrySearchResults {

    private org.osid.financials.posting.PostEntryList postEntries;
    private final org.osid.financials.posting.PostEntryQueryInspector inspector;
    private final java.util.Collection<org.osid.financials.posting.records.PostEntrySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPostEntrySearchResults.
     *
     *  @param postEntries the result set
     *  @param postEntryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>postEntries</code>
     *          or <code>postEntryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPostEntrySearchResults(org.osid.financials.posting.PostEntryList postEntries,
                                            org.osid.financials.posting.PostEntryQueryInspector postEntryQueryInspector) {
        nullarg(postEntries, "post entries");
        nullarg(postEntryQueryInspector, "post entry query inspectpr");

        this.postEntries = postEntries;
        this.inspector = postEntryQueryInspector;

        return;
    }


    /**
     *  Gets the post entry list resulting from a search.
     *
     *  @return a post entry list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntries() {
        if (this.postEntries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.financials.posting.PostEntryList postEntries = this.postEntries;
        this.postEntries = null;
	return (postEntries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.financials.posting.PostEntryQueryInspector getPostEntryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  post entry search record <code> Type. </code> This method must
     *  be used to retrieve a postEntry implementing the requested
     *  record.
     *
     *  @param postEntrySearchRecordType a postEntry search 
     *         record type 
     *  @return the post entry search
     *  @throws org.osid.NullArgumentException
     *          <code>postEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(postEntrySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostEntrySearchResultsRecord getPostEntrySearchResultsRecord(org.osid.type.Type postEntrySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.financials.posting.records.PostEntrySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(postEntrySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(postEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record post entry search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPostEntryRecord(org.osid.financials.posting.records.PostEntrySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "post entry record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
