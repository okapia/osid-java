//
// InvariantIndexedMapPoolProcessorLookupSession
//
//    Implements a PoolProcessor lookup service backed by a fixed
//    collection of poolProcessors indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolProcessor lookup service backed by a fixed
 *  collection of pool processors. The pool processors are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some pool processors may be compatible
 *  with more types than are indicated through these pool processor
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapPoolProcessorLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractIndexedMapPoolProcessorLookupSession
    implements org.osid.provisioning.rules.PoolProcessorLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapPoolProcessorLookupSession} using an
     *  array of poolProcessors.
     *
     *  @param distributor the distributor
     *  @param poolProcessors an array of pool processors
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code poolProcessors} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                                    org.osid.provisioning.rules.PoolProcessor[] poolProcessors) {

        setDistributor(distributor);
        putPoolProcessors(poolProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapPoolProcessorLookupSession} using a
     *  collection of pool processors.
     *
     *  @param distributor the distributor
     *  @param poolProcessors a collection of pool processors
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code poolProcessors} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                                    java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessor> poolProcessors) {

        setDistributor(distributor);
        putPoolProcessors(poolProcessors);
        return;
    }
}
