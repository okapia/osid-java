//
// AbstractAssemblyAntimatroidQuery.java
//
//     An AntimatroidQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.sequencing.antimatroid.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AntimatroidQuery that stores terms.
 */

public abstract class AbstractAssemblyAntimatroidQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.sequencing.AntimatroidQuery,
               org.osid.sequencing.AntimatroidQueryInspector,
               org.osid.sequencing.AntimatroidSearchOrder {

    private final java.util.Collection<org.osid.sequencing.records.AntimatroidQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.sequencing.records.AntimatroidQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.sequencing.records.AntimatroidSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAntimatroidQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAntimatroidQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the chain <code> Id </code> for this query. 
     *
     *  @param  chainId the chain <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> chainId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchChainId(org.osid.id.Id chainId, boolean match) {
        getAssembler().addIdTerm(getChainIdColumn(), chainId, match);
        return;
    }


    /**
     *  Clears the chain <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearChainIdTerms() {
        getAssembler().clearTerms(getChainIdColumn());
        return;
    }


    /**
     *  Gets the chain <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getChainIdTerms() {
        return (getAssembler().getIdTerms(getChainIdColumn()));
    }


    /**
     *  Gets the ChainId column name.
     *
     * @return the column name
     */

    protected String getChainIdColumn() {
        return ("chain_id");
    }


    /**
     *  Tests if a <code> ChainQuery </code> is available. 
     *
     *  @return <code> true </code> if an input query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputQuery() {
        return (false);
    }


    /**
     *  Gets the query for a chain. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the chain query 
     *  @throws org.osid.UnimplementedException <code> supportsChainQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainQuery getChainQuery() {
        throw new org.osid.UnimplementedException("supportsChainQuery() is false");
    }


    /**
     *  Matches action groups with any chain. 
     *
     *  @param  match <code> true </code> to match antimatroids with any 
     *          chain, <code> false </code> to match antimatroids with no 
     *          chains 
     */

    @OSID @Override
    public void matchAnyChain(boolean match) {
        getAssembler().addIdWildcardTerm(getChainColumn(), match);
        return;
    }


    /**
     *  Clears the chain query terms. 
     */

    @OSID @Override
    public void clearChainTerms() {
        getAssembler().clearTerms(getChainColumn());
        return;
    }


    /**
     *  Gets the chain query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.sequencing.ChainQueryInspector[] getChainTerms() {
        return (new org.osid.sequencing.ChainQueryInspector[0]);
    }


    /**
     *  Gets the Chain column name.
     *
     * @return the column name
     */

    protected String getChainColumn() {
        return ("chain");
    }


    /**
     *  Sets the antimatroid <code> Id </code> for this query to match 
     *  antimatroids that have the specified antimatroid as an ancestor. 
     *
     *  @param  antimatroidId an antimatroid <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAntimatroidId(org.osid.id.Id antimatroidId, 
                                           boolean match) {
        getAssembler().addIdTerm(getAncestorAntimatroidIdColumn(), antimatroidId, match);
        return;
    }


    /**
     *  Clears the ancestor antimatroid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorAntimatroidIdTerms() {
        getAssembler().clearTerms(getAncestorAntimatroidIdColumn());
        return;
    }


    /**
     *  Gets the ancestor antimatroid <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAntimatroidIdTerms() {
        return (getAssembler().getIdTerms(getAncestorAntimatroidIdColumn()));
    }


    /**
     *  Gets the AncestorAntimatroidId column name.
     *
     * @return the column name
     */

    protected String getAncestorAntimatroidIdColumn() {
        return ("ancestor_antimatroid_id");
    }


    /**
     *  Tests if an <code> AntimatroidQuery </code> is available. 
     *
     *  @return <code> true </code> if an antimatroid query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAntimatroidQuery() {
        return (false);
    }


    /**
     *  Gets the query for an antimatroid. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the antimatroid query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAntimatroidQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQuery getAncestorAntimatroidQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAntimatroidQuery() is false");
    }


    /**
     *  Matches antimatroids with any ancestor. 
     *
     *  @param  match <code> true </code> to match antimatroids with any 
     *          ancestor, <code> false </code> to match root antimatroids 
     */

    @OSID @Override
    public void matchAnyAncestorAntimatroid(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorAntimatroidColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor antimatroid query terms. 
     */

    @OSID @Override
    public void clearAncestorAntimatroidTerms() {
        getAssembler().clearTerms(getAncestorAntimatroidColumn());
        return;
    }


    /**
     *  Gets the ancestor antimatroid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQueryInspector[] getAncestorAntimatroidTerms() {
        return (new org.osid.sequencing.AntimatroidQueryInspector[0]);
    }


    /**
     *  Gets the AncestorAntimatroid column name.
     *
     * @return the column name
     */

    protected String getAncestorAntimatroidColumn() {
        return ("ancestor_antimatroid");
    }


    /**
     *  Sets the antimatroid <code> Id </code> for this query to match 
     *  antimatroids that have the specified antimatroid as a descendant. 
     *
     *  @param  antimatroidId an antimatroid <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAntimatroidId(org.osid.id.Id antimatroidId, 
                                             boolean match) {
        getAssembler().addIdTerm(getDescendantAntimatroidIdColumn(), antimatroidId, match);
        return;
    }


    /**
     *  Clears the descendant antimatroid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantAntimatroidIdTerms() {
        getAssembler().clearTerms(getDescendantAntimatroidIdColumn());
        return;
    }


    /**
     *  Gets the descendant antimatroid <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAntimatroidIdTerms() {
        return (getAssembler().getIdTerms(getDescendantAntimatroidIdColumn()));
    }


    /**
     *  Gets the DescendantAntimatroidId column name.
     *
     * @return the column name
     */

    protected String getDescendantAntimatroidIdColumn() {
        return ("descendant_antimatroid_id");
    }


    /**
     *  Tests if an <code> AntimatroidQuery </code> is available. 
     *
     *  @return <code> true </code> if an antimatroid query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAntimatroidQuery() {
        return (false);
    }


    /**
     *  Gets the query for an antimatroid/ Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the antimatroid query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAntimatroidQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQuery getDescendantAntimatroidQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAntimatroidQuery() is false");
    }


    /**
     *  Matches antimatroids with any descendant. 
     *
     *  @param  match <code> true </code> to match antimatroids with any 
     *          descendant, <code> false </code> to match leaf antimatroids 
     */

    @OSID @Override
    public void matchAnyDescendantAntimatroid(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantAntimatroidColumn(), match);
        return;
    }


    /**
     *  Clears the descendant antimatroid query terms. 
     */

    @OSID @Override
    public void clearDescendantAntimatroidTerms() {
        getAssembler().clearTerms(getDescendantAntimatroidColumn());
        return;
    }


    /**
     *  Gets the descendant antimatroid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQueryInspector[] getDescendantAntimatroidTerms() {
        return (new org.osid.sequencing.AntimatroidQueryInspector[0]);
    }


    /**
     *  Gets the DescendantAntimatroid column name.
     *
     * @return the column name
     */

    protected String getDescendantAntimatroidColumn() {
        return ("descendant_antimatroid");
    }


    /**
     *  Tests if this antimatroid supports the given record
     *  <code>Type</code>.
     *
     *  @param  antimatroidRecordType an antimatroid record type 
     *  @return <code>true</code> if the antimatroidRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type antimatroidRecordType) {
        for (org.osid.sequencing.records.AntimatroidQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(antimatroidRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  antimatroidRecordType the antimatroid record type 
     *  @return the antimatroid query record 
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(antimatroidRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.AntimatroidQueryRecord getAntimatroidQueryRecord(org.osid.type.Type antimatroidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.AntimatroidQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(antimatroidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(antimatroidRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  antimatroidRecordType the antimatroid record type 
     *  @return the antimatroid query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(antimatroidRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.AntimatroidQueryInspectorRecord getAntimatroidQueryInspectorRecord(org.osid.type.Type antimatroidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.AntimatroidQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(antimatroidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(antimatroidRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param antimatroidRecordType the antimatroid record type
     *  @return the antimatroid search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(antimatroidRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.AntimatroidSearchOrderRecord getAntimatroidSearchOrderRecord(org.osid.type.Type antimatroidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.AntimatroidSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(antimatroidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(antimatroidRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this antimatroid. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param antimatroidQueryRecord the antimatroid query record
     *  @param antimatroidQueryInspectorRecord the antimatroid query inspector
     *         record
     *  @param antimatroidSearchOrderRecord the antimatroid search order record
     *  @param antimatroidRecordType antimatroid record type
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidQueryRecord</code>,
     *          <code>antimatroidQueryInspectorRecord</code>,
     *          <code>antimatroidSearchOrderRecord</code> or
     *          <code>antimatroidRecordTypeantimatroid</code> is
     *          <code>null</code>
     */
            
    protected void addAntimatroidRecords(org.osid.sequencing.records.AntimatroidQueryRecord antimatroidQueryRecord, 
                                      org.osid.sequencing.records.AntimatroidQueryInspectorRecord antimatroidQueryInspectorRecord, 
                                      org.osid.sequencing.records.AntimatroidSearchOrderRecord antimatroidSearchOrderRecord, 
                                      org.osid.type.Type antimatroidRecordType) {

        addRecordType(antimatroidRecordType);

        nullarg(antimatroidQueryRecord, "antimatroid query record");
        nullarg(antimatroidQueryInspectorRecord, "antimatroid query inspector record");
        nullarg(antimatroidSearchOrderRecord, "antimatroid search odrer record");

        this.queryRecords.add(antimatroidQueryRecord);
        this.queryInspectorRecords.add(antimatroidQueryInspectorRecord);
        this.searchOrderRecords.add(antimatroidSearchOrderRecord);
        
        return;
    }
}
