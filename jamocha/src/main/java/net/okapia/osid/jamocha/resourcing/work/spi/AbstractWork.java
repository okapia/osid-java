//
// AbstractWork.java
//
//     Defines a Work.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.work.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Work</code>.
 */

public abstract class AbstractWork
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.resourcing.Work {

    private org.osid.resourcing.Job job;
    boolean needsCompetence = false;
    private final java.util.Collection<org.osid.resourcing.Competency> competencies = new java.util.LinkedHashSet<>();
    private org.osid.calendaring.DateTime createdDate;
    private org.osid.calendaring.DateTime completionDate;

    private final java.util.Collection<org.osid.resourcing.records.WorkRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the job of which this work is a part. 
     *
     *  @return the job <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getJobId() {
        return (this.job.getId());
    }


    /**
     *  Gets the job of which this work is a part. 
     *
     *  @return the job 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Job getJob()
        throws org.osid.OperationFailedException {

        return (this.job);
    }


    /**
     *  Sets the job.
     *
     *  @param job a job
     *  @throws org.osid.NullArgumentException
     *          <code>job</code> is <code>null</code>
     */

    protected void setJob(org.osid.resourcing.Job job) {
        nullarg(job, "job");
        this.job = job;
        return;
    }


    /**
     *  Tests if specific competencies are needed for this work. 
     *
     *  @return <code> true </code> if competency if specified, <code> false 
     *          </code> if incompetence will do 
     */

    @OSID @Override
    public boolean needsCompetence() {
        return (this.needsCompetence);
    }


    /**
     *  Gets the <code> Ids </code> of the competencies. 
     *
     *  @return the competency <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> needsCompetence() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCompetencyIds() {
        if (!needsCompetence()) {
            throw new org.osid.IllegalStateException("needsCompetence() is false");
        }

        try {
            org.osid.resourcing.CompetencyList competencies = getCompetencies();
            return (new net.okapia.osid.jamocha.adapter.converter.resourcing.competency.CompetencyToIdList(competencies));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the competency. 
     *
     *  @return the competency 
     *  @throws org.osid.IllegalStateException <code> needsCompetence() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetencies()
        throws org.osid.OperationFailedException {

        if (!needsCompetence()) {
            throw new org.osid.IllegalStateException("needsCompetence() is false");
        }

        return (new net.okapia.osid.jamocha.resourcing.competency.ArrayCompetencyList(this.competencies));
    }


    /**
     *  Adds a competency.
     *
     *  @param competency a competency
     *  @throws org.osid.NullArgumentException
     *          <code>competency</code> is <code>null</code>
     */

    protected void addCompetency(org.osid.resourcing.Competency competency) {
        nullarg(competency, "competency");
        this.competencies.add(competency);
        this.needsCompetence = true;
        return;
    }


    /**
     *  Sets all the competencies.
     *
     *  @param competencies a collection of competencies
     *  @throws org.osid.NullArgumentException
     *          <code>competencies</code> is <code>null</code>
     */

    protected void setCompetencies(java.util.Collection<org.osid.resourcing.Competency> competencies) {
        nullarg(competencies, "competencies");
        this.competencies.clear();
        this.competencies.addAll(competencies);
        this.needsCompetence = true;
        return;
    }


    /**
     *  Gets the date this work was created. 
     *
     *  @return the created date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreatedDate() {
        return (this.createdDate);
    }


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setCreatedDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "created date");
        this.createdDate = date;
        return;
    }


    /**
     *  Tests if this work has been completed. 
     *
     *  @return <code> true </code> if this work is complete, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.completionDate != null);
    }


    /**
     *  Gets the completion date. 
     *
     *  @return the completion date 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCompletionDate() {
        if (!isComplete()) {
            throw new org.osid.IllegalStateException("isCOmplete) is null");
        }

        return (this.completionDate);
    }


    /**
     *  Sets the completion date.
     *
     *  @param date a completion date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setCompletionDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "completion date");
        this.completionDate = date;
        return;
    }


    /**
     *  Tests if this work supports the given record
     *  <code>Type</code>.
     *
     *  @param  workRecordType a work record type 
     *  @return <code>true</code> if the workRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type workRecordType) {
        for (org.osid.resourcing.records.WorkRecord record : this.records) {
            if (record.implementsRecordType(workRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Work</code>
     *  record <code>Type</code>.
     *
     *  @param  workRecordType the work record type 
     *  @return the work record 
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.WorkRecord getWorkRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.WorkRecord record : this.records) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Adds a record to this work. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param workRecord the work record
     *  @param workRecordType work record type
     *  @throws org.osid.NullArgumentException
     *          <code>workRecord</code> or
     *          <code>workRecordTypework</code> is
     *          <code>null</code>
     */
            
    protected void addWorkRecord(org.osid.resourcing.records.WorkRecord workRecord, 
                                 org.osid.type.Type workRecordType) {

        nullarg(workRecord, "work record");
        addRecordType(workRecordType);
        this.records.add(workRecord);
        
        return;
    }
}
