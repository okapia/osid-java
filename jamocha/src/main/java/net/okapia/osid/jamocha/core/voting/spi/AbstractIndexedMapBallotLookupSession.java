//
// AbstractIndexedMapBallotLookupSession.java
//
//    A simple framework for providing a Ballot lookup service
//    backed by a fixed collection of ballots with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Ballot lookup service backed by a
 *  fixed collection of ballots. The ballots are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some ballots may be compatible
 *  with more types than are indicated through these ballot
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Ballots</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBallotLookupSession
    extends AbstractMapBallotLookupSession
    implements org.osid.voting.BallotLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.Ballot> ballotsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Ballot>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.Ballot> ballotsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Ballot>());


    /**
     *  Makes a <code>Ballot</code> available in this session.
     *
     *  @param  ballot a ballot
     *  @throws org.osid.NullArgumentException <code>ballot<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBallot(org.osid.voting.Ballot ballot) {
        super.putBallot(ballot);

        this.ballotsByGenus.put(ballot.getGenusType(), ballot);
        
        try (org.osid.type.TypeList types = ballot.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ballotsByRecord.put(types.getNextType(), ballot);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a ballot from this session.
     *
     *  @param ballotId the <code>Id</code> of the ballot
     *  @throws org.osid.NullArgumentException <code>ballotId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBallot(org.osid.id.Id ballotId) {
        org.osid.voting.Ballot ballot;
        try {
            ballot = getBallot(ballotId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.ballotsByGenus.remove(ballot.getGenusType());

        try (org.osid.type.TypeList types = ballot.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ballotsByRecord.remove(types.getNextType(), ballot);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBallot(ballotId);
        return;
    }


    /**
     *  Gets a <code>BallotList</code> corresponding to the given
     *  ballot genus <code>Type</code> which does not include
     *  ballots of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known ballots or an error results. Otherwise,
     *  the returned list may contain only those ballots that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  ballotGenusType a ballot genus type 
     *  @return the returned <code>Ballot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByGenusType(org.osid.type.Type ballotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.ballot.ArrayBallotList(this.ballotsByGenus.get(ballotGenusType)));
    }


    /**
     *  Gets a <code>BallotList</code> containing the given
     *  ballot record <code>Type</code>. In plenary mode, the
     *  returned list contains all known ballots or an error
     *  results. Otherwise, the returned list may contain only those
     *  ballots that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  ballotRecordType a ballot record type 
     *  @return the returned <code>ballot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByRecordType(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.ballot.ArrayBallotList(this.ballotsByRecord.get(ballotRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.ballotsByGenus.clear();
        this.ballotsByRecord.clear();

        super.close();

        return;
    }
}
