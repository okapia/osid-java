//
// AbstractAssemblyDeviceQuery.java
//
//     A DeviceQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.device.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DeviceQuery that stores terms.
 */

public abstract class AbstractAssemblyDeviceQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.control.DeviceQuery,
               org.osid.control.DeviceQueryInspector,
               org.osid.control.DeviceSearchOrder {

    private final java.util.Collection<org.osid.control.records.DeviceQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.DeviceQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.DeviceSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDeviceQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDeviceQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the system <code> Id </code> for this query. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this device supports the given record
     *  <code>Type</code>.
     *
     *  @param  deviceRecordType a device record type 
     *  @return <code>true</code> if the deviceRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>deviceRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type deviceRecordType) {
        for (org.osid.control.records.DeviceQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(deviceRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  deviceRecordType the device record type 
     *  @return the device query record 
     *  @throws org.osid.NullArgumentException
     *          <code>deviceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.DeviceQueryRecord getDeviceQueryRecord(org.osid.type.Type deviceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.DeviceQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(deviceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  deviceRecordType the device record type 
     *  @return the device query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>deviceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.DeviceQueryInspectorRecord getDeviceQueryInspectorRecord(org.osid.type.Type deviceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.DeviceQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(deviceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param deviceRecordType the device record type
     *  @return the device search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>deviceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.DeviceSearchOrderRecord getDeviceSearchOrderRecord(org.osid.type.Type deviceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.DeviceSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(deviceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this device. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param deviceQueryRecord the device query record
     *  @param deviceQueryInspectorRecord the device query inspector
     *         record
     *  @param deviceSearchOrderRecord the device search order record
     *  @param deviceRecordType device record type
     *  @throws org.osid.NullArgumentException
     *          <code>deviceQueryRecord</code>,
     *          <code>deviceQueryInspectorRecord</code>,
     *          <code>deviceSearchOrderRecord</code> or
     *          <code>deviceRecordTypedevice</code> is
     *          <code>null</code>
     */
            
    protected void addDeviceRecords(org.osid.control.records.DeviceQueryRecord deviceQueryRecord, 
                                      org.osid.control.records.DeviceQueryInspectorRecord deviceQueryInspectorRecord, 
                                      org.osid.control.records.DeviceSearchOrderRecord deviceSearchOrderRecord, 
                                      org.osid.type.Type deviceRecordType) {

        addRecordType(deviceRecordType);

        nullarg(deviceQueryRecord, "device query record");
        nullarg(deviceQueryInspectorRecord, "device query inspector record");
        nullarg(deviceSearchOrderRecord, "device search odrer record");

        this.queryRecords.add(deviceQueryRecord);
        this.queryInspectorRecords.add(deviceQueryInspectorRecord);
        this.searchOrderRecords.add(deviceSearchOrderRecord);
        
        return;
    }
}
