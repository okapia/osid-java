//
// AbstractAuctionProcessorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAuctionProcessorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.bidding.rules.AuctionProcessorSearchResults {

    private org.osid.bidding.rules.AuctionProcessorList auctionProcessors;
    private final org.osid.bidding.rules.AuctionProcessorQueryInspector inspector;
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAuctionProcessorSearchResults.
     *
     *  @param auctionProcessors the result set
     *  @param auctionProcessorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>auctionProcessors</code>
     *          or <code>auctionProcessorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAuctionProcessorSearchResults(org.osid.bidding.rules.AuctionProcessorList auctionProcessors,
                                            org.osid.bidding.rules.AuctionProcessorQueryInspector auctionProcessorQueryInspector) {
        nullarg(auctionProcessors, "auction processors");
        nullarg(auctionProcessorQueryInspector, "auction processor query inspectpr");

        this.auctionProcessors = auctionProcessors;
        this.inspector = auctionProcessorQueryInspector;

        return;
    }


    /**
     *  Gets the auction processor list resulting from a search.
     *
     *  @return an auction processor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessors() {
        if (this.auctionProcessors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.bidding.rules.AuctionProcessorList auctionProcessors = this.auctionProcessors;
        this.auctionProcessors = null;
	return (auctionProcessors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.bidding.rules.AuctionProcessorQueryInspector getAuctionProcessorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  auction processor search record <code> Type. </code> This method must
     *  be used to retrieve an auctionProcessor implementing the requested
     *  record.
     *
     *  @param auctionProcessorSearchRecordType an auctionProcessor search 
     *         record type 
     *  @return the auction processor search
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(auctionProcessorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorSearchResultsRecord getAuctionProcessorSearchResultsRecord(org.osid.type.Type auctionProcessorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.bidding.rules.records.AuctionProcessorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(auctionProcessorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(auctionProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record auction processor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAuctionProcessorRecord(org.osid.bidding.rules.records.AuctionProcessorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "auction processor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
