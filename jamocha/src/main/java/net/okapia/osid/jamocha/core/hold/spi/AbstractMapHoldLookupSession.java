//
// AbstractMapHoldLookupSession
//
//    A simple framework for providing a Hold lookup service
//    backed by a fixed collection of holds.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Hold lookup service backed by a
 *  fixed collection of holds. The holds are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Holds</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapHoldLookupSession
    extends net.okapia.osid.jamocha.hold.spi.AbstractHoldLookupSession
    implements org.osid.hold.HoldLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.hold.Hold> holds = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.hold.Hold>());


    /**
     *  Makes a <code>Hold</code> available in this session.
     *
     *  @param  hold a hold
     *  @throws org.osid.NullArgumentException <code>hold<code>
     *          is <code>null</code>
     */

    protected void putHold(org.osid.hold.Hold hold) {
        this.holds.put(hold.getId(), hold);
        return;
    }


    /**
     *  Makes an array of holds available in this session.
     *
     *  @param  holds an array of holds
     *  @throws org.osid.NullArgumentException <code>holds<code>
     *          is <code>null</code>
     */

    protected void putHolds(org.osid.hold.Hold[] holds) {
        putHolds(java.util.Arrays.asList(holds));
        return;
    }


    /**
     *  Makes a collection of holds available in this session.
     *
     *  @param  holds a collection of holds
     *  @throws org.osid.NullArgumentException <code>holds<code>
     *          is <code>null</code>
     */

    protected void putHolds(java.util.Collection<? extends org.osid.hold.Hold> holds) {
        for (org.osid.hold.Hold hold : holds) {
            this.holds.put(hold.getId(), hold);
        }

        return;
    }


    /**
     *  Removes a Hold from this session.
     *
     *  @param  holdId the <code>Id</code> of the hold
     *  @throws org.osid.NullArgumentException <code>holdId<code> is
     *          <code>null</code>
     */

    protected void removeHold(org.osid.id.Id holdId) {
        this.holds.remove(holdId);
        return;
    }


    /**
     *  Gets the <code>Hold</code> specified by its <code>Id</code>.
     *
     *  @param  holdId <code>Id</code> of the <code>Hold</code>
     *  @return the hold
     *  @throws org.osid.NotFoundException <code>holdId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>holdId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Hold getHold(org.osid.id.Id holdId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.hold.Hold hold = this.holds.get(holdId);
        if (hold == null) {
            throw new org.osid.NotFoundException("hold not found: " + holdId);
        }

        return (hold);
    }


    /**
     *  Gets all <code>Holds</code>. In plenary mode, the returned
     *  list contains all known holds or an error
     *  results. Otherwise, the returned list may contain only those
     *  holds that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Holds</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHolds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.hold.ArrayHoldList(this.holds.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.holds.clear();
        super.close();
        return;
    }
}
