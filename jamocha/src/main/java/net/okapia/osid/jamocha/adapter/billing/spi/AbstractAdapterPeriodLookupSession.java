//
// AbstractAdapterPeriodLookupSession.java
//
//    A Period lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Period lookup session adapter.
 */

public abstract class AbstractAdapterPeriodLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.billing.PeriodLookupSession {

    private final org.osid.billing.PeriodLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPeriodLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPeriodLookupSession(org.osid.billing.PeriodLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code Period} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPeriods() {
        return (this.session.canLookupPeriods());
    }


    /**
     *  A complete view of the {@code Period} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePeriodView() {
        this.session.useComparativePeriodView();
        return;
    }


    /**
     *  A complete view of the {@code Period} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPeriodView() {
        this.session.usePlenaryPeriodView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include periods in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the {@code Period} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Period} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Period} and
     *  retained for compatibility.
     *
     *  @param periodId {@code Id} of the {@code Period}
     *  @return the period
     *  @throws org.osid.NotFoundException {@code periodId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code periodId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod(org.osid.id.Id periodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPeriod(periodId));
    }


    /**
     *  Gets a {@code PeriodList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  periods specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Periods} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  periodIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Period} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code periodIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByIds(org.osid.id.IdList periodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPeriodsByIds(periodIds));
    }


    /**
     *  Gets a {@code PeriodList} corresponding to the given
     *  period genus {@code Type} which does not include
     *  periods of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  periodGenusType a period genus type 
     *  @return the returned {@code Period} list
     *  @throws org.osid.NullArgumentException
     *          {@code periodGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByGenusType(org.osid.type.Type periodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPeriodsByGenusType(periodGenusType));
    }


    /**
     *  Gets a {@code PeriodList} corresponding to the given
     *  period genus {@code Type} and include any additional
     *  periods with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  periodGenusType a period genus type 
     *  @return the returned {@code Period} list
     *  @throws org.osid.NullArgumentException
     *          {@code periodGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByParentGenusType(org.osid.type.Type periodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPeriodsByParentGenusType(periodGenusType));
    }


    /**
     *  Gets a {@code PeriodList} containing the given
     *  period record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  periodRecordType a period record type 
     *  @return the returned {@code Period} list
     *  @throws org.osid.NullArgumentException
     *          {@code periodRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByRecordType(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPeriodsByRecordType(periodRecordType));
    }


    /**
     *  Gets a {@code PeriodList} where to the given {@code DateTime}
     *  range falls within the period inclusive. Periods containing
     *  the given date are matched. In plenary mode, the returned list
     *  contains all of the periods specified in the {@code Id} list,
     *  in the order of the list, including duplicates, or an error
     *  results if an {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Periods} may be
     *  omitted from the list including returning a unique set.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Period} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or {@code
     *          to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsOnDate(org.osid.calendaring.DateTime from, 
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPeriodsOnDate(from, to));
    }


    /**
     *  Gets all {@code Periods}. 
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Periods} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPeriods());
    }
}
