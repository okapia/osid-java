//
// AbstractIndexedMapAssessmentOfferedLookupSession.java
//
//    A simple framework for providing an AssessmentOffered lookup service
//    backed by a fixed collection of assessments offered with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AssessmentOffered lookup service backed by a
 *  fixed collection of assessments offered. The assessments offered are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some assessments offered may be compatible
 *  with more types than are indicated through these assessment offered
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AssessmentsOffered</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAssessmentOfferedLookupSession
    extends AbstractMapAssessmentOfferedLookupSession
    implements org.osid.assessment.AssessmentOfferedLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.assessment.AssessmentOffered> assessmentsOfferedByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.AssessmentOffered>());
    private final MultiMap<org.osid.type.Type, org.osid.assessment.AssessmentOffered> assessmentsOfferedByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.AssessmentOffered>());


    /**
     *  Makes an <code>AssessmentOffered</code> available in this session.
     *
     *  @param  assessmentOffered an assessment offered
     *  @throws org.osid.NullArgumentException <code>assessmentOffered<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAssessmentOffered(org.osid.assessment.AssessmentOffered assessmentOffered) {
        super.putAssessmentOffered(assessmentOffered);

        this.assessmentsOfferedByGenus.put(assessmentOffered.getGenusType(), assessmentOffered);
        
        try (org.osid.type.TypeList types = assessmentOffered.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentsOfferedByRecord.put(types.getNextType(), assessmentOffered);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an assessment offered from this session.
     *
     *  @param assessmentOfferedId the <code>Id</code> of the assessment offered
     *  @throws org.osid.NullArgumentException <code>assessmentOfferedId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAssessmentOffered(org.osid.id.Id assessmentOfferedId) {
        org.osid.assessment.AssessmentOffered assessmentOffered;
        try {
            assessmentOffered = getAssessmentOffered(assessmentOfferedId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.assessmentsOfferedByGenus.remove(assessmentOffered.getGenusType());

        try (org.osid.type.TypeList types = assessmentOffered.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentsOfferedByRecord.remove(types.getNextType(), assessmentOffered);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAssessmentOffered(assessmentOfferedId);
        return;
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  assessment offered genus <code>Type</code> which does not include
     *  assessments offered of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known assessments offered or an error results. Otherwise,
     *  the returned list may contain only those assessments offered that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  assessmentOfferedGenusType an assessment offered genus type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByGenusType(org.osid.type.Type assessmentOfferedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.assessmentoffered.ArrayAssessmentOfferedList(this.assessmentsOfferedByGenus.get(assessmentOfferedGenusType)));
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> containing the given
     *  assessment offered record <code>Type</code>. In plenary mode, the
     *  returned list contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  assessmentOfferedRecordType an assessment offered record type 
     *  @return the returned <code>assessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByRecordType(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.assessmentoffered.ArrayAssessmentOfferedList(this.assessmentsOfferedByRecord.get(assessmentOfferedRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessmentsOfferedByGenus.clear();
        this.assessmentsOfferedByRecord.clear();

        super.close();

        return;
    }
}
