//
// SourceableElements.java
//
//     Id definitions for OsidForm and OsidQuery fields.
//
//
// Tom Coppeto
// Okapia
// 15 May 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import net.okapia.osid.primordium.id.BasicId;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Ids for OsidForm and OsidQuery elements. These are used in
 *  Metadata and may also be used as a key for relational mapping.
 */

public class SourceableElements
    extends Elements {


    /**
     *  Gets the provider Id element Id.
     *
     *  @return the provider Id element Id
     */

    public static org.osid.id.Id getProviderId() {
        return (makeElementId("osid.Sourceable.ProviderId"));
    }


    /**
     *  Gets the provider element Id.
     *
     *  @return the provider element Id
     */

    public static org.osid.id.Id getProvider() {
        return (makeElementId("osid.Sourceable.Provider"));
    }


    /**
     *  Gets the branding Id element Id.
     *
     *  @return the branding Id element Id
     */

    public static org.osid.id.Id getBrandingId() {
        return (makeElementId("osid.Sourceable.BrandingId"));
    }


    /**
     *  Gets the branding element Id.
     *
     *  @return the branding element Id
     */

    public static org.osid.id.Id getBranding() {
        return (makeElementId("osid.Sourceable.Branding"));
    }


    /**
     *  Gets the license element Id.
     *
     *  @return the license element Id
     */

    public static org.osid.id.Id getLicense() {
        return (makeElementId("osid.Sourceable.License"));
    }
}
