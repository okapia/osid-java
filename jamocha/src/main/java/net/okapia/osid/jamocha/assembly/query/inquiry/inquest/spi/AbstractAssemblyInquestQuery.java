//
// AbstractAssemblyInquestQuery.java
//
//     An InquestQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inquiry.inquest.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InquestQuery that stores terms.
 */

public abstract class AbstractAssemblyInquestQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.inquiry.InquestQuery,
               org.osid.inquiry.InquestQueryInspector,
               org.osid.inquiry.InquestSearchOrder {

    private final java.util.Collection<org.osid.inquiry.records.InquestQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.InquestQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.InquestSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyInquestQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyInquestQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the inquiry <code> Id </code> for this query to match inquests 
     *  that have a related response. 
     *
     *  @param  inquiryId an inquiry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> inquiryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquiryId(org.osid.id.Id inquiryId, boolean match) {
        getAssembler().addIdTerm(getInquiryIdColumn(), inquiryId, match);
        return;
    }


    /**
     *  Clears the inquiry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquiryIdTerms() {
        getAssembler().clearTerms(getInquiryIdColumn());
        return;
    }


    /**
     *  Gets the inquiry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquiryIdTerms() {
        return (getAssembler().getIdTerms(getInquiryIdColumn()));
    }


    /**
     *  Gets the InquiryId column name.
     *
     * @return the column name
     */

    protected String getInquiryIdColumn() {
        return ("inquiry_id");
    }


    /**
     *  Tests if an <code> InquiryQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquiry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquiry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquiry query 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuery getInquiryQuery() {
        throw new org.osid.UnimplementedException("supportsInquiryQuery() is false");
    }


    /**
     *  Matches inquests that have any inquiry. 
     *
     *  @param  match <code> true </code> to match inquests with any inquiry, 
     *          <code> false </code> to match inquests with no inquiry 
     */

    @OSID @Override
    public void matchAnyInquiry(boolean match) {
        getAssembler().addIdWildcardTerm(getInquiryColumn(), match);
        return;
    }


    /**
     *  Clears the inquiry query terms. 
     */

    @OSID @Override
    public void clearInquiryTerms() {
        getAssembler().clearTerms(getInquiryColumn());
        return;
    }


    /**
     *  Gets the inquiry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQueryInspector[] getInquiryTerms() {
        return (new org.osid.inquiry.InquiryQueryInspector[0]);
    }


    /**
     *  Gets the Inquiry column name.
     *
     * @return the column name
     */

    protected String getInquiryColumn() {
        return ("inquiry");
    }


    /**
     *  Sets the audit <code> Id </code> for this query. 
     *
     *  @param  auditId the audit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auditId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAuditId(org.osid.id.Id auditId, boolean match) {
        getAssembler().addIdTerm(getAuditIdColumn(), auditId, match);
        return;
    }


    /**
     *  Clears the audit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuditIdTerms() {
        getAssembler().clearTerms(getAuditIdColumn());
        return;
    }


    /**
     *  Gets the audit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuditIdTerms() {
        return (getAssembler().getIdTerms(getAuditIdColumn()));
    }


    /**
     *  Gets the AuditId column name.
     *
     * @return the column name
     */

    protected String getAuditIdColumn() {
        return ("audit_id");
    }


    /**
     *  Tests if an <code> AuditQuery </code> is available. 
     *
     *  @return <code> true </code> if an audit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an audit. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the audit query 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuery getAuditQuery() {
        throw new org.osid.UnimplementedException("supportsAuditQuery() is false");
    }


    /**
     *  Matches inquests with any audit. 
     *
     *  @param  match <code> true </code> to match inquests with any audit, 
     *          <code> false </code> to match inquests with no audit 
     */

    @OSID @Override
    public void matchAnyAudit(boolean match) {
        getAssembler().addIdWildcardTerm(getAuditColumn(), match);
        return;
    }


    /**
     *  Clears the audit query terms. 
     */

    @OSID @Override
    public void clearAuditTerms() {
        getAssembler().clearTerms(getAuditColumn());
        return;
    }


    /**
     *  Gets the audit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQueryInspector[] getAuditTerms() {
        return (new org.osid.inquiry.AuditQueryInspector[0]);
    }


    /**
     *  Gets the Audit column name.
     *
     * @return the column name
     */

    protected String getAuditColumn() {
        return ("audit");
    }


    /**
     *  Sets the response <code> Id </code> for this query. 
     *
     *  @param  responseId the response <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> responseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResponseId(org.osid.id.Id responseId, boolean match) {
        getAssembler().addIdTerm(getResponseIdColumn(), responseId, match);
        return;
    }


    /**
     *  Clears the response <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResponseIdTerms() {
        getAssembler().clearTerms(getResponseIdColumn());
        return;
    }


    /**
     *  Gets the response <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResponseIdTerms() {
        return (getAssembler().getIdTerms(getResponseIdColumn()));
    }


    /**
     *  Gets the ResponseId column name.
     *
     * @return the column name
     */

    protected String getResponseIdColumn() {
        return ("response_id");
    }


    /**
     *  Tests if a <code> ResponseQuery </code> is available. 
     *
     *  @return <code> true </code> if a response query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a response. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the response query 
     *  @throws org.osid.UnimplementedException <code> supportsResponseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQuery getResponseQuery() {
        throw new org.osid.UnimplementedException("supportsResponseQuery() is false");
    }


    /**
     *  Matches inquests with any response. 
     *
     *  @param  match <code> true </code> to match inquests with any response, 
     *          <code> false </code> to match inquests with no response 
     */

    @OSID @Override
    public void matchAnyResponse(boolean match) {
        getAssembler().addIdWildcardTerm(getResponseColumn(), match);
        return;
    }


    /**
     *  Clears the response query terms. 
     */

    @OSID @Override
    public void clearResponseTerms() {
        getAssembler().clearTerms(getResponseColumn());
        return;
    }


    /**
     *  Gets the response query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQueryInspector[] getResponseTerms() {
        return (new org.osid.inquiry.ResponseQueryInspector[0]);
    }


    /**
     *  Gets the Response column name.
     *
     * @return the column name
     */

    protected String getResponseColumn() {
        return ("response");
    }


    /**
     *  Sets the inquest <code> Id </code> for this query to match inquests 
     *  that have the specified inquest as an ancestor. 
     *
     *  @param  inquestId an inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorInquestId(org.osid.id.Id inquestId, boolean match) {
        getAssembler().addIdTerm(getAncestorInquestIdColumn(), inquestId, match);
        return;
    }


    /**
     *  Clears the ancestor inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorInquestIdTerms() {
        getAssembler().clearTerms(getAncestorInquestIdColumn());
        return;
    }


    /**
     *  Gets the ancestor inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorInquestIdTerms() {
        return (getAssembler().getIdTerms(getAncestorInquestIdColumn()));
    }


    /**
     *  Gets the AncestorInquestId column name.
     *
     * @return the column name
     */

    protected String getAncestorInquestIdColumn() {
        return ("ancestor_inquest_id");
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorInquestQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getAncestorInquestQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorInquestQuery() is false");
    }


    /**
     *  Matches inquests with any ancestor. 
     *
     *  @param  match <code> true </code> to match inquests with any ancestor, 
     *          <code> false </code> to match root inquests 
     */

    @OSID @Override
    public void matchAnyAncestorInquest(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorInquestColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor inquest query terms. 
     */

    @OSID @Override
    public void clearAncestorInquestTerms() {
        getAssembler().clearTerms(getAncestorInquestColumn());
        return;
    }


    /**
     *  Gets the ancestor inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getAncestorInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }


    /**
     *  Gets the AncestorInquest column name.
     *
     * @return the column name
     */

    protected String getAncestorInquestColumn() {
        return ("ancestor_inquest");
    }


    /**
     *  Sets the inquest <code> Id </code> for this query to match inquests 
     *  that have the specified inquest as a descendant. 
     *
     *  @param  inquestId an inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantInquestId(org.osid.id.Id inquestId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantInquestIdColumn(), inquestId, match);
        return;
    }


    /**
     *  Clears the descendant inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantInquestIdTerms() {
        getAssembler().clearTerms(getDescendantInquestIdColumn());
        return;
    }


    /**
     *  Gets the descendant inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantInquestIdTerms() {
        return (getAssembler().getIdTerms(getDescendantInquestIdColumn()));
    }


    /**
     *  Gets the DescendantInquestId column name.
     *
     * @return the column name
     */

    protected String getDescendantInquestIdColumn() {
        return ("descendant_inquest_id");
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantInquestQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getDescendantInquestQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantInquestQuery() is false");
    }


    /**
     *  Matches inquests with any descendant. 
     *
     *  @param  match <code> true </code> to match inquests with any 
     *          descendant, <code> false </code> to match leaf inquests 
     */

    @OSID @Override
    public void matchAnyDescendantInquest(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantInquestColumn(), match);
        return;
    }


    /**
     *  Clears the descendant inquest query terms. 
     */

    @OSID @Override
    public void clearDescendantInquestTerms() {
        getAssembler().clearTerms(getDescendantInquestColumn());
        return;
    }


    /**
     *  Gets the descendant inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getDescendantInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }


    /**
     *  Gets the DescendantInquest column name.
     *
     * @return the column name
     */

    protected String getDescendantInquestColumn() {
        return ("descendant_inquest");
    }


    /**
     *  Tests if this inquest supports the given record
     *  <code>Type</code>.
     *
     *  @param  inquestRecordType an inquest record type 
     *  @return <code>true</code> if the inquestRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>inquestRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inquestRecordType) {
        for (org.osid.inquiry.records.InquestQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inquestRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  inquestRecordType the inquest record type 
     *  @return the inquest query record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquestRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquestRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquestQueryRecord getInquestQueryRecord(org.osid.type.Type inquestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquestQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inquestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquestRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  inquestRecordType the inquest record type 
     *  @return the inquest query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquestRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquestRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquestQueryInspectorRecord getInquestQueryInspectorRecord(org.osid.type.Type inquestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquestQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(inquestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquestRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param inquestRecordType the inquest record type
     *  @return the inquest search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquestRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquestRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquestSearchOrderRecord getInquestSearchOrderRecord(org.osid.type.Type inquestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquestSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(inquestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquestRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this inquest. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inquestQueryRecord the inquest query record
     *  @param inquestQueryInspectorRecord the inquest query inspector
     *         record
     *  @param inquestSearchOrderRecord the inquest search order record
     *  @param inquestRecordType inquest record type
     *  @throws org.osid.NullArgumentException
     *          <code>inquestQueryRecord</code>,
     *          <code>inquestQueryInspectorRecord</code>,
     *          <code>inquestSearchOrderRecord</code> or
     *          <code>inquestRecordTypeinquest</code> is
     *          <code>null</code>
     */
            
    protected void addInquestRecords(org.osid.inquiry.records.InquestQueryRecord inquestQueryRecord, 
                                      org.osid.inquiry.records.InquestQueryInspectorRecord inquestQueryInspectorRecord, 
                                      org.osid.inquiry.records.InquestSearchOrderRecord inquestSearchOrderRecord, 
                                      org.osid.type.Type inquestRecordType) {

        addRecordType(inquestRecordType);

        nullarg(inquestQueryRecord, "inquest query record");
        nullarg(inquestQueryInspectorRecord, "inquest query inspector record");
        nullarg(inquestSearchOrderRecord, "inquest search odrer record");

        this.queryRecords.add(inquestQueryRecord);
        this.queryInspectorRecords.add(inquestQueryInspectorRecord);
        this.searchOrderRecords.add(inquestSearchOrderRecord);
        
        return;
    }
}
