//
// InvariantIndexedMapProxyLogEntryLookupSession
//
//    Implements a LogEntry lookup service backed by a fixed
//    collection of logEntries indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking;


/**
 *  Implements a LogEntry lookup service backed by a fixed
 *  collection of logEntries. The logEntries are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some logEntries may be compatible
 *  with more types than are indicated through these logEntry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyLogEntryLookupSession
    extends net.okapia.osid.jamocha.core.tracking.spi.AbstractIndexedMapLogEntryLookupSession
    implements org.osid.tracking.LogEntryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyLogEntryLookupSession}
     *  using an array of log entries.
     *
     *  @param frontOffice the front office
     *  @param logEntries an array of log entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code logEntries} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyLogEntryLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                         org.osid.tracking.LogEntry[] logEntries, 
                                                         org.osid.proxy.Proxy proxy) {

        setFrontOffice(frontOffice);
        setSessionProxy(proxy);
        putLogEntries(logEntries);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyLogEntryLookupSession}
     *  using a collection of log entries.
     *
     *  @param frontOffice the front office
     *  @param logEntries a collection of log entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code logEntries} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyLogEntryLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                         java.util.Collection<? extends org.osid.tracking.LogEntry> logEntries,
                                                         org.osid.proxy.Proxy proxy) {

        setFrontOffice(frontOffice);
        setSessionProxy(proxy);
        putLogEntries(logEntries);

        return;
    }
}
