//
// AbstractInquiryManager.java
//
//     An adapter for a InquiryManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a InquiryManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterInquiryManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.inquiry.InquiryManager>
    implements org.osid.inquiry.InquiryManager {


    /**
     *  Constructs a new {@code AbstractAdapterInquiryManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterInquiryManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterInquiryManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterInquiryManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any action group federation is exposed. Federation is exposed 
     *  when a specific action group may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of action groups appears as a single action group. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if an acknowledgement service is supported for the current 
     *  agent. 
     *
     *  @return <code> true </code> if acknowledgement is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgement() {
        return (getAdapteeManager().supportsAcknowledgement());
    }


    /**
     *  Tests if an acknowledgement notification service is supported for the 
     *  current agent. 
     *
     *  @return <code> true </code> if an acknowledgement notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgementNotification() {
        return (getAdapteeManager().supportsAcknowledgementNotification());
    }


    /**
     *  Tests if looking up inquiries is supported. 
     *
     *  @return <code> true </code> if inquiry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryLookup() {
        return (getAdapteeManager().supportsInquiryLookup());
    }


    /**
     *  Tests if querying inquiries is supported. 
     *
     *  @return <code> true </code> if inquiry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryQuery() {
        return (getAdapteeManager().supportsInquiryQuery());
    }


    /**
     *  Tests if searching inquiries is supported. 
     *
     *  @return <code> true </code> if inquiry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquirySearch() {
        return (getAdapteeManager().supportsInquirySearch());
    }


    /**
     *  Tests if an inquiry administrative service is supported. 
     *
     *  @return <code> true </code> if inquiry administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryAdmin() {
        return (getAdapteeManager().supportsInquiryAdmin());
    }


    /**
     *  Tests if an inquiry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if inquiry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryNotification() {
        return (getAdapteeManager().supportsInquiryNotification());
    }


    /**
     *  Tests if an inquiry inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an inquiry inquest lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryInquest() {
        return (getAdapteeManager().supportsInquiryInquest());
    }


    /**
     *  Tests if an inquiry inquest assignment service is supported. 
     *
     *  @return <code> true </code> if an inquiry to inquest assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryInquestAssignment() {
        return (getAdapteeManager().supportsInquiryInquestAssignment());
    }


    /**
     *  Tests if an inquiry smart inquest service is supported. 
     *
     *  @return <code> true </code> if a smart inquest service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquirySmartInquest() {
        return (getAdapteeManager().supportsInquirySmartInquest());
    }


    /**
     *  Tests if looking up audits is supported. 
     *
     *  @return <code> true </code> if audit lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditLookup() {
        return (getAdapteeManager().supportsAuditLookup());
    }


    /**
     *  Tests if querying audits is supported. 
     *
     *  @return <code> true </code> if audit query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditQuery() {
        return (getAdapteeManager().supportsAuditQuery());
    }


    /**
     *  Tests if searching audits is supported. 
     *
     *  @return <code> true </code> if audit search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditSearch() {
        return (getAdapteeManager().supportsAuditSearch());
    }


    /**
     *  Tests if audit administrative service is supported. 
     *
     *  @return <code> true </code> if audit administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditAdmin() {
        return (getAdapteeManager().supportsAuditAdmin());
    }


    /**
     *  Tests if an audit notification service is supported. 
     *
     *  @return <code> true </code> if audit notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditNotification() {
        return (getAdapteeManager().supportsAuditNotification());
    }


    /**
     *  Tests if an audit inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an audit inquest lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditInquest() {
        return (getAdapteeManager().supportsAuditInquest());
    }


    /**
     *  Tests if an audit inquest service is supported. 
     *
     *  @return <code> true </code> if audit to inquest assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditInquestAssignment() {
        return (getAdapteeManager().supportsAuditInquestAssignment());
    }


    /**
     *  Tests if an audit smart inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an audit smart inquest service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditSmartInquest() {
        return (getAdapteeManager().supportsAuditSmartInquest());
    }


    /**
     *  Tests if looking up responses is supported. 
     *
     *  @return <code> true </code> if response lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseLookup() {
        return (getAdapteeManager().supportsResponseLookup());
    }


    /**
     *  Tests if querying responses is supported. 
     *
     *  @return <code> true </code> if response query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseQuery() {
        return (getAdapteeManager().supportsResponseQuery());
    }


    /**
     *  Tests if searching responses is supported. 
     *
     *  @return <code> true </code> if response search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseSearch() {
        return (getAdapteeManager().supportsResponseSearch());
    }


    /**
     *  Tests if response <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if response administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseAdmin() {
        return (getAdapteeManager().supportsResponseAdmin());
    }


    /**
     *  Tests if a response <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if response notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseNotification() {
        return (getAdapteeManager().supportsResponseNotification());
    }


    /**
     *  Tests if a response inquest lookup service is supported. 
     *
     *  @return <code> true </code> if a response inquest lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseInquest() {
        return (getAdapteeManager().supportsResponseInquest());
    }


    /**
     *  Tests if a response inquest assignment service is supported. 
     *
     *  @return <code> true </code> if a response to inquest assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseInquestAssignment() {
        return (getAdapteeManager().supportsResponseInquestAssignment());
    }


    /**
     *  Tests if a response smart inquest service is supported. 
     *
     *  @return <code> true </code> if a response smart inquest service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseSmartInquest() {
        return (getAdapteeManager().supportsResponseSmartInquest());
    }


    /**
     *  Tests if looking up inquests is supported. 
     *
     *  @return <code> true </code> if inquest lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestLookup() {
        return (getAdapteeManager().supportsInquestLookup());
    }


    /**
     *  Tests if querying inquests is supported. 
     *
     *  @return <code> true </code> if an inquest query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestQuery() {
        return (getAdapteeManager().supportsInquestQuery());
    }


    /**
     *  Tests if searching inquests is supported. 
     *
     *  @return <code> true </code> if inquest search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestSearch() {
        return (getAdapteeManager().supportsInquestSearch());
    }


    /**
     *  Tests if inquest administrative service is supported. 
     *
     *  @return <code> true </code> if inquest administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestAdmin() {
        return (getAdapteeManager().supportsInquestAdmin());
    }


    /**
     *  Tests if an inquest <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if inquest notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestNotification() {
        return (getAdapteeManager().supportsInquestNotification());
    }


    /**
     *  Tests for the availability of an inquest hierarchy traversal service. 
     *
     *  @return <code> true </code> if inquest hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestHierarchy() {
        return (getAdapteeManager().supportsInquestHierarchy());
    }


    /**
     *  Tests for the availability of an inquest hierarchy design service. 
     *
     *  @return <code> true </code> if inquest hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestHierarchyDesign() {
        return (getAdapteeManager().supportsInquestHierarchyDesign());
    }


    /**
     *  Tests for the availability of a inquiry batch service. 
     *
     *  @return <code> true </code> if inquiry batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryBatch() {
        return (getAdapteeManager().supportsInquiryBatch());
    }


    /**
     *  Tests for the availability of a inquiry rules service. 
     *
     *  @return <code> true </code> if inquiry rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryRules() {
        return (getAdapteeManager().supportsInquiryRules());
    }


    /**
     *  Gets the supported <code> Inquiry </code> record types. 
     *
     *  @return a list containing the supported <code> Inquiry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquiryRecordTypes() {
        return (getAdapteeManager().getInquiryRecordTypes());
    }


    /**
     *  Tests if the given <code> Inquiry </code> record type is supported. 
     *
     *  @param  inquiryRecordType a <code> Type </code> indicating a <code> 
     *          Inquiry </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquiryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquiryRecordType(org.osid.type.Type inquiryRecordType) {
        return (getAdapteeManager().supportsInquiryRecordType(inquiryRecordType));
    }


    /**
     *  Gets the supported <code> Inquiry </code> search types. 
     *
     *  @return a list containing the supported <code> Inquiry </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquirySearchRecordTypes() {
        return (getAdapteeManager().getInquirySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Inquiry </code> search type is supported. 
     *
     *  @param  inquirySearchRecordType a <code> Type </code> indicating a 
     *          <code> Inquiry </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquirySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquirySearchRecordType(org.osid.type.Type inquirySearchRecordType) {
        return (getAdapteeManager().supportsInquirySearchRecordType(inquirySearchRecordType));
    }


    /**
     *  Gets the supported <code> Audit </code> record types. 
     *
     *  @return a list containing the supported <code> Audit </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuditRecordTypes() {
        return (getAdapteeManager().getAuditRecordTypes());
    }


    /**
     *  Tests if the given <code> Audit </code> record type is supported. 
     *
     *  @param  auditRecordType a <code> Type </code> indicating a <code> 
     *          Audit </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auditRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuditRecordType(org.osid.type.Type auditRecordType) {
        return (getAdapteeManager().supportsAuditRecordType(auditRecordType));
    }


    /**
     *  Gets the supported <code> Audit </code> search record types. 
     *
     *  @return a list containing the supported <code> Audit </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuditSearchRecordTypes() {
        return (getAdapteeManager().getAuditSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Audit </code> search record type is 
     *  supported. 
     *
     *  @param  auditSearchRecordType a <code> Type </code> indicating a 
     *          <code> Audit </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auditSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuditSearchRecordType(org.osid.type.Type auditSearchRecordType) {
        return (getAdapteeManager().supportsAuditSearchRecordType(auditSearchRecordType));
    }


    /**
     *  Gets the supported <code> Response </code> record types. 
     *
     *  @return a list containing the supported <code> Response </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseRecordTypes() {
        return (getAdapteeManager().getResponseRecordTypes());
    }


    /**
     *  Tests if the given <code> Response </code> record type is supported. 
     *
     *  @param  responseRecordType a <code> Type </code> indicating a <code> 
     *          Response </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseRecordType(org.osid.type.Type responseRecordType) {
        return (getAdapteeManager().supportsResponseRecordType(responseRecordType));
    }


    /**
     *  Gets the supported <code> Response </code> search types. 
     *
     *  @return a list containing the supported <code> Response </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseSearchRecordTypes() {
        return (getAdapteeManager().getResponseSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Response </code> search type is supported. 
     *
     *  @param  responseSearchRecordType a <code> Type </code> indicating a 
     *          <code> Response </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseSearchRecordType(org.osid.type.Type responseSearchRecordType) {
        return (getAdapteeManager().supportsResponseSearchRecordType(responseSearchRecordType));
    }


    /**
     *  Gets the supported <code> Inquest </code> record types. 
     *
     *  @return a list containing the supported <code> Inquest </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquestRecordTypes() {
        return (getAdapteeManager().getInquestRecordTypes());
    }


    /**
     *  Tests if the given <code> Inquest </code> record type is supported. 
     *
     *  @param  inquestRecordType a <code> Type </code> indicating a <code> 
     *          Inquest </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquestRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquestRecordType(org.osid.type.Type inquestRecordType) {
        return (getAdapteeManager().supportsInquestRecordType(inquestRecordType));
    }


    /**
     *  Gets the supported <code> Inquest </code> search record types. 
     *
     *  @return a list containing the supported <code> Inquest </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquestSearchRecordTypes() {
        return (getAdapteeManager().getInquestSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Inquest </code> search record type is 
     *  supported. 
     *
     *  @param  inquestSearchRecordType a <code> Type </code> indicating a 
     *          <code> Inquest </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquestSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquestSearchRecordType(org.osid.type.Type inquestSearchRecordType) {
        return (getAdapteeManager().supportsInquestSearchRecordType(inquestSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement service. 
     *
     *  @return an <code> AcknowledgementSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementSession getAcknowledgementSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcknowledgementSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @return an <code> AcknowledgementSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgement() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementSession getAcknowledgementSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAcknowledgementSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement notification service for inquiries related to the 
     *  authentciated agent. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @return an <code> AcknowledgementNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementNotificationSession getAcknowledgementNotificationSession(org.osid.inquiry.InquiryReceiver inquiryReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcknowledgementNotificationSession(inquiryReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement notification service for the given inquest for 
     *  inquiries related to the authentciated agent. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AcknowledgementNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementNotificationSession getAcknowledgementNotificationSessionForInquest(org.osid.inquiry.InquiryReceiver inquiryReceiver, 
                                                                                                               org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAcknowledgementNotificationSessionForInquest(inquiryReceiver, inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry lookup 
     *  service. 
     *
     *  @return an <code> InquiryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryLookupSession getInquiryLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry lookup 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryLookupSession getInquiryLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryLookupSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry query 
     *  service. 
     *
     *  @return an <code> InquiryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuerySession getInquiryQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry query 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryQuerySession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuerySession getInquiryQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryQuerySessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry search 
     *  service. 
     *
     *  @return an <code> InquirySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquirySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySearchSession getInquirySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquirySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry search 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquirySearchSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquirySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySearchSession getInquirySearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquirySearchSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  administration service. 
     *
     *  @return an <code> InquiryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryAdminSession getInquiryAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryAdminSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryAdminSession getInquiryAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryAdminSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  notification service. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @return an <code> InquiryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryNotificationSession getInquiryNotificationSession(org.osid.inquiry.InquiryReceiver inquiryReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryNotificationSession(inquiryReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  notification service for the given inquest. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryNotificationSession getInquiryNotificationSessionForInquest(org.osid.inquiry.InquiryReceiver inquiryReceiver, 
                                                                                               org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryNotificationSessionForInquest(inquiryReceiver, inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup inquiry/inquest 
     *  mappings. 
     *
     *  @return an <code> InquiryInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryInquestSession getInquiryInquestSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryInquestSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  inquiries to inquests. 
     *
     *  @return an <code> InquiryInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryInquestAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryInquestAssignmentSession getInquiryInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryInquestAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage inquiry smart inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquirySmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquirySmartInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySmartInquestSession getInquirySmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquirySmartInquestSession(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit lookup 
     *  service. 
     *
     *  @return an <code> AuditLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditLookupSession getAuditLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit lookup 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditLookupSession getAuditLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditLookupSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit query 
     *  service. 
     *
     *  @return an <code> AuditQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuerySession getAuditQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit query 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditQuerySession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuerySession getAuditQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditQuerySessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit search 
     *  service. 
     *
     *  @return an <code> AuditSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchSession getAuditSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit search 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditSearchSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchSession getAuditSearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditSearchSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  administration service. 
     *
     *  @return an <code> AuditAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditAdminSession getAuditAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditAdminSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditAdminSession getAuditAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditAdminSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  notification service. 
     *
     *  @param  auditReceiver the notification callback 
     *  @return an <code> AuditNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auditReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditNotificationSession getAuditNotificationSession(org.osid.inquiry.AuditReceiver auditReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditNotificationSession(auditReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  notification service for the given inquest. 
     *
     *  @param  auditReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auditReceiver </code> or 
     *          <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditNotificationSession getAuditNotificationSessionForInquest(org.osid.inquiry.AuditReceiver auditReceiver, 
                                                                                           org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditNotificationSessionForInquest(auditReceiver, inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup audit/inquest mappings. 
     *
     *  @return an <code> AuditInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditInquest() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditInquestSession getAuditInquestSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditInquestSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning audits 
     *  to inquests. 
     *
     *  @return an <code> AuditInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditInquestAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditInquestAssignmentSession getAuditInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditInquestAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage audit smart inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @return an <code> AuditSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditSmartInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSmartInquestSession getAuditSmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditSmartInquestSession(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  lookup service. 
     *
     *  @return a <code> ResponseLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseLookupSession getResponseLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @return a <code> ResponseLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseLookupSession getResponseLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseLookupSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response query 
     *  service. 
     *
     *  @return a <code> ResponseQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQuerySession getResponseQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response query 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return a <code> ResponseQuerySession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQuerySession getResponseQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseQuerySessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  search service. 
     *
     *  @return a <code> ResponseSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseSearchSession getResponseSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  search service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return a <code> ResponseSearchSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseSearchSession getResponseSearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseSearchSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  administration service. 
     *
     *  @return a <code> ResponseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseAdminSession getResponseAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Dostributor 
     *          </code> 
     *  @return a <code> ResponseAdminSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseAdminSession getResponseAdminSessionForInput(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseAdminSessionForInput(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  notification service. 
     *
     *  @param  responseReceiver the notification callback 
     *  @return a <code> ResponseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> responseReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseNotificationSession getResponseNotificationSession(org.osid.inquiry.ResponseReceiver responseReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseNotificationSession(responseReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  notification service for the given inquest. 
     *
     *  @param  responseReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return a <code> ResponseNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> responseReceiver </code> 
     *          or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseNotificationSession getResponseNotificationSessionForInquest(org.osid.inquiry.ResponseReceiver responseReceiver, 
                                                                                                 org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseNotificationSessionForInquest(responseReceiver, inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup response/inquest 
     *  mappings. 
     *
     *  @return a <code> ResponseInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseInquestSession getResponseInquestSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseInquestSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  responses to inquests. 
     *
     *  @return a <code> ResponseInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseInquestAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseInquestAssignmentSession getResponseInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseInquestAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return a <code> ResponseSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseSmartInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseSmartInquestSession getResponseSmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResponseSmartInquestSession(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest lookup 
     *  service. 
     *
     *  @return an <code> InquestLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestLookupSession getInquestLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquestLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest query 
     *  service. 
     *
     *  @return an <code> InquestQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuerySession getInquestQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquestQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest search 
     *  service. 
     *
     *  @return an <code> InquestSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestSearchSession getInquestSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquestSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  administrative service. 
     *
     *  @return an <code> InquestAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestAdminSession getInquestAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquestAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  notification service. 
     *
     *  @param  inquestReceiver the notification callback 
     *  @return an <code> InquestNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquestReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestNotificationSession getInquestNotificationSession(org.osid.inquiry.InquestReceiver inquestReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquestNotificationSession(inquestReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  hierarchy service. 
     *
     *  @return an <code> InquestHierarchySession </code> for inquests 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestHierarchySession getInquestHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquestHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  hierarchy design service. 
     *
     *  @return an <code> HierarchyDesignSession </code> for inquests 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestHierarchyDesignSession getInquestHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquestHierarchyDesignSession());
    }


    /**
     *  Gets the <code> InquiryBatchManager. </code> 
     *
     *  @return an <code> InquiryBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.InquiryBatchManager getInquiryBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryBatchManager());
    }


    /**
     *  Gets the <code> InquiryRulesManager. </code> 
     *
     *  @return an <code> InquiryRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryRulesManager getInquiryRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
