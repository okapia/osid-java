//
// AbstractPostQuerySession.java
//
//     A template for making PostQuerySessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic template for query sessions.
 */

public abstract class AbstractPostQuerySession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.forum.PostQuerySession {

    private boolean federated = false;
    private org.osid.forum.Forum forum = new net.okapia.osid.jamocha.nil.forum.forum.UnknownForum();


      /**
       *  Gets the <code>Forum/code> <code>Id</code> associated
       *  with this session.
       *
       *  @return the <code>Forum Id</code> associated with
       *          this session
       *  @throws org.osid.IllegalStateException this session has been
       *          closed
       */

    @OSID @Override
      public org.osid.id.Id getForumId() {
        return (this.forum.getId());
    }


    /**
     *  Gets the <code>Forum</code> associated with this 
     *  session.
     *
     *  @return the <code>Forum</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.forum);
    }


    /**
     *  Sets the <code>Forum</code>.
     *
     *  @param  forum the forum for this session
     *  @throws org.osid.NullArgumentException <code>forum</code>
     *          is <code>null</code>
     */

    protected void setForum(org.osid.forum.Forum forum) {
        nullarg(forum, "forum");
        this.forum = forum;
        return;
    }

    /**
     *  Federates the view for methods in this session. A federated
     *  view will include posts in forums which are children
     *  of this forum in the forum hierarchy.
     */

    @OSID @Override
    public void useFederatedForumView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this forum only.
     */

    @OSID @Override
    public void useIsolatedForumView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Tests if this user can perform <code> Post </code>
     *  searches. A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer search operations to
     *  unauthorized users.
     *
     *  @return <code> false </code> if search methods are not authorized, 
     *          <code> true </code> otherwise 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canSearchPosts() {
        return (true);
    }
}
