//
// AbstractDemographicSearch.java
//
//     A template for making a Demographic Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographic.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing demographic searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDemographicSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resource.demographic.DemographicSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resource.demographic.records.DemographicSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resource.demographic.DemographicSearchOrder demographicSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of demographics. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  demographicIds list of demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographicIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDemographics(org.osid.id.IdList demographicIds) {
        while (demographicIds.hasNext()) {
            try {
                this.ids.add(demographicIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDemographics</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of demographic Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDemographicIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  demographicSearchOrder demographic search order 
     *  @throws org.osid.NullArgumentException
     *          <code>demographicSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>demographicSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDemographicResults(org.osid.resource.demographic.DemographicSearchOrder demographicSearchOrder) {
	this.demographicSearchOrder = demographicSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resource.demographic.DemographicSearchOrder getDemographicSearchOrder() {
	return (this.demographicSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given demographic search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a demographic implementing the requested record.
     *
     *  @param demographicSearchRecordType a demographic search record
     *         type
     *  @return the demographic search record
     *  @throws org.osid.NullArgumentException
     *          <code>demographicSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicSearchRecord getDemographicSearchRecord(org.osid.type.Type demographicSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resource.demographic.records.DemographicSearchRecord record : this.records) {
            if (record.implementsRecordType(demographicSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this demographic search. 
     *
     *  @param demographicSearchRecord demographic search record
     *  @param demographicSearchRecordType demographic search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDemographicSearchRecord(org.osid.resource.demographic.records.DemographicSearchRecord demographicSearchRecord, 
                                           org.osid.type.Type demographicSearchRecordType) {

        addRecordType(demographicSearchRecordType);
        this.records.add(demographicSearchRecord);        
        return;
    }
}
