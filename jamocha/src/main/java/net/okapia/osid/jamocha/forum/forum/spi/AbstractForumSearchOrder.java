//
// AbstractForumSearchOdrer.java
//
//     Defines a ForumSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.forum.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ForumSearchOrder}.
 */

public abstract class AbstractForumSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogSearchOrder
    implements org.osid.forum.ForumSearchOrder {

    private final java.util.Collection<org.osid.forum.records.ForumSearchOrderRecord> records = new java.util.LinkedHashSet<>();



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  forumRecordType a forum record type 
     *  @return {@code true} if the forumRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code forumRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type forumRecordType) {
        for (org.osid.forum.records.ForumSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(forumRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  forumRecordType the forum record type 
     *  @return the forum search order record
     *  @throws org.osid.NullArgumentException
     *          {@code forumRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(forumRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.forum.records.ForumSearchOrderRecord getForumSearchOrderRecord(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ForumSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(forumRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(forumRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this forum. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param forumRecord the forum search odrer record
     *  @param forumRecordType forum record type
     *  @throws org.osid.NullArgumentException
     *          {@code forumRecord} or
     *          {@code forumRecordTypeforum} is
     *          {@code null}
     */
            
    protected void addForumRecord(org.osid.forum.records.ForumSearchOrderRecord forumSearchOrderRecord, 
                                     org.osid.type.Type forumRecordType) {

        addRecordType(forumRecordType);
        this.records.add(forumSearchOrderRecord);
        
        return;
    }
}
