//
// AbstractAdapterParameterLookupSession.java
//
//    A Parameter lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Parameter lookup session adapter.
 */

public abstract class AbstractAdapterParameterLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.configuration.ParameterLookupSession {

    private final org.osid.configuration.ParameterLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterParameterLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterParameterLookupSession(org.osid.configuration.ParameterLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Configuration/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Configuration Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.session.getConfigurationId());
    }


    /**
     *  Gets the {@code Configuration} associated with this session.
     *
     *  @return the {@code Configuration} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getConfiguration());
    }


    /**
     *  Tests if this user can perform {@code Parameter} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupParameters() {
        return (this.session.canLookupParameters());
    }


    /**
     *  A complete view of the {@code Parameter} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParameterView() {
        this.session.useComparativeParameterView();
        return;
    }


    /**
     *  A complete view of the {@code Parameter} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParameterView() {
        this.session.usePlenaryParameterView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameters in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.session.useFederatedConfigurationView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.session.useIsolatedConfigurationView();
        return;
    }
    

    /**
     *  Only active parameters are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveParameterView() {
        this.session.useActiveParameterView();
        return;
    }


    /**
     *  Active and inactive parameters are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusParameterView() {
        this.session.useAnyStatusParameterView();
        return;
    }
    
     
    /**
     *  Gets the {@code Parameter} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Parameter} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Parameter} and
     *  retained for compatibility.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param parameterId {@code Id} of the {@code Parameter}
     *  @return the parameter
     *  @throws org.osid.NotFoundException {@code parameterId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code parameterId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Parameter getParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParameter(parameterId));
    }


    /**
     *  Gets a {@code ParameterList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  parameters specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Parameters} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Parameter} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code parameterIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByIds(org.osid.id.IdList parameterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParametersByIds(parameterIds));
    }


    /**
     *  Gets a {@code ParameterList} corresponding to the given
     *  parameter genus {@code Type} which does not include
     *  parameters of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterGenusType a parameter genus type 
     *  @return the returned {@code Parameter} list
     *  @throws org.osid.NullArgumentException
     *          {@code parameterGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByGenusType(org.osid.type.Type parameterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParametersByGenusType(parameterGenusType));
    }


    /**
     *  Gets a {@code ParameterList} corresponding to the given
     *  parameter genus {@code Type} and include any additional
     *  parameters with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterGenusType a parameter genus type 
     *  @return the returned {@code Parameter} list
     *  @throws org.osid.NullArgumentException
     *          {@code parameterGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByParentGenusType(org.osid.type.Type parameterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParametersByParentGenusType(parameterGenusType));
    }


    /**
     *  Gets a {@code ParameterList} containing the given
     *  parameter record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterRecordType a parameter record type 
     *  @return the returned {@code Parameter} list
     *  @throws org.osid.NullArgumentException
     *          {@code parameterRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByRecordType(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParametersByRecordType(parameterRecordType));
    }


    /**
     *  Gets all {@code Parameters}. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @return a list of {@code Parameters} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParameters()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParameters());
    }
}
