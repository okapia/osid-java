//
// DirectoryFilterList.java
//
//     Implements a filtering DirectoryList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.filing.directory;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering DirectoryList.
 */

public final class DirectoryFilterList
    extends net.okapia.osid.jamocha.inline.filter.filing.directory.spi.AbstractDirectoryFilterList
    implements org.osid.filing.DirectoryList,
               DirectoryFilter {

    private final DirectoryFilter filter;


    /**
     *  Creates a new <code>DirectoryFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>DirectoryList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public DirectoryFilterList(DirectoryFilter filter, org.osid.filing.DirectoryList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters Directories.
     *
     *  @param directory the directory to filter
     *  @return <code>true</code> if the directory passes the filter,
     *          <code>false</code> if the directory should be filtered
     */

    @Override
    public boolean pass(org.osid.filing.Directory directory) {
        return (this.filter.pass(directory));
    }
}
