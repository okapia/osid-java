//
// MutableMapProxyModuleLookupSession
//
//    Implements a Module lookup service backed by a collection of
//    modules that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus;


/**
 *  Implements a Module lookup service backed by a collection of
 *  modules. The modules are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of modules can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyModuleLookupSession
    extends net.okapia.osid.jamocha.core.course.syllabus.spi.AbstractMapModuleLookupSession
    implements org.osid.course.syllabus.ModuleLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyModuleLookupSession}
     *  with no modules.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyModuleLookupSession} with a
     *  single module.
     *
     *  @param courseCatalog the course catalog
     *  @param module a module
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code module}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.syllabus.Module module, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putModule(module);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyModuleLookupSession} using an
     *  array of modules.
     *
     *  @param courseCatalog the course catalog
     *  @param modules an array of modules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code modules}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.syllabus.Module[] modules, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putModules(modules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyModuleLookupSession} using a
     *  collection of modules.
     *
     *  @param courseCatalog the course catalog
     *  @param modules a collection of modules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code modules}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                java.util.Collection<? extends org.osid.course.syllabus.Module> modules,
                                                org.osid.proxy.Proxy proxy) {
   
        this(courseCatalog, proxy);
        setSessionProxy(proxy);
        putModules(modules);
        return;
    }

    
    /**
     *  Makes a {@code Module} available in this session.
     *
     *  @param module an module
     *  @throws org.osid.NullArgumentException {@code module{@code 
     *          is {@code null}
     */

    @Override
    public void putModule(org.osid.course.syllabus.Module module) {
        super.putModule(module);
        return;
    }


    /**
     *  Makes an array of modules available in this session.
     *
     *  @param modules an array of modules
     *  @throws org.osid.NullArgumentException {@code modules{@code 
     *          is {@code null}
     */

    @Override
    public void putModules(org.osid.course.syllabus.Module[] modules) {
        super.putModules(modules);
        return;
    }


    /**
     *  Makes collection of modules available in this session.
     *
     *  @param modules
     *  @throws org.osid.NullArgumentException {@code module{@code 
     *          is {@code null}
     */

    @Override
    public void putModules(java.util.Collection<? extends org.osid.course.syllabus.Module> modules) {
        super.putModules(modules);
        return;
    }


    /**
     *  Removes a Module from this session.
     *
     *  @param moduleId the {@code Id} of the module
     *  @throws org.osid.NullArgumentException {@code moduleId{@code  is
     *          {@code null}
     */

    @Override
    public void removeModule(org.osid.id.Id moduleId) {
        super.removeModule(moduleId);
        return;
    }    
}
