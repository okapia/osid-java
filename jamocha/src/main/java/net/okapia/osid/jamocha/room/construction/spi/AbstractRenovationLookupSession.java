//
// AbstractRenovationLookupSession.java
//
//    A starter implementation framework for providing a Renovation
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Renovation
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRenovations(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRenovationLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.room.construction.RenovationLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();
    

    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Renovation</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRenovations() {
        return (true);
    }


    /**
     *  A complete view of the <code>Renovation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRenovationView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Renovation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRenovationView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include renovations in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only renovations whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveRenovationView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All renovations of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRenovationView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Renovation</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Renovation</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Renovation</code> and
     *  retained for compatibility.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationId <code>Id</code> of the
     *          <code>Renovation</code>
     *  @return the renovation
     *  @throws org.osid.NotFoundException <code>renovationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>renovationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.Renovation getRenovation(org.osid.id.Id renovationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.room.construction.RenovationList renovations = getRenovations()) {
            while (renovations.hasNext()) {
                org.osid.room.construction.Renovation renovation = renovations.getNextRenovation();
                if (renovation.getId().equals(renovationId)) {
                    return (renovation);
                }
            }
        } 

        throw new org.osid.NotFoundException(renovationId + " not found");
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  renovations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Renovations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, renovations are returned that are currently effective.
     *  In any effective mode, effective renovations and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRenovations()</code>.
     *
     *  @param  renovationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>renovationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByIds(org.osid.id.IdList renovationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.construction.Renovation> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = renovationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRenovation(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("renovation " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.construction.renovation.LinkedRenovationList(ret));
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  renovation genus <code>Type</code> which does not include
     *  renovations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently effective.
     *  In any effective mode, effective renovations and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRenovations()</code>.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusType(org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.RenovationGenusFilterList(getRenovations(), renovationGenusType));
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  renovation genus <code>Type</code> and include any additional
     *  renovations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRenovations()</code>.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByParentGenusType(org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRenovationsByGenusType(renovationGenusType));
    }


    /**
     *  Gets a <code>RenovationList</code> containing the given
     *  renovation record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRenovations()</code>.
     *
     *  @param  renovationRecordType a renovation record type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByRecordType(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.RenovationRecordFilterList(getRenovations(), renovationRecordType));
    }


    /**
     *  Gets a <code>RenovationList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible
     *  through this session.
     *  
     *  In active mode, renovations are returned that are currently
     *  active. In any status mode, active and inactive renovations
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Renovation</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsOnDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.TemporalRenovationFilterList(getRenovations(), from, to));
    }
        

    /**
     *  Gets a list of all renovations of a genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>renovationGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeOnDate(org.osid.type.Type renovationGenusType, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.TemporalRenovationFilterList(getRenovationsByGenusType(renovationGenusType), from, to));
    }


    /**
     *  Gets a list of all renovations corresponding to a room
     *  <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.NullArgumentException <code>roomId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForRoom(org.osid.id.Id roomId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.construction.Renovation> ret = new java.util.ArrayList<>();

        try (org.osid.room.construction.RenovationList renovations = getRenovations()) {
            while (renovations.hasNext()) {
                org.osid.room.construction.Renovation renovation = renovations.getNextRenovation();
                try (org.osid.id.IdList ids = renovation.getRoomIds()) {
                    if (ids.getNextId().equals(roomId)) {
                        ret.add(renovation);
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.construction.renovation.LinkedRenovationList(ret));
    }


    /**
     *  Gets a list of all renovations for a room and of a renovation
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code> or
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForRoom(org.osid.id.Id roomId, 
                                                                                      org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.RenovationGenusFilterList(getRenovationsForRoom(roomId), renovationGenusType));
    }


    /**
     *  Gets a list of all renovations for a room with an effective
     *  start date within the given range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForRoomOnDate(org.osid.id.Id roomId, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.TemporalRenovationFilterList(getRenovationsForRoom(roomId), from, to));
    }


    /**
     *  Gets a list of all renovations for a room and of a renovation
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>renovationGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForRoomOnDate(org.osid.id.Id roomId, 
                                                                                            org.osid.type.Type renovationGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.TemporalRenovationFilterList(getRenovationsByGenusTypeForRoom(roomId, renovationGenusType), from, to));
    }


    /**
     *  Gets a list of all renovations corresponding to a floor
     *  <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId the <code>Id</code> of the floor 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.NullArgumentException <code>floorId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForFloor(org.osid.id.Id floorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.construction.Renovation> ret = new java.util.ArrayList<>();

        try (org.osid.room.construction.RenovationList renovations = getRenovations()) {
            while (renovations.hasNext()) {
                org.osid.room.construction.Renovation renovation = renovations.getNextRenovation();
                try (org.osid.room.RoomList rooms = renovation.getRooms()) {
                    if (rooms.getNextRoom().getFloorId().equals(floorId)) {
                        ret.add(renovation);
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.construction.renovation.LinkedRenovationList(ret));
    }


    /**
     *  Gets a list of all renovations for a floor and of a renovation
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code>Id</code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>floorId</code> or
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForFloor(org.osid.id.Id floorId, 
                                                                                      org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.RenovationGenusFilterList(getRenovationsForFloor(floorId), renovationGenusType));
    }


    /**
     *  Gets a list of all renovations for a floor with an effective
     *  start date within the given range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>floorId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForFloorOnDate(org.osid.id.Id floorId, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.TemporalRenovationFilterList(getRenovationsForFloor(floorId), from, to));
    }


    /**
     *  Gets a list of all renovations for a floor and of a renovation
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code>Id</code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>floorId</code>,
     *          <code>renovationGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForFloorOnDate(org.osid.id.Id floorId, 
                                                                                            org.osid.type.Type renovationGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.TemporalRenovationFilterList(getRenovationsByGenusTypeForFloor(floorId, renovationGenusType), from, to));
    }


    /**
     *  Gets a list of all renovations corresponding to a building
     *  <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.NullArgumentException <code>buildingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.construction.Renovation> ret = new java.util.ArrayList<>();

        try (org.osid.room.construction.RenovationList renovations = getRenovations()) {
            while (renovations.hasNext()) {
                org.osid.room.construction.Renovation renovation = renovations.getNextRenovation();
                try (org.osid.room.RoomList rooms = renovation.getRooms()) {
                    if (rooms.getNextRoom().getFloor().getBuildingId().equals(buildingId)) {
                        ret.add(renovation);
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.construction.renovation.LinkedRenovationList(ret));
    }


    /**
     *  Gets a list of all renovations for a building and of a renovation
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code> or
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                                      org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.RenovationGenusFilterList(getRenovationsForBuilding(buildingId), renovationGenusType));
    }


    /**
     *  Gets a list of all renovations for a building with an effective
     *  start date within the given range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.TemporalRenovationFilterList(getRenovationsForBuilding(buildingId), from, to));
    }


    /**
     *  Gets a list of all renovations for a building and of a renovation
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RenovationList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>renovationGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                                org.osid.type.Type renovationGenusType, 
                                                                                                org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.TemporalRenovationFilterList(getRenovationsByGenusTypeForBuilding(buildingId, renovationGenusType), from, to));
    }

    
    /**
     *  Gets all <code>Renovations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Renovations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.room.construction.RenovationList getRenovations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the renovation list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of renovations
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.room.construction.RenovationList filterRenovationsOnViews(org.osid.room.construction.RenovationList list)
        throws org.osid.OperationFailedException {

        org.osid.room.construction.RenovationList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.room.construction.renovation.EffectiveRenovationFilterList(ret);
        }

        return (ret);
    }
}
