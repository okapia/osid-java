//
// AbstractStepProcessorLookupSession.java
//
//    A starter implementation framework for providing a StepProcessor
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a StepProcessor
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getStepProcessors(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractStepProcessorLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.workflow.rules.StepProcessorLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();
    

    /**
     *  Gets the <code>Office/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>StepProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStepProcessors() {
        return (true);
    }


    /**
     *  A complete view of the <code>StepProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepProcessorView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>StepProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepProcessorView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step processors in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active step processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive step processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>StepProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>StepProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>StepProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @param  stepProcessorId <code>Id</code> of the
     *          <code>StepProcessor</code>
     *  @return the step processor
     *  @throws org.osid.NotFoundException <code>stepProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stepProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessor getStepProcessor(org.osid.id.Id stepProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.workflow.rules.StepProcessorList stepProcessors = getStepProcessors()) {
            while (stepProcessors.hasNext()) {
                org.osid.workflow.rules.StepProcessor stepProcessor = stepProcessors.getNextStepProcessor();
                if (stepProcessor.getId().equals(stepProcessorId)) {
                    return (stepProcessor);
                }
            }
        } 

        throw new org.osid.NotFoundException(stepProcessorId + " not found");
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stepProcessors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>StepProcessors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getStepProcessors()</code>.
     *
     *  @param  stepProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByIds(org.osid.id.IdList stepProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.workflow.rules.StepProcessor> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = stepProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getStepProcessor(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("step processor " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.workflow.rules.stepprocessor.LinkedStepProcessorList(ret));
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the given
     *  step processor genus <code>Type</code> which does not include
     *  step processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  step processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getStepProcessors()</code>.
     *
     *  @param  stepProcessorGenusType a stepProcessor genus type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByGenusType(org.osid.type.Type stepProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepprocessor.StepProcessorGenusFilterList(getStepProcessors(), stepProcessorGenusType));
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the given
     *  step processor genus <code>Type</code> and include any additional
     *  step processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  step processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStepProcessors()</code>.
     *
     *  @param  stepProcessorGenusType a stepProcessor genus type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByParentGenusType(org.osid.type.Type stepProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStepProcessorsByGenusType(stepProcessorGenusType));
    }


    /**
     *  Gets a <code>StepProcessorList</code> containing the given
     *  step processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  step processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStepProcessors()</code>.
     *
     *  @param  stepProcessorRecordType a stepProcessor record type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByRecordType(org.osid.type.Type stepProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepprocessor.StepProcessorRecordFilterList(getStepProcessors(), stepProcessorRecordType));
    }


    /**
     *  Gets all <code>StepProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  step processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @return a list of <code>StepProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.workflow.rules.StepProcessorList getStepProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the step processor list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of step processors
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.workflow.rules.StepProcessorList filterStepProcessorsOnViews(org.osid.workflow.rules.StepProcessorList list)
        throws org.osid.OperationFailedException {

        org.osid.workflow.rules.StepProcessorList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepprocessor.ActiveStepProcessorFilterList(ret);
        }

        return (ret);
    }
}
