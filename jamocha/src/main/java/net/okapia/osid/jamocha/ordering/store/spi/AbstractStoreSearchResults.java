//
// AbstractStoreSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.store.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractStoreSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.ordering.StoreSearchResults {

    private org.osid.ordering.StoreList stores;
    private final org.osid.ordering.StoreQueryInspector inspector;
    private final java.util.Collection<org.osid.ordering.records.StoreSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractStoreSearchResults.
     *
     *  @param stores the result set
     *  @param storeQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>stores</code>
     *          or <code>storeQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractStoreSearchResults(org.osid.ordering.StoreList stores,
                                            org.osid.ordering.StoreQueryInspector storeQueryInspector) {
        nullarg(stores, "stores");
        nullarg(storeQueryInspector, "store query inspectpr");

        this.stores = stores;
        this.inspector = storeQueryInspector;

        return;
    }


    /**
     *  Gets the store list resulting from a search.
     *
     *  @return a store list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStores() {
        if (this.stores == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.ordering.StoreList stores = this.stores;
        this.stores = null;
	return (stores);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.ordering.StoreQueryInspector getStoreQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  store search record <code> Type. </code> This method must
     *  be used to retrieve a store implementing the requested
     *  record.
     *
     *  @param storeSearchRecordType a store search 
     *         record type 
     *  @return the store search
     *  @throws org.osid.NullArgumentException
     *          <code>storeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(storeSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.StoreSearchResultsRecord getStoreSearchResultsRecord(org.osid.type.Type storeSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.ordering.records.StoreSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(storeSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(storeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record store search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addStoreRecord(org.osid.ordering.records.StoreSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "store record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
