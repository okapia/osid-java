//
// AbstractNodeWarehouseHierarchySession.java
//
//     Defines a Warehouse hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a warehouse hierarchy session for delivering a hierarchy
 *  of warehouses using the WarehouseNode interface.
 */

public abstract class AbstractNodeWarehouseHierarchySession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractWarehouseHierarchySession
    implements org.osid.inventory.WarehouseHierarchySession {

    private java.util.Collection<org.osid.inventory.WarehouseNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root warehouse <code> Ids </code> in this hierarchy.
     *
     *  @return the root warehouse <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootWarehouseIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.inventory.warehousenode.WarehouseNodeToIdList(this.roots));
    }


    /**
     *  Gets the root warehouses in the warehouse hierarchy. A node
     *  with no parents is an orphan. While all warehouse <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root warehouses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getRootWarehouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.warehousenode.WarehouseNodeToWarehouseList(new net.okapia.osid.jamocha.inventory.warehousenode.ArrayWarehouseNodeList(this.roots)));
    }


    /**
     *  Adds a root warehouse node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootWarehouse(org.osid.inventory.WarehouseNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root warehouse nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootWarehouses(java.util.Collection<org.osid.inventory.WarehouseNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root warehouse node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootWarehouse(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.inventory.WarehouseNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Warehouse </code> has any parents. 
     *
     *  @param  warehouseId a warehouse <code> Id </code> 
     *  @return <code> true </code> if the warehouse has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentWarehouses(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getWarehouseNode(warehouseId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  warehouse.
     *
     *  @param  id an <code> Id </code> 
     *  @param  warehouseId the <code> Id </code> of a warehouse 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> warehouseId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> warehouseId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfWarehouse(org.osid.id.Id id, org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inventory.WarehouseNodeList parents = getWarehouseNode(warehouseId).getParentWarehouseNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextWarehouseNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given warehouse. 
     *
     *  @param  warehouseId a warehouse <code> Id </code> 
     *  @return the parent <code> Ids </code> of the warehouse 
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentWarehouseIds(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.warehouse.WarehouseToIdList(getParentWarehouses(warehouseId)));
    }


    /**
     *  Gets the parents of the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> to query 
     *  @return the parents of the warehouse 
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getParentWarehouses(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.warehousenode.WarehouseNodeToWarehouseList(getWarehouseNode(warehouseId).getParentWarehouseNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  warehouse.
     *
     *  @param  id an <code> Id </code> 
     *  @param  warehouseId the Id of a warehouse 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> warehouseId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfWarehouse(org.osid.id.Id id, org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfWarehouse(id, warehouseId)) {
            return (true);
        }

        try (org.osid.inventory.WarehouseList parents = getParentWarehouses(warehouseId)) {
            while (parents.hasNext()) {
                if (isAncestorOfWarehouse(id, parents.getNextWarehouse().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a warehouse has any children. 
     *
     *  @param  warehouseId a warehouse <code> Id </code> 
     *  @return <code> true </code> if the <code> warehouseId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildWarehouses(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getWarehouseNode(warehouseId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  warehouse.
     *
     *  @param  id an <code> Id </code> 
     *  @param warehouseId the <code> Id </code> of a 
     *         warehouse
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> warehouseId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> warehouseId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfWarehouse(org.osid.id.Id id, org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfWarehouse(warehouseId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  warehouse.
     *
     *  @param  warehouseId the <code> Id </code> to query 
     *  @return the children of the warehouse 
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildWarehouseIds(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.warehouse.WarehouseToIdList(getChildWarehouses(warehouseId)));
    }


    /**
     *  Gets the children of the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> to query 
     *  @return the children of the warehouse 
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getChildWarehouses(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.warehousenode.WarehouseNodeToWarehouseList(getWarehouseNode(warehouseId).getChildWarehouseNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  warehouse.
     *
     *  @param  id an <code> Id </code> 
     *  @param warehouseId the <code> Id </code> of a 
     *         warehouse
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> warehouseId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfWarehouse(org.osid.id.Id id, org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfWarehouse(warehouseId, id)) {
            return (true);
        }

        try (org.osid.inventory.WarehouseList children = getChildWarehouses(warehouseId)) {
            while (children.hasNext()) {
                if (isDescendantOfWarehouse(id, children.getNextWarehouse().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  warehouse.
     *
     *  @param  warehouseId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified warehouse node 
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getWarehouseNodeIds(org.osid.id.Id warehouseId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.inventory.warehousenode.WarehouseNodeToNode(getWarehouseNode(warehouseId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given warehouse.
     *
     *  @param  warehouseId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified warehouse node 
     *  @throws org.osid.NotFoundException <code> warehouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> warehouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseNode getWarehouseNodes(org.osid.id.Id warehouseId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getWarehouseNode(warehouseId));
    }


    /**
     *  Closes this <code>WarehouseHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a warehouse node.
     *
     *  @param warehouseId the id of the warehouse node
     *  @throws org.osid.NotFoundException <code>warehouseId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>warehouseId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.inventory.WarehouseNode getWarehouseNode(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(warehouseId, "warehouse Id");
        for (org.osid.inventory.WarehouseNode warehouse : this.roots) {
            if (warehouse.getId().equals(warehouseId)) {
                return (warehouse);
            }

            org.osid.inventory.WarehouseNode r = findWarehouse(warehouse, warehouseId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(warehouseId + " is not found");
    }


    protected org.osid.inventory.WarehouseNode findWarehouse(org.osid.inventory.WarehouseNode node, 
                                                             org.osid.id.Id warehouseId) 
	throws org.osid.OperationFailedException {

        try (org.osid.inventory.WarehouseNodeList children = node.getChildWarehouseNodes()) {
            while (children.hasNext()) {
                org.osid.inventory.WarehouseNode warehouse = children.getNextWarehouseNode();
                if (warehouse.getId().equals(warehouseId)) {
                    return (warehouse);
                }
                
                warehouse = findWarehouse(warehouse, warehouseId);
                if (warehouse != null) {
                    return (warehouse);
                }
            }
        }

        return (null);
    }
}
