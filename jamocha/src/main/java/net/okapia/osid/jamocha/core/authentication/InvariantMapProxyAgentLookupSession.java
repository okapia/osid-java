//
// InvariantMapProxyAgentLookupSession
//
//    Implements an Agent lookup service backed by a fixed
//    collection of agents. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication;


/**
 *  Implements an Agent lookup service backed by a fixed
 *  collection of agents. The agents are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAgentLookupSession
    extends net.okapia.osid.jamocha.core.authentication.spi.AbstractMapAgentLookupSession
    implements org.osid.authentication.AgentLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAgentLookupSession} with no
     *  agents.
     *
     *  @param agency the agency
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAgentLookupSession(org.osid.authentication.Agency agency,
                                                  org.osid.proxy.Proxy proxy) {
        setAgency(agency);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAgentLookupSession} with a single
     *  agent.
     *
     *  @param agency the agency
     *  @param agent an single agent
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency},
     *          {@code agent} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAgentLookupSession(org.osid.authentication.Agency agency,
                                                  org.osid.authentication.Agent agent, org.osid.proxy.Proxy proxy) {

        this(agency, proxy);
        putAgent(agent);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAgentLookupSession} using
     *  an array of agents.
     *
     *  @param agency the agency
     *  @param agents an array of agents
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency},
     *          {@code agents} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAgentLookupSession(org.osid.authentication.Agency agency,
                                                  org.osid.authentication.Agent[] agents, org.osid.proxy.Proxy proxy) {

        this(agency, proxy);
        putAgents(agents);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAgentLookupSession} using a
     *  collection of agents.
     *
     *  @param agency the agency
     *  @param agents a collection of agents
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency},
     *          {@code agents} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAgentLookupSession(org.osid.authentication.Agency agency,
                                                  java.util.Collection<? extends org.osid.authentication.Agent> agents,
                                                  org.osid.proxy.Proxy proxy) {

        this(agency, proxy);
        putAgents(agents);
        return;
    }
}
