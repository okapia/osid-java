//
// AbstractImmutableGradebookColumn.java
//
//     Wraps a mutable GradebookColumn to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradebookcolumn.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>GradebookColumn</code> to hide modifiers. This
 *  wrapper provides an immutized GradebookColumn from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying gradebookColumn whose state changes are visible.
 */

public abstract class AbstractImmutableGradebookColumn
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.grading.GradebookColumn {

    private final org.osid.grading.GradebookColumn gradebookColumn;


    /**
     *  Constructs a new <code>AbstractImmutableGradebookColumn</code>.
     *
     *  @param gradebookColumn the gradebook column to immutablize
     *  @throws org.osid.NullArgumentException <code>gradebookColumn</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        super(gradebookColumn);
        this.gradebookColumn = gradebookColumn;
        return;
    }


    /**
     *  Gets the <code> GradeSystem Id </code> in which this grade belongs. 
     *
     *  @return the grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeSystemId() {
        return (this.gradebookColumn.getGradeSystemId());
    }


    /**
     *  Gets the <code> GradeSystem </code> in which this grade belongs. 
     *
     *  @return the package grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystem()
        throws org.osid.OperationFailedException {

        return (this.gradebookColumn.getGradeSystem());
    }


    /**
     *  Gets the gradebook column record corresponding to the given <code> 
     *  GradeBookColumn </code> record <code> Type. </code> This method ie 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> gradebookColumnRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(gradebookColumnRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  gradebookColumnRecordType the type of the record to retrieve 
     *  @return the gradebook column record 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(gradebookColumnRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnRecord getGradebookColumnRecord(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException {

        return (this.gradebookColumn.getGradebookColumnRecord(gradebookColumnRecordType));
    }
}

