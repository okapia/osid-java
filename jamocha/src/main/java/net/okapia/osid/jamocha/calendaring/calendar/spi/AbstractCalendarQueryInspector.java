//
// AbstractCalendarQueryInspector.java
//
//     A template for making a CalendarQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.calendar.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for calendars.
 */

public abstract class AbstractCalendarQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.calendaring.CalendarQueryInspector {

    private final java.util.Collection<org.osid.calendaring.records.CalendarQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the time period <code> Id </code> terms. 
     *
     *  @return the time period <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the time period terms. 
     *
     *  @return the time period terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Gets the commitment <code> Id </code> terms. 
     *
     *  @return the commitment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommitmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the commitment terms. 
     *
     *  @return the commitment terms 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQueryInspector[] getCommitmentTerms() {
        return (new org.osid.calendaring.CommitmentQueryInspector[0]);
    }


    /**
     *  Gets the ancestor calendar <code> Id </code> terms. 
     *
     *  @return the ancestor calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor calendar terms. 
     *
     *  @return the ancestor calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getAncestorCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the descendant calendar <code> Id </code> terms. 
     *
     *  @return the descendant calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant calendar terms. 
     *
     *  @return the descendant calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getDescendantCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given calendar query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a calendar implementing the requested record.
     *
     *  @param calendarRecordType a calendar record type
     *  @return the calendar query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>calendarRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(calendarRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CalendarQueryInspectorRecord getCalendarQueryInspectorRecord(org.osid.type.Type calendarRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.CalendarQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(calendarRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(calendarRecordType + " is not supported");
    }


    /**
     *  Adds a record to this calendar query. 
     *
     *  @param calendarQueryInspectorRecord calendar query inspector
     *         record
     *  @param calendarRecordType calendar record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCalendarQueryInspectorRecord(org.osid.calendaring.records.CalendarQueryInspectorRecord calendarQueryInspectorRecord, 
                                                   org.osid.type.Type calendarRecordType) {

        addRecordType(calendarRecordType);
        nullarg(calendarRecordType, "calendar record type");
        this.records.add(calendarQueryInspectorRecord);        
        return;
    }
}
