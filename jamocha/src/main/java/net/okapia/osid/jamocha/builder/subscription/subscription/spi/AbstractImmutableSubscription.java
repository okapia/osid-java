//
// AbstractImmutableSubscription.java
//
//     Wraps a mutable Subscription to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.subscription.subscription.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Subscription</code> to hide modifiers. This
 *  wrapper provides an immutized Subscription from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying subscription whose state changes are visible.
 */

public abstract class AbstractImmutableSubscription
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.subscription.Subscription {

    private final org.osid.subscription.Subscription subscription;


    /**
     *  Constructs a new <code>AbstractImmutableSubscription</code>.
     *
     *  @param subscription the subscription to immutablize
     *  @throws org.osid.NullArgumentException <code>subscription</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSubscription(org.osid.subscription.Subscription subscription) {
        super(subscription);
        this.subscription = subscription;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the dispatch. 
     *
     *  @return the dispatch <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDispatchId() {
        return (this.subscription.getDispatchId());
    }


    /**
     *  Gets the dispatch. 
     *
     *  @return the dispatch <code> Id </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.subscription.Dispatch getDispatch()
        throws org.osid.OperationFailedException {

        return (this.subscription.getDispatch());
    }


    /**
     *  Gets the <code> Id </code> of the subscriber. 
     *
     *  @return the subscriber <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubscriberId() {
        return (this.subscription.getSubscriberId());
    }


    /**
     *  Gets the subscriber. 
     *
     *  @return the subscriber 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSubscriber()
        throws org.osid.OperationFailedException {

        return (this.subscription.getSubscriber());
    }


    /**
     *  Gets the <code> Id </code> of the subscriber's address. 
     *
     *  @return the address <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAddressId() {
        return (this.subscription.getAddressId());
    }


    /**
     *  Gets the subscriber's address. 
     *
     *  @return the subscriber's address. 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress()
        throws org.osid.OperationFailedException {

        return (this.subscription.getAddress());
    }


    /**
     *  Gets the subscription record corresponding to the given <code> 
     *  Subscription </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  subscriptionRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(subscriptionRecordType) </code> is <code> true </code> . 
     *
     *  @param  subscriptionRecordType the type of subscription record to 
     *          retrieve 
     *  @return the subscription record 
     *  @throws org.osid.NullArgumentException <code> subscriptionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(subscriptionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionRecord getSubscriptionRecord(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException {

        return (this.subscription.getSubscriptionRecord(subscriptionRecordType));
    }
}

