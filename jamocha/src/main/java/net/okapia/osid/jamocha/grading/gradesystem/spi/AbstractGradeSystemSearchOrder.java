//
// AbstractGradeSystemSearchOdrer.java
//
//     Defines a GradeSystemSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradesystem.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code GradeSystemSearchOrder}.
 */

public abstract class AbstractGradeSystemSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.grading.GradeSystemSearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradeSystemSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by systems based on grades. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBasedOnGrades(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by lowest score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLowestNumericScore(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by score increment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumericScoreIncrement(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by highest score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByHighestNumericScore(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  gradeSystemRecordType a grade system record type 
     *  @return {@code true} if the gradeSystemRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeSystemRecordType) {
        for (org.osid.grading.records.GradeSystemSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  gradeSystemRecordType the grade system record type 
     *  @return the grade system search order record
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(gradeSystemRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemSearchOrderRecord getGradeSystemSearchOrderRecord(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeSystemSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeSystemRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this grade system. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeSystemRecord the grade system search odrer record
     *  @param gradeSystemRecordType grade system record type
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemRecord} or
     *          {@code gradeSystemRecordTypegradeSystem} is
     *          {@code null}
     */
            
    protected void addGradeSystemRecord(org.osid.grading.records.GradeSystemSearchOrderRecord gradeSystemSearchOrderRecord, 
                                     org.osid.type.Type gradeSystemRecordType) {

        addRecordType(gradeSystemRecordType);
        this.records.add(gradeSystemSearchOrderRecord);
        
        return;
    }
}
