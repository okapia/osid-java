//
// AbstractCommissionQueryInspector.java
//
//     A template for making a CommissionQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.commission.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for commissions.
 */

public abstract class AbstractCommissionQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.resourcing.CommissionQueryInspector {

    private final java.util.Collection<org.osid.resourcing.records.CommissionQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.resourcing.WorkQueryInspector[0]);
    }


    /**
     *  Gets the competency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompetencyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the competency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQueryInspector[] getCompetencyTerms() {
        return (new org.osid.resourcing.CompetencyQueryInspector[0]);
    }


    /**
     *  Gets the percentage availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getPercentageTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the effort <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEffortIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the effort query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQueryInspector[] getEffortTerms() {
        return (new org.osid.resourcing.EffortQueryInspector[0]);
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given commission query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a commission implementing the requested record.
     *
     *  @param commissionRecordType a commission record type
     *  @return the commission query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CommissionQueryInspectorRecord getCommissionQueryInspectorRecord(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CommissionQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(commissionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this commission query. 
     *
     *  @param commissionQueryInspectorRecord commission query inspector
     *         record
     *  @param commissionRecordType commission record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommissionQueryInspectorRecord(org.osid.resourcing.records.CommissionQueryInspectorRecord commissionQueryInspectorRecord, 
                                                   org.osid.type.Type commissionRecordType) {

        addRecordType(commissionRecordType);
        nullarg(commissionRecordType, "commission record type");
        this.records.add(commissionQueryInspectorRecord);        
        return;
    }
}
