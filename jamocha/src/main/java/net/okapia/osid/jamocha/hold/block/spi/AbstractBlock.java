//
// AbstractBlock.java
//
//     Defines a Block.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.block.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Block</code>.
 */

public abstract class AbstractBlock
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.hold.Block {

    private final java.util.Collection<org.osid.hold.Issue> issues = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.records.BlockRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Ids </code> of the issues. 
     *
     *  @return the issue <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIssueIds() {
        try {
            org.osid.hold.IssueList issues = getIssues();
            return (new net.okapia.osid.jamocha.adapter.converter.hold.issue.IssueToIdList(issues));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the issues. 
     *
     *  @return the issues 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.hold.IssueList getIssues()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.hold.issue.ArrayIssueList(this.issues));
    }


    /**
     *  Adds an issue.
     *
     *  @param issue an issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    protected void addIssue(org.osid.hold.Issue issue) {
        nullarg(issue, "issue");
        this.issues.add(issue);
        return;
    }


    /**
     *  Sets all the issues.
     *
     *  @param issues a collection of issues
     *  @throws org.osid.NullArgumentException <code>issues</code> is
     *          <code>null</code>
     */

    protected void setIssues(java.util.Collection<org.osid.hold.Issue> issues) {
        nullarg(issues, "issues");

        this.issues.clear();
        this.issues.addAll(issues);

        return;
    }

    
    /**
     *  Tests if this block supports the given record
     *  <code>Type</code>.
     *
     *  @param  blockRecordType a block record type 
     *  @return <code>true</code> if the blockRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type blockRecordType) {
        for (org.osid.hold.records.BlockRecord record : this.records) {
            if (record.implementsRecordType(blockRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Block</code> record <code>Type</code>.
     *
     *  @param  blockRecordType the block record type 
     *  @return the block record 
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blockRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.BlockRecord getBlockRecord(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.BlockRecord record : this.records) {
            if (record.implementsRecordType(blockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blockRecordType + " is not supported");
    }


    /**
     *  Adds a record to this block. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param blockRecord the block record
     *  @param blockRecordType block record type
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecord</code> or
     *          <code>blockRecordTypeblock</code> is
     *          <code>null</code>
     */
            
    protected void addBlockRecord(org.osid.hold.records.BlockRecord blockRecord, 
                                  org.osid.type.Type blockRecordType) {

        nullarg(blockRecord, "block record");
        addRecordType(blockRecordType);
        this.records.add(blockRecord);
        
        return;
    }
}
