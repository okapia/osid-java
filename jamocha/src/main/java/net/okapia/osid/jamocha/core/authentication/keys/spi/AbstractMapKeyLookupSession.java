//
// AbstractMapKeyLookupSession
//
//    A simple framework for providing a Key lookup service
//    backed by a fixed collection of keys.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.keys.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Key lookup service backed by a
 *  fixed collection of keys. The keys are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Keys</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapKeyLookupSession
    extends net.okapia.osid.jamocha.authentication.keys.spi.AbstractKeyLookupSession
    implements org.osid.authentication.keys.KeyLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.authentication.keys.Key> keys = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.authentication.keys.Key>());


    /**
     *  Makes a <code>Key</code> available in this session.
     *
     *  @param  key a key
     *  @throws org.osid.NullArgumentException <code>key<code>
     *          is <code>null</code>
     */

    protected void putKey(org.osid.authentication.keys.Key key) {
        this.keys.put(key.getId(), key);
        return;
    }


    /**
     *  Makes an array of keys available in this session.
     *
     *  @param  keys an array of keys
     *  @throws org.osid.NullArgumentException <code>keys<code>
     *          is <code>null</code>
     */

    protected void putKeys(org.osid.authentication.keys.Key[] keys) {
        putKeys(java.util.Arrays.asList(keys));
        return;
    }


    /**
     *  Makes a collection of keys available in this session.
     *
     *  @param  keys a collection of keys
     *  @throws org.osid.NullArgumentException <code>keys<code>
     *          is <code>null</code>
     */

    protected void putKeys(java.util.Collection<? extends org.osid.authentication.keys.Key> keys) {
        for (org.osid.authentication.keys.Key key : keys) {
            this.keys.put(key.getId(), key);
        }

        return;
    }


    /**
     *  Removes a Key from this session.
     *
     *  @param  keyId the <code>Id</code> of the key
     *  @throws org.osid.NullArgumentException <code>keyId<code> is
     *          <code>null</code>
     */

    protected void removeKey(org.osid.id.Id keyId) {
        this.keys.remove(keyId);
        return;
    }


    /**
     *  Gets the <code>Key</code> specified by its <code>Id</code>.
     *
     *  @param  keyId <code>Id</code> of the <code>Key</code>
     *  @return the key
     *  @throws org.osid.NotFoundException <code>keyId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>keyId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.Key getKey(org.osid.id.Id keyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.authentication.keys.Key key = this.keys.get(keyId);
        if (key == null) {
            throw new org.osid.NotFoundException("key not found: " + keyId);
        }

        return (key);
    }


    /**
     *  Gets all <code>Keys</code>. In plenary mode, the returned
     *  list contains all known keys or an error
     *  results. Otherwise, the returned list may contain only those
     *  keys that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Keys</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeys()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.keys.key.ArrayKeyList(this.keys.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.keys.clear();
        super.close();
        return;
    }
}
