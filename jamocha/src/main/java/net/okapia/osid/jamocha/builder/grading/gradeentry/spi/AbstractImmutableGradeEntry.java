//
// AbstractImmutableGradeEntry.java
//
//     Wraps a mutable GradeEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradeentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>GradeEntry</code> to hide modifiers. This
 *  wrapper provides an immutized GradeEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying gradeEntry whose state changes are visible.
 */

public abstract class AbstractImmutableGradeEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.grading.GradeEntry {

    private final org.osid.grading.GradeEntry gradeEntry;


    /**
     *  Constructs a new <code>AbstractImmutableGradeEntry</code>.
     *
     *  @param gradeEntry the grade entry to immutablize
     *  @throws org.osid.NullArgumentException <code>gradeEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableGradeEntry(org.osid.grading.GradeEntry gradeEntry) {
        super(gradeEntry);
        this.gradeEntry = gradeEntry;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradebookColumn. </code> 
     *
     *  @return the <code> Id </code> of the <code> GradebookColumn </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradebookColumnId() {
        return (this.gradeEntry.getGradebookColumnId());
    }


    /**
     *  Gets the <code> GradebookColumn. </code> 
     *
     *  @return the <code> GradebookColumn </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumn getGradebookColumn()
        throws org.osid.OperationFailedException {

        return (this.gradeEntry.getGradebookColumn());
    }


    /**
     *  Gets the <code> Id </code> of the key resource of this entry. The key 
     *  resource may be a student or other applicable key to identify a row of 
     *  grading entries. 
     *
     *  @return <code> Id </code> of the key resource 
     */

    @OSID @Override
    public org.osid.id.Id getKeyResourceId() {
        return (this.gradeEntry.getKeyResourceId());
    }


    /**
     *  Gets the key resource of this entry. The key resource may be a student 
     *  or other applicable key to identify a row of grading entries. 
     *
     *  @return the key resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getKeyResource()
        throws org.osid.OperationFailedException {

        return (this.gradeEntry.getKeyResource());
    }


    /**
     *  Tests if this is a calculated entry. 
     *
     *  @return <code> true </code> if this entry is a calculated entry, 
     *          <code> false </code> otherwise. If <code> true, </code> then 
     *          <code> overridesCalculatedEntry() </code> must be <code> 
     *          false. </code> 
     */

    @OSID @Override
    public boolean isDerived() {
        return (this.gradeEntry.isDerived());
    }


    /**
     *  Tests if this is a manual entry that overrides a calculated entry. 
     *
     *  @return <code> true </code> if this entry overrides a calculated 
     *          entry, <code> false </code> otherwise. If <code> true, </code> 
     *          then <code> isDerived() </code> must be <code> false. </code> 
     */

    @OSID @Override
    public boolean overridesCalculatedEntry() {
        return (this.gradeEntry.overridesCalculatedEntry());
    }


    /**
     *  Gets the calculated entry <code> Id </code> this entry overrides. 
     *
     *  @return the calculated entry <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> overridesDerivedEntry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOverriddenCalculatedEntryId() {
        return (this.gradeEntry.getOverriddenCalculatedEntryId());
    }


    /**
     *  Gets the calculated entry this entry overrides. 
     *
     *  @return the calculated entry 
     *  @throws org.osid.IllegalStateException <code> 
     *          overridesCalculatedEntry() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeEntry getOverriddenCalculatedEntry()
        throws org.osid.OperationFailedException {

        return (this.gradeEntry.getOverriddenCalculatedEntry());
    }


    /**
     *  Tests if this is entry should be ignored in any averaging, scaling or 
     *  curve calculation. 
     *
     *  @return <code> true </code> if this entry is ignored, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isIgnoredForCalculations() {
        return (this.gradeEntry.isIgnoredForCalculations());
    }


    /**
     *  Tests if a grade or score has been assigned to this entry. Generally, 
     *  an entry is created with a grade or score. 
     *
     *  @return <code> true </code> if a grade has been assigned, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.gradeEntry.isGraded());
    }


    /**
     *  Gets the grade <code> Id </code> in this entry if the grading system 
     *  is based on grades. 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> GradeSystem.isBasedOnGrades() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeId() {
        return (this.gradeEntry.getGradeId());
    }


    /**
     *  Gets the grade in this entry if the grading system is based on grades. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> GradeSystem.isBasedOnGrades() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getGrade()
        throws org.osid.OperationFailedException {

        return (this.gradeEntry.getGrade());
    }


    /**
     *  Gets the score in this entry if the grading system is not based on 
     *  grades. 
     *
     *  @return the score 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> GradeSystem.isBasedOnGrades() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getScore() {
        return (this.gradeEntry.getScore());
    }


    /**
     *  Gets the time the gradeable object was graded. 
     *
     *  @return the timestamp of the grading entry 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> isDerived() </code> is <code> 
     *          true </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeGraded() {
        return (this.gradeEntry.getTimeGraded());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Resource </code> that created 
     *  this entry. 
     *
     *  @return the <code> Id </code> of the <code> Resource </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> isDerived() </code> is <code> 
     *          true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGraderId() {
        return (this.gradeEntry.getGraderId());
    }


    /**
     *  Gets the <code> Resource </code> that created this entry. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() is false or 
     *          isDerived() is true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getGrader()
        throws org.osid.OperationFailedException {

        return (this.gradeEntry.getGrader());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> that created 
     *  this entry. 
     *
     *  @return the <code> Id </code> of the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> isDerived() </code> is <code> 
     *          true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradingAgentId() {
        return (this.gradeEntry.getGradingAgentId());
    }


    /**
     *  Gets the <code> Agent </code> that created this entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() is false or 
     *          isDerived() is true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getGradingAgent()
        throws org.osid.OperationFailedException {

        return (this.gradeEntry.getGradingAgent());
    }


    /**
     *  Gets the grade entry record corresponding to the given <code> 
     *  GradeEntry </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  gradeEntryRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(gradeEntryRecordType) </code> is <code> true </code> . 
     *
     *  @param  gradeEntryRecordType the type of the record to retrieve 
     *  @return the grade entry record 
     *  @throws org.osid.NullArgumentException <code> gradeEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(gradeEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntryRecord getGradeEntryRecord(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.gradeEntry.getGradeEntryRecord(gradeEntryRecordType));
    }
}

