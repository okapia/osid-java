//
// AbstractRenovationSearch.java
//
//     A template for making a Renovation Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.renovation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing renovation searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRenovationSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.room.construction.RenovationSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.room.construction.records.RenovationSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.room.construction.RenovationSearchOrder renovationSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of renovations. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  renovationIds list of renovations
     *  @throws org.osid.NullArgumentException
     *          <code>renovationIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRenovations(org.osid.id.IdList renovationIds) {
        while (renovationIds.hasNext()) {
            try {
                this.ids.add(renovationIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRenovations</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of renovation Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRenovationIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  renovationSearchOrder renovation search order 
     *  @throws org.osid.NullArgumentException
     *          <code>renovationSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>renovationSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRenovationResults(org.osid.room.construction.RenovationSearchOrder renovationSearchOrder) {
	this.renovationSearchOrder = renovationSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.room.construction.RenovationSearchOrder getRenovationSearchOrder() {
	return (this.renovationSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given renovation search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a renovation implementing the requested record.
     *
     *  @param renovationSearchRecordType a renovation search record
     *         type
     *  @return the renovation search record
     *  @throws org.osid.NullArgumentException
     *          <code>renovationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(renovationSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.RenovationSearchRecord getRenovationSearchRecord(org.osid.type.Type renovationSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.room.construction.records.RenovationSearchRecord record : this.records) {
            if (record.implementsRecordType(renovationSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(renovationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this renovation search. 
     *
     *  @param renovationSearchRecord renovation search record
     *  @param renovationSearchRecordType renovation search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRenovationSearchRecord(org.osid.room.construction.records.RenovationSearchRecord renovationSearchRecord, 
                                           org.osid.type.Type renovationSearchRecordType) {

        addRecordType(renovationSearchRecordType);
        this.records.add(renovationSearchRecord);        
        return;
    }
}
