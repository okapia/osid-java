//
// CheckMiter.java
//
//     Defines a Check miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.check;


/**
 *  Defines a <code>Check</code> miter for use with the builders.
 */

public interface CheckMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.rules.check.Check {


    /**
     *  Sets the start date for a time check.
     *
     *  @param date a time check date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setTimeCheckStartDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the end date for a time check,
     *
     *  @param date a time check date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setTimeCheckEndDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the event for a time check.
     *
     *  @param event a time check event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public void setTimeCheckEvent(org.osid.calendaring.Event event);


    /**
     *  Sets the cyclic event for a time check.
     *
     *  @param cyclicEvent a time check cyclic event
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEvent</code> is <code>null</code>
     */

    public void setTimeCheckCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent);


    /**
     *  Sets the block for a hold check.
     *
     *  @param block a hold check block
     *  @throws org.osid.NullArgumentException <code>block</code> is
     *          <code>null</code>
     */

    public void setHoldCheckBlock(org.osid.hold.Block block);


    /**
     *  Sets the audit for an inquiry check.
     *
     *  @param audit an inquiry check audit
     *  @throws org.osid.NullArgumentException <code>audit</code> is
     *          <code>null</code>
     */

    public void setInquiryCheckAudit(org.osid.inquiry.Audit audit);


    /**
     *  Sets the agenda for a process check.
     *
     *  @param agenda a process check agenda
     *  @throws org.osid.NullArgumentException <code>agenda</code> is
     *          <code>null</code>
     */

    public void setProcessCheckAgenda(org.osid.rules.check.Agenda agenda);


    /**
     *  Adds a Check record.
     *
     *  @param record a check record
     *  @param recordType the type of check record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCheckRecord(org.osid.rules.check.records.CheckRecord record, org.osid.type.Type recordType);
}       


