//
// AbstractAssemblyCompetencyQuery.java
//
//     A CompetencyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.competency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CompetencyQuery that stores terms.
 */

public abstract class AbstractAssemblyCompetencyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.resourcing.CompetencyQuery,
               org.osid.resourcing.CompetencyQueryInspector,
               org.osid.resourcing.CompetencySearchOrder {

    private final java.util.Collection<org.osid.resourcing.records.CompetencyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.CompetencyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.CompetencySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCompetencyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCompetencyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the learning objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId the learning objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        getAssembler().addIdTerm(getLearningObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the learning objective <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        getAssembler().clearTerms(getLearningObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the learning objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getLearningObjectiveIdColumn()));
    }


    /**
     *  Gets the LearningObjectiveId column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveIdColumn() {
        return ("learning_objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if a learning objective query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the learning objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches competencies that have any learning objective. 
     *
     *  @param  match <code> true </code> to match competencies with any 
     *          learning objective, <code> false </code> to match competencies 
     *          with no learning objective 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getLearningObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the learning objective query terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        getAssembler().clearTerms(getLearningObjectiveColumn());
        return;
    }


    /**
     *  Gets the learning objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the LearningObjective column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveColumn() {
        return ("learning_objective");
    }


    /**
     *  Sets the availability <code> Id </code> for this query. 
     *
     *  @param  availabilityId the availability <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> availabilityId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAvailabilityId(org.osid.id.Id availabilityId, 
                                    boolean match) {
        getAssembler().addIdTerm(getAvailabilityIdColumn(), availabilityId, match);
        return;
    }


    /**
     *  Clears the availability <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAvailabilityIdTerms() {
        getAssembler().clearTerms(getAvailabilityIdColumn());
        return;
    }


    /**
     *  Gets the availability <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAvailabilityIdTerms() {
        return (getAssembler().getIdTerms(getAvailabilityIdColumn()));
    }


    /**
     *  Gets the AvailabilityId column name.
     *
     * @return the column name
     */

    protected String getAvailabilityIdColumn() {
        return ("availability_id");
    }


    /**
     *  Tests if an <code> AvailabilityQuery </code> is available. 
     *
     *  @return <code> true </code> if an availability query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an availability. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the availability query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuery getAvailabilityQuery() {
        throw new org.osid.UnimplementedException("supportsAvailabilityQuery() is false");
    }


    /**
     *  Matches competencies that are used in any availability. 
     *
     *  @param  match <code> true </code> to match competencies with any 
     *          availability, <code> false </code> to match competencies with 
     *          no availability 
     */

    @OSID @Override
    public void matchAnyAvailability(boolean match) {
        getAssembler().addIdWildcardTerm(getAvailabilityColumn(), match);
        return;
    }


    /**
     *  Clears the availability query terms. 
     */

    @OSID @Override
    public void clearAvailabilityTerms() {
        getAssembler().clearTerms(getAvailabilityColumn());
        return;
    }


    /**
     *  Gets the availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQueryInspector[] getAvailabilityTerms() {
        return (new org.osid.resourcing.AvailabilityQueryInspector[0]);
    }


    /**
     *  Gets the Availability column name.
     *
     * @return the column name
     */

    protected String getAvailabilityColumn() {
        return ("availability");
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        getAssembler().addIdTerm(getWorkIdColumn(), workId, match);
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        getAssembler().clearTerms(getWorkIdColumn());
        return;
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (getAssembler().getIdTerms(getWorkIdColumn()));
    }


    /**
     *  Gets the WorkId column name.
     *
     * @return the column name
     */

    protected String getWorkIdColumn() {
        return ("work_id");
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches competencies that are used in any work. 
     *
     *  @param  match <code> true </code> to match competencies with any work, 
     *          <code> false </code> to match competencies with no work 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        getAssembler().addIdWildcardTerm(getWorkColumn(), match);
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        getAssembler().clearTerms(getWorkColumn());
        return;
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.resourcing.WorkQueryInspector[0]);
    }


    /**
     *  Gets the Work column name.
     *
     * @return the column name
     */

    protected String getWorkColumn() {
        return ("work");
    }


    /**
     *  Sets the job <code> Id </code> for this query. 
     *
     *  @param  jobId the job <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchJobId(org.osid.id.Id jobId, boolean match) {
        getAssembler().addIdTerm(getJobIdColumn(), jobId, match);
        return;
    }


    /**
     *  Clears the job <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearJobIdTerms() {
        getAssembler().clearTerms(getJobIdColumn());
        return;
    }


    /**
     *  Gets the job <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJobIdTerms() {
        return (getAssembler().getIdTerms(getJobIdColumn()));
    }


    /**
     *  Gets the JobId column name.
     *
     * @return the column name
     */

    protected String getJobIdColumn() {
        return ("job_id");
    }


    /**
     *  Tests if a <code> JobQuery </code> is available. 
     *
     *  @return <code> true </code> if a job query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the job query 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuery getJobQuery() {
        throw new org.osid.UnimplementedException("supportsJobQuery() is false");
    }


    /**
     *  Matches competencies that are used in any job. 
     *
     *  @param  match <code> true </code> to match competencies with any job, 
     *          <code> false </code> to match competencies with no job 
     */

    @OSID @Override
    public void matchAnyJob(boolean match) {
        getAssembler().addIdWildcardTerm(getJobColumn(), match);
        return;
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearJobTerms() {
        getAssembler().clearTerms(getJobColumn());
        return;
    }


    /**
     *  Gets the job query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.JobQueryInspector[] getJobTerms() {
        return (new org.osid.resourcing.JobQueryInspector[0]);
    }


    /**
     *  Gets the Job column name.
     *
     * @return the column name
     */

    protected String getJobColumn() {
        return ("job");
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match 
     *  competencies assigned to foundries. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this competency supports the given record
     *  <code>Type</code>.
     *
     *  @param  competencyRecordType a competency record type 
     *  @return <code>true</code> if the competencyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type competencyRecordType) {
        for (org.osid.resourcing.records.CompetencyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(competencyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  competencyRecordType the competency record type 
     *  @return the competency query record 
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(competencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CompetencyQueryRecord getCompetencyQueryRecord(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CompetencyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(competencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(competencyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  competencyRecordType the competency record type 
     *  @return the competency query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(competencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CompetencyQueryInspectorRecord getCompetencyQueryInspectorRecord(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CompetencyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(competencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(competencyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param competencyRecordType the competency record type
     *  @return the competency search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(competencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CompetencySearchOrderRecord getCompetencySearchOrderRecord(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CompetencySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(competencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(competencyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this competency. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param competencyQueryRecord the competency query record
     *  @param competencyQueryInspectorRecord the competency query inspector
     *         record
     *  @param competencySearchOrderRecord the competency search order record
     *  @param competencyRecordType competency record type
     *  @throws org.osid.NullArgumentException
     *          <code>competencyQueryRecord</code>,
     *          <code>competencyQueryInspectorRecord</code>,
     *          <code>competencySearchOrderRecord</code> or
     *          <code>competencyRecordTypecompetency</code> is
     *          <code>null</code>
     */
            
    protected void addCompetencyRecords(org.osid.resourcing.records.CompetencyQueryRecord competencyQueryRecord, 
                                      org.osid.resourcing.records.CompetencyQueryInspectorRecord competencyQueryInspectorRecord, 
                                      org.osid.resourcing.records.CompetencySearchOrderRecord competencySearchOrderRecord, 
                                      org.osid.type.Type competencyRecordType) {

        addRecordType(competencyRecordType);

        nullarg(competencyQueryRecord, "competency query record");
        nullarg(competencyQueryInspectorRecord, "competency query inspector record");
        nullarg(competencySearchOrderRecord, "competency search odrer record");

        this.queryRecords.add(competencyQueryRecord);
        this.queryInspectorRecords.add(competencyQueryInspectorRecord);
        this.searchOrderRecords.add(competencySearchOrderRecord);
        
        return;
    }
}
