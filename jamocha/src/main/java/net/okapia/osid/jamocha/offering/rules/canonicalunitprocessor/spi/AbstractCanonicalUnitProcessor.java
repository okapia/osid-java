//
// AbstractCanonicalUnitProcessor.java
//
//     Defines a CanonicalUnitProcessor.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>CanonicalUnitProcessor</code>.
 */

public abstract class AbstractCanonicalUnitProcessor
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessor
    implements org.osid.offering.rules.CanonicalUnitProcessor {

    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this canonicalUnitProcessor supports the given record
     *  <code>Type</code>.
     *
     *  @param canonicalUnitProcessorRecordType a canonical unit
     *         processor record type
     *  @return <code>true</code> if the
     *          canonicalUnitProcessorRecordType is supported,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type canonicalUnitProcessorRecordType) {
        for (org.osid.offering.rules.records.CanonicalUnitProcessorRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param canonicalUnitProcessorRecordType the canonical unit
     *         processor record type
     *  @return the canonical unit processor record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorRecord getCanonicalUnitProcessorRecord(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit processor. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param canonicalUnitProcessorRecord the canonical unit
     *         processor record
     *  @param canonicalUnitProcessorRecordType canonical unit
     *         processor record type
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecord</code> or
     *          <code>canonicalUnitProcessorRecordTypecanonicalUnitProcessor</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitProcessorRecord(org.osid.offering.rules.records.CanonicalUnitProcessorRecord canonicalUnitProcessorRecord, 
                                     org.osid.type.Type canonicalUnitProcessorRecordType) {

        addRecordType(canonicalUnitProcessorRecordType);
        this.records.add(canonicalUnitProcessorRecord);
        
        return;
    }
}
