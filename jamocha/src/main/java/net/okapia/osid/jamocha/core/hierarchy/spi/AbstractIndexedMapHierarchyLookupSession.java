
package net.okapia.osid.jamocha.core.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Hierarchy lookup service backed by a
 *  fixed collection of hierarchies. The hierarchies are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some hierarchies may be compatible
 *  with more types than are indicated through these hierarchy
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Hierarchies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapHierarchyLookupSession
    extends AbstractMapHierarchyLookupSession
    implements org.osid.hierarchy.HierarchyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.hierarchy.Hierarchy> hierarchiesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hierarchy.Hierarchy>());
    private final MultiMap<org.osid.type.Type, org.osid.hierarchy.Hierarchy> hierarchiesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hierarchy.Hierarchy>());


    /**
     *  Makes a <code>Hierarchy</code> available in this session.
     *
     *  @param  hierarchy a hierarchy
     *  @throws org.osid.NullArgumentException <code>hierarchy<code> is
     *          <code>null</code>
     */

    @Override
    protected void putHierarchy(org.osid.hierarchy.Hierarchy hierarchy) {
        super.putHierarchy(hierarchy);

        this.hierarchiesByGenus.put(hierarchy.getGenusType(), hierarchy);
        
        try (org.osid.type.TypeList types = hierarchy.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.hierarchiesByRecord.put(types.getNextType(), hierarchy);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a hierarchy from this session.
     *
     *  @param hierarchyId the <code>Id</code> of the hierarchy
     *  @throws org.osid.NullArgumentException <code>hierarchyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeHierarchy(org.osid.id.Id hierarchyId) {
        org.osid.hierarchy.Hierarchy hierarchy;
        try {
            hierarchy = getHierarchy(hierarchyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.hierarchiesByGenus.remove(hierarchy.getGenusType());

        try (org.osid.type.TypeList types = hierarchy.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.hierarchiesByRecord.remove(types.getNextType(), hierarchy);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeHierarchy(hierarchyId);
        return;
    }


    /**
     *  Gets a <code>HierarchyList</code> corresponding to the given
     *  hierarchy genus <code>Type</code> which does not include
     *  hierarchies of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known hierarchies or an error results. Otherwise,
     *  the returned list may contain only those hierarchies that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  hierarchyGenusType a hierarchy genus type 
     *  @return the returned <code>Hierarchy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByGenusType(org.osid.type.Type hierarchyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hierarchy.hierarchy.ArrayHierarchyList(this.hierarchiesByGenus.get(hierarchyGenusType)));
    }


    /**
     *  Gets a <code>HierarchyList</code> containing the given
     *  hierarchy record <code>Type</code>. In plenary mode, the
     *  returned list contains all known hierarchies or an error
     *  results. Otherwise, the returned list may contain only those
     *  hierarchies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  hierarchyRecordType a hierarchy record type 
     *  @return the returned <code>hierarchy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByRecordType(org.osid.type.Type hierarchyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hierarchy.hierarchy.ArrayHierarchyList(this.hierarchiesByRecord.get(hierarchyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.hierarchiesByGenus.clear();
        this.hierarchiesByRecord.clear();

        super.close();

        return;
    }
}
