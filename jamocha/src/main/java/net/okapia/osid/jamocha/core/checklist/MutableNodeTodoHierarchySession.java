//
// MutableNodeTodoHierarchySession.java
//
//     Defines a Todo hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist;


/**
 *  Defines a todo hierarchy session for delivering a hierarchy
 *  of todos using the TodoNode interface.
 */

public final class MutableNodeTodoHierarchySession
    extends net.okapia.osid.jamocha.core.checklist.spi.AbstractNodeTodoHierarchySession
    implements org.osid.checklist.TodoHierarchySession {


    /**
     *  Constructs a new
     *  <code>MutableNodeTodoHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public MutableNodeTodoHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeTodoHierarchySession</code> using the
     *  root node for the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>node</code> 
     *          is <code>null</code>
     */

    public MutableNodeTodoHierarchySession(org.osid.checklist.TodoNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getTodo().getDisplayName())
                     .description(root.getTodo().getDescription())
                     .build());

        addRootTodo(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeTodoHierarchySession</code> using the
     *  given root as the root node.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public MutableNodeTodoHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               org.osid.checklist.TodoNode root) {
        setHierarchy(hierarchy);
        addRootTodo(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeTodoHierarchySession</code> using a
     *  collection of nodes as roots in the hierarchy.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root nodes
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public MutableNodeTodoHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               java.util.Collection<org.osid.checklist.TodoNode> roots) {
        setHierarchy(hierarchy);
        addRootTodos(roots);
        return;
    }


    /**
     *  Adds a root todo node to the hierarchy.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootTodo(org.osid.checklist.TodoNode root) {
        super.addRootTodo(root);
        return;
    }


    /**
     *  Adds a collection of root todo nodes.
     *
     *  @param roots hierarchy roots
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootTodos(java.util.Collection<org.osid.checklist.TodoNode> roots) {
        super.addRootTodos(roots);
        return;
    }


    /**
     *  Removes a root todo node from the hierarchy.
     *
     *  @param rootId a root node {@code Id}
     *  @throws org.osid.NullArgumentException <code>rootId</code> is
     *          <code>null</code>
     */

    @Override
    public void removeRootTodo(org.osid.id.Id rootId) {
        super.removeRootTodo(rootId);
        return;
    }
}
