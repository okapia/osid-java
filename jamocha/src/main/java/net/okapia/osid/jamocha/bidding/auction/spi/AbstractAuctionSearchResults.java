//
// AbstractAuctionSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAuctionSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.bidding.AuctionSearchResults {

    private org.osid.bidding.AuctionList auctions;
    private final org.osid.bidding.AuctionQueryInspector inspector;
    private final java.util.Collection<org.osid.bidding.records.AuctionSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAuctionSearchResults.
     *
     *  @param auctions the result set
     *  @param auctionQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>auctions</code>
     *          or <code>auctionQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAuctionSearchResults(org.osid.bidding.AuctionList auctions,
                                            org.osid.bidding.AuctionQueryInspector auctionQueryInspector) {
        nullarg(auctions, "auctions");
        nullarg(auctionQueryInspector, "auction query inspectpr");

        this.auctions = auctions;
        this.inspector = auctionQueryInspector;

        return;
    }


    /**
     *  Gets the auction list resulting from a search.
     *
     *  @return an auction list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctions() {
        if (this.auctions == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.bidding.AuctionList auctions = this.auctions;
        this.auctions = null;
	return (auctions);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.bidding.AuctionQueryInspector getAuctionQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  auction search record <code> Type. </code> This method must
     *  be used to retrieve an auction implementing the requested
     *  record.
     *
     *  @param auctionSearchRecordType an auction search 
     *         record type 
     *  @return the auction search
     *  @throws org.osid.NullArgumentException
     *          <code>auctionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(auctionSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionSearchResultsRecord getAuctionSearchResultsRecord(org.osid.type.Type auctionSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.bidding.records.AuctionSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(auctionSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(auctionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record auction search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAuctionRecord(org.osid.bidding.records.AuctionSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "auction record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
