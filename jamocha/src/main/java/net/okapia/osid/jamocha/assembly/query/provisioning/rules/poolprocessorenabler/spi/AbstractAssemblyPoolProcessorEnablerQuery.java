//
// AbstractAssemblyPoolProcessorEnablerQuery.java
//
//     A PoolProcessorEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.rules.poolprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PoolProcessorEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyPoolProcessorEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.provisioning.rules.PoolProcessorEnablerQuery,
               org.osid.provisioning.rules.PoolProcessorEnablerQueryInspector,
               org.osid.provisioning.rules.PoolProcessorEnablerSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPoolProcessorEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPoolProcessorEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the pool processor. 
     *
     *  @param  poolProcessorId the pool processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolProcessorId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledPoolProcessorId(org.osid.id.Id poolProcessorId, 
                                          boolean match) {
        getAssembler().addIdTerm(getRuledPoolProcessorIdColumn(), poolProcessorId, match);
        return;
    }


    /**
     *  Clears the pool processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledPoolProcessorIdTerms() {
        getAssembler().clearTerms(getRuledPoolProcessorIdColumn());
        return;
    }


    /**
     *  Gets the pool processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledPoolProcessorIdTerms() {
        return (getAssembler().getIdTerms(getRuledPoolProcessorIdColumn()));
    }


    /**
     *  Gets the RuledPoolProcessorId column name.
     *
     * @return the column name
     */

    protected String getRuledPoolProcessorIdColumn() {
        return ("ruled_pool_processor_id");
    }


    /**
     *  Tests if a <code> PoolProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledPoolProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the pool processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledPoolProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorQuery getRuledPoolProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledPoolProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any pool processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any pool 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          pool processors 
     */

    @OSID @Override
    public void matchAnyRuledPoolProcessor(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledPoolProcessorColumn(), match);
        return;
    }


    /**
     *  Clears the pool processor query terms. 
     */

    @OSID @Override
    public void clearRuledPoolProcessorTerms() {
        getAssembler().clearTerms(getRuledPoolProcessorColumn());
        return;
    }


    /**
     *  Gets the pool processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorQueryInspector[] getRuledPoolProcessorTerms() {
        return (new org.osid.provisioning.rules.PoolProcessorQueryInspector[0]);
    }


    /**
     *  Gets the RuledPoolProcessor column name.
     *
     * @return the column name
     */

    protected String getRuledPoolProcessorColumn() {
        return ("ruled_pool_processor");
    }


    /**
     *  Matches enablers mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this poolProcessorEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  poolProcessorEnablerRecordType a pool processor enabler record type 
     *  @return <code>true</code> if the poolProcessorEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type poolProcessorEnablerRecordType) {
        for (org.osid.provisioning.rules.records.PoolProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(poolProcessorEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  poolProcessorEnablerRecordType the pool processor enabler record type 
     *  @return the pool processor enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorEnablerQueryRecord getPoolProcessorEnablerQueryRecord(org.osid.type.Type poolProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(poolProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  poolProcessorEnablerRecordType the pool processor enabler record type 
     *  @return the pool processor enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorEnablerQueryInspectorRecord getPoolProcessorEnablerQueryInspectorRecord(org.osid.type.Type poolProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(poolProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param poolProcessorEnablerRecordType the pool processor enabler record type
     *  @return the pool processor enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorEnablerSearchOrderRecord getPoolProcessorEnablerSearchOrderRecord(org.osid.type.Type poolProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(poolProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this pool processor enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param poolProcessorEnablerQueryRecord the pool processor enabler query record
     *  @param poolProcessorEnablerQueryInspectorRecord the pool processor enabler query inspector
     *         record
     *  @param poolProcessorEnablerSearchOrderRecord the pool processor enabler search order record
     *  @param poolProcessorEnablerRecordType pool processor enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerQueryRecord</code>,
     *          <code>poolProcessorEnablerQueryInspectorRecord</code>,
     *          <code>poolProcessorEnablerSearchOrderRecord</code> or
     *          <code>poolProcessorEnablerRecordTypepoolProcessorEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addPoolProcessorEnablerRecords(org.osid.provisioning.rules.records.PoolProcessorEnablerQueryRecord poolProcessorEnablerQueryRecord, 
                                      org.osid.provisioning.rules.records.PoolProcessorEnablerQueryInspectorRecord poolProcessorEnablerQueryInspectorRecord, 
                                      org.osid.provisioning.rules.records.PoolProcessorEnablerSearchOrderRecord poolProcessorEnablerSearchOrderRecord, 
                                      org.osid.type.Type poolProcessorEnablerRecordType) {

        addRecordType(poolProcessorEnablerRecordType);

        nullarg(poolProcessorEnablerQueryRecord, "pool processor enabler query record");
        nullarg(poolProcessorEnablerQueryInspectorRecord, "pool processor enabler query inspector record");
        nullarg(poolProcessorEnablerSearchOrderRecord, "pool processor enabler search odrer record");

        this.queryRecords.add(poolProcessorEnablerQueryRecord);
        this.queryInspectorRecords.add(poolProcessorEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(poolProcessorEnablerSearchOrderRecord);
        
        return;
    }
}
