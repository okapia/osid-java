//
// AbstractMapInputEnablerLookupSession
//
//    A simple framework for providing an InputEnabler lookup service
//    backed by a fixed collection of input enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an InputEnabler lookup service backed by a
 *  fixed collection of input enablers. The input enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>InputEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapInputEnablerLookupSession
    extends net.okapia.osid.jamocha.control.rules.spi.AbstractInputEnablerLookupSession
    implements org.osid.control.rules.InputEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.rules.InputEnabler> inputEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.rules.InputEnabler>());


    /**
     *  Makes an <code>InputEnabler</code> available in this session.
     *
     *  @param  inputEnabler an input enabler
     *  @throws org.osid.NullArgumentException <code>inputEnabler<code>
     *          is <code>null</code>
     */

    protected void putInputEnabler(org.osid.control.rules.InputEnabler inputEnabler) {
        this.inputEnablers.put(inputEnabler.getId(), inputEnabler);
        return;
    }


    /**
     *  Makes an array of input enablers available in this session.
     *
     *  @param  inputEnablers an array of input enablers
     *  @throws org.osid.NullArgumentException <code>inputEnablers<code>
     *          is <code>null</code>
     */

    protected void putInputEnablers(org.osid.control.rules.InputEnabler[] inputEnablers) {
        putInputEnablers(java.util.Arrays.asList(inputEnablers));
        return;
    }


    /**
     *  Makes a collection of input enablers available in this session.
     *
     *  @param  inputEnablers a collection of input enablers
     *  @throws org.osid.NullArgumentException <code>inputEnablers<code>
     *          is <code>null</code>
     */

    protected void putInputEnablers(java.util.Collection<? extends org.osid.control.rules.InputEnabler> inputEnablers) {
        for (org.osid.control.rules.InputEnabler inputEnabler : inputEnablers) {
            this.inputEnablers.put(inputEnabler.getId(), inputEnabler);
        }

        return;
    }


    /**
     *  Removes an InputEnabler from this session.
     *
     *  @param  inputEnablerId the <code>Id</code> of the input enabler
     *  @throws org.osid.NullArgumentException <code>inputEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeInputEnabler(org.osid.id.Id inputEnablerId) {
        this.inputEnablers.remove(inputEnablerId);
        return;
    }


    /**
     *  Gets the <code>InputEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  inputEnablerId <code>Id</code> of the <code>InputEnabler</code>
     *  @return the inputEnabler
     *  @throws org.osid.NotFoundException <code>inputEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>inputEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnabler getInputEnabler(org.osid.id.Id inputEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.rules.InputEnabler inputEnabler = this.inputEnablers.get(inputEnablerId);
        if (inputEnabler == null) {
            throw new org.osid.NotFoundException("inputEnabler not found: " + inputEnablerId);
        }

        return (inputEnabler);
    }


    /**
     *  Gets all <code>InputEnablers</code>. In plenary mode, the returned
     *  list contains all known inputEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  inputEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>InputEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.inputenabler.ArrayInputEnablerList(this.inputEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inputEnablers.clear();
        super.close();
        return;
    }
}
