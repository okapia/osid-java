//
// CommissionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.commission.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CommissionElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the CommissionElement Id.
     *
     *  @return the commission element Id
     */

    public static org.osid.id.Id getCommissionEntityId() {
        return (makeEntityId("osid.resourcing.Commission"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.resourcing.commission.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.resourcing.commission.Resource"));
    }


    /**
     *  Gets the WorkId element Id.
     *
     *  @return the WorkId element Id
     */

    public static org.osid.id.Id getWorkId() {
        return (makeElementId("osid.resourcing.commission.WorkId"));
    }


    /**
     *  Gets the Work element Id.
     *
     *  @return the Work element Id
     */

    public static org.osid.id.Id getWork() {
        return (makeElementId("osid.resourcing.commission.Work"));
    }


    /**
     *  Gets the CompetencyId element Id.
     *
     *  @return the CompetencyId element Id
     */

    public static org.osid.id.Id getCompetencyId() {
        return (makeElementId("osid.resourcing.commission.CompetencyId"));
    }


    /**
     *  Gets the Competency element Id.
     *
     *  @return the Competency element Id
     */

    public static org.osid.id.Id getCompetency() {
        return (makeElementId("osid.resourcing.commission.Competency"));
    }


    /**
     *  Gets the Percentage element Id.
     *
     *  @return the Percentage element Id
     */

    public static org.osid.id.Id getPercentage() {
        return (makeElementId("osid.resourcing.commission.Percentage"));
    }


    /**
     *  Gets the FoundryId element Id.
     *
     *  @return the FoundryId element Id
     */

    public static org.osid.id.Id getFoundryId() {
        return (makeQueryElementId("osid.resourcing.commission.FoundryId"));
    }


    /**
     *  Gets the EffortId element Id.
     *
     *  @return the EffortId element Id
     */

    public static org.osid.id.Id getEffortId() {
        return (makeQueryElementId("osid.resourcing.commission.EffortId"));
    }


    /**
     *  Gets the Effort element Id.
     *
     *  @return the Effort element Id
     */

    public static org.osid.id.Id getEffort() {
        return (makeQueryElementId("osid.resourcing.commission.Effort"));
    }


    /**
     *  Gets the Foundry element Id.
     *
     *  @return the Foundry element Id
     */

    public static org.osid.id.Id getFoundry() {
        return (makeQueryElementId("osid.resourcing.commission.Foundry"));
    }
}
