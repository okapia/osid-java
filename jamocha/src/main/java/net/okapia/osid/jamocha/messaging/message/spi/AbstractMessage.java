//
// AbstractMessage.java
//
//     Defines a Message.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.message.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Message</code>.
 */

public abstract class AbstractMessage
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.messaging.Message {

    private org.osid.locale.DisplayText subjectLine;
    private org.osid.locale.DisplayText text;
    private org.osid.calendaring.DateTime sentTime;
    private org.osid.resource.Resource sender;
    private org.osid.authentication.Agent sendingAgent;
    private org.osid.calendaring.DateTime receivedTime;
    private final java.util.Collection<org.osid.resource.Resource> recipients = new java.util.LinkedHashSet<>();
    private org.osid.messaging.Receipt receipt;

    private final java.util.Collection<org.osid.messaging.records.MessageRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the subject line of this message. 
     *
     *  @return the subject 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSubjectLine() {
        return (this.subjectLine);
    }


    /**
     *  Sets the subject line.
     *
     *  @param subjectLine a subject line
     *  @throws org.osid.NullArgumentException
     *          <code>subjectLine</code> is <code>null</code>
     */

    protected void setSubjectLine(org.osid.locale.DisplayText subjectLine) {
        nullarg(subjectLine, "subject line");
        this.subjectLine = subjectLine;
        return;
    }


    /**
     *  Gets the text of the message. 
     *
     *  @return the text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getText() {
        return (this.text);
    }


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @throws org.osid.NullArgumentException
     *          <code>text</code> is <code>null</code>
     */

    protected void setText(org.osid.locale.DisplayText text) {
        nullarg(text, "text");
        this.text = text;
        return;
    }


    /**
     *  Tests if this message has been sent. 
     *
     *  @return <code> true </code> if this message has been sent, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isSent() {
        return (this.sentTime != null);
    }


    /**
     *  Gets the time this message was sent. 
     *
     *  @return the time 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getSentTime() {
        if (!isSent()) {
            throw new org.osid.IllegalStateException("isSent() is false");
        }

        return (this.sentTime);
    }


    /**
     *  Sets the sent time.
     *
     *  @param time a sent time
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    protected void setSentTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "sent time");
        this.sentTime = time;
        return;
    }


    /**
     *  Gets the sender <code> Id </code> of this message. 
     *
     *  @return the sender agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSenderId() {
        if (!isSent()) {
            throw new org.osid.IllegalStateException("isSent() is false");
        }

        return (this.sender.getId());
    }


    /**
     *  Gets the sender of this message. 
     *
     *  @return the sender 
     *  @throws org.osid.IllegalStateException <code> isSent() </code>
     *          is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSender()
        throws org.osid.OperationFailedException {

        if (!isSent()) {
            throw new org.osid.IllegalStateException("isSent() is false");
        }

        return (this.sender);
    }


    /**
     *  Sets the sender.
     *
     *  @param sender a sender
     *  @throws org.osid.NullArgumentException
     *          <code>sender</code> is <code>null</code>
     */

    protected void setSender(org.osid.resource.Resource sender) {
        nullarg(sender, "sender");
        this.sender = sender;
        return;
    }


    /**
     *  Gets the sending agent <code> Id </code> of this message.
     *
     *  @return the sending agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSendingAgentId() {
        if (!isSent()) {
            throw new org.osid.IllegalStateException("isSent() is false");
        }

        return (this.sendingAgent.getId());
    }


    /**
     *  Gets the sending agent of this message. 
     *
     *  @return the sending agent 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getSendingAgent()
        throws org.osid.OperationFailedException {

        if (!isSent()) {
            throw new org.osid.IllegalStateException("isSent() is false");
        }

        return (this.sendingAgent);
    }


    /**
     *  Sets the sending agent.
     *
     *  @param agent a sending agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setSendingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "sending agent");
        this.sendingAgent = agent;
        return;
    }


    /**
     *  Gets the time this message was received. 
     *
     *  @return the time 
     *  @throws org.osid.IllegalStateException <code> isSent() </code>
     *          is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReceivedTime() {
        if (!isSent()) {
            throw new org.osid.IllegalStateException("isSent() is false");
        }
        
        return (this.receivedTime);
    }


    /**
     *  Sets the received time.
     *
     *  @param time a received time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setReceivedTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "received time");
        this.receivedTime = time;
        return;
    }


    /**
     *  Gets the list of all addressed recipient <code> Ids </code> of this 
     *  message. 
     *
     *  @return the recipient resource <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getRecipientIds() {
        if (!isSent()) {
            throw new org.osid.IllegalStateException("isSent() is false");
        }

        try {
            org.osid.resource.ResourceList recipients = getRecipients();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(recipients));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the list of all addressed recipients of this message. 
     *
     *  @return the recpient resources 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getRecipients()
        throws org.osid.OperationFailedException {

        if (!isSent()) {
            throw new org.osid.IllegalStateException("isSent() is false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.recipients));
    }


    /**
     *  Adds a recipient.
     *
     *  @param recipient a recipient
     *  @throws org.osid.NullArgumentException
     *          <code>recipient</code> is <code>null</code>
     */

    protected void addRecipient(org.osid.resource.Resource recipient) {
        nullarg(recipient, "recipient");
        this.recipients.add(recipient);
        return;
    }


    /**
     *  Sets all the recipients.
     *
     *  @param recipients a collection of recipients
     *  @throws org.osid.NullArgumentException
     *          <code>recipients</code> is <code>null</code>
     */

    protected void setRecipients(java.util.Collection<org.osid.resource.Resource> recipients) {
        nullarg(recipients, "recipients");

        this.recipients.clear();
        this.recipients.addAll(recipients);

        return;
    }


    /**
     *  Tests if the resource related to the authenticated agent is one of the 
     *  recipients of this message. 
     *
     *  @return <code> true </code> if this agent is a recipient, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean isRecipient() {
        if (!isSent()) {
            throw new org.osid.IllegalStateException("isSent() is false");
        }

        if (this.receipt == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the receipt <code> Id </code> for this message. A receipt is 
     *  available for the receiver of this message. 
     *
     *  @return the receipt <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isRecipient() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReceiptId() {
        if (!isRecipient()) {
            throw new org.osid.IllegalStateException("isRecipient() is false");
        }

        return (this.receipt.getId());
    }


    /**
     *  Gets the receipt for this message. A receipt is available for the 
     *  receiver of this message. 
     *
     *  @return the receipt 
     *  @throws org.osid.IllegalStateException <code> isRecipient() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.messaging.Receipt getReceipt()
        throws org.osid.OperationFailedException {

        if (!isRecipient()) {
            throw new org.osid.IllegalStateException("isRecipient() is false");
        }

        return (this.receipt);
    }


    /**
     *  Sets the receipt.
     *
     *  @param receipt a receipt
     *  @throws org.osid.NullArgumentException
     *          <code>receipt</code> is <code>null</code>
     */

    protected void setReceipt(org.osid.messaging.Receipt receipt) {
        nullarg(receipt, "receipt");
        this.receipt = receipt;
        return;
    }


    /**
     *  Tests if this message supports the given record
     *  <code>Type</code>.
     *
     *  @param  messageRecordType a message record type 
     *  @return <code>true</code> if the messageRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type messageRecordType) {
        for (org.osid.messaging.records.MessageRecord record : this.records) {
            if (record.implementsRecordType(messageRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Message</code> record <code>Type</code>.
     *
     *  @param  messageRecordType the message record type 
     *  @return the message record 
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(messageRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MessageRecord getMessageRecord(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MessageRecord record : this.records) {
            if (record.implementsRecordType(messageRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(messageRecordType + " is not supported");
    }


    /**
     *  Adds a record to this message. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param messageRecord the message record
     *  @param messageRecordType message record type
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecord</code> or
     *          <code>messageRecordTypemessage</code> is
     *          <code>null</code>
     */
            
    protected void addMessageRecord(org.osid.messaging.records.MessageRecord messageRecord, 
                                    org.osid.type.Type messageRecordType) {

        nullarg(messageRecord, "message record");
        addRecordType(messageRecordType);
        this.records.add(messageRecord);
        
        return;
    }
}
