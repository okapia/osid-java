//
// AbstractQueryPathLookupSession.java
//
//    An inline adapter that maps a PathLookupSession to
//    a PathQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.topology.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PathLookupSession to
 *  a PathQuerySession.
 */

public abstract class AbstractQueryPathLookupSession
    extends net.okapia.osid.jamocha.topology.path.spi.AbstractPathLookupSession
    implements org.osid.topology.path.PathLookupSession {

      private boolean effectiveonly = false;

    private final org.osid.topology.path.PathQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPathLookupSession.
     *
     *  @param querySession the underlying path query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPathLookupSession(org.osid.topology.path.PathQuerySession querySession) {
        nullarg(querySession, "path query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Graph</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.session.getGraphId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this 
     *  session.
     *
     *  @return the <code>Graph</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getGraph());
    }


    /**
     *  Tests if this user can perform <code>Path</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPaths() {
        return (this.session.canSearchPaths());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include paths in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.session.useFederatedGraphView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.session.useIsolatedGraphView();
        return;
    }
    

    /**
     *  Only paths whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectivePathView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All paths of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectivePathView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Path</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Path</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Path</code> and
     *  retained for compatibility.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  pathId <code>Id</code> of the
     *          <code>Path</code>
     *  @return the path
     *  @throws org.osid.NotFoundException <code>pathId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pathId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.Path getPath(org.osid.id.Id pathId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchId(pathId, true);
        org.osid.topology.path.PathList paths = this.session.getPathsByQuery(query);
        if (paths.hasNext()) {
            return (paths.getNextPath());
        } 
        
        throw new org.osid.NotFoundException(pathId + " not found");
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  paths specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Paths</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, paths are returned that are currently effective.
     *  In any effective mode, effective paths and those currently expired
     *  are returned.
     *
     *  @param  pathIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>pathIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByIds(org.osid.id.IdList pathIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();

        try (org.osid.id.IdList ids = pathIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> which does not include
     *  paths of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently effective.
     *  In any effective mode, effective paths and those currently expired
     *  are returned.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchGenusType(pathGenusType, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> and include any additional
     *  paths with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByParentGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchParentGenusType(pathGenusType, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a <code>PathList</code> containing the given
     *  path record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  pathRecordType a path record type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByRecordType(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchRecordType(pathRecordType, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a <code>PathList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible
     *  through this session.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Path</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.path.PathList getPathsOnDate(org.osid.calendaring.DateTime from, 
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getPathsByQuery(query));
    }
        

    /**
     *  Gets a list of paths corresponding to a starting node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.path.PathList getPathsForStartingNode(org.osid.id.Id startingNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchStartingNodeId(startingNodeId, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a list of paths corresponding to a starting node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible
     *  through this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForStartingNodeOnDate(org.osid.id.Id startingNodeId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchStartingNodeId(startingNodeId, true);
        query.matchDate(from, to, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a list of paths corresponding to a ending node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>endingNodeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.path.PathList getPathsForEndingNode(org.osid.id.Id endingNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchEndingNodeId(endingNodeId, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a list of paths corresponding to a ending node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>endingNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForEndingNodeOnDate(org.osid.id.Id endingNodeId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchEndingNodeId(endingNodeId, true);
        query.matchDate(from, to, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a list of paths corresponding to starting node and ending
     *  node <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param startingNodeId the <code>Id</code> of the starting node
     *  @param endingNodeId the <code>Id</code> of the ending node
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingNodeId</code>, <code>endingNodeId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForNodes(org.osid.id.Id startingNodeId,
                                                            org.osid.id.Id endingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.path.PathQuery query = getQuery();
        query.matchStartingNodeId(startingNodeId, true);
        query.matchEndingNodeId(endingNodeId, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a list of paths corresponding to starting node and ending
     *  node <code>Ids</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code>,
     *          <code>endingNodeId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForNodesOnDate(org.osid.id.Id startingNodeId,
                                                                  org.osid.id.Id endingNodeId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.topology.path.PathQuery query = getQuery();
        query.matchStartingNodeId(startingNodeId, true);
        query.matchEndingNodeId(endingNodeId, true);
        query.matchDate(from, to, true);
        return (this.session.getPathsByQuery(query));
    }

    
    /**
     *  Gets all <code>Paths</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Paths</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.topology.path.PathQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.topology.path.PathQuery getQuery() {
        org.osid.topology.path.PathQuery query = this.session.getPathQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
