//
// AbstractImmutablePriceSchedule.java
//
//     Wraps a mutable PriceSchedule to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.priceschedule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>PriceSchedule</code> to hide modifiers. This
 *  wrapper provides an immutized PriceSchedule from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying priceSchedule whose state changes are visible.
 */

public abstract class AbstractImmutablePriceSchedule
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.ordering.PriceSchedule {

    private final org.osid.ordering.PriceSchedule priceSchedule;


    /**
     *  Constructs a new <code>AbstractImmutablePriceSchedule</code>.
     *
     *  @param priceSchedule the price schedule to immutablize
     *  @throws org.osid.NullArgumentException <code>priceSchedule</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePriceSchedule(org.osid.ordering.PriceSchedule priceSchedule) {
        super(priceSchedule);
        this.priceSchedule = priceSchedule;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the prices. 
     *
     *  @return the price <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPriceIds() {
        return (this.priceSchedule.getPriceIds());
    }


    /**
     *  Gets the prices. 
     *
     *  @return the prices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceList getPrices()
        throws org.osid.OperationFailedException {

        return (this.priceSchedule.getPrices());
    }


    /**
     *  Gets the price schedule record corresponding to the given <code> 
     *  PriceSchedule </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  priceScheduleRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(priceScheduleRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  priceScheduleRecordType the type of price schedule record to 
     *          retrieve 
     *  @return the price schedule record 
     *  @throws org.osid.NullArgumentException <code> priceScheduleRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(priceScheduleRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.records.PriceScheduleRecord getPriceScheduleRecord(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException {

        return (this.priceSchedule.getPriceScheduleRecord(priceScheduleRecordType));
    }
}

