//
// AbstractCourseRegistrationBatchManager.java
//
//     An adapter for a CourseRegistrationBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.registration.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseRegistrationBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseRegistrationBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.course.registration.batch.CourseRegistrationBatchManager>
    implements org.osid.course.registration.batch.CourseRegistrationBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseRegistrationBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseRegistrationBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseRegistrationBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseRegistrationBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of actvity bundles is available. 
     *
     *  @return <code> true </code> if an activity bundle bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleBatchAdmin() {
        return (getAdapteeManager().supportsActivityBundleBatchAdmin());
    }


    /**
     *  Tests if bulk administration of registrations is available. 
     *
     *  @return <code> true </code> if a registration bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationBatchAdmin() {
        return (getAdapteeManager().supportsRegistrationBatchAdmin());
    }


    /**
     *  Tests if bulk administration of activity registrations is available. 
     *
     *  @return <code> true </code> if an activity registration bulk 
     *          administrative service is available, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationBatchAdmin() {
        return (getAdapteeManager().supportsActivityRegistrationBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  bundle administration service. 
     *
     *  @return an <code> ActivityBundleBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.batch.ActivityBundleBatchAdminSession getActivityBundleBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  bundle administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.batch.ActivityBundleBatchAdminSession getActivityBundleBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  registration administration service. 
     *
     *  @return a <code> RegistrationBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.batch.RegistrationBatchAdminSession getRegistrationBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  registration administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.batch.RegistrationBatchAdminSession getRegistrationBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  registration administration service. 
     *
     *  @return an <code> ActivityRegistrationBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationBatchAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.batch.ActivityRegistrationBatchAdminSession getActivityRegistrationBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  registration administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.batch.ActivityRegistrationBatchAdminSession getActivityRegistrationBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
