//
// InvariantMapProxyStepConstrainerEnablerLookupSession
//
//    Implements a StepConstrainerEnabler lookup service backed by a fixed
//    collection of stepConstrainerEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules;


/**
 *  Implements a StepConstrainerEnabler lookup service backed by a fixed
 *  collection of step constrainer enablers. The step constrainer enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyStepConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.workflow.rules.spi.AbstractMapStepConstrainerEnablerLookupSession
    implements org.osid.workflow.rules.StepConstrainerEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyStepConstrainerEnablerLookupSession} with no
     *  step constrainer enablers.
     *
     *  @param office the office
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyStepConstrainerEnablerLookupSession(org.osid.workflow.Office office,
                                                  org.osid.proxy.Proxy proxy) {
        setOffice(office);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyStepConstrainerEnablerLookupSession} with a single
     *  step constrainer enabler.
     *
     *  @param office the office
     *  @param stepConstrainerEnabler a single step constrainer enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code stepConstrainerEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyStepConstrainerEnablerLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.rules.StepConstrainerEnabler stepConstrainerEnabler, org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putStepConstrainerEnabler(stepConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyStepConstrainerEnablerLookupSession} using
     *  an array of step constrainer enablers.
     *
     *  @param office the office
     *  @param stepConstrainerEnablers an array of step constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code stepConstrainerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyStepConstrainerEnablerLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.rules.StepConstrainerEnabler[] stepConstrainerEnablers, org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putStepConstrainerEnablers(stepConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyStepConstrainerEnablerLookupSession} using a
     *  collection of step constrainer enablers.
     *
     *  @param office the office
     *  @param stepConstrainerEnablers a collection of step constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code stepConstrainerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyStepConstrainerEnablerLookupSession(org.osid.workflow.Office office,
                                                  java.util.Collection<? extends org.osid.workflow.rules.StepConstrainerEnabler> stepConstrainerEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putStepConstrainerEnablers(stepConstrainerEnablers);
        return;
    }
}
