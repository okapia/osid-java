//
// AbstractResponseNotificationSession.java
//
//     A template for making ResponseNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Response} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Response} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for response entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractResponseNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inquiry.ResponseNotificationSession {

    private boolean federated = false;
    private org.osid.inquiry.Inquest inquest = new net.okapia.osid.jamocha.nil.inquiry.inquest.UnknownInquest();


    /**
     *  Gets the {@code Inquest/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Inquest Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.inquest.getId());
    }

    
    /**
     *  Gets the {@code Inquest} associated with this session.
     *
     *  @return the {@code Inquest} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.inquest);
    }


    /**
     *  Sets the {@code Inquest}.
     *
     *  @param inquest the inquest for this session
     *  @throws org.osid.NullArgumentException {@code inquest}
     *          is {@code null}
     */

    protected void setInquest(org.osid.inquiry.Inquest inquest) {
        nullarg(inquest, "inquest");
        this.inquest = inquest;
        return;
    }


    /**
     *  Tests if this user can register for {@code Response}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForResponseNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeResponseNotification() </code>.
     */

    @OSID @Override
    public void reliableResponseNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableResponseNotifications() {
        return;
    }


    /**
     *  Acknowledge a response notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeResponseNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for responses in inquests which
     *  are children of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new responses. {@code
     *  ResponseReceiver.newResponse()} is invoked when a new {@code
     *  Response} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewResponses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new responses for the given
     *  inquiry {@code Id}. {@code ResponseReceiver.newResponse()} is
     *  invoked when a new {@code Response} is created.
     *
     *  @param  inquiryId the {@code Id} of the inquiry to monitor
     *  @throws org.osid.NullArgumentException {@code inquiryId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewResponsesForInquiry(org.osid.id.Id inquiryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new responses for the given
     *  responder {@code Id}. {@code ResponseReceiver.newResponse()}
     *  is invoked when a new {@code Response} is created.
     *
     *  @param  resourceId the {@code Id} of the responder to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewResponsesForResponder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated responses. {@code
     *  ResponseReceiver.changedResponse()} is invoked when a response
     *  is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedResponses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated responses for the given
     *  inquiry {@code Id}. {@code ResponseReceiver.changedResponse()}
     *  is invoked when a {@code Response} in this inquest is changed.
     *
     *  @param  inquiryId the {@code Id} of the inquiry to monitor
     *  @throws org.osid.NullArgumentException {@code inquiryId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedResponsesForInquiry(org.osid.id.Id inquiryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated responses for the given
     *  responder {@code Id}. {@code
     *  ResponseReceiver.changedResponse()} is invoked when a {@code
     *  Response} in this inquest is changed.
     *
     *  @param  resourceId the {@code Id} of the responder to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedResponsesForResponder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated response. {@code
     *  ResponseReceiver.changedResponse()} is invoked when the
     *  specified response is changed.
     *
     *  @param responseId the {@code Id} of the {@code Response} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code responseId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedResponse(org.osid.id.Id responseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted responses. {@code
     *  ResponseReceiver.deletedResponse()} is invoked when a response
     *  is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedResponses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted responses for the given
     *  inquiry {@code Id}. {@code ResponseReceiver.deletedResponse()}
     *  is invoked when a {@code Response} is deleted or removed from
     *  this inquest.
     *
     *  @param  inquiryId the {@code Id} of the inquiry to monitor
     *  @throws org.osid.NullArgumentException {@code inquiryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedResponsesForInquiry(org.osid.id.Id inquiryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted responses for the given
     *  responder {@code Id}. {@code
     *  ResponseReceiver.deletedResponse()} is invoked when a {@code
     *  Response} is deleted or removed from this inquest.
     *
     *  @param  resourceId the {@code Id} of the responder to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedResponsesForResponder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted response. {@code
     *  ResponseReceiver.deletedResponse()} is invoked when the
     *  specified response is deleted.
     *
     *  @param responseId the {@code Id} of the
     *          {@code Response} to monitor
     *  @throws org.osid.NullArgumentException {@code responseId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedResponse(org.osid.id.Id responseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
