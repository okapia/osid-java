//
// SubjectElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.subject.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class SubjectElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the SubjectElement Id.
     *
     *  @return the subject element Id
     */

    public static org.osid.id.Id getSubjectEntityId() {
        return (makeEntityId("osid.ontology.Subject"));
    }


    /**
     *  Gets the AncestorSubjectId element Id.
     *
     *  @return the AncestorSubjectId element Id
     */

    public static org.osid.id.Id getAncestorSubjectId() {
        return (makeQueryElementId("osid.ontology.subject.AncestorSubjectId"));
    }


    /**
     *  Gets the AncestorSubject element Id.
     *
     *  @return the AncestorSubject element Id
     */

    public static org.osid.id.Id getAncestorSubject() {
        return (makeQueryElementId("osid.ontology.subject.AncestorSubject"));
    }


    /**
     *  Gets the DescendantSubjectId element Id.
     *
     *  @return the DescendantSubjectId element Id
     */

    public static org.osid.id.Id getDescendantSubjectId() {
        return (makeQueryElementId("osid.ontology.subject.DescendantSubjectId"));
    }


    /**
     *  Gets the DescendantSubject element Id.
     *
     *  @return the DescendantSubject element Id
     */

    public static org.osid.id.Id getDescendantSubject() {
        return (makeQueryElementId("osid.ontology.subject.DescendantSubject"));
    }


    /**
     *  Gets the RelevancyId element Id.
     *
     *  @return the RelevancyId element Id
     */

    public static org.osid.id.Id getRelevancyId() {
        return (makeQueryElementId("osid.ontology.subject.RelevancyId"));
    }


    /**
     *  Gets the Relevancy element Id.
     *
     *  @return the Relevancy element Id
     */

    public static org.osid.id.Id getRelevancy() {
        return (makeQueryElementId("osid.ontology.subject.Relevancy"));
    }


    /**
     *  Gets the OntologyId element Id.
     *
     *  @return the OntologyId element Id
     */

    public static org.osid.id.Id getOntologyId() {
        return (makeQueryElementId("osid.ontology.subject.OntologyId"));
    }


    /**
     *  Gets the Ontology element Id.
     *
     *  @return the Ontology element Id
     */

    public static org.osid.id.Id getOntology() {
        return (makeQueryElementId("osid.ontology.subject.Ontology"));
    }
}
