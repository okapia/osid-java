//
// AbstractAdapterOsidEnabler.java
//
//     Defines an Enabler wrapper.
//
//
// Tom Coppeto
// Okapia
// 20 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an Enabler wrapper.
 */

public abstract class AbstractAdapterOsidEnabler
    extends AbstractAdapterOsidRule
    implements org.osid.OsidEnabler {

    private final org.osid.OsidEnabler enabler;


    /**
     *  Creates a new <code>AbstractAdapterOsidEnabler</code>.
     *
     *  @param enabler the <code>OsidEnabler</code> for this object
     *  @throws org.osid.NullArgumentException <code>enabler</code>
     *          is <code>null</code>
     */

    protected AbstractAdapterOsidEnabler(org.osid.OsidEnabler enabler) {
        super(enabler);
        this.enabler = enabler;
        return;
    }

    
    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        return (this.enabler.isEffective());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.enabler.getStartDate());
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.enabler.getEndDate());
    }


    /**
     *  Tests if the effectiveness of the enabler is governed by a
     *  <code> Schedule. </code> If a schedule exists, it is bounded
     *  by the effective dates of this enabler. If <code>
     *  isEffectiveBySchedule() </code> is <code> true, </code> <code>
     *  isEffectiveByEvent() </code> and <code>
     *  isEffectiveByCyclicEvent() </code> must be <code>
     *  false. </code>
     *
     *  @return <code> true </code> if the enabler is governed by
     *          schedule, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isEffectiveBySchedule() {
        return (this.enabler.isEffectiveBySchedule());
    }


    /**
     *  Gets the schedule <code> Id </code>. 
     *
     *  @return the schedule <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isEffectiveBySchedule() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScheduleId() {
        return (this.enabler.getScheduleId());
    }


    /**
     *  Gets the schedule. 
     *
     *  @return the schedule 
     *  @throws org.osid.IllegalStateException <code>
     *          isEffectiveBySchedule() </code> is <code> false
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Schedule getSchedule()
        throws org.osid.OperationFailedException {

        return (this.enabler.getSchedule());
    }


    /**
     *  Tests if the effectiveness of the enabler is governed by an
     *  <code> Event </code> such that the start and end dates of the
     *  event govern the effectiveness. The event may also be a <code>
     *  RecurringEvent </code> in which case the enabler is effective
     *  for start and end dates of each event in the series If an
     *  event exists, it is bounded by the effective dates of this
     *  enabler. If <code> isEffectiveByEvent() </code> is <code>
     *  true, </code> <code> isEffectiveBySchedule() </code> and
     *  <code> isEffectiveByCyclicEvent() </code> must be <code>
     *  false.  </code>
     *
     *  @return <code> true </code> if the enabler is governed by an event, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isEffectiveByEvent() {
        return (this.enabler.isEffectiveByEvent());
    }


    /**
     *  Gets the event <code> Id </code>. 
     *
     *  @return the event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isEffectiveByEvent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEventId() {
        return (this.enabler.getEventId());
    }


    /**
     *  Gets the event. 
     *
     *  @return the event 
     *  @throws org.osid.IllegalStateException <code> isEffectiveByEvent() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent()
        throws org.osid.OperationFailedException {

        return (this.enabler.getEvent());
    }


    /**
     *  Tests if the effectiveness of the enabler is governed by a
     *  <code> CyclicEvent. </code> If a cyclic event exists, it is
     *  evaluated by the accompanying cyclic time period.. If <code>
     *  isEffectiveByCyclicEvent() </code> is <code> true, </code>
     *  <code> isEffectiveBySchedule() </code> and <code>
     *  isEffectiveByEvent() </code> must be <code> false. </code>
     *
     *  @return <code> true </code> if the enabler is governed by a cyclic 
     *          event, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isEffectiveByCyclicEvent() {
        return (this.enabler.isEffectiveByCyclicEvent());
    }


    /**
     *  Gets the cyclic event <code> Id </code>. 
     *
     *  @return the cyclic event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          isEffectiveByCyclicEvent() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCyclicEventId() {
        return (this.enabler.getCyclicEventId());
    }


    /**
     *  Gets the cyclic event. 
     *
     *  @return the cyclic event 
     *  @throws org.osid.IllegalStateException <code> 
     *          isEffectiveByCyclicEvent() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getCyclicEvent()
        throws org.osid.OperationFailedException {
        
        return (this.enabler.getCyclicEvent());
    }


    /**
     *  Tests if the effectiveness of the enabler applies to a demographic 
     *  resource. 
     *
     *  @return <code> true </code> if the rule apples to a demographic. 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isEffectiveForDemographic() {
        return (this.enabler.isEffectiveForDemographic());
    }


    /**
     *  Gets the demographic resource <code> Id </code>. 
     *
     *  @return the resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          isEffectiveForDemographic() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDemographicId() {
        if (!isEffectiveForDemographic()) {
            throw new org.osid.IllegalStateException("isEffectiveForDemographic is false");
        }

        return (this.enabler.getDemographicId());
    }


    /**
     *  Gets the demographic resource. 
     *
     *  @return the resource representing the demographic 
     *  @throws org.osid.IllegalStateException <code> 
     *          isEffectiveForDemographic() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getDemographic()
        throws org.osid.OperationFailedException {
        
        return (this.enabler.getDemographic());
    }
}
