//
// AbstractMapPollsLookupSession
//
//    A simple framework for providing a Polls lookup service
//    backed by a fixed collection of polls.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Polls lookup service backed by a
 *  fixed collection of polls. The polls are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Polls</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPollsLookupSession
    extends net.okapia.osid.jamocha.voting.spi.AbstractPollsLookupSession
    implements org.osid.voting.PollsLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.Polls> pollses = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.Polls>());


    /**
     *  Makes a <code>Polls</code> available in this session.
     *
     *  @param  polls a polls
     *  @throws org.osid.NullArgumentException <code>polls<code>
     *          is <code>null</code>
     */

    protected void putPolls(org.osid.voting.Polls polls) {
        this.pollses.put(polls.getId(), polls);
        return;
    }


    /**
     *  Makes an array of polls available in this session.
     *
     *  @param  pollses an array of polls
     *  @throws org.osid.NullArgumentException <code>pollses<code>
     *          is <code>null</code>
     */

    protected void putPollses(org.osid.voting.Polls[] pollses) {
        putPollses(java.util.Arrays.asList(pollses));
        return;
    }


    /**
     *  Makes a collection of polls available in this session.
     *
     *  @param  pollses a collection of polls
     *  @throws org.osid.NullArgumentException <code>pollses<code>
     *          is <code>null</code>
     */

    protected void putPollses(java.util.Collection<? extends org.osid.voting.Polls> pollses) {
        for (org.osid.voting.Polls polls : pollses) {
            this.pollses.put(polls.getId(), polls);
        }

        return;
    }


    /**
     *  Removes a Polls from this session.
     *
     *  @param  pollsId the <code>Id</code> of the polls
     *  @throws org.osid.NullArgumentException <code>pollsId<code> is
     *          <code>null</code>
     */

    protected void removePolls(org.osid.id.Id pollsId) {
        this.pollses.remove(pollsId);
        return;
    }


    /**
     *  Gets the <code>Polls</code> specified by its <code>Id</code>.
     *
     *  @param  pollsId <code>Id</code> of the <code>Polls</code>
     *  @return the polls
     *  @throws org.osid.NotFoundException <code>pollsId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>pollsId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.Polls polls = this.pollses.get(pollsId);
        if (polls == null) {
            throw new org.osid.NotFoundException("polls not found: " + pollsId);
        }

        return (polls);
    }


    /**
     *  Gets all <code>Polls</code>. In plenary mode, the returned
     *  list contains all known polls or an error results. Otherwise,
     *  the returned list may contain only those polls that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @return a list of <code>Polls</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getAllPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.polls.ArrayPollsList(this.pollses.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.pollses.clear();
        super.close();
        return;
    }
}
