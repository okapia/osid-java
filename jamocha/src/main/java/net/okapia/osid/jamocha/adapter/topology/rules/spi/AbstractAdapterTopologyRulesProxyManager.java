//
// AbstractTopologyRulesProxyManager.java
//
//     An adapter for a TopologyRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a TopologyRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterTopologyRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.topology.rules.TopologyRulesProxyManager>
    implements org.osid.topology.rules.TopologyRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterTopologyRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterTopologyRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterTopologyRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterTopologyRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up edge enablers is supported. 
     *
     *  @return <code> true </code> if edge enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerLookup() {
        return (getAdapteeManager().supportsEdgeEnablerLookup());
    }


    /**
     *  Tests if querying edge enablers is supported. 
     *
     *  @return <code> true </code> if edge enabler query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerQuery() {
        return (getAdapteeManager().supportsEdgeEnablerQuery());
    }


    /**
     *  Tests if searching edge enablers is supported. 
     *
     *  @return <code> true </code> if edge enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerSearch() {
        return (getAdapteeManager().supportsEdgeEnablerSearch());
    }


    /**
     *  Tests if an edge enabler administrative service is supported. 
     *
     *  @return <code> true </code> if edge enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerAdmin() {
        return (getAdapteeManager().supportsEdgeEnablerAdmin());
    }


    /**
     *  Tests if an edge enabler notification service is supported. 
     *
     *  @return <code> true </code> if edge enabler notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerNotification() {
        return (getAdapteeManager().supportsEdgeEnablerNotification());
    }


    /**
     *  Tests if an edge enabler graph lookup service is supported. 
     *
     *  @return <code> true </code> if an edge enabler graph lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerGraph() {
        return (getAdapteeManager().supportsEdgeEnablerGraph());
    }


    /**
     *  Tests if an edge enabler graph service is supported. 
     *
     *  @return <code> true </code> if edge enabler graph assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerGraphAssignment() {
        return (getAdapteeManager().supportsEdgeEnablerGraphAssignment());
    }


    /**
     *  Tests if an edge enabler graph lookup service is supported. 
     *
     *  @return <code> true </code> if an edge enabler graph service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerSmartGraph() {
        return (getAdapteeManager().supportsEdgeEnablerSmartGraph());
    }


    /**
     *  Tests if an edge enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an edge enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerRuleLookup() {
        return (getAdapteeManager().supportsEdgeEnablerRuleLookup());
    }


    /**
     *  Tests if an edge enabler rule application service is supported. 
     *
     *  @return <code> true </code> if edge enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerRuleApplication() {
        return (getAdapteeManager().supportsEdgeEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> EdgeEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> EdgeEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEdgeEnablerRecordTypes() {
        return (getAdapteeManager().getEdgeEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> EdgeEnabler </code> record type is 
     *  supported. 
     *
     *  @param  edgeEnablerRecordType a <code> Type </code> indicating an 
     *          <code> EdgeEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> edgeEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerRecordType(org.osid.type.Type edgeEnablerRecordType) {
        return (getAdapteeManager().supportsEdgeEnablerRecordType(edgeEnablerRecordType));
    }


    /**
     *  Gets the supported <code> EdgeEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> EdgeEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEdgeEnablerSearchRecordTypes() {
        return (getAdapteeManager().getEdgeEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> EdgeEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  edgeEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> EdgeEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          edgeEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerSearchRecordType(org.osid.type.Type edgeEnablerSearchRecordType) {
        return (getAdapteeManager().supportsEdgeEnablerSearchRecordType(edgeEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerLookupSession getEdgeEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  lookup service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerLookupSession getEdgeEnablerLookupSessionForGraph(org.osid.id.Id graphId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerLookupSessionForGraph(graphId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerQuerySession getEdgeEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  query service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerQuerySession getEdgeEnablerQuerySessionForGraph(org.osid.id.Id graphId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerQuerySessionForGraph(graphId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerSearchSession getEdgeEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enablers 
     *  earch service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerSearchSession getEdgeEnablerSearchSessionForGraph(org.osid.id.Id graphId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerSearchSessionForGraph(graphId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerAdminSession getEdgeEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerAdminSession getEdgeEnablerAdminSessionForGraph(org.osid.id.Id graphId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerAdminSessionForGraph(graphId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  notification service. 
     *
     *  @param  edgeEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> edgeEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerNotificationSession getEdgeEnablerNotificationSession(org.osid.topology.rules.EdgeEnablerReceiver edgeEnablerReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerNotificationSession(edgeEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  notification service for the given graph. 
     *
     *  @param  edgeEnablerReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> edgeEnablerReceiver, 
     *          graphId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerNotificationSession getEdgeEnablerNotificationSessionForGraph(org.osid.topology.rules.EdgeEnablerReceiver edgeEnablerReceiver, 
                                                                                                            org.osid.id.Id graphId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerNotificationSessionForGraph(edgeEnablerReceiver, graphId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup edge enabler/graph 
     *  mappings for edge enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerGraphSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerGraphSession getEdgeEnablerGraphSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerGraphSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning edge 
     *  enablers to ontologies for edge. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerGraphAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerGraphAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerGraphAssignmentSession getEdgeEnablerGraphAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerGraphAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage edge enabler smart 
     *  ontologies. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerSmartGraph() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerSmartGraphSession getEdgeEnablerSmartGraphSession(org.osid.id.Id graphId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerSmartGraphSession(graphId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  mapping lookup service for looking up the rules applied to the graph. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleLookupSession getEdgeEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  mapping lookup service for the given graph for looking up rules 
     *  applied to a graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleLookupSession getEdgeEnablerRuleLookupSessionForGraph(org.osid.id.Id graphId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerRuleLookupSessionForGraph(graphId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  assignment service to apply enablers to ontologies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleApplicationSession getEdgeEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  assignment service for the given graph to apply enablers to 
     *  ontologies. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleApplicationSession getEdgeEnablerRuleApplicationSessionForGraph(org.osid.id.Id graphId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeEnablerRuleApplicationSessionForGraph(graphId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
