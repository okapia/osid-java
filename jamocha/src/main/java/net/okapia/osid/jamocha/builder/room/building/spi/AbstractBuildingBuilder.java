//
// AbstractBuilding.java
//
//     Defines a Building builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.building.spi;


/**
 *  Defines a <code>Building</code> builder.
 */

public abstract class AbstractBuildingBuilder<T extends AbstractBuildingBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.room.building.BuildingMiter building;


    /**
     *  Constructs a new <code>AbstractBuildingBuilder</code>.
     *
     *  @param building the building to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBuildingBuilder(net.okapia.osid.jamocha.builder.room.building.BuildingMiter building) {
        super(building);
        this.building = building;
        return;
    }


    /**
     *  Builds the building.
     *
     *  @return the new building
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.room.Building build() {
        (new net.okapia.osid.jamocha.builder.validator.room.building.BuildingValidator(getValidations())).validate(this.building);
        return (new net.okapia.osid.jamocha.builder.room.building.ImmutableBuilding(this.building));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the building miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.room.building.BuildingMiter getMiter() {
        return (this.building);
    }


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>address</code> is <code>null</code>
     */

    public T address(org.osid.contact.Address address) {
        getMiter().setAddress(address);
        return (self());
    }


    /**
     *  Sets the official name.
     *
     *  @param officialName an official name
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>officialName</code> is <code>null</code>
     */

    public T officialName(org.osid.locale.DisplayText officialName) {
        getMiter().setOfficialName(officialName);
        return (self());
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    public T number(String number) {
        getMiter().setNumber(number);
        return (self());
    }


    /**
     *  Sets the enclosing building.
     *
     *  @param building an enclosing building
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    public T enclosingBuilding(org.osid.room.Building building) {
        getMiter().setEnclosingBuilding(building);
        return (self());
    }


    /**
     *  Adds a subdivision.
     *
     *  @param subdivision a subdivision
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>subdivision</code> is <code>null</code>
     */

    public T subdivision(org.osid.room.Building subdivision) {
        getMiter().addSubdivision(subdivision);
        return (self());
    }


    /**
     *  Sets all the subdivisions.
     *
     *  @param subdivisions a collection of subdivisions
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>subdivisions</code> is <code>null</code>
     */

    public T subdivisions(java.util.Collection<org.osid.room.Building> subdivisions) {
        getMiter().setSubdivisions(subdivisions);
        return (self());
    }


    /**
     *  Sets the gross area.
     *
     *  @param area a gross area
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    public T grossArea(java.math.BigDecimal area) {
        getMiter().setGrossArea(area);
        return (self());
    }


    /**
     *  Adds a Building record.
     *
     *  @param record a building record
     *  @param recordType the type of building record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.room.records.BuildingRecord record, org.osid.type.Type recordType) {
        getMiter().addBuildingRecord(record, recordType);
        return (self());
    }
}       


