//
// AbstractFederatingParameterProcessorLookupSession.java
//
//     An abstract federating adapter for a ParameterProcessorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ParameterProcessorLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingParameterProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.configuration.rules.ParameterProcessorLookupSession>
    implements org.osid.configuration.rules.ParameterProcessorLookupSession {

    private boolean parallel = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();


    /**
     *  Constructs a new <code>AbstractFederatingParameterProcessorLookupSession</code>.
     */

    protected AbstractFederatingParameterProcessorLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.configuration.rules.ParameterProcessorLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Configuration/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the <code>Configuration</code>.
     *
     *  @param  configuration the configuration for this session
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this user can perform <code>ParameterProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParameterProcessors() {
        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            if (session.canLookupParameterProcessors()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ParameterProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParameterProcessorView() {
        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            session.useComparativeParameterProcessorView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ParameterProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParameterProcessorView() {
        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            session.usePlenaryParameterProcessorView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameter processors in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            session.useFederatedConfigurationView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            session.useIsolatedConfigurationView();
        }

        return;
    }


    /**
     *  Only active parameter processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveParameterProcessorView() {
        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            session.useActiveParameterProcessorView();
        }

        return;
    }


    /**
     *  Active and inactive parameter processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusParameterProcessorView() {
        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            session.useAnyStatusParameterProcessorView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>ParameterProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ParameterProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ParameterProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorId <code>Id</code> of the
     *          <code>ParameterProcessor</code>
     *  @return the parameter processor
     *  @throws org.osid.NotFoundException <code>parameterProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>parameterProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessor getParameterProcessor(org.osid.id.Id parameterProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            try {
                return (session.getParameterProcessor(parameterProcessorId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(parameterProcessorId + " not found");
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  parameterProcessors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ParameterProcessors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByIds(org.osid.id.IdList parameterProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.configuration.rules.parameterprocessor.MutableParameterProcessorList ret = new net.okapia.osid.jamocha.configuration.rules.parameterprocessor.MutableParameterProcessorList();

        try (org.osid.id.IdList ids = parameterProcessorIds) {
            while (ids.hasNext()) {
                ret.addParameterProcessor(getParameterProcessor(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  parameter processor genus <code>Type</code> which does not include
     *  parameter processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorGenusType a parameterProcessor genus type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByGenusType(org.osid.type.Type parameterProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.parameterprocessor.FederatingParameterProcessorList ret = getParameterProcessorList();

        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            ret.addParameterProcessorList(session.getParameterProcessorsByGenusType(parameterProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  parameter processor genus <code>Type</code> and include any additional
     *  parameter processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorGenusType a parameterProcessor genus type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByParentGenusType(org.osid.type.Type parameterProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.parameterprocessor.FederatingParameterProcessorList ret = getParameterProcessorList();

        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            ret.addParameterProcessorList(session.getParameterProcessorsByParentGenusType(parameterProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> containing the given
     *  parameter processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorRecordType a parameterProcessor record type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByRecordType(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.parameterprocessor.FederatingParameterProcessorList ret = getParameterProcessorList();

        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            ret.addParameterProcessorList(session.getParameterProcessorsByRecordType(parameterProcessorRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ParameterProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @return a list of <code>ParameterProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.parameterprocessor.FederatingParameterProcessorList ret = getParameterProcessorList();

        for (org.osid.configuration.rules.ParameterProcessorLookupSession session : getSessions()) {
            ret.addParameterProcessorList(session.getParameterProcessors());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.configuration.rules.parameterprocessor.FederatingParameterProcessorList getParameterProcessorList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.configuration.rules.parameterprocessor.ParallelParameterProcessorList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.configuration.rules.parameterprocessor.CompositeParameterProcessorList());
        }
    }
}
