//
// AbstractMapProfileEntryLookupSession
//
//    A simple framework for providing a ProfileEntry lookup service
//    backed by a fixed collection of profile entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ProfileEntry lookup service backed by a
 *  fixed collection of profile entries. The profile entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProfileEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProfileEntryLookupSession
    extends net.okapia.osid.jamocha.profile.spi.AbstractProfileEntryLookupSession
    implements org.osid.profile.ProfileEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.profile.ProfileEntry> profileEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.profile.ProfileEntry>());
    private final java.util.Map<org.osid.id.Id, org.osid.id.Id> explicitProfileEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.id.Id>());


    /**
     *  Makes a <code>ProfileEntry</code> available in this session.
     *
     *  @param  profileEntry a profile entry
     *  @throws org.osid.NullArgumentException <code>profileEntry<code>
     *          is <code>null</code>
     */

    protected void putProfileEntry(org.osid.profile.ProfileEntry profileEntry) {
        this.profileEntries.put(profileEntry.getId(), profileEntry);
        return;
    }


    /**
     *  Makes an array of profile entries available in this session.
     *
     *  @param  profileEntries an array of profile entries
     *  @throws org.osid.NullArgumentException <code>profileEntries<code>
     *          is <code>null</code>
     */

    protected void putProfileEntries(org.osid.profile.ProfileEntry[] profileEntries) {
        putProfileEntries(java.util.Arrays.asList(profileEntries));
        return;
    }


    /**
     *  Makes a collection of profile entries available in this session.
     *
     *  @param  profileEntries a collection of profile entries
     *  @throws org.osid.NullArgumentException <code>profileEntries<code>
     *          is <code>null</code>
     */

    protected void putProfileEntries(java.util.Collection<? extends org.osid.profile.ProfileEntry> profileEntries) {
        for (org.osid.profile.ProfileEntry profileEntry : profileEntries) {
            this.profileEntries.put(profileEntry.getId(), profileEntry);
        }

        return;
    }


    /**
     *  Removes a ProfileEntry from this session.
     *
     *  @param  profileEntryId the <code>Id</code> of the profile entry
     *  @throws org.osid.NullArgumentException <code>profileEntryId<code> is
     *          <code>null</code>
     */

    protected void removeProfileEntry(org.osid.id.Id profileEntryId) {
        this.profileEntries.remove(profileEntryId);
        return;
    }


    /**
     *  Sets the explicit profile entry for an implicit profile
     *  entry. Only one explicit profile entry per customer. This
     *  method does not check for the existence of the Ids or wether
     *  they are really explicit or implicit.
     *
     *  @param  explicitProfileEntryId the explicit profile entry Id
     *  @param  implicitProfileEntryId the implicit profile entry Id
     *  @throws org.osid.NullArgumentException
     *          <code>explicitProfileEntryId<code> or
     *          <code>implicitProfileEntryId<code> is
     *          <code>null</code>
     */

    protected void setExplicitProfileEntry(org.osid.id.Id explicitProfileEntryId, 
                                           org.osid.id.Id implicitProfileEntryId) {

        this.explicitProfileEntries.put(implicitProfileEntryId, explicitProfileEntryId);
        return;
    }


    /**
     *  Unsets the explicit profile entry for an implicit profile
     *  entry. 
     *
     *  @param  implicitProfileEntryId the implicit profile entry Id
     *  @throws org.osid.NullArgumentException
     *          <code>implicitProfileEntryId<code> is
     *          <code>null</code>
     */

    protected void removeExplicitProfileEntry(org.osid.id.Id implicitProfileEntryId) {
        this.explicitProfileEntries.remove(implicitProfileEntryId);
        return;
    }


    /**
     *  Gets the <code>ProfileEntry</code> specified by its <code>Id</code>.
     *
     *  @param  profileEntryId <code>Id</code> of the <code>ProfileEntry</code>
     *  @return the profileEntry
     *  @throws org.osid.NotFoundException <code>profileEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>profileEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntry getProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.profile.ProfileEntry profileEntry = this.profileEntries.get(profileEntryId);
        if (profileEntry == null) {
            throw new org.osid.NotFoundException("profileEntry not found: " + profileEntryId);
        }

        return (profileEntry);
    }


    /**
     *  Gets the explicit <code>ProfileEntry</code> that generated the
     *  given implicit profile entry. If the given
     *  <code>ProfileEntry</code> is explicit, then the same
     *  <code>ProfileEntry</code> is returned.
     *
     *  @param  profileEntryId a profile entry 
     *  @return the explicit <code>ProfileEntry</code> 
     *  @throws org.osid.NotFoundException <code>profileEntryId</code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntry getExplicitProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.id.Id id = this.explicitProfileEntries.get(profileEntryId);
        org.osid.profile.ProfileEntry entry;

        if (id == null) {
            entry = getProfileEntry(profileEntryId);
            if (entry.isImplicit()) {
                throw new org.osid.NotFoundException("no implicit profile entry found for " + profileEntryId);
            }
        } else {
            entry = getProfileEntry(id);
            if (entry.isImplicit()) {
                throw new org.osid.OperationFailedException("an implicit profile entry was found for " + profileEntryId);
            }
        }

        return (entry);
    }


    /**
     *  Gets all <code>ProfileEntries</code>. In plenary mode, the returned
     *  list contains all known profileEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  profileEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ProfileEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.profileentry.ArrayProfileEntryList(this.profileEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.profileEntries.clear();
        this.explicitProfileEntries.clear();

        super.close();
        return;
    }
}
