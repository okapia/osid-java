//
// CourseSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.course;


/**
 *  A template for implementing a search results.
 */

public final class CourseSearchResults
    extends net.okapia.osid.jamocha.course.course.spi.AbstractCourseSearchResults
    implements org.osid.course.CourseSearchResults {


    /**
     *  Constructs a new <code>CourseSearchResults.
     *
     *  @param courses the result set
     *  @param courseQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>courses</code>
     *          or <code>courseQueryInspector</code> is
     *          <code>null</code>
     */

    public CourseSearchResults(org.osid.course.CourseList courses,
                                 org.osid.course.CourseQueryInspector courseQueryInspector) {
        super(courses, courseQueryInspector);
        return;
    }
}
