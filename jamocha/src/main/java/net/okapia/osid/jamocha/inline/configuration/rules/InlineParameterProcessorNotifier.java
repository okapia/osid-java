//
// InlineParameterProcessorNotifier.java
//
//     A callback interface for performing parameter processor
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.configuration.rules.spi;


/**
 *  A callback interface for performing parameter processor
 *  notifications.
 */

public interface InlineParameterProcessorNotifier {



    /**
     *  Notifies the creation of a new parameter processor.
     *
     *  @param parameterProcessorId the {@code Id} of the new 
     *         parameter processor
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newParameterProcessor(org.osid.id.Id parameterProcessorId);


    /**
     *  Notifies the change of an updated parameter processor.
     *
     *  @param parameterProcessorId the {@code Id} of the changed
     *         parameter processor
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedParameterProcessor(org.osid.id.Id parameterProcessorId);


    /**
     *  Notifies the deletion of an parameter processor.
     *
     *  @param parameterProcessorId the {@code Id} of the deleted
     *         parameter processor
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedParameterProcessor(org.osid.id.Id parameterProcessorId);

}
