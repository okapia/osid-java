//
// AbstractImmutableResource.java
//
//     Wraps a mutable Resource to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.resource.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Resource</code> to hide modifiers. This
 *  wrapper provides an immutized Resource from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying resource whose state changes are visible.
 */

public abstract class AbstractImmutableResource
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.resource.Resource {

    private final org.osid.resource.Resource resource;


    /**
     *  Constructs a new <code>AbstractImmutableResource</code>.
     *
     *  @param resource the resource to immutablize
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableResource(org.osid.resource.Resource resource) {
        super(resource);
        this.resource = resource;
        return;
    }


    /**
     *  Tests if this resource is a group. A resource that is a group can be 
     *  used in the group sessions. 
     *
     *  @return <code> true </code> if this resource is a group, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGroup() {
        return (this.resource.isGroup());
    }


    /**
     *  Tests if this resource is a demographic. A resource that is a 
     *  demographic can be used in the demographic service and the group 
     *  sessions. 
     *
     *  @return <code> true </code> if this resource is a demographic, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isDemographic() {
        return (this.resource.isDemographic());
    }


    /**
     *  Tests if this resource has an avatar. 
     *
     *  @return <code> true </code> if this resource has an avatar, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAvatar() {
        return (this.resource.hasAvatar());
    }


    /**
     *  Gets the asset <code> Id. </code> 
     *
     *  @return the asset <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAvatar() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAvatarId() {
        return (this.resource.getAvatarId());
    }


    /**
     *  Gets the asset. 
     *
     *  @return the asset 
     *  @throws org.osid.IllegalStateException <code> hasAvatar() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.Asset getAvatar()
        throws org.osid.OperationFailedException {

        return (this.resource.getAvatar());
    }


    /**
     *  Gets the resource record corresponding to the given <code> Resource 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  resourceRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(resourceRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  resourceRecordType the resource record type 
     *  @return the resource record 
     *  @throws org.osid.NullArgumentException <code> resourceRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(resourceRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRecord getResourceRecord(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException {

        return (this.resource.getResourceRecord(resourceRecordType));
    }
}

