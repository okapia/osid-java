//
// AbstractImmutableJournalEntry.java
//
//     Wraps a mutable JournalEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.journaling.journalentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>JournalEntry</code> to hide modifiers. This
 *  wrapper provides an immutized JournalEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying journalEntry whose state changes are visible.
 */

public abstract class AbstractImmutableJournalEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.journaling.JournalEntry {

    private final org.osid.journaling.JournalEntry journalEntry;


    /**
     *  Constructs a new <code>AbstractImmutableJournalEntry</code>.
     *
     *  @param journalEntry the journal entry to immutablize
     *  @throws org.osid.NullArgumentException <code>journalEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableJournalEntry(org.osid.journaling.JournalEntry journalEntry) {
        super(journalEntry);
        this.journalEntry = journalEntry;
        return;
    }


    /**
     *  Gets the branch <code> Id </code> for this entry. 
     *
     *  @return the branch <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBranchId() {
        return (this.journalEntry.getBranchId());
    }


    /**
     *  Gets the branch for this entry. 
     *
     *  @return the branch 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.journaling.Branch getBranch()
        throws org.osid.OperationFailedException {

        return (this.journalEntry.getBranch());
    }


    /**
     *  Gets the principal <code> Id </code> of the journaled object. 
     *
     *  @return the source <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceId() {
        return (this.journalEntry.getSourceId());
    }


    /**
     *  Gets the version <code> Id </code> of the journaled object. 
     *
     *  @return the version <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getVersionId() {
        return (this.journalEntry.getVersionId());
    }


    /**
     *  Gets the <code> timestamp </code> of this journal entry. 
     *
     *  @return the time of this entry 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.journalEntry.getTimestamp());
    }


    /**
     *  Gets the Id of the resource who created this entry. 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.journalEntry.getResourceId());
    }


    /**
     *  Gets the resource who created this entry. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.journalEntry.getResource());
    }


    /**
     *  Gets the Id of the agent who created this entry. 
     *
     *  @return the <code> Agent </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.journalEntry.getAgentId());
    }


    /**
     *  Gets the agent who created this entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.journalEntry.getAgent());
    }


    /**
     *  Gets the journal entry record corresponding to the given <code> 
     *  JournalEntry </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  journalEntryRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(journalEntryRecordType) </code> is <code> true </code> . 
     *
     *  @param  journalEntryRecordType the type of journal entry record to 
     *          retrieve 
     *  @return the journal entry record 
     *  @throws org.osid.NullArgumentException <code> journalEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(journalEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.records.JournalEntryRecord getJournalEntryRecord(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.journalEntry.getJournalEntryRecord(journalEntryRecordType));
    }
}

