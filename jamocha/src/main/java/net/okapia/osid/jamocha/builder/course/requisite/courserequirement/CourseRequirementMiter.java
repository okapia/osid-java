//
// CourseRequirementMiter.java
//
//     Defines a CourseRequirement miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.courserequirement;


/**
 *  Defines a <code>CourseRequirement</code> miter for use with the builders.
 */

public interface CourseRequirementMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.course.requisite.CourseRequirement {


    /**
     *  Adds an alternative requisite.
     *
     *  @param requisite an alternative requisite
     *  @throws org.osid.NullArgumentException <code>requisite</code>
q     *          is <code>null</code>
     */

    public void addAltRequisite(org.osid.course.requisite.Requisite requisite);


    /**
     *  Sets all the alternative requisites.
     *
     *  @param requisites a collection of alternative requisites
     *  @throws org.osid.NullArgumentException <code>requisites</code>
     *          is <code>null</code>
     */

    public void setAltRequisites(java.util.Collection<org.osid.course.requisite.Requisite> requisites);


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public void setCourse(org.osid.course.Course course);


    /**
     *  Sets the requires completion flag.
     *
     *  @param requires <code> true </code> if a completion of the
     *  course is required, <code> false </code> is the course
     *         is a co-requisite
     */

    public void setRequiresCompletion(boolean requires);


    /**
     *  Sets the timeframe.
     *
     *  @param timeframe a timeframe
     *  @throws org.osid.NullArgumentException <code>timeframe</code>
     *          is <code>null</code>
     */

    public void setTimeframe(org.osid.calendaring.Duration timeframe);


    /**
     *  Sets the minimum grade.
     *
     *  @param grade a minimum grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public void setMinimumGrade(org.osid.grading.Grade grade);


    /**
     *  Sets the minimum score system.
     *
     *  @param system a minimum score system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public void setMinimumScoreSystem(org.osid.grading.GradeSystem system);


    /**
     *  Sets the minimum score.
     *
     *  @param score a minimum score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public void setMinimumScore(java.math.BigDecimal score);


    /**
     *  Sets the minimum earned credits.
     *
     *  @param credits the minimum earned credits
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public void setMinimumEarnedCredits(java.math.BigDecimal credits);


    /**
     *  Adds a CourseRequirement record.
     *
     *  @param record a courseRequirement record
     *  @param recordType the type of courseRequirement record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCourseRequirementRecord(org.osid.course.requisite.records.CourseRequirementRecord record, org.osid.type.Type recordType);
}       


