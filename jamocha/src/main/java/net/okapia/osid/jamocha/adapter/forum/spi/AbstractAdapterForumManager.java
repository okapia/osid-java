//
// AbstractForumManager.java
//
//     An adapter for a ForumManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.forum.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ForumManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterForumManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.forum.ForumManager>
    implements org.osid.forum.ForumManager {


    /**
     *  Constructs a new {@code AbstractAdapterForumManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterForumManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterForumManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterForumManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any post federation is exposed. Federation is exposed when a 
     *  specific post may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of posts 
     *  appears as a single post. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of an post lookup service. 
     *
     *  @return <code> true </code> if post lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostLookup() {
        return (getAdapteeManager().supportsPostLookup());
    }


    /**
     *  Tests if querying posts is available. 
     *
     *  @return <code> true </code> if post query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostQuery() {
        return (getAdapteeManager().supportsPostQuery());
    }


    /**
     *  Tests if searching for posts is available. 
     *
     *  @return <code> true </code> if post search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSearch() {
        return (getAdapteeManager().supportsPostSearch());
    }


    /**
     *  Tests for the availability of a post administrative service for 
     *  creating and deleting posts. 
     *
     *  @return <code> true </code> if post administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostAdmin() {
        return (getAdapteeManager().supportsPostAdmin());
    }


    /**
     *  Tests for the availability of a post notification service. 
     *
     *  @return <code> true </code> if post notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostNotification() {
        return (getAdapteeManager().supportsPostNotification());
    }


    /**
     *  Tests if a post to forum lookup session is available. 
     *
     *  @return <code> true </code> if post forum lookup session is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostForum() {
        return (getAdapteeManager().supportsPostForum());
    }


    /**
     *  Tests if a post to forum assignment session is available. 
     *
     *  @return <code> true </code> if post forum assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostForumAssignment() {
        return (getAdapteeManager().supportsPostForumAssignment());
    }


    /**
     *  Tests if a post smart foruming session is available. 
     *
     *  @return <code> true </code> if post smart foruming is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSmartForum() {
        return (getAdapteeManager().supportsPostSmartForum());
    }


    /**
     *  Tests for the availability of a reply lookup service. 
     *
     *  @return <code> true </code> if reply lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyLookup() {
        return (getAdapteeManager().supportsReplyLookup());
    }


    /**
     *  Tests if searching for replies is available. 
     *
     *  @return <code> true </code> if reply search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyAdmin() {
        return (getAdapteeManager().supportsReplyAdmin());
    }


    /**
     *  Tests if reply notification is available. 
     *
     *  @return <code> true </code> if reply notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyNotification() {
        return (getAdapteeManager().supportsReplyNotification());
    }


    /**
     *  Tests for the availability of an forum lookup service. 
     *
     *  @return <code> true </code> if forum lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumLookup() {
        return (getAdapteeManager().supportsForumLookup());
    }


    /**
     *  Tests if querying forums is available. 
     *
     *  @return <code> true </code> if forum query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumQuery() {
        return (getAdapteeManager().supportsForumQuery());
    }


    /**
     *  Tests if searching for forums is available. 
     *
     *  @return <code> true </code> if forum search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumSearch() {
        return (getAdapteeManager().supportsForumSearch());
    }


    /**
     *  Tests for the availability of a forum administrative service for 
     *  creating and deleting forums. 
     *
     *  @return <code> true </code> if forum administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumAdmin() {
        return (getAdapteeManager().supportsForumAdmin());
    }


    /**
     *  Tests for the availability of a forum notification service. 
     *
     *  @return <code> true </code> if forum notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumNotification() {
        return (getAdapteeManager().supportsForumNotification());
    }


    /**
     *  Tests for the availability of a forum hierarchy traversal service. 
     *
     *  @return <code> true </code> if forum hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumHierarchy() {
        return (getAdapteeManager().supportsForumHierarchy());
    }


    /**
     *  Tests for the availability of a forum hierarchy design service. 
     *
     *  @return <code> true </code> if forum hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumHierarchyDesign() {
        return (getAdapteeManager().supportsForumHierarchyDesign());
    }


    /**
     *  Tests if forum batch service is available. 
     *
     *  @return <code> true </code> if forum batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumBatch() {
        return (getAdapteeManager().supportsForumBatch());
    }


    /**
     *  Gets the supported <code> Post </code> record types. 
     *
     *  @return a list containing the supported post record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostRecordTypes() {
        return (getAdapteeManager().getPostRecordTypes());
    }


    /**
     *  Tests if the given <code> Post </code> record type is supported. 
     *
     *  @param  postRecordType a <code> Type </code> indicating a <code> Post 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostRecordType(org.osid.type.Type postRecordType) {
        return (getAdapteeManager().supportsPostRecordType(postRecordType));
    }


    /**
     *  Gets the supported post search record types. 
     *
     *  @return a list containing the supported post search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostSearchRecordTypes() {
        return (getAdapteeManager().getPostSearchRecordTypes());
    }


    /**
     *  Tests if the given post search record type is supported. 
     *
     *  @param  postSearchRecordType a <code> Type </code> indicating a post 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostSearchRecordType(org.osid.type.Type postSearchRecordType) {
        return (getAdapteeManager().supportsPostSearchRecordType(postSearchRecordType));
    }


    /**
     *  Gets the supported <code> Reply </code> record types. 
     *
     *  @return a list containing the supported reply record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getReplyRecordTypes() {
        return (getAdapteeManager().getReplyRecordTypes());
    }


    /**
     *  Tests if the given <code> Reply </code> record type is supported. 
     *
     *  @param  replyRecordType a <code> Type </code> indicating a <code> 
     *          Reply </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> replyRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsReplyRecordType(org.osid.type.Type replyRecordType) {
        return (getAdapteeManager().supportsReplyRecordType(replyRecordType));
    }


    /**
     *  Gets the supported reply search record types. 
     *
     *  @return a list containing the supported reply search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getReplySearchRecordTypes() {
        return (getAdapteeManager().getReplySearchRecordTypes());
    }


    /**
     *  Tests if the given reply search record type is supported. 
     *
     *  @param  replySearchRecordType a <code> Type </code> indicating a reply 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> replySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsReplySearchRecordType(org.osid.type.Type replySearchRecordType) {
        return (getAdapteeManager().supportsReplySearchRecordType(replySearchRecordType));
    }


    /**
     *  Gets the supported <code> Forum </code> record types. 
     *
     *  @return a list containing the supported forum record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getForumRecordTypes() {
        return (getAdapteeManager().getForumRecordTypes());
    }


    /**
     *  Tests if the given <code> Forum </code> record type is supported. 
     *
     *  @param  forumRecordType a <code> Type </code> indicating a <code> 
     *          Forum </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> forumRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsForumRecordType(org.osid.type.Type forumRecordType) {
        return (getAdapteeManager().supportsForumRecordType(forumRecordType));
    }


    /**
     *  Gets the supported forum search record types. 
     *
     *  @return a list containing the supported forum search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getForumSearchRecordTypes() {
        return (getAdapteeManager().getForumSearchRecordTypes());
    }


    /**
     *  Tests if the given forum search record type is supported. 
     *
     *  @param  forumSearchRecordType a <code> Type </code> indicating a forum 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> forumSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsForumSearchRecordType(org.osid.type.Type forumSearchRecordType) {
        return (getAdapteeManager().supportsForumSearchRecordType(forumSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service. 
     *
     *  @return a <code> PostLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostLookupSession getPostLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Post </code> 
     *  @return a <code> PostLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostLookupSession getPostLookupSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostLookupSessionForForum(forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service. 
     *
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuerySession getPostQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Post </code> 
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuerySession getPostQuerySessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostQuerySessionForForum(forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service. 
     *
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostSearchSession getPostSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Post </code> 
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostSearchSession getPostSearchSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostSearchSessionForForum(forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administrative service. 
     *
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostAdminSession getPostAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administrative service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Post </code> 
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostAdminSession getPostAdminSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostAdminSessionForForum(forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service. 
     *
     *  @param  postReceiver the receiver 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostNotificationSession getPostNotificationSession(org.osid.forum.PostReceiver postReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostNotificationSession(postReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service for the given forum. 
     *
     *  @param  postReceiver the receiver 
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostNotificationSession getPostNotificationSessionForForum(org.osid.forum.PostReceiver postReceiver, 
                                                                                     org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostNotificationSessionForForum(postReceiver, forumId));
    }


    /**
     *  Gets the session for retrieving post to forum mappings. 
     *
     *  @return a <code> PostForumSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostForum() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostForumSession getPostForumSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostForumSession());
    }


    /**
     *  Gets the session for assigning post to forum mappings. 
     *
     *  @return a <code> PostForumAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostForumAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostForumAssignmentSession getPostForumAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostForumAssignmentSession());
    }


    /**
     *  Gets the session associated with the post smart forum for the given 
     *  forum. 
     *
     *  @param  forumId the <code> Id </code> of the forum 
     *  @return a <code> PostSmartForumSession </code> 
     *  @throws org.osid.NotFoundException <code> forumId </code> not found 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostSmartForum() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostSmartForumSession getPostSmartForumSession(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostSmartForumSession(forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply lookup 
     *  service. 
     *
     *  @return a <code> ReplyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyLookupSession getReplyLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReplyLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply lookup 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> ReplyLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyLookupSession getReplyLookupSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReplyLookupSessionForForum(forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  administration service. 
     *
     *  @return a <code> ReplyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyAdminSession getReplyAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReplyAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  administration service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> ReplyAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyAdminSession getReplyAdminSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReplyAdminSessionForForum(forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  notification service. 
     *
     *  @param  replyReceiver the receiver 
     *  @return a <code> ReplyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> replyReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReplyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyNotificationSession getReplyNotificationSession(org.osid.forum.ReplyReceiver replyReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReplyNotificationSession(replyReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  notification service for the given forum. 
     *
     *  @param  replyReceiver the receiver 
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> ReplyNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> replyReceiver </code> or 
     *          <code> forumId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReplyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyNotificationSession getReplyNotificationSessionForForum(org.osid.forum.ReplyReceiver replyReceiver, 
                                                                                       org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReplyNotificationSessionForForum(replyReceiver, forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum lookup 
     *  service. 
     *
     *  @return a <code> ForumLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumLookupSession getForumLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum query 
     *  service. 
     *
     *  @return a <code> ForumQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuerySession getForumQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum search 
     *  service. 
     *
     *  @return a <code> ForumSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumSearchSession getForumSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  administrative service. 
     *
     *  @return a <code> ForumAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumAdminSession getForumAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  notification service. 
     *
     *  @param  forumReceiver the receiver 
     *  @return a <code> ForumNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> forumReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumNotificationSession getForumNotificationSession(org.osid.forum.ForumReceiver forumReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumNotificationSession(forumReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  hierarchy service. 
     *
     *  @return a <code> ForumHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumHierarchySession getForumHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  hierarchy design service. 
     *
     *  @return a <code> ForumHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumHierarchyDesignSession getForumHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumHierarchyDesignSession());
    }


    /**
     *  Gets a <code> ForumBatchManager. </code> 
     *
     *  @return a <code> ForumBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.batch.ForumBatchManager getForumBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
