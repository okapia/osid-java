//
// AbstractBloggingBatchManager.java
//
//     An adapter for a BloggingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.blogging.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a BloggingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterBloggingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.blogging.batch.BloggingBatchManager>
    implements org.osid.blogging.batch.BloggingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterBloggingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterBloggingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterBloggingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterBloggingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of entries is available. 
     *
     *  @return <code> true </code> if an entry bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBatchAdmin() {
        return (getAdapteeManager().supportsEntryBatchAdmin());
    }


    /**
     *  Tests if bulk administration of blogs is available. 
     *
     *  @return <code> true </code> if a blog bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogBatchAdmin() {
        return (getAdapteeManager().supportsBlogBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service. 
     *
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.EntryBatchAdminSession getEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the <code> Blog </code> 
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Blog </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.EntryBatchAdminSession getEntryBatchAdminSessionForBlog(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryBatchAdminSessionForBlog(blogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk blog 
     *  administration service. 
     *
     *  @return a <code> BlogBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlogBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.BlogBatchAdminSession getBlogBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlogBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
