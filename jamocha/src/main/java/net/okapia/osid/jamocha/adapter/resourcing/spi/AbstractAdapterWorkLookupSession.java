//
// AbstractAdapterWorkLookupSession.java
//
//    A Work lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Work lookup session adapter.
 */

public abstract class AbstractAdapterWorkLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.WorkLookupSession {

    private final org.osid.resourcing.WorkLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterWorkLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterWorkLookupSession(org.osid.resourcing.WorkLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code Work} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupWorks() {
        return (this.session.canLookupWorks());
    }


    /**
     *  A complete view of the {@code Work} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWorkView() {
        this.session.useComparativeWorkView();
        return;
    }


    /**
     *  A complete view of the {@code Work} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWorkView() {
        this.session.usePlenaryWorkView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include works in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    
     
    /**
     *  Gets the {@code Work} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Work} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Work} and
     *  retained for compatibility.
     *
     *  @param workId {@code Id} of the {@code Work}
     *  @return the work
     *  @throws org.osid.NotFoundException {@code workId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code workId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Work getWork(org.osid.id.Id workId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWork(workId));
    }


    /**
     *  Gets a {@code WorkList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  works specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Works} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  workIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Work} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code workIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByIds(org.osid.id.IdList workIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorksByIds(workIds));
    }


    /**
     *  Gets a {@code WorkList} corresponding to the given
     *  work genus {@code Type} which does not include
     *  works of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned {@code Work} list
     *  @throws org.osid.NullArgumentException
     *          {@code workGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorksByGenusType(workGenusType));
    }


    /**
     *  Gets a {@code WorkList} corresponding to the given
     *  work genus {@code Type} and include any additional
     *  works with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned {@code Work} list
     *  @throws org.osid.NullArgumentException
     *          {@code workGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByParentGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorksByParentGenusType(workGenusType));
    }


    /**
     *  Gets a {@code WorkList} containing the given
     *  work record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workRecordType a work record type 
     *  @return the returned {@code Work} list
     *  @throws org.osid.NullArgumentException
     *          {@code workRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByRecordType(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorksByRecordType(workRecordType));
    }


    /**
     *  Gets a {@code WorkList} corresponding to the given job.  In
     *  plenary mode, the returned list contains all known works or an
     *  error results. Otherwise, the returned list may contain only
     *  those works that are accessible through this session.
     *
     *  @param  jobId a job {@code Id} 
     *  @return the returned {@code Work} list 
     *  @throws org.osid.NullArgumentException {@code jobId} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    public org.osid.resourcing.WorkList getWorksForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorksForJob(jobId));
    }


    /**
     *  Gets a {@code WorkList} of uncommitted works corresponding to
     *  the given job {@code .} In plenary mode, the returned list
     *  contains all known works or an error results. Otherwise, the
     *  returned list may contain only those works that are accessible
     *  through this session.
     *
     *  @param  jobId a job {@code Id} 
     *  @return the returned {@code Work} list 
     *  @throws org.osid.NullArgumentException {@code jobId} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getUncommittedWorksForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUncommittedWorksForJob(jobId));
    }


    /**
     *  Gets a {@code WorkList} of incomplete works corresponding to
     *  the given job {@code}. In plenary mode, the returned list
     *  contains all known works or an error results. Otherwise, the
     *  returned list may contain only those works that are accessible
     *  through this session.
     *
     *  @param  jobId a job {@code Id} 
     *  @return the returned {@code Work} list 
     *  @throws org.osid.NullArgumentException {@code jobId} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorksForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIncompleteWorksForJob(jobId));
    }


    /**
     *  Gets a {@code WorkList} of all works created between the given
     *  date range inclusive {@code .} In plenary mode, the returned
     *  list contains all known works or an error results. Otherwise,
     *  the returned list may contain only those works that are
     *  accessible through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned {@code Work} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorksByDate(from, to));
    }


    /**
     *  Gets a {@code WorkList} of incomplete works created between
     *  the given date range inclusive {@code .} In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned {@code Work} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorksByDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIncompleteWorksByDate(from, to));
    }


    /**
     *  Gets a {@code WorkList} of uncommitted works created between
     *  the given date range inclusive {@code .} In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned {@code Work} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getUncommittedWorksByDate(org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUncommittedWorksByDate(from, to));
    }


    /**
     *  Gets a {@code WorkList} of all works of corresponding to the
     *  given job and created between the given date range inclusive.
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session.
     *
     *  @param  jobId a job {@code Id} 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned {@code Work} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code jobId, from} or 
     *          {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByDateForJob(org.osid.id.Id jobId, 
                                                             org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorksByDateForJob(jobId, from, to));
    }


    /**
     *  Gets a {@code WorkList} of incomplete works corresponding to
     *  the given job and created between the given date range
     *  inclusive {@code}. In plenary mode, the returned list contains
     *  all known works or an error results. Otherwise, the returned
     *  list may contain only those works that are accessible through
     *  this session.
     *
     *  @param  jobId a job {@code Id} 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned {@code Work} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code jobId, from} or 
     *          {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorksByDateForJob(org.osid.id.Id jobId, 
                                                                       org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIncompleteWorksByDateForJob(jobId, from, to));
    }


    /**
     *  Gets all incomplete {@code Works.} In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @return a list of {@code Works} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIncompleteWorks());
    }


    /**
     *  Gets all {@code Works} that have no commitments. In plenary
     *  mode, the returned list contains all known works or an error
     *  results.  Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @return a list of uncommitted {@code Works} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getUncommittedWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUncommittedWorks());
    }


    /**
     *  Gets all {@code Works}. 
     *
     *  In plenary mode, the returned list contains all known
     *  works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Works} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorks());
    }
}
