//
// InvariantMapProxyPersonLookupSession
//
//    Implements a Person lookup service backed by a fixed
//    collection of persons. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements a Person lookup service backed by a fixed
 *  collection of persons. The persons are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyPersonLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractMapPersonLookupSession
    implements org.osid.personnel.PersonLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPersonLookupSession} with no
     *  persons.
     *
     *  @param realm the realm
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPersonLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.proxy.Proxy proxy) {
        setRealm(realm);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyPersonLookupSession} with a single
     *  person.
     *
     *  @param realm the realm
     *  @param person a single person
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm},
     *          {@code person} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPersonLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.personnel.Person person, org.osid.proxy.Proxy proxy) {

        this(realm, proxy);
        putPerson(person);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPersonLookupSession} using
     *  an array of persons.
     *
     *  @param realm the realm
     *  @param persons an array of persons
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm},
     *          {@code persons} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPersonLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.personnel.Person[] persons, org.osid.proxy.Proxy proxy) {

        this(realm, proxy);
        putPersons(persons);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPersonLookupSession} using a
     *  collection of persons.
     *
     *  @param realm the realm
     *  @param persons a collection of persons
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm},
     *          {@code persons} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPersonLookupSession(org.osid.personnel.Realm realm,
                                                  java.util.Collection<? extends org.osid.personnel.Person> persons,
                                                  org.osid.proxy.Proxy proxy) {

        this(realm, proxy);
        putPersons(persons);
        return;
    }
}
