//
// AbstractInstructionQueryInspector.java
//
//     A template for making an InstructionQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.instruction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for instructions.
 */

public abstract class AbstractInstructionQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.rules.check.InstructionQueryInspector {

    private final java.util.Collection<org.osid.rules.check.records.InstructionQueryInspectorRecord> records = new java.util.ArrayList<>();
    
    private final OsidRelationshipQueryInspector inspector = new OsidRelationshipQueryInspector();


    /**
     *  Gets the end reaosn state <code> Id </code> query terms. 
     *
     *  @return the state <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEndReasonIdTerms() {
        return (this.inspector.getEndReasonIdTerms());
    }


    /**
     *  Gets the end reason state query terms.
     *
     *  @return the end reason state terms
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getEndReasonTerms() {
        return (this.inspector.getEndReasonTerms());
    }


    /**
     *  Gets the agenda <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgendaIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agenda query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQueryInspector[] getAgendaTerms() {
        return (new org.osid.rules.check.AgendaQueryInspector[0]);
    }


    /**
     *  Gets the check query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCheckIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the check query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQueryInspector[] getCheckTerms() {
        return (new org.osid.rules.check.CheckQueryInspector[0]);
    }


    /**
     *  Gets the message query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getMessageTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the warning query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getWarningTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the continue-on-fail query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getContinueOnFailTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the engine <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEngineIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the engine query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.EngineQueryInspector[] getEngineTerms() {
        return (new org.osid.rules.EngineQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given instruction query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an instruction implementing the requested record.
     *
     *  @param instructionRecordType an instruction record type
     *  @return the instruction query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(instructionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.InstructionQueryInspectorRecord getInstructionQueryInspectorRecord(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.InstructionQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(instructionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this instruction query. 
     *
     *  @param instructionQueryInspectorRecord instruction query inspector
     *         record
     *  @param instructionRecordType instruction record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInstructionQueryInspectorRecord(org.osid.rules.check.records.InstructionQueryInspectorRecord instructionQueryInspectorRecord, 
                                                   org.osid.type.Type instructionRecordType) {

        addRecordType(instructionRecordType);
        nullarg(instructionRecordType, "instruction record type");
        this.records.add(instructionQueryInspectorRecord);        
        return;
    }


    protected class OsidRelationshipQueryInspector
        extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
        implements org.osid.OsidRelationshipQueryInspector {
    }
}
