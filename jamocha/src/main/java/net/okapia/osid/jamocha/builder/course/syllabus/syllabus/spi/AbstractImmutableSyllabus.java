//
// AbstractImmutableSyllabus.java
//
//     Wraps a mutable Syllabus to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.syllabus.syllabus.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Syllabus</code> to hide modifiers. This
 *  wrapper provides an immutized Syllabus from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying syllabus whose state changes are visible.
 */

public abstract class AbstractImmutableSyllabus
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.course.syllabus.Syllabus {

    private final org.osid.course.syllabus.Syllabus syllabus;


    /**
     *  Constructs a new <code>AbstractImmutableSyllabus</code>.
     *
     *  @param syllabus the syllabus to immutablize
     *  @throws org.osid.NullArgumentException <code>syllabus</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSyllabus(org.osid.course.syllabus.Syllabus syllabus) {
        super(syllabus);
        this.syllabus = syllabus;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the course. 
     *
     *  @return the course <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.syllabus.getCourseId());
    }


    /**
     *  Gets the course. 
     *
     *  @return the course 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.syllabus.getCourse());
    }


    /**
     *  Gets the syllabus record corresponding to the given <code> Syllabus 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  syllabusRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(syllabusRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  syllabusRecordType the type of syllabus record to retrieve 
     *  @return the syllabus record 
     *  @throws org.osid.NullArgumentException <code> syllabusRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(syllabusRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.records.SyllabusRecord getSyllabusRecord(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException {

        return (this.syllabus.getSyllabusRecord(syllabusRecordType));
    }
}

