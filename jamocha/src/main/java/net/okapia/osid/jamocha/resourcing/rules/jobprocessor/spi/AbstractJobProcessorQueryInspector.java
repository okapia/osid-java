//
// AbstractJobProcessorQueryInspector.java
//
//     A template for making a JobProcessorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for job processors.
 */

public abstract class AbstractJobProcessorQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQueryInspector
    implements org.osid.resourcing.rules.JobProcessorQueryInspector {

    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the job <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledJobIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the job query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.JobQueryInspector[] getRuledJobTerms() {
        return (new org.osid.resourcing.JobQueryInspector[0]);
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given job processor query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a job processor implementing the requested record.
     *
     *  @param jobProcessorRecordType a job processor record type
     *  @return the job processor query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorQueryInspectorRecord getJobProcessorQueryInspectorRecord(org.osid.type.Type jobProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobProcessorQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(jobProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job processor query. 
     *
     *  @param jobProcessorQueryInspectorRecord job processor query inspector
     *         record
     *  @param jobProcessorRecordType jobProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobProcessorQueryInspectorRecord(org.osid.resourcing.rules.records.JobProcessorQueryInspectorRecord jobProcessorQueryInspectorRecord, 
                                                   org.osid.type.Type jobProcessorRecordType) {

        addRecordType(jobProcessorRecordType);
        nullarg(jobProcessorRecordType, "job processor record type");
        this.records.add(jobProcessorQueryInspectorRecord);        
        return;
    }
}
