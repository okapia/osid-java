//
// AbstractSpeedZone.java
//
//     Defines a SpeedZone.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.speedzone.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>SpeedZone</code>.
 */

public abstract class AbstractSpeedZone
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.mapping.path.SpeedZone {

    private org.osid.mapping.path.Path path;
    private org.osid.mapping.Coordinate startingCoordinate;
    private org.osid.mapping.Coordinate endingCoordinate;
    private org.osid.mapping.Speed speedLimit;
    private boolean implicit = false;

    private final java.util.Collection<org.osid.mapping.path.records.SpeedZoneRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the path <code> Id </code> of this speed zone. 
     *
     *  @return the <code> Id </code> of the path 
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        return (this.path.getId());
    }


    /**
     *  Gets the path of this speed zone. 
     *
     *  @return the path 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        return (this.path);
    }


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException
     *          <code>path</code> is <code>null</code>
     */

    protected void setPath(org.osid.mapping.path.Path path) {
        nullarg(path, "path");
        this.path = path;
        return;
    }


    /**
     *  Gets the starting coordinate of the speed zone on the path. 
     *
     *  @return the start of the zone 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getStartingCoordinate() {
        return (this.startingCoordinate);
    }


    /**
     *  Sets the starting coordinate.
     *
     *  @param coordinate a starting coordinate
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    protected void setStartingCoordinate(org.osid.mapping.Coordinate coordinate) {
        nullarg(coordinate, "starting coordinate");
        this.startingCoordinate = coordinate;
        return;
    }


    /**
     *  Gets the ending coordinate of the speed zone on the path. 
     *
     *  @return the end of the zone 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getEndingCoordinate() {
        return (this.endingCoordinate);
    }


    /**
     *  Sets the ending coordinate.
     *
     *  @param coordinate an ending coordinate
     *  @throws org.osid.NullArgumentException
     *          <code>coordinate</code> is <code>null</code>
     */

    protected void setEndingCoordinate(org.osid.mapping.Coordinate coordinate) {
        nullarg(coordinate, "ending coordinate");
        this.endingCoordinate = coordinate;
        return;
    }


    /**
     *  Tests if this speed zone is implicit. An implicit speed zone is 
     *  generated from other information such as an <code> Obstacle. </code> 
     *
     *  @return <code> true </code> if this speed zone is implicit,
     *          <code> false </code> if explicitly managed
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.implicit);
    }


    /**
     *  Sets the implicit flag.
     *
     *  @param implicit <code> true </code> if this speed zone is
     *         implicit, <code> false </code> if explicitly managed
     */

    protected void setImplicit(boolean implicit) {
        this.implicit = implicit;
        return;
    }


    /**
     *  Gets the speed limit in this zone. 
     *
     *  @return the speed limit 
     */

    @OSID @Override
    public org.osid.mapping.Speed getSpeedLimit() {
        return (this.speedLimit);
    }


    /**
     *  Sets the speed limit.
     *
     *  @param limit a speed limit
     *  @throws org.osid.NullArgumentException
     *          <code>limit</code> is <code>null</code>
     */

    protected void setSpeedLimit(org.osid.mapping.Speed limit) {
        nullarg(limit, "speed limit");
        this.speedLimit = speedLimit;
        return;
    }


    /**
     *  Tests if this speedZone supports the given record
     *  <code>Type</code>.
     *
     *  @param  speedZoneRecordType a speed zone record type 
     *  @return <code>true</code> if the speedZoneRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type speedZoneRecordType) {
        for (org.osid.mapping.path.records.SpeedZoneRecord record : this.records) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>SpeedZone</code> record <code>Type</code>.
     *
     *  @param  speedZoneRecordType the speed zone record type 
     *  @return the speed zone record 
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneRecord getSpeedZoneRecord(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SpeedZoneRecord record : this.records) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneRecordType + " is not supported");
    }


    /**
     *  Adds a record to this speed zone. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param speedZoneRecord the speed zone record
     *  @param speedZoneRecordType speed zone record type
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecord</code> or
     *          <code>speedZoneRecordTypespeedZone</code> is
     *          <code>null</code>
     */
            
    protected void addSpeedZoneRecord(org.osid.mapping.path.records.SpeedZoneRecord speedZoneRecord, 
                                      org.osid.type.Type speedZoneRecordType) {

        nullarg(speedZoneRecord, "speed zone record");
        addRecordType(speedZoneRecordType);
        this.records.add(speedZoneRecord);
        
        return;
    }
}
