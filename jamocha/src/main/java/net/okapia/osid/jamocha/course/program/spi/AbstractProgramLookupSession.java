//
// AbstractProgramLookupSession.java
//
//    A starter implementation framework for providing a Program
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Program
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPrograms(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProgramLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.program.ProgramLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Program</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPrograms() {
        return (true);
    }


    /**
     *  A complete view of the <code>Program</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProgramView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Program</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProgramView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include programs in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active programs are returned by methods in this session.
     */

    @OSID @Override
    public void useActiveProgramView() {
       this.activeonly = true;         
       return;
    }


    /**
     *  All programs of any active or inactive status are returned by
     *  all methods in this session.
     */

    @OSID @Override
    public void useAnyStatusProgramView() {
        this.activeonly = false;
        return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */

    protected boolean isActiveOnly() {
        return (this.activeonly);
    }

     
    /**
     *  Gets the <code>Program</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Program</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Program</code> and
     *  retained for compatibility.
     *
     *  In active mode, programs are returned that are currently
     *  active.  In any status mode, active and inactive programs
     *  expired are returned.
     *
     *  @param  programId <code>Id</code> of the
     *          <code>Program</code>
     *  @return the program
     *  @throws org.osid.NotFoundException <code>programId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>programId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram(org.osid.id.Id programId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.program.ProgramList programs = getPrograms()) {
            while (programs.hasNext()) {
                org.osid.course.program.Program program = programs.getNextProgram();
                if (program.getId().equals(programId)) {
                    return (program);
                }
            }
        } 

        throw new org.osid.NotFoundException(programId + " not found");
    }


    /**
     *  Gets a <code>ProgramList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  programs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Programs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, programs are returned that are currently
     *  active.  In any status mode, active and inactive programs
     *  expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPrograms()</code>.
     *
     *  @param  programIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Program</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>programIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByIds(org.osid.id.IdList programIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.program.Program> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = programIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProgram(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("program " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.program.program.LinkedProgramList(ret));
    }


    /**
     *  Gets a <code>ProgramList</code> corresponding to the given
     *  program genus <code>Type</code> which does not include
     *  programs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  programs or an error results. Otherwise, the returned list
     *  may contain only those programs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, programs are returned that are currently
     *  active.  In any status mode, active and inactive programs
     *  expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPrograms()</code>.
     *
     *  @param  programGenusType a program genus type 
     *  @return the returned <code>Program</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByGenusType(org.osid.type.Type programGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.program.ProgramGenusFilterList(getPrograms(), programGenusType));
    }


    /**
     *  Gets a <code>ProgramList</code> corresponding to the given
     *  program genus <code>Type</code> and include any additional
     *  programs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  programs or an error results. Otherwise, the returned list
     *  may contain only those programs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, programs are returned that are currently
     *  active.  In any status mode, active and inactive programs
     *  expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPrograms()</code>.
     *
     *  @param  programGenusType a program genus type 
     *  @return the returned <code>Program</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByParentGenusType(org.osid.type.Type programGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProgramsByGenusType(programGenusType));
    }


    /**
     *  Gets a <code>ProgramList</code> containing the given
     *  program record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  programs or an error results. Otherwise, the returned list
     *  may contain only those programs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, programs are returned that are currently
     *  active.  In any status mode, active and inactive programs
     *  expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPrograms()</code>.
     *
     *  @param  programRecordType a program record type 
     *  @return the returned <code>Program</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByRecordType(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.program.ProgramRecordFilterList(getPrograms(), programRecordType));
    }


    /**
     *  Gets a <code> ProgramList </code> containing the given
     *  creential.
     *  
     *  In plenary mode, the returned list contains all known programs
     *  or an error results. Otherwise, the returned list may contain
     *  only those programs that are accessible through this session.
     *  
     *  In active mode, programs are returned that are currently
     *  active.  In any status mode, active and inactive programs
     *  expired are returned.
     *
     *  @param  credentialId a credential <code> Id </code> 
     *  @return the returned <code> ProgramList </code> list 
     *  @throws org.osid.NullArgumentException <code> credentialId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByCredential(org.osid.id.Id credentialId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.program.Program> ret = new java.util.ArrayList<>();

        try (org.osid.course.program.ProgramList programs = getPrograms()) {
            while (programs.hasNext()) {
                org.osid.course.program.Program program = programs.getNextProgram();
                if (program.earnsCredentials()) {
                    try (org.osid.id.IdList credentialIds = program.getCredentialIds()) {
                        while (credentialIds.hasNext()) {
                            if (credentialIds.getNextId().equals(credentialId)) {
                                ret.add(program);
                            }
                        }
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.program.program.LinkedProgramList(ret));
    }


    /**
     *  Gets all <code>Programs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  programs or an error results. Otherwise, the returned list
     *  may contain only those programs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, programs are returned that are currently
     *  active.  In any status mode, active and inactive programs
     *  expired are returned.
     *
     *  @return a list of <code>Programs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.program.ProgramList getPrograms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the program list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of programs
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.program.ProgramList filterProgramsOnViews(org.osid.course.program.ProgramList list)
        throws org.osid.OperationFailedException {

        org.osid.course.program.ProgramList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.program.program.ActiveProgramFilterList(ret);
        }

        return (ret);
    }
}
