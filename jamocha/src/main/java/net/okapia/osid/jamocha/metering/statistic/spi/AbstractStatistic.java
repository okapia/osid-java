//
// AbstractStatistic.java
//
//     Defines a Statistic.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.statistic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Statistic</code>.
 */

public abstract class AbstractStatistic
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendium
    implements org.osid.metering.Statistic {

    private org.osid.metering.Meter meter;
    private org.osid.id.Id meteredObjectId;
    private org.osid.calendaring.DateTime start;
    private org.osid.calendaring.DateTime end;
    private java.math.BigDecimal sum;
    private java.math.BigDecimal mean;
    private java.math.BigDecimal median;
    private java.math.BigDecimal mode;
    private java.math.BigDecimal standardDeviation;
    private java.math.BigDecimal rms;
    private java.math.BigDecimal delta;
    private java.math.BigDecimal percentChange;
    private java.math.BigDecimal averageRate;
    private org.osid.calendaring.DateTimeResolution units;
    
    private final java.util.Collection<org.osid.metering.records.StatisticRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Meter </code> associated with 
     *  this reading. 
     *
     *  @return the <code> Id </code> of the <code> Meter </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMeterId() {
        return (this.meter.getId());
    }


    /**
     *  Gets the <code> Meter </code> associated with this reading. 
     *
     *  @return the <code> Meter </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.metering.Meter getMeter()
        throws org.osid.OperationFailedException {

        return (this.meter);
    }


    /**
     *  Sets the meter.
     *
     *  @param meter a meter
     *  @throws org.osid.NullArgumentException <code>meter</code> is
     *          <code>null</code>
     */

    protected void setMeter(org.osid.metering.Meter meter) {
        nullarg(meter, "meter");
        this.meter = meter;
        return;
    }


    /**
     *  Gets the metered object associated with this reading. 
     *
     *  @return the metered object <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMeteredObjectId() {
        return (this.meteredObjectId);
    }


    /**
     *  Sets the metered object Id.
     *
     *  @param objectId a metered object id
     *  @throws org.osid.NullArgumentException <code>objectId</code>
     *          is <code>null</code>
     */

    protected void setMeteredObjectId(org.osid.id.Id objectId) {
        nullarg(objectId, "metered object Id");
        this.meteredObjectId = objectId;
        return;
    }


    /**
     *  Gets the total. 
     *
     *  @return the sum 
     */

    @OSID @Override
    public java.math.BigDecimal getSum() {
        return (this.sum);
    }


    /**
     *  Sets the sum.
     *
     *  @param sum the sum
     *  @throws org.osid.NullArgumentException <code>sum</code> is
     *          <code>null</code>
     */

    protected void setSum(java.math.BigDecimal sum) {
        nullarg(sum, "sum");
        this.sum = sum;
        return;
    }


    /**
     *  Gets the mean. 
     *
     *  @return the mean 
     */

    @OSID @Override
    public java.math.BigDecimal getMean() {
        return (this.mean);
    }


    /**
     *  Sets the mean.
     *
     *  @param mean the mean
     *  @throws org.osid.NullArgumentException <code>mean</code> is
     *          <code>null</code>
     */

    protected void setMean(java.math.BigDecimal mean) {
        nullarg(mean, "mean");
        this.mean = mean;
        return;
    }


    /**
     *  Gets the median. 
     *
     *  @return the mean 
     */

    @OSID @Override
    public java.math.BigDecimal getMedian() {
        return (this.median);
    }


    /**
     *  Sets the median.
     *
     *  @param median the median
     *  @throws org.osid.NullArgumentException <code>median</code> is
     *          <code>null</code>
     */

    protected void setMedian(java.math.BigDecimal median) {
        nullarg(median, "median");
        this.median = median;
        return;
    }


    /**
     *  Gets the mode. 
     *
     *  @return the mode 
     */

    @OSID @Override
    public java.math.BigDecimal getMode() {
        return (this.mode);
    }


    /**
     *  Sets the mode.
     *
     *  @param mode the mode
     *  @throws org.osid.NullArgumentException <code>mode</code> is
     *          <code>null</code>
     */

    protected void setMode(java.math.BigDecimal mode) {
        nullarg(mode, "mode");
        this.mode = mode;
        return;
    }


    /**
     *  Gets the standard deviation. 
     *
     *  @return the standard deviation 
     */

    @OSID @Override
    public java.math.BigDecimal getStandardDeviation() {
        return (this.standardDeviation);
    }


    /**
     *  Sets the standard deviation.
     *
     *  @param standardDeviation the standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    protected void setStandardDeviation(java.math.BigDecimal standardDeviation) {
        nullarg(standardDeviation, "standard deviation");
        this.standardDeviation = standardDeviation;
        return;
    }


    /**
     *  Gets the root mean square. 
     *
     *  @return the rms 
     */

    @OSID @Override
    public java.math.BigDecimal getRMS() {
        return (this.rms);
    }


    /**
     *  Sets the rms.
     *
     *  @param rms the root mean square
     *  @throws org.osid.NullArgumentException <code>rms</code> is
     *          <code>null</code>
     */

    protected void setRMS(java.math.BigDecimal rms) {
        nullarg(rms, "root mean square");
        this.rms = rms;
        return;
    }


    /**
     *  Gets the difference between the end and start values. 
     *
     *  @return the delta 
     */

    @OSID @Override
    public java.math.BigDecimal getDelta() {
        return (this.delta);
    }


    /**
     *  Sets the delta.
     *
     *  @param delta the delta
     *  @throws org.osid.NullArgumentException <code>delta</code> is
     *          <code>null</code>
     */

    protected void setDelta(java.math.BigDecimal delta) {
        nullarg(delta, "delta");
        this.delta = delta;
        return;
    }


    /**
     *  Gets the percent change between the end and start values (e.g. 
     *  50.25%). 
     *
     *  @return the delta 
     */

    @OSID @Override
    public java.math.BigDecimal getPercentChange() {
        return (this.percentChange);
    }


    /**
     *  Sets the percent change.
     *
     *  @param percent the percent change
     *  @throws org.osid.NullArgumentException <code>percent</code> is
     *          <code>null</code>
     */

    protected void setPercentChange(java.math.BigDecimal percent) {
        nullarg(percent, "percent change");
        this.percentChange = percent;
        return;
    }


    /**
     *  Gets the average rate of change. 
     *
     *  @param  units the time units 
     *  @return the average rate 
     *  @throws org.osid.NullArgumentException <code> units </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getAverageRate(org.osid.calendaring.DateTimeResolution units) {
        return (calculateRate(this.averageRate, this.units, units));
    }


    /**
     *  Sets the average rate.
     *
     *  @param rate the average rate
     *  @param units the time units
     *  @throws org.osid.NullArgumentException <code>rate</code> or
     *          <code>units</code> is <code>null</code>
     */

    protected void setAverageRate(java.math.BigDecimal rate, org.osid.calendaring.DateTimeResolution units) {
        nullarg(rate, "rate");
        nullarg(units, "units");

        this.averageRate  = rate;
        this.units = units;

        return;
    }


    /**
     *  Tests if this statistic supports the given record
     *  <code>Type</code>.
     *
     *  @param  statisticRecordType a statistic record type 
     *  @return <code>true</code> if the statisticRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>statisticRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type statisticRecordType) {
        for (org.osid.metering.records.StatisticRecord record : this.records) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Statistic</code> record <code>Type</code>.
     *
     *  @param  statisticRecordType the statistic record type 
     *  @return the statistic record 
     *  @throws org.osid.NullArgumentException
     *          <code>statisticRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(statisticRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.StatisticRecord getStatisticRecord(org.osid.type.Type statisticRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.StatisticRecord record : this.records) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(statisticRecordType + " is not supported");
    }


    /**
     *  Adds a record to this statistic. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param statisticRecord the statistic record
     *  @param statisticRecordType statistic record type
     *  @throws org.osid.NullArgumentException
     *          <code>statisticRecord</code> or
     *          <code>statisticRecordTypestatistic</code> is
     *          <code>null</code>
     */
            
    protected void addStatisticRecord(org.osid.metering.records.StatisticRecord statisticRecord, 
                                     org.osid.type.Type statisticRecordType) {

        addRecordType(statisticRecordType);
        this.records.add(statisticRecord);
        
        return;
    }


    protected static java.math.BigDecimal calculateRate(java.math.BigDecimal rate, 
                                                        org.osid.calendaring.DateTimeResolution source, 
                                                        org.osid.calendaring.DateTimeResolution target) {

        if (source == target) {
            return (rate);
        }

        return (rate);
        // .multiply(net.okapia.osid.torrefacto.util.DateTimeResolutionRatio.calculate(source, target)));
    }
}
