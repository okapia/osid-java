//
// AbstractRoomBatchProxyManager.java
//
//     An adapter for a RoomBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RoomBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRoomBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.room.batch.RoomBatchProxyManager>
    implements org.osid.room.batch.RoomBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterRoomBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRoomBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRoomBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRoomBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of rooms is available. 
     *
     *  @return <code> true </code> if a room bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomBatchAdmin() {
        return (getAdapteeManager().supportsRoomBatchAdmin());
    }


    /**
     *  Tests if bulk administration of floors is available. 
     *
     *  @return <code> true </code> if a floor bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorBatchAdmin() {
        return (getAdapteeManager().supportsFloorBatchAdmin());
    }


    /**
     *  Tests if bulk administration of buildings is available. 
     *
     *  @return <code> true </code> if a building bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingBatchAdmin() {
        return (getAdapteeManager().supportsBuildingBatchAdmin());
    }


    /**
     *  Tests if bulk administration of campuses is available. 
     *
     *  @return <code> true </code> if an campus bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusBatchAdmin() {
        return (getAdapteeManager().supportsCampusBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk room 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.RoomBatchAdminSession getRoomBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk room 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.RoomBatchAdminSession getRoomBatchAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomBatchAdminSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk room 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.FloorBatchAdminSession getFloorBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk floor 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.FloorBatchAdminSession getFloorBatchAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorBatchAdminSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk building 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.BuildingBatchAdminSession getBuildingBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk building 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.BuildingBatchAdminSession getBuildingBatchAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingBatchAdminSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk campus 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.CampusBatchAdminSession getCampusBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCampusBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
