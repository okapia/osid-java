//
// AbstractImmutableAssessmentTaken.java
//
//     Wraps a mutable AssessmentTaken to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmenttaken.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AssessmentTaken</code> to hide modifiers. This
 *  wrapper provides an immutized AssessmentTaken from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying assessmentTaken whose state changes are visible.
 */

public abstract class AbstractImmutableAssessmentTaken
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.assessment.AssessmentTaken {

    private final org.osid.assessment.AssessmentTaken assessmentTaken;


    /**
     *  Constructs a new <code>AbstractImmutableAssessmentTaken</code>.
     *
     *  @param assessmentTaken the assessment taken to immutablize
     *  @throws org.osid.NullArgumentException <code>assessmentTaken</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAssessmentTaken(org.osid.assessment.AssessmentTaken assessmentTaken) {
        super(assessmentTaken);
        this.assessmentTaken = assessmentTaken;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> AssessmentOffered. </code> 
     *
     *  @return the assessment offered <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentOfferedId() {
        return (this.assessmentTaken.getAssessmentOfferedId());
    }


    /**
     *  Gets the <code> AssessmentOffered. </code> 
     *
     *  @return the assessment offered 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOffered getAssessmentOffered()
        throws org.osid.OperationFailedException {

        return (this.assessmentTaken.getAssessmentOffered());
    }


    /**
     *  Gets the <code> Id </code> of the resource who took or is taking this 
     *  assessment. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTakerId() {
        return (this.assessmentTaken.getTakerId());
    }


    /**
     *  Gets the <code> Resource </code> taking this assessment. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getTaker()
        throws org.osid.OperationFailedException {

        return (this.assessmentTaken.getTaker());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> who took or is 
     *  taking the assessment. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTakingAgentId() {
        return (this.assessmentTaken.getTakingAgentId());
    }


    /**
     *  Gets the <code> Agent. </code> 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getTakingAgent()
        throws org.osid.OperationFailedException {

        return (this.assessmentTaken.getTakingAgent());
    }


    /**
     *  Tests if this assessment has begun. 
     *
     *  @return <code> true </code> if the assessment has begun, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasStarted() {
        return (this.assessmentTaken.hasStarted());
    }


    /**
     *  Gets the time this assessment was started. 
     *
     *  @return the start time 
     *  @throws org.osid.IllegalStateException <code> hasStarted() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getActualStartTime() {
        return (this.assessmentTaken.getActualStartTime());
    }


    /**
     *  Tests if this assessment has ended. 
     *
     *  @return <code> true </code> if the assessment has ended, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasEnded() {
        return (this.assessmentTaken.hasEnded());
    }


    /**
     *  Gets the time of this assessment was completed. 
     *
     *  @return the end time 
     *  @throws org.osid.IllegalStateException <code> hasEnded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCompletionTime() {
        return (this.assessmentTaken.getCompletionTime());
    }


    /**
     *  Gets the total time spent taking this assessment. 
     *
     *  @return the total time spent 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeSpent() {
        return (this.assessmentTaken.getTimeSpent());
    }


    /**
     *  Gets a completion percentage of the assessment. 
     *
     *  @return the percent complete (0-100) 
     */

    @OSID @Override
    public long getCompletion() {
        return (this.assessmentTaken.getCompletion());
    }


    /**
     *  Tests if a score is available for this assessment. 
     *
     *  @return <code> true </code> if a score is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isScored() {
        return (this.assessmentTaken.isScored());
    }


    /**
     *  Gets a score system <code> Id </code> for the assessment. 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> isScore() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScoreSystemId() {
        return (this.assessmentTaken.getScoreSystemId());
    }


    /**
     *  Gets a grade system for the score. 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> isScored() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getScoreSystem()
        throws org.osid.OperationFailedException {

        return (this.assessmentTaken.getScoreSystem());
    }


    /**
     *  Gets a score for the assessment. 
     *
     *  @return the score 
     *  @throws org.osid.IllegalStateException <code> isScored() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getScore() {
        return (this.assessmentTaken.getScore());
    }


    /**
     *  Tests if a grade is available for this assessment. 
     *
     *  @return <code> true </code> if a grade is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.assessmentTaken.isGraded());
    }


    /**
     *  Gets a grade <code> Id </code> for the assessment. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeId() {
        return (this.assessmentTaken.getGradeId());
    }


    /**
     *  Gets a grade for the assessment. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getGrade()
        throws org.osid.OperationFailedException {

        return (this.assessmentTaken.getGrade());
    }


    /**
     *  Gets any overall feedback available for this assessment by the grader. 
     *
     *  @return feedback 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getFeedback() {
        return (this.assessmentTaken.getFeedback());
    }


    /**
     *  Tests if a rubric assessment taken is associated with this
     *  assessment.
     *
     *  @return <code> true </code> if a rubric is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasRubric() {
        return (this.assessmentTaken.hasRubric());
    }


    /**
     *  Gets the <code> Id </code> of the rubric. 
     *
     *  @return an assessment taken <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRubricId() {

        return (this.assessmentTaken.getRubricId());
    }


    /**
     *  Gets the rubric. 
     *
     *  @return the assessment taken
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTaken getRubric() 
        throws org.osid.OperationFailedException {

        return (this.assessmentTaken.getRubric());
    }


    /**
     *  Gets the assessment taken record corresponding to the given <code> 
     *  AssessmentTaken </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> assessmentTakenRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(assessmentTakenRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  assessmentTakenRecordType an assessment taken record type 
     *  @return the assessment taken record 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentTakenRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(assessmentTakenRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenRecord getAssessmentTakenRecord(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException {

        return (this.assessmentTaken.getAssessmentTakenRecord(assessmentTakenRecordType));
    }
}

