//
// AbstractImmutableParameter.java
//
//     Wraps a mutable Parameter to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.parameter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Parameter</code> to hide modifiers. This
 *  wrapper provides an immutized Parameter from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying parameter whose state changes are visible.
 */

public abstract class AbstractImmutableParameter
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.configuration.Parameter {

    private final org.osid.configuration.Parameter parameter;


    /**
     *  Constructs a new <code>AbstractImmutableParameter</code>.
     *
     *  @param parameter the parameter to immutablize
     *  @throws org.osid.NullArgumentException <code>parameter</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableParameter(org.osid.configuration.Parameter parameter) {
        super(parameter);
        this.parameter = parameter;
        return;
    }


    /**
     *  Gets the syntax for the values of this parameter. 
     *
     *  @return the syntax of the values 
     */

    @OSID @Override
    public org.osid.Syntax getValueSyntax() {
        return (this.parameter.getValueSyntax());
    }


    /**
     *  Gets the type of the value if the syntax is a coordinate. 
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          COORDINATE </code> 
     */

    @OSID @Override
    public org.osid.type.Type getValueCoordinateType() {
        return (this.parameter.getValueCoordinateType());
    }


    /**
     *  Tests if the coordinate supports the given coordinate <code> Type. 
     *  </code> 
     *
     *  @param  coordinateType a coordinate type 
     *  @return <code> true </code> if the coordinate values associated with 
     *          this parameter implement the given coordinate <code> Type, 
     *          </code> <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueCoordinateType(org.osid.type.Type coordinateType) {
        return (this.parameter.implementsValueCoordinateType(coordinateType));
    }


    /**
     *  Gets the type of the value if the syntax is a heading. 
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          HEADING </code> 
     */

    @OSID @Override
    public org.osid.type.Type getValueHeadingType() {
        return (this.parameter.getValueHeadingType());
    }


    /**
     *  Tests if the heading supports the given heading <code> Type. 
     *  </code> 
     *
     *  @param  headingType a heading type 
     *  @return <code> true </code> if the heading values associated with 
     *          this parameter implement the given heading <code> Type, 
     *          </code> <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          HEADING </code> 
     *  @throws org.osid.NullArgumentException <code> headingType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueHeadingType(org.osid.type.Type headingType) {
        return (this.parameter.implementsValueHeadingType(headingType));
    }


    /**
     *  Gets the type of the value if the syntax is a spatial unit. 
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.type.Type getValueSpatialUnitRecordType() {
        return (this.parameter.getValueSpatialUnitRecordType());
    }


    /**
     *  Tests if the spatial unit supports the given record <code> Type. 
     *  </code> 
     *
     *  @param  spatialUnitRecordType a spatial unit record type 
     *  @return <code> true </code> if the spatial unit values associated with 
     *          this parameter implement the given record <code> Type, </code> 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          SPATIALUNIT </code> 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordTYpe 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        return (this.parameter.implementsValueSpatialUnitRecordType(spatialUnitRecordType));
    }


    /**
     *  Gets the type of the value if the syntax is an object. 
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          OBJECT </code> 
     */

    @OSID @Override
    public org.osid.type.Type getValueObjectType() {
        return (this.parameter.getValueObjectType());
    }


    /**
     *  Tests if the object supports the given <code> Type. </code> This 
     *  method should be checked before retrieving the object value. 
     *
     *  @param  valueType a type 
     *  @return <code> true </code> if the obect values associated with this 
     *          parameter implement the given <code> Type, </code> <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          OBJECT </code> 
     *  @throws org.osid.NullArgumentException <code> valueType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueObjectType(org.osid.type.Type valueType) {
        return (this.parameter.implementsValueObjectType(valueType));
    }


    /**
     *  Gets the type of the value if the syntax is a version. 
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          VERSION </code> 
     */

    @OSID @Override
    public org.osid.type.Type getValueVersionScheme() {
        return (this.parameter.getValueVersionScheme());
    }


    /**
     *  Tests if the version supports the given version <code> Type. </code> 
     *
     *  @param  versionType a version type 
     *  @return <code> true </code> if the version values associated with this 
     *          parameter implement the given version <code> Type, </code> 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          VERSION </code> 
     *  @throws org.osid.NullArgumentException <code> versionType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueVersionScheme(org.osid.type.Type versionType) {
        return (this.parameter.implementsValueVersionScheme(versionType));
    }


    /**
     *  Tests if if the values assigned to this parameter will be shuffled or 
     *  values are sorted by index. 
     *
     *  @return <code> true </code> if the values are shuffled, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean areValuesShuffled() {
        return (this.parameter.areValuesShuffled());
    }


    /**
     *  Gets the parameter record corresponding to the given <code> Parameter 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  parameterRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(parameterRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  parameterRecordType the type of parameter record to retrieve 
     *  @return the parameter record 
     *  @throws org.osid.NullArgumentException <code> parameterRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(parameterRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.records.ParameterRecord getParameterRecord(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException {

        return (this.parameter.getParameterRecord(parameterRecordType));
    }
}

