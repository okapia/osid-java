//
// CommentElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.comment.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CommentElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the CommentElement Id.
     *
     *  @return the comment element Id
     */

    public static org.osid.id.Id getCommentEntityId() {
        return (makeEntityId("osid.commenting.Comment"));
    }


    /**
     *  Gets the ReferenceId element Id.
     *
     *  @return the ReferenceId element Id
     */

    public static org.osid.id.Id getReferenceId() {
        return (makeElementId("osid.commenting.comment.ReferenceId"));
    }


    /**
     *  Gets the CommentorId element Id.
     *
     *  @return the CommentorId element Id
     */

    public static org.osid.id.Id getCommentorId() {
        return (makeElementId("osid.commenting.comment.CommentorId"));
    }


    /**
     *  Gets the Commentor element Id.
     *
     *  @return the Commentor element Id
     */

    public static org.osid.id.Id getCommentor() {
        return (makeElementId("osid.commenting.comment.Commentor"));
    }


    /**
     *  Gets the CommentingAgentId element Id.
     *
     *  @return the CommentingAgentId element Id
     */

    public static org.osid.id.Id getCommentingAgentId() {
        return (makeElementId("osid.commenting.comment.CommentingAgentId"));
    }


    /**
     *  Gets the CommentingAgent element Id.
     *
     *  @return the CommentingAgent element Id
     */

    public static org.osid.id.Id getCommentingAgent() {
        return (makeElementId("osid.commenting.comment.CommentingAgent"));
    }


    /**
     *  Gets the Text element Id.
     *
     *  @return the Text element Id
     */

    public static org.osid.id.Id getText() {
        return (makeElementId("osid.commenting.comment.Text"));
    }


    /**
     *  Gets the RatingId element Id.
     *
     *  @return the RatingId element Id
     */

    public static org.osid.id.Id getRatingId() {
        return (makeElementId("osid.commenting.comment.RatingId"));
    }


    /**
     *  Gets the Rating element Id.
     *
     *  @return the Rating element Id
     */

    public static org.osid.id.Id getRating() {
        return (makeElementId("osid.commenting.comment.Rating"));
    }


    /**
     *  Gets the BookId element Id.
     *
     *  @return the BookId element Id
     */

    public static org.osid.id.Id getBookId() {
        return (makeQueryElementId("osid.commenting.comment.BookId"));
    }


    /**
     *  Gets the Book element Id.
     *
     *  @return the Book element Id
     */

    public static org.osid.id.Id getBook() {
        return (makeQueryElementId("osid.commenting.comment.Book"));
    }


    /**
     *  Gets the Reference element Id.
     *
     *  @return the Reference element Id
     */

    public static org.osid.id.Id getReference() {
        return (makeSearchOrderElementId("osid.commenting.comment.Reference"));
    }
}
