//
// AbstractAssemblyCanonicalUnitProcessorEnablerQuery.java
//
//     A CanonicalUnitProcessorEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.offering.rules.canonicalunitprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CanonicalUnitProcessorEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyCanonicalUnitProcessorEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery,
               org.osid.offering.rules.CanonicalUnitProcessorEnablerQueryInspector,
               org.osid.offering.rules.CanonicalUnitProcessorEnablerSearchOrder {

    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCanonicalUnitProcessorEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCanonicalUnitProcessorEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the canonical unit processor. 
     *
     *  @param  canonicalUnitProcessorId the canonical unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitProcessorId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCanonicalUnitProcessorId(org.osid.id.Id canonicalUnitProcessorId, 
                                                   boolean match) {
        getAssembler().addIdTerm(getRuledCanonicalUnitProcessorIdColumn(), canonicalUnitProcessorId, match);
        return;
    }


    /**
     *  Clears the canonical unit processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitProcessorIdTerms() {
        getAssembler().clearTerms(getRuledCanonicalUnitProcessorIdColumn());
        return;
    }


    /**
     *  Gets the canonical unit processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCanonicalUnitProcessorIdTerms() {
        return (getAssembler().getIdTerms(getRuledCanonicalUnitProcessorIdColumn()));
    }


    /**
     *  Gets the RuledCanonicalUnitProcessorId column name.
     *
     * @return the column name
     */

    protected String getRuledCanonicalUnitProcessorIdColumn() {
        return ("ruled_canonical_unit_processor_id");
    }


    /**
     *  Tests if a <code> CanonicalUnitProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a canonical unit processor query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCanonicalUnitProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a canonical unit processor. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the canonical unit processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCanonicalUnitProcessorQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQuery getRuledCanonicalUnitProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCanonicalUnitProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any canonical unit processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any canonical 
     *          unit processor, <code> false </code> to match enablers mapped 
     *          to no canonical unit processors 
     */

    @OSID @Override
    public void matchAnyRuledCanonicalUnitProcessor(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledCanonicalUnitProcessorColumn(), match);
        return;
    }


    /**
     *  Clears the canonical unit processor query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitProcessorTerms() {
        getAssembler().clearTerms(getRuledCanonicalUnitProcessorColumn());
        return;
    }


    /**
     *  Gets the canonical unit processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQueryInspector[] getRuledCanonicalUnitProcessorTerms() {
        return (new org.osid.offering.rules.CanonicalUnitProcessorQueryInspector[0]);
    }


    /**
     *  Gets the RuledCanonicalUnitProcessor column name.
     *
     * @return the column name
     */

    protected String getRuledCanonicalUnitProcessorColumn() {
        return ("ruled_canonical_unit_processor");
    }


    /**
     *  Matches enablers mapped to the catalogue. 
     *
     *  @param  catalogueId the catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        getAssembler().addIdTerm(getCatalogueIdColumn(), catalogueId, match);
        return;
    }


    /**
     *  Clears the catalogue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        getAssembler().clearTerms(getCatalogueIdColumn());
        return;
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (getAssembler().getIdTerms(getCatalogueIdColumn()));
    }


    /**
     *  Gets the CatalogueId column name.
     *
     * @return the column name
     */

    protected String getCatalogueIdColumn() {
        return ("catalogue_id");
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears the catalogue query terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        getAssembler().clearTerms(getCatalogueColumn());
        return;
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the Catalogue column name.
     *
     * @return the column name
     */

    protected String getCatalogueColumn() {
        return ("catalogue");
    }


    /**
     *  Tests if this canonicalUnitProcessorEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  canonicalUnitProcessorEnablerRecordType a canonical unit processor enabler record type 
     *  @return <code>true</code> if the canonicalUnitProcessorEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type canonicalUnitProcessorEnablerRecordType) {
        for (org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(canonicalUnitProcessorEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  canonicalUnitProcessorEnablerRecordType the canonical unit processor enabler record type 
     *  @return the canonical unit processor enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryRecord getCanonicalUnitProcessorEnablerQueryRecord(org.osid.type.Type canonicalUnitProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(canonicalUnitProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  canonicalUnitProcessorEnablerRecordType the canonical unit processor enabler record type 
     *  @return the canonical unit processor enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryInspectorRecord getCanonicalUnitProcessorEnablerQueryInspectorRecord(org.osid.type.Type canonicalUnitProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(canonicalUnitProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param canonicalUnitProcessorEnablerRecordType the canonical unit processor enabler record type
     *  @return the canonical unit processor enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorEnablerSearchOrderRecord getCanonicalUnitProcessorEnablerSearchOrderRecord(org.osid.type.Type canonicalUnitProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(canonicalUnitProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this canonical unit processor enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param canonicalUnitProcessorEnablerQueryRecord the canonical unit processor enabler query record
     *  @param canonicalUnitProcessorEnablerQueryInspectorRecord the canonical unit processor enabler query inspector
     *         record
     *  @param canonicalUnitProcessorEnablerSearchOrderRecord the canonical unit processor enabler search order record
     *  @param canonicalUnitProcessorEnablerRecordType canonical unit processor enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerQueryRecord</code>,
     *          <code>canonicalUnitProcessorEnablerQueryInspectorRecord</code>,
     *          <code>canonicalUnitProcessorEnablerSearchOrderRecord</code> or
     *          <code>canonicalUnitProcessorEnablerRecordTypecanonicalUnitProcessorEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addCanonicalUnitProcessorEnablerRecords(org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryRecord canonicalUnitProcessorEnablerQueryRecord, 
                                      org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryInspectorRecord canonicalUnitProcessorEnablerQueryInspectorRecord, 
                                      org.osid.offering.rules.records.CanonicalUnitProcessorEnablerSearchOrderRecord canonicalUnitProcessorEnablerSearchOrderRecord, 
                                      org.osid.type.Type canonicalUnitProcessorEnablerRecordType) {

        addRecordType(canonicalUnitProcessorEnablerRecordType);

        nullarg(canonicalUnitProcessorEnablerQueryRecord, "canonical unit processor enabler query record");
        nullarg(canonicalUnitProcessorEnablerQueryInspectorRecord, "canonical unit processor enabler query inspector record");
        nullarg(canonicalUnitProcessorEnablerSearchOrderRecord, "canonical unit processor enabler search odrer record");

        this.queryRecords.add(canonicalUnitProcessorEnablerQueryRecord);
        this.queryInspectorRecords.add(canonicalUnitProcessorEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(canonicalUnitProcessorEnablerSearchOrderRecord);
        
        return;
    }
}
