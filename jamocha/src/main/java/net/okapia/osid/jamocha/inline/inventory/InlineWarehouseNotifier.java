//
// InlineWarehouseNotifier.java
//
//     A callback interface for performing warehouse
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.inventory.spi;


/**
 *  A callback interface for performing warehouse
 *  notifications.
 */

public interface InlineWarehouseNotifier {



    /**
     *  Notifies the creation of a new warehouse.
     *
     *  @param warehouseId the {@code Id} of the new 
     *         warehouse
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newWarehouse(org.osid.id.Id warehouseId);


    /**
     *  Notifies the change of an updated warehouse.
     *
     *  @param warehouseId the {@code Id} of the changed
     *         warehouse
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedWarehouse(org.osid.id.Id warehouseId);


    /**
     *  Notifies the deletion of an warehouse.
     *
     *  @param warehouseId the {@code Id} of the deleted
     *         warehouse
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedWarehouse(org.osid.id.Id warehouseId);

}
