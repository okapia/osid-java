//
// AbstractAssemblyOsidRuleQuery.java
//
//     An OsidRuleQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OsidRuleQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidRuleQuery
    extends AbstractAssemblyOperableOsidObjectQuery
    implements org.osid.OsidRuleQuery,
               org.osid.OsidRuleQueryInspector,
               org.osid.OsidRuleSearchOrder {

    
    /** 
     *  Constructs a new
     *  <code>AbstractAssemblyOsidRuleQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidRuleQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }

    
    /**
     *  Match the <code> Id </code> of the rule. 
     *
     *  @param  ruleId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ruleId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuleId(org.osid.id.Id ruleId, boolean match) {
        getAssembler().addIdTerm(getRuleIdColumn(), ruleId, match);
        return;
    }


    /**
     *  Clears all rule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRuleIdTerms() {
        getAssembler().clearTerms(getRuleIdColumn());
        return;
    }


    /**
     *  Gets the rule <code> Id </code> query terms. 
     *
     *  @return the rule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuleIdTerms() {
        return (getAssembler().getIdTerms(getRuleIdColumn()));
    }


    /**
     *  Gets the column name for the rule Id field.
     *
     *  @return the column name
     */

    protected String getRuleIdColumn() {
        return ("rule_id");
    }    


    /**
     *  Tests if a <code> RuleQuery </code> for the rule is available. 
     *
     *  @return <code> true </code> if a rule query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleQuery() {
        return (false);
    }


    /**
     *  Gets the query for the rule. Each retrieval performs a boolean <code> 
     *  OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the rule query 
     *  @throws org.osid.UnimplementedException <code> supportsRuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleQuery getRuleQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsRuleQuery() is false");
    }


    /**
     *  Match any associated rule. 
     *
     *  @param  match <code> true </code> to match any rule, <code> false 
     *          </code> to match no rules 
     */

    @OSID @Override
    public void matchAnyRule(boolean match) {
        getAssembler().addIdWildcardTerm(getRuleColumn(), match);
        return;
    }


    /**
     *  Clears all rule terms. 
     */

    @OSID @Override
    public void clearRuleTerms() {
        getAssembler().clearTerms(getRuleColumn());
        return;
    }


    /**
     *  Gets the rule query terms. 
     *
     *  @return the rule query terms 
     */

    @OSID @Override
    public org.osid.rules.RuleQueryInspector[] getRuleTerms() {
        return (new org.osid.rules.RuleQueryInspector[0]);
    }


    /**
     *  Specifies a preference for ordering the results by the associated 
     *  rule. The element of the rule to order is not specified but may be 
     *  managed through a <code> RuleSearchOrder. </code> 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRule(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRuleColumn(), style);
        return;
    }


    /**
     *  Tests if a <code> RuleSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a rule search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a rule. 
     *
     *  @return the rule search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSearchOrder getRuleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRuleSearchOrder() is false");
    }


    /**
     *  Gets the column name for the rule field.
     *
     *  @return the column name
     */

    protected String getRuleColumn() {
        return ("rule");
    }    
}
