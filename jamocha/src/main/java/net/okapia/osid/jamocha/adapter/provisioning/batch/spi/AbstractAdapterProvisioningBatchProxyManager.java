//
// AbstractProvisioningBatchProxyManager.java
//
//     An adapter for a ProvisioningBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ProvisioningBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterProvisioningBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.provisioning.batch.ProvisioningBatchProxyManager>
    implements org.osid.provisioning.batch.ProvisioningBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterProvisioningBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterProvisioningBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterProvisioningBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterProvisioningBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of provisions is available. 
     *
     *  @return <code> true </code> if a provision bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionBatchAdmin() {
        return (getAdapteeManager().supportsProvisionBatchAdmin());
    }


    /**
     *  Tests if bulk administration of queues is available. 
     *
     *  @return <code> true </code> if a queue bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueBatchAdmin() {
        return (getAdapteeManager().supportsQueueBatchAdmin());
    }


    /**
     *  Tests if bulk administration of requests is available. 
     *
     *  @return <code> true </code> if a request bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestBatchAdmin() {
        return (getAdapteeManager().supportsRequestBatchAdmin());
    }


    /**
     *  Tests if bulk administration of requests is available. 
     *
     *  @return <code> true </code> if a request transaction bulk 
     *          administrative service is available, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionBatchAdmin() {
        return (getAdapteeManager().supportsRequestTransactionBatchAdmin());
    }


    /**
     *  Tests if bulk administration of pools is available. 
     *
     *  @return <code> true </code> if a pool bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolBatchAdmin() {
        return (getAdapteeManager().supportsPoolBatchAdmin());
    }


    /**
     *  Tests if bulk administration of provisionables is available. 
     *
     *  @return <code> true </code> if a provisionable bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableBatchAdmin() {
        return (getAdapteeManager().supportsProvisionableBatchAdmin());
    }


    /**
     *  Tests if bulk administration of brokers is available. 
     *
     *  @return <code> true </code> if a broker bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerBatchAdmin() {
        return (getAdapteeManager().supportsBrokerBatchAdmin());
    }


    /**
     *  Tests if bulk administration of distributors is available. 
     *
     *  @return <code> true </code> if a distributor bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorBatchAdmin() {
        return (getAdapteeManager().supportsDistributorBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk provision 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionBatchAdminSession getProvisionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk provision 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionBatchAdminSession getProvisionBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionBatchAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.QueueBatchAdminSession getQueueBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.QueueBatchAdminSession getQueueBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueBatchAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestBatchAdminSession getRequestBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestBatchAdminSession getRequestBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestBatchAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  transaction administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestTransactionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestTransactionBatchAdminSession getRequestTransactionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestTransactionBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  transaction administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestTransactionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestTransactionBatchAdminSession getRequestTransactionBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestTransactionBatchAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk pool 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.PoolBatchAdminSession getPoolBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk pool 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.PoolBatchAdminSession getPoolBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolBatchAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  provisionable administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionableBatchAdminSession getProvisionableBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  provisionable administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionableBatchAdminSession getProvisionableBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableBatchAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk broker 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.BrokerBatchAdminSession getBrokerBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk broker 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.BrokerBatchAdminSession getBrokerBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerBatchAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  distributor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DistributorBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.DistributorBatchAdminSession getDistributorBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDistributorBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
