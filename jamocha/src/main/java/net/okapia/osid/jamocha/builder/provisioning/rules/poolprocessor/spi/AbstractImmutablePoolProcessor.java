//
// AbstractImmutablePoolProcessor.java
//
//     Wraps a mutable PoolProcessor to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.poolprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>PoolProcessor</code> to hide modifiers. This
 *  wrapper provides an immutized PoolProcessor from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying poolProcessor whose state changes are visible.
 */

public abstract class AbstractImmutablePoolProcessor
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidProcessor
    implements org.osid.provisioning.rules.PoolProcessor {

    private final org.osid.provisioning.rules.PoolProcessor poolProcessor;


    /**
     *  Constructs a new <code>AbstractImmutablePoolProcessor</code>.
     *
     *  @param poolProcessor the pool processor to immutablize
     *  @throws org.osid.NullArgumentException <code>poolProcessor</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePoolProcessor(org.osid.provisioning.rules.PoolProcessor poolProcessor) {
        super(poolProcessor);
        this.poolProcessor = poolProcessor;
        return;
    }


    /**
     *  Tests if allocations balance the usage by preferring the least used 
     *  provisionables in the pool. 
     *
     *  @return <code> true </code> if the least used provisionables are 
     *          preferred, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean allocatesByLeastUse() {
        return (this.poolProcessor.allocatesByLeastUse());
    }


    /**
     *  Tests if allocations prefer the most used provisionables in the pool. 
     *
     *  @return <code> true </code> if the most used provisionables are 
     *          preferred, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean allocatesByMostUse() {
        return (this.poolProcessor.allocatesByMostUse());
    }


    /**
     *  Tests if allocations prefer the cheapest provisionables. 
     *
     *  @return <code> true </code> if the cheapest provisionables are 
     *          preferred, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean allocatesByLeastCost() {
        return (this.poolProcessor.allocatesByLeastCost());
    }


    /**
     *  Tests if allocations prefer the most expensive provisionables. 
     *
     *  @return <code> true </code> if the most expensive provisionables are 
     *          prefsrred, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean allocatesByMostCost() {
        return (this.poolProcessor.allocatesByMostCost());
    }


    /**
     *  Gets the pool processor record corresponding to the given <code> 
     *  PoolProcessor </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  poolProcessorRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(poolProcessorRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  poolProcessorRecordType the type of pool processor record to 
     *          retrieve 
     *  @return the pool processor record 
     *  @throws org.osid.NullArgumentException <code> poolProcessorRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(poolProcessorRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorRecord getPoolProcessorRecord(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException {

        return (this.poolProcessor.getPoolProcessorRecord(poolProcessorRecordType));
    }
}

