//
// AbstractAssemblyGradeSystemQuery.java
//
//     A GradeSystemQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.grading.gradesystem.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A GradeSystemQuery that stores terms.
 */

public abstract class AbstractAssemblyGradeSystemQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.grading.GradeSystemQuery,
               org.osid.grading.GradeSystemQueryInspector,
               org.osid.grading.GradeSystemSearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradeSystemQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradeSystemQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradeSystemSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyGradeSystemQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyGradeSystemQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches grade systems based on grades. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     */

    @OSID @Override
    public void matchBasedOnGrades(boolean match) {
        getAssembler().addBooleanTerm(getBasedOnGradesColumn(), match);
        return;
    }


    /**
     *  Clears the grade <code> based </code> terms. 
     */

    @OSID @Override
    public void clearBasedOnGradesTerms() {
        getAssembler().clearTerms(getBasedOnGradesColumn());
        return;
    }


    /**
     *  Gets the grade-based systems terms. 
     *
     *  @return the grade-based systems terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getBasedOnGradesTerms() {
        return (getAssembler().getBooleanTerms(getBasedOnGradesColumn()));
    }


    /**
     *  Orders the results by systems based on grades. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBasedOnGrades(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBasedOnGradesColumn(), style);
        return;
    }


    /**
     *  Gets the BasedOnGrades column name.
     *
     * @return the column name
     */

    protected String getBasedOnGradesColumn() {
        return ("based_on_grades");
    }


    /**
     *  Sets the grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getGradeIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        getAssembler().clearTerms(getGradeIdColumn());
        return;
    }


    /**
     *  Gets the grade <code> Id </code> terms. 
     *
     *  @return the grade <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (getAssembler().getIdTerms(getGradeIdColumn()));
    }


    /**
     *  Gets the GradeId column name.
     *
     * @return the column name
     */

    protected String getGradeIdColumn() {
        return ("grade_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available for querying grades. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches grade systems with any grade. 
     *
     *  @param  match <code> true </code> to match grade systems with any 
     *          grade, <code> false </code> to match systems with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeColumn(), match);
        return;
    }


    /**
     *  Clears the grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        getAssembler().clearTerms(getGradeColumn());
        return;
    }


    /**
     *  Gets the grade terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the Grade column name.
     *
     * @return the column name
     */

    protected String getGradeColumn() {
        return ("grade");
    }


    /**
     *  Matches grade systems whose low end score falls in the specified range 
     *  inclusive. 
     *
     *  @param  start low end of range 
     *  @param  end high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLowestNumericScore(java.math.BigDecimal start, 
                                        java.math.BigDecimal end, 
                                        boolean match) {
        getAssembler().addDecimalRangeTerm(getLowestNumericScoreColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the lowest numeric score terms. 
     */

    @OSID @Override
    public void clearLowestNumericScoreTerms() {
        getAssembler().clearTerms(getLowestNumericScoreColumn());
        return;
    }


    /**
     *  Gets the lowest numeric score terms. 
     *
     *  @return the lowest numeric score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getLowestNumericScoreTerms() {
        return (getAssembler().getDecimalRangeTerms(getLowestNumericScoreColumn()));
    }


    /**
     *  Orders the results by lowest score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLowestNumericScore(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLowestNumericScoreColumn(), style);
        return;
    }


    /**
     *  Gets the LowestNumericScore column name.
     *
     * @return the column name
     */

    protected String getLowestNumericScoreColumn() {
        return ("lowest_numeric_score");
    }


    /**
     *  Matches grade systems numeric score increment is between the specified 
     *  range inclusive. 
     *
     *  @param  start low end of range 
     *  @param  end high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchNumericScoreIncrement(java.math.BigDecimal start, 
                                           java.math.BigDecimal end, 
                                           boolean match) {
        getAssembler().addDecimalRangeTerm(getNumericScoreIncrementColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the numeric score increment terms. 
     */

    @OSID @Override
    public void clearNumericScoreIncrementTerms() {
        getAssembler().clearTerms(getNumericScoreIncrementColumn());
        return;
    }


    /**
     *  Gets the numeric score increment terms. 
     *
     *  @return the numeric score increment terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getNumericScoreIncrementTerms() {
        return (getAssembler().getDecimalRangeTerms(getNumericScoreIncrementColumn()));
    }


    /**
     *  Orders the results by score increment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumericScoreIncrement(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNumericScoreIncrementColumn(), style);
        return;
    }


    /**
     *  Gets the NumericScoreIncrement column name.
     *
     * @return the column name
     */

    protected String getNumericScoreIncrementColumn() {
        return ("numeric_score_increment");
    }


    /**
     *  Matches grade systems whose high end score falls in the specified 
     *  range inclusive. 
     *
     *  @param  start low end of range 
     *  @param  end high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchHighestNumericScore(java.math.BigDecimal start, 
                                         java.math.BigDecimal end, 
                                         boolean match) {
        getAssembler().addDecimalRangeTerm(getHighestNumericScoreColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the highest numeric score terms. 
     */

    @OSID @Override
    public void clearHighestNumericScoreTerms() {
        getAssembler().clearTerms(getHighestNumericScoreColumn());
        return;
    }


    /**
     *  Gets the highest numeric score terms. 
     *
     *  @return the highest numeric score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getHighestNumericScoreTerms() {
        return (getAssembler().getDecimalRangeTerms(getHighestNumericScoreColumn()));
    }


    /**
     *  Orders the results by highest score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByHighestNumericScore(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getHighestNumericScoreColumn(), style);
        return;
    }


    /**
     *  Gets the HighestNumericScore column name.
     *
     * @return the column name
     */

    protected String getHighestNumericScoreColumn() {
        return ("highest_numeric_score");
    }


    /**
     *  Sets the gradebook column <code> Id </code> for this query. 
     *
     *  @param  gradebookColumnId a gradebook column <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookColumnId(org.osid.id.Id gradebookColumnId, 
                                       boolean match) {
        getAssembler().addIdTerm(getGradebookColumnIdColumn(), gradebookColumnId, match);
        return;
    }


    /**
     *  Clears the gradebook column <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookColumnIdTerms() {
        getAssembler().clearTerms(getGradebookColumnIdColumn());
        return;
    }


    /**
     *  Gets the gradebook column <code> Id </code> terms. 
     *
     *  @return the gradebook column <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookColumnIdTerms() {
        return (getAssembler().getIdTerms(getGradebookColumnIdColumn()));
    }


    /**
     *  Gets the GradebookColumnId column name.
     *
     * @return the column name
     */

    protected String getGradebookColumnIdColumn() {
        return ("gradebook_column_id");
    }


    /**
     *  Tests if a <code> GradebookColumnQuery </code> is available. 
     *
     *  @return <code> true </code> if a gradebook column query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook column. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the gradebook column query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuery getGradebookColumnQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnQuery() is false");
    }


    /**
     *  Matches grade systems assigned to any gradebook column. 
     *
     *  @param  match <code> true </code> to match grade systems mapped to any 
     *          column, <code> false </code> to match systems mapped to no 
     *          columns 
     */

    @OSID @Override
    public void matchAnyGradebookColumn(boolean match) {
        getAssembler().addIdWildcardTerm(getGradebookColumnColumn(), match);
        return;
    }


    /**
     *  Clears the gradebook column terms. 
     */

    @OSID @Override
    public void clearGradebookColumnTerms() {
        getAssembler().clearTerms(getGradebookColumnColumn());
        return;
    }


    /**
     *  Gets the gradebook column terms. 
     *
     *  @return the gradebook column terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQueryInspector[] getGradebookColumnTerms() {
        return (new org.osid.grading.GradebookColumnQueryInspector[0]);
    }


    /**
     *  Gets the GradebookColumn column name.
     *
     * @return the column name
     */

    protected String getGradebookColumnColumn() {
        return ("gradebook_column");
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookId(org.osid.id.Id gradebookId, boolean match) {
        getAssembler().addIdTerm(getGradebookIdColumn(), gradebookId, match);
        return;
    }


    /**
     *  Clears the gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookIdTerms() {
        getAssembler().clearTerms(getGradebookIdColumn());
        return;
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (getAssembler().getIdTerms(getGradebookIdColumn()));
    }


    /**
     *  Gets the GradebookId column name.
     *
     * @return the column name
     */

    protected String getGradebookIdColumn() {
        return ("gradebook_id");
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available . 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookQuery() is false");
    }


    /**
     *  Clears the gradebook terms. 
     */

    @OSID @Override
    public void clearGradebookTerms() {
        getAssembler().clearTerms(getGradebookColumn());
        return;
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }


    /**
     *  Gets the Gradebook column name.
     *
     * @return the column name
     */

    protected String getGradebookColumn() {
        return ("gradebook");
    }


    /**
     *  Tests if this gradeSystem supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradeSystemRecordType a grade system record type 
     *  @return <code>true</code> if the gradeSystemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeSystemRecordType) {
        for (org.osid.grading.records.GradeSystemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  gradeSystemRecordType the grade system record type 
     *  @return the grade system query record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeSystemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemQueryRecord getGradeSystemQueryRecord(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeSystemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeSystemRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  gradeSystemRecordType the grade system record type 
     *  @return the grade system query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeSystemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemQueryInspectorRecord getGradeSystemQueryInspectorRecord(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeSystemQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeSystemRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param gradeSystemRecordType the grade system record type
     *  @return the grade system search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeSystemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemSearchOrderRecord getGradeSystemSearchOrderRecord(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeSystemSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeSystemRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this grade system. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeSystemQueryRecord the grade system query record
     *  @param gradeSystemQueryInspectorRecord the grade system query inspector
     *         record
     *  @param gradeSystemSearchOrderRecord the grade system search order record
     *  @param gradeSystemRecordType grade system record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemQueryRecord</code>,
     *          <code>gradeSystemQueryInspectorRecord</code>,
     *          <code>gradeSystemSearchOrderRecord</code> or
     *          <code>gradeSystemRecordTypegradeSystem</code> is
     *          <code>null</code>
     */
            
    protected void addGradeSystemRecords(org.osid.grading.records.GradeSystemQueryRecord gradeSystemQueryRecord, 
                                      org.osid.grading.records.GradeSystemQueryInspectorRecord gradeSystemQueryInspectorRecord, 
                                      org.osid.grading.records.GradeSystemSearchOrderRecord gradeSystemSearchOrderRecord, 
                                      org.osid.type.Type gradeSystemRecordType) {

        addRecordType(gradeSystemRecordType);

        nullarg(gradeSystemQueryRecord, "grade system query record");
        nullarg(gradeSystemQueryInspectorRecord, "grade system query inspector record");
        nullarg(gradeSystemSearchOrderRecord, "grade system search odrer record");

        this.queryRecords.add(gradeSystemQueryRecord);
        this.queryInspectorRecords.add(gradeSystemQueryInspectorRecord);
        this.searchOrderRecords.add(gradeSystemSearchOrderRecord);
        
        return;
    }
}
