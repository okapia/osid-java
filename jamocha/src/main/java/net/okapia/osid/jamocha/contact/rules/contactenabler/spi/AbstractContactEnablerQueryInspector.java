//
// AbstractContactEnablerQueryInspector.java
//
//     A template for making a ContactEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.rules.contactenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for contact enablers.
 */

public abstract class AbstractContactEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.contact.rules.ContactEnablerQueryInspector {

    private final java.util.Collection<org.osid.contact.rules.records.ContactEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the contact <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledContactIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the contact query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.contact.ContactQueryInspector[] getRuledContactTerms() {
        return (new org.osid.contact.ContactQueryInspector[0]);
    }


    /**
     *  Gets the address book <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressBookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the address book query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given contact enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a contact enabler implementing the requested record.
     *
     *  @param contactEnablerRecordType a contact enabler record type
     *  @return the contact enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.rules.records.ContactEnablerQueryInspectorRecord getContactEnablerQueryInspectorRecord(org.osid.type.Type contactEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.rules.records.ContactEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(contactEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this contact enabler query. 
     *
     *  @param contactEnablerQueryInspectorRecord contact enabler query inspector
     *         record
     *  @param contactEnablerRecordType contactEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addContactEnablerQueryInspectorRecord(org.osid.contact.rules.records.ContactEnablerQueryInspectorRecord contactEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type contactEnablerRecordType) {

        addRecordType(contactEnablerRecordType);
        nullarg(contactEnablerRecordType, "contact enabler record type");
        this.records.add(contactEnablerQueryInspectorRecord);        
        return;
    }
}
