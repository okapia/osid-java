//
// AbstractRaceConstrainerEnablerSearch.java
//
//     A template for making a RaceConstrainerEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing race constrainer enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRaceConstrainerEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.voting.rules.RaceConstrainerEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.voting.rules.RaceConstrainerEnablerSearchOrder raceConstrainerEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of race constrainer enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  raceConstrainerEnablerIds list of race constrainer enablers
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRaceConstrainerEnablers(org.osid.id.IdList raceConstrainerEnablerIds) {
        while (raceConstrainerEnablerIds.hasNext()) {
            try {
                this.ids.add(raceConstrainerEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRaceConstrainerEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of race constrainer enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRaceConstrainerEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  raceConstrainerEnablerSearchOrder race constrainer enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>raceConstrainerEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRaceConstrainerEnablerResults(org.osid.voting.rules.RaceConstrainerEnablerSearchOrder raceConstrainerEnablerSearchOrder) {
	this.raceConstrainerEnablerSearchOrder = raceConstrainerEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.voting.rules.RaceConstrainerEnablerSearchOrder getRaceConstrainerEnablerSearchOrder() {
	return (this.raceConstrainerEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given race constrainer enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a race constrainer enabler implementing the requested record.
     *
     *  @param raceConstrainerEnablerSearchRecordType a race constrainer enabler search record
     *         type
     *  @return the race constrainer enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerEnablerSearchRecord getRaceConstrainerEnablerSearchRecord(org.osid.type.Type raceConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.voting.rules.records.RaceConstrainerEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(raceConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race constrainer enabler search. 
     *
     *  @param raceConstrainerEnablerSearchRecord race constrainer enabler search record
     *  @param raceConstrainerEnablerSearchRecordType raceConstrainerEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceConstrainerEnablerSearchRecord(org.osid.voting.rules.records.RaceConstrainerEnablerSearchRecord raceConstrainerEnablerSearchRecord, 
                                           org.osid.type.Type raceConstrainerEnablerSearchRecordType) {

        addRecordType(raceConstrainerEnablerSearchRecordType);
        this.records.add(raceConstrainerEnablerSearchRecord);        
        return;
    }
}
