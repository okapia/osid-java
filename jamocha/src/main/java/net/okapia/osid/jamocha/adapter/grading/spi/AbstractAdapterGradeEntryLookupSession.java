//
// AbstractAdapterGradeEntryLookupSession.java
//
//    A GradeEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A GradeEntry lookup session adapter.
 */

public abstract class AbstractAdapterGradeEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.grading.GradeEntryLookupSession {

    private final org.osid.grading.GradeEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterGradeEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterGradeEntryLookupSession(org.osid.grading.GradeEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Gradebook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Gradebook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.session.getGradebookId());
    }


    /**
     *  Gets the {@code Gradebook} associated with this session.
     *
     *  @return the {@code Gradebook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGradebook());
    }


    /**
     *  Tests if this user can perform {@code GradeEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupGradeEntries() {
        return (this.session.canLookupGradeEntries());
    }


    /**
     *  A complete view of the {@code GradeEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradeEntryView() {
        this.session.useComparativeGradeEntryView();
        return;
    }


    /**
     *  A complete view of the {@code GradeEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradeEntryView() {
        this.session.usePlenaryGradeEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade entries in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.session.useFederatedGradebookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.session.useIsolatedGradebookView();
        return;
    }
    

    /**
     *  Only grade entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveGradeEntryView() {
        this.session.useEffectiveGradeEntryView();
        return;
    }
    

    /**
     *  All grade entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveGradeEntryView() {
        this.session.useAnyEffectiveGradeEntryView();
        return;
    }

     
    /**
     *  Gets the {@code GradeEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code GradeEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code GradeEntry} and
     *  retained for compatibility.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param gradeEntryId {@code Id} of the {@code GradeEntry}
     *  @return the grade entry
     *  @throws org.osid.NotFoundException {@code gradeEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code gradeEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntry getGradeEntry(org.osid.id.Id gradeEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntry(gradeEntryId));
    }


    /**
     *  Gets a {@code GradeEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code GradeEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param  gradeEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code GradeEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code gradeEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByIds(org.osid.id.IdList gradeEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesByIds(gradeEntryIds));
    }


    /**
     *  Gets a {@code GradeEntryList} corresponding to the given
     *  grade entry genus {@code Type} which does not include
     *  grade entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param  gradeEntryGenusType a gradeEntry genus type 
     *  @return the returned {@code GradeEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradeEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByGenusType(org.osid.type.Type gradeEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesByGenusType(gradeEntryGenusType));
    }


    /**
     *  Gets a {@code GradeEntryList} corresponding to the given
     *  grade entry genus {@code Type} and include any additional
     *  grade entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param  gradeEntryGenusType a gradeEntry genus type 
     *  @return the returned {@code GradeEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradeEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByParentGenusType(org.osid.type.Type gradeEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesByParentGenusType(gradeEntryGenusType));
    }


    /**
     *  Gets a {@code GradeEntryList} containing the given
     *  grade entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @param  gradeEntryRecordType a gradeEntry record type 
     *  @return the returned {@code GradeEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradeEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByRecordType(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesByRecordType(gradeEntryRecordType));
    }


    /**
     *  Gets a {@code GradeEntryList} effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *  
     *  In active mode, grade entries are returned that are currently
     *  active. In any status mode, active and inactive grade entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code GradeEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesOnDate(from, to));
    }
        

    /**
     *  Gets a list of grade entries corresponding to a gradebook
     *  column {@code Id}.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the {@code Id} of the gradebook column
     *  @return the returned {@code GradeEntryList}
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesForGradebookColumn(gradebookColumnId));
    }


    /**
     *  Gets a list of grade entries corresponding to a gradebook
     *  column {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the {@code Id} of the gradebook column
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code GradeEntryList}
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnOnDate(org.osid.id.Id gradebookColumnId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesForGradebookColumnOnDate(gradebookColumnId, from, to));
    }


    /**
     *  Gets a list of grade entries corresponding to a resource
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code GradeEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesForResource(resourceId));
    }


    /**
     *  Gets a list of grade entries corresponding to a resource
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code GradeEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForResourceOnDate(org.osid.id.Id resourceId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of grade entries corresponding to gradebook column
     *  and resource {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective.  In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  gradebookColumnId the {@code Id} of the gradebook column
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code GradeEntryList}
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnAndResource(org.osid.id.Id gradebookColumnId,
                                                                                        org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesForGradebookColumnAndResource(gradebookColumnId, resourceId));
    }


    /**
     *  Gets a list of grade entries corresponding to gradebook column
     *  and resource {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *
     *  In effective mode, grade entries are returned that are
     *  currently effective. In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code GradeEntryList}
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesForGradebookColumnAndResourceOnDate(org.osid.id.Id gradebookColumnId,
                                                                                              org.osid.id.Id resourceId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesForGradebookColumnAndResourceOnDate(gradebookColumnId, resourceId, from, to));
    }


    /**
     *  Gets a {@code GradeEntryList} for the given grader. 
     *  
     *  In plenary mode, the returned list contains all known grade
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those grade entries that are accessible through
     *  this session.
     *  
     *  In effective mode, grade entries are returned that are
     *  currently effective. In any effective mode, effective grade
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code GradeEntry} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByGrader(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntriesByGrader(resourceId));
    }


    /**
     *  Gets all {@code GradeEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  grade entries or an error results. Otherwise, the returned list
     *  may contain only those grade entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, grade entries are returned that are currently
     *  effective.  In any effective mode, effective grade entries and
     *  those currently expired are returned.
     *
     *  @return a list of {@code GradeEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeEntries());
    }
}
