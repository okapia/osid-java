//
// AbstractSpeedZoneQuery.java
//
//     A template for making a SpeedZone Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.speedzone.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for speed zones.
 */

public abstract class AbstractSpeedZoneQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.mapping.path.SpeedZoneQuery {

    private final java.util.Collection<org.osid.mapping.path.records.SpeedZoneQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the path <code> Id </code> for this query to match speed zones 
     *  along the given path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        return;
    }


    /**
     *  Matches speed zones ovarlapping with the specified <code>
     *  Coordinate.  </code>
     *
     *  @param  coordinate a coordinate 
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the coordinate query terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        return;
    }


    /**
     *  Matches speed zones contained within the specified <code> Coordinates 
     *  </code> on its path inclusive. 
     *
     *  @param  spatialUnit a spatial unit
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnit</code> <code> null </code>
     */

    @OSID @Override
    public void matchContainingSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the spatial unit query terms. 
     */

    @OSID @Override
    public void clearContainingSpatialUnitTerms() {
        return;
    }


    /**
     *  Matches implicitly defined speed zones. 
     *
     *  @param  match <code> true </code> to match implicit speed zones with 
     *          any boundary, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchImplicit(boolean match) {
        return;
    }


    /**
     *  Clears the implicit query terms. 
     */

    @OSID @Override
    public void clearImplicitTerms() {
        return;
    }


    /**
     *  Matches speed zones with speed limite within the given range 
     *  inclusive. 
     *
     *  @param  from starting speed range 
     *  @param  to ending speed range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSpeedLimit(org.osid.mapping.Speed from, 
                                org.osid.mapping.Speed to, boolean match) {
        return;
    }


    /**
     *  Matches speed zones with any speed limit set. 
     *
     *  @param  match <code> true </code> to match implicit speed zones with 
     *          any speed limit, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnySpeedLimit(boolean match) {
        return;
    }


    /**
     *  Clears the speed limit query terms. 
     */

    @OSID @Override
    public void clearSpeedLimitTerms() {
        return;
    }


    /**
     *  Sets the map <code> Id </code> for this query. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given speed zone query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a speed zone implementing the requested record.
     *
     *  @param speedZoneRecordType a speed zone record type
     *  @return the speed zone query record
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneQueryRecord getSpeedZoneQueryRecord(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SpeedZoneQueryRecord record : this.records) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneRecordType + " is not supported");
    }


    /**
     *  Adds a record to this speed zone query. 
     *
     *  @param speedZoneQueryRecord speed zone query record
     *  @param speedZoneRecordType speedZone record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSpeedZoneQueryRecord(org.osid.mapping.path.records.SpeedZoneQueryRecord speedZoneQueryRecord, 
                                          org.osid.type.Type speedZoneRecordType) {

        addRecordType(speedZoneRecordType);
        nullarg(speedZoneQueryRecord, "speed zone query record");
        this.records.add(speedZoneQueryRecord);        
        return;
    }
}
