//
// AbstractImmutableHold.java
//
//     Wraps a mutable Hold to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hold.hold.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Hold</code> to hide modifiers. This
 *  wrapper provides an immutized Hold from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying hold whose state changes are visible.
 */

public abstract class AbstractImmutableHold
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.hold.Hold {

    private final org.osid.hold.Hold hold;


    /**
     *  Constructs a new <code>AbstractImmutableHold</code>.
     *
     *  @param hold the hold to immutablize
     *  @throws org.osid.NullArgumentException <code>hold</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableHold(org.osid.hold.Hold hold) {
        super(hold);
        this.hold = hold;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the issue. 
     *
     *  @return the issue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getIssueId() {
        return (this.hold.getIssueId());
    }


    /**
     *  Gets the issue. 
     *
     *  @return the issue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.hold.Issue getIssue()
        throws org.osid.OperationFailedException {

        return (this.hold.getIssue());
    }


    /**
     *  Tests if this hold is for a <code> Resource. </code> 
     *
     *  @return <code> true </code> if this hold has a <code> Resource, 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasResource() {
        return (this.hold.hasResource());
    }


    /**
     *  Gets the <code> Id </code> of resource. 
     *
     *  @return the resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.hold.getResourceId());
    }


    /**
     *  Gets the resources that can be assigned to an issue. 
     *
     *  @return the resource 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.hold.getResource());
    }


    /**
     *  Tests if this hold is for an <code> Agent. </code> 
     *
     *  @return <code> true </code> if this hold has an <code> Agent, </code> 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAgent() {
        return (this.hold.hasAgent());
    }


    /**
     *  Gets the <code> Agent Id </code> for this hold. 
     *
     *  @return the <code> Agent Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.hold.getAgentId());
    }


    /**
     *  Gets the <code> Agent </code> for this hold. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.hold.getAgent());
    }


    /**
     *  Gets the hold record corresponding to the given <code> Hold </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> holdRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(holdRecordType) </code> is <code> true </code> . 
     *
     *  @param  holdRecordType the type of hold record to retrieve 
     *  @return the hold record 
     *  @throws org.osid.NullArgumentException <code> holdRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(holdRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.records.HoldRecord getHoldRecord(org.osid.type.Type holdRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.hold.getHoldRecord(holdRecordType));
    }
}

