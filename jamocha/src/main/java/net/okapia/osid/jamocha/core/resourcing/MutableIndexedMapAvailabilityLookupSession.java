//
// MutableIndexedMapAvailabilityLookupSession
//
//    Implements an Availability lookup service backed by a collection of
//    availabilities indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements an Availability lookup service backed by a collection of
 *  availabilities. The availabilities are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some availabilities may be compatible
 *  with more types than are indicated through these availability
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of availabilities can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapAvailabilityLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractIndexedMapAvailabilityLookupSession
    implements org.osid.resourcing.AvailabilityLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapAvailabilityLookupSession} with no availabilities.
     *
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumentException {@code foundry}
     *          is {@code null}
     */

      public MutableIndexedMapAvailabilityLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAvailabilityLookupSession} with a
     *  single availability.
     *  
     *  @param foundry the foundry
     *  @param  availability an single availability
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code availability} is {@code null}
     */

    public MutableIndexedMapAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Availability availability) {
        this(foundry);
        putAvailability(availability);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAvailabilityLookupSession} using an
     *  array of availabilities.
     *
     *  @param foundry the foundry
     *  @param  availabilities an array of availabilities
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code availabilities} is {@code null}
     */

    public MutableIndexedMapAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Availability[] availabilities) {
        this(foundry);
        putAvailabilities(availabilities);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAvailabilityLookupSession} using a
     *  collection of availabilities.
     *
     *  @param foundry the foundry
     *  @param  availabilities a collection of availabilities
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code availabilities} is {@code null}
     */

    public MutableIndexedMapAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.Availability> availabilities) {

        this(foundry);
        putAvailabilities(availabilities);
        return;
    }
    

    /**
     *  Makes an {@code Availability} available in this session.
     *
     *  @param  availability an availability
     *  @throws org.osid.NullArgumentException {@code availability{@code  is
     *          {@code null}
     */

    @Override
    public void putAvailability(org.osid.resourcing.Availability availability) {
        super.putAvailability(availability);
        return;
    }


    /**
     *  Makes an array of availabilities available in this session.
     *
     *  @param  availabilities an array of availabilities
     *  @throws org.osid.NullArgumentException {@code availabilities{@code 
     *          is {@code null}
     */

    @Override
    public void putAvailabilities(org.osid.resourcing.Availability[] availabilities) {
        super.putAvailabilities(availabilities);
        return;
    }


    /**
     *  Makes collection of availabilities available in this session.
     *
     *  @param  availabilities a collection of availabilities
     *  @throws org.osid.NullArgumentException {@code availability{@code  is
     *          {@code null}
     */

    @Override
    public void putAvailabilities(java.util.Collection<? extends org.osid.resourcing.Availability> availabilities) {
        super.putAvailabilities(availabilities);
        return;
    }


    /**
     *  Removes an Availability from this session.
     *
     *  @param availabilityId the {@code Id} of the availability
     *  @throws org.osid.NullArgumentException {@code availabilityId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAvailability(org.osid.id.Id availabilityId) {
        super.removeAvailability(availabilityId);
        return;
    }    
}
