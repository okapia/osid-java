//
// AbstractHoldEnablerQueryInspector.java
//
//     A template for making a HoldEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.rules.holdenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for hold enablers.
 */

public abstract class AbstractHoldEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.hold.rules.HoldEnablerQueryInspector {

    private final java.util.Collection<org.osid.hold.rules.records.HoldEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the hold <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledHoldIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the hold query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.HoldQueryInspector[] getRuledHoldTerms() {
        return (new org.osid.hold.HoldQueryInspector[0]);
    }


    /**
     *  Gets the oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOublietteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given hold enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a hold enabler implementing the requested record.
     *
     *  @param holdEnablerRecordType a hold enabler record type
     *  @return the hold enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.rules.records.HoldEnablerQueryInspectorRecord getHoldEnablerQueryInspectorRecord(org.osid.type.Type holdEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.rules.records.HoldEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(holdEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this hold enabler query. 
     *
     *  @param holdEnablerQueryInspectorRecord hold enabler query inspector
     *         record
     *  @param holdEnablerRecordType holdEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addHoldEnablerQueryInspectorRecord(org.osid.hold.rules.records.HoldEnablerQueryInspectorRecord holdEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type holdEnablerRecordType) {

        addRecordType(holdEnablerRecordType);
        nullarg(holdEnablerRecordType, "hold enabler record type");
        this.records.add(holdEnablerQueryInspectorRecord);        
        return;
    }
}
