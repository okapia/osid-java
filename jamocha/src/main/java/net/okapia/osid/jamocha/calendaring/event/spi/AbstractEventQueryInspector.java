//
// AbstractEventQueryInspector.java
//
//     A template for making an EventQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.event.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for events.
 */

public abstract class AbstractEventQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.calendaring.EventQueryInspector {

    private final java.util.Collection<org.osid.calendaring.records.EventQueryInspectorRecord> records = new java.util.ArrayList<>();
    private final OsidContainableQueryInspector inspector = new OsidContainableQueryInspector();


    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.inspector.getSequesteredTerms());
    }

    
    /**
     *  Gets the implicit terms. 
     *
     *  @return the implicit terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getImplicitTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the duration terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationTerms() {
        return (new org.osid.search.terms.DurationTerm[0]);
    }


    /**
     *  Gets the recurring event <code> Id </code> terms. 
     *
     *  @return the recurring event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecurringEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the recurring event terms. 
     *
     *  @return the recurring event terms 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQueryInspector[] getRecurringEventTerms() {
        return (new org.osid.calendaring.RecurringEventQueryInspector[0]);
    }


    /**
     *  Gets the superseding event <code> Id </code> terms. 
     *
     *  @return the superseding event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupersedingEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the superseding event terms. 
     *
     *  @return the superseding event terms 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQueryInspector[] getSupersedingEventTerms() {
        return (new org.osid.calendaring.SupersedingEventQueryInspector[0]);
    }


    /**
     *  Gets the offset event <code> Id </code> terms. 
     *
     *  @return the offset event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOffsetEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the offset event terms. 
     *
     *  @return the offset event terms 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQueryInspector[] getOffsetEventTerms() {
        return (new org.osid.calendaring.OffsetEventQueryInspector[0]);
    }


    /**
     *  Gets the location description terms. 
     *
     *  @return the location description terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLocationDescriptionTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the location <code> Id </code> terms. 
     *
     *  @return the location <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the location terms. 
     *
     *  @return the location terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the sponsor <code> Id </code> terms. 
     *
     *  @return the sponsor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the sponsor terms. 
     *
     *  @return the sponsor terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the coordinate terms. 
     *
     *  @return the coordinate terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (new org.osid.search.terms.CoordinateTerm[0]);
    }


    /**
     *  Gets the spatial unit terms. 
     *
     *  @return the spatial unit terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialUnitTerms() {
        return (new org.osid.search.terms.SpatialUnitTerm[0]);
    }


    /**
     *  Gets the commitment <code> Id </code> terms. 
     *
     *  @return the commitment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommitmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the commitment terms. 
     *
     *  @return the commitment terms 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQueryInspector[] getCommitmentTerms() {
        return (new org.osid.calendaring.CommitmentQueryInspector[0]);
    }


    /**
     *  Gets the containing event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the containing event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getContainingEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given event query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an event implementing the requested record.
     *
     *  @param eventRecordType an event record type
     *  @return the event query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(eventRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.EventQueryInspectorRecord getEventQueryInspectorRecord(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.EventQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(eventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(eventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this event query. 
     *
     *  @param eventQueryInspectorRecord event query inspector
     *         record
     *  @param eventRecordType event record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEventQueryInspectorRecord(org.osid.calendaring.records.EventQueryInspectorRecord eventQueryInspectorRecord, 
                                                   org.osid.type.Type eventRecordType) {

        addRecordType(eventRecordType);
        nullarg(eventRecordType, "event record type");
        this.records.add(eventQueryInspectorRecord);        
        return;
    }


    protected class OsidContainableQueryInspector
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQueryInspector
        implements org.osid.OsidContainableQueryInspector {
    }
}
