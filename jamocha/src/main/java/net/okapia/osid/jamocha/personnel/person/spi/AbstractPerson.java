//
// AbstractPerson.java
//
//     Defines a Person.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.person.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Person</code>.
 */

public abstract class AbstractPerson
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.personnel.Person {

    private org.osid.locale.DisplayText salutation;
    private org.osid.locale.DisplayText givenName;
    private org.osid.locale.DisplayText preferredName;
    private org.osid.locale.DisplayText surname;

    private final java.util.Collection<org.osid.locale.DisplayText> forenameAliases = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.locale.DisplayText> middleNames = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.locale.DisplayText> surnameAliases = new java.util.LinkedHashSet<>();

    private org.osid.locale.DisplayText generationQualifier;
    private org.osid.locale.DisplayText qualificationSuffix;
    private org.osid.calendaring.DateTime birthDate;
    private org.osid.calendaring.DateTime deathDate;
    private String institutionalIdentifier;

    private final java.util.Collection<org.osid.personnel.records.PersonRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the title for this person (Mr., Dr., Ms.). 
     *
     *  @return the title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSalutation() {
        return (this.salutation);
    }


    /**
     *  Sets the salutation.
     *
     *  @param salutation a salutation
     *  @throws org.osid.NullArgumentException
     *          <code>salutation</code> is <code>null</code>
     */

    protected void setSalutation(org.osid.locale.DisplayText salutation) {
        nullarg(salutation, "salutation");
        this.salutation = salutation;
        return;
    }


    /**
     *  Gets the given name of this person. 
     *
     *  @return the given name 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getGivenName() {
        return (this.givenName);
    }


    /**
     *  Sets the given name.
     *
     *  @param givenName a given name
     *  @throws org.osid.NullArgumentException
     *          <code>givenName</code> is <code>null</code>
     */

    protected void setGivenName(org.osid.locale.DisplayText givenName) {
        nullarg(givenName, "given name");
        this.givenName = givenName;
        return;
    }


    /**
     *  Gets the preferred forename or mononym of this person. 
     *
     *  @return the preferred name 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getPreferredName() {
        return (this.preferredName);
    }


    /**
     *  Sets the preferred name.
     *
     *  @param preferredName a preferred name
     *  @throws org.osid.NullArgumentException
     *          <code>preferredName</code> is <code>null</code>
     */

    protected void setPreferredName(org.osid.locale.DisplayText preferredName) {
        nullarg(preferredName, "preferred name");
        this.preferredName = preferredName;
        return;
    }


    /**
     *  Gets additional forenames this person is or was known by. 
     *
     *  @return forname aliases 
     */

    @OSID @Override
    public org.osid.locale.DisplayText[] getForenameAliases() {
        return (this.forenameAliases.toArray(new org.osid.locale.DisplayText[this.forenameAliases.size()]));
    }


    /**
     *  Adds ae forename alias.
     *
     *  @param forenameAlias a forename alias
     *  @throws org.osid.NullArgumentException
     *          <code>forenameAlias</code> is <code>null</code>
     */

    protected void addForenameAlias(org.osid.locale.DisplayText forenameAlias) {
        nullarg(forenameAlias, "forename alias");
        this.forenameAliases.add(forenameAlias);
        return;
    }


    /**
     *  Sets the forename aliases.
     *
     *  @param forenameAliases a collection of forename aliases
     *  @throws org.osid.NullArgumentException
     *          <code>forenameAliases</code> is <code>null</code>
     */

    protected void setForenameAliases(java.util.Collection<org.osid.locale.DisplayText> forenameAliases) {
        nullarg(forenameAliases, "forename aliases");
        this.forenameAliases.clear();
        this.forenameAliases.addAll(forenameAliases);
        return;
    }


    /**
     *  Gets the middle names of this person. 
     *
     *  @return the middle names 
     */

    @OSID @Override
    public org.osid.locale.DisplayText[] getMiddleNames() {
        return (this.middleNames.toArray(new org.osid.locale.DisplayText[this.middleNames.size()]));
    }


    /**
     *  Adds a middle name.
     *
     *  @param middleName a middle names
     *  @throws org.osid.NullArgumentException <code>middleName</code>
     *          is <code>null</code>
     */

    protected void addMiddleName(org.osid.locale.DisplayText middleName) {
        nullarg(middleName, "middle name");    
        this.middleNames.add(middleName);
        return;
    }


    /**
     *  Sets the middle names.
     *
     *  @param middleNames a collection of middle names
     *  @throws org.osid.NullArgumentException
     *          <code>middleNames</code> is <code>null</code>
     */

    protected void setMiddleNames(java.util.Collection<org.osid.locale.DisplayText> middleNames) {
        nullarg(middleNames, "middle names");
        this.middleNames.clear();
        this.middleNames.addAll(middleNames);
        return;
    }


    /**
     *  Gets the surname of this person. 
     *
     *  @return the surname 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSurname() {
        return (this.surname);
    }


    /**
     *  Sets the surname.
     *
     *  @param surname a surname
     *  @throws org.osid.NullArgumentException <code>surname</code> is
     *          <code>null</code>
     */

    protected void setSurname(org.osid.locale.DisplayText surname) {
        nullarg(surname, "surname");
        this.surname = surname;
        return;
    }


    /**
     *  Gets additional surnames this person is or was known by.
     *
     *  @return the surname aliases 
     */

    @OSID @Override
    public org.osid.locale.DisplayText[] getSurnameAliases() {
        return (this.surnameAliases.toArray(new org.osid.locale.DisplayText[this.surnameAliases.size()]));
    }


    /**
     *  Adds a surname alias.
     *
     *  @param surnameAlias a surname alias
     *  @throws org.osid.NullArgumentException
     *          <code>surnameAlias</code> is <code>null</code>
     */

    protected void addSurnameAlias(org.osid.locale.DisplayText surnameAlias) {
        nullarg(surnameAlias, "surname alias");
        this.surnameAliases.add(surnameAlias);
        return;
    }


    /**
     *  Sets the surname aliases.
     *
     *  @param surnameAliases a collection of surname aliases
     *  @throws org.osid.NullArgumentException
     *          <code>surnameAliases</code> is <code>null</code>
     */

    protected void setSurnameAliases(java.util.Collection<org.osid.locale.DisplayText> surnameAliases) {
        nullarg(surnameAliases, "surname aliases");
        this.surnameAliases.clear();
        this.surnameAliases.addAll(surnameAliases);
        return;
    }


    /**
     *  Gets the generation qualifier of this person. 
     *
     *  @return the generation qualifier 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getGenerationQualifier() {
        return (this.generationQualifier);
    }


    /**
     *  Sets the generation qualifier.
     *
     *  @param qualifier a generation qualifier
     *  @throws org.osid.NullArgumentException
     *          <code>qualifier</code> is <code>null</code>
     */

    protected void setGenerationQualifier(org.osid.locale.DisplayText qualifier) {
        nullarg(qualifier, "generation qualifier");
        this.generationQualifier = qualifier;
        return;
    }


    /**
     *  Gets the qualification suffix of this person (MD, Phd). 
     *
     *  @return the generation qualifier 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getQualificationSuffix() {
        return (this.qualificationSuffix);
    }


    /**
     *  Sets the qualification suffix.
     *
     *  @param suffix a qualification suffix
     *  @throws org.osid.NullArgumentException
     *          <code>suffix</code> is <code>null</code>
     */

    protected void setQualificationSuffix(org.osid.locale.DisplayText suffix) {
        nullarg(suffix, "qualification suffix");
        this.qualificationSuffix = suffix;
        return;
    }


    /**
     *  Tests if a birth date is available. 
     *
     *  @return <code> true </code> if a birth date is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasBirthDate() {
        return (this.birthDate != null);
    }


    /**
     *  Gets the date of birth for this person. 
     *
     *  @return the date of birth 
     *  @throws org.osid.IllegalStateException <code> hasBirthDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getBirthDate() {
        if (!hasBirthDate()) {
            throw new org.osid.IllegalStateException("hasBirthDate() is false");
        }

        return (this.birthDate);
    }


    /**
     *  Sets the birth date.
     *
     *  @param date a birth date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setBirthDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "birth date");
        this.birthDate = date;
        return;
    }


    /**
     *  Tests if this person died. 
     *
     *  @return <code> true </code> if this person is dead, <code> false 
     *          </code> if still kicking 
     */

    @OSID @Override
    public boolean isDeceased() {
        return (this.deathDate != null);
    }


    /**
     *  Gets the date of death for this person. 
     *
     *  @return the date of death 
     *  @throws org.osid.IllegalStateException <code> isDead() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDeathDate() {
        if (!isDeceased()) {
            throw new org.osid.IllegalStateException("isDeceased() is false");
        }

        return (this.deathDate);
    }


    /**
     *  Sets the death date.
     *
     *  @param date a death date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDeathDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "death date");
        this.deathDate = date;
        return;
    }


    /**
     *  Gets the institutional identifier for this person. 
     *
     *  @return the institutional identifier 
     */

    @OSID @Override
    public String getInstitutionalIdentifier() {
        return (this.institutionalIdentifier);
    }


    /**
     *  Sets the institutional identifier.
     *
     *  @param identifier an institutional identifier
     *  @throws org.osid.NullArgumentException
     *          <code>iIdentifier</code> is <code>null</code>
     */

    protected void setInstitutionalIdentifier(String identifier) {
        nullarg(identifier, "institutional identifier");
        this.institutionalIdentifier = identifier;
        return;
    }


    /**
     *  Tests if this person supports the given record
     *  <code>Type</code>.
     *
     *  @param  personRecordType a person record type 
     *  @return <code>true</code> if the personRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type personRecordType) {
        for (org.osid.personnel.records.PersonRecord record : this.records) {
            if (record.implementsRecordType(personRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Person</code>
     *  record <code>Type</code>.
     *
     *  @param  personRecordType the person record type 
     *  @return the person record 
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(personRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PersonRecord getPersonRecord(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PersonRecord record : this.records) {
            if (record.implementsRecordType(personRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(personRecordType + " is not supported");
    }


    /**
     *  Adds a record to this person. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param personRecord the person record
     *  @param personRecordType person record type
     *  @throws org.osid.NullArgumentException
     *          <code>personRecord</code> or
     *          <code>personRecordTypeperson</code> is
     *          <code>null</code>
     */
            
    protected void addPersonRecord(org.osid.personnel.records.PersonRecord personRecord, 
                                   org.osid.type.Type personRecordType) {
        
        nullarg(personRecord, "person record");
        addRecordType(personRecordType);
        this.records.add(personRecord);
        
        return;
    }
}
