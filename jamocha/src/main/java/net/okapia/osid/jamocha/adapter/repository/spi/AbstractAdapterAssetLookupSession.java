//
// AbstractAdapterAssetLookupSession.java
//
//    An Asset lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.repository.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Asset lookup session adapter.
 */

public abstract class AbstractAdapterAssetLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.repository.AssetLookupSession {

    private final org.osid.repository.AssetLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAssetLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAssetLookupSession(org.osid.repository.AssetLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Repository/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Repository Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.session.getRepositoryId());
    }


    /**
     *  Gets the {@code Repository} associated with this session.
     *
     *  @return the {@code Repository} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRepository());
    }


    /**
     *  Tests if this user can perform {@code Asset} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAssets() {
        return (this.session.canLookupAssets());
    }


    /**
     *  A complete view of the {@code Asset} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssetView() {
        this.session.useComparativeAssetView();
        return;
    }


    /**
     *  A complete view of the {@code Asset} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssetView() {
        this.session.usePlenaryAssetView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assets in repositories which are children
     *  of this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        this.session.useFederatedRepositoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this repository only.
     */

    @OSID @Override
    public void useIsolatedRepositoryView() {
        this.session.useIsolatedRepositoryView();
        return;
    }
    
     
    /**
     *  Gets the {@code Asset} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Asset} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Asset} and
     *  retained for compatibility.
     *
     *  @param assetId {@code Id} of the {@code Asset}
     *  @return the asset
     *  @throws org.osid.NotFoundException {@code assetId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code assetId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Asset getAsset(org.osid.id.Id assetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAsset(assetId));
    }


    /**
     *  Gets an {@code AssetList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assets specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Assets} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  assetIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Asset} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code assetIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByIds(org.osid.id.IdList assetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssetsByIds(assetIds));
    }


    /**
     *  Gets an {@code AssetList} corresponding to the given
     *  asset genus {@code Type} which does not include
     *  assets of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assetGenusType an asset genus type 
     *  @return the returned {@code Asset} list
     *  @throws org.osid.NullArgumentException
     *          {@code assetGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByGenusType(org.osid.type.Type assetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssetsByGenusType(assetGenusType));
    }


    /**
     *  Gets an {@code AssetList} corresponding to the given
     *  asset genus {@code Type} and include any additional
     *  assets with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assetGenusType an asset genus type 
     *  @return the returned {@code Asset} list
     *  @throws org.osid.NullArgumentException
     *          {@code assetGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByParentGenusType(org.osid.type.Type assetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssetsByParentGenusType(assetGenusType));
    }


    /**
     *  Gets an {@code AssetList} containing the given
     *  asset record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assetRecordType an asset record type 
     *  @return the returned {@code Asset} list
     *  @throws org.osid.NullArgumentException
     *          {@code assetRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByRecordType(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssetsByRecordType(assetRecordType));
    }


    /**
     *  Gets an {@code AssetList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Asset} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssetsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Assets}. 
     *
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Assets} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssets());
    }
}
