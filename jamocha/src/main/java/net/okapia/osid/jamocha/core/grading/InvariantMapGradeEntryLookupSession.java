//
// InvariantMapGradeEntryLookupSession
//
//    Implements a GradeEntry lookup service backed by a fixed collection of
//    gradeEntries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading;


/**
 *  Implements a GradeEntry lookup service backed by a fixed
 *  collection of grade entries. The grade entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapGradeEntryLookupSession
    extends net.okapia.osid.jamocha.core.grading.spi.AbstractMapGradeEntryLookupSession
    implements org.osid.grading.GradeEntryLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapGradeEntryLookupSession</code> with no
     *  grade entries.
     *  
     *  @param gradebook the gradebook
     *  @throws org.osid.NullArgumnetException {@code gradebook} is
     *          {@code null}
     */

    public InvariantMapGradeEntryLookupSession(org.osid.grading.Gradebook gradebook) {
        setGradebook(gradebook);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapGradeEntryLookupSession</code> with a single
     *  grade entry.
     *  
     *  @param gradebook the gradebook
     *  @param gradeEntry a single grade entry
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradeEntry} is <code>null</code>
     */

      public InvariantMapGradeEntryLookupSession(org.osid.grading.Gradebook gradebook,
                                               org.osid.grading.GradeEntry gradeEntry) {
        this(gradebook);
        putGradeEntry(gradeEntry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapGradeEntryLookupSession</code> using an array
     *  of grade entries.
     *  
     *  @param gradebook the gradebook
     *  @param gradeEntries an array of grade entries
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradeEntries} is <code>null</code>
     */

      public InvariantMapGradeEntryLookupSession(org.osid.grading.Gradebook gradebook,
                                               org.osid.grading.GradeEntry[] gradeEntries) {
        this(gradebook);
        putGradeEntries(gradeEntries);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapGradeEntryLookupSession</code> using a
     *  collection of grade entries.
     *
     *  @param gradebook the gradebook
     *  @param gradeEntries a collection of grade entries
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradeEntries} is <code>null</code>
     */

      public InvariantMapGradeEntryLookupSession(org.osid.grading.Gradebook gradebook,
                                               java.util.Collection<? extends org.osid.grading.GradeEntry> gradeEntries) {
        this(gradebook);
        putGradeEntries(gradeEntries);
        return;
    }
}
