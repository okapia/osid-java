//
// AbstractPositionValidator.java
//
//     Validates a Position.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.personnel.position.spi;


/**
 *  Validates a Position.
 */

public abstract class AbstractPositionValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractTemporalOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractPositionValidator</code>.
     */

    protected AbstractPositionValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractPositionValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractPositionValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Position.
     *
     *  @param position a position to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>position</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.personnel.Position position) {
        super.validate(position);

        testNestedObject(position, "getOrganization");
        test(position.getTitle(), "getTitle()");
        testNestedObject(position, "getLevel");
        
        testConditionalMethod(position, "getQualificationIds", position.hasQualifications(), "hasQualifications()");
        testConditionalMethod(position, "getQualifications", position.hasQualifications(), "hasQualifications()");
        if (position.hasQualifications()) {
            testNestedObjects(position, "getQualificationIds", "getQualifications");
        }

        
        test(position.getTargetAppointments(), "getTargetAppointments()");
        testPercentage(position.getRequiredCommitment(), "getRequiredCommitment()");

        testConditionalMethod(position, "getLowSalaryRange", position.hasSalaryRange(), "hasSalaryRange()");
        testConditionalMethod(position, "getHighSalaryRange", position.hasSalaryRange(), "hasSalaryRange()");
        if (position.hasSalaryRange()) {
            test(position.getLowSalaryRange(), "getLowSalaryRange()");
            test(position.getMidpointSalaryRange(), "getMidpointSalaryRange()");
            test(position.getHighSalaryRange(), "getHighSalaryRange()");
            test(position.getCompensationFrequency(), "getCompensationFrequency()");

            if (position.getLowSalaryRange().isLarger(position.getMidpointSalaryRange()) ||
                position.getMidpointSalaryRange().isLarger(position.getHighSalaryRange())) {
                throw new org.osid.BadLogicException("salary range out of order");
            }
        }

        testConditionalMethod(position, "getBenefitsType", position.hasBenefits(), "hasBenefits()");

        return;
    }
}
