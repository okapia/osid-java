//
// QueryCampusLookupSession.java
//
//    An inline adapter that maps a CampusLookupSession to
//    a CampusQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.room;


/**
 *  An inline adapter that maps a CampusLookupSession to
 *  a CampusQuerySession.
 */

public final class QueryCampusLookupSession
    extends net.okapia.osid.jamocha.inline.room.spi.AbstractQueryCampusLookupSession
    implements org.osid.room.CampusLookupSession {


    /**
     *  Constructs a new QueryCampusLookupSession.
     *
     *  @param querySession the underlying campus query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    public QueryCampusLookupSession(org.osid.room.CampusQuerySession querySession) {
        super(querySession);
        return;
    }


    /**
     *  Constructs a new QueryCampusLookupSession.
     *
     *  @param querySession the underlying campus query session
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code querySession} or
     *          {@code proxy} is {@code null}
     */

    public QueryCampusLookupSession(org.osid.room.CampusQuerySession querySession,
                                      org.osid.proxy.Proxy proxy) {
        super(querySession);
        setSessionProxy(proxy);
        return;
    }
}
