//
// AbstractMapStoreLookupSession
//
//    A simple framework for providing a Store lookup service
//    backed by a fixed collection of stores.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Store lookup service backed by a
 *  fixed collection of stores. The stores are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Stores</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapStoreLookupSession
    extends net.okapia.osid.jamocha.ordering.spi.AbstractStoreLookupSession
    implements org.osid.ordering.StoreLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.ordering.Store> stores = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.ordering.Store>());


    /**
     *  Makes a <code>Store</code> available in this session.
     *
     *  @param  store a store
     *  @throws org.osid.NullArgumentException <code>store<code>
     *          is <code>null</code>
     */

    protected void putStore(org.osid.ordering.Store store) {
        this.stores.put(store.getId(), store);
        return;
    }


    /**
     *  Makes an array of stores available in this session.
     *
     *  @param  stores an array of stores
     *  @throws org.osid.NullArgumentException <code>stores<code>
     *          is <code>null</code>
     */

    protected void putStores(org.osid.ordering.Store[] stores) {
        putStores(java.util.Arrays.asList(stores));
        return;
    }


    /**
     *  Makes a collection of stores available in this session.
     *
     *  @param  stores a collection of stores
     *  @throws org.osid.NullArgumentException <code>stores<code>
     *          is <code>null</code>
     */

    protected void putStores(java.util.Collection<? extends org.osid.ordering.Store> stores) {
        for (org.osid.ordering.Store store : stores) {
            this.stores.put(store.getId(), store);
        }

        return;
    }


    /**
     *  Removes a Store from this session.
     *
     *  @param  storeId the <code>Id</code> of the store
     *  @throws org.osid.NullArgumentException <code>storeId<code> is
     *          <code>null</code>
     */

    protected void removeStore(org.osid.id.Id storeId) {
        this.stores.remove(storeId);
        return;
    }


    /**
     *  Gets the <code>Store</code> specified by its <code>Id</code>.
     *
     *  @param  storeId <code>Id</code> of the <code>Store</code>
     *  @return the store
     *  @throws org.osid.NotFoundException <code>storeId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>storeId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ordering.Store store = this.stores.get(storeId);
        if (store == null) {
            throw new org.osid.NotFoundException("store not found: " + storeId);
        }

        return (store);
    }


    /**
     *  Gets all <code>Stores</code>. In plenary mode, the returned
     *  list contains all known stores or an error
     *  results. Otherwise, the returned list may contain only those
     *  stores that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Stores</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStores()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.store.ArrayStoreList(this.stores.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stores.clear();
        super.close();
        return;
    }
}
