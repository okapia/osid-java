//
// AbstractMapStepProcessorEnablerLookupSession
//
//    A simple framework for providing a StepProcessorEnabler lookup service
//    backed by a fixed collection of step processor enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a StepProcessorEnabler lookup service backed by a
 *  fixed collection of step processor enablers. The step processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>StepProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapStepProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.workflow.rules.spi.AbstractStepProcessorEnablerLookupSession
    implements org.osid.workflow.rules.StepProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.workflow.rules.StepProcessorEnabler> stepProcessorEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.workflow.rules.StepProcessorEnabler>());


    /**
     *  Makes a <code>StepProcessorEnabler</code> available in this session.
     *
     *  @param  stepProcessorEnabler a step processor enabler
     *  @throws org.osid.NullArgumentException <code>stepProcessorEnabler<code>
     *          is <code>null</code>
     */

    protected void putStepProcessorEnabler(org.osid.workflow.rules.StepProcessorEnabler stepProcessorEnabler) {
        this.stepProcessorEnablers.put(stepProcessorEnabler.getId(), stepProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of step processor enablers available in this session.
     *
     *  @param  stepProcessorEnablers an array of step processor enablers
     *  @throws org.osid.NullArgumentException <code>stepProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putStepProcessorEnablers(org.osid.workflow.rules.StepProcessorEnabler[] stepProcessorEnablers) {
        putStepProcessorEnablers(java.util.Arrays.asList(stepProcessorEnablers));
        return;
    }


    /**
     *  Makes a collection of step processor enablers available in this session.
     *
     *  @param  stepProcessorEnablers a collection of step processor enablers
     *  @throws org.osid.NullArgumentException <code>stepProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putStepProcessorEnablers(java.util.Collection<? extends org.osid.workflow.rules.StepProcessorEnabler> stepProcessorEnablers) {
        for (org.osid.workflow.rules.StepProcessorEnabler stepProcessorEnabler : stepProcessorEnablers) {
            this.stepProcessorEnablers.put(stepProcessorEnabler.getId(), stepProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes a StepProcessorEnabler from this session.
     *
     *  @param  stepProcessorEnablerId the <code>Id</code> of the step processor enabler
     *  @throws org.osid.NullArgumentException <code>stepProcessorEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeStepProcessorEnabler(org.osid.id.Id stepProcessorEnablerId) {
        this.stepProcessorEnablers.remove(stepProcessorEnablerId);
        return;
    }


    /**
     *  Gets the <code>StepProcessorEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  stepProcessorEnablerId <code>Id</code> of the <code>StepProcessorEnabler</code>
     *  @return the stepProcessorEnabler
     *  @throws org.osid.NotFoundException <code>stepProcessorEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>stepProcessorEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnabler getStepProcessorEnabler(org.osid.id.Id stepProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.workflow.rules.StepProcessorEnabler stepProcessorEnabler = this.stepProcessorEnablers.get(stepProcessorEnablerId);
        if (stepProcessorEnabler == null) {
            throw new org.osid.NotFoundException("stepProcessorEnabler not found: " + stepProcessorEnablerId);
        }

        return (stepProcessorEnabler);
    }


    /**
     *  Gets all <code>StepProcessorEnablers</code>. In plenary mode, the returned
     *  list contains all known stepProcessorEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  stepProcessorEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>StepProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerList getStepProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepprocessorenabler.ArrayStepProcessorEnablerList(this.stepProcessorEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stepProcessorEnablers.clear();
        super.close();
        return;
    }
}
