//
// InvariantMapProxyBrokerProcessorLookupSession
//
//    Implements a BrokerProcessor lookup service backed by a fixed
//    collection of brokerProcessors. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a BrokerProcessor lookup service backed by a fixed
 *  collection of broker processors. The broker processors are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyBrokerProcessorLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapBrokerProcessorLookupSession
    implements org.osid.provisioning.rules.BrokerProcessorLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBrokerProcessorLookupSession} with no
     *  broker processors.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyBrokerProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyBrokerProcessorLookupSession} with a single
     *  broker processor.
     *
     *  @param distributor the distributor
     *  @param brokerProcessor a single broker processor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerProcessor} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBrokerProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.rules.BrokerProcessor brokerProcessor, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putBrokerProcessor(brokerProcessor);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyBrokerProcessorLookupSession} using
     *  an array of broker processors.
     *
     *  @param distributor the distributor
     *  @param brokerProcessors an array of broker processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerProcessors} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBrokerProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.rules.BrokerProcessor[] brokerProcessors, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putBrokerProcessors(brokerProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBrokerProcessorLookupSession} using a
     *  collection of broker processors.
     *
     *  @param distributor the distributor
     *  @param brokerProcessors a collection of broker processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerProcessors} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBrokerProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                                  java.util.Collection<? extends org.osid.provisioning.rules.BrokerProcessor> brokerProcessors,
                                                  org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putBrokerProcessors(brokerProcessors);
        return;
    }
}
