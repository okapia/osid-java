//
// MutableIndexedMapProxyRuleLookupSession
//
//    Implements a Rule lookup service backed by a collection of
//    rules indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules;


/**
 *  Implements a Rule lookup service backed by a collection of
 *  rules. The rules are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some rules may be compatible
 *  with more types than are indicated through these rule
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of rules can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyRuleLookupSession
    extends net.okapia.osid.jamocha.core.rules.spi.AbstractIndexedMapRuleLookupSession
    implements org.osid.rules.RuleLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRuleLookupSession} with
     *  no rule.
     *
     *  @param engine the engine
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRuleLookupSession(org.osid.rules.Engine engine,
                                                       org.osid.proxy.Proxy proxy) {
        setEngine(engine);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRuleLookupSession} with
     *  a single rule.
     *
     *  @param engine the engine
     *  @param  rule an rule
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code rule}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRuleLookupSession(org.osid.rules.Engine engine,
                                                       org.osid.rules.Rule rule, org.osid.proxy.Proxy proxy) {

        this(engine, proxy);
        putRule(rule);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRuleLookupSession} using
     *  an array of rules.
     *
     *  @param engine the engine
     *  @param  rules an array of rules
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code rules}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRuleLookupSession(org.osid.rules.Engine engine,
                                                       org.osid.rules.Rule[] rules, org.osid.proxy.Proxy proxy) {

        this(engine, proxy);
        putRules(rules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRuleLookupSession} using
     *  a collection of rules.
     *
     *  @param engine the engine
     *  @param  rules a collection of rules
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code rules}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRuleLookupSession(org.osid.rules.Engine engine,
                                                       java.util.Collection<? extends org.osid.rules.Rule> rules,
                                                       org.osid.proxy.Proxy proxy) {
        this(engine, proxy);
        putRules(rules);
        return;
    }

    
    /**
     *  Makes a {@code Rule} available in this session.
     *
     *  @param  rule a rule
     *  @throws org.osid.NullArgumentException {@code rule{@code 
     *          is {@code null}
     */

    @Override
    public void putRule(org.osid.rules.Rule rule) {
        super.putRule(rule);
        return;
    }


    /**
     *  Makes an array of rules available in this session.
     *
     *  @param  rules an array of rules
     *  @throws org.osid.NullArgumentException {@code rules{@code 
     *          is {@code null}
     */

    @Override
    public void putRules(org.osid.rules.Rule[] rules) {
        super.putRules(rules);
        return;
    }


    /**
     *  Makes collection of rules available in this session.
     *
     *  @param  rules a collection of rules
     *  @throws org.osid.NullArgumentException {@code rule{@code 
     *          is {@code null}
     */

    @Override
    public void putRules(java.util.Collection<? extends org.osid.rules.Rule> rules) {
        super.putRules(rules);
        return;
    }


    /**
     *  Removes a Rule from this session.
     *
     *  @param ruleId the {@code Id} of the rule
     *  @throws org.osid.NullArgumentException {@code ruleId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRule(org.osid.id.Id ruleId) {
        super.removeRule(ruleId);
        return;
    }    
}
