//
// AbstractAllocation.java
//
//     Defines an Allocation.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.allocation.allocation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Allocation</code>.
 */

public abstract class AbstractAllocation
    extends net.okapia.osid.jamocha.spi.AbstractBrowsable
    implements org.osid.filing.allocation.Allocation {

    private org.osid.filing.Directory directory;
    private org.osid.authentication.Agent agent;
    private long totalSpace;
    private long usedSpace;
    private long totalFiles;
    private long usedFiles;

    private final java.util.Collection<org.osid.filing.allocation.records.AllocationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the absolute path to the directory responsible for these usage 
     *  stats. The return may be a partition or volume containing the 
     *  requested directory. 
     *
     *  @return path name 
     */

    @OSID @Override
    public String getDirectoryPath() {
        return (this.directory.getPath());
    }


    /**
     *  Gets the directory responsible for these usage stats. The return may 
     *  be a partition, quota controlled directory, or volume containing the 
     *  requested directory. 
     *
     *  @return directory 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory() {
        return (this.directory);
    }


    /**
     *  Sets the directory.
     *
     *  @param directory a directory
     *  @throws org.osid.NullArgumentException
     *          <code>directory</code> is <code>null</code>
     */

    protected void setDirectory(org.osid.filing.Directory directory) {
        nullarg(directory, "directory");
        this.directory = directory;
        return;
    }


    /**
     *  Tests if this allocation is assigned to a specific user. 
     *
     *  @return <code> true </code> if this allocation is assigned to a 
     *          specific user, <code> false </code> if this allocation applied 
     *          to all users 
     */

    @OSID @Override
    public boolean isAssignedToUser() {
        if (this.agent == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the agent <code> Id </code> of the user. 
     *
     *  @return the agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isAssignedToUser() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        if (!isAssignedToUser()) {
            throw new org.osid.IllegalStateException("isAssignedToUser() is false");
        }

        return (this.agent.getId());
    }


    /**
     *  Gets the agent of the user. 
     *
     *  @return the agent 
     *  @throws org.osid.IllegalStateException <code> isAssignedToUser() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        if (!isAssignedToUser()) {
            throw new org.osid.IllegalStateException("isAssignedToUser() is false");
        }

        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Gets the total space in bytes of this allocation. 
     *
     *  @return number of bytes in this allocation 
     */

    @OSID @Override
    public long getTotalSpace() {
        return (this.totalSpace);
    }


    /**
     *  Sets the total space.
     *
     *  @param bytes the total space
     *  @throws org.osid.IllegalArgumentException <code>bytes</code>
     *          is negative
     */

    protected void setTotalSpace(long bytes) {
        cardinalarg(bytes, "bytes");
        this.totalSpace = totalSpace;
        return;
    }


    /**
     *  Gets the used space in bytes of this allocation. 
     *
     *  @return number of used bytes in this allocation 
     */

    @OSID @Override
    public long getUsedSpace() {
        return (this.usedSpace);
    }


    /**
     *  Sets the used space.
     *
     *  @param bytes the used space
     *  @throws org.osid.InvalieArgumentException <code>bytes</code>
     *          is negative
     */

    protected void setUsedSpace(long bytes) {
        cardinalarg(bytes, "bytes");
        this.usedSpace = bytes;
        return;
    }


    /**
     *  Gets the available space in bytes of this allocation.
     *
     *  @return number of available bytes in this allocation 
     */

    @OSID @Override
    public long getAvailableSpace() {
        return (getTotalSpace() - getUsedSpace());
    }


    /**
     *  Gets the total number of files of this allocation. 
     *
     *  @return number of files in this allocation 
     */

    @OSID @Override
    public long getTotalFiles() {
        return (this.totalFiles);
    }


    /**
     *  Sets the total files.
     *
     *  @param files the total files
     *  @throws org.osid.IllagelArgumentException <code>files</code>
     *          is negative
     */

    protected void setTotalFiles(long files) {
        cardinalarg(files, "files");
        this.totalFiles = files;
        return;
    }


    /**
     *  Gets the used number of files of this allocation. 
     *
     *  @return number of used files in this allocation 
     */

    @OSID @Override
    public long getUsedFiles() {
        return (this.usedFiles);
    }


    /**
     *  Sets the used files.
     *
     *  @param files the used files
     *  @throws org.osid.NullArgumentException <code>files</code> is
     *          negative
     */

    protected void setUsedFiles(long files) {
        cardinalarg(files, "files");
        this.usedFiles = files;
        return;
    }


    /**
     *  Gets the available number o files of this allocation. 
     *
     *  @return number of available files in this allocation 
     */

    @OSID @Override
    public long getAvailableFiles() {
        return (getTotalFiles() - getUsedFiles());
    }


    /**
     *  Tests if this allocation supports the given record
     *  <code>Type</code>.
     *
     *  @param  allocationRecordType an allocation record type 
     *  @return <code>true</code> if the allocationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>allocationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type allocationRecordType) {
        for (org.osid.filing.allocation.records.AllocationRecord record : this.records) {
            if (record.implementsRecordType(allocationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Allocation</code> record <code>Type</code>.
     *
     *  @param  allocationRecordType the allocation record type 
     *  @return the allocation record 
     *  @throws org.osid.NullArgumentException
     *          <code>allocationRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(allocationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.allocation.records.AllocationRecord getAllocationRecord(org.osid.type.Type allocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.allocation.records.AllocationRecord record : this.records) {
            if (record.implementsRecordType(allocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(allocationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this allocation. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param allocationRecord the allocation record
     *  @param allocationRecordType allocation record type
     *  @throws org.osid.NullArgumentException
     *          <code>allocationRecord</code> or
     *          <code>allocationRecordTypeallocation</code> is
     *          <code>null</code>
     */
            
    protected void addAllocationRecord(org.osid.filing.allocation.records.AllocationRecord allocationRecord, 
                                     org.osid.type.Type allocationRecordType) {

        addRecordType(allocationRecordType);
        this.records.add(allocationRecord);
        
        return;
    }
}
