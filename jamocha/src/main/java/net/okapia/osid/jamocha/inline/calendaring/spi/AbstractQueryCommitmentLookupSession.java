//
// AbstractQueryCommitmentLookupSession.java
//
//    An inline adapter that maps a CommitmentLookupSession to
//    a CommitmentQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CommitmentLookupSession to
 *  a CommitmentQuerySession.
 */

public abstract class AbstractQueryCommitmentLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractCommitmentLookupSession
    implements org.osid.calendaring.CommitmentLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.calendaring.CommitmentQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCommitmentLookupSession.
     *
     *  @param querySession the underlying commitment query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCommitmentLookupSession(org.osid.calendaring.CommitmentQuerySession querySession) {
        nullarg(querySession, "commitment query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>Commitment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCommitments() {
        return (this.session.canSearchCommitments());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commitments in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only commitments whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCommitmentView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All commitments of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCommitmentView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Commitment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Commitment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Commitment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  commitmentId <code>Id</code> of the
     *          <code>Commitment</code>
     *  @return the commitment
     *  @throws org.osid.NotFoundException <code>commitmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commitmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Commitment getCommitment(org.osid.id.Id commitmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchId(commitmentId, true);
        org.osid.calendaring.CommitmentList commitments = this.session.getCommitmentsByQuery(query);
        if (commitments.hasNext()) {
            return (commitments.getNextCommitment());
        } 
        
        throw new org.osid.NotFoundException(commitmentId + " not found");
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  commitments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Commitments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, commitments are returned that are currently effective.
     *  In any effective mode, effective commitments and those currently expired
     *  are returned.
     *
     *  @param  commitmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByIds(org.osid.id.IdList commitmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();

        try (org.osid.id.IdList ids = commitmentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  commitment genus <code>Type</code> which does not include
     *  commitments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently effective.
     *  In any effective mode, effective commitments and those currently expired
     *  are returned.
     *
     *  @param  commitmentGenusType a commitment genus type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusType(org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchGenusType(commitmentGenusType, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  commitment genus <code>Type</code> and include any additional
     *  commitments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  commitmentGenusType a commitment genus type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByParentGenusType(org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchParentGenusType(commitmentGenusType, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a <code>CommitmentList</code> containing the given
     *  commitment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  commitmentRecordType a commitment record type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByRecordType(org.osid.type.Type commitmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchRecordType(commitmentRecordType, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a <code>CommitmentList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *  
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Commitment</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCommitmentsByQuery(query));
    }
        

    /**
     *  Gets a <code> CommitmentList </code> of the given genus type
     *  and effective during the entire given date range inclusive bu
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *  
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  commitmentGenusType a commitment genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return list of commitments 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> commitmentGenusType, 
     *          from, </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeOnDate(org.osid.type.Type commitmentGenusType, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchGenusType(commitmentGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a list of commitments corresponding to a event
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>eventId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.calendaring.CommitmentList getCommitmentsForEvent(org.osid.id.Id eventId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchEventId(eventId, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a list of commitments corresponding to a event
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>eventId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEventOnDate(org.osid.id.Id eventId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchEventId(eventId, true);
        query.matchDate(from, to, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets the commitments for the given event and commitment genus
     *  type including any genus types derived from the given genus
     *  type.
     *  
     *  If the event is a recurring event, the commitments are
     *  returned for the recurring event only. In plenary mode, the
     *  returned list contains all of the commitments mapped to the
     *  event <code> Id </code> or an error results if an Id in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible commitments may be ommitted.
     *  
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code> 
     *  @param  commitmentGenusType commitment genus type 
     *  @return list of commitments 
     *  @throws org.osid.NullArgumentException <code> eventId </code> or 
     *          <code> commitmentGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEvent(org.osid.id.Id eventId, 
                                                                                 org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchEventId(eventId, true);
        query.matchGenusType(commitmentGenusType, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a <code> CommitmentList </code> for the given event and
     *  commitment genus type effective during the entire given date
     *  range inclusive but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *  
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code> 
     *  @param  commitmentGenusType commitment genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return list of commitments 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> eventId, 
     *          commitmentGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEventOnDate(org.osid.id.Id eventId, 
                                                                                       org.osid.type.Type commitmentGenusType, 
                                                                                       org.osid.calendaring.DateTime from, 
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchEventId(eventId, true);
        query.matchGenusType(commitmentGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a list of commitments corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.calendaring.CommitmentList getCommitmentsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a list of commitments corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForResourceOnDate(org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets the commitments for the given resource and commitment
     *  genus type including any genus types derived from the given
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all of the
     *  commitments mapped to the resource <code> Id </code> or an
     *  error results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Commitments
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *  
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> 
     *  @param  commitmentGenusType commitment genus type 
     *  @return list of commitments 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or 
     *          <code> commitmentGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForResource(org.osid.id.Id resourceId, 
                                                                                    org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchGenusType(commitmentGenusType, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a <code> CommitmentList </code> for the given resource,
     *  commitment genus type, and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *  
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  resourceId <code> Id </code> of the <code> Event </code> 
     *  @param  commitmentGenusType commitment genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return list of commitments 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, 
     *          commitmentGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                          org.osid.type.Type commitmentGenusType, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchGenusType(commitmentGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a list of commitments corresponding to event and resource
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>eventId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEventAndResource(org.osid.id.Id eventId,
                                                                                 org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchEventId(eventId, true);
        query.matchResourceId(resourceId, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a list of commitments corresponding to event and resource
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>eventId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEventAndResourceOnDate(org.osid.id.Id eventId,
                                                                                       org.osid.id.Id resourceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchEventId(eventId, true);
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets the commitmentsof the given genus type for the given
     *  event, resource, and commitment genus type including any genus
     *  types derived from the given genus type.. If the event is a
     *  recurring event, the commitments are returned for the
     *  recurring event only.
     *  
     *  In plenary mode, the returned list contains all of the
     *  commitments mapped to the event <code> Id </code> or an error
     *  results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Commitments
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *  
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code> 
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> 
     *  @param  commitmentGenusType commitment genus type 
     *  @return list of commitments 
     *  @throws org.osid.NullArgumentException <code> eventId, resourceId 
     *          </code> or <code> commitmentGenusType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEventAndResource(org.osid.id.Id eventId, 
                                                                                            org.osid.id.Id resourceId, 
                                                                                            org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchEventId(eventId, true);
        query.matchResourceId(resourceId, true);
        query.matchGenusType(commitmentGenusType, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets a <code> CommitmentList </code> of the given genus type
     *  for the given event, resource, commitment genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *  
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code> 
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> 
     *  @param  commitmentGenusType commitment genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return list of commitments 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> eventId, resourceId, 
     *          commitmentGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEventAndResourceOnDate(org.osid.id.Id eventId, 
                                                                                                  org.osid.id.Id resourceId, 
                                                                                                  org.osid.type.Type commitmentGenusType, 
                                                                                                  org.osid.calendaring.DateTime from, 
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchEventId(eventId, true);
        query.matchResourceId(resourceId, true);
        query.matchGenusType(commitmentGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets all <code>Commitments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Commitments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.CommitmentQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCommitmentsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.CommitmentQuery getQuery() {
        org.osid.calendaring.CommitmentQuery query = this.session.getCommitmentQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
