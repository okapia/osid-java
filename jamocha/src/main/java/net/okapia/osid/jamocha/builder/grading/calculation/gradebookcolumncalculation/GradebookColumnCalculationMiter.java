//
// GradebookColumnCalculationMiter.java
//
//     Defines a GradebookColumnCalculation miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation;


/**
 *  Defines a <code>GradebookColumnCalculation</code> miter for use with the builders.
 */

public interface GradebookColumnCalculationMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.grading.calculation.GradebookColumnCalculation {


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    public void setGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn);


    /**
     *  Adds an input gradebook column.
     *
     *  @param gradebookColumn an input gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    public void addInputGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn);


    /**
     *  Sets all the input gradebook columns.
     *
     *  @param gradebookColumns a collection of input gradebook columns
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumns</code> is <code>null</code>
     */

    public void setInputGradebookColumns(java.util.Collection<org.osid.grading.GradebookColumn> gradebookColumns);


    /**
     *  Sets the operation.
     *
     *  @param operation an operation
     *  @throws org.osid.NullArgumentException <code>operation</code>
     *          is <code>null</code>
     */

    public void setOperation(org.osid.grading.calculation.CalculationOperation operation);


    /**
     *  Sets the tweaked center.
     *
     *  @param center a tweaked center
     *  @throws org.osid.NullArgumentException <code>center</code> is
     *          <code>null</code>
     */

    public void setTweakedCenter(java.math.BigDecimal center);


    /**
     *  Sets the tweaked standard deviation.
     *
     *  @param standardDeviation a tweaked standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    public void setTweakedStandardDeviation(java.math.BigDecimal standardDeviation);


    /**
     *  Adds a GradebookColumnCalculation record.
     *
     *  @param record a gradebookColumnCalculation record
     *  @param recordType the type of gradebookColumnCalculation record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addGradebookColumnCalculationRecord(org.osid.grading.calculation.records.GradebookColumnCalculationRecord record, org.osid.type.Type recordType);
}       


