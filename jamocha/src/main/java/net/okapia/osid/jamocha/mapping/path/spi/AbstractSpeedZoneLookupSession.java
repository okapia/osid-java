//
// AbstractSpeedZoneLookupSession.java
//
//    A starter implementation framework for providing a SpeedZone
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a SpeedZone
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSpeedZones(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSpeedZoneLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.mapping.path.SpeedZoneLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();
    

    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>SpeedZone</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSpeedZones() {
        return (true);
    }


    /**
     *  A complete view of the <code>SpeedZone</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSpeedZoneView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>SpeedZone</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySpeedZoneView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include speed zones in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active speed zones are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSpeedZoneView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive speed zones are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSpeedZoneView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>SpeedZone</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SpeedZone</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SpeedZone</code> and
     *  retained for compatibility.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @param  speedZoneId <code>Id</code> of the
     *          <code>SpeedZone</code>
     *  @return the speed zone
     *  @throws org.osid.NotFoundException <code>speedZoneId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>speedZoneId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZone getSpeedZone(org.osid.id.Id speedZoneId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.mapping.path.SpeedZoneList speedZones = getSpeedZones()) {
            while (speedZones.hasNext()) {
                org.osid.mapping.path.SpeedZone speedZone = speedZones.getNextSpeedZone();
                if (speedZone.getId().equals(speedZoneId)) {
                    return (speedZone);
                }
            }
        } 

        throw new org.osid.NotFoundException(speedZoneId + " not found");
    }


    /**
     *  Gets a <code>SpeedZoneList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  speedZones specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SpeedZones</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSpeedZones()</code>.
     *
     *  @param  speedZoneIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SpeedZone</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByIds(org.osid.id.IdList speedZoneIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.mapping.path.SpeedZone> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = speedZoneIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSpeedZone(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("speed zone " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.mapping.path.speedzone.LinkedSpeedZoneList(ret));
    }


    /**
     *  Gets a <code>SpeedZoneList</code> corresponding to the given
     *  speed zone genus <code>Type</code> which does not include
     *  speed zones of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  speed zones or an error results. Otherwise, the returned list
     *  may contain only those speed zones that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSpeedZones()</code>.
     *
     *  @param  speedZoneGenusType a speedZone genus type 
     *  @return the returned <code>SpeedZone</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByGenusType(org.osid.type.Type speedZoneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.speedzone.SpeedZoneGenusFilterList(getSpeedZones(), speedZoneGenusType));
    }


    /**
     *  Gets a <code>SpeedZoneList</code> corresponding to the given
     *  speed zone genus <code>Type</code> and include any additional
     *  speed zones with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  speed zones or an error results. Otherwise, the returned list
     *  may contain only those speed zones that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSpeedZones()</code>.
     *
     *  @param  speedZoneGenusType a speedZone genus type 
     *  @return the returned <code>SpeedZone</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByParentGenusType(org.osid.type.Type speedZoneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSpeedZonesByGenusType(speedZoneGenusType));
    }


    /**
     *  Gets a <code>SpeedZoneList</code> containing the given
     *  speed zone record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  speed zones or an error results. Otherwise, the returned list
     *  may contain only those speed zones that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSpeedZones()</code>.
     *
     *  @param  speedZoneRecordType a speedZone record type 
     *  @return the returned <code>SpeedZone</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByRecordType(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.speedzone.SpeedZoneRecordFilterList(getSpeedZones(), speedZoneRecordType));
    }


    /**
     *  Gets a <code>SpeedZoneList</code> containing the given path. 
     *  
     *  In plenary mode, the returned list contains all known spwed
     *  zones or an error results. Otherwise, the returned list may
     *  contain only those speed zones that are accessible through
     *  this session.
     *  
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @param  pathId a path <code>Id</code> 
     *  @return the returned <code>SpeedZone</code> list 
     *  @throws org.osid.NullArgumentException <code>pathId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.speedzone.SpeedZoneFilterList(new PathFilter(pathId), getSpeedZones()));
    }        


    /**
     *  Gets a <code>SpeedZoneList</code> containing the given path
     *  between the given coordinates inclusive.
     *  
     *  In plenary mode, the returned list contains all known speed
     *  zones or an error results. Otherwise, the returned list may
     *  contain only those speed zones that are accessible through
     *  this session.
     *  
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  This method should be implemented.
     *
     *  @param  pathId a path <code>Id</code> 
     *  @param  coordinate starting coordinate 
     *  @param  distance a distance from coordinate 
     *  @return the returned <code>SpeedZone</code> list 
     *  @throws org.osid.NullArgumentException <code>pathId</code>,
     *          <code>coordinate</code> or <code>distance</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesForPathAtCoordinate(org.osid.id.Id pathId, 
                                                                                org.osid.mapping.Coordinate coordinate, 
                                                                                org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSpeedZonesForPath(pathId));
    }


    /**
     *  Gets all <code>SpeedZones</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  speed zones or an error results. Otherwise, the returned list
     *  may contain only those speed zones that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @return a list of <code>SpeedZones</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.mapping.path.SpeedZoneList getSpeedZones()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the speed zone list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of speed zones
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.mapping.path.SpeedZoneList filterSpeedZonesOnViews(org.osid.mapping.path.SpeedZoneList list)
        throws org.osid.OperationFailedException {

        org.osid.mapping.path.SpeedZoneList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.mapping.path.speedzone.ActiveSpeedZoneFilterList(ret);
        }

        return (ret);
    }


    public static class PathFilter
        implements net.okapia.osid.jamocha.inline.filter.mapping.path.speedzone.SpeedZoneFilter {

        private final org.osid.id.Id pathId;

        
        /**
         *  Constructs a new <code>PathFilterList</code>.
         *
         *  @param pathId the path to filter
         *  @throws org.osid.NullArgumentException
         *          <code>pathId</code> is <code>null</code>
         */

        public PathFilter(org.osid.id.Id pathId) {
            nullarg(pathId, "path Id");
            this.pathId = pathId;
            return;
        }


        /**
         *  Used by the SpeedZoneFilterList to filter the speed zone
         *  list based on path.
         *
         *  @param speedZone the speed zone
         *  @return <code>true</code> to pass the speed zone,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.mapping.path.SpeedZone speedZone) {
            return (speedZone.getPathId().equals(this.pathId));
        }
    }    
}
