//
// AbstractBrokerConstrainerEnablerSearch.java
//
//     A template for making a BrokerConstrainerEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing broker constrainer enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBrokerConstrainerEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.rules.BrokerConstrainerEnablerSearchOrder brokerConstrainerEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of broker constrainer enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  brokerConstrainerEnablerIds list of broker constrainer enablers
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBrokerConstrainerEnablers(org.osid.id.IdList brokerConstrainerEnablerIds) {
        while (brokerConstrainerEnablerIds.hasNext()) {
            try {
                this.ids.add(brokerConstrainerEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBrokerConstrainerEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of broker constrainer enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBrokerConstrainerEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  brokerConstrainerEnablerSearchOrder broker constrainer enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>brokerConstrainerEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBrokerConstrainerEnablerResults(org.osid.provisioning.rules.BrokerConstrainerEnablerSearchOrder brokerConstrainerEnablerSearchOrder) {
	this.brokerConstrainerEnablerSearchOrder = brokerConstrainerEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.rules.BrokerConstrainerEnablerSearchOrder getBrokerConstrainerEnablerSearchOrder() {
	return (this.brokerConstrainerEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given broker constrainer enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a broker constrainer enabler implementing the requested record.
     *
     *  @param brokerConstrainerEnablerSearchRecordType a broker constrainer enabler search record
     *         type
     *  @return the broker constrainer enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchRecord getBrokerConstrainerEnablerSearchRecord(org.osid.type.Type brokerConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(brokerConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this broker constrainer enabler search. 
     *
     *  @param brokerConstrainerEnablerSearchRecord broker constrainer enabler search record
     *  @param brokerConstrainerEnablerSearchRecordType brokerConstrainerEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBrokerConstrainerEnablerSearchRecord(org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchRecord brokerConstrainerEnablerSearchRecord, 
                                           org.osid.type.Type brokerConstrainerEnablerSearchRecordType) {

        addRecordType(brokerConstrainerEnablerSearchRecordType);
        this.records.add(brokerConstrainerEnablerSearchRecord);        
        return;
    }
}
