//
// AbstractFederatingReplyLookupSession.java
//
//     An abstract federating adapter for a ReplyLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ReplyLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingReplyLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.forum.ReplyLookupSession>
    implements org.osid.forum.ReplyLookupSession {

    private boolean parallel = false;
    private org.osid.forum.Forum forum = new net.okapia.osid.jamocha.nil.forum.forum.UnknownForum();


    /**
     *  Constructs a new <code>AbstractFederatingReplyLookupSession</code>.
     */

    protected AbstractFederatingReplyLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.forum.ReplyLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Forum/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Forum Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getForumId() {
        return (this.forum.getId());
    }


    /**
     *  Gets the <code>Forum</code> associated with this 
     *  session.
     *
     *  @return the <code>Forum</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.forum);
    }


    /**
     *  Sets the <code>Forum</code>.
     *
     *  @param  forum the forum for this session
     *  @throws org.osid.NullArgumentException <code>forum</code>
     *          is <code>null</code>
     */

    protected void setForum(org.osid.forum.Forum forum) {
        nullarg(forum, "forum");
        this.forum = forum;
        return;
    }


    /**
     *  Tests if this user can perform <code>Reply</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupReplies() {
        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            if (session.canLookupReplies()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Reply</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeReplyView() {
        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            session.useComparativeReplyView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Reply</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryReplyView() {
        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            session.usePlenaryReplyView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include replies in forums which are children
     *  of this forum in the forum hierarchy.
     */

    @OSID @Override
    public void useFederatedForumView() {
        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            session.useFederatedForumView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this forum only.
     */

    @OSID @Override
    public void useIsolatedForumView() {
        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            session.useIsolatedForumView();
        }

        return;
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  replies.
     */

    @OSID @Override
    public void useSequesteredReplyView() {
        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            session.useSequesteredReplyView();
        }

        return;
    }


    /**
     *  All replies are returned including sequestered replies.
     */

    @OSID @Override
    public void useUnsequesteredReplyView() {
        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            session.useUnsequesteredReplyView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Reply</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Reply</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Reply</code> and
     *  retained for compatibility.
     *
     *  @param  replyId <code>Id</code> of the
     *          <code>Reply</code>
     *  @return the reply
     *  @throws org.osid.NotFoundException <code>replyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>replyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Reply getReply(org.osid.id.Id replyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            try {
                return (session.getReply(replyId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(replyId + " not found");
    }


    /**
     *  Gets a <code>ReplyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  replies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Replies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  replyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Reply</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>replyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByIds(org.osid.id.IdList replyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.forum.reply.MutableReplyList ret = new net.okapia.osid.jamocha.forum.reply.MutableReplyList();

        try (org.osid.id.IdList ids = replyIds) {
            while (ids.hasNext()) {
                ret.addReply(getReply(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ReplyList</code> corresponding to the given
     *  reply genus <code>Type</code> which does not include
     *  replies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  replyGenusType a reply genus type 
     *  @return the returned <code>Reply</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>replyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByGenusType(org.osid.type.Type replyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesByGenusType(replyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ReplyList</code> corresponding to the given
     *  reply genus <code>Type</code> and include any additional
     *  replies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  replyGenusType a reply genus type 
     *  @return the returned <code>Reply</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>replyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByParentGenusType(org.osid.type.Type replyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesByParentGenusType(replyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ReplyList</code> containing the given
     *  reply record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  replyRecordType a reply record type 
     *  @return the returned <code>Reply</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByRecordType(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesByRecordType(replyRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all replies corresponding to the given date
     *  range inclusive.
     *
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>ReplyList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDate(org.osid.calendaring.DateTime from,
                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all replies corresponding to a post <code>
     *  Id. </code>
     *
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the <code>Id</code> of the post
     *  @return the returned <code>ReplyList</code>
     *  @throws org.osid.NullArgumentException <code>postId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesForPost(org.osid.id.Id postId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesForPost(postId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all replies corresponding to post
     *  <code>Id</code> in the given daterange inclusive.
     *
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replis that are accessible through this session.
     *
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the <code>Id</code> of the post
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>ReplyList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>postId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDateForPost(org.osid.id.Id postId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesByDateForPost(postId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all replies corresponding to a poster.
     *
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  resourceId the resource <code>Id</code>
     *  @return the returned <code>ReplyList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesForPoster(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesForPoster(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all replies corresponding to a post
     *  <code>Id</code> for the given poster within the date range
     *  inclusive.
     *
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  resourceId the resource <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>ReplyList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDateForPoster(org.osid.id.Id resourceId,
                                                              org.osid.calendaring.DateTime from,
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesByDateForPoster(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all replies corresponding to a post
     *  <code>Id</code> and poster.
     *
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the <code>Id</code> of the post
     *  @param  resourceId the resource <code>Id</code>
     *  @return the returned <code>ReplyList</code>
     *  @throws org.osid.NullArgumentException <code>postId</code> or
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesForPostAndPoster(org.osid.id.Id postId,
                                                               org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesForPostAndPoster(postId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all replies corresponding to a post
     *  <code>Id</code> and poster within the given daterange
     *  incluisve.
     *
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the <code>Id</code> of the post
     *  @param  resourceId the resource <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>ReplyList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>postId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDateForPostAndPoster(org.osid.id.Id postId,
                                                                     org.osid.id.Id resourceId,
                                                                     org.osid.calendaring.DateTime from,
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getRepliesByDateForPostAndPoster(postId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Replies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Replies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getReplies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList ret = getReplyList();

        for (org.osid.forum.ReplyLookupSession session : getSessions()) {
            ret.addReplyList(session.getReplies());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.forum.reply.FederatingReplyList getReplyList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.forum.reply.ParallelReplyList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.forum.reply.CompositeReplyList());
        }
    }
}
