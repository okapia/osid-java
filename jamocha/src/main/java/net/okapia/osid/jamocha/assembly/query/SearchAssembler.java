//
// SearchAssembler.java
//
//     An interface for assembling a search.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query;


/**
 *  A utility interface for assembling search.
 */

public interface SearchAssembler {

    
    /**
     *  Specify a limit of results.
     *
     *  @param start the starting element
     *  @param end the ending element
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> is <code>null</code>
     */

    public void limitResults(long start, long end);


    /**
     *  Gets the start limit of the result set.
     *
     *  @return the start limit, <code>-1</code> if no limit set
     */

    public long getStartLimit();


    /**
     *  Gets the end limit of the result set.
     *
     *  @return the end limit, <code>-1</code> if no limit set
     */

    public long getEndLimit();
}
