//
// BuildingMiter.java
//
//     Defines a Building miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.building;


/**
 *  Defines a <code>Building</code> miter for use with the builders.
 */

public interface BuildingMiter
    extends net.okapia.osid.jamocha.builder.spi.TemporalOsidObjectMiter,
            org.osid.room.Building {


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @throws org.osid.NullArgumentException <code>address</code> is
     *          <code>null</code>
     */

    public void setAddress(org.osid.contact.Address address);


    /**
     *  Sets the official name.
     *
     *  @param name an official name
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          <code>null</code>
     */

    public void setOfficialName(org.osid.locale.DisplayText name);


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setNumber(String number);


    /**
     *  Sets the enclosing building.
     *
     *  @param building an enclosing building
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    public void setEnclosingBuilding(org.osid.room.Building building);


    /**
     *  Adds a subdivision.
     *
     *  @param building a subdivision
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    public void addSubdivision(org.osid.room.Building building);


    /**
     *  Sets all the subdivisions.
     *
     *  @param buildings a collection of subdivisions
     *  @throws org.osid.NullArgumentException <code>buildings</code>
     *          is <code>null</code>
     */

    public void setSubdivisions(java.util.Collection<org.osid.room.Building> buildings);


    /**
     *  Sets the gross area.
     *
     *  @param area a gross area
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    public void setGrossArea(java.math.BigDecimal area);


    /**
     *  Adds a Building record.
     *
     *  @param record a building record
     *  @param recordType the type of building record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addBuildingRecord(org.osid.room.records.BuildingRecord record, org.osid.type.Type recordType);
}       


