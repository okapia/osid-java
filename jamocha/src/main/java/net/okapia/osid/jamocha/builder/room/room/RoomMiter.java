//
// RoomMiter.java
//
//     Defines a Room miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.room;


/**
 *  Defines a <code>Room</code> miter for use with the builders.
 */

public interface RoomMiter
    extends net.okapia.osid.jamocha.builder.spi.TemporalOsidObjectMiter,
            org.osid.room.Room {


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    public void setBuilding(org.osid.room.Building building);


    /**
     *  Sets the floor.
     *
     *  @param floor a floor
     *  @throws org.osid.NullArgumentException <code>floor</code> is
     *          <code>null</code>
     */

    public void setFloor(org.osid.room.Floor floor);


    /**
     *  Sets the enclosing room.
     *
     *  @param room an enclosing room
     *  @throws org.osid.NullArgumentException <code>room</code> is
     *          <code>null</code>
     */

    public void setEnclosingRoom(org.osid.room.Room room);


    /**
     *  Adds a subdivision.
     *
     *  @param room a subdivision
     *  @throws org.osid.NullArgumentException
     *          <code>room</code> is <code>null</code>
     */

    public void addSubdivision(org.osid.room.Room room);


    /**
     *  Sets all the subdivisions.
     *
     *  @param rooms a collection of subdivisions
     *  @throws org.osid.NullArgumentException
     *          <code>rooms</code> is <code>null</code>
     */

    public void setSubdivisions(java.util.Collection<org.osid.room.Room> rooms);


    /**
     *  Sets the designated name.
     *
     *  @param name a designated name
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          <code>null</code>
     */

    public void setDesignatedName(org.osid.locale.DisplayText name);


    /**
     *  Sets the room number.
     *
     *  @param number a room number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setRoomNumber(String number);


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     */

    public void setCode(String code);


    /**
     *  Sets the area.
     *
     *  @param area an area
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    public void setArea(java.math.BigDecimal area);


    /**
     *  Sets the occupancy limit.
     *
     *  @param limit an occupancy limit
     *  @throws org.osid.InvalidArgumentException <code>limit</code>
     *          is negative
     */

    public void setOccupancyLimit(long limit);


    /**
     *  Adds a resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void addResource(org.osid.resource.Resource resource);


    /**
     *  Sets all the resources.
     *
     *  @param resources a collection of resources
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          is <code>null</code>
     */

    public void setResources(java.util.Collection<org.osid.resource.Resource> resources);


    /**
     *  Adds a Room record.
     *
     *  @param record a room record
     *  @param recordType the type of room record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRoomRecord(org.osid.room.records.RoomRecord record, org.osid.type.Type recordType);
}       


