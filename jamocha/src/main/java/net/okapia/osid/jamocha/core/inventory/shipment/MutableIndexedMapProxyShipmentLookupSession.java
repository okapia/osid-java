//
// MutableIndexedMapProxyShipmentLookupSession
//
//    Implements a Shipment lookup service backed by a collection of
//    shipments indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.shipment;


/**
 *  Implements a Shipment lookup service backed by a collection of
 *  shipments. The shipments are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some shipments may be compatible
 *  with more types than are indicated through these shipment
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of shipments can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyShipmentLookupSession
    extends net.okapia.osid.jamocha.core.inventory.shipment.spi.AbstractIndexedMapShipmentLookupSession
    implements org.osid.inventory.shipment.ShipmentLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyShipmentLookupSession} with
     *  no shipment.
     *
     *  @param warehouse the warehouse
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       org.osid.proxy.Proxy proxy) {
        setWarehouse(warehouse);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyShipmentLookupSession} with
     *  a single shipment.
     *
     *  @param warehouse the warehouse
     *  @param  shipment an shipment
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code shipment}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       org.osid.inventory.shipment.Shipment shipment, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putShipment(shipment);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyShipmentLookupSession} using
     *  an array of shipments.
     *
     *  @param warehouse the warehouse
     *  @param  shipments an array of shipments
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code shipments}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       org.osid.inventory.shipment.Shipment[] shipments, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putShipments(shipments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyShipmentLookupSession} using
     *  a collection of shipments.
     *
     *  @param warehouse the warehouse
     *  @param  shipments a collection of shipments
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code shipments}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       java.util.Collection<? extends org.osid.inventory.shipment.Shipment> shipments,
                                                       org.osid.proxy.Proxy proxy) {
        this(warehouse, proxy);
        putShipments(shipments);
        return;
    }

    
    /**
     *  Makes a {@code Shipment} available in this session.
     *
     *  @param  shipment a shipment
     *  @throws org.osid.NullArgumentException {@code shipment{@code 
     *          is {@code null}
     */

    @Override
    public void putShipment(org.osid.inventory.shipment.Shipment shipment) {
        super.putShipment(shipment);
        return;
    }


    /**
     *  Makes an array of shipments available in this session.
     *
     *  @param  shipments an array of shipments
     *  @throws org.osid.NullArgumentException {@code shipments{@code 
     *          is {@code null}
     */

    @Override
    public void putShipments(org.osid.inventory.shipment.Shipment[] shipments) {
        super.putShipments(shipments);
        return;
    }


    /**
     *  Makes collection of shipments available in this session.
     *
     *  @param  shipments a collection of shipments
     *  @throws org.osid.NullArgumentException {@code shipment{@code 
     *          is {@code null}
     */

    @Override
    public void putShipments(java.util.Collection<? extends org.osid.inventory.shipment.Shipment> shipments) {
        super.putShipments(shipments);
        return;
    }


    /**
     *  Removes a Shipment from this session.
     *
     *  @param shipmentId the {@code Id} of the shipment
     *  @throws org.osid.NullArgumentException {@code shipmentId{@code  is
     *          {@code null}
     */

    @Override
    public void removeShipment(org.osid.id.Id shipmentId) {
        super.removeShipment(shipmentId);
        return;
    }    
}
