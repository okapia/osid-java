//
// AbstractIndexedMapAwardLookupSession.java
//
//    A simple framework for providing an Award lookup service
//    backed by a fixed collection of awards with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Award lookup service backed by a
 *  fixed collection of awards. The awards are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some awards may be compatible
 *  with more types than are indicated through these award
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Awards</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAwardLookupSession
    extends AbstractMapAwardLookupSession
    implements org.osid.recognition.AwardLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.recognition.Award> awardsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recognition.Award>());
    private final MultiMap<org.osid.type.Type, org.osid.recognition.Award> awardsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recognition.Award>());


    /**
     *  Makes an <code>Award</code> available in this session.
     *
     *  @param  award an award
     *  @throws org.osid.NullArgumentException <code>award<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAward(org.osid.recognition.Award award) {
        super.putAward(award);

        this.awardsByGenus.put(award.getGenusType(), award);
        
        try (org.osid.type.TypeList types = award.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.awardsByRecord.put(types.getNextType(), award);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an award from this session.
     *
     *  @param awardId the <code>Id</code> of the award
     *  @throws org.osid.NullArgumentException <code>awardId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAward(org.osid.id.Id awardId) {
        org.osid.recognition.Award award;
        try {
            award = getAward(awardId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.awardsByGenus.remove(award.getGenusType());

        try (org.osid.type.TypeList types = award.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.awardsByRecord.remove(types.getNextType(), award);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAward(awardId);
        return;
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  award genus <code>Type</code> which does not include
     *  awards of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known awards or an error results. Otherwise,
     *  the returned list may contain only those awards that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  awardGenusType an award genus type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByGenusType(org.osid.type.Type awardGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.award.ArrayAwardList(this.awardsByGenus.get(awardGenusType)));
    }


    /**
     *  Gets an <code>AwardList</code> containing the given
     *  award record <code>Type</code>. In plenary mode, the
     *  returned list contains all known awards or an error
     *  results. Otherwise, the returned list may contain only those
     *  awards that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  awardRecordType an award record type 
     *  @return the returned <code>award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByRecordType(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.award.ArrayAwardList(this.awardsByRecord.get(awardRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.awardsByGenus.clear();
        this.awardsByRecord.clear();

        super.close();

        return;
    }
}
