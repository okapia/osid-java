//
// MutableIndexedMapProxyStateLookupSession
//
//    Implements a State lookup service backed by a collection of
//    states indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process;


/**
 *  Implements a State lookup service backed by a collection of
 *  states. The states are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some states may be compatible
 *  with more types than are indicated through these state
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of states can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyStateLookupSession
    extends net.okapia.osid.jamocha.core.process.spi.AbstractIndexedMapStateLookupSession
    implements org.osid.process.StateLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyStateLookupSession} with
     *  no state.
     *
     *  @param process the process
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code process} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyStateLookupSession(org.osid.process.Process process,
                                                       org.osid.proxy.Proxy proxy) {
        setProcess(process);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyStateLookupSession} with
     *  a single state.
     *
     *  @param process the process
     *  @param  state an state
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code process},
     *          {@code state}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyStateLookupSession(org.osid.process.Process process,
                                                       org.osid.process.State state, org.osid.proxy.Proxy proxy) {

        this(process, proxy);
        putState(state);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyStateLookupSession} using
     *  an array of states.
     *
     *  @param process the process
     *  @param  states an array of states
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code process},
     *          {@code states}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyStateLookupSession(org.osid.process.Process process,
                                                       org.osid.process.State[] states, org.osid.proxy.Proxy proxy) {

        this(process, proxy);
        putStates(states);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyStateLookupSession} using
     *  a collection of states.
     *
     *  @param process the process
     *  @param  states a collection of states
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code process},
     *          {@code states}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyStateLookupSession(org.osid.process.Process process,
                                                       java.util.Collection<? extends org.osid.process.State> states,
                                                       org.osid.proxy.Proxy proxy) {
        this(process, proxy);
        putStates(states);
        return;
    }

    
    /**
     *  Makes a {@code State} available in this session.
     *
     *  @param  state a state
     *  @throws org.osid.NullArgumentException {@code state{@code 
     *          is {@code null}
     */

    @Override
    public void putState(org.osid.process.State state) {
        super.putState(state);
        return;
    }


    /**
     *  Makes an array of states available in this session.
     *
     *  @param  states an array of states
     *  @throws org.osid.NullArgumentException {@code states{@code 
     *          is {@code null}
     */

    @Override
    public void putStates(org.osid.process.State[] states) {
        super.putStates(states);
        return;
    }


    /**
     *  Makes collection of states available in this session.
     *
     *  @param  states a collection of states
     *  @throws org.osid.NullArgumentException {@code state{@code 
     *          is {@code null}
     */

    @Override
    public void putStates(java.util.Collection<? extends org.osid.process.State> states) {
        super.putStates(states);
        return;
    }


    /**
     *  Removes a State from this session.
     *
     *  @param stateId the {@code Id} of the state
     *  @throws org.osid.NullArgumentException {@code stateId{@code  is
     *          {@code null}
     */

    @Override
    public void removeState(org.osid.id.Id stateId) {
        super.removeState(stateId);
        return;
    }    
}
