//
// AbstractAssemblyBudgetEntryQuery.java
//
//     A BudgetEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.financials.budgeting.budgetentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BudgetEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyBudgetEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.financials.budgeting.BudgetEntryQuery,
               org.osid.financials.budgeting.BudgetEntryQueryInspector,
               org.osid.financials.budgeting.BudgetEntrySearchOrder {

    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBudgetEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBudgetEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the budget <code> Id </code> for this query. 
     *
     *  @param  budgetId a budget <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> budgetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBudgetId(org.osid.id.Id budgetId, boolean match) {
        getAssembler().addIdTerm(getBudgetIdColumn(), budgetId, match);
        return;
    }


    /**
     *  Clears the budget <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBudgetIdTerms() {
        getAssembler().clearTerms(getBudgetIdColumn());
        return;
    }


    /**
     *  Gets the budget <code> Id </code> query terms. 
     *
     *  @return the v <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBudgetIdTerms() {
        return (getAssembler().getIdTerms(getBudgetIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by budget. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBudget(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBudgetColumn(), style);
        return;
    }


    /**
     *  Gets the BudgetId column name.
     *
     * @return the column name
     */

    protected String getBudgetIdColumn() {
        return ("budget_id");
    }


    /**
     *  Tests if a <code> BudgetQuery </code> is available. 
     *
     *  @return <code> true </code> if a budget query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetQuery() {
        return (false);
    }


    /**
     *  Gets the query for a budget. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the payer query 
     *  @throws org.osid.UnimplementedException the budget query 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQuery getBudgetQuery() {
        throw new org.osid.UnimplementedException("supportsBudgetQuery() is false");
    }


    /**
     *  Clears the budget terms. 
     */

    @OSID @Override
    public void clearBudgetTerms() {
        getAssembler().clearTerms(getBudgetColumn());
        return;
    }


    /**
     *  Gets the budget query terms. 
     *
     *  @return the budget query terms 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQueryInspector[] getBudgetTerms() {
        return (new org.osid.financials.budgeting.BudgetQueryInspector[0]);
    }


    /**
     *  Tests if a budget search order is available. 
     *
     *  @return <code> true </code> if a budget search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetSearchOrder() {
        return (false);
    }


    /**
     *  Gets the budget order. 
     *
     *  @return the budget search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSearchOrder getBudgetSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBudgetSearchOrder() is false");
    }


    /**
     *  Gets the Budget column name.
     *
     * @return the column name
     */

    protected String getBudgetColumn() {
        return ("budget");
    }


    /**
     *  Sets the account <code> Id </code> for this query. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAccountId(org.osid.id.Id accountId, boolean match) {
        getAssembler().addIdTerm(getAccountIdColumn(), accountId, match);
        return;
    }


    /**
     *  Clears the account <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAccountIdTerms() {
        getAssembler().clearTerms(getAccountIdColumn());
        return;
    }


    /**
     *  Gets the account <code> Id </code> query terms. 
     *
     *  @return the account <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAccountIdTerms() {
        return (getAssembler().getIdTerms(getAccountIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by account. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAccount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAccountColumn(), style);
        return;
    }


    /**
     *  Gets the AccountId column name.
     *
     * @return the column name
     */

    protected String getAccountIdColumn() {
        return ("account_id");
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAccountQuery() is false");
    }


    /**
     *  Matches entries that have any account set. 
     *
     *  @param  match <code> true </code> to match entries with any account, 
     *          <code> false </code> to match entries with no account 
     */

    @OSID @Override
    public void matchAnyAccount(boolean match) {
        getAssembler().addIdWildcardTerm(getAccountColumn(), match);
        return;
    }


    /**
     *  Clears the account terms. 
     */

    @OSID @Override
    public void clearAccountTerms() {
        getAssembler().clearTerms(getAccountColumn());
        return;
    }


    /**
     *  Gets the account query terms. 
     *
     *  @return the account query terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Tests if an account search order is available. 
     *
     *  @return <code> true </code> if an search account order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountSearchOrder() {
        return (false);
    }


    /**
     *  Gets the account order. 
     *
     *  @return the account search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchOrder getAccountSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAccountSearchOrder() is false");
    }


    /**
     *  Gets the Account column name.
     *
     * @return the column name
     */

    protected String getAccountColumn() {
        return ("account");
    }


    /**
     *  Matches the amount between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency low, 
                            org.osid.financials.Currency high, boolean match) {
        getAssembler().addCurrencyRangeTerm(getAmountColumn(), low, high, match);
        return;
    }


    /**
     *  Matches entries that have any amount set. 
     *
     *  @param  match <code> true </code> to match entries with any amount, 
     *          <code> false </code> to match entries with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getAmountColumn(), match);
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        getAssembler().clearTerms(getAmountColumn());
        return;
    }


    /**
     *  Gets the amount query terms. 
     *
     *  @return the date range query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (getAssembler().getCurrencyRangeTerms(getAmountColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the amount. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAmountColumn(), style);
        return;
    }


    /**
     *  Gets the Amount column name.
     *
     * @return the column name
     */

    protected String getAmountColumn() {
        return ("amount");
    }


    /**
     *  Matches entries that have debit amounts. 
     *
     *  @param  match <code> true </code> to match entries with a debit 
     *          amount, <code> false </code> to match entries with a credit 
     *          amount 
     */

    @OSID @Override
    public void matchDebit(boolean match) {
        getAssembler().addBooleanTerm(getDebitColumn(), match);
        return;
    }


    /**
     *  Clears the debit terms. 
     */

    @OSID @Override
    public void clearDebitTerms() {
        getAssembler().clearTerms(getDebitColumn());
        return;
    }


    /**
     *  Gets the debit query terms. 
     *
     *  @return the debit query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDebitTerms() {
        return (getAssembler().getBooleanTerms(getDebitColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the debit flag. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDebit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDebitColumn(), style);
        return;
    }


    /**
     *  Gets the Debit column name.
     *
     * @return the column name
     */

    protected String getDebitColumn() {
        return ("debit");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match budget 
     *  entries assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this budgetEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  budgetEntryRecordType a budget entry record type 
     *  @return <code>true</code> if the budgetEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type budgetEntryRecordType) {
        for (org.osid.financials.budgeting.records.BudgetEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(budgetEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  budgetEntryRecordType the budget entry record type 
     *  @return the budget entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetEntryQueryRecord getBudgetEntryQueryRecord(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(budgetEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  budgetEntryRecordType the budget entry record type 
     *  @return the budget entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetEntryQueryInspectorRecord getBudgetEntryQueryInspectorRecord(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(budgetEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param budgetEntryRecordType the budget entry record type
     *  @return the budget entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetEntrySearchOrderRecord getBudgetEntrySearchOrderRecord(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(budgetEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this budget entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param budgetEntryQueryRecord the budget entry query record
     *  @param budgetEntryQueryInspectorRecord the budget entry query inspector
     *         record
     *  @param budgetEntrySearchOrderRecord the budget entry search order record
     *  @param budgetEntryRecordType budget entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryQueryRecord</code>,
     *          <code>budgetEntryQueryInspectorRecord</code>,
     *          <code>budgetEntrySearchOrderRecord</code> or
     *          <code>budgetEntryRecordTypebudgetEntry</code> is
     *          <code>null</code>
     */
            
    protected void addBudgetEntryRecords(org.osid.financials.budgeting.records.BudgetEntryQueryRecord budgetEntryQueryRecord, 
                                      org.osid.financials.budgeting.records.BudgetEntryQueryInspectorRecord budgetEntryQueryInspectorRecord, 
                                      org.osid.financials.budgeting.records.BudgetEntrySearchOrderRecord budgetEntrySearchOrderRecord, 
                                      org.osid.type.Type budgetEntryRecordType) {

        addRecordType(budgetEntryRecordType);

        nullarg(budgetEntryQueryRecord, "budget entry query record");
        nullarg(budgetEntryQueryInspectorRecord, "budget entry query inspector record");
        nullarg(budgetEntrySearchOrderRecord, "budget entry search odrer record");

        this.queryRecords.add(budgetEntryQueryRecord);
        this.queryInspectorRecords.add(budgetEntryQueryInspectorRecord);
        this.searchOrderRecords.add(budgetEntrySearchOrderRecord);
        
        return;
    }
}
