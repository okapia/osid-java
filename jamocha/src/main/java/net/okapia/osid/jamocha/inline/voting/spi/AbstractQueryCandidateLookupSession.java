//
// AbstractQueryCandidateLookupSession.java
//
//    An inline adapter that maps a CandidateLookupSession to
//    a CandidateQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CandidateLookupSession to
 *  a CandidateQuerySession.
 */

public abstract class AbstractQueryCandidateLookupSession
    extends net.okapia.osid.jamocha.voting.spi.AbstractCandidateLookupSession
    implements org.osid.voting.CandidateLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.voting.CandidateQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCandidateLookupSession.
     *
     *  @param querySession the underlying candidate query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCandidateLookupSession(org.osid.voting.CandidateQuerySession querySession) {
        nullarg(querySession, "candidate query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Polls</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform <code>Candidate</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCandidates() {
        return (this.session.canSearchCandidates());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include candidates in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only candidates whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCandidateView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All candidates of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCandidateView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Candidate</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Candidate</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Candidate</code> and
     *  retained for compatibility.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateId <code>Id</code> of the
     *          <code>Candidate</code>
     *  @return the candidate
     *  @throws org.osid.NotFoundException <code>candidateId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>candidateId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Candidate getCandidate(org.osid.id.Id candidateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchId(candidateId, true);
        org.osid.voting.CandidateList candidates = this.session.getCandidatesByQuery(query);
        if (candidates.hasNext()) {
            return (candidates.getNextCandidate());
        } 
        
        throw new org.osid.NotFoundException(candidateId + " not found");
    }


    /**
     *  Gets a <code>CandidateList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  candidates specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Candidates</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Candidate</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>candidateIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByIds(org.osid.id.IdList candidateIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();

        try (org.osid.id.IdList ids = candidateIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets a <code>CandidateList</code> corresponding to the given
     *  candidate genus <code>Type</code> which does not include
     *  candidates of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateGenusType a candidate genus type 
     *  @return the returned <code>Candidate</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>candidateGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByGenusType(org.osid.type.Type candidateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchGenusType(candidateGenusType, true);
        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets a <code>CandidateList</code> corresponding to the given
     *  candidate genus <code>Type</code> and include any additional
     *  candidates with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateGenusType a candidate genus type 
     *  @return the returned <code>Candidate</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>candidateGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByParentGenusType(org.osid.type.Type candidateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchParentGenusType(candidateGenusType, true);
        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets a <code>CandidateList</code> containing the given
     *  candidate record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateRecordType a candidate record type 
     *  @return the returned <code>Candidate</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>candidateRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByRecordType(org.osid.type.Type candidateRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchRecordType(candidateRecordType, true);
        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets a <code>CandidateList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *  
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Candidate</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCandidatesByQuery(query));
    }
        

    /**
     *  Gets a list of candidates corresponding to a race
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.CandidateList getCandidatesForRace(org.osid.id.Id raceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchRaceId(raceId, true);
        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets a list of candidates corresponding to a race
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRaceOnDate(org.osid.id.Id raceId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchRaceId(raceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets a list of candidates corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.CandidateList getCandidatesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets a list of candidates corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForResourceOnDate(org.osid.id.Id resourceId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets a list of candidates corresponding to race and resource
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRaceAndResource(org.osid.id.Id raceId,
                                                                         org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchRaceId(raceId, true);
        query.matchResourceId(resourceId, true);
        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets a list of candidates corresponding to race and resource
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible
     *  through this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CandidateList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRaceAndResourceOnDate(org.osid.id.Id raceId,
                                                                               org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.CandidateQuery query = getQuery();
        query.matchRaceId(raceId, true);
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCandidatesByQuery(query));
    }

    
    /**
     *  Gets all <code>Candidates</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Candidates</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidates()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.CandidateQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCandidatesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.voting.CandidateQuery getQuery() {
        org.osid.voting.CandidateQuery query = this.session.getCandidateQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
