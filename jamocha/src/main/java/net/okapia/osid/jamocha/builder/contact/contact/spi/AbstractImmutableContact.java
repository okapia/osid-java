//
// AbstractImmutableContact.java
//
//     Wraps a mutable Contact to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.contact.contact.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Contact</code> to hide modifiers. This
 *  wrapper provides an immutized Contact from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying contact whose state changes are visible.
 */

public abstract class AbstractImmutableContact
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.contact.Contact {

    private final org.osid.contact.Contact contact;


    /**
     *  Constructs a new <code>AbstractImmutableContact</code>.
     *
     *  @param contact the contact to immutablize
     *  @throws org.osid.NullArgumentException <code>contact</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableContact(org.osid.contact.Contact contact) {
        super(contact);
        this.contact = contact;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the reference. 
     *
     *  @return the reference <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        return (this.contact.getReferenceId());
    }


    /**
     *  Gets the <code> Id </code> of the addressee. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAddresseeId() {
        return (this.contact.getAddresseeId());
    }


    /**
     *  Gets the addressee. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getAddressee()
        throws org.osid.OperationFailedException {

        return (this.contact.getAddressee());
    }


    /**
     *  Gets the <code> Id </code> of the subscriber's address. 
     *
     *  @return the subscriber <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAddressId() {
        return (this.contact.getAddressId());
    }


    /**
     *  Gets the subscriber's address. 
     *
     *  @return the subscriber's address. 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress()
        throws org.osid.OperationFailedException {

        return (this.contact.getAddress());
    }


    /**
     *  Gets the contact record corresponding to the given <code> Contact 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> contactRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(contactRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  contactRecordType the type of contact record to retrieve 
     *  @return the contact record 
     *  @throws org.osid.NullArgumentException <code> contactRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(contactRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.records.ContactRecord getContactRecord(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException {

        return (this.contact.getContactRecord(contactRecordType));
    }
}

