//
// AbstractMapRaceProcessorLookupSession
//
//    A simple framework for providing a RaceProcessor lookup service
//    backed by a fixed collection of race processors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a RaceProcessor lookup service backed by a
 *  fixed collection of race processors. The race processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RaceProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRaceProcessorLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractRaceProcessorLookupSession
    implements org.osid.voting.rules.RaceProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.rules.RaceProcessor> raceProcessors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.rules.RaceProcessor>());


    /**
     *  Makes a <code>RaceProcessor</code> available in this session.
     *
     *  @param  raceProcessor a race processor
     *  @throws org.osid.NullArgumentException <code>raceProcessor<code>
     *          is <code>null</code>
     */

    protected void putRaceProcessor(org.osid.voting.rules.RaceProcessor raceProcessor) {
        this.raceProcessors.put(raceProcessor.getId(), raceProcessor);
        return;
    }


    /**
     *  Makes an array of race processors available in this session.
     *
     *  @param  raceProcessors an array of race processors
     *  @throws org.osid.NullArgumentException <code>raceProcessors<code>
     *          is <code>null</code>
     */

    protected void putRaceProcessors(org.osid.voting.rules.RaceProcessor[] raceProcessors) {
        putRaceProcessors(java.util.Arrays.asList(raceProcessors));
        return;
    }


    /**
     *  Makes a collection of race processors available in this session.
     *
     *  @param  raceProcessors a collection of race processors
     *  @throws org.osid.NullArgumentException <code>raceProcessors<code>
     *          is <code>null</code>
     */

    protected void putRaceProcessors(java.util.Collection<? extends org.osid.voting.rules.RaceProcessor> raceProcessors) {
        for (org.osid.voting.rules.RaceProcessor raceProcessor : raceProcessors) {
            this.raceProcessors.put(raceProcessor.getId(), raceProcessor);
        }

        return;
    }


    /**
     *  Removes a RaceProcessor from this session.
     *
     *  @param  raceProcessorId the <code>Id</code> of the race processor
     *  @throws org.osid.NullArgumentException <code>raceProcessorId<code> is
     *          <code>null</code>
     */

    protected void removeRaceProcessor(org.osid.id.Id raceProcessorId) {
        this.raceProcessors.remove(raceProcessorId);
        return;
    }


    /**
     *  Gets the <code>RaceProcessor</code> specified by its <code>Id</code>.
     *
     *  @param  raceProcessorId <code>Id</code> of the <code>RaceProcessor</code>
     *  @return the raceProcessor
     *  @throws org.osid.NotFoundException <code>raceProcessorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>raceProcessorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessor getRaceProcessor(org.osid.id.Id raceProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.rules.RaceProcessor raceProcessor = this.raceProcessors.get(raceProcessorId);
        if (raceProcessor == null) {
            throw new org.osid.NotFoundException("raceProcessor not found: " + raceProcessorId);
        }

        return (raceProcessor);
    }


    /**
     *  Gets all <code>RaceProcessors</code>. In plenary mode, the returned
     *  list contains all known raceProcessors or an error
     *  results. Otherwise, the returned list may contain only those
     *  raceProcessors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>RaceProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceprocessor.ArrayRaceProcessorList(this.raceProcessors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.raceProcessors.clear();
        super.close();
        return;
    }
}
