//
// AbstractMapBillingLookupSession
//
//    A simple framework for providing a Billing lookup service
//    backed by a fixed collection of billings.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Billing lookup service backed by a
 *  fixed collection of billings. The billings are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Billings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBillingLookupSession
    extends net.okapia.osid.jamocha.acknowledgement.spi.AbstractBillingLookupSession
    implements org.osid.acknowledgement.BillingLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.acknowledgement.Billing> billings = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.acknowledgement.Billing>());


    /**
     *  Makes a <code>Billing</code> available in this session.
     *
     *  @param  billing a billing
     *  @throws org.osid.NullArgumentException <code>billing<code>
     *          is <code>null</code>
     */

    protected void putBilling(org.osid.acknowledgement.Billing billing) {
        this.billings.put(billing.getId(), billing);
        return;
    }


    /**
     *  Makes an array of billings available in this session.
     *
     *  @param  billings an array of billings
     *  @throws org.osid.NullArgumentException <code>billings<code>
     *          is <code>null</code>
     */

    protected void putBillings(org.osid.acknowledgement.Billing[] billings) {
        putBillings(java.util.Arrays.asList(billings));
        return;
    }


    /**
     *  Makes a collection of billings available in this session.
     *
     *  @param  billings a collection of billings
     *  @throws org.osid.NullArgumentException <code>billings<code>
     *          is <code>null</code>
     */

    protected void putBillings(java.util.Collection<? extends org.osid.acknowledgement.Billing> billings) {
        for (org.osid.acknowledgement.Billing billing : billings) {
            this.billings.put(billing.getId(), billing);
        }

        return;
    }


    /**
     *  Removes a Billing from this session.
     *
     *  @param  billingId the <code>Id</code> of the billing
     *  @throws org.osid.NullArgumentException <code>billingId<code> is
     *          <code>null</code>
     */

    protected void removeBilling(org.osid.id.Id billingId) {
        this.billings.remove(billingId);
        return;
    }


    /**
     *  Gets the <code>Billing</code> specified by its <code>Id</code>.
     *
     *  @param  billingId <code>Id</code> of the <code>Billing</code>
     *  @return the billing
     *  @throws org.osid.NotFoundException <code>billingId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>billingId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Billing getBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.acknowledgement.Billing billing = this.billings.get(billingId);
        if (billing == null) {
            throw new org.osid.NotFoundException("billing not found: " + billingId);
        }

        return (billing);
    }


    /**
     *  Gets all <code>Billings</code>. In plenary mode, the returned
     *  list contains all known billings or an error
     *  results. Otherwise, the returned list may contain only those
     *  billings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Billings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.acknowledgement.billing.ArrayBillingList(this.billings.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.billings.clear();
        super.close();
        return;
    }
}
