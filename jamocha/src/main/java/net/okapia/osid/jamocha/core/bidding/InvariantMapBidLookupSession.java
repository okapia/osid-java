//
// InvariantMapBidLookupSession
//
//    Implements a Bid lookup service backed by a fixed collection of
//    bids.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding;


/**
 *  Implements a Bid lookup service backed by a fixed
 *  collection of bids. The bids are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapBidLookupSession
    extends net.okapia.osid.jamocha.core.bidding.spi.AbstractMapBidLookupSession
    implements org.osid.bidding.BidLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapBidLookupSession</code> with no
     *  bids.
     *  
     *  @param auctionHouse the auction house
     *  @throws org.osid.NullArgumnetException {@code auctionHouse} is
     *          {@code null}
     */

    public InvariantMapBidLookupSession(org.osid.bidding.AuctionHouse auctionHouse) {
        setAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBidLookupSession</code> with a single
     *  bid.
     *  
     *  @param auctionHouse the auction house
     *  @param bid a single bid
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code bid} is <code>null</code>
     */

      public InvariantMapBidLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               org.osid.bidding.Bid bid) {
        this(auctionHouse);
        putBid(bid);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBidLookupSession</code> using an array
     *  of bids.
     *  
     *  @param auctionHouse the auction house
     *  @param bids an array of bids
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code bids} is <code>null</code>
     */

      public InvariantMapBidLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               org.osid.bidding.Bid[] bids) {
        this(auctionHouse);
        putBids(bids);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBidLookupSession</code> using a
     *  collection of bids.
     *
     *  @param auctionHouse the auction house
     *  @param bids a collection of bids
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code bids} is <code>null</code>
     */

      public InvariantMapBidLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               java.util.Collection<? extends org.osid.bidding.Bid> bids) {
        this(auctionHouse);
        putBids(bids);
        return;
    }
}
