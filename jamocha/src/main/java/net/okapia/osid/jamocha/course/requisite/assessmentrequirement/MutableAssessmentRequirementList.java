//
// MutableAssessmentRequirementList.java
//
//     Implements an AssessmentRequirementList. This list allows AssessmentRequirements to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.assessmentrequirement;


/**
 *  <p>Implements an AssessmentRequirementList. This list allows AssessmentRequirements to be
 *  added after this assessmentRequirement has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this assessmentRequirement must
 *  invoke <code>eol()</code> when there are no more assessmentRequirements to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>AssessmentRequirementList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more assessmentRequirements are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more assessmentRequirements to be added.</p>
 */

public final class MutableAssessmentRequirementList
    extends net.okapia.osid.jamocha.course.requisite.assessmentrequirement.spi.AbstractMutableAssessmentRequirementList
    implements org.osid.course.requisite.AssessmentRequirementList {


    /**
     *  Creates a new empty <code>MutableAssessmentRequirementList</code>.
     */

    public MutableAssessmentRequirementList() {
        super();
    }


    /**
     *  Creates a new <code>MutableAssessmentRequirementList</code>.
     *
     *  @param assessmentRequirement an <code>AssessmentRequirement</code>
     *  @throws org.osid.NullArgumentException <code>assessmentRequirement</code>
     *          is <code>null</code>
     */

    public MutableAssessmentRequirementList(org.osid.course.requisite.AssessmentRequirement assessmentRequirement) {
        super(assessmentRequirement);
        return;
    }


    /**
     *  Creates a new <code>MutableAssessmentRequirementList</code>.
     *
     *  @param array an array of assessmentrequirements
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableAssessmentRequirementList(org.osid.course.requisite.AssessmentRequirement[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableAssessmentRequirementList</code>.
     *
     *  @param collection a java.util.Collection of assessmentrequirements
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableAssessmentRequirementList(java.util.Collection<org.osid.course.requisite.AssessmentRequirement> collection) {
        super(collection);
        return;
    }
}
