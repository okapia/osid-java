//
// DemographicEnablerFilterList.java
//
//     Implements a filtering DemographicEnablerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.resource.demographic.demographicenabler;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering DemographicEnablerList.
 */

public final class DemographicEnablerFilterList
    extends net.okapia.osid.jamocha.inline.filter.resource.demographic.demographicenabler.spi.AbstractDemographicEnablerFilterList
    implements org.osid.resource.demographic.DemographicEnablerList,
               DemographicEnablerFilter {

    private final DemographicEnablerFilter filter;


    /**
     *  Creates a new <code>DemographicEnablerFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>DemographicEnablerList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public DemographicEnablerFilterList(DemographicEnablerFilter filter, org.osid.resource.demographic.DemographicEnablerList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters DemographicEnablers.
     *
     *  @param demographicEnabler the demographic enabler to filter
     *  @return <code>true</code> if the demographic enabler passes the filter,
     *          <code>false</code> if the demographic enabler should be filtered
     */

    @Override
    public boolean pass(org.osid.resource.demographic.DemographicEnabler demographicEnabler) {
        return (this.filter.pass(demographicEnabler));
    }
}
