//
// AbstractIndexedMapCredentialLookupSession.java
//
//    A simple framework for providing a Credential lookup service
//    backed by a fixed collection of credentials with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Credential lookup service backed by a
 *  fixed collection of credentials. The credentials are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some credentials may be compatible
 *  with more types than are indicated through these credential
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Credentials</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCredentialLookupSession
    extends AbstractMapCredentialLookupSession
    implements org.osid.course.program.CredentialLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.program.Credential> credentialsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.program.Credential>());
    private final MultiMap<org.osid.type.Type, org.osid.course.program.Credential> credentialsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.program.Credential>());


    /**
     *  Makes a <code>Credential</code> available in this session.
     *
     *  @param  credential a credential
     *  @throws org.osid.NullArgumentException <code>credential<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCredential(org.osid.course.program.Credential credential) {
        super.putCredential(credential);

        this.credentialsByGenus.put(credential.getGenusType(), credential);
        
        try (org.osid.type.TypeList types = credential.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.credentialsByRecord.put(types.getNextType(), credential);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a credential from this session.
     *
     *  @param credentialId the <code>Id</code> of the credential
     *  @throws org.osid.NullArgumentException <code>credentialId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCredential(org.osid.id.Id credentialId) {
        org.osid.course.program.Credential credential;
        try {
            credential = getCredential(credentialId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.credentialsByGenus.remove(credential.getGenusType());

        try (org.osid.type.TypeList types = credential.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.credentialsByRecord.remove(types.getNextType(), credential);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCredential(credentialId);
        return;
    }


    /**
     *  Gets a <code>CredentialList</code> corresponding to the given
     *  credential genus <code>Type</code> which does not include
     *  credentials of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known credentials or an error results. Otherwise,
     *  the returned list may contain only those credentials that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  credentialGenusType a credential genus type 
     *  @return the returned <code>Credential</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByGenusType(org.osid.type.Type credentialGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.credential.ArrayCredentialList(this.credentialsByGenus.get(credentialGenusType)));
    }


    /**
     *  Gets a <code>CredentialList</code> containing the given
     *  credential record <code>Type</code>. In plenary mode, the
     *  returned list contains all known credentials or an error
     *  results. Otherwise, the returned list may contain only those
     *  credentials that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  credentialRecordType a credential record type 
     *  @return the returned <code>credential</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByRecordType(org.osid.type.Type credentialRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.credential.ArrayCredentialList(this.credentialsByRecord.get(credentialRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.credentialsByGenus.clear();
        this.credentialsByRecord.clear();

        super.close();

        return;
    }
}
