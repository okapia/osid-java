//
// CompositeLeaseBatchFormList.java
//
//     Implements a LeaseBatchFormList. This list allows 
//     LeaseBatchFormLists to be added in serial. This is not as 
//     efficient as the ParallelLeaseBatchFormList but 
//     preserves the ordering.
//
//
// Tom Coppeto
// Okapia
// 17 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.room.squatting.batch.leasebatchform;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a <code>LeaseBatchFormList</code>. This list allows
 *  LeaseBatchFormLists to be added a bit at a time and are read off in
 *  serial. <code>noMore</code> must be invoked when all the lists
 *  have been added.
 *
 *  Two error handlers are offered. A hard error throws an exception
 *  for any error received by an underlying list. A soft error ignores
 *  errors in underlying lists and proceeds to the next list.
 */

public class CompositeLeaseBatchFormList
    extends net.okapia.osid.jamocha.room.squatting.batch.leasebatchform.spi.AbstractLeaseBatchFormList
    implements org.osid.room.squatting.batch.LeaseBatchFormList,
               net.okapia.osid.jamocha.adapter.federator.room.squatting.batch.leasebatchform.FederatingLeaseBatchFormList {

    private final java.util.List<org.osid.room.squatting.batch.LeaseBatchFormList> lists = java.util.Collections.synchronizedList(new java.util.LinkedList<org.osid.room.squatting.batch.LeaseBatchFormList>());
    private org.osid.room.squatting.batch.LeaseBatchFormList active;

    private boolean soft = false;
    private boolean eol = false;


    /**
     *  Creates a new empty <code>CompositeLeaseBatchFormList</code>. The
     *  underlying errors fail hard.
     */

    public CompositeLeaseBatchFormList() {
        return;
    }


    /**
     *  Creates a new empty <code>CompositeLeaseBatchFormList</code>.
     *
     *  @param soft <code>true</code> to fail soft, <code>false</code>
     *         to fail hard
     */

    public CompositeLeaseBatchFormList(boolean soft) {
        this.soft = soft;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        primeList();
        if (this.active == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *  
     *  This method does not imply asynchronous usage. All OSID
     *  methods may block.
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        primeList();
        if (this.active == null) {
            return (0);
        } else {
            return (this.active.available());
        }
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *  
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        long skipped = 0;

        while (skipped < n) {
            primeList();
            if (this.active == null) {
                return;
            }
            
            long s = this.active.available();
            if ((s + skipped) > n) {
                s = n - skipped;
            }
                        
            this.active.skip(s);
            skipped += s;
        }

        return;
    }


    /**
     *  Gets the next <code> LeaseBatchForm </code> in this list.
     *
     *  @return the next <code> LeaseBatchForm </code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code> LeaseBatchForm </code> is available
     *          before calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.squatting.batch.LeaseBatchForm getNextLeaseBatchForm()
        throws org.osid.OperationFailedException {

        while (true) {
            primeList();
            if (this.active != null) {
                try {
                    return (this.active.getNextLeaseBatchForm());
                } catch (org.osid.OperationFailedException ofe) {
                    if (!this.soft) {
                        throw ofe;
                    }
                }
            } else {
                throw new org.osid.OperationFailedException("unable to get a next element");
            }
        }
    }

        
    /**
     *  Gets the next set of <code> LeaseBatchForm </code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param n the number of <code> LeaseBatchForm </code> elements
     *          requested which must be less than or equal to <code>
     *          available() </code>
     *  @return an array of <code> LeaseBatchForm </code> elements. <code>
     *          </code> The length of the array is less than or equal
     *          to the number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is
     *          less than zero
     */

    @OSID @Override
    public org.osid.room.squatting.batch.LeaseBatchForm[] getNextLeaseBatchForms(long n)
        throws org.osid.OperationFailedException {
        
        java.util.List<org.osid.room.squatting.batch.LeaseBatchForm> ret = new java.util.ArrayList<>((int) n);
        long count = 0;

        while (count < n) {
            primeList();
            if (this.active == null) {
                break;
            }

            try {
                for (org.osid.room.squatting.batch.LeaseBatchForm leaseBatchForm : this.active.getNextLeaseBatchForms(this.active.available())) {
                    ret.add(leaseBatchForm);
                    ++count;
                }
            } catch (org.osid.OperationFailedException ofe) {
                if (!this.soft) {
                    throw ofe;
                }
            }
        }

        return (ret.toArray(new org.osid.room.squatting.batch.LeaseBatchForm[ret.size()]));      
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {

        for (org.osid.room.squatting.batch.LeaseBatchFormList list : this.lists) {
            try {
                list.close();
            } catch (Exception e) {}
        }

        this.active  = null;
        this.eol     = true;
        this.lists.clear();

        return;
    }

        
    /**
     *  Adds a <code>LeaseBatchFormList</code> to this
     *  <code>LeaseBatchFormList</code>.
     *
     *  @param leaseBatchFormList the <code>LeaseBatchFormList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>leaseBatchFormList</code>
     *          is <code>null</code>
     */

    @Override
    public void addLeaseBatchFormList(org.osid.room.squatting.batch.LeaseBatchFormList leaseBatchFormList) {
        nullarg(leaseBatchFormList, "leasebatchform list");

        synchronized (this.lists) {
            this.lists.add(leaseBatchFormList);
            this.lists.notifyAll();
        }

        return;
    }


    /**
     *  Adds a collection of <code>LeaseBatchFormLists</code> to this
     *  <code>LeaseBatchFormList</code>.
     *
     *  @param leaseBatchFormLists the <code>LeaseBatchFormList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>leaseBatchFormLists</code> is <code>null</code>
     */

    @Override
    public void addLeaseBatchFormLists(java.util.Collection<org.osid.room.squatting.batch.LeaseBatchFormList> leaseBatchFormLists) {
        nullarg(leaseBatchFormLists, "leasebatchform lists");

        for (org.osid.room.squatting.batch.LeaseBatchFormList list : leaseBatchFormLists) {
            addLeaseBatchFormList(list);
        }

        return;
    }


    /**
     *  No more. Invoked when no more lists are to be added.
     */

    @Override
    public void noMore() {
        this.eol = true;
        synchronized (this.lists) {
            this.lists.notifyAll();
        }

        return;
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    @Override
    public boolean isOperational() {
        if ((this.lists.size() == 0) && this.eol) {
            return (false);
        }

        return (true);
    }


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty() {
        if (this.lists.size() == 0) {
            return (true);
        }

        return (false);
    }


    private void primeList() {
        while (true) {
            if (this.active == null) {
                if ((this.lists.size() == 0) && (this.eol)) {
                    return;
                }
                
                this.active = this.lists.get(0);
            }
            
            if (this.active.hasNext()) {
                return;
            }
            
            this.lists.remove(0);
            this.active = null;
            
            synchronized (this.lists) {                 
                if (!this.eol && (this.lists.size() == 0)) {
                    try {
                        this.lists.wait();
                    } catch (InterruptedException ie) { }
                }
            }
        }
    }
}
