//
// MutableMapAwardLookupSession
//
//    Implements an Award lookup service backed by a collection of
//    awards that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition;


/**
 *  Implements an Award lookup service backed by a collection of
 *  awards. The awards are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of awards can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapAwardLookupSession
    extends net.okapia.osid.jamocha.core.recognition.spi.AbstractMapAwardLookupSession
    implements org.osid.recognition.AwardLookupSession {


    /**
     *  Constructs a new {@code MutableMapAwardLookupSession}
     *  with no awards.
     *
     *  @param academy the academy
     *  @throws org.osid.NullArgumentException {@code academy} is
     *          {@code null}
     */

      public MutableMapAwardLookupSession(org.osid.recognition.Academy academy) {
        setAcademy(academy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAwardLookupSession} with a
     *  single award.
     *
     *  @param academy the academy  
     *  @param award an award
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code award} is {@code null}
     */

    public MutableMapAwardLookupSession(org.osid.recognition.Academy academy,
                                           org.osid.recognition.Award award) {
        this(academy);
        putAward(award);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAwardLookupSession}
     *  using an array of awards.
     *
     *  @param academy the academy
     *  @param awards an array of awards
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code awards} is {@code null}
     */

    public MutableMapAwardLookupSession(org.osid.recognition.Academy academy,
                                           org.osid.recognition.Award[] awards) {
        this(academy);
        putAwards(awards);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAwardLookupSession}
     *  using a collection of awards.
     *
     *  @param academy the academy
     *  @param awards a collection of awards
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code awards} is {@code null}
     */

    public MutableMapAwardLookupSession(org.osid.recognition.Academy academy,
                                           java.util.Collection<? extends org.osid.recognition.Award> awards) {

        this(academy);
        putAwards(awards);
        return;
    }

    
    /**
     *  Makes an {@code Award} available in this session.
     *
     *  @param award an award
     *  @throws org.osid.NullArgumentException {@code award{@code  is
     *          {@code null}
     */

    @Override
    public void putAward(org.osid.recognition.Award award) {
        super.putAward(award);
        return;
    }


    /**
     *  Makes an array of awards available in this session.
     *
     *  @param awards an array of awards
     *  @throws org.osid.NullArgumentException {@code awards{@code 
     *          is {@code null}
     */

    @Override
    public void putAwards(org.osid.recognition.Award[] awards) {
        super.putAwards(awards);
        return;
    }


    /**
     *  Makes collection of awards available in this session.
     *
     *  @param awards a collection of awards
     *  @throws org.osid.NullArgumentException {@code awards{@code  is
     *          {@code null}
     */

    @Override
    public void putAwards(java.util.Collection<? extends org.osid.recognition.Award> awards) {
        super.putAwards(awards);
        return;
    }


    /**
     *  Removes an Award from this session.
     *
     *  @param awardId the {@code Id} of the award
     *  @throws org.osid.NullArgumentException {@code awardId{@code 
     *          is {@code null}
     */

    @Override
    public void removeAward(org.osid.id.Id awardId) {
        super.removeAward(awardId);
        return;
    }    
}
