//
// AbstractAdapterAddressBookLookupSession.java
//
//    An AddressBook lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.contact.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AddressBook lookup session adapter.
 */

public abstract class AbstractAdapterAddressBookLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.contact.AddressBookLookupSession {

    private final org.osid.contact.AddressBookLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAddressBookLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAddressBookLookupSession(org.osid.contact.AddressBookLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code AddressBook} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAddressBooks() {
        return (this.session.canLookupAddressBooks());
    }


    /**
     *  A complete view of the {@code AddressBook} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAddressBookView() {
        this.session.useComparativeAddressBookView();
        return;
    }


    /**
     *  A complete view of the {@code AddressBook} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAddressBookView() {
        this.session.usePlenaryAddressBookView();
        return;
    }

     
    /**
     *  Gets the {@code AddressBook} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AddressBook} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AddressBook} and
     *  retained for compatibility.
     *
     *  @param addressBookId {@code Id} of the {@code AddressBook}
     *  @return the address book
     *  @throws org.osid.NotFoundException {@code addressBookId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code addressBookId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressBook(addressBookId));
    }


    /**
     *  Gets an {@code AddressBookList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  addressBooks specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AddressBooks} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  addressBookIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AddressBook} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code addressBookIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByIds(org.osid.id.IdList addressBookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressBooksByIds(addressBookIds));
    }


    /**
     *  Gets an {@code AddressBookList} corresponding to the given
     *  address book genus {@code Type} which does not include
     *  address books of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressBookGenusType an addressBook genus type 
     *  @return the returned {@code AddressBook} list
     *  @throws org.osid.NullArgumentException
     *          {@code addressBookGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByGenusType(org.osid.type.Type addressBookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressBooksByGenusType(addressBookGenusType));
    }


    /**
     *  Gets an {@code AddressBookList} corresponding to the given
     *  address book genus {@code Type} and include any additional
     *  address books with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressBookGenusType an addressBook genus type 
     *  @return the returned {@code AddressBook} list
     *  @throws org.osid.NullArgumentException
     *          {@code addressBookGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByParentGenusType(org.osid.type.Type addressBookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressBooksByParentGenusType(addressBookGenusType));
    }


    /**
     *  Gets an {@code AddressBookList} containing the given
     *  address book record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressBookRecordType an addressBook record type 
     *  @return the returned {@code AddressBook} list
     *  @throws org.osid.NullArgumentException
     *          {@code addressBookRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByRecordType(org.osid.type.Type addressBookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressBooksByRecordType(addressBookRecordType));
    }


    /**
     *  Gets an {@code AddressBookList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code AddressBook} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressBooksByProvider(resourceId));
    }


    /**
     *  Gets all {@code AddressBooks}. 
     *
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code AddressBooks} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressBooks());
    }
}
