//
// AbstractIndexedMapDepotLookupSession.java
//
//    A simple framework for providing a Depot lookup service
//    backed by a fixed collection of depots with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Depot lookup service backed by a
 *  fixed collection of depots. The depots are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some depots may be compatible
 *  with more types than are indicated through these depot
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Depots</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDepotLookupSession
    extends AbstractMapDepotLookupSession
    implements org.osid.installation.DepotLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.installation.Depot> depotsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.installation.Depot>());
    private final MultiMap<org.osid.type.Type, org.osid.installation.Depot> depotsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.installation.Depot>());


    /**
     *  Makes a <code>Depot</code> available in this session.
     *
     *  @param  depot a depot
     *  @throws org.osid.NullArgumentException <code>depot<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDepot(org.osid.installation.Depot depot) {
        super.putDepot(depot);

        this.depotsByGenus.put(depot.getGenusType(), depot);
        
        try (org.osid.type.TypeList types = depot.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.depotsByRecord.put(types.getNextType(), depot);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a depot from this session.
     *
     *  @param depotId the <code>Id</code> of the depot
     *  @throws org.osid.NullArgumentException <code>depotId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDepot(org.osid.id.Id depotId) {
        org.osid.installation.Depot depot;
        try {
            depot = getDepot(depotId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.depotsByGenus.remove(depot.getGenusType());

        try (org.osid.type.TypeList types = depot.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.depotsByRecord.remove(types.getNextType(), depot);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDepot(depotId);
        return;
    }


    /**
     *  Gets a <code>DepotList</code> corresponding to the given
     *  depot genus <code>Type</code> which does not include
     *  depots of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known depots or an error results. Otherwise,
     *  the returned list may contain only those depots that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  depotGenusType a depot genus type 
     *  @return the returned <code>Depot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>depotGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByGenusType(org.osid.type.Type depotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.depot.ArrayDepotList(this.depotsByGenus.get(depotGenusType)));
    }


    /**
     *  Gets a <code>DepotList</code> containing the given
     *  depot record <code>Type</code>. In plenary mode, the
     *  returned list contains all known depots or an error
     *  results. Otherwise, the returned list may contain only those
     *  depots that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  depotRecordType a depot record type 
     *  @return the returned <code>depot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>depotRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByRecordType(org.osid.type.Type depotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.depot.ArrayDepotList(this.depotsByRecord.get(depotRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.depotsByGenus.clear();
        this.depotsByRecord.clear();

        super.close();

        return;
    }
}
