//
// AbstractQueryProgramOfferingLookupSession.java
//
//    An inline adapter that maps a ProgramOfferingLookupSession to
//    a ProgramOfferingQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ProgramOfferingLookupSession to
 *  a ProgramOfferingQuerySession.
 */

public abstract class AbstractQueryProgramOfferingLookupSession
    extends net.okapia.osid.jamocha.course.program.spi.AbstractProgramOfferingLookupSession
    implements org.osid.course.program.ProgramOfferingLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.course.program.ProgramOfferingQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryProgramOfferingLookupSession.
     *
     *  @param querySession the underlying program offering query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryProgramOfferingLookupSession(org.osid.course.program.ProgramOfferingQuerySession querySession) {
        nullarg(querySession, "program offering query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>ProgramOffering</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProgramOfferings() {
        return (this.session.canSearchProgramOfferings());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include program offerings in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only program offerings whose effective dates are current are
     *  returned by methods in this session.
     */

    public void useEffectiveProgramOfferingView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All program offerings of any effective dates are returned by
     *  all methods in this session.
     */

    public void useAnyEffectiveProgramOfferingView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>ProgramOffering</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProgramOffering</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProgramOffering</code> and
     *  retained for compatibility.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingId <code>Id</code> of the
     *          <code>ProgramOffering</code>
     *  @return the program offering
     *  @throws org.osid.NotFoundException <code>programOfferingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOffering getProgramOffering(org.osid.id.Id programOfferingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchId(programOfferingId, true);
        org.osid.course.program.ProgramOfferingList programOfferings = this.session.getProgramOfferingsByQuery(query);
        if (programOfferings.hasNext()) {
            return (programOfferings.getNextProgramOffering());
        } 
        
        throw new org.osid.NotFoundException(programOfferingId + " not found");
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  programOfferings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProgramOfferings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByIds(org.osid.id.IdList programOfferingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();

        try (org.osid.id.IdList ids = programOfferingIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the given
     *  program offering genus <code>Type</code> which does not include
     *  program offerings of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingGenusType a programOffering genus type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByGenusType(org.osid.type.Type programOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchGenusType(programOfferingGenusType, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the
     *  given program offering genus <code>Type</code> and include any
     *  additional program offerings with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingGenusType a programOffering genus type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByParentGenusType(org.osid.type.Type programOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchParentGenusType(programOfferingGenusType, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> containing the given
     *  program offering record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingRecordType a programOffering record type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByRecordType(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchRecordType(programOfferingRecordType, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsOnDate(org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets all <code> ProgramOfferings </code> associated with a
     *  given <code> Program. </code> 
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programId a program <code> Id </code> 
     *  @return a list of <code> ProgramOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> programId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchProgramId(programId, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> for a program and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramOnDate(org.osid.id.Id programId, 
                                                                                           org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchDate(from, to, true);
        query.matchProgramId(programId, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets all <code> ProgramOfferings </code> associated with a
     *  given <code> Term. </code> 
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code> 
     *  @return a list of <code> ProgramOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchTermId(termId, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> for a term and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>termId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForTermOnDate(org.osid.id.Id termId, 
                                                                                        org.osid.calendaring.DateTime from, 
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchDate(from, to, true);
        query.matchTermId(termId, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets all <code> ProgramOfferings </code> associated with a
     *  given <code>Program</code> and <code>Term</code> 
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results.  Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  termId a term <code> Id </code> 
     *  @return a list of <code> ProgramOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> programId </code>
     *          or <code> termId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramAndTerm(org.osid.id.Id programId, 
                                                                                            org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchProgramId(programId, true);
        query.matchTermId(termId, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> for a program, term, and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  termId a term <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>termId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramAndTermOnDate(org.osid.id.Id programId, 
                                                                                                  org.osid.id.Id termId,
                                                                                                  org.osid.calendaring.DateTime from, 
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchDate(from, to, true);
        query.matchProgramId(programId, true);
        query.matchTermId(termId, true);
        return (this.session.getProgramOfferingsByQuery(query));
    }

    
    /**
     *  Gets all <code>ProgramOfferings</code>. 
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @return a list of <code>ProgramOfferings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.course.program.ProgramOfferingQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getProgramOfferingsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.program.ProgramOfferingQuery getQuery() {
        org.osid.course.program.ProgramOfferingQuery query = this.session.getProgramOfferingQuery();

        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }
        
        return (query);
    }
}
