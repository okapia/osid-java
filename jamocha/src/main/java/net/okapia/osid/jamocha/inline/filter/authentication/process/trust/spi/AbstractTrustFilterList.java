//
// AbstractTrustList
//
//     Implements a filter for a TrustList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.authentication.process.trust.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a TrustList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedTrustList
 *  to improve performance.
 */

public abstract class AbstractTrustFilterList
    extends net.okapia.osid.jamocha.authentication.process.trust.spi.AbstractTrustList
    implements org.osid.authentication.process.TrustList,
               net.okapia.osid.jamocha.inline.filter.authentication.process.trust.TrustFilter {

    private org.osid.authentication.process.Trust trust;
    private final org.osid.authentication.process.TrustList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractTrustFilterList</code>.
     *
     *  @param trustList a <code>TrustList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>trustList</code> is <code>null</code>
     */

    protected AbstractTrustFilterList(org.osid.authentication.process.TrustList trustList) {
        nullarg(trustList, "trust list");
        this.list = trustList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.trust == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Trust </code> in this list. 
     *
     *  @return the next <code> Trust </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Trust </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.process.Trust getNextTrust()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.authentication.process.Trust trust = this.trust;
            this.trust = null;
            return (trust);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in trust list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.trust = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Trusts.
     *
     *  @param trust the trust to filter
     *  @return <code>true</code> if the trust passes the filter,
     *          <code>false</code> if the trust should be filtered
     */

    public abstract boolean pass(org.osid.authentication.process.Trust trust);


    protected void prime() {
        if (this.trust != null) {
            return;
        }

        org.osid.authentication.process.Trust trust = null;

        while (this.list.hasNext()) {
            try {
                trust = this.list.getNextTrust();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(trust)) {
                this.trust = trust;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
