//
// MutableIndexedMapProxyObstacleLookupSession
//
//    Implements an Obstacle lookup service backed by a collection of
//    obstacles indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements an Obstacle lookup service backed by a collection of
 *  obstacles. The obstacles are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some obstacles may be compatible
 *  with more types than are indicated through these obstacle
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of obstacles can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyObstacleLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractIndexedMapObstacleLookupSession
    implements org.osid.mapping.path.ObstacleLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyObstacleLookupSession} with
     *  no obstacle.
     *
     *  @param map the map
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyObstacleLookupSession(org.osid.mapping.Map map,
                                                       org.osid.proxy.Proxy proxy) {
        setMap(map);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyObstacleLookupSession} with
     *  a single obstacle.
     *
     *  @param map the map
     *  @param  obstacle an obstacle
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code obstacle}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyObstacleLookupSession(org.osid.mapping.Map map,
                                                       org.osid.mapping.path.Obstacle obstacle, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putObstacle(obstacle);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyObstacleLookupSession} using
     *  an array of obstacles.
     *
     *  @param map the map
     *  @param  obstacles an array of obstacles
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code obstacles}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyObstacleLookupSession(org.osid.mapping.Map map,
                                                       org.osid.mapping.path.Obstacle[] obstacles, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putObstacles(obstacles);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyObstacleLookupSession} using
     *  a collection of obstacles.
     *
     *  @param map the map
     *  @param  obstacles a collection of obstacles
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code obstacles}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyObstacleLookupSession(org.osid.mapping.Map map,
                                                       java.util.Collection<? extends org.osid.mapping.path.Obstacle> obstacles,
                                                       org.osid.proxy.Proxy proxy) {
        this(map, proxy);
        putObstacles(obstacles);
        return;
    }

    
    /**
     *  Makes an {@code Obstacle} available in this session.
     *
     *  @param  obstacle an obstacle
     *  @throws org.osid.NullArgumentException {@code obstacle{@code 
     *          is {@code null}
     */

    @Override
    public void putObstacle(org.osid.mapping.path.Obstacle obstacle) {
        super.putObstacle(obstacle);
        return;
    }


    /**
     *  Makes an array of obstacles available in this session.
     *
     *  @param  obstacles an array of obstacles
     *  @throws org.osid.NullArgumentException {@code obstacles{@code 
     *          is {@code null}
     */

    @Override
    public void putObstacles(org.osid.mapping.path.Obstacle[] obstacles) {
        super.putObstacles(obstacles);
        return;
    }


    /**
     *  Makes collection of obstacles available in this session.
     *
     *  @param  obstacles a collection of obstacles
     *  @throws org.osid.NullArgumentException {@code obstacle{@code 
     *          is {@code null}
     */

    @Override
    public void putObstacles(java.util.Collection<? extends org.osid.mapping.path.Obstacle> obstacles) {
        super.putObstacles(obstacles);
        return;
    }


    /**
     *  Removes an Obstacle from this session.
     *
     *  @param obstacleId the {@code Id} of the obstacle
     *  @throws org.osid.NullArgumentException {@code obstacleId{@code  is
     *          {@code null}
     */

    @Override
    public void removeObstacle(org.osid.id.Id obstacleId) {
        super.removeObstacle(obstacleId);
        return;
    }    
}
