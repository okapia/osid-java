//
// AbstractMutableGradebookColumnSummary.java
//
//     Defines a mutable GradebookColumnSummary.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>GradebookColumnSummary</code>.
 */

public abstract class AbstractMutableGradebookColumnSummary
    extends net.okapia.osid.jamocha.grading.gradebookcolumnsummary.spi.AbstractGradebookColumnSummary
    implements org.osid.grading.GradebookColumnSummary,
               net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary.GradebookColumnSummaryMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this gradebook column summary. 
     *
     *  @param record gradebook column summary record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addGradebookColumnSummaryRecord(org.osid.grading.records.GradebookColumnSummaryRecord record, org.osid.type.Type recordType) {
        super.addGradebookColumnSummaryRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this gradebook column summary.
     *
     *  @param displayName the name for this gradebook column summary
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this gradebook column summary.
     *
     *  @param description the description of this gradebook column summary
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    @Override
    public void setGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        super.setGradebookColumn(gradebookColumn);
        return;
    }


    /**
     *  Sets the mean.
     *
     *  @param mean a mean
     *  @throws org.osid.NullArgumentException <code>mean</code> is
     *          <code>null</code>
     */

    @Override
    public void setMean(java.math.BigDecimal mean) {
        super.setMean(mean);
        return;
    }


    /**
     *  Sets the median.
     *
     *  @param median a median
     *  @throws org.osid.NullArgumentException <code>median</code> is
     *          <code>null</code>
     */

    @Override
    public void setMedian(java.math.BigDecimal median) {
        super.setMedian(median);
        return;
    }


    /**
     *  Sets the mode.
     *
     *  @param mode a mode
     *  @throws org.osid.NullArgumentException <code>mode</code> is
     *          <code>null</code>
     */

    @Override
    public void setMode(java.math.BigDecimal mode) {
        super.setMode(mode);
        return;
    }


    /**
     *  Sets the rms.
     *
     *  @param rms the root mean square
     *  @throws org.osid.NullArgumentException <code>rms</code> is
     *          <code>null</code>
     */

    @Override
    public void setRMS(java.math.BigDecimal rms) {
        super.setRMS(rms);
        return;
    }


    /**
     *  Sets the standard deviation.
     *
     *  @param standardDeviation a standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    @Override
    public void setStandardDeviation(java.math.BigDecimal standardDeviation) {
        super.setStandardDeviation(standardDeviation);
        return;
    }


    /**
     *  Sets the sum.
     *
     *  @param sum a sum
     *  @throws org.osid.NullArgumentException <code>sum</code> is
     *          <code>null</code>
     */

    @Override
    public void setSum(java.math.BigDecimal sum) {
        super.setSum(sum);
        return;
    }
}

