//
// AbstractFileSearchOdrer.java
//
//     Defines a FileSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.file.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code FileSearchOrder}.
 */

public abstract class AbstractFileSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.filing.FileSearchOrder {

    private final java.util.Collection<org.osid.filing.records.FileSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the
     *  entry name.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByName(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  entry path.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByPath(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  entry owner.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByOwner(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return {@code true} if an agent search order interface is 
     *          available, {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsOwnerSearchOrder() {
        return (false);
    }


    /**
     *  Gets an agent search order interface. 
     *
     *  @return an agent search order interface 
     *  @throws org.osid.UnimplementedException {@code 
     *          supportsOwnerSearchOrder()} is {@code false} 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getOwnerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the entry 
     *  creation time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByCreatedTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the entry 
     *  modification time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByModifiedTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the entry last 
     *  access time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByLastAccessTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the results by file size. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderBySize(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  fileRecordType a file record type 
     *  @return {@code true} if the fileRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code fileRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type fileRecordType) {
        for (org.osid.filing.records.FileSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(fileRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  fileRecordType the file record type 
     *  @return the file search order record
     *  @throws org.osid.NullArgumentException
     *          {@code fileRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(fileRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.filing.records.FileSearchOrderRecord getFileSearchOrderRecord(org.osid.type.Type fileRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.records.FileSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(fileRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fileRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this file. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param fileRecord the file search odrer record
     *  @param fileRecordType file record type
     *  @throws org.osid.NullArgumentException
     *          {@code fileRecord} or
     *          {@code fileRecordTypefile} is
     *          {@code null}
     */
            
    protected void addFileRecord(org.osid.filing.records.FileSearchOrderRecord fileSearchOrderRecord, 
                                     org.osid.type.Type fileRecordType) {

        addRecordType(fileRecordType);
        this.records.add(fileSearchOrderRecord);
        
        return;
    }
}
