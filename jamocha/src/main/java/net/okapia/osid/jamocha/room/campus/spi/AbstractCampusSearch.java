//
// AbstractCampusSearch.java
//
//     A template for making a Campus Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.campus.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing campus searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCampusSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.room.CampusSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.room.records.CampusSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.room.CampusSearchOrder campusSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of campuses. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  campusIds list of campuses
     *  @throws org.osid.NullArgumentException
     *          <code>campusIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCampuses(org.osid.id.IdList campusIds) {
        while (campusIds.hasNext()) {
            try {
                this.ids.add(campusIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCampuses</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of campus Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCampusIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  campusSearchOrder campus search order 
     *  @throws org.osid.NullArgumentException
     *          <code>campusSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>campusSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCampusResults(org.osid.room.CampusSearchOrder campusSearchOrder) {
	this.campusSearchOrder = campusSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.room.CampusSearchOrder getCampusSearchOrder() {
	return (this.campusSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given campus search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a campus implementing the requested record.
     *
     *  @param campusSearchRecordType a campus search record
     *         type
     *  @return the campus search record
     *  @throws org.osid.NullArgumentException
     *          <code>campusSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(campusSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.CampusSearchRecord getCampusSearchRecord(org.osid.type.Type campusSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.room.records.CampusSearchRecord record : this.records) {
            if (record.implementsRecordType(campusSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(campusSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this campus search. 
     *
     *  @param campusSearchRecord campus search record
     *  @param campusSearchRecordType campus search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCampusSearchRecord(org.osid.room.records.CampusSearchRecord campusSearchRecord, 
                                           org.osid.type.Type campusSearchRecordType) {

        addRecordType(campusSearchRecordType);
        this.records.add(campusSearchRecord);        
        return;
    }
}
