//
// AbstractChecklistSearch.java
//
//     A template for making a Checklist Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.checklist.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing checklist searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractChecklistSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.checklist.ChecklistSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.checklist.records.ChecklistSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.checklist.ChecklistSearchOrder checklistSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of checklists. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  checklistIds list of checklists
     *  @throws org.osid.NullArgumentException
     *          <code>checklistIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongChecklists(org.osid.id.IdList checklistIds) {
        while (checklistIds.hasNext()) {
            try {
                this.ids.add(checklistIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongChecklists</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of checklist Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getChecklistIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  checklistSearchOrder checklist search order 
     *  @throws org.osid.NullArgumentException
     *          <code>checklistSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>checklistSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderChecklistResults(org.osid.checklist.ChecklistSearchOrder checklistSearchOrder) {
	this.checklistSearchOrder = checklistSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.checklist.ChecklistSearchOrder getChecklistSearchOrder() {
	return (this.checklistSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given checklist search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a checklist implementing the requested record.
     *
     *  @param checklistSearchRecordType a checklist search record
     *         type
     *  @return the checklist search record
     *  @throws org.osid.NullArgumentException
     *          <code>checklistSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checklistSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.ChecklistSearchRecord getChecklistSearchRecord(org.osid.type.Type checklistSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.checklist.records.ChecklistSearchRecord record : this.records) {
            if (record.implementsRecordType(checklistSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checklistSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this checklist search. 
     *
     *  @param checklistSearchRecord checklist search record
     *  @param checklistSearchRecordType checklist search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addChecklistSearchRecord(org.osid.checklist.records.ChecklistSearchRecord checklistSearchRecord, 
                                           org.osid.type.Type checklistSearchRecordType) {

        addRecordType(checklistSearchRecordType);
        this.records.add(checklistSearchRecord);        
        return;
    }
}
