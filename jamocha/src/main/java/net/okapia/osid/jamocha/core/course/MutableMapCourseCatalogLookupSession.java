//
// MutableMapCourseCatalogLookupSession
//
//    Implements a CourseCatalog lookup service backed by a collection of
//    courseCatalogs that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements a CourseCatalog lookup service backed by a collection of
 *  course catalogs. The course catalogs are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of course catalogs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapCourseCatalogLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractMapCourseCatalogLookupSession
    implements org.osid.course.CourseCatalogLookupSession {


    /**
     *  Constructs a new {@code MutableMapCourseCatalogLookupSession}
     *  with no course catalogs.
     */

    public MutableMapCourseCatalogLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCourseCatalogLookupSession} with a
     *  single courseCatalog.
     *  
     *  @param courseCatalog a course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

    public MutableMapCourseCatalogLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        putCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCourseCatalogLookupSession}
     *  using an array of course catalogs.
     *
     *  @param courseCatalogs an array of course catalogs
     *  @throws org.osid.NullArgumentException {@code courseCatalogs}
     *          is {@code null}
     */

    public MutableMapCourseCatalogLookupSession(org.osid.course.CourseCatalog[] courseCatalogs) {
        putCourseCatalogs(courseCatalogs);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCourseCatalogLookupSession}
     *  using a collection of course catalogs.
     *
     *  @param courseCatalogs a collection of course catalogs
     *  @throws org.osid.NullArgumentException {@code courseCatalogs}
     *          is {@code null}
     */

    public MutableMapCourseCatalogLookupSession(java.util.Collection<? extends org.osid.course.CourseCatalog> courseCatalogs) {
        putCourseCatalogs(courseCatalogs);
        return;
    }

    
    /**
     *  Makes a {@code CourseCatalog} available in this session.
     *
     *  @param courseCatalog a course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog{@code  is
     *          {@code null}
     */

    @Override
    public void putCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        super.putCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Makes an array of course catalogs available in this session.
     *
     *  @param courseCatalogs an array of course catalogs
     *  @throws org.osid.NullArgumentException {@code courseCatalogs{@code 
     *          is {@code null}
     */

    @Override
    public void putCourseCatalogs(org.osid.course.CourseCatalog[] courseCatalogs) {
        super.putCourseCatalogs(courseCatalogs);
        return;
    }


    /**
     *  Makes collection of course catalogs available in this session.
     *
     *  @param courseCatalogs a collection of course catalogs
     *  @throws org.osid.NullArgumentException {@code courseCatalogs{@code  is
     *          {@code null}
     */

    @Override
    public void putCourseCatalogs(java.util.Collection<? extends org.osid.course.CourseCatalog> courseCatalogs) {
        super.putCourseCatalogs(courseCatalogs);
        return;
    }


    /**
     *  Removes a CourseCatalog from this session.
     *
     *  @param courseCatalogId the {@code Id} of the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalogId{@code 
     *          is {@code null}
     */

    @Override
    public void removeCourseCatalog(org.osid.id.Id courseCatalogId) {
        super.removeCourseCatalog(courseCatalogId);
        return;
    }    
}
