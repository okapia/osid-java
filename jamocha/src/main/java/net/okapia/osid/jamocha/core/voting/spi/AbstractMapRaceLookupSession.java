//
// AbstractMapRaceLookupSession
//
//    A simple framework for providing a Race lookup service
//    backed by a fixed collection of races.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Race lookup service backed by a
 *  fixed collection of races. The races are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Races</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRaceLookupSession
    extends net.okapia.osid.jamocha.voting.spi.AbstractRaceLookupSession
    implements org.osid.voting.RaceLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.Race> races = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.Race>());


    /**
     *  Makes a <code>Race</code> available in this session.
     *
     *  @param  race a race
     *  @throws org.osid.NullArgumentException <code>race<code>
     *          is <code>null</code>
     */

    protected void putRace(org.osid.voting.Race race) {
        this.races.put(race.getId(), race);
        return;
    }


    /**
     *  Makes an array of races available in this session.
     *
     *  @param  races an array of races
     *  @throws org.osid.NullArgumentException <code>races<code>
     *          is <code>null</code>
     */

    protected void putRaces(org.osid.voting.Race[] races) {
        putRaces(java.util.Arrays.asList(races));
        return;
    }


    /**
     *  Makes a collection of races available in this session.
     *
     *  @param  races a collection of races
     *  @throws org.osid.NullArgumentException <code>races<code>
     *          is <code>null</code>
     */

    protected void putRaces(java.util.Collection<? extends org.osid.voting.Race> races) {
        for (org.osid.voting.Race race : races) {
            this.races.put(race.getId(), race);
        }

        return;
    }


    /**
     *  Removes a Race from this session.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @throws org.osid.NullArgumentException <code>raceId<code> is
     *          <code>null</code>
     */

    protected void removeRace(org.osid.id.Id raceId) {
        this.races.remove(raceId);
        return;
    }


    /**
     *  Gets the <code>Race</code> specified by its <code>Id</code>.
     *
     *  @param  raceId <code>Id</code> of the <code>Race</code>
     *  @return the race
     *  @throws org.osid.NotFoundException <code>raceId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>raceId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Race getRace(org.osid.id.Id raceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.Race race = this.races.get(raceId);
        if (race == null) {
            throw new org.osid.NotFoundException("race not found: " + raceId);
        }

        return (race);
    }


    /**
     *  Gets all <code>Races</code>. In plenary mode, the returned
     *  list contains all known races or an error
     *  results. Otherwise, the returned list may contain only those
     *  races that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Races</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRaces()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.race.ArrayRaceList(this.races.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.races.clear();
        super.close();
        return;
    }
}
