//
// ValueMiter.java
//
//     Defines a Value miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.value;


/**
 *  Defines a <code>Value</code> miter for use with the builders.
 */

public interface ValueMiter
    extends net.okapia.osid.jamocha.builder.spi.OperableOsidObjectMiter,
            org.osid.configuration.Value {


    /**
     *  Sets the parameter.
     *
     *  @param parameter a parameter
     *  @throws org.osid.NullArgumentException <code>parameter</code>
     *          is <code>null</code>
     */

    public void setParameter(org.osid.configuration.Parameter parameter);


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @throws org.osid.InvalidArgumentException
     *          <code>priority</code> is negative
     */

    public void setPriority(long priority);


    /**
     *  Sets the boolean value.
     *
     *  @param booleanValue a boolean value
     *  @throws org.osid.NullArgumentException
     *          <code>booleanValue</code> is <code>null</code>
     */

    public void setBooleanValue(boolean booleanValue);


    /**
     *  Sets the bytes value.
     *
     *  @param bytesValue a bytes value
     *  @throws org.osid.NullArgumentException
     *          <code>bytesValue</code> is <code>null</code>
     */

    public void setBytesValue(byte[] bytesValue);


    /**
     *  Sets the cardinal value.
     *
     *  @param cardinalValue a cardinal value
     *  @throws org.osid.InvalidArgumentException
     *          <code>cardinalValue</code> is negative
     */

    public void setCardinalValue(long cardinalValue);


    /**
     *  Sets the coordinate value.
     *
     *  @param coordinateValue a coordinate value
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateValue</code> is <code>null</code>
     */

    public void setCoordinateValue(org.osid.mapping.Coordinate coordinateValue);


    /**
     *  Sets the currency value.
     *
     *  @param currencyValue a currency value
     *  @throws org.osid.NullArgumentException
     *          <code>currencyValue</code> is <code>null</code>
     */

    public void setCurrencyValue(org.osid.financials.Currency currencyValue);


    /**
     *  Sets the date time value.
     *
     *  @param dateTimeValue a date time value
     *  @throws org.osid.NullArgumentException
     *          <code>dateTimeValue</code> is <code>null</code>
     */

    public void setDateTimeValue(org.osid.calendaring.DateTime dateTimeValue);


    /**
     *  Sets the decimal value.
     *
     *  @param decimalValue a decimal value
     *  @throws org.osid.NullArgumentException
     *          <code>decimalValue</code> is <code>null</code>
     */

    public void setDecimalValue(java.math.BigDecimal decimalValue);


    /**
     *  Sets the distance value.
     *
     *  @param distanceValue a distance value
     *  @throws org.osid.NullArgumentException
     *          <code>distanceValue</code> is <code>null</code>
     */

    public void setDistanceValue(org.osid.mapping.Distance distanceValue);


    /**
     *  Sets the duration value.
     *
     *  @param durationValue a duration value
     *  @throws org.osid.NullArgumentException
     *          <code>durationValue</code> is <code>null</code>
     */

    public void setDurationValue(org.osid.calendaring.Duration durationValue);


    /**
     *  Sets the heading value.
     *
     *  @param headingValue a heading value
     *  @throws org.osid.NullArgumentException
     *          <code>headingValue</code> is <code>null</code>
     */

    public void setHeadingValue(org.osid.mapping.Heading headingValue);


    /**
     *  Sets the id value.
     *
     *  @param idValue an id value
     *  @throws org.osid.NullArgumentException <code>idValue</code> is
     *          <code>null</code>
     */

    public void setIdValue(org.osid.id.Id idValue);


    /**
     *  Sets the integer value.
     *
     *  @param integerValue an integer value
     */

    public void setIntegerValue(long integerValue);


    /**
     *  Sets the object value.
     *
     *  @param objectValue an object value
     *  @throws org.osid.NullArgumentException
     *          <code>objectValue</code> is <code>null</code>
     */

    public void setObjectValue(java.lang.Object objectValue);


    /**
     *  Sets the spatial unit value.
     *
     *  @param spatialUnitValue a spatial unit value
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnitValue</code> is <code>null</code>
     */

    public void setSpatialUnitValue(org.osid.mapping.SpatialUnit spatialUnitValue);


    /**
     *  Sets the speed value.
     *
     *  @param speedValue a speed value
     *  @throws org.osid.NullArgumentException <code>speedValue</code>
     *          is <code>null</code>
     */

    public void setSpeedValue(org.osid.mapping.Speed speedValue);


    /**
     *  Sets the string value.
     *
     *  @param stringValue a string value
     *  @throws org.osid.NullArgumentException
     *          <code>stringValue</code> is <code>null</code>
     */

    public void setStringValue(String stringValue);


    /**
     *  Sets the time value.
     *
     *  @param timeValue a time value
     *  @throws org.osid.NullArgumentException <code>timeValue</code>
     *          is <code>null</code>
     */

    public void setTimeValue(org.osid.calendaring.Time timeValue);


    /**
     *  Sets the type value.
     *
     *  @param typeValue a type value
     *  @throws org.osid.NullArgumentException <code>typeValue</code>
     *          is <code>null</code>
     */

    public void setTypeValue(org.osid.type.Type typeValue);


    /**
     *  Sets the version value.
     *
     *  @param versionValue a version value
     *  @throws org.osid.NullArgumentException
     *          <code>versionValue</code> is <code>null</code>
     */

    public void setVersionValue(org.osid.installation.Version versionValue);
}       


