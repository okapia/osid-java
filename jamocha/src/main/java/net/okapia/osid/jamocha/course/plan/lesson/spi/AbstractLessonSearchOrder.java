//
// AbstractLessonSearchOdrer.java
//
//     Defines a LessonSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.lesson.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code LessonSearchOrder}.
 */

public abstract class AbstractLessonSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.course.plan.LessonSearchOrder {

    private final java.util.Collection<org.osid.course.plan.records.LessonSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the plan. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPlan(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> PlanSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a plan search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a plan. 
     *
     *  @return the plan search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSearchOrder getPlanSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPlanSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the docet. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDocet(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> DocetSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a docet search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a docet. 
     *
     *  @return the docet search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSearchOrder getDocetSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDocetSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the planned start time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPlannedStartTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the begun flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBegun(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the actual start time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualStartTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the actual starting 
     *  activity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualStartingActivity(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> ActivitySearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActualStartingActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an actual starting actvity. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActualStartingActivitySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchOrder getActualStartingActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActualStartingActivitySearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the complete flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByComplete(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the skipped flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySkipped(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the actual end time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualEndTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the actual ending 
     *  activity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualEndingActivity(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> ActivitySearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActualEndingActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an actual ending actvity. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActualEndingActivitySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchOrder getActualEndingActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActualEndingActivitySearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the actual time spent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualTimeSpent(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  lessonRecordType a lesson record type 
     *  @return {@code true} if the lessonRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code lessonRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type lessonRecordType) {
        for (org.osid.course.plan.records.LessonSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  lessonRecordType the lesson record type 
     *  @return the lesson search order record
     *  @throws org.osid.NullArgumentException
     *          {@code lessonRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(lessonRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.plan.records.LessonSearchOrderRecord getLessonSearchOrderRecord(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.LessonSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(lessonRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this lesson. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param lessonRecord the lesson search odrer record
     *  @param lessonRecordType lesson record type
     *  @throws org.osid.NullArgumentException
     *          {@code lessonRecord} or
     *          {@code lessonRecordTypelesson} is
     *          {@code null}
     */
            
    protected void addLessonRecord(org.osid.course.plan.records.LessonSearchOrderRecord lessonSearchOrderRecord, 
                                     org.osid.type.Type lessonRecordType) {

        addRecordType(lessonRecordType);
        this.records.add(lessonSearchOrderRecord);
        
        return;
    }
}
