//
// AbstractMutableAppointment.java
//
//     Defines a mutable Appointment.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.appointment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Appointment</code>.
 */

public abstract class AbstractMutableAppointment
    extends net.okapia.osid.jamocha.personnel.appointment.spi.AbstractAppointment
    implements org.osid.personnel.Appointment,
               net.okapia.osid.jamocha.builder.personnel.appointment.AppointmentMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this appointment. 
     *
     *  @param record appointment record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAppointmentRecord(org.osid.personnel.records.AppointmentRecord record, org.osid.type.Type recordType) {
        super.addAppointmentRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this appointment is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this appointment ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this appointment.
     *
     *  @param displayName the name for this appointment
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this appointment.
     *
     *  @param description the description of this appointment
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this appointment
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the person.
     *
     *  @param person a person
     *  @throws org.osid.NullArgumentException <code>person</code> is
     *          <code>null</code>
     */

    @Override
    public void setPerson(org.osid.personnel.Person person) {
        super.setPerson(person);
        return;
    }


    /**
     *  Sets the position.
     *
     *  @param position a position
     *  @throws org.osid.NullArgumentException <code>position</code>
     *          is <code>null</code>
     */

    @Override
    public void setPosition(org.osid.personnel.Position position) {
        super.setPosition(position);
        return;
    }


    /**
     *  Sets the commitment.
     *
     *  @param percentage a commitment (0-100)
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    @Override
    public void setCommitment(long percentage) {
        super.setCommitment(percentage);
        return;
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    @Override
    public void setTitle(org.osid.locale.DisplayText title) {
        super.setTitle(title);
        return;
    }


    /**
     *  Sets the salary.
     *
     *  @param salary a salary
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    @Override
    public void setSalary(org.osid.financials.Currency salary) {
        super.setSalary(salary);
        return;
    }


    /**
     *  Sets the salary basis.
     *
     *  @param basis a salary basis
     *  @throws org.osid.NullArgumentException <code>basis</code> is
     *          <code>null</code>
     */

    @Override
    public void setSalaryBasis(long basis) {
        super.setSalaryBasis(basis);
        return;
    }
}

