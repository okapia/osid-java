//
// AbstractSyllabusQuery.java
//
//     A template for making a Syllabus Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for syllabi.
 */

public abstract class AbstractSyllabusQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.course.syllabus.SyllabusQuery {

    private final java.util.Collection<org.osid.course.syllabus.records.SyllabusQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets a course <code> Id. </code> 
     *
     *  @param  courseId a curse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        return;
    }


    /**
     *  Sets a module <code> Id. </code> 
     *
     *  @param  moduleId a module <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> moduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModuleId(org.osid.id.Id moduleId, boolean match) {
        return;
    }


    /**
     *  Clears the module <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModuleIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ModuleQuery </code> is available. 
     *
     *  @return <code> true </code> if a module query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a module query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the module query 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuery getModuleQuery() {
        throw new org.osid.UnimplementedException("supportsModuleQuery() is false");
    }


    /**
     *  Matches any module. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyModule(boolean match) {
        return;
    }


    /**
     *  Clears the module terms. 
     */

    @OSID @Override
    public void clearModuleTerms() {
        return;
    }


    /**
     *  Sets the docet <code> Id </code> for this query to match syllabi 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given syllabus query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a syllabus implementing the requested record.
     *
     *  @param syllabusRecordType a syllabus record type
     *  @return the syllabus query record
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(syllabusRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.SyllabusQueryRecord getSyllabusQueryRecord(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.SyllabusQueryRecord record : this.records) {
            if (record.implementsRecordType(syllabusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(syllabusRecordType + " is not supported");
    }


    /**
     *  Adds a record to this syllabus query. 
     *
     *  @param syllabusQueryRecord syllabus query record
     *  @param syllabusRecordType syllabus record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSyllabusQueryRecord(org.osid.course.syllabus.records.SyllabusQueryRecord syllabusQueryRecord, 
                                          org.osid.type.Type syllabusRecordType) {

        addRecordType(syllabusRecordType);
        nullarg(syllabusQueryRecord, "syllabus query record");
        this.records.add(syllabusQueryRecord);        
        return;
    }
}
