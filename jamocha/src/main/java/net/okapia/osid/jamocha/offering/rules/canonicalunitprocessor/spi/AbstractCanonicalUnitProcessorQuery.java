//
// AbstractCanonicalUnitProcessorQuery.java
//
//     A template for making a CanonicalUnitProcessor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for canonical unit processors.
 */

public abstract class AbstractCanonicalUnitProcessorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQuery
    implements org.osid.offering.rules.CanonicalUnitProcessorQuery {

    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to the canonical unit. 
     *
     *  @param  canonicalUnitId the canonical unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCanonicalUnitId(org.osid.id.Id canonicalUnitId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the canonical unit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CanonicalUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if a canonical unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCanonicalUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for a canonical unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the canonical unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCanonicalUnitQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuery getRuledCanonicalUnitQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCanonicalUnitQuery() is false");
    }


    /**
     *  Matches mapped to any canonical unit. 
     *
     *  @param  match <code> true </code> for mapped to any canonical unit, 
     *          <code> false </code> to match mapped to no canonicalUnit 
     */

    @OSID @Override
    public void matchAnyRuledCanonicalUnit(boolean match) {
        return;
    }


    /**
     *  Clears the canonical unit query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitTerms() {
        return;
    }


    /**
     *  Matches mapped to the catalogue. 
     *
     *  @param  catalogueId the catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        return;
    }


    /**
     *  Clears the catalogue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears the catalogue query terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given canonical unit processor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a canonical unit processor implementing the requested record.
     *
     *  @param canonicalUnitProcessorRecordType a canonical unit processor record type
     *  @return the canonical unit processor query record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorQueryRecord getCanonicalUnitProcessorQueryRecord(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorQueryRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit processor query. 
     *
     *  @param canonicalUnitProcessorQueryRecord canonical unit processor query record
     *  @param canonicalUnitProcessorRecordType canonicalUnitProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitProcessorQueryRecord(org.osid.offering.rules.records.CanonicalUnitProcessorQueryRecord canonicalUnitProcessorQueryRecord, 
                                          org.osid.type.Type canonicalUnitProcessorRecordType) {

        addRecordType(canonicalUnitProcessorRecordType);
        nullarg(canonicalUnitProcessorQueryRecord, "canonical unit processor query record");
        this.records.add(canonicalUnitProcessorQueryRecord);        
        return;
    }
}
