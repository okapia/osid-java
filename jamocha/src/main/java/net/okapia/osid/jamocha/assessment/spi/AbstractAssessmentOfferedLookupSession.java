//
// AbstractAssessmentOfferedLookupSession.java
//
//    A starter implementation framework for providing an AssessmentOffered
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an AssessmentOffered
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAssessmentsOffered(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAssessmentOfferedLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.assessment.AssessmentOfferedLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();
    

    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }

    /**
     *  Tests if this user can perform <code>AssessmentOffered</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessmentsOffered() {
        return (true);
    }


    /**
     *  A complete view of the <code>AssessmentOffered</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentOfferedView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>AssessmentOffered</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentOfferedView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments offered in banks which are
     *  children of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>AssessmentOffered</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code>AssessmentOffered</code> may have a different <code> Id
     *  </code> than requested, such as the case where a duplicate
     *  <code> Id </code> was assigned to a
     *  <code>AssessmentOffered</code> and retained for compatibility.
     *
     *  @param  assessmentOfferedId <code>Id</code> of the
     *          <code>AssessmentOffered</code>
     *  @return the assessment offered
     *  @throws org.osid.NotFoundException <code>assessmentOfferedId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentOfferedId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOffered getAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.assessment.AssessmentOfferedList assessmentsOffered = getAssessmentsOffered()) {
            while (assessmentsOffered.hasNext()) {
                org.osid.assessment.AssessmentOffered assessmentOffered = assessmentsOffered.getNextAssessmentOffered();
                if (assessmentOffered.getId().equals(assessmentOfferedId)) {
                    return (assessmentOffered);
                }
            }
        } 

        throw new org.osid.NotFoundException(assessmentOfferedId + " not found");
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentsOffered specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AssessmentsOffered</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAssessmentsOffered()</code>.
     *
     *  @param  assessmentOfferedIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByIds(org.osid.id.IdList assessmentOfferedIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.AssessmentOffered> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = assessmentOfferedIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAssessmentOffered(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("assessment offered " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.assessment.assessmentoffered.LinkedAssessmentOfferedList(ret));
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  assessment offered genus <code>Type</code> which does not include
     *  assessments offered of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAssessmentsOffered()</code>.
     *
     *  @param  assessmentOfferedGenusType an assessmentOffered genus type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByGenusType(org.osid.type.Type assessmentOfferedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmentoffered.AssessmentOfferedGenusFilterList(getAssessmentsOffered(), assessmentOfferedGenusType));
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  assessment offered genus <code>Type</code> and include any additional
     *  assessments offered with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssessmentsOffered()</code>.
     *
     *  @param  assessmentOfferedGenusType an assessmentOffered genus type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByParentGenusType(org.osid.type.Type assessmentOfferedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAssessmentsOfferedByGenusType(assessmentOfferedGenusType));
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> containing the given
     *  assessment offered record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssessmentsOffered()</code>.
     *
     *  @param  assessmentOfferedRecordType an assessmentOffered record type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByRecordType(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmentoffered.AssessmentOfferedRecordFilterList(getAssessmentsOffered(), assessmentOfferedRecordType));
    }


    /**
     *  Gets an <code> AssessmentOfferedList </code> that have
     *  designated start times where the start times fall in the given
     *  range inclusive.  In plenary mode, the returned list contains
     *  all known assessments offered or an error results. Otherwise,
     *  the returned list may contain only those assessments offered
     *  that are accessible through this session.
     *
     *  @param  start start of time range
     *  @param  end end of time range
     *  @return the returned <code> AssessmentOffered </code> list
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less
     *          than <code> start </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByDate(org.osid.calendaring.DateTime start,
                                                                                 org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmentoffered.AssessmentOfferedFilterList(new StartDateFilter(start, end), getAssessmentsOffered()));
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> by the given
     *  assessment.  In plenary mode, the returned list contains all
     *  known assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session.
     *
     *  @param  assessmentId <code>Id</code> of an <code>Assessment</code> 
     *  @return the returned <code>AssessmentOffered</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmentoffered.AssessmentOfferedFilterList(new AssessmentFilter(assessmentId), getAssessmentsOffered()));
    }


    /**
     *  Gets all <code>AssessmentsOffered</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AssessmentsOffered</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.assessment.AssessmentOfferedList getAssessmentsOffered()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the assessment offered list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of assessments offered
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.assessment.AssessmentOfferedList filterAssessmentsOfferedOnViews(org.osid.assessment.AssessmentOfferedList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }


    public static class StartDateFilter
        implements net.okapia.osid.jamocha.inline.filter.assessment.assessmentoffered.AssessmentOfferedFilter {

        private final org.osid.calendaring.DateTime start;
        private final org.osid.calendaring.DateTime end;

        
        /**
         *  Constructs a new <code>StartDateFilter</code>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        public StartDateFilter(org.osid.calendaring.DateTime from,
                               org.osid.calendaring.DateTime to) {

            nullarg(from, "start date");
            nullarg(to, "end date");

            this.start = from;
            this.end = to;

            return;
        }


        /**
         *  Used by the AssessmentOfferedFilterList to filter the list
         *  based on start date.
         *
         *  @param assessmentOffered the assessment offered
         *  @return <code>true</code> to pass the assessment offered,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.assessment.AssessmentOffered assessmentOffered) {
            if (!assessmentOffered.hasStartTime()) {
                return (false);
            }

            if (assessmentOffered.getStartTime().isLess(this.start)) {
                return (false);
            }

            if (assessmentOffered.getStartTime().isGreater(this.end)) {
                return (false);
            }

            return (true);
        }
    }


    public static class AssessmentFilter
        implements net.okapia.osid.jamocha.inline.filter.assessment.assessmentoffered.AssessmentOfferedFilter {

        private final org.osid.id.Id assessmentId;

        
        /**
         *  Constructs a new <code>TakerFilter</code>.
         *
         *  @param assessmentId the assessment to filter
         *  @throws org.osid.NullArgumentException
         *          <code>assessmentId</code> is <code>null</code>
         */

        public AssessmentFilter(org.osid.id.Id assessmentId) {
            nullarg(assessmentId, "assessment Id");
            this.assessmentId = assessmentId;
            return;
        }


        /**
         *  Used by the AssessmentOfferedFilterList to filter the list
         *  based on assessment.
         *
         *  @param assessmentOffered the assessment offered
         *  @return <code>true</code> to pass the assessment offered,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.assessment.AssessmentOffered assessmentOffered) {
            if (assessmentOffered.getAssessmentId().equals(this.assessmentId)) {
                return (true);
            } else {
                return (false);
            }
        }
    }
}
