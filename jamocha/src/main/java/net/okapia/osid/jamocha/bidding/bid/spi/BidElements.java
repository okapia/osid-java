//
// BidElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.bid.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class BidElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the BidElement Id.
     *
     *  @return the bid element Id
     */

    public static org.osid.id.Id getBidEntityId() {
        return (makeEntityId("osid.bidding.Bid"));
    }


    /**
     *  Gets the AuctionId element Id.
     *
     *  @return the AuctionId element Id
     */

    public static org.osid.id.Id getAuctionId() {
        return (makeElementId("osid.bidding.bid.AuctionId"));
    }


    /**
     *  Gets the Auction element Id.
     *
     *  @return the Auction element Id
     */

    public static org.osid.id.Id getAuction() {
        return (makeElementId("osid.bidding.bid.Auction"));
    }


    /**
     *  Gets the BidderId element Id.
     *
     *  @return the BidderId element Id
     */

    public static org.osid.id.Id getBidderId() {
        return (makeElementId("osid.bidding.bid.BidderId"));
    }


    /**
     *  Gets the Bidder element Id.
     *
     *  @return the Bidder element Id
     */

    public static org.osid.id.Id getBidder() {
        return (makeElementId("osid.bidding.bid.Bidder"));
    }


    /**
     *  Gets the BiddingAgentId element Id.
     *
     *  @return the BiddingAgentId element Id
     */

    public static org.osid.id.Id getBiddingAgentId() {
        return (makeElementId("osid.bidding.bid.BiddingAgentId"));
    }


    /**
     *  Gets the BiddingAgent element Id.
     *
     *  @return the BiddingAgent element Id
     */

    public static org.osid.id.Id getBiddingAgent() {
        return (makeElementId("osid.bidding.bid.BiddingAgent"));
    }


    /**
     *  Gets the Quantity element Id.
     *
     *  @return the Quantity element Id
     */

    public static org.osid.id.Id getQuantity() {
        return (makeElementId("osid.bidding.bid.Quantity"));
    }


    /**
     *  Gets the CurrentBid element Id.
     *
     *  @return the CurrentBid element Id
     */

    public static org.osid.id.Id getCurrentBid() {
        return (makeElementId("osid.bidding.bid.CurrentBid"));
    }


    /**
     *  Gets the MaximumBid element Id.
     *
     *  @return the MaximumBid element Id
     */

    public static org.osid.id.Id getMaximumBid() {
        return (makeElementId("osid.bidding.bid.MaximumBid"));
    }


    /**
     *  Gets the SettlementAmount element Id.
     *
     *  @return the SettlementAmount element Id
     */

    public static org.osid.id.Id getSettlementAmount() {
        return (makeElementId("osid.bidding.bid.SettlementAmount"));
    }


    /**
     *  Gets the Winner element Id.
     *
     *  @return the Winner element Id
     */

    public static org.osid.id.Id getWinner() {
        return (makeElementId("osid.bidding.bid.Winner"));
    }


    /**
     *  Gets the AuctionHouseId element Id.
     *
     *  @return the AuctionHouseId element Id
     */

    public static org.osid.id.Id getAuctionHouseId() {
        return (makeQueryElementId("osid.bidding.bid.AuctionHouseId"));
    }


    /**
     *  Gets the AuctionHouse element Id.
     *
     *  @return the AuctionHouse element Id
     */

    public static org.osid.id.Id getAuctionHouse() {
        return (makeQueryElementId("osid.bidding.bid.AuctionHouse"));
    }
}
