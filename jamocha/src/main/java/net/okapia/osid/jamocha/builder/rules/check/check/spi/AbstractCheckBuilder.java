//
// AbstractCheck.java
//
//     Defines a Check builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.check.spi;


/**
 *  Defines a <code>Check</code> builder.
 */

public abstract class AbstractCheckBuilder<T extends AbstractCheckBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.rules.check.check.CheckMiter check;


    /**
     *  Constructs a new <code>AbstractCheckBuilder</code>.
     *
     *  @param check the check to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCheckBuilder(net.okapia.osid.jamocha.builder.rules.check.check.CheckMiter check) {
        super(check);
        this.check = check;
        return;
    }


    /**
     *  Builds the check.
     *
     *  @return the new check
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.rules.check.Check build() {
        (new net.okapia.osid.jamocha.builder.validator.rules.check.check.CheckValidator(getValidations())).validate(this.check);
        return (new net.okapia.osid.jamocha.builder.rules.check.check.ImmutableCheck(this.check));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the check miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.rules.check.check.CheckMiter getMiter() {
        return (this.check);
    }


    /**
     *  Sets the time check start date.
     *
     *  @param timeCheckDate a time check date
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>timeCheckDate</code> is <code>null</code>
     */

    public T timeCheckStartDate(org.osid.calendaring.DateTime timeCheckDate) {
        getMiter().setTimeCheckStartDate(timeCheckDate);
        return (self());
    }


    /**
     *  Sets the time check end date.
     *
     *  @param timeCheckDate a time check date
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>timeCheckDate</code> is <code>null</code>
     */

    public T timeCheckEndDate(org.osid.calendaring.DateTime timeCheckDate) {
        getMiter().setTimeCheckEndDate(timeCheckDate);
        return (self());
    }


    /**
     *  Sets the time check event.
     *
     *  @param timeCheckEvent a time check event
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>timeCheckEvent</code> is <code>null</code>
     */

    public T timeCheckEvent(org.osid.calendaring.Event timeCheckEvent) {
        getMiter().setTimeCheckEvent(timeCheckEvent);
        return (self());
    }


    /**
     *  Sets the time check cyclic event.
     *
     *  @param timeCheckCyclicEvent a time check cyclic event
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>timeCheckCyclicEvent</code> is <code>null</code>
     */

    public T timeCheckCyclicEvent(org.osid.calendaring.cycle.CyclicEvent timeCheckCyclicEvent) {
        getMiter().setTimeCheckCyclicEvent(timeCheckCyclicEvent);
        return (self());
    }


    /**
     *  Sets the hold check block.
     *
     *  @param holdCheckBlock a hold check block
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>holdCheckBlock</code> is <code>null</code>
     */

    public T holdCheckBlock(org.osid.hold.Block holdCheckBlock) {
        getMiter().setHoldCheckBlock(holdCheckBlock);
        return (self());
    }


    /**
     *  Sets the inquiry check audit.
     *
     *  @param inquiryCheckAudit an inquiry check audit
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryCheckAudit</code> is <code>null</code>
     */

    public T inquiryCheckAudit(org.osid.inquiry.Audit inquiryCheckAudit) {
        getMiter().setInquiryCheckAudit(inquiryCheckAudit);
        return (self());
    }


    /**
     *  Sets the process check agenda.
     *
     *  @param processCheckAgenda a process check agenda
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>processCheckAgenda</code> is <code>null</code>
     */

    public T processCheckAgenda(org.osid.rules.check.Agenda processCheckAgenda) {
        getMiter().setProcessCheckAgenda(processCheckAgenda);
        return (self());
    }


    /**
     *  Adds a Check record.
     *
     *  @param record a check record
     *  @param recordType the type of check record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.rules.check.records.CheckRecord record, org.osid.type.Type recordType) {
        getMiter().addCheckRecord(record, recordType);
        return (self());
    }
}       


