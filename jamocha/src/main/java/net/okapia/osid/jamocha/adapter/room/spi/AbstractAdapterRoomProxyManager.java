//
// AbstractRoomProxyManager.java
//
//     An adapter for a RoomProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RoomProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRoomProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.room.RoomProxyManager>
    implements org.osid.room.RoomProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterRoomProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRoomProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRoomProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRoomProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any building federation is exposed. Federation is exposed 
     *  when a specific building may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of buildinges appears as a single building. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a room lookup service. 
     *
     *  @return <code> true </code> if room lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomLookup() {
        return (getAdapteeManager().supportsRoomLookup());
    }


    /**
     *  Tests if querying rooms is available. 
     *
     *  @return <code> true </code> if room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (getAdapteeManager().supportsRoomQuery());
    }


    /**
     *  Tests if searching for rooms is available. 
     *
     *  @return <code> true </code> if room search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSearch() {
        return (getAdapteeManager().supportsRoomSearch());
    }


    /**
     *  Tests if searching for rooms is available. 
     *
     *  @return <code> true </code> if room search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomAdmin() {
        return (getAdapteeManager().supportsRoomAdmin());
    }


    /**
     *  Tests if room notification is available. 
     *
     *  @return <code> true </code> if room notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomNotification() {
        return (getAdapteeManager().supportsRoomNotification());
    }


    /**
     *  Tests if a room to campus lookup session is available. 
     *
     *  @return <code> true </code> if room campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomCampus() {
        return (getAdapteeManager().supportsRoomCampus());
    }


    /**
     *  Tests if a room to campus assignment session is available. 
     *
     *  @return <code> true </code> if room campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomCampusAssignment() {
        return (getAdapteeManager().supportsRoomCampusAssignment());
    }


    /**
     *  Tests if a room smart campus session is available. 
     *
     *  @return <code> true </code> if room smart campus is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSmartCampus() {
        return (getAdapteeManager().supportsRoomSmartCampus());
    }


    /**
     *  Tests for the availability of an floor lookup service. 
     *
     *  @return <code> true </code> if floor lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorLookup() {
        return (getAdapteeManager().supportsFloorLookup());
    }


    /**
     *  Tests if querying floores is available. 
     *
     *  @return <code> true </code> if floor query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorQuery() {
        return (getAdapteeManager().supportsFloorQuery());
    }


    /**
     *  Tests if searching for floores is available. 
     *
     *  @return <code> true </code> if floor search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorSearch() {
        return (getAdapteeManager().supportsFloorSearch());
    }


    /**
     *  Tests for the availability of a floor administrative service for 
     *  creating and deleting floores. 
     *
     *  @return <code> true </code> if floor administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorAdmin() {
        return (getAdapteeManager().supportsFloorAdmin());
    }


    /**
     *  Tests for the availability of a floor notification service. 
     *
     *  @return <code> true </code> if floor notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorNotification() {
        return (getAdapteeManager().supportsFloorNotification());
    }


    /**
     *  Tests if a floor to campus lookup session is available. 
     *
     *  @return <code> true </code> if floor campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorCampus() {
        return (getAdapteeManager().supportsFloorCampus());
    }


    /**
     *  Tests if a floor to campus assignment session is available. 
     *
     *  @return <code> true </code> if floor campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorCampusAssignment() {
        return (getAdapteeManager().supportsFloorCampusAssignment());
    }


    /**
     *  Tests if a floor smart campus session is available. 
     *
     *  @return <code> true </code> if floor smart campus is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorSmartCampus() {
        return (getAdapteeManager().supportsFloorSmartCampus());
    }


    /**
     *  Tests for the availability of an building lookup service. 
     *
     *  @return <code> true </code> if building lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingLookup() {
        return (getAdapteeManager().supportsBuildingLookup());
    }


    /**
     *  Tests if querying buildinges is available. 
     *
     *  @return <code> true </code> if building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (getAdapteeManager().supportsBuildingQuery());
    }


    /**
     *  Tests if searching for buildinges is available. 
     *
     *  @return <code> true </code> if building search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSearch() {
        return (getAdapteeManager().supportsBuildingSearch());
    }


    /**
     *  Tests for the availability of a building administrative service for 
     *  creating and deleting buildinges. 
     *
     *  @return <code> true </code> if building administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingAdmin() {
        return (getAdapteeManager().supportsBuildingAdmin());
    }


    /**
     *  Tests for the availability of a building notification service. 
     *
     *  @return <code> true </code> if building notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingNotification() {
        return (getAdapteeManager().supportsBuildingNotification());
    }


    /**
     *  Tests if a building to campus lookup session is available. 
     *
     *  @return <code> true </code> if building campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingCampus() {
        return (getAdapteeManager().supportsBuildingCampus());
    }


    /**
     *  Tests if a building to campus assignment session is available. 
     *
     *  @return <code> true </code> if building campus assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingCampusAssignment() {
        return (getAdapteeManager().supportsBuildingCampusAssignment());
    }


    /**
     *  Tests if a building smart campus session is available. 
     *
     *  @return <code> true </code> if building smart campus is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSmartCampus() {
        return (getAdapteeManager().supportsBuildingSmartCampus());
    }


    /**
     *  Tests for the availability of an campus lookup service. 
     *
     *  @return <code> true </code> if campus lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusLookup() {
        return (getAdapteeManager().supportsCampusLookup());
    }


    /**
     *  Tests if querying campuses is available. 
     *
     *  @return <code> true </code> if campus query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (getAdapteeManager().supportsCampusQuery());
    }


    /**
     *  Tests if searching for campuses is available. 
     *
     *  @return <code> true </code> if campus search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusSearch() {
        return (getAdapteeManager().supportsCampusSearch());
    }


    /**
     *  Tests for the availability of a campus administrative service for 
     *  creating and deleting campuses. 
     *
     *  @return <code> true </code> if campus administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusAdmin() {
        return (getAdapteeManager().supportsCampusAdmin());
    }


    /**
     *  Tests for the availability of a campus notification service. 
     *
     *  @return <code> true </code> if campus notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusNotification() {
        return (getAdapteeManager().supportsCampusNotification());
    }


    /**
     *  Tests for the availability of a campus hierarchy traversal service. 
     *
     *  @return <code> true </code> if campus hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusHierarchy() {
        return (getAdapteeManager().supportsCampusHierarchy());
    }


    /**
     *  Tests for the availability of a campus hierarchy design service. 
     *
     *  @return <code> true </code> if campus hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusHierarchyDesign() {
        return (getAdapteeManager().supportsCampusHierarchyDesign());
    }


    /**
     *  Tests for the availability of a room batch service. 
     *
     *  @return <code> true </code> if a room batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomBatch() {
        return (getAdapteeManager().supportsRoomBatch());
    }


    /**
     *  Tests for the availability of a room construction service. 
     *
     *  @return <code> true </code> if a room construction service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomConstruction() {
        return (getAdapteeManager().supportsRoomConstruction());
    }


    /**
     *  Tests for the availability of a room squatting service. 
     *
     *  @return <code> true </code> if a room squatting service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSquatting() {
        return (getAdapteeManager().supportsRoomSquatting());
    }


    /**
     *  Gets the supported <code> Room </code> record types. 
     *
     *  @return a list containing the supported room record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRoomRecordTypes() {
        return (getAdapteeManager().getRoomRecordTypes());
    }


    /**
     *  Tests if the given <code> Room </code> record type is supported. 
     *
     *  @param  roomRecordType a <code> Type </code> indicating a <code> Room 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> roomRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRoomRecordType(org.osid.type.Type roomRecordType) {
        return (getAdapteeManager().supportsRoomRecordType(roomRecordType));
    }


    /**
     *  Gets the supported room search record types. 
     *
     *  @return a list containing the supported room search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRoomSearchRecordTypes() {
        return (getAdapteeManager().getRoomSearchRecordTypes());
    }


    /**
     *  Tests if the given room search record type is supported. 
     *
     *  @param  roomSearchRecordType a <code> Type </code> indicating a room 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> roomSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRoomSearchRecordType(org.osid.type.Type roomSearchRecordType) {
        return (getAdapteeManager().supportsRoomSearchRecordType(roomSearchRecordType));
    }


    /**
     *  Gets the supported <code> Floor </code> record types. 
     *
     *  @return a list containing the supported floor record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFloorRecordTypes() {
        return (getAdapteeManager().getFloorRecordTypes());
    }


    /**
     *  Tests if the given <code> Floor </code> record type is supported. 
     *
     *  @param  floorRecordType a <code> Type </code> indicating a <code> 
     *          Floor </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> floorRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFloorRecordType(org.osid.type.Type floorRecordType) {
        return (getAdapteeManager().supportsFloorRecordType(floorRecordType));
    }


    /**
     *  Gets the supported floor search record types. 
     *
     *  @return a list containing the supported floor search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFloorSearchRecordTypes() {
        return (getAdapteeManager().getFloorSearchRecordTypes());
    }


    /**
     *  Tests if the given floor search record type is supported. 
     *
     *  @param  floorSearchRecordType a <code> Type </code> indicating a floor 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> floorSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFloorSearchRecordType(org.osid.type.Type floorSearchRecordType) {
        return (getAdapteeManager().supportsFloorSearchRecordType(floorSearchRecordType));
    }


    /**
     *  Gets the supported <code> Building </code> record types. 
     *
     *  @return a list containing the supported building record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBuildingRecordTypes() {
        return (getAdapteeManager().getBuildingRecordTypes());
    }


    /**
     *  Tests if the given <code> Building </code> record type is supported. 
     *
     *  @param  buildingRecordType a <code> Type </code> indicating a <code> 
     *          Building </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> buildingRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBuildingRecordType(org.osid.type.Type buildingRecordType) {
        return (getAdapteeManager().supportsBuildingRecordType(buildingRecordType));
    }


    /**
     *  Gets the supported building search record types. 
     *
     *  @return a list containing the supported building search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBuildingSearchRecordTypes() {
        return (getAdapteeManager().getBuildingSearchRecordTypes());
    }


    /**
     *  Tests if the given building search record type is supported. 
     *
     *  @param  buildingSearchRecordType a <code> Type </code> indicating a 
     *          building record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> buildingSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBuildingSearchRecordType(org.osid.type.Type buildingSearchRecordType) {
        return (getAdapteeManager().supportsBuildingSearchRecordType(buildingSearchRecordType));
    }


    /**
     *  Gets the supported <code> Campus </code> record types. 
     *
     *  @return a list containing the supported campus record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCampusRecordTypes() {
        return (getAdapteeManager().getCampusRecordTypes());
    }


    /**
     *  Tests if the given <code> Campus </code> record type is supported. 
     *
     *  @param  campusRecordType a <code> Type </code> indicating a <code> 
     *          Campus </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> campusRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCampusRecordType(org.osid.type.Type campusRecordType) {
        return (getAdapteeManager().supportsCampusRecordType(campusRecordType));
    }


    /**
     *  Gets the supported campus search record types. 
     *
     *  @return a list containing the supported campus search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCampusSearchRecordTypes() {
        return (getAdapteeManager().getCampusSearchRecordTypes());
    }


    /**
     *  Tests if the given campus search record type is supported. 
     *
     *  @param  campusSearchRecordType a <code> Type </code> indicating a 
     *          campus record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> campusSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCampusSearchRecordType(org.osid.type.Type campusSearchRecordType) {
        return (getAdapteeManager().supportsCampusSearchRecordType(campusSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomLookupSession getRoomLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomLookupSession getRoomLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomLookupSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuerySession getRoomQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuerySession getRoomQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomQuerySessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchSession getRoomSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchSession getRoomSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomSearchSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomAdminSession getRoomAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomAdminSession getRoomAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomAdminSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  notification service. 
     *
     *  @param  roomReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> RoomNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> roomReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomNotificationSession getRoomNotificationSession(org.osid.room.RoomReceiver roomReceiver, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomNotificationSession(roomReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  notification service for the given campus. 
     *
     *  @param  roomReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> roomReceiver, campusId, 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomNotificationSession getRoomNotificationSessionForCampus(org.osid.room.RoomReceiver roomReceiver, 
                                                                                     org.osid.id.Id campusId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomNotificationSessionForCampus(roomReceiver, campusId, proxy));
    }


    /**
     *  Gets the session for retrieving room to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomCampusSession getRoomCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomCampusSession(proxy));
    }


    /**
     *  Gets the session for assigning room to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomCampusAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomCampusAssignmentSession getRoomCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomCampusAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the room smart campus for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @param  proxy a proxy 
     *  @return a <code> RoomSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSmartCampusSession getRoomSmartCampusSession(org.osid.id.Id campusId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomSmartCampusSession(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorLookupSession getFloorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorLookupSession getFloorLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorLookupSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuerySession getFloorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Floor </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuerySession getFloorQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorQuerySessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSearchSession getFloorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Floor </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSearchSession getFloorSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorSearchSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorAdminSession getFloorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Floor </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorAdminSession getFloorAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorAdminSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  notification service. 
     *
     *  @param  floorReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> FloorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> floorReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorNotificationSession getFloorNotificationSession(org.osid.room.FloorReceiver floorReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorNotificationSession(floorReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  notification service for the given campus. 
     *
     *  @param  floorReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Floor </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> floorReceiver, campusId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorNotificationSession getFloorNotificationSessionForCampus(org.osid.room.FloorReceiver floorReceiver, 
                                                                                       org.osid.id.Id campusId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorNotificationSessionForCampus(floorReceiver, campusId, proxy));
    }


    /**
     *  Gets the session for retrieving floor to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorCampusSession getFloorCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorCampusSession(proxy));
    }


    /**
     *  Gets the session for assigning floor to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.FloorCampusAssignmentSession getFloorCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorCampusAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic floor campuses for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of a campus 
     *  @param  proxy a proxy 
     *  @return a <code> FloorSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSmartCampusSession getFloorSmartCampusSession(org.osid.id.Id campusId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFloorSmartCampusSession(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingLookupSession getBuildingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  lookup service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingLookupSession getBuildingLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingLookupSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuerySession getBuildingQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Building </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuerySession getBuildingQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingQuerySessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchSession getBuildingSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  search service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Building </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchSession getBuildingSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingSearchSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingAdminSession getBuildingAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Building </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingAdminSession getBuildingAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingAdminSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  notification service. 
     *
     *  @param  buildingReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> buildingReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingNotificationSession getBuildingNotificationSession(org.osid.room.BuildingReceiver buildingReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingNotificationSession(buildingReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  notification service for the given campus. 
     *
     *  @param  buildingReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Building </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> buildingReceiver, 
     *          campusId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingNotificationSession getBuildingNotificationSessionForCampus(org.osid.room.BuildingReceiver buildingReceiver, 
                                                                                             org.osid.id.Id campusId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingNotificationSessionForCampus(buildingReceiver, campusId, proxy));
    }


    /**
     *  Gets the session for retrieving building to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingCampusSession getBuildingCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingCampusSession(proxy));
    }


    /**
     *  Gets the session for assigning building to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingCampusAssignmentSession getBuildingCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingCampusAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic building campuses for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of a campus 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSmartCampusSession getBuildingSmartCampusSession(org.osid.id.Id campusId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBuildingSmartCampusSession(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusLookupSession getCampusLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCampusLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuerySession getCampusQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCampusQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusSearchSession getCampusSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCampusSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusAdminSession getCampusAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCampusAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  notification service. 
     *
     *  @param  campusReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CampusNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> campusReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusNotificationSession getCampusNotificationSession(org.osid.room.CampusReceiver campusReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCampusNotificationSession(campusReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusHierarchySession getCampusHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCampusHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.CampusHierarchyDesignSession getCampusHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCampusHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the <code> RoomBatchProxyManager. </code> 
     *
     *  @return a <code> RoomBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.RoomBatchProxyManager getRoomBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomBatchProxyManager());
    }


    /**
     *  Gets the <code> RoomConstructionProxyManager. </code> 
     *
     *  @return a <code> RoomConstructionProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomConstruction() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RoomConstructionProxyManager getRoomConstructionProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomConstructionProxyManager());
    }


    /**
     *  Gets the <code> RoomSquattingProxyManager. </code> 
     *
     *  @return a <code> RoomSquattingProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomSquatting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.RoomSquattingProxyManager getRoomSquattingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomSquattingProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
