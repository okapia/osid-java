//
// AbstractMailboxSearch.java
//
//     A template for making a Mailbox Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.mailbox.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing mailbox searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractMailboxSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.messaging.MailboxSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.messaging.records.MailboxSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.messaging.MailboxSearchOrder mailboxSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of mailboxes. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  mailboxIds list of mailboxes
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongMailboxes(org.osid.id.IdList mailboxIds) {
        while (mailboxIds.hasNext()) {
            try {
                this.ids.add(mailboxIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongMailboxes</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of mailbox Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getMailboxIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  mailboxSearchOrder mailbox search order 
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>mailboxSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderMailboxResults(org.osid.messaging.MailboxSearchOrder mailboxSearchOrder) {
	this.mailboxSearchOrder = mailboxSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.messaging.MailboxSearchOrder getMailboxSearchOrder() {
	return (this.mailboxSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given mailbox search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a mailbox implementing the requested record.
     *
     *  @param mailboxSearchRecordType a mailbox search record
     *         type
     *  @return the mailbox search record
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mailboxSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MailboxSearchRecord getMailboxSearchRecord(org.osid.type.Type mailboxSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.messaging.records.MailboxSearchRecord record : this.records) {
            if (record.implementsRecordType(mailboxSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mailboxSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this mailbox search. 
     *
     *  @param mailboxSearchRecord mailbox search record
     *  @param mailboxSearchRecordType mailbox search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMailboxSearchRecord(org.osid.messaging.records.MailboxSearchRecord mailboxSearchRecord, 
                                           org.osid.type.Type mailboxSearchRecordType) {

        addRecordType(mailboxSearchRecordType);
        this.records.add(mailboxSearchRecord);        
        return;
    }
}
