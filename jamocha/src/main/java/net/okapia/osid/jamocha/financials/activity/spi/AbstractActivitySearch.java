//
// AbstractActivitySearch.java
//
//     A template for making an Activity Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.activity.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing activity searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractActivitySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.financials.ActivitySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.financials.records.ActivitySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.financials.ActivitySearchOrder activitySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of activities. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  activityIds list of activities
     *  @throws org.osid.NullArgumentException
     *          <code>activityIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongActivities(org.osid.id.IdList activityIds) {
        while (activityIds.hasNext()) {
            try {
                this.ids.add(activityIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongActivities</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of activity Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getActivityIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  activitySearchOrder activity search order 
     *  @throws org.osid.NullArgumentException
     *          <code>activitySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>activitySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderActivityResults(org.osid.financials.ActivitySearchOrder activitySearchOrder) {
	this.activitySearchOrder = activitySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.financials.ActivitySearchOrder getActivitySearchOrder() {
	return (this.activitySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given activity search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an activity implementing the requested record.
     *
     *  @param activitySearchRecordType an activity search record
     *         type
     *  @return the activity search record
     *  @throws org.osid.NullArgumentException
     *          <code>activitySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activitySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.ActivitySearchRecord getActivitySearchRecord(org.osid.type.Type activitySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.financials.records.ActivitySearchRecord record : this.records) {
            if (record.implementsRecordType(activitySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activitySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity search. 
     *
     *  @param activitySearchRecord activity search record
     *  @param activitySearchRecordType activity search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivitySearchRecord(org.osid.financials.records.ActivitySearchRecord activitySearchRecord, 
                                           org.osid.type.Type activitySearchRecordType) {

        addRecordType(activitySearchRecordType);
        this.records.add(activitySearchRecord);        
        return;
    }
}
