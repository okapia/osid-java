//
// AbstractSiteQuery.java
//
//     A template for making a Site Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.site.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for sites.
 */

public abstract class AbstractSiteQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.installation.SiteQuery {

    private final java.util.Collection<org.osid.installation.records.SiteQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the installation <code> Id </code> for this query. 
     *
     *  @param  installationId a site <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> installationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchInstallationId(org.osid.id.Id installationId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the installation <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInstallationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InstallationQuery </code> is available for querying 
     *  installations. 
     *
     *  @return <code> true </code> if an installation query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an installation. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the site query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.SiteQuery getInstallationQuery() {
        throw new org.osid.UnimplementedException("supportsInstallationQuery() is false");
    }


    /**
     *  Matches sites with any installation. 
     *
     *  @param  match <code> true </code> to match sites with any package, 
     *          <code> false </code> to match sites with no packages 
     */

    @OSID @Override
    public void matchAnyInstallation(boolean match) {
        return;
    }


    /**
     *  Clears the installation query terms. 
     */

    @OSID @Override
    public void clearInstallationTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given site query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a site implementing the requested record.
     *
     *  @param siteRecordType a site record type
     *  @return the site query record
     *  @throws org.osid.NullArgumentException
     *          <code>siteRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(siteRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.SiteQueryRecord getSiteQueryRecord(org.osid.type.Type siteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.SiteQueryRecord record : this.records) {
            if (record.implementsRecordType(siteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(siteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this site query. 
     *
     *  @param siteQueryRecord site query record
     *  @param siteRecordType site record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSiteQueryRecord(org.osid.installation.records.SiteQueryRecord siteQueryRecord, 
                                          org.osid.type.Type siteRecordType) {

        addRecordType(siteRecordType);
        nullarg(siteQueryRecord, "site query record");
        this.records.add(siteQueryRecord);        
        return;
    }
}
