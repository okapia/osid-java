//
// AbstractAssemblyAddressBookQuery.java
//
//     An AddressBookQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.contact.addressbook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AddressBookQuery that stores terms.
 */

public abstract class AbstractAssemblyAddressBookQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.contact.AddressBookQuery,
               org.osid.contact.AddressBookQueryInspector,
               org.osid.contact.AddressBookSearchOrder {

    private final java.util.Collection<org.osid.contact.records.AddressBookQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.contact.records.AddressBookQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.contact.records.AddressBookSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAddressBookQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAddressBookQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the contact <code> Id </code> for this query to match contacts 
     *  assigned to address books. 
     *
     *  @param  contactId a contact <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> contactId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContactId(org.osid.id.Id contactId, boolean match) {
        getAssembler().addIdTerm(getContactIdColumn(), contactId, match);
        return;
    }


    /**
     *  Clears the contact <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContactIdTerms() {
        getAssembler().clearTerms(getContactIdColumn());
        return;
    }


    /**
     *  Gets the contact <code> Id </code> terms. 
     *
     *  @return the contact <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContactIdTerms() {
        return (getAssembler().getIdTerms(getContactIdColumn()));
    }


    /**
     *  Gets the ContactId column name.
     *
     * @return the column name
     */

    protected String getContactIdColumn() {
        return ("contact_id");
    }


    /**
     *  Tests if a contact query is available. 
     *
     *  @return <code> true </code> if a contact query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book. 
     *
     *  @return the contact query 
     *  @throws org.osid.UnimplementedException <code> supportsContactQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactQuery getContactQuery() {
        throw new org.osid.UnimplementedException("supportsContactQuery() is false");
    }


    /**
     *  Matches address books with any contact. 
     *
     *  @param  match <code> true </code> to match address books with any 
     *          contact, <code> false </code> to match address books with no 
     *          contacts 
     */

    @OSID @Override
    public void matchAnyContact(boolean match) {
        getAssembler().addIdWildcardTerm(getContactColumn(), match);
        return;
    }


    /**
     *  Clears the contact terms. 
     */

    @OSID @Override
    public void clearContactTerms() {
        getAssembler().clearTerms(getContactColumn());
        return;
    }


    /**
     *  Gets the contact terms. 
     *
     *  @return the contact terms 
     */

    @OSID @Override
    public org.osid.contact.ContactQueryInspector[] getContactTerms() {
        return (new org.osid.contact.ContactQueryInspector[0]);
    }


    /**
     *  Gets the Contact column name.
     *
     * @return the column name
     */

    protected String getContactColumn() {
        return ("contact");
    }


    /**
     *  Sets the address <code> Id </code> for this query to match contacts 
     *  assigned to addresses. 
     *
     *  @param  addressId an address <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressId(org.osid.id.Id addressId, boolean match) {
        getAssembler().addIdTerm(getAddressIdColumn(), addressId, match);
        return;
    }


    /**
     *  Clears the address <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressIdTerms() {
        getAssembler().clearTerms(getAddressIdColumn());
        return;
    }


    /**
     *  Gets the address <code> Id </code> terms. 
     *
     *  @return the address <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressIdTerms() {
        return (getAssembler().getIdTerms(getAddressIdColumn()));
    }


    /**
     *  Gets the AddressId column name.
     *
     * @return the column name
     */

    protected String getAddressIdColumn() {
        return ("address_id");
    }


    /**
     *  Tests if an <code> AddressQuery </code> is available. 
     *
     *  @return <code> true </code> if an address query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address query 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuery getAddressQuery() {
        throw new org.osid.UnimplementedException("supportsAddressQuery() is false");
    }


    /**
     *  Matches address books with any address. 
     *
     *  @param  match <code> true </code> to match address books with any 
     *          address, <code> false </code> to match address books with no 
     *          addresses 
     */

    @OSID @Override
    public void matchAnyAddress(boolean match) {
        getAssembler().addIdWildcardTerm(getAddressColumn(), match);
        return;
    }


    /**
     *  Clears the address terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        getAssembler().clearTerms(getAddressColumn());
        return;
    }


    /**
     *  Gets the address terms. 
     *
     *  @return the address terms 
     */

    @OSID @Override
    public org.osid.contact.AddressQueryInspector[] getAddressTerms() {
        return (new org.osid.contact.AddressQueryInspector[0]);
    }


    /**
     *  Gets the Address column name.
     *
     * @return the column name
     */

    protected String getAddressColumn() {
        return ("address");
    }


    /**
     *  Sets the address book <code> Id </code> for this query to match 
     *  address books that have the specified address book as an ancestor. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAddressBookId(org.osid.id.Id addressBookId, 
                                           boolean match) {
        getAssembler().addIdTerm(getAncestorAddressBookIdColumn(), addressBookId, match);
        return;
    }


    /**
     *  Clears the ancestor address book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorAddressBookIdTerms() {
        getAssembler().clearTerms(getAncestorAddressBookIdColumn());
        return;
    }


    /**
     *  Gets the ancestor address book <code> Id </code> terms. 
     *
     *  @return the ancestor address book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAddressBookIdTerms() {
        return (getAssembler().getIdTerms(getAncestorAddressBookIdColumn()));
    }


    /**
     *  Gets the AncestorAddressBookId column name.
     *
     * @return the column name
     */

    protected String getAncestorAddressBookIdColumn() {
        return ("ancestor_address_book_id");
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAddressBookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getAncestorAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAddressBookQuery() is false");
    }


    /**
     *  Matches address books with any ancestor. 
     *
     *  @param  match <code> true </code> to match address books with any 
     *          ancestor, <code> false </code> to match root address books 
     */

    @OSID @Override
    public void matchAnyAncestorAddressBook(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorAddressBookColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor address book terms. 
     */

    @OSID @Override
    public void clearAncestorAddressBookTerms() {
        getAssembler().clearTerms(getAncestorAddressBookColumn());
        return;
    }


    /**
     *  Gets the ancestor address book terms. 
     *
     *  @return the ancestor address book terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getAncestorAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }


    /**
     *  Gets the AncestorAddressBook column name.
     *
     * @return the column name
     */

    protected String getAncestorAddressBookColumn() {
        return ("ancestor_address_book");
    }


    /**
     *  Sets the address book <code> Id </code> for this query to match 
     *  address books that have the specified address book as a descendant. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAddressBookId(org.osid.id.Id addressBookId, 
                                             boolean match) {
        getAssembler().addIdTerm(getDescendantAddressBookIdColumn(), addressBookId, match);
        return;
    }


    /**
     *  Clears the descendant address book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantAddressBookIdTerms() {
        getAssembler().clearTerms(getDescendantAddressBookIdColumn());
        return;
    }


    /**
     *  Gets the descendant address book <code> Id </code> terms. 
     *
     *  @return the descendant address book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAddressBookIdTerms() {
        return (getAssembler().getIdTerms(getDescendantAddressBookIdColumn()));
    }


    /**
     *  Gets the DescendantAddressBookId column name.
     *
     * @return the column name
     */

    protected String getDescendantAddressBookIdColumn() {
        return ("descendant_address_book_id");
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAddressBookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getDescendantAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAddressBookQuery() is false");
    }


    /**
     *  Matches address books with any descendant. 
     *
     *  @param  match <code> true </code> to match address books with any 
     *          descendant, <code> false </code> to match leaf address books 
     */

    @OSID @Override
    public void matchAnyDescendantAddressBook(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantAddressBookColumn(), match);
        return;
    }


    /**
     *  Clears the descendant address book terms. 
     */

    @OSID @Override
    public void clearDescendantAddressBookTerms() {
        getAssembler().clearTerms(getDescendantAddressBookColumn());
        return;
    }


    /**
     *  Gets the descendant address book terms. 
     *
     *  @return the descendant address book terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getDescendantAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }


    /**
     *  Gets the DescendantAddressBook column name.
     *
     * @return the column name
     */

    protected String getDescendantAddressBookColumn() {
        return ("descendant_address_book");
    }


    /**
     *  Tests if this addressBook supports the given record
     *  <code>Type</code>.
     *
     *  @param  addressBookRecordType an address book record type 
     *  @return <code>true</code> if the addressBookRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type addressBookRecordType) {
        for (org.osid.contact.records.AddressBookQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(addressBookRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  addressBookRecordType the address book record type 
     *  @return the address book query record 
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressBookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressBookQueryRecord getAddressBookQueryRecord(org.osid.type.Type addressBookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressBookQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(addressBookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressBookRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  addressBookRecordType the address book record type 
     *  @return the address book query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressBookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressBookQueryInspectorRecord getAddressBookQueryInspectorRecord(org.osid.type.Type addressBookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressBookQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(addressBookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressBookRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param addressBookRecordType the address book record type
     *  @return the address book search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressBookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressBookSearchOrderRecord getAddressBookSearchOrderRecord(org.osid.type.Type addressBookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressBookSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(addressBookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressBookRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this address book. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param addressBookQueryRecord the address book query record
     *  @param addressBookQueryInspectorRecord the address book query inspector
     *         record
     *  @param addressBookSearchOrderRecord the address book search order record
     *  @param addressBookRecordType address book record type
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookQueryRecord</code>,
     *          <code>addressBookQueryInspectorRecord</code>,
     *          <code>addressBookSearchOrderRecord</code> or
     *          <code>addressBookRecordTypeaddressBook</code> is
     *          <code>null</code>
     */
            
    protected void addAddressBookRecords(org.osid.contact.records.AddressBookQueryRecord addressBookQueryRecord, 
                                      org.osid.contact.records.AddressBookQueryInspectorRecord addressBookQueryInspectorRecord, 
                                      org.osid.contact.records.AddressBookSearchOrderRecord addressBookSearchOrderRecord, 
                                      org.osid.type.Type addressBookRecordType) {

        addRecordType(addressBookRecordType);

        nullarg(addressBookQueryRecord, "address book query record");
        nullarg(addressBookQueryInspectorRecord, "address book query inspector record");
        nullarg(addressBookSearchOrderRecord, "address book search odrer record");

        this.queryRecords.add(addressBookQueryRecord);
        this.queryInspectorRecords.add(addressBookQueryInspectorRecord);
        this.searchOrderRecords.add(addressBookSearchOrderRecord);
        
        return;
    }
}
