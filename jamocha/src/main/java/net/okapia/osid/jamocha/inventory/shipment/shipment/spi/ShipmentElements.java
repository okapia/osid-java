//
// ShipmentElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.shipment.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ShipmentElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ShipmentElement Id.
     *
     *  @return the shipment element Id
     */

    public static org.osid.id.Id getShipmentEntityId() {
        return (makeEntityId("osid.inventory.shipment.Shipment"));
    }


    /**
     *  Gets the SourceId element Id.
     *
     *  @return the SourceId element Id
     */

    public static org.osid.id.Id getSourceId() {
        return (makeElementId("osid.inventory.shipment.shipment.SourceId"));
    }


    /**
     *  Gets the Source element Id.
     *
     *  @return the Source element Id
     */

    public static org.osid.id.Id getSource() {
        return (makeElementId("osid.inventory.shipment.shipment.Source"));
    }


    /**
     *  Gets the OrderId element Id.
     *
     *  @return the OrderId element Id
     */

    public static org.osid.id.Id getOrderId() {
        return (makeElementId("osid.inventory.shipment.shipment.OrderId"));
    }


    /**
     *  Gets the Order element Id.
     *
     *  @return the Order element Id
     */

    public static org.osid.id.Id getOrder() {
        return (makeElementId("osid.inventory.shipment.shipment.Order"));
    }


    /**
     *  Gets the Date element Id.
     *
     *  @return the Date element Id
     */

    public static org.osid.id.Id getDate() {
        return (makeElementId("osid.inventory.shipment.shipment.Date"));
    }


    /**
     *  Gets the EntryIds element Id.
     *
     *  @return the EntryIds element Id
     */

    public static org.osid.id.Id getEntryIds() {
        return (makeElementId("osid.inventory.shipment.shipment.EntryIds"));
    }


    /**
     *  Gets the Entries element Id.
     *
     *  @return the Entries element Id
     */

    public static org.osid.id.Id getEntries() {
        return (makeElementId("osid.inventory.shipment.shipment.Entries"));
    }


    /**
     *  Gets the WarehouseId element Id.
     *
     *  @return the WarehouseId element Id
     */

    public static org.osid.id.Id getWarehouseId() {
        return (makeQueryElementId("osid.inventory.shipment.shipment.WarehouseId"));
    }


    /**
     *  Gets the Warehouse element Id.
     *
     *  @return the Warehouse element Id
     */

    public static org.osid.id.Id getWarehouse() {
        return (makeQueryElementId("osid.inventory.shipment.shipment.Warehouse"));
    }
}
