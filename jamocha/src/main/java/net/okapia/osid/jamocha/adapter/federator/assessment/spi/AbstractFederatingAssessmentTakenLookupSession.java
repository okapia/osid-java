//
// AbstractFederatingAssessmentTakenLookupSession.java
//
//     An abstract federating adapter for an AssessmentTakenLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AssessmentTakenLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAssessmentTakenLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.assessment.AssessmentTakenLookupSession>
    implements org.osid.assessment.AssessmentTakenLookupSession {

    private boolean parallel = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();


    /**
     *  Constructs a new <code>AbstractFederatingAssessmentTakenLookupSession</code>.
     */

    protected AbstractFederatingAssessmentTakenLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.assessment.AssessmentTakenLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }


    /**
     *  Tests if this user can perform <code>AssessmentTaken</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessmentsTaken() {
        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            if (session.canLookupAssessmentsTaken()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AssessmentTaken</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentTakenView() {
        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            session.useComparativeAssessmentTakenView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AssessmentTaken</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentTakenView() {
        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            session.usePlenaryAssessmentTakenView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments taken in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            session.useFederatedBankView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            session.useIsolatedBankView();
        }

        return;
    }

     
    /**
     *  Gets the <code>AssessmentTaken</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentTaken</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AssessmentTaken</code> and
     *  retained for compatibility.
     *
     *  @param  assessmentTakenId <code>Id</code> of the
     *          <code>AssessmentTaken</code>
     *  @return the assessment taken
     *  @throws org.osid.NotFoundException <code>assessmentTakenId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentTakenId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTaken getAssessmentTaken(org.osid.id.Id assessmentTakenId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            try {
                return (session.getAssessmentTaken(assessmentTakenId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(assessmentTakenId + " not found");
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentsTaken specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>AssessmentsTaken</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  @param  assessmentTakenIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByIds(org.osid.id.IdList assessmentTakenIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.assessment.assessmenttaken.MutableAssessmentTakenList ret = new net.okapia.osid.jamocha.assessment.assessmenttaken.MutableAssessmentTakenList();

        try (org.osid.id.IdList ids = assessmentTakenIds) {
            while (ids.hasNext()) {
                ret.addAssessmentTaken(getAssessmentTaken(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the given
     *  assessment taken genus <code>Type</code> which does not include
     *  assessments taken of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentTakenGenusType an assessmentTaken genus type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByGenusType(org.osid.type.Type assessmentTakenGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenByGenusType(assessmentTakenGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the given
     *  assessment taken genus <code>Type</code> and include any additional
     *  assessments taken with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentTakenGenusType an assessmentTaken genus type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByParentGenusType(org.osid.type.Type assessmentTakenGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenByParentGenusType(assessmentTakenGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> containing the given
     *  assessment taken record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentTakenRecordType an assessmentTaken record type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByRecordType(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenByRecordType(assessmentTakenRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> started in the given
     *  date range inclusive. In plenary mode, the returned list
     *  contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */
    
    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDate(org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> for the given resource. In
     *  plenary mode, the returned list contains all known assessments offered
     *  or an error results. Otherwise, the returned list may contain only
     *  those assessments offered that are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTaker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenForTaker(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given resource. In plenary
     *  mode, the returned list contains all known assessments offered
     *  or an error results.  Otherwise, the returned list may contain
     *  only those assessments offered that are accessible through
     *  this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from </code>
     *          or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTaker(org.osid.id.Id resourceId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenByDateForTaker(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> for the given
     *  assessment. In plenary mode, the returned list contains all
     *  known assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session.
     *
     *  @param  assessmentId <code> Id </code> of an <code>Assessment</code>
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenForAssessment(assessmentId));
        }

        ret.noMore();
        return (ret);
    }        


    /**
     *  Gets an <code>AssessmentTakenList</code> started in the given
     *  date range inclusive for the given assessment. In plenary
     *  mode, the returned list contains all known assessments offered
     *  or an error results. Otherwise, the returned list may contain
     *  only those assessments offered that are accessible through
     *  this session.
     *
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> assessmentId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForAssessment(org.osid.id.Id assessmentId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenByDateForAssessment(assessmentId, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets an <code> AssessmentTakenList </code> for the given
     *  resource and assessment. In plenary mode, the returned list
     *  contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code>
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> or <code> assessmentId </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */
    
    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTakerAndAssessment(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenForTakerAndAssessment(resourceId, assessmentId));
        }

        ret.noMore();
        return (ret);
    }        

    
    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given resource and
     *  assessment. In plenary mode, the returned list contains all
     *  known assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          assessmentId, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTakerAndAssessment(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id assessmentId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
    
        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenByDateForTakerAndAssessment(resourceId, assessmentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> by the given
     *  assessment offered. In plenary mode, the returned list
     *  contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this session.
     *
     *  @param  assessmentOfferedId <code> Id </code> of an <code>
     *          AssessmentOffered </code>
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForAssessmentOffered(org.osid.id.Id assessmentOfferedId) 
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenForAssessmentOffered(assessmentOfferedId));
        }

        ret.noMore();
        return (ret);
    }                


    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given assessment
     *  offered. In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session.
     *
     *  @param assessmentOfferedId <code> Id </code> of an <code>
     *          AssessmentOffered </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>
     *          assessmentOfferedId, from </code> or <code> to </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForAssessmentOffered(org.osid.id.Id assessmentOfferedId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenByDateForAssessmentOffered(assessmentOfferedId, from, to));
        }

        ret.noMore();
        return (ret);
    }        


    /**
     *  Gets an <code>AssessmentTakenList</code> for the given
     *  resource and assessment offered. In plenary mode, the returned
     *  list contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this session.
     *
     *  @param  resourceId <code>Id</code> of a <code>Resource</code>
     *  @param  assessmentOfferedId <code>Id</code> of an 
     *          <code>AssessmentOffered</code>
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>assessmenOfferedtId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTakerAndAssessmentOffered(org.osid.id.Id resourceId,
                                                                                                   org.osid.id.Id assessmentOfferedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenForTakerAndAssessmentOffered(resourceId, assessmentOfferedId));
        }

        ret.noMore();
        return (ret);
    }        
    

    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given resource and
     *  assessment offered. In plenary mode, the returned list
     *  contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param assessmentOfferedId <code> Id </code> of an <code>
     *         AssessmentOffered </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          assessmentOfferedId, from </code> or <code> to </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTakerAndAssessmentOffered(org.osid.id.Id resourceId,
                                                                                                         org.osid.id.Id assessmentOfferedId,
                                                                                                         org.osid.calendaring.DateTime from,
                                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTakenByDateForTakerAndAssessmentOffered(resourceId, assessmentOfferedId, from, to));
        }

        ret.noMore();
        return (ret);
    }        

    
    /**
     *  Gets all <code>AssessmentsTaken</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AssessmentsTaken</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTaken()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList ret = getAssessmentTakenList();

        for (org.osid.assessment.AssessmentTakenLookupSession session : getSessions()) {
            ret.addAssessmentTakenList(session.getAssessmentsTaken());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.FederatingAssessmentTakenList getAssessmentTakenList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.ParallelAssessmentTakenList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.assessmenttaken.CompositeAssessmentTakenList());
        }
    }
}
