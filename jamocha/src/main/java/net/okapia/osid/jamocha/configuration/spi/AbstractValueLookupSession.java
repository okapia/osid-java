//
// AbstractValueLookupSession.java
//
//    A starter implementation framework for providing a Value
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Value
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getValues(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractValueLookupSession
    extends AbstractValueRetrievalSession
    implements org.osid.configuration.ValueLookupSession {

    private boolean activeonly = false;
    

    /**
     *  Only active values are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveValueView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive values are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusValueView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    

    /**
     *  Gets all the <code> Values </code> for the given parameter <code> Id. 
     *  </code> 
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value list 
     *  @throws org.osid.NotFoundException the <code> parameterId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ValueList ret = new net.okapia.osid.jamocha.inline.filter.configuration.value.ValueFilterList(new ParameterFilter(parameterId), getValues());

        if (ret.hasNext()) {
            return (ret);
        }

        throw new org.osid.NotFoundException(parameterId + " not found");
    }

     
    /**
     *  Gets the <code>Value</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Value</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Value</code> and
     *  retained for compatibility.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueId <code>Id</code> of the
     *          <code>Value</code>
     *  @return the value
     *  @throws org.osid.NotFoundException <code>valueId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>valueId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Value getValue(org.osid.id.Id valueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.configuration.ValueList values = getValues()) {
            while (values.hasNext()) {
                org.osid.configuration.Value value = values.getNextValue();
                if (value.getId().equals(valueId)) {
                    return (value);
                }
            }
        } 

        throw new org.osid.NotFoundException(valueId + " not found");
    }


    /**
     *  Gets a <code>ValueList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  values specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Values</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getValues()</code>.
     *
     *  @param  valueIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Value</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>valueIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByIds(org.osid.id.IdList valueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.configuration.Value> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = valueIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getValue(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("value " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.configuration.value.LinkedValueList(ret));
    }


    /**
     *  Gets a <code>ValueList</code> corresponding to the given
     *  value genus <code>Type</code> which does not include
     *  values of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getValues()</code>.
     *
     *  @param  valueGenusType a value genus type 
     *  @return the returned <code>Value</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByGenusType(org.osid.type.Type valueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.value.ValueGenusFilterList(getValues(), valueGenusType));
    }


    /**
     *  Gets a <code>ValueList</code> corresponding to the given
     *  value genus <code>Type</code> and include any additional
     *  values with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getValues()</code>.
     *
     *  @param  valueGenusType a value genus type 
     *  @return the returned <code>Value</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParentGenusType(org.osid.type.Type valueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getValuesByGenusType(valueGenusType));
    }


    /**
     *  Gets a <code>ValueList</code> containing the given
     *  value record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getValues()</code>.
     *
     *  @param  valueRecordType a value record type 
     *  @return the returned <code>Value</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByRecordType(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.value.ValueRecordFilterList(getValues(), valueRecordType));
    }


    /**
     *  Gets all <code>Values</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @return a list of <code>Values</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.configuration.ValueList getValues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets the values in this configuration based on a
     *  condition. <code> </code> The condition specified is applied
     *  to any or all parameters in this configuration as
     *  applicable. In pleneary mode, all values are returned or an
     *  error results. In comparative mode, inaccessible values may be
     *  omitted.
     *
     *  @param  valueCondition a value condition 
     *  @return the value list 
     *  @throws org.osid.NullArgumentException <code> valueCondition </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> valueCondition </code> 
     *          not of this service 
     */
    
    @OSID @Override
    public org.osid.configuration.ValueList getValuesOnCondition(org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        if (!(valueCondition instanceof net.okapia.osid.jamocha.nil.configuration.valuecondition.UnknownValueCondition)) {
            throw new org.osid.UnsupportedException("valueCondition did not originate from getValueCondition()");
        }

        return (getValues());        
    }


    /**
     *  Filters the value list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of values
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.configuration.ValueList filterValuesOnViews(org.osid.configuration.ValueList list)
        throws org.osid.OperationFailedException {

        org.osid.configuration.ValueList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.configuration.value.ActiveValueFilterList(ret);
        }

        return (ret);
    }


    public static class ParameterFilter
        implements net.okapia.osid.jamocha.inline.filter.configuration.value.ValueFilter {

        private final org.osid.id.Id parameterId;

        
        /**
         *  Constructs a new <code>ParameterFilter</code>.
         *
         *  @param parameterId the parameter to filter
         *  @throws org.osid.NullArgumentException
         *          <code>parameterId</code> is <code>null</code>
         */

        public ParameterFilter(org.osid.id.Id parameterId) {
            nullarg(parameterId, "parameter Id");
            this.parameterId = parameterId;
            return;
        }


        /**
         *  Used by the ValueFilterList to filter the value list
         *  based on parameter.
         *
         *  @param value the value
         *  @return <code>true</code> to pass the value,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.configuration.Value value) {
            return (value.getParameterId().equals(this.parameterId));
        }
    }
}
