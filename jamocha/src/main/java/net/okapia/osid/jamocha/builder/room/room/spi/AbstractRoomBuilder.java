//
// AbstractRoom.java
//
//     Defines a Room builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.room.spi;


/**
 *  Defines a <code>Room</code> builder.
 */

public abstract class AbstractRoomBuilder<T extends AbstractRoomBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.room.room.RoomMiter room;


    /**
     *  Constructs a new <code>AbstractRoomBuilder</code>.
     *
     *  @param room the room to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRoomBuilder(net.okapia.osid.jamocha.builder.room.room.RoomMiter room) {
        super(room);
        this.room = room;
        return;
    }


    /**
     *  Builds the room.
     *
     *  @return the new room
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.room.Room build() {
        (new net.okapia.osid.jamocha.builder.validator.room.room.RoomValidator(getValidations())).validate(this.room);
        return (new net.okapia.osid.jamocha.builder.room.room.ImmutableRoom(this.room));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the room miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.room.room.RoomMiter getMiter() {
        return (this.room);
    }


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    public T building(org.osid.room.Building building) {
        getMiter().setBuilding(building);
        return (self());
    }


    /**
     *  Sets the floor.
     *
     *  @param floor a floor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>floor</code> is
     *          <code>null</code>
     */

    public T floor(org.osid.room.Floor floor) {
        getMiter().setFloor(floor);
        return (self());
    }


    /**
     *  Sets the enclosing room.
     *
     *  @param room an enclosing room
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>room</code> is
     *          <code>null</code>
     */

    public T enclosingRoom(org.osid.room.Room room) {
        getMiter().setEnclosingRoom(room);
        return (self());
    }


    /**
     *  Adds a subdivision.
     *
     *  @param subdivision a subdivision
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>subdivision</code> is <code>null</code>
     */

    public T subdivision(org.osid.room.Room subdivision) {
        getMiter().addSubdivision(subdivision);
        return (self());
    }


    /**
     *  Sets all the subdivisions.
     *
     *  @param subdivisions a collection of subdivisions
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>subdivisions</code> is <code>null</code>
     */

    public T subdivisions(java.util.Collection<org.osid.room.Room> subdivisions) {
        getMiter().setSubdivisions(subdivisions);
        return (self());
    }


    /**
     *  Sets the designated name.
     *
     *  @param name a designated name
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          <code>null</code>
     */

    public T designatedName(org.osid.locale.DisplayText name) {
        getMiter().setDesignatedName(name);
        return (self());
    }


    /**
     *  Sets the room number.
     *
     *  @param number a room number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T roomNumber(String number) {
        getMiter().setRoomNumber(number);
        return (self());
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     */

    public T code(String code) {
        getMiter().setCode(code);
        return (self());
    }


    /**
     *  Sets the area.
     *
     *  @param area an area
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>area</code> is
     *          negative
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    public T area(java.math.BigDecimal area) {
        getMiter().setArea(area);
        return (self());
    }


    /**
     *  Sets the occupancy limit.
     *
     *  @param limit an occupancy limit
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>limit</code> is
     *          negative
     */

    public T occupancyLimit(long limit) {
        getMiter().setOccupancyLimit(limit);
        return (self());
    }


    /**
     *  Adds a resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().addResource(resource);
        return (self());
    }


    /**
     *  Sets all the resources.
     *
     *  @param resources a collection of resources
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          is <code>null</code>
     */

    public T resources(java.util.Collection<org.osid.resource.Resource> resources) {
        getMiter().setResources(resources);
        return (self());
    }


    /**
     *  Adds a Room record.
     *
     *  @param record a room record
     *  @param recordType the type of room record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.room.records.RoomRecord record, org.osid.type.Type recordType) {
        getMiter().addRoomRecord(record, recordType);
        return (self());
    }
}       


