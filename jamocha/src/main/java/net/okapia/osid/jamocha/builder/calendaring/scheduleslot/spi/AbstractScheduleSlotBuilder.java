//
// AbstractScheduleSlot.java
//
//     Defines a ScheduleSlot builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.scheduleslot.spi;


/**
 *  Defines a <code>ScheduleSlot</code> builder.
 */

public abstract class AbstractScheduleSlotBuilder<T extends AbstractScheduleSlotBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractContainableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.scheduleslot.ScheduleSlotMiter scheduleSlot;


    /**
     *  Constructs a new <code>AbstractScheduleSlotBuilder</code>.
     *
     *  @param scheduleSlot the schedule slot to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractScheduleSlotBuilder(net.okapia.osid.jamocha.builder.calendaring.scheduleslot.ScheduleSlotMiter scheduleSlot) {
        super(scheduleSlot);
        this.scheduleSlot = scheduleSlot;
        return;
    }


    /**
     *  Builds the schedule slot.
     *
     *  @return the new schedule slot
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.ScheduleSlot build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.scheduleslot.ScheduleSlotValidator(getValidations())).validate(this.scheduleSlot);
        return (new net.okapia.osid.jamocha.builder.calendaring.scheduleslot.ImmutableScheduleSlot(this.scheduleSlot));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the schedule slot miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.scheduleslot.ScheduleSlotMiter getMiter() {
        return (this.scheduleSlot);
    }


    /**
     *  Adds a schedule slot.
     *
     *  @param slot a schedule slot
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>slot</code> is
     *          <code>null</code>
     */

    public T scheduleSlot(org.osid.calendaring.ScheduleSlot slot) {
        getMiter().addScheduleSlot(slot);
        return (self());
    }


    /**
     *  Sets all the schedule slots.
     *
     *  @param slots a collection of schedule slots
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>slots</code> is
     *          <code>null</code>
     */

    public T scheduleSlots(java.util.Collection<org.osid.calendaring.ScheduleSlot> slots) {
        getMiter().setScheduleSlots(slots);
        return (self());
    }


    /**
     *  Adds a weekday.
     *
     *  @param weekday a weekday
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>weekday</code>
     *          is negative
     */

    public T weekday(long weekday) {
        getMiter().addWeekday(weekday);
        return (self());
    }


    /**
     *  Sets all the weekdays.
     *
     *  @param weekdays a collection of weekdays
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>weekdays</code> is <code>null</code>
     */

    public T weekdays(java.util.Collection<Long> weekdays) {
        getMiter().setWeekdays(weekdays);
        return (self());
    }


    /**
     *  Sets the weekly interval.
     *
     *  @param weeklyInterval a weekly interval
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>weeklyInterval</code> is <code>null</code>
     */

    public T weeklyInterval(long weeklyInterval) {
        getMiter().setWeeklyInterval(weeklyInterval);
        return (self());
    }


    /**
     *  Sets the week of month.
     *
     *  @param week a week of month
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>week</code> is <code>null</code>
     */

    public T weekOfMonth(long week) {
        getMiter().setWeekOfMonth(week);
        return (self());
    }


    /**
     *  Sets the weekday time.
     *
     *  @param time a weekday time
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    public T weekdayTime(org.osid.calendaring.Time time) {
        getMiter().setWeekdayTime(time);
        return (self());
    }


    /**
     *  Sets the fixed interval.
     *
     *  @param interval a fixed interval
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>interval</code> is <code>null</code>
     */

    public T interval(org.osid.calendaring.Duration interval) {
        getMiter().setFixedInterval(interval);
        return (self());
    }


    /**
     *  Sets the duration.
     *
     *  @param duration the duration
     *  @throws org.osid.NullArgumentException <code>duration</code> is
     *          <code>null</code>
     */

    public T duration(org.osid.calendaring.Duration duration) {
        getMiter().setDuration(duration);
        return (self());
    }


    /**
     *  Adds a ScheduleSlot record.
     *
     *  @param record a schedule slot record
     *  @param recordType the type of schedule slot record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.records.ScheduleSlotRecord record, org.osid.type.Type recordType) {
        getMiter().addScheduleSlotRecord(record, recordType);
        return (self());
    }
}       


