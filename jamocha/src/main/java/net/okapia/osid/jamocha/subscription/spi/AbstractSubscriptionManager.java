//
// AbstractSubscriptionManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractSubscriptionManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.subscription.SubscriptionManager,
               org.osid.subscription.SubscriptionProxyManager {

    private final Types subscriptionRecordTypes            = new TypeRefSet();
    private final Types subscriptionSearchRecordTypes      = new TypeRefSet();

    private final Types dispatchRecordTypes                = new TypeRefSet();
    private final Types dispatchSearchRecordTypes          = new TypeRefSet();

    private final Types publisherRecordTypes               = new TypeRefSet();
    private final Types publisherSearchRecordTypes         = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractSubscriptionManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractSubscriptionManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any dispatch federation is exposed. Federation is exposed 
     *  when a specific dispatch may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of dispatches appears as a single dispatch. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a my subscription lookup service. 
     *
     *  @return <code> true </code> if my subscription lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMySubscription() {
        return (false);
    }


    /**
     *  Tests for the availability of a my subscription administrative 
     *  service. 
     *
     *  @return <code> true </code> if my subscription admin is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMySubscriptionAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a subscription lookup service. 
     *
     *  @return <code> true </code> if subscription lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionLookup() {
        return (false);
    }


    /**
     *  Tests if querying subscriptions is available. 
     *
     *  @return <code> true </code> if subscription query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionQuery() {
        return (false);
    }


    /**
     *  Tests if searching for subscriptions is available. 
     *
     *  @return <code> true </code> if subscription search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionSearch() {
        return (false);
    }


    /**
     *  Tests if searching for subscriptions is available. 
     *
     *  @return <code> true </code> if subscription search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionAdmin() {
        return (false);
    }


    /**
     *  Tests if subscription notification is available. 
     *
     *  @return <code> true </code> if subscription notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionNotification() {
        return (false);
    }


    /**
     *  Tests if a subscription to publisher lookup session is available. 
     *
     *  @return <code> true </code> if subscription publisher lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionPublisher() {
        return (false);
    }


    /**
     *  Tests if a subscription to publisher assignment session is available. 
     *
     *  @return <code> true </code> if subscription publisher assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionPublisherAssignment() {
        return (false);
    }


    /**
     *  Tests if a subscription smart publisher session is available. 
     *
     *  @return <code> true </code> if subscription smart publisher is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionSmartPublisher() {
        return (false);
    }


    /**
     *  Tests for the availability of an dispatch lookup service. 
     *
     *  @return <code> true </code> if dispatch lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchLookup() {
        return (false);
    }


    /**
     *  Tests if querying dispatches is available. 
     *
     *  @return <code> true </code> if dispatch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchQuery() {
        return (false);
    }


    /**
     *  Tests if searching for dispatches is available. 
     *
     *  @return <code> true </code> if dispatch search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a dispatch administrative service for 
     *  creating and deleting dispatches. 
     *
     *  @return <code> true </code> if dispatch administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a dispatch notification service. 
     *
     *  @return <code> true </code> if dispatch notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchNotification() {
        return (false);
    }


    /**
     *  Tests if a dispatch to publisher lookup session is available. 
     *
     *  @return <code> true </code> if dispatch publisher lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchPublisher() {
        return (false);
    }


    /**
     *  Tests if a dispatch to publisher assignment session is available. 
     *
     *  @return <code> true </code> if dispatch publisher assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchPublisherAssignment() {
        return (false);
    }


    /**
     *  Tests if a dispatch smart publisher session is available. 
     *
     *  @return <code> true </code> if dispatch smart publisher is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchSmartPublisher() {
        return (false);
    }


    /**
     *  Tests for the availability of an publisher lookup service. 
     *
     *  @return <code> true </code> if publisher lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherLookup() {
        return (false);
    }


    /**
     *  Tests if querying publishers is available. 
     *
     *  @return <code> true </code> if publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherQuery() {
        return (false);
    }


    /**
     *  Tests if searching for publishers is available. 
     *
     *  @return <code> true </code> if publisher search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a publisher administrative service for 
     *  creating and deleting publishers. 
     *
     *  @return <code> true </code> if publisher administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a publisher notification service. 
     *
     *  @return <code> true </code> if publisher notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a publisher hierarchy traversal service. 
     *
     *  @return <code> true </code> if publisher hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a publisher hierarchy design service. 
     *
     *  @return <code> true </code> if publisher hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a subscription batch service. 
     *
     *  @return <code> true </code> if a subscription batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a subscription rules service. 
     *
     *  @return <code> true </code> if a subscription rules service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Subscription </code> record types. 
     *
     *  @return a list containing the supported subscription record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubscriptionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.subscriptionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Subscription </code> record type is 
     *  supported. 
     *
     *  @param  subscriptionRecordType a <code> Type </code> indicating a 
     *          <code> Subscription </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> subscriptionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSubscriptionRecordType(org.osid.type.Type subscriptionRecordType) {
        return (this.subscriptionRecordTypes.contains(subscriptionRecordType));
    }


    /**
     *  Adds support for a subscription record type.
     *
     *  @param subscriptionRecordType a subscription record type
     *  @throws org.osid.NullArgumentException
     *  <code>subscriptionRecordType</code> is <code>null</code>
     */

    protected void addSubscriptionRecordType(org.osid.type.Type subscriptionRecordType) {
        this.subscriptionRecordTypes.add(subscriptionRecordType);
        return;
    }


    /**
     *  Removes support for a subscription record type.
     *
     *  @param subscriptionRecordType a subscription record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>subscriptionRecordType</code> is <code>null</code>
     */

    protected void removeSubscriptionRecordType(org.osid.type.Type subscriptionRecordType) {
        this.subscriptionRecordTypes.remove(subscriptionRecordType);
        return;
    }


    /**
     *  Gets the supported subscription search record types. 
     *
     *  @return a list containing the supported subscription search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubscriptionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.subscriptionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given subscription search record type is supported. 
     *
     *  @param  subscriptionSearchRecordType a <code> Type </code> indicating 
     *          a subscription record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          subscriptionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSubscriptionSearchRecordType(org.osid.type.Type subscriptionSearchRecordType) {
        return (this.subscriptionSearchRecordTypes.contains(subscriptionSearchRecordType));
    }


    /**
     *  Adds support for a subscription search record type.
     *
     *  @param subscriptionSearchRecordType a subscription search record type
     *  @throws org.osid.NullArgumentException
     *  <code>subscriptionSearchRecordType</code> is <code>null</code>
     */

    protected void addSubscriptionSearchRecordType(org.osid.type.Type subscriptionSearchRecordType) {
        this.subscriptionSearchRecordTypes.add(subscriptionSearchRecordType);
        return;
    }


    /**
     *  Removes support for a subscription search record type.
     *
     *  @param subscriptionSearchRecordType a subscription search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>subscriptionSearchRecordType</code> is <code>null</code>
     */

    protected void removeSubscriptionSearchRecordType(org.osid.type.Type subscriptionSearchRecordType) {
        this.subscriptionSearchRecordTypes.remove(subscriptionSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Dispatch </code> record types. 
     *
     *  @return a list containing the supported dispatch record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDispatchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.dispatchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Dispatch </code> record type is supported. 
     *
     *  @param  dispatchRecordType a <code> Type </code> indicating a <code> 
     *          Dispatch </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> dispatchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDispatchRecordType(org.osid.type.Type dispatchRecordType) {
        return (this.dispatchRecordTypes.contains(dispatchRecordType));
    }


    /**
     *  Adds support for a dispatch record type.
     *
     *  @param dispatchRecordType a dispatch record type
     *  @throws org.osid.NullArgumentException
     *  <code>dispatchRecordType</code> is <code>null</code>
     */

    protected void addDispatchRecordType(org.osid.type.Type dispatchRecordType) {
        this.dispatchRecordTypes.add(dispatchRecordType);
        return;
    }


    /**
     *  Removes support for a dispatch record type.
     *
     *  @param dispatchRecordType a dispatch record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>dispatchRecordType</code> is <code>null</code>
     */

    protected void removeDispatchRecordType(org.osid.type.Type dispatchRecordType) {
        this.dispatchRecordTypes.remove(dispatchRecordType);
        return;
    }


    /**
     *  Gets the supported dispatch search record types. 
     *
     *  @return a list containing the supported dispatch search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDispatchSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.dispatchSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given dispatch search record type is supported. 
     *
     *  @param  dispatchSearchRecordType a <code> Type </code> indicating a 
     *          dispatch record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> dispatchSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDispatchSearchRecordType(org.osid.type.Type dispatchSearchRecordType) {
        return (this.dispatchSearchRecordTypes.contains(dispatchSearchRecordType));
    }


    /**
     *  Adds support for a dispatch search record type.
     *
     *  @param dispatchSearchRecordType a dispatch search record type
     *  @throws org.osid.NullArgumentException
     *  <code>dispatchSearchRecordType</code> is <code>null</code>
     */

    protected void addDispatchSearchRecordType(org.osid.type.Type dispatchSearchRecordType) {
        this.dispatchSearchRecordTypes.add(dispatchSearchRecordType);
        return;
    }


    /**
     *  Removes support for a dispatch search record type.
     *
     *  @param dispatchSearchRecordType a dispatch search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>dispatchSearchRecordType</code> is <code>null</code>
     */

    protected void removeDispatchSearchRecordType(org.osid.type.Type dispatchSearchRecordType) {
        this.dispatchSearchRecordTypes.remove(dispatchSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Publisher </code> record types. 
     *
     *  @return a list containing the supported publisher record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPublisherRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.publisherRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Publisher </code> record type is supported. 
     *
     *  @param  publisherRecordType a <code> Type </code> indicating a <code> 
     *          Publisher </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> publisherRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPublisherRecordType(org.osid.type.Type publisherRecordType) {
        return (this.publisherRecordTypes.contains(publisherRecordType));
    }


    /**
     *  Adds support for a publisher record type.
     *
     *  @param publisherRecordType a publisher record type
     *  @throws org.osid.NullArgumentException
     *  <code>publisherRecordType</code> is <code>null</code>
     */

    protected void addPublisherRecordType(org.osid.type.Type publisherRecordType) {
        this.publisherRecordTypes.add(publisherRecordType);
        return;
    }


    /**
     *  Removes support for a publisher record type.
     *
     *  @param publisherRecordType a publisher record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>publisherRecordType</code> is <code>null</code>
     */

    protected void removePublisherRecordType(org.osid.type.Type publisherRecordType) {
        this.publisherRecordTypes.remove(publisherRecordType);
        return;
    }


    /**
     *  Gets the supported publisher search record types. 
     *
     *  @return a list containing the supported publisher search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPublisherSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.publisherSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given publisher search record type is supported. 
     *
     *  @param  publisherSearchRecordType a <code> Type </code> indicating a 
     *          publisher record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          publisherSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPublisherSearchRecordType(org.osid.type.Type publisherSearchRecordType) {
        return (this.publisherSearchRecordTypes.contains(publisherSearchRecordType));
    }


    /**
     *  Adds support for a publisher search record type.
     *
     *  @param publisherSearchRecordType a publisher search record type
     *  @throws org.osid.NullArgumentException
     *  <code>publisherSearchRecordType</code> is <code>null</code>
     */

    protected void addPublisherSearchRecordType(org.osid.type.Type publisherSearchRecordType) {
        this.publisherSearchRecordTypes.add(publisherSearchRecordType);
        return;
    }


    /**
     *  Removes support for a publisher search record type.
     *
     *  @param publisherSearchRecordType a publisher search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>publisherSearchRecordType</code> is <code>null</code>
     */

    protected void removePublisherSearchRecordType(org.osid.type.Type publisherSearchRecordType) {
        this.publisherSearchRecordTypes.remove(publisherSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service for the authenticated agent. 
     *
     *  @return a <code> My </code> S <code> ubscriptionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscription() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionSession getMySubscriptionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getMySubscriptionSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service for the authenticated agent. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> My </code> S <code> ubscriptionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscription() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionSession getMySubscriptionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getMySubscriptionSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service for the authenticated agent for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> MySubscriptionSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscription() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionSession getMySubscriptionSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getMySubscriptionSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service for the authenticated agent for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MySubscriptionSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscription() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionSession getMySubscriptionSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getMySubscriptionSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administrative service for the authenticated agent. 
     *
     *  @return a <code> My </code> S <code> ubscriptionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscriptionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionAdminSession getMySubscriptionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getMySubscriptionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administrative service for the authenticated agent. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> My </code> S <code> ubscriptionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscriptionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionAdminSession getMySubscriptionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getMySubscriptionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administrative service for the authenticated agent for the given 
     *  publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> MySubscriptionSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscriptionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionAdminSession getMySubscriptionAdminSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getMySubscriptionAdminSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administrative service for the authenticated agent for the given 
     *  publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MySubscriptionSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscriptionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionAdminSession getMySubscriptionAdminSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getMySubscriptionAdminSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service. 
     *
     *  @return a <code> SubscriptionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionLookupSession getSubscriptionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionLookupSession getSubscriptionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionLookupSession getSubscriptionLookupSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionLookupSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionLookupSession getSubscriptionLookupSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionLookupSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  query service. 
     *
     *  @return a <code> SubscriptionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuerySession getSubscriptionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuerySession getSubscriptionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  query service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuerySession getSubscriptionQuerySessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionQuerySessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  query service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Subscription </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuerySession getSubscriptionQuerySessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionQuerySessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  search service. 
     *
     *  @return a <code> SubscriptionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionSearchSession getSubscriptionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionSearchSession getSubscriptionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  search service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionSearchSession getSubscriptionSearchSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionSearchSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  search service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Subscription </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionSearchSession getSubscriptionSearchSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionSearchSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administration service. 
     *
     *  @return a <code> SubscriptionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionAdminSession getSubscriptionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionAdminSession getSubscriptionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administration service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionAdminSession getSubscriptionAdminSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionAdminSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administration service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Subscription </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionAdminSession getSubscriptionAdminSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionAdminSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  notification service. 
     *
     *  @param  subscriptionReceiver the receiver 
     *  @return a <code> SubscriptionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> subscriptionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionNotificationSession getSubscriptionNotificationSession(org.osid.subscription.SubscriptionReceiver subscriptionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  notification service. 
     *
     *  @param  subscriptionReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> subscriptionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionNotificationSession getSubscriptionNotificationSession(org.osid.subscription.SubscriptionReceiver subscriptionReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  notification service for the given publisher. 
     *
     *  @param  subscriptionReceiver the receiver 
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> subscriptionReceiver 
     *          </code> or <code> publisherId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionNotificationSession getSubscriptionNotificationSessionForPublisher(org.osid.subscription.SubscriptionReceiver subscriptionReceiver, 
                                                                                                                org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionNotificationSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  notification service for the given publisher. 
     *
     *  @param  subscriptionReceiver the receiver 
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Subscription </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> subscriptionReceiver, 
     *          publisherId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionNotificationSession getSubscriptionNotificationSessionForPublisher(org.osid.subscription.SubscriptionReceiver subscriptionReceiver, 
                                                                                                                org.osid.id.Id publisherId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionNotificationSessionForPublisher not implemented");
    }


    /**
     *  Gets the session for retrieving subscription to publisher mappings. 
     *
     *  @return a <code> SubscriptionPublisherSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionPublisherSession getSubscriptionPublisherSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionPublisherSession not implemented");
    }


    /**
     *  Gets the session for retrieving subscription to publisher mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionPublisherSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionPublisherSession getSubscriptionPublisherSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionPublisherSession not implemented");
    }


    /**
     *  Gets the session for assigning subscription to publisher mappings. 
     *
     *  @return a <code> SubscriptionPublisherAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionPublisherAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionPublisherAssignmentSession getSubscriptionPublisherAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionPublisherAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning subscription to publisher mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionPublisherAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionPublisherAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionPublisherAssignmentSession getSubscriptionPublisherAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionPublisherAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the subscription smart publisher for 
     *  the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the publisher 
     *  @return a <code> SubscriptionSmartPublisherSession </code> 
     *  @throws org.osid.NotFoundException <code> publisherId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionSmartPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionSmartPublisherSession getSubscriptionSmartPublisherSession(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionSmartPublisherSession not implemented");
    }


    /**
     *  Gets the session associated with the subscription smart publisher for 
     *  the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the publisher 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionSmartPublisherSession </code> 
     *  @throws org.osid.NotFoundException <code> publisherId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionSmartPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionSmartPublisherSession getSubscriptionSmartPublisherSession(org.osid.id.Id publisherId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionSmartPublisherSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  lookup service. 
     *
     *  @return a <code> DispatchLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchLookupSession getDispatchLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DispatchLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchLookupSession getDispatchLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  lookup service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Dispatch 
     *          </code> 
     *  @return a <code> DispatchLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchLookupSession getDispatchLookupSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchLookupSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  lookup service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DispatchLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchLookupSession getDispatchLookupSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchLookupSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch query 
     *  service. 
     *
     *  @return a <code> DispatchQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuerySession getDispatchQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DispatchQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuerySession getDispatchQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch query 
     *  service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Dispatch 
     *          </code> 
     *  @return a <code> DispatchQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuerySession getDispatchQuerySessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchQuerySessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch query 
     *  service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DispatchQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Dispatch </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuerySession getDispatchQuerySessionForPublisher(org.osid.id.Id publisherId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchQuerySessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  search service. 
     *
     *  @return a <code> DispatchSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSearchSession getDispatchSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DispatchSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSearchSession getDispatchSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  search service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Dispatch 
     *          </code> 
     *  @return a <code> DispatchSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSearchSession getDispatchSearchSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchSearchSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  search service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DispatchSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dispatch </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSearchSession getDispatchSearchSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchSearchSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  administrative service. 
     *
     *  @return a <code> DispatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchAdminSession getDispatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DispatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchAdminSession getDispatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  administrative service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Dispatch 
     *          </code> 
     *  @return a <code> DispatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchAdminSession getDispatchAdminSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchAdminSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  administration service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DispatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dispatch </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchAdminSession getDispatchAdminSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchAdminSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  notification service. 
     *
     *  @param  dispatchReceiver the receiver 
     *  @return a <code> DispatchNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> dispatchReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchNotificationSession getDispatchNotificationSession(org.osid.subscription.DispatchReceiver dispatchReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  notification service. 
     *
     *  @param  dispatchReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> DispatchNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> dispatchReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchNotificationSession getDispatchNotificationSession(org.osid.subscription.DispatchReceiver dispatchReceiver, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  notification service for the given publisher. 
     *
     *  @param  dispatchReceiver the receiver 
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> DispatchNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dispatchReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchNotificationSession getDispatchNotificationSessionForPublisher(org.osid.subscription.DispatchReceiver dispatchReceiver, 
                                                                                                        org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchNotificationSessionForPublisher not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  notification service for the given publisher. 
     *
     *  @param  dispatchReceiver the receiver 
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DispatchNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dispatch </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dispatchReceiver, 
     *          publisherId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchNotificationSession getDispatchNotificationSessionForPublisher(org.osid.subscription.DispatchReceiver dispatchReceiver, 
                                                                                                        org.osid.id.Id publisherId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchNotificationSessionForPublisher not implemented");
    }


    /**
     *  Gets the session for retrieving dispatch to publisher mappings. 
     *
     *  @return a <code> DispatchPublisherSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchPublisher() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchPublisherSession getDispatchPublisherSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchPublisherSession not implemented");
    }


    /**
     *  Gets the session for retrieving dispatch to publisher mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DispatchPublisherSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchPublisher() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchPublisherSession getDispatchPublisherSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchPublisherSession not implemented");
    }


    /**
     *  Gets the session for assigning dispatch to publisher mappings. 
     *
     *  @return a <code> DispatchPublisherAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchPublisherAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchPublisherAssignmentSession getDispatchPublisherAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchPublisherAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning dispatch to publisher mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DispatchPublisherAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchPublisherAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchPublisherAssignmentSession getDispatchPublisherAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchPublisherAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the dispatch smart publisher for the 
     *  given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the publisher 
     *  @return a <code> DispatchSmartPublisherSession </code> 
     *  @throws org.osid.NotFoundException <code> publisherId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSmartPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSmartPublisherSession getDispatchSmartPublisherSession(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getDispatchSmartPublisherSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic dispatch publishers for the 
     *  given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of a publisher 
     *  @param  proxy a proxy 
     *  @return a <code> DispatchSmartPublisherSession </code> 
     *  @throws org.osid.NotFoundException <code> publisherId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSmartPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSmartPublisherSession getDispatchSmartPublisherSession(org.osid.id.Id publisherId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getDispatchSmartPublisherSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  lookup service. 
     *
     *  @return a <code> PublisherLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherLookupSession getPublisherLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getPublisherLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PublisherLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherLookupSession getPublisherLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getPublisherLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  query service. 
     *
     *  @return a <code> PublisherQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuerySession getPublisherQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getPublisherQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PublisherQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuerySession getPublisherQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getPublisherQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  search service. 
     *
     *  @return a <code> PublisherSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherSearchSession getPublisherSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getPublisherSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PublisherSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherSearchSession getPublisherSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getPublisherSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  administrative service. 
     *
     *  @return a <code> PublisherAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherAdminSession getPublisherAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getPublisherAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PublisherAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherAdminSession getPublisherAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getPublisherAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  notification service. 
     *
     *  @param  publisherReceiver the receiver 
     *  @return a <code> PublisherNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> publisherReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherNotificationSession getPublisherNotificationSession(org.osid.subscription.PublisherReceiver publisherReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getPublisherNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  notification service. 
     *
     *  @param  publisherReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PublisherNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> publisherReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherNotificationSession getPublisherNotificationSession(org.osid.subscription.PublisherReceiver publisherReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getPublisherNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  hierarchy service. 
     *
     *  @return a <code> PublisherHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherHierarchySession getPublisherHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getPublisherHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PublisherHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherHierarchySession getPublisherHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getPublisherHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  hierarchy design service. 
     *
     *  @return a <code> PublisherHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherHierarchyDesignSession getPublisherHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getPublisherHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PublisherHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherHierarchyDesignSession getPublisherHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getPublisherHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> SubscriptionBatchManager. </code> 
     *
     *  @return a <code> SubscriptionBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionbatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.batch.SubscriptionBatchManager getSubscriptionBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionBatchManager not implemented");
    }


    /**
     *  Gets the <code> SubscriptionBatchProxyManager. </code> 
     *
     *  @return a <code> SubscriptionBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.batch.SubscriptionBatchProxyManager getSubscriptionBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> SubscriptionRulesManager. </code> 
     *
     *  @return a <code> SubscriptionRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionRulesManager getSubscriptionRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionManager.getSubscriptionRulesManager not implemented");
    }


    /**
     *  Gets the <code> SubscriptionRulesProxyManager. </code> 
     *
     *  @return a <code> SubscriptionRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionRulesProxyManager getSubscriptionRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.subscription.SubscriptionProxyManager.getSubscriptionRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.subscriptionRecordTypes.clear();
        this.subscriptionRecordTypes.clear();

        this.subscriptionSearchRecordTypes.clear();
        this.subscriptionSearchRecordTypes.clear();

        this.dispatchRecordTypes.clear();
        this.dispatchRecordTypes.clear();

        this.dispatchSearchRecordTypes.clear();
        this.dispatchSearchRecordTypes.clear();

        this.publisherRecordTypes.clear();
        this.publisherRecordTypes.clear();

        this.publisherSearchRecordTypes.clear();
        this.publisherSearchRecordTypes.clear();

        return;
    }
}
