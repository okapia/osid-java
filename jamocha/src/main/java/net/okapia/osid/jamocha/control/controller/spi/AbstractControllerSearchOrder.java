//
// AbstractControllerSearchOdrer.java
//
//     Defines a ControllerSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.controller.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ControllerSearchOrder}.
 */

public abstract class AbstractControllerSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectSearchOrder
    implements org.osid.control.ControllerSearchOrder {

    private final java.util.Collection<org.osid.control.records.ControllerSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by address. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAddress(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by model. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByModel(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a model search order is available. 
     *
     *  @return <code> true </code> if a model search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelSearchOrder() {
        return (false);
    }


    /**
     *  Gets the model search order. 
     *
     *  @return the model search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsModelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSearchOrder getModelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsModelSearchOrder() is false");
    }


    /**
     *  Orders the results by version. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVersion(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the toggles. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByToggleable(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the variable capabilities. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariable(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the variable by percentage capabilities. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariablePercentage(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by variable minimum. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariableMinimum(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by variable maximum. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariableMaximum(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the variable by discreet state capabilities. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDiscreetStates(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  controllerRecordType a controller record type 
     *  @return {@code true} if the controllerRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code controllerRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type controllerRecordType) {
        for (org.osid.control.records.ControllerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  controllerRecordType the controller record type 
     *  @return the controller search order record
     *  @throws org.osid.NullArgumentException
     *          {@code controllerRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(controllerRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.control.records.ControllerSearchOrderRecord getControllerSearchOrderRecord(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ControllerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(controllerRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this controller. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param controllerRecord the controller search odrer record
     *  @param controllerRecordType controller record type
     *  @throws org.osid.NullArgumentException
     *          {@code controllerRecord} or
     *          {@code controllerRecordTypecontroller} is
     *          {@code null}
     */
            
    protected void addControllerRecord(org.osid.control.records.ControllerSearchOrderRecord controllerSearchOrderRecord, 
                                     org.osid.type.Type controllerRecordType) {

        addRecordType(controllerRecordType);
        this.records.add(controllerSearchOrderRecord);
        
        return;
    }
}
