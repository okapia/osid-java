//
// AbstractFederatingJournalEntryLookupSession.java
//
//     An abstract federating adapter for a JournalEntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  JournalEntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingJournalEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.journaling.JournalEntryLookupSession>
    implements org.osid.journaling.JournalEntryLookupSession {

    private boolean parallel = false;
    private org.osid.journaling.Journal journal = new net.okapia.osid.jamocha.nil.journaling.journal.UnknownJournal();


    /**
     *  Constructs a new <code>AbstractFederatingJournalEntryLookupSession</code>.
     */

    protected AbstractFederatingJournalEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.journaling.JournalEntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Journal/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Journal Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getJournalId() {
        return (this.journal.getId());
    }


    /**
     *  Gets the <code>Journal</code> associated with this 
     *  session.
     *
     *  @return the <code>Journal</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.journal);
    }


    /**
     *  Sets the <code>Journal</code>.
     *
     *  @param  journal the journal for this session
     *  @throws org.osid.NullArgumentException <code>journal</code>
     *          is <code>null</code>
     */

    protected void setJournal(org.osid.journaling.Journal journal) {
        nullarg(journal, "journal");
        this.journal = journal;
        return;
    }


    /**
     *  Tests if this user can perform <code>JournalEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canReadJournal() {
        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            if (session.canReadJournal()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>JournalEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJournalEntryView() {
        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            session.useComparativeJournalEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>JournalEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJournalEntryView() {
        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            session.usePlenaryJournalEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include journal entries in journals which are children
     *  of this journal in the journal hierarchy.
     */

    @OSID @Override
    public void useFederatedJournalView() {
        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            session.useFederatedJournalView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this journal only.
     */

    @OSID @Override
    public void useIsolatedJournalView() {
        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            session.useIsolatedJournalView();
        }

        return;
    }

     
    /**
     *  Gets the <code>JournalEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>JournalEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>JournalEntry</code> and
     *  retained for compatibility.
     *
     *  @param  journalEntryId <code>Id</code> of the
     *          <code>JournalEntry</code>
     *  @return the journal entry
     *  @throws org.osid.NotFoundException <code>journalEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>journalEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry getJournalEntry(org.osid.id.Id journalEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            try {
                return (session.getJournalEntry(journalEntryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(journalEntryId + " not found");
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  journalEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>JournalEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  journalEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByIds(org.osid.id.IdList journalEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.journaling.journalentry.MutableJournalEntryList ret = new net.okapia.osid.jamocha.journaling.journalentry.MutableJournalEntryList();

        try (org.osid.id.IdList ids = journalEntryIds) {
            while (ids.hasNext()) {
                ret.addJournalEntry(getJournalEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  journal entry genus <code>Type</code> which does not include
     *  journal entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalEntryGenusType a journalEntry genus type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByGenusType(org.osid.type.Type journalEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByGenusType(journalEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  journal entry genus <code>Type</code> and include any additional
     *  journal entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalEntryGenusType a journalEntry genus type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByParentGenusType(org.osid.type.Type journalEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByParentGenusType(journalEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>JournalEntryList</code> containing the given
     *  journal entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalEntryRecordType a journalEntry record type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByRecordType(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByRecordType(journalEntryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of journal entries corresponding to a branch
     *  <code> Id</code>. In plenary mode, the returned list contains
     *  all known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  branchId the <code>Id</code> of the branch
     *  @return the returned <code>JournalEntryList</code>
     *  @throws org.osid.NullArgumentException <code>branchId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForBranch(org.osid.id.Id branchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesForBranch(branchId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the journal entry corresponding to a resource
     *  <code>Id</code> and date. The entries returned have a
     *  date equal to or more recent than the requested date. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  branchId the <code>Id</code> of the branch
     *  @param  date from date
     *  @return the returned <code>JournalEntryList</code>
     *  @throws org.osid.NullArgumentException <code>branchId</code>
     *          or <code>date</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranch(org.osid.id.Id branchId,
                                                                                 org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByDateForBranch(branchId, date));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of journal entries corresponding to a branch
     *  <code>Id</code> and date range. Entries are returned with
     *  dates that fall between the requested dates inclusive. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results.  Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  branchId the <code> Id </code> of the branch
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code> JournalEntryList </code>
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> branchId, from </code>
     *          or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForBranch(org.osid.id.Id branchId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByDateRangeForBranch(branchId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of journal entries corresponding to a source
     *  <code> Id</code>. In plenary mode, the returned list contains
     *  all known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @return the returned <code>JournalEntryList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesForSource(sourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the journal entry corresponding to a resource <code> Id
     *  </code> and date. The entry returned has a date equal to or
     *  more recent than the requested date. In plenary mode, the
     *  returned list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  sourceId the <code> Id </code> of the source
     *  @param  date from date
     *  @return the returned <code> JournalEntryList </code>
     *  @throws org.osid.NullArgumentException <code> sourceId </code> or
     *          <code> date </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForSource(org.osid.id.Id sourceId,
                                                                                 org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByDateForSource(sourceId, date));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of journal entries corresponding to a source
     *  <code>Id</code> and date range. Entries are returned with
     *  dates that fall between the requested dates inclusive. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results.  Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  sourceId the <code> Id </code> of the source
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code> JournalEntryList </code>
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> sourceId, from </code>
     *          or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForSource(org.osid.id.Id sourceId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByDateRangeForSource(sourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of journal entries corresponding to a branch and
     *  source <code>Id</code>. A source <code>Id</code> of any
     *  version may be requested.
     *
     *  In plenary mode, the returned list contains all known journal
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  branchId the <code> Id </code> of the branch
     *  @param  sourceId the <code> Id </code> of the source
     *  @return the returned <code> JournalEntryList </code>
     *  @throws org.osid.NullArgumentException <code> branchId </code> or
     *          <code> sourceId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForBranchAndSource(org.osid.id.Id branchId,
                                                                                    org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesForBranchAndSource(branchId, sourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the journal entry corresponding to a branch and source
     *  <code>Id</code> and date. The entry returned has a date equal
     *  to or more recent than the requested date. The
     *  <code>sourceId</code> may correspond to any version. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  branchId a branch <code>Id</code>
     *  @param  sourceId the <code>Id</code> of the source
     *  @param  date from date
     *  @return the returned <code>JournalEntryList</code>
     *  @throws org.osid.NullArgumentException <code>branchId</code>,
     *          <code>sourceId</code> or <code>date</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranchAndSource(org.osid.id.Id branchId,
                                                                                          org.osid.id.Id sourceId,
                                                                                          org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByDateForBranchAndSource(branchId, sourceId, date));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of journal entries corresponding to a branch and
     *  source <code>Id</code> and date range. Entries are returned
     *  with dates that fall between the requested dates
     *  inclusive. The <code>sourceId</code> may correspond to any
     *  version In plenary mode, the returned list contains all known
     *  journal entries or an error results.  Otherwise, the returned
     *  list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  branchId a branch <code>Id</code>
     *  @param  sourceId the <code>Id</code> of the source
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>JournalEntryList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is less
     *          than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>branchId<code>,
     *          <code>sourceId</code>, <code>from </code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForBranchAndSource(org.osid.id.Id branchId,
                                                                                               org.osid.id.Id sourceId,
                                                                                               org.osid.calendaring.DateTime from,
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByDateRangeForBranchAndSource(branchId, sourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }    


    /**
     *  Gets a list of journal entries corresponding to a resource
     *  <code>Id</code>. In plenary mode, the returned list contains
     *  all known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>JournalEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }    


    /**
     *  Gets the journal entry corresponding to a resource <code>Id</code>
     *  and date. The entry returned has a date equal to or more recent than
     *  the requested date. In plenary mode, the returned list contains all
     *  known journal entries or an error results. Otherwise, the returned
     *  list may contain only those journal entries that are accessible
     *  through this session.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  date from date
     *  @return the returned <code>JournalEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>date</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForResource(org.osid.id.Id resourceId,
                                                                                   org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByDateForResource(resourceId, date));
        }

        ret.noMore();
        return (ret);
    }    


    /**
     *  Gets a list of journal entries corresponding to a resource
     *  <code>Id</code> and date range. Entries are returned with
     *  dates that fall between the requested dates inclusive. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results.  Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>JournalEntryList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForResource(org.osid.id.Id resourceId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
  
        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntriesByDateRangeForResource(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }    


    /**
     *  Gets all <code>JournalEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>JournalEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList ret = getJournalEntryList();

        for (org.osid.journaling.JournalEntryLookupSession session : getSessions()) {
            ret.addJournalEntryList(session.getJournalEntries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.FederatingJournalEntryList getJournalEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.ParallelJournalEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.journaling.journalentry.CompositeJournalEntryList());
        }
    }
}
