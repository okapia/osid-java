//
// AbstractLessonAnchor.java
//
//     Defines a LessonAnchor builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.lessonanchor.spi;


/**
 *  Defines a <code>LessonAnchor</code> builder.
 */

public abstract class AbstractLessonAnchorBuilder<T extends AbstractLessonAnchorBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.plan.lessonanchor.LessonAnchorMiter lessonAnchor;


    /**
     *  Constructs a new <code>AbstractLessonAnchorBuilder</code>.
     *
     *  @param lessonAnchor the lesson anchor to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractLessonAnchorBuilder(net.okapia.osid.jamocha.builder.course.plan.lessonanchor.LessonAnchorMiter lessonAnchor) {
        this.lessonAnchor = lessonAnchor;
        return;
    }


    /**
     *  Builds the lessonAnchor.
     *
     *  @return the new lessonAnchor
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.plan.LessonAnchor build() {
        (new net.okapia.osid.jamocha.builder.validator.course.plan.lessonanchor.LessonAnchorValidator(getValidations())).validate(this.lessonAnchor);
        return (new net.okapia.osid.jamocha.builder.course.plan.lessonanchor.ImmutableLessonAnchor(this.lessonAnchor));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the lesson anchor miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.plan.lessonanchor.LessonAnchorMiter getMiter() {
        return (this.lessonAnchor);
    }


    /**
     *  Sets the lesson.
     *
     *  @param lesson the lesson
     *  @throws org.osid.NullArgumentException <code>lesson</code> is
     *          <code>null</code>
     */

    public T lesson(org.osid.course.plan.Lesson lesson) {
        getMiter().setLesson(lesson);
        return(self());
    }


    /**
     *  Sets the activity.
     *
     *  @param activity the activity
     *  @throws org.osid.NullArgumentException <code>activity</code> is
     *          <code>null</code>
     */

    public T activity(org.osid.course.Activity activity) {
        getMiter().setActivity(activity);
        return(self());
    }


    /**
     *  Sets the time offset.
     *
     *  @param duration the duration from the start of the activity
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    public T time(org.osid.calendaring.Duration duration) {
        getMiter().setTime(duration);
        return(self());
    }
}       


