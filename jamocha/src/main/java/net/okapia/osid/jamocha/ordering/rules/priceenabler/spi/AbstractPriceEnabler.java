//
// AbstractPriceEnabler.java
//
//     Defines a PriceEnabler.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.rules.priceenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>PriceEnabler</code>.
 */

public abstract class AbstractPriceEnabler
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnabler
    implements org.osid.ordering.rules.PriceEnabler {

    private final java.util.Collection<org.osid.ordering.rules.records.PriceEnablerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this priceEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  priceEnablerRecordType a price enabler record type 
     *  @return <code>true</code> if the priceEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type priceEnablerRecordType) {
        for (org.osid.ordering.rules.records.PriceEnablerRecord record : this.records) {
            if (record.implementsRecordType(priceEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>PriceEnabler</code> record <code>Type</code>.
     *
     *  @param  priceEnablerRecordType the price enabler record type 
     *  @return the price enabler record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.rules.records.PriceEnablerRecord getPriceEnablerRecord(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.rules.records.PriceEnablerRecord record : this.records) {
            if (record.implementsRecordType(priceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price enabler. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param priceEnablerRecord the price enabler record
     *  @param priceEnablerRecordType price enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecord</code> or
     *          <code>priceEnablerRecordTypepriceEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addPriceEnablerRecord(org.osid.ordering.rules.records.PriceEnablerRecord priceEnablerRecord, 
                                         org.osid.type.Type priceEnablerRecordType) {
        
        nullarg(priceEnablerRecord, "price enabler record");
        addRecordType(priceEnablerRecordType);
        this.records.add(priceEnablerRecord);
        
        return;
    }
}
