//
// AbstractVotingManager.java
//
//     An adapter for a VotingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a VotingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterVotingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.voting.VotingManager>
    implements org.osid.voting.VotingManager {


    /**
     *  Constructs a new {@code AbstractAdapterVotingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterVotingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterVotingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterVotingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if voting is supported. 
     *
     *  @return <code> true </code> if voting is supported <code> , </code> 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoting() {
        return (getAdapteeManager().supportsVoting());
    }


    /**
     *  Tests if race results is supported. 
     *
     *  @return <code> true </code> if race results is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceResults() {
        return (getAdapteeManager().supportsRaceResults());
    }


    /**
     *  Tests if voting allocation lookup is supported. 
     *
     *  @return <code> true </code> if voting allocation lookup is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingAllocationLookup() {
        return (getAdapteeManager().supportsVotingAllocationLookup());
    }


    /**
     *  Tests if voting allocation administration is supported. 
     *
     *  @return <code> true </code> if voting allocation administration is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingAllocationAdmin() {
        return (getAdapteeManager().supportsVotingAllocationAdmin());
    }


    /**
     *  Tests if looking up votes is supported. 
     *
     *  @return <code> true </code> if votes lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteLookup() {
        return (getAdapteeManager().supportsVoteLookup());
    }


    /**
     *  Tests if querying votes is supported. 
     *
     *  @return <code> true </code> if votes query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteQuery() {
        return (getAdapteeManager().supportsVoteQuery());
    }


    /**
     *  Tests if searching votes is supported. 
     *
     *  @return <code> true </code> if votes search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteSearch() {
        return (getAdapteeManager().supportsVoteSearch());
    }


    /**
     *  Tests if a votes <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if votes notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteNotification() {
        return (getAdapteeManager().supportsVoteNotification());
    }


    /**
     *  Tests if retrieving mappings of votes and polls is supported. 
     *
     *  @return <code> true </code> if vote polls mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotePolls() {
        return (getAdapteeManager().supportsVotePolls());
    }


    /**
     *  Tests if managing mappings of votes and polls is supported. 
     *
     *  @return <code> true </code> if vote polls assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotePollsAssignment() {
        return (getAdapteeManager().supportsVotePollsAssignment());
    }


    /**
     *  Tests if vote smart polls are available. 
     *
     *  @return <code> true </code> if vote smart polls are supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteSmartPolls() {
        return (getAdapteeManager().supportsVoteSmartPolls());
    }


    /**
     *  Tests if candidate lookup is supported. 
     *
     *  @return <code> true </code> if candidate lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateLookup() {
        return (getAdapteeManager().supportsCandidateLookup());
    }


    /**
     *  Tests if candidate query is supported. 
     *
     *  @return <code> true </code> if candidate query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateQuery() {
        return (getAdapteeManager().supportsCandidateQuery());
    }


    /**
     *  Tests if candidate search is supported. 
     *
     *  @return <code> true </code> if candidate search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateSearch() {
        return (getAdapteeManager().supportsCandidateSearch());
    }


    /**
     *  Tests if candidate administration is supported. 
     *
     *  @return <code> true </code> if candidate administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateAdmin() {
        return (getAdapteeManager().supportsCandidateAdmin());
    }


    /**
     *  Tests if candidate notification is supported. Messages may be sent 
     *  when candidates are created, modified, or deleted. 
     *
     *  @return <code> true </code> if candidate notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateNotification() {
        return (getAdapteeManager().supportsCandidateNotification());
    }


    /**
     *  Tests if retrieving mappings of candidate and polls is supported. 
     *
     *  @return <code> true </code> if candidate polls mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidatePolls() {
        return (getAdapteeManager().supportsCandidatePolls());
    }


    /**
     *  Tests if managing mappings of candidate and polls is supported. 
     *
     *  @return <code> true </code> if candidate polls assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidatePollsAssignment() {
        return (getAdapteeManager().supportsCandidatePollsAssignment());
    }


    /**
     *  Tests if candidate smart polls are available. 
     *
     *  @return <code> true </code> if candidate smart polls are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateSmartPolls() {
        return (getAdapteeManager().supportsCandidateSmartPolls());
    }


    /**
     *  Tests if looking up races is supported. 
     *
     *  @return <code> true </code> if race lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceLookup() {
        return (getAdapteeManager().supportsRaceLookup());
    }


    /**
     *  Tests if querying races is supported. 
     *
     *  @return <code> true </code> if race query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceQuery() {
        return (getAdapteeManager().supportsRaceQuery());
    }


    /**
     *  Tests if searching races is supported. 
     *
     *  @return <code> true </code> if races search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceSearch() {
        return (getAdapteeManager().supportsRaceSearch());
    }


    /**
     *  Tests if a race <code> a </code> dministrative service is supported. 
     *
     *  @return <code> true </code> if race administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceAdmin() {
        return (getAdapteeManager().supportsRaceAdmin());
    }


    /**
     *  Tests if a race <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if race notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceNotification() {
        return (getAdapteeManager().supportsRaceNotification());
    }


    /**
     *  Tests if retrieving mappings of races and polls is supported. 
     *
     *  @return <code> true </code> if race polls mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRacePolls() {
        return (getAdapteeManager().supportsRacePolls());
    }


    /**
     *  Tests if managing mappings of races and polls is supported. 
     *
     *  @return <code> true </code> if race polls assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRacePollsAssignment() {
        return (getAdapteeManager().supportsRacePollsAssignment());
    }


    /**
     *  Tests if race smart polls are available. 
     *
     *  @return <code> true </code> if race smart polls are supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceSmartPolls() {
        return (getAdapteeManager().supportsRaceSmartPolls());
    }


    /**
     *  Tests if looking up ballots is supported. 
     *
     *  @return <code> true </code> if ballot lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotLookup() {
        return (getAdapteeManager().supportsBallotLookup());
    }


    /**
     *  Tests if querying ballots is supported. 
     *
     *  @return <code> true </code> if ballot query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotQuery() {
        return (getAdapteeManager().supportsBallotQuery());
    }


    /**
     *  Tests if searching ballots is supported. 
     *
     *  @return <code> true </code> if ballot search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotSearch() {
        return (getAdapteeManager().supportsBallotSearch());
    }


    /**
     *  Tests if a ballot <code> a </code> dministrative service is supported. 
     *
     *  @return <code> true </code> if ballot administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotAdmin() {
        return (getAdapteeManager().supportsBallotAdmin());
    }


    /**
     *  Tests if a ballot notification service is supported. 
     *
     *  @return <code> true </code> if ballot notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotNotification() {
        return (getAdapteeManager().supportsBallotNotification());
    }


    /**
     *  Tests if retrieving mappings of ballots and polls is supported. 
     *
     *  @return <code> true </code> if ballot polls mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotPolls() {
        return (getAdapteeManager().supportsBallotPolls());
    }


    /**
     *  Tests if managing mappings of ballots and polls is supported. 
     *
     *  @return <code> true </code> if ballot polls assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotPollsAssignment() {
        return (getAdapteeManager().supportsBallotPollsAssignment());
    }


    /**
     *  Tests if ballot smart polls are available. 
     *
     *  @return <code> true </code> if ballot smart polls are supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotSmartPolls() {
        return (getAdapteeManager().supportsBallotSmartPolls());
    }


    /**
     *  Tests if polls lookup is supported. 
     *
     *  @return <code> true </code> if polls lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsLookup() {
        return (getAdapteeManager().supportsPollsLookup());
    }


    /**
     *  Tests if polls query is supported. 
     *
     *  @return <code> true </code> if polls query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (getAdapteeManager().supportsPollsQuery());
    }


    /**
     *  Tests if polls search is supported. 
     *
     *  @return <code> true </code> if polls search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsSearch() {
        return (getAdapteeManager().supportsPollsSearch());
    }


    /**
     *  Tests if polls administration is supported. 
     *
     *  @return <code> true </code> if polls administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsAdmin() {
        return (getAdapteeManager().supportsPollsAdmin());
    }


    /**
     *  Tests if polls notification is supported. Messages may be sent when 
     *  <code> Polls </code> objects are created, deleted or updated. 
     *  Notifications for candidates within polls are sent via the candidate 
     *  notification session. 
     *
     *  @return <code> true </code> if polls notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsNotification() {
        return (getAdapteeManager().supportsPollsNotification());
    }


    /**
     *  Tests if a polls hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a polls hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsHierarchy() {
        return (getAdapteeManager().supportsPollsHierarchy());
    }


    /**
     *  Tests if a polls hierarchy design is supported. 
     *
     *  @return <code> true </code> if a polls hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsHierarchyDesign() {
        return (getAdapteeManager().supportsPollsHierarchyDesign());
    }


    /**
     *  Tests if a voting batch service is supported. 
     *
     *  @return <code> true </code> if a voting batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingBatch() {
        return (getAdapteeManager().supportsVotingBatch());
    }


    /**
     *  Tests if a voting rules service is supported. 
     *
     *  @return <code> true </code> if a voting rules service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingRules() {
        return (getAdapteeManager().supportsVotingRules());
    }


    /**
     *  Gets the supported <code> Vote </code> record types. 
     *
     *  @return a list containing the supported <code> Vote </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVoteRecordTypes() {
        return (getAdapteeManager().getVoteRecordTypes());
    }


    /**
     *  Tests if the given <code> Vote </code> record type is supported. 
     *
     *  @param  voteRecordType a <code> Type </code> indicating a <code> Vote 
     *          </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> voteRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVoteRecordType(org.osid.type.Type voteRecordType) {
        return (getAdapteeManager().supportsVoteRecordType(voteRecordType));
    }


    /**
     *  Gets the supported <code> Vote </code> search record types. 
     *
     *  @return a list containing the supported <code> Vote </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVoteSearchRecordTypes() {
        return (getAdapteeManager().getVoteSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Vote </code> search record type is 
     *  supported. 
     *
     *  @param  voteSearchRecordType a <code> Type </code> indicating a <code> 
     *          Vote </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> voteSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVoteSearchRecordType(org.osid.type.Type voteSearchRecordType) {
        return (getAdapteeManager().supportsVoteSearchRecordType(voteSearchRecordType));
    }


    /**
     *  Gets the supported <code> VoterAllocation </code> record types. 
     *
     *  @return a list containing the supported <code> VoterAllocation </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVoterAllocationRecordTypes() {
        return (getAdapteeManager().getVoterAllocationRecordTypes());
    }


    /**
     *  Tests if the given <code> VoterAllocation </code> record type is 
     *  supported. 
     *
     *  @param  voterAllocationRecordType a <code> Type </code> indicating a 
     *          <code> VoterAllocation </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          voterAllocationRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVoterAllocationRecordType(org.osid.type.Type voterAllocationRecordType) {
        return (getAdapteeManager().supportsVoterAllocationRecordType(voterAllocationRecordType));
    }


    /**
     *  Gets all the candidate record types supported. 
     *
     *  @return the list of supported candidate record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCandidateRecordTypes() {
        return (getAdapteeManager().getCandidateRecordTypes());
    }


    /**
     *  Tests if a given candidate record type is supported. 
     *
     *  @param  candidateRecordType the candidate type 
     *  @return <code> true </code> if the candidate record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> candidateRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCandidateRecordType(org.osid.type.Type candidateRecordType) {
        return (getAdapteeManager().supportsCandidateRecordType(candidateRecordType));
    }


    /**
     *  Gets all the candidate search record types supported. 
     *
     *  @return the list of supported candidate search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCandidateSearchRecordTypes() {
        return (getAdapteeManager().getCandidateSearchRecordTypes());
    }


    /**
     *  Tests if a given candidate search type is supported. 
     *
     *  @param  candidateSearchRecordType the candidate search type 
     *  @return <code> true </code> if the candidate search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          candidateSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCandidateSearchRecordType(org.osid.type.Type candidateSearchRecordType) {
        return (getAdapteeManager().supportsCandidateSearchRecordType(candidateSearchRecordType));
    }


    /**
     *  Gets the supported <code> Race </code> record types. 
     *
     *  @return a list containing the supported <code> Race </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceRecordTypes() {
        return (getAdapteeManager().getRaceRecordTypes());
    }


    /**
     *  Tests if the given <code> Race </code> record type is supported. 
     *
     *  @param  raceRecordType a <code> Type </code> indicating a <code> Race 
     *          </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> raceRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceRecordType(org.osid.type.Type raceRecordType) {
        return (getAdapteeManager().supportsRaceRecordType(raceRecordType));
    }


    /**
     *  Gets the supported <code> Race </code> search record types. 
     *
     *  @return a list containing the supported <code> Race </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceSearchRecordTypes() {
        return (getAdapteeManager().getRaceSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Race </code> search record type is 
     *  supported. 
     *
     *  @param  raceSearchRecordType a <code> Type </code> indicating a <code> 
     *          Race </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> raceSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceSearchRecordType(org.osid.type.Type raceSearchRecordType) {
        return (getAdapteeManager().supportsRaceSearchRecordType(raceSearchRecordType));
    }


    /**
     *  Gets the supported <code> Ballot </code> record types. 
     *
     *  @return a list containing the supported <code> Ballot </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotRecordTypes() {
        return (getAdapteeManager().getBallotRecordTypes());
    }


    /**
     *  Tests if the given <code> Ballot </code> record type is supported. 
     *
     *  @param  ballotRecordType a <code> Type </code> indicating a <code> 
     *          Ballot </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ballotRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBallotRecordType(org.osid.type.Type ballotRecordType) {
        return (getAdapteeManager().supportsBallotRecordType(ballotRecordType));
    }


    /**
     *  Gets the supported <code> Ballot </code> search record types. 
     *
     *  @return a list containing the supported <code> Ballot </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotSearchRecordTypes() {
        return (getAdapteeManager().getBallotSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Ballot </code> search record type is 
     *  supported. 
     *
     *  @param  ballotSearchRecordType a <code> Type </code> indicating a 
     *          <code> Ballot </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ballotSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBallotSearchRecordType(org.osid.type.Type ballotSearchRecordType) {
        return (getAdapteeManager().supportsBallotSearchRecordType(ballotSearchRecordType));
    }


    /**
     *  Gets the supported <code> VotingResults </code> record types. 
     *
     *  @return a list containing the supported <code> VotingResults </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVotingResultsRecordTypes() {
        return (getAdapteeManager().getVotingResultsRecordTypes());
    }


    /**
     *  Tests if the given <code> VotingResults </code> record type is 
     *  supported. 
     *
     *  @param  votingResultsRecordType a <code> Type </code> indicating a 
     *          <code> VotingResults </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> votingResultsRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVotingResultsRecordType(org.osid.type.Type votingResultsRecordType) {
        return (getAdapteeManager().supportsVotingResultsRecordType(votingResultsRecordType));
    }


    /**
     *  Gets all the polls record types supported. 
     *
     *  @return the list of supported polls record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPollsRecordTypes() {
        return (getAdapteeManager().getPollsRecordTypes());
    }


    /**
     *  Tests if a given polls record type is supported. 
     *
     *  @param  pollsRecordType the polls record type 
     *  @return <code> true </code> if the polls record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pollsRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPollsRecordType(org.osid.type.Type pollsRecordType) {
        return (getAdapteeManager().supportsPollsRecordType(pollsRecordType));
    }


    /**
     *  Gets all the polls search record types supported. 
     *
     *  @return the list of supported polls search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPollsSearchRecordTypes() {
        return (getAdapteeManager().getPollsSearchRecordTypes());
    }


    /**
     *  Tests if a given polls search record type is supported. 
     *
     *  @param  pollsSearchRecordType the polls search record type 
     *  @return <code> true </code> if the polls search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pollsSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPollsSearchRecordType(org.osid.type.Type pollsSearchRecordType) {
        return (getAdapteeManager().supportsPollsSearchRecordType(pollsSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voting 
     *  service. 
     *
     *  @return a <code> VotingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingSession getVotingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVotingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voting service 
     *  for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Pools </code> 
     *  @return a <code> VotingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoting() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingSession getVotingSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getVotingSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race results 
     *  service. 
     *
     *  @return a <code> RaceResultsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceResults() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceResultsSession getRaceResultsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceResultsSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race results 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Pools </code> 
     *  @return a <code> RaceResultsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceResults() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceResultsSession getRaceResultsSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceResultsSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation lookup service. 
     *
     *  @return a <code> VoterAllocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationLookupSession getVoterAllocationLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVoterAllocationLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoterAllocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given Id 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationLookupSession getVoterAllocationLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getVoterAllocationLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation administrative service. 
     *
     *  @return a <code> VoterAllocationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationAdminSession getVoterAllocationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVoterAllocationAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation administrative service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoterAllocationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given Id 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationAdminSession getVoterAllocationAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getVoterAllocationAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote lookup 
     *  service. 
     *
     *  @return a <code> VoteLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteLookupSession getVoteLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote lookup 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteLookupSession getVoteLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote query 
     *  service. 
     *
     *  @return a <code> VoteQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteQuerySession getVoteQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote query 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoteQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteQuerySession getVoteQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote search 
     *  service. 
     *
     *  @return a <code> VoteSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSearchSession getVoteSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote search 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoteSearchSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSearchSession getVoteSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote 
     *  notification service. 
     *
     *  @param  voteReceiver the notification callback 
     *  @return a <code> VoteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> voteReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoteNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteNotificationSession getVoteNotificationSession(org.osid.voting.VoteReceiver voteReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteNotificationSession(voteReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote 
     *  notification service for the given polls. 
     *
     *  @param  voteReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> voteReceiver </code> or 
     *          <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteNotificationSession getVoteNotificationSessionForPolls(org.osid.voting.VoteReceiver voteReceiver, 
                                                                                      org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteNotificationSessionForPolls(voteReceiver, pollsId));
    }


    /**
     *  Gets the session for retrieving vote to polls mappings. 
     *
     *  @return a <code> VotePollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVotePolls() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotePollsSession getVotePollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVotePollsSession());
    }


    /**
     *  Gets the session for assigning votes to polls mappings. 
     *
     *  @return a <code> VotePollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotePollsAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotePollsAssignmentSession getVotePollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVotePollsAssignmentSession());
    }


    /**
     *  Gets the session for managing dynamic vote polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return a <code> VoteSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoteSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSmartPollsSession getVoteSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteSmartPollsSession(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the candidate 
     *  lookup service. 
     *
     *  @return <code> a CandidateLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateLookupSession getCandidateLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the candidate 
     *  lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateLookupSession getCandidateLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets a candidate query session. 
     *
     *  @return <code> a CandidateQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuerySession getCandidateQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateQuerySession());
    }


    /**
     *  Gets a candidate query session for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuerySession getCandidateQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets a candidate search session. 
     *
     *  @return <code> a CandidateSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSearchSession getCandidateSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateSearchSession());
    }


    /**
     *  Gets a candidate search session for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSearchSession getCandidateSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets a candidate administration session for creating, updating and 
     *  deleting candidates. 
     *
     *  @return <code> a CandidateAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateAdminSession getCandidateAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateAdminSession());
    }


    /**
     *  Gets a candidate administration session for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateAdminSession getCandidateAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  candidate changes. 
     *
     *  @param  candidateReceiver the notification callback 
     *  @return <code> a CandidateNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> candidateReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateNotificationSession getCandidateNotificationSession(org.osid.voting.CandidateReceiver candidateReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateNotificationSession(candidateReceiver));
    }


    /**
     *  Gets the candidate notification session for the given polls. 
     *
     *  @param  candidateReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> candidateReceiver 
     *          </code> or <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateNotificationSession getCandidateNotificationSessionForPolls(org.osid.voting.CandidateReceiver candidateReceiver, 
                                                                                                org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateNotificationSessionForPolls(candidateReceiver, pollsId));
    }


    /**
     *  Gets the session for retrieving candidate to polls mappings. 
     *
     *  @return a <code> CandidatePollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidatePolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidatePollsSession getCandidatePollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidatePollsSession());
    }


    /**
     *  Gets the session for assigning candidate to polls mappings. 
     *
     *  @return a <code> CandidatePollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidatePollsAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidatePollsAssignmentSession getCandidatePollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidatePollsAssignmentSession());
    }


    /**
     *  Gets the session for managing dynamic candidate polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return a <code> CandidateSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSmartPollsSession getCandidateSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateSmartPollsSession(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race lookup 
     *  service. 
     *
     *  @return a <code> RaceLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceLookupSession getRaceLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race lookup 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceLookupSession getRaceLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race query 
     *  service. 
     *
     *  @return a <code> RaceQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuerySession getRaceQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race query 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuerySession getRaceQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race search 
     *  service. 
     *
     *  @return a <code> RaceSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceSearchSession getRaceSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race search 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceSearchSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supporstRaceSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSearchSession getRaceSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  administration service. 
     *
     *  @return a <code> RaceAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceAdminSession getRaceAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceAdminSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceAdminSession getRaceAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  notification service. 
     *
     *  @param  raceReceiver the notification callback 
     *  @return a <code> RaceNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> raceReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceNotificationSession getRaceNotificationSession(org.osid.voting.RaceReceiver raceReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceNotificationSession(raceReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  notification service for the given polls. 
     *
     *  @param  raceReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> raceReceiver </code> or 
     *          <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceNotificationSession getRaceNotificationSessionForPolls(org.osid.voting.RaceReceiver raceReceiver, 
                                                                                      org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceNotificationSessionForPolls(raceReceiver, pollsId));
    }


    /**
     *  Gets the session for retrieving race to polls mappings. 
     *
     *  @return a <code> RacePollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRacePolls() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RacePollsSession getRacePollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRacePollsSession());
    }


    /**
     *  Gets the session for assigning race to polls mappings. 
     *
     *  @return a <code> RacePollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRacePollsAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RacePollsAssignmentSession getRacePollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRacePollsAssignmentSession());
    }


    /**
     *  Gets the session for managing dynamic race polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return a <code> RaceSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceSmartPollsSession getRaceSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceSmartPollsSession(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot lookup 
     *  service. 
     *
     *  @return a <code> BallotLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotLookupSession getBallotLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot lookup 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotLookupSession getBallotLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot query 
     *  service. 
     *
     *  @return a <code> BallotQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuerySession getBallotQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot query 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuerySession getBallotQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot search 
     *  service. 
     *
     *  @return a <code> RaceSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSearchSession getBallotSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot search 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotSearchSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supporstBallotSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSearchSession getBallotSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  administration service. 
     *
     *  @return a <code> BallotAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotAdminSession getBallotAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotAdminSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotAdminSession getBallotAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  notification service. 
     *
     *  @param  ballotReceiver the notification callback 
     *  @return a <code> BallotNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ballotReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotNotificationSession getBallotNotificationSession(org.osid.voting.BallotReceiver ballotReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotNotificationSession(ballotReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  notification service for the given polls. 
     *
     *  @param  ballotReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> ballotReceiver </code> 
     *          or <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotNotificationSession getBallotNotificationSessionForPolls(org.osid.voting.BallotReceiver ballotReceiver, 
                                                                                          org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotNotificationSessionForPolls(ballotReceiver, pollsId));
    }


    /**
     *  Gets the session for retrieving ballot to polls mappings. 
     *
     *  @return a <code> BallotPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotPolls() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotPollsSession getBallotPollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotPollsSession());
    }


    /**
     *  Gets the session for assigning ballot to polls mappings. 
     *
     *  @return a <code> BallotPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotPollsAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotPollsAssignmentSession getBallotPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotPollsAssignmentSession());
    }


    /**
     *  Gets the session for managing dynamic ballot polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return a <code> BallotSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSmartPollsSession getBallotSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotSmartPollsSession(pollsId));
    }


    /**
     *  Gets the polls lookup session. 
     *
     *  @return a <code> PollsLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsLookupSession getPollsLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPollsLookupSession());
    }


    /**
     *  Gets the polls query session. 
     *
     *  @return a <code> PollsQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuerySession getPollsQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPollsQuerySession());
    }


    /**
     *  Gets the polls search session. 
     *
     *  @return a <code> PollsSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsSearchSession getPollsSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPollsSearchSession());
    }


    /**
     *  Gets the polls administrative session for creating, updating and 
     *  deleteing polls. 
     *
     *  @return a <code> PollsAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsAdminSession getPollsAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPollsAdminSession());
    }


    /**
     *  Gets the notification session for subscripollsg to changes to a polls. 
     *
     *  @param  pollsReceiver the notification callback 
     *  @return a <code> PollsNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> pollsReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsNotificationSession getPollsNotificationSession(org.osid.voting.PollsReceiver pollsReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPollsNotificationSession(pollsReceiver));
    }


    /**
     *  Gets the polls hierarchy traversal session. 
     *
     *  @return <code> a PollsHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsHierarchySession getPollsHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPollsHierarchySession());
    }


    /**
     *  Gets the polls hierarchy design session. 
     *
     *  @return a <code> PollsHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsHierarchyDesignSession getPollsHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPollsHierarchyDesignSession());
    }


    /**
     *  Gets a <code> VotingBatchManager. </code> 
     *
     *  @return a <code> VotingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVotingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.VotingBatchManager getVotingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVotingBatchManager());
    }


    /**
     *  Gets a <code> VotingRulesManager. </code> 
     *
     *  @return a <code> VotingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVotingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.VotingRulesManager getVotingRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVotingRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
