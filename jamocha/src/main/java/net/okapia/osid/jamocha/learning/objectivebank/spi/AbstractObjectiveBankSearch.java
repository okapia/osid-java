//
// AbstractObjectiveBankSearch.java
//
//     A template for making an ObjectiveBank Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objectivebank.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing objective bank searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractObjectiveBankSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.learning.ObjectiveBankSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.learning.records.ObjectiveBankSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.learning.ObjectiveBankSearchOrder objectiveBankSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of objective banks. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  objectiveBankIds list of objective banks
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongObjectiveBanks(org.osid.id.IdList objectiveBankIds) {
        while (objectiveBankIds.hasNext()) {
            try {
                this.ids.add(objectiveBankIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongObjectiveBanks</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of objective bank Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getObjectiveBankIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  objectiveBankSearchOrder objective bank search order 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>objectiveBankSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderObjectiveBankResults(org.osid.learning.ObjectiveBankSearchOrder objectiveBankSearchOrder) {
	this.objectiveBankSearchOrder = objectiveBankSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.learning.ObjectiveBankSearchOrder getObjectiveBankSearchOrder() {
	return (this.objectiveBankSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given objective bank search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an objective bank implementing the requested record.
     *
     *  @param objectiveBankSearchRecordType an objective bank search record
     *         type
     *  @return the objective bank search record
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveBankSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveBankSearchRecord getObjectiveBankSearchRecord(org.osid.type.Type objectiveBankSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.learning.records.ObjectiveBankSearchRecord record : this.records) {
            if (record.implementsRecordType(objectiveBankSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveBankSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this objective bank search. 
     *
     *  @param objectiveBankSearchRecord objective bank search record
     *  @param objectiveBankSearchRecordType objectiveBank search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObjectiveBankSearchRecord(org.osid.learning.records.ObjectiveBankSearchRecord objectiveBankSearchRecord, 
                                           org.osid.type.Type objectiveBankSearchRecordType) {

        addRecordType(objectiveBankSearchRecordType);
        this.records.add(objectiveBankSearchRecord);        
        return;
    }
}
