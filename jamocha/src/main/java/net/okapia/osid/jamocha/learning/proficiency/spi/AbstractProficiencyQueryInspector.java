//
// AbstractProficiencyQueryInspector.java
//
//     A template for making a ProficiencyQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.proficiency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for proficiencies.
 */

public abstract class AbstractProficiencyQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.learning.ProficiencyQueryInspector {

    private final java.util.Collection<org.osid.learning.records.ProficiencyQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the objective <code> Id </code> terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the completion terms. 
     *
     *  @return the completion terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getCompletionTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minimum completion terms. 
     *
     *  @return the minimum completion terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumCompletionTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the level <code> Id </code> query terms. 
     *
     *  @return the level <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the level query terms. 
     *
     *  @return the level terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the objective bank <code> Id </code> query terms. 
     *
     *  @return the objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective bank query terms. 
     *
     *  @return the objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given proficiency query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a proficiency implementing the requested record.
     *
     *  @param proficiencyRecordType a proficiency record type
     *  @return the proficiency query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(proficiencyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencyQueryInspectorRecord getProficiencyQueryInspectorRecord(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ProficiencyQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proficiencyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this proficiency query. 
     *
     *  @param proficiencyQueryInspectorRecord proficiency query inspector
     *         record
     *  @param proficiencyRecordType proficiency record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProficiencyQueryInspectorRecord(org.osid.learning.records.ProficiencyQueryInspectorRecord proficiencyQueryInspectorRecord, 
                                                   org.osid.type.Type proficiencyRecordType) {

        addRecordType(proficiencyRecordType);
        nullarg(proficiencyRecordType, "proficiency record type");
        this.records.add(proficiencyQueryInspectorRecord);        
        return;
    }
}
