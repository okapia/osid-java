//
// AbstractMapSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.map.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractMapSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.MapSearchResults {

    private org.osid.mapping.MapList maps;
    private final org.osid.mapping.MapQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.records.MapSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractMapSearchResults.
     *
     *  @param maps the result set
     *  @param mapQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>maps</code>
     *          or <code>mapQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractMapSearchResults(org.osid.mapping.MapList maps,
                                            org.osid.mapping.MapQueryInspector mapQueryInspector) {
        nullarg(maps, "maps");
        nullarg(mapQueryInspector, "map query inspectpr");

        this.maps = maps;
        this.inspector = mapQueryInspector;

        return;
    }


    /**
     *  Gets the map list resulting from a search.
     *
     *  @return a map list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.MapList getMaps() {
        if (this.maps == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.MapList maps = this.maps;
        this.maps = null;
	return (maps);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.MapQueryInspector getMapQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  map search record <code> Type. </code> This method must
     *  be used to retrieve a map implementing the requested
     *  record.
     *
     *  @param mapSearchRecordType a map search 
     *         record type 
     *  @return the map search
     *  @throws org.osid.NullArgumentException
     *          <code>mapSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(mapSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.MapSearchResultsRecord getMapSearchResultsRecord(org.osid.type.Type mapSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.records.MapSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(mapSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(mapSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record map search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addMapRecord(org.osid.mapping.records.MapSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "map record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
