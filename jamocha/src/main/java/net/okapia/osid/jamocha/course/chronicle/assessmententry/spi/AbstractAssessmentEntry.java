//
// AbstractAssessmentEntry.java
//
//     Defines an AssessmentEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.assessmententry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AssessmentEntry</code>.
 */

public abstract class AbstractAssessmentEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.chronicle.AssessmentEntry {

    private org.osid.resource.Resource student;
    private org.osid.assessment.Assessment assessment;
    private org.osid.calendaring.DateTime dateCompleted;
    private org.osid.course.program.Program program;
    private org.osid.course.Course course;
    private org.osid.grading.Grade grade;
    private org.osid.grading.GradeSystem scoreScale;
    private java.math.BigDecimal score;

    private final java.util.Collection<org.osid.course.chronicle.records.AssessmentEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.student.getId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.student);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    protected void setStudent(org.osid.resource.Resource student) {
        nullarg(student, "student");
        this.student = student;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Assessment. </code> 
     *
     *  @return the assessment <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.assessment.getId());
    }


    /**
     *  Gets the <code> Assessment. </code> 
     *
     *  @return the assessment 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.assessment);
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    protected void setAssessment(org.osid.assessment.Assessment assessment) {
        nullarg(assessment, "assessment");
        this.assessment = assessment;
        return;
    }


    /**
     *  Gets the completion date. 
     *
     *  @return the date completion 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateCompleted() {
        return (this.dateCompleted);
    }


    /**
     *  Sets the date completed.
     *
     *  @param date a date completed
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setDateCompleted(org.osid.calendaring.DateTime date) {
        nullarg(date, "date completed");
        this.dateCompleted = date;
        return;
    }


    /**
     *  Tests if this assessment applies to a program. If <code> hasCourse() 
     *  </code> is <code> true </code> then <code> hasProgram() </code> must 
     *  be <code> false. </code> 
     *
     *  @return <code> true </code> if a program is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasProgram() {
        return (this.program != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasProgram()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        if (!hasProgram()) {
            throw new org.osid.IllegalStateException("hasProgram() is false");
        }

        return (this.program.getId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        if (!hasProgram()) {
            throw new org.osid.IllegalStateException("hasProgram() is false");
        }

        return (this.program);
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    protected void setProgram(org.osid.course.program.Program program) {
        nullarg(program, "program");
        this.program = program;
        return;
    }


    /**
     *  Tests if this assessment applies to a specific course. If <code> 
     *  hasCourse() </code> is <code> true </code> then <code> hasProgram() 
     *  </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if a course is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasCourse() {
        return (this.course != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Course. </code> 
     *
     *  @return the course <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasCourse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        if (!hasCourse()) {
            throw new org.osid.IllegalStateException("hasCourse() is false");
        }

        return (this.course.getId());
    }


    /**
     *  Gets the <code> Course. </code> 
     *
     *  @return the course 
     *  @throws org.osid.IllegalStateException <code> hasCourse() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        if (!hasCourse()) {
            throw new org.osid.IllegalStateException("hasCourse() is false");
        }

        return (this.course);
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    protected void setCourse(org.osid.course.Course course) {
        nullarg(course, "course");
        this.course = course;
        return;
    }


    /**
     *  Tests if a grade is available. 
     *
     *  @return <code> true </code> if a grade is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasGrade() {
        return (this.grade != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Grade. </code> 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasGrade() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeId() {
        if (!hasGrade()) {
            throw new org.osid.IllegalStateException("hasGrade() is false");
        }

        return (this.grade.getId());
    }


    /**
     *  Gets the <code> Grade. </code> 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasGrade() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getGrade()
        throws org.osid.OperationFailedException {

        if (!hasGrade()) {
            throw new org.osid.IllegalStateException("hasGrade() is false");
        }

        return (this.grade);
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    protected void setGrade(org.osid.grading.Grade grade) {
        nullarg(grade, "grade");
        this.grade = grade;
        return;
    }


    /**
     *  Tests if a score is available. 
     *
     *  @return <code> true </code> if a score is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasScore() {
        return ((this.scoreScale != null) && (this.score != null));
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScoreScaleId() {
        if (!hasScore()) {
            throw new org.osid.IllegalStateException("hasScore() is false");
        }

        return (this.scoreScale.getId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getScoreScale()
        throws org.osid.OperationFailedException {

        if (!hasScore()) {
            throw new org.osid.IllegalStateException("hasScore() is false");
        }

        return (this.scoreScale);
    }


    /**
     *  Sets the score scale.
     *
     *  @param scoreScale a score scale
     *  @throws org.osid.NullArgumentException
     *          <code>scoreScale</code> is <code>null</code>
     */

    protected void setScoreScale(org.osid.grading.GradeSystem scoreScale) {
        nullarg(scoreScale, "score scale");
        this.scoreScale = scoreScale;
        return;
    }


    /**
     *  Gets the cumulative score. 
     *
     *  @return the score 
     *  @throws org.osid.IllegalStateException <code> hasScore()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal getScore() {
        if (!hasScore()) {
            throw new org.osid.IllegalStateException("hasScore() is false");
        }

        return (this.score);
    }


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    protected void setScore(java.math.BigDecimal score) {
        nullarg(score, "score");
        this.score = score;
        return;
    }


    /**
     *  Tests if this assessmentEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  assessmentEntryRecordType an assessment entry record type 
     *  @return <code>true</code> if the assessmentEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentEntryRecordType) {
        for (org.osid.course.chronicle.records.AssessmentEntryRecord record : this.records) {
            if (record.implementsRecordType(assessmentEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AssessmentEntry</code> record <code>Type</code>.
     *
     *  @param  assessmentEntryRecordType the assessment entry record type 
     *  @return the assessment entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AssessmentEntryRecord getAssessmentEntryRecord(org.osid.type.Type assessmentEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AssessmentEntryRecord record : this.records) {
            if (record.implementsRecordType(assessmentEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentEntryRecord the assessment entry record
     *  @param assessmentEntryRecordType assessment entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryRecord</code> or
     *          <code>assessmentEntryRecordTypeassessmentEntry</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentEntryRecord(org.osid.course.chronicle.records.AssessmentEntryRecord assessmentEntryRecord, 
                                            org.osid.type.Type assessmentEntryRecordType) {

        nullarg(assessmentEntryRecord, "assessment entry record");
        addRecordType(assessmentEntryRecordType);
        this.records.add(assessmentEntryRecord);
        
        return;
    }
}
