//
// OfferingConstrainerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainer.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class OfferingConstrainerElements
    extends net.okapia.osid.jamocha.spi.OsidConstrainerElements {


    /**
     *  Gets the OfferingConstrainerElement Id.
     *
     *  @return the offering constrainer element Id
     */

    public static org.osid.id.Id getOfferingConstrainerEntityId() {
        return (makeEntityId("osid.offering.rules.OfferingConstrainer"));
    }


    /**
     *  Gets the RuledCanonicalUnitId element Id.
     *
     *  @return the RuledCanonicalUnitId element Id
     */

    public static org.osid.id.Id getRuledCanonicalUnitId() {
        return (makeQueryElementId("osid.offering.rules.offeringconstrainer.RuledCanonicalUnitId"));
    }


    /**
     *  Gets the RuledCanonicalUnit element Id.
     *
     *  @return the RuledCanonicalUnit element Id
     */

    public static org.osid.id.Id getRuledCanonicalUnit() {
        return (makeQueryElementId("osid.offering.rules.offeringconstrainer.RuledCanonicalUnit"));
    }


    /**
     *  Gets the OverrideDescription element Id.
     *
     *  @return the OverrideDescription element Id
     */

    public static org.osid.id.Id getOverrideDescription() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.OverrideDescription"));
    }


    /**
     *  Gets the OverrideTitle element Id.
     *
     *  @return the OverrideTitle element Id
     */

    public static org.osid.id.Id getOverrideTitle() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.OverrideTitle"));
    }


    /**
     *  Gets the OverrideCode element Id.
     *
     *  @return the OverrideCode element Id
     */

    public static org.osid.id.Id getOverrideCode() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.OverrideCode"));
    }


    /**
     *  Gets the OverrideTimePeriods element Id.
     *
     *  @return the OverrideTimePeriods element Id
     */

    public static org.osid.id.Id getOverrideTimePeriods() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.OverrideTimePeriods"));
    }


    /**
     *  Gets the ConstrainTimePeriods element Id.
     *
     *  @return the ConstrainTimePeriods element Id
     */

    public static org.osid.id.Id getConstrainTimePeriods() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.ConstrainTimePeriods"));
    }


    /**
     *  Gets the OverrideResultOptions element Id.
     *
     *  @return the OverrideResultOptions element Id
     */

    public static org.osid.id.Id getOverrideResultOptions() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.OverrideResultOptions"));
    }


    /**
     *  Gets the ConstrainResultOptions element Id.
     *
     *  @return the ConstrainResultOptions element Id
     */

    public static org.osid.id.Id getConstrainResultOptions() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.ConstrainResultOptions"));
    }


    /**
     *  Gets the OverrideSponsors element Id.
     *
     *  @return the OverrideSponsors element Id
     */

    public static org.osid.id.Id getOverrideSponsors() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.OverrideSponsors"));
    }


    /**
     *  Gets the ConstrainSponsors element Id.
     *
     *  @return the ConstrainSponsors element Id
     */

    public static org.osid.id.Id getConstrainSponsors() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.ConstrainSponsors"));
    }


    /**
     *  Gets the CatalogueId element Id.
     *
     *  @return the CatalogueId element Id
     */

    public static org.osid.id.Id getCatalogueId() {
        return (makeElementId("osid.offering.rules.offeringconstrainer.CatalogueId"));
    }


    /**
     *  Gets the Catalogue element Id.
     *
     *  @return the Catalogue element Id
     */

    public static org.osid.id.Id getCatalogue() {
        return (makeQueryElementId("osid.offering.rules.offeringconstrainer.Catalogue"));
    }
}
