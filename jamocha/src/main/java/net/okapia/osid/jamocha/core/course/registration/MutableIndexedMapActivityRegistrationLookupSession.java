//
// MutableIndexedMapActivityRegistrationLookupSession
//
//    Implements an ActivityRegistration lookup service backed by a collection of
//    activityRegistrations indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration;


/**
 *  Implements an ActivityRegistration lookup service backed by a collection of
 *  activity registrations. The activity registrations are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some activity registrations may be compatible
 *  with more types than are indicated through these activity registration
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of activity registrations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapActivityRegistrationLookupSession
    extends net.okapia.osid.jamocha.core.course.registration.spi.AbstractIndexedMapActivityRegistrationLookupSession
    implements org.osid.course.registration.ActivityRegistrationLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapActivityRegistrationLookupSession} with no activity registrations.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

      public MutableIndexedMapActivityRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapActivityRegistrationLookupSession} with a
     *  single activity registration.
     *  
     *  @param courseCatalog the course catalog
     *  @param  activityRegistration an single activityRegistration
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code activityRegistration} is {@code null}
     */

    public MutableIndexedMapActivityRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.registration.ActivityRegistration activityRegistration) {
        this(courseCatalog);
        putActivityRegistration(activityRegistration);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapActivityRegistrationLookupSession} using an
     *  array of activity registrations.
     *
     *  @param courseCatalog the course catalog
     *  @param  activityRegistrations an array of activity registrations
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code activityRegistrations} is {@code null}
     */

    public MutableIndexedMapActivityRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.registration.ActivityRegistration[] activityRegistrations) {
        this(courseCatalog);
        putActivityRegistrations(activityRegistrations);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapActivityRegistrationLookupSession} using a
     *  collection of activity registrations.
     *
     *  @param courseCatalog the course catalog
     *  @param  activityRegistrations a collection of activity registrations
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code activityRegistrations} is {@code null}
     */

    public MutableIndexedMapActivityRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.registration.ActivityRegistration> activityRegistrations) {

        this(courseCatalog);
        putActivityRegistrations(activityRegistrations);
        return;
    }
    

    /**
     *  Makes an {@code ActivityRegistration} available in this session.
     *
     *  @param  activityRegistration an activity registration
     *  @throws org.osid.NullArgumentException {@code activityRegistration{@code  is
     *          {@code null}
     */

    @Override
    public void putActivityRegistration(org.osid.course.registration.ActivityRegistration activityRegistration) {
        super.putActivityRegistration(activityRegistration);
        return;
    }


    /**
     *  Makes an array of activity registrations available in this session.
     *
     *  @param  activityRegistrations an array of activity registrations
     *  @throws org.osid.NullArgumentException {@code activityRegistrations{@code 
     *          is {@code null}
     */

    @Override
    public void putActivityRegistrations(org.osid.course.registration.ActivityRegistration[] activityRegistrations) {
        super.putActivityRegistrations(activityRegistrations);
        return;
    }


    /**
     *  Makes collection of activity registrations available in this session.
     *
     *  @param  activityRegistrations a collection of activity registrations
     *  @throws org.osid.NullArgumentException {@code activityRegistration{@code  is
     *          {@code null}
     */

    @Override
    public void putActivityRegistrations(java.util.Collection<? extends org.osid.course.registration.ActivityRegistration> activityRegistrations) {
        super.putActivityRegistrations(activityRegistrations);
        return;
    }


    /**
     *  Removes an ActivityRegistration from this session.
     *
     *  @param activityRegistrationId the {@code Id} of the activity registration
     *  @throws org.osid.NullArgumentException {@code activityRegistrationId{@code  is
     *          {@code null}
     */

    @Override
    public void removeActivityRegistration(org.osid.id.Id activityRegistrationId) {
        super.removeActivityRegistration(activityRegistrationId);
        return;
    }    
}
