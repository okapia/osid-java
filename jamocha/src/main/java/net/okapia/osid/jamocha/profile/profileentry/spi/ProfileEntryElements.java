//
// ProfileEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profileentry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProfileEntryElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ProfileEntryElement Id.
     *
     *  @return the profile entry element Id
     */

    public static org.osid.id.Id getProfileEntryEntityId() {
        return (makeEntityId("osid.profile.ProfileEntry"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.profile.profileentry.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.profile.profileentry.Resource"));
    }


    /**
     *  Gets the AgentId element Id.
     *
     *  @return the AgentId element Id
     */

    public static org.osid.id.Id getAgentId() {
        return (makeElementId("osid.profile.profileentry.AgentId"));
    }


    /**
     *  Gets the Agent element Id.
     *
     *  @return the Agent element Id
     */

    public static org.osid.id.Id getAgent() {
        return (makeElementId("osid.profile.profileentry.Agent"));
    }


    /**
     *  Gets the ProfileItemId element Id.
     *
     *  @return the ProfileItemId element Id
     */

    public static org.osid.id.Id getProfileItemId() {
        return (makeElementId("osid.profile.profileentry.ProfileItemId"));
    }


    /**
     *  Gets the ProfileItem element Id.
     *
     *  @return the ProfileItem element Id
     */

    public static org.osid.id.Id getProfileItem() {
        return (makeElementId("osid.profile.profileentry.ProfileItem"));
    }


    /**
     *  Gets the Implicit element Id.
     *
     *  @return the Implicit element Id
     */

    public static org.osid.id.Id getImplicit() {
        return (makeElementId("osid.profile.profileentry.Implicit"));
    }


    /**
     *  Gets the RelatedProfileEntryId element Id.
     *
     *  @return the RelatedProfileEntryId element Id
     */

    public static org.osid.id.Id getRelatedProfileEntryId() {
        return (makeQueryElementId("osid.profile.profileentry.RelatedProfileEntryId"));
    }


    /**
     *  Gets the RelatedProfileEntry element Id.
     *
     *  @return the RelatedProfileEntry element Id
     */

    public static org.osid.id.Id getRelatedProfileEntry() {
        return (makeQueryElementId("osid.profile.profileentry.RelatedProfileEntry"));
    }


    /**
     *  Gets the ProfileId element Id.
     *
     *  @return the ProfileId element Id
     */

    public static org.osid.id.Id getProfileId() {
        return (makeQueryElementId("osid.profile.profileentry.ProfileId"));
    }


    /**
     *  Gets the Profile element Id.
     *
     *  @return the Profile element Id
     */

    public static org.osid.id.Id getProfile() {
        return (makeQueryElementId("osid.profile.profileentry.Profile"));
    }
}
