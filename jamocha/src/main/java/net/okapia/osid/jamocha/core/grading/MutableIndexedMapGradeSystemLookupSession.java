//
// MutableIndexedMapGradeSystemLookupSession
//
//    Implements a GradeSystem lookup service backed by a collection of
//    gradeSystems indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading;


/**
 *  Implements a GradeSystem lookup service backed by a collection of
 *  grade systems. The grade systems are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some grade systems may be compatible
 *  with more types than are indicated through these grade system
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of grade systems can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapGradeSystemLookupSession
    extends net.okapia.osid.jamocha.core.grading.spi.AbstractIndexedMapGradeSystemLookupSession
    implements org.osid.grading.GradeSystemLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapGradeSystemLookupSession} with no grade systems.
     *
     *  @param gradebook the gradebook
     *  @throws org.osid.NullArgumentException {@code gradebook}
     *          is {@code null}
     */

      public MutableIndexedMapGradeSystemLookupSession(org.osid.grading.Gradebook gradebook) {
        setGradebook(gradebook);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGradeSystemLookupSession} with a
     *  single grade system.
     *  
     *  @param gradebook the gradebook
     *  @param  gradeSystem a single gradeSystem
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradeSystem} is {@code null}
     */

    public MutableIndexedMapGradeSystemLookupSession(org.osid.grading.Gradebook gradebook,
                                                  org.osid.grading.GradeSystem gradeSystem) {
        this(gradebook);
        putGradeSystem(gradeSystem);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGradeSystemLookupSession} using an
     *  array of grade systems.
     *
     *  @param gradebook the gradebook
     *  @param  gradeSystems an array of grade systems
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradeSystems} is {@code null}
     */

    public MutableIndexedMapGradeSystemLookupSession(org.osid.grading.Gradebook gradebook,
                                                  org.osid.grading.GradeSystem[] gradeSystems) {
        this(gradebook);
        putGradeSystems(gradeSystems);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGradeSystemLookupSession} using a
     *  collection of grade systems.
     *
     *  @param gradebook the gradebook
     *  @param  gradeSystems a collection of grade systems
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradeSystems} is {@code null}
     */

    public MutableIndexedMapGradeSystemLookupSession(org.osid.grading.Gradebook gradebook,
                                                  java.util.Collection<? extends org.osid.grading.GradeSystem> gradeSystems) {

        this(gradebook);
        putGradeSystems(gradeSystems);
        return;
    }
    

    /**
     *  Makes a {@code GradeSystem} available in this session.
     *
     *  @param  gradeSystem a grade system
     *  @throws org.osid.NullArgumentException {@code gradeSystem{@code  is
     *          {@code null}
     */

    @Override
    public void putGradeSystem(org.osid.grading.GradeSystem gradeSystem) {
        super.putGradeSystem(gradeSystem);
        return;
    }


    /**
     *  Makes an array of grade systems available in this session.
     *
     *  @param  gradeSystems an array of grade systems
     *  @throws org.osid.NullArgumentException {@code gradeSystems{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeSystems(org.osid.grading.GradeSystem[] gradeSystems) {
        super.putGradeSystems(gradeSystems);
        return;
    }


    /**
     *  Makes collection of grade systems available in this session.
     *
     *  @param  gradeSystems a collection of grade systems
     *  @throws org.osid.NullArgumentException {@code gradeSystem{@code  is
     *          {@code null}
     */

    @Override
    public void putGradeSystems(java.util.Collection<? extends org.osid.grading.GradeSystem> gradeSystems) {
        super.putGradeSystems(gradeSystems);
        return;
    }


    /**
     *  Removes a GradeSystem from this session.
     *
     *  @param gradeSystemId the {@code Id} of the grade system
     *  @throws org.osid.NullArgumentException {@code gradeSystemId{@code  is
     *          {@code null}
     */

    @Override
    public void removeGradeSystem(org.osid.id.Id gradeSystemId) {
        super.removeGradeSystem(gradeSystemId);
        return;
    }    
}
