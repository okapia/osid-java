//
// AbstractMapCanonicalUnitProcessorLookupSession
//
//    A simple framework for providing a CanonicalUnitProcessor lookup service
//    backed by a fixed collection of canonical unit processors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CanonicalUnitProcessor lookup service backed by a
 *  fixed collection of canonical unit processors. The canonical unit processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CanonicalUnitProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCanonicalUnitProcessorLookupSession
    extends net.okapia.osid.jamocha.offering.rules.spi.AbstractCanonicalUnitProcessorLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.rules.CanonicalUnitProcessor>());


    /**
     *  Makes a <code>CanonicalUnitProcessor</code> available in this session.
     *
     *  @param  canonicalUnitProcessor a canonical unit processor
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessor<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnitProcessor(org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor) {
        this.canonicalUnitProcessors.put(canonicalUnitProcessor.getId(), canonicalUnitProcessor);
        return;
    }


    /**
     *  Makes an array of canonical unit processors available in this session.
     *
     *  @param  canonicalUnitProcessors an array of canonical unit processors
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessors<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnitProcessors(org.osid.offering.rules.CanonicalUnitProcessor[] canonicalUnitProcessors) {
        putCanonicalUnitProcessors(java.util.Arrays.asList(canonicalUnitProcessors));
        return;
    }


    /**
     *  Makes a collection of canonical unit processors available in this session.
     *
     *  @param  canonicalUnitProcessors a collection of canonical unit processors
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessors<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnitProcessors(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessors) {
        for (org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor : canonicalUnitProcessors) {
            this.canonicalUnitProcessors.put(canonicalUnitProcessor.getId(), canonicalUnitProcessor);
        }

        return;
    }


    /**
     *  Removes a CanonicalUnitProcessor from this session.
     *
     *  @param  canonicalUnitProcessorId the <code>Id</code> of the canonical unit processor
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorId<code> is
     *          <code>null</code>
     */

    protected void removeCanonicalUnitProcessor(org.osid.id.Id canonicalUnitProcessorId) {
        this.canonicalUnitProcessors.remove(canonicalUnitProcessorId);
        return;
    }


    /**
     *  Gets the <code>CanonicalUnitProcessor</code> specified by its <code>Id</code>.
     *
     *  @param  canonicalUnitProcessorId <code>Id</code> of the <code>CanonicalUnitProcessor</code>
     *  @return the canonicalUnitProcessor
     *  @throws org.osid.NotFoundException <code>canonicalUnitProcessorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessor getCanonicalUnitProcessor(org.osid.id.Id canonicalUnitProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor = this.canonicalUnitProcessors.get(canonicalUnitProcessorId);
        if (canonicalUnitProcessor == null) {
            throw new org.osid.NotFoundException("canonicalUnitProcessor not found: " + canonicalUnitProcessorId);
        }

        return (canonicalUnitProcessor);
    }


    /**
     *  Gets all <code>CanonicalUnitProcessors</code>. In plenary mode, the returned
     *  list contains all known canonicalUnitProcessors or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonicalUnitProcessors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CanonicalUnitProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.ArrayCanonicalUnitProcessorList(this.canonicalUnitProcessors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.canonicalUnitProcessors.clear();
        super.close();
        return;
    }
}
