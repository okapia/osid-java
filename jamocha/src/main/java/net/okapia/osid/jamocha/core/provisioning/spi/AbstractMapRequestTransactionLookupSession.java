//
// AbstractMapRequestTransactionLookupSession
//
//    A simple framework for providing a RequestTransaction lookup service
//    backed by a fixed collection of request transactions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a RequestTransaction lookup service backed by a
 *  fixed collection of request transactions. The request transactions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RequestTransactions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRequestTransactionLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractRequestTransactionLookupSession
    implements org.osid.provisioning.RequestTransactionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.RequestTransaction> requestTransactions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.RequestTransaction>());


    /**
     *  Makes a <code>RequestTransaction</code> available in this session.
     *
     *  @param  requestTransaction a request transaction
     *  @throws org.osid.NullArgumentException <code>requestTransaction<code>
     *          is <code>null</code>
     */

    protected void putRequestTransaction(org.osid.provisioning.RequestTransaction requestTransaction) {
        this.requestTransactions.put(requestTransaction.getId(), requestTransaction);
        return;
    }


    /**
     *  Makes an array of request transactions available in this session.
     *
     *  @param  requestTransactions an array of request transactions
     *  @throws org.osid.NullArgumentException <code>requestTransactions<code>
     *          is <code>null</code>
     */

    protected void putRequestTransactions(org.osid.provisioning.RequestTransaction[] requestTransactions) {
        putRequestTransactions(java.util.Arrays.asList(requestTransactions));
        return;
    }


    /**
     *  Makes a collection of request transactions available in this session.
     *
     *  @param  requestTransactions a collection of request transactions
     *  @throws org.osid.NullArgumentException <code>requestTransactions<code>
     *          is <code>null</code>
     */

    protected void putRequestTransactions(java.util.Collection<? extends org.osid.provisioning.RequestTransaction> requestTransactions) {
        for (org.osid.provisioning.RequestTransaction requestTransaction : requestTransactions) {
            this.requestTransactions.put(requestTransaction.getId(), requestTransaction);
        }

        return;
    }


    /**
     *  Removes a RequestTransaction from this session.
     *
     *  @param  requestTransactionId the <code>Id</code> of the request transaction
     *  @throws org.osid.NullArgumentException <code>requestTransactionId<code> is
     *          <code>null</code>
     */

    protected void removeRequestTransaction(org.osid.id.Id requestTransactionId) {
        this.requestTransactions.remove(requestTransactionId);
        return;
    }


    /**
     *  Gets the <code>RequestTransaction</code> specified by its <code>Id</code>.
     *
     *  @param  requestTransactionId <code>Id</code> of the <code>RequestTransaction</code>
     *  @return the requestTransaction
     *  @throws org.osid.NotFoundException <code>requestTransactionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>requestTransactionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransaction getRequestTransaction(org.osid.id.Id requestTransactionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.RequestTransaction requestTransaction = this.requestTransactions.get(requestTransactionId);
        if (requestTransaction == null) {
            throw new org.osid.NotFoundException("requestTransaction not found: " + requestTransactionId);
        }

        return (requestTransaction);
    }


    /**
     *  Gets all <code>RequestTransactions</code>. In plenary mode, the returned
     *  list contains all known requestTransactions or an error
     *  results. Otherwise, the returned list may contain only those
     *  requestTransactions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>RequestTransactions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.requesttransaction.ArrayRequestTransactionList(this.requestTransactions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.requestTransactions.clear();
        super.close();
        return;
    }
}
