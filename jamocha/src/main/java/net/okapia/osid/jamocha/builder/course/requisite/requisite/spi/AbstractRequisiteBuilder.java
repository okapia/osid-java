//
// AbstractRequisite.java
//
//     Defines a Requisite builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.requisite.spi;


/**
 *  Defines a <code>Requisite</code> builder.
 */

public abstract class AbstractRequisiteBuilder<T extends AbstractRequisiteBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.requisite.requisite.RequisiteMiter requisite;


    /**
     *  Constructs a new <code>AbstractRequisiteBuilder</code>.
     *
     *  @param requisite the requisite to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRequisiteBuilder(net.okapia.osid.jamocha.builder.course.requisite.requisite.RequisiteMiter requisite) {
        super(requisite);
        this.requisite = requisite;
        return;
    }


    /**
     *  Builds the requisite.
     *
     *  @return the new requisite
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.requisite.Requisite build() {
        (new net.okapia.osid.jamocha.builder.validator.course.requisite.requisite.RequisiteValidator(getValidations())).validate(this.requisite);
        return (new net.okapia.osid.jamocha.builder.course.requisite.requisite.ImmutableRequisite(this.requisite));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the requisite miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.requisite.requisite.RequisiteMiter getMiter() {
        return (this.requisite);
    }


    /**
     *  Sets the sequestered flag.
     */

    public T sequestered() {
        getMiter().setSequestered(true);
        return (self());
    }


    /**
     *  Sets the unsequestered flag.
     */

    public T unsequestered() {
        getMiter().setSequestered(false);
        return (self());
    }


    /**
     *  Adds a requisite option.
     *
     *  @param option a requisite option
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public T requisiteOption(org.osid.course.requisite.Requisite option) {
        getMiter().addRequisiteOption(option);
        return (self());
    }


    /**
     *  Sets all the requisite options.
     *
     *  @param options a collection of requisite options
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public T requisiteOptions(java.util.Collection<org.osid.course.requisite.Requisite> options) {
        getMiter().setRequisiteOptions(options);
        return (self());
    }


    /**
     *  Adds a course requirement.
     *
     *  @param requirement a course requirement
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public T courseRequirement(org.osid.course.requisite.CourseRequirement requirement) {
        getMiter().addCourseRequirement(requirement);
        return (self());
    }


    /**
     *  Sets the course requirements.
     *
     *  @param requirements a collection of course requirements
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public T courseRequirements(java.util.Collection<org.osid.course.requisite.CourseRequirement> requirements) {
        getMiter().setCourseRequirements(requirements);
        return (self());
    }


    /**
     *  Adds a program requirement.
     *
     *  @param requirement a program requirement
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public T programRequirement(org.osid.course.requisite.ProgramRequirement requirement) {
        getMiter().addProgramRequirement(requirement);
        return (self());
    }


    /**
     *  Sets the program requirements.
     *
     *  @param requirements a collection of program requirements
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public T programRequirements(java.util.Collection<org.osid.course.requisite.ProgramRequirement> requirements) {
        getMiter().setProgramRequirements(requirements);
        return (self());
    }


    /**
     *  Adds a credential requirement.
     *
     *  @param requirement a credential requirement
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public T credentialRequirement(org.osid.course.requisite.CredentialRequirement requirement) {
        getMiter().addCredentialRequirement(requirement);
        return (self());
    }


    /**
     *  Sets the credential requirements.
     *
     *  @param requirements a collection of credential requirements
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public T credentialRequirements(java.util.Collection<org.osid.course.requisite.CredentialRequirement> requirements) {
        getMiter().setCredentialRequirements(requirements);
        return (self());
    }


    /**
     *  Adds a learning objective requirement.
     *
     *  @param requirement a learning objective requirement
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public T learningObjectiveRequirement(org.osid.course.requisite.LearningObjectiveRequirement requirement) {
        getMiter().addLearningObjectiveRequirement(requirement);
        return (self());
    }


    /**
     *  Sets the learning objective requirements.
     *
     *  @param requirements a collection of learning objective requirements
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public T learningObjectiveRequirements(java.util.Collection<org.osid.course.requisite.LearningObjectiveRequirement> requirements) {
        getMiter().setLearningObjectiveRequirements(requirements);
        return (self());
    }


    /**
     *  Adds an assessment requirement.
     *
     *  @param requirement an assessment requirement
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public T assessmentRequirement(org.osid.course.requisite.AssessmentRequirement requirement) {
        getMiter().addAssessmentRequirement(requirement);
        return (self());
    }


    /**
     *  Sets the assessment requirements.
     *
     *  @param requirements a collection of assessment requirements
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public T assessmentRequirements(java.util.Collection<org.osid.course.requisite.AssessmentRequirement> requirements) {
        getMiter().setAssessmentRequirements(requirements);
        return (self());
    }


    /**
     *  Adds an award requirement.
     *
     *  @param requirement an award requirement
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public T awardRequirement(org.osid.course.requisite.AwardRequirement requirement) {
        getMiter().addAwardRequirement(requirement);
        return (self());
    }


    /**
     *  Sets the award requirements.
     *
     *  @param requirements a collection of award requirements
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public T awardRequirements(java.util.Collection<org.osid.course.requisite.AwardRequirement> requirements) {
        getMiter().setAwardRequirements(requirements);
        return (self());
    }


    /**
     *  Adds a Requisite record.
     *
     *  @param record a requisite record
     *  @param recordType the type of requisite record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.requisite.records.RequisiteRecord record, org.osid.type.Type recordType) {
        getMiter().addRequisiteRecord(record, recordType);
        return (self());
    }
}       


