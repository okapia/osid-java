//
// AbstractRequisiteSearchOdrer.java
//
//     Defines a RequisiteSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.requisite.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code RequisiteSearchOrder}.
 */

public abstract class AbstractRequisiteSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerSearchOrder
    implements org.osid.course.requisite.RequisiteSearchOrder {

    private final java.util.Collection<org.osid.course.requisite.records.RequisiteSearchOrderRecord> records = new java.util.LinkedHashSet<>();

    private final OsidContainableSearchOrder order = new OsidContainableSearchOrder();


    /**
     *  Specifies a preference for ordering the result set by the
     *  sequestered flag.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */
    
    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.order.orderBySequestered(style);
        return;
    }


    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  requisiteRecordType a requisite record type 
     *  @return {@code true} if the requisiteRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code requisiteRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type requisiteRecordType) {
        for (org.osid.course.requisite.records.RequisiteSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  requisiteRecordType the requisite record type 
     *  @return the requisite search order record
     *  @throws org.osid.NullArgumentException
     *          {@code requisiteRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(requisiteRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteSearchOrderRecord getRequisiteSearchOrderRecord(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.RequisiteSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requisiteRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this requisite. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param requisiteRecord the requisite search odrer record
     *  @param requisiteRecordType requisite record type
     *  @throws org.osid.NullArgumentException
     *          {@code requisiteRecord} or
     *          {@code requisiteRecordTyperequisite} is
     *          {@code null}
     */
            
    protected void addRequisiteRecord(org.osid.course.requisite.records.RequisiteSearchOrderRecord requisiteSearchOrderRecord, 
                                     org.osid.type.Type requisiteRecordType) {

        addRecordType(requisiteRecordType);
        this.records.add(requisiteSearchOrderRecord);
        
        return;
    }


    protected class OsidContainableSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableSearchOrder
        implements org.osid.OsidContainableSearchOrder {
    }
}
