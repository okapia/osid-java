//
// InvariantMapProxyObjectiveBankLookupSession
//
//    Implements an ObjectiveBank lookup service backed by a fixed
//    collection of objectiveBanks. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning;


/**
 *  Implements an ObjectiveBank lookup service backed by a fixed
 *  collection of objective banks. The objective banks are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyObjectiveBankLookupSession
    extends net.okapia.osid.jamocha.core.learning.spi.AbstractMapObjectiveBankLookupSession
    implements org.osid.learning.ObjectiveBankLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyObjectiveBankLookupSession} with no
     *  objective banks.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyObjectiveBankLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyObjectiveBankLookupSession} with a
     *  single objective bank.
     *
     *  @param objectiveBank an single objective bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyObjectiveBankLookupSession(org.osid.learning.ObjectiveBank objectiveBank, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putObjectiveBank(objectiveBank);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyObjectiveBankLookupSession} using
     *  an array of objective banks.
     *
     *  @param objectiveBanks an array of objective banks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBanks} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyObjectiveBankLookupSession(org.osid.learning.ObjectiveBank[] objectiveBanks, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putObjectiveBanks(objectiveBanks);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyObjectiveBankLookupSession} using a
     *  collection of objective banks.
     *
     *  @param objectiveBanks a collection of objective banks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBanks} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyObjectiveBankLookupSession(java.util.Collection<? extends org.osid.learning.ObjectiveBank> objectiveBanks,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putObjectiveBanks(objectiveBanks);
        return;
    }
}
