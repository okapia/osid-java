//
// AbstractAssessmentOffered.java
//
//     Defines an AssessmentOffered builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmentoffered.spi;


/**
 *  Defines an <code>AssessmentOffered</code> builder.
 */

public abstract class AbstractAssessmentOfferedBuilder<T extends AbstractAssessmentOfferedBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.assessment.assessmentoffered.AssessmentOfferedMiter assessmentOffered;


    /**
     *  Constructs a new <code>AbstractAssessmentOfferedBuilder</code>.
     *
     *  @param assessmentOffered the assessment offered to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAssessmentOfferedBuilder(net.okapia.osid.jamocha.builder.assessment.assessmentoffered.AssessmentOfferedMiter assessmentOffered) {
        super(assessmentOffered);
        this.assessmentOffered = assessmentOffered;
        return;
    }


    /**
     *  Builds the assessment offered.
     *
     *  @return the new assessment offered
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.assessment.AssessmentOffered build() {
        (new net.okapia.osid.jamocha.builder.validator.assessment.assessmentoffered.AssessmentOfferedValidator(getValidations())).validate(this.assessmentOffered);
        return (new net.okapia.osid.jamocha.builder.assessment.assessmentoffered.ImmutableAssessmentOffered(this.assessmentOffered));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the assessment offered miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.assessment.assessmentoffered.AssessmentOfferedMiter getMiter() {
        return (this.assessmentOffered);
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    public T assessment(org.osid.assessment.Assessment assessment) {
        getMiter().setAssessment(assessment);
        return (self());
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>level</code> is <code>null</code>
     */

    public T level(org.osid.grading.Grade level) {
        getMiter().setLevel(level);
        return (self());
    }


    /**
     *  Sets the sequential flag.
     *
     *  @param sequential <code> true </code> if the items are taken
     *         sequentially, <code> false </code> if the items can be
     *         skipped and revisited
     *  @return the builder
     */

    public T itemsSequential(boolean sequential) {
        getMiter().setItemsSequential(sequential);
        return (self());
    }


    /**
     *  Sets the shuffled flag.
     *
     *  @param shuffled <code> true </code> if the items appear in a
     *         random order, <code> false </code> otherwise
     *  @return the builder
     */

    public T itemsShuffled(boolean shuffled) {
        getMiter().setItemsShuffled(shuffled);
        return (self());
    }


    /**
     *  Sets the start time.
     *
     *  @param startTime a start time
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>startTime</code> is <code>null</code>
     */

    public T startTime(org.osid.calendaring.DateTime startTime) {
        getMiter().setStartTime(startTime);
        return (self());
    }


    /**
     *  Sets the deadline.
     *
     *  @param deadline a deadline
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>deadline</code> is <code>null</code>
     */

    public T deadline(org.osid.calendaring.DateTime deadline) {
        getMiter().setDeadline(deadline);
        return (self());
    }


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    public T duration(org.osid.calendaring.Duration duration) {
        getMiter().setDuration(duration);
        return (self());
    }


    /**
     *  Sets the rubric.
     *
     *  @param assessmentOffered the rubric assessment offered
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOffered</code> is <code>null</code>
     */

    public T rubric(org.osid.assessment.AssessmentOffered assessmentOffered) {
        getMiter().setRubric(assessmentOffered);
        return (self());
    }


    /**
     *  Sets the score system.
     *
     *  @param gradeSystem the scoring system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    public T setScoreSystem(org.osid.grading.GradeSystem gradeSystem) {
        getMiter().setScoreSystem(gradeSystem);
        return (self());
    }


    /**
     *  Sets the grade system.
     *
     *  @param gradeSystem the scoring system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    public T setGradeSystem(org.osid.grading.GradeSystem gradeSystem) {
        getMiter().setGradeSystem(gradeSystem);
        return (self());
    }


    /**
     *  Adds an AssessmentOffered record.
     *
     *  @param record an assessment offered record
     *  @param recordType the type of assessment offered record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.assessment.records.AssessmentOfferedRecord record, org.osid.type.Type recordType) {
        getMiter().addAssessmentOfferedRecord(record, recordType);
        return (self());
    }
}       


