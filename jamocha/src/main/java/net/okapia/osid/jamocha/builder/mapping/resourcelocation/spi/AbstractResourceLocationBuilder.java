//
// AbstractResourceLocation.java
//
//     Defines a ResourceLocation builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.resourcelocation.spi;


/**
 *  Defines a <code>ResourceLocation</code> builder.
 */

public abstract class AbstractResourceLocationBuilder<T extends AbstractResourceLocationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCompendiumBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.resourcelocation.ResourceLocationMiter resourceLocation;


    /**
     *  Constructs a new <code>AbstractResourceLocationBuilder</code>.
     *
     *  @param resourceLocation the resource coordinare to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractResourceLocationBuilder(net.okapia.osid.jamocha.builder.mapping.resourcelocation.ResourceLocationMiter resourceLocation) {
        super(resourceLocation);
        this.resourceLocation = resourceLocation;
        return;
    }


    /**
     *  Builds the resource location.
     *
     *  @return the new resource location
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.ResourceLocation build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.resourcelocation.ResourceLocationValidator(getValidations())).validate(this.resourceLocation);
        return (new net.okapia.osid.jamocha.builder.mapping.resourcelocation.ImmutableResourceLocation(this.resourceLocation));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the resource location miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.resourcelocation.ResourceLocationMiter getMiter() {
        return (this.resourceLocation);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource the resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */
    
    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the location.
     *
     *  @param location the location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public T location(org.osid.mapping.Location location) {
        getMiter().setLocation(location);
        return (self());
    }


    /**
     *  Sets the coordinate.
     *
     *  @param coordinate the coordinate
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public T coordinate(org.osid.mapping.Coordinate coordinate) {
        getMiter().setCoordinate(coordinate);
        return (self());
    }


    /**
     *  Adds a ResourceLocation record.
     *
     *  @param record a resource location record
     *  @param recordType the type of resource location record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.records.ResourceLocationRecord record, org.osid.type.Type recordType) {
        getMiter().addResourceLocationRecord(record, recordType);
        return (self());
    }
}       


