//
// AbstractIndexedMapActivityUnitLookupSession.java
//
//    A simple framework for providing an ActivityUnit lookup service
//    backed by a fixed collection of activity units with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an ActivityUnit lookup service backed by a
 *  fixed collection of activity units. The activity units are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some activity units may be compatible
 *  with more types than are indicated through these activity unit
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActivityUnits</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapActivityUnitLookupSession
    extends AbstractMapActivityUnitLookupSession
    implements org.osid.course.ActivityUnitLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.ActivityUnit> activityUnitsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.ActivityUnit>());
    private final MultiMap<org.osid.type.Type, org.osid.course.ActivityUnit> activityUnitsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.ActivityUnit>());


    /**
     *  Makes an <code>ActivityUnit</code> available in this session.
     *
     *  @param  activityUnit an activity unit
     *  @throws org.osid.NullArgumentException <code>activityUnit<code> is
     *          <code>null</code>
     */

    @Override
    protected void putActivityUnit(org.osid.course.ActivityUnit activityUnit) {
        super.putActivityUnit(activityUnit);

        this.activityUnitsByGenus.put(activityUnit.getGenusType(), activityUnit);
        
        try (org.osid.type.TypeList types = activityUnit.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.activityUnitsByRecord.put(types.getNextType(), activityUnit);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an activity unit from this session.
     *
     *  @param activityUnitId the <code>Id</code> of the activity unit
     *  @throws org.osid.NullArgumentException <code>activityUnitId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeActivityUnit(org.osid.id.Id activityUnitId) {
        org.osid.course.ActivityUnit activityUnit;
        try {
            activityUnit = getActivityUnit(activityUnitId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.activityUnitsByGenus.remove(activityUnit.getGenusType());

        try (org.osid.type.TypeList types = activityUnit.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.activityUnitsByRecord.remove(types.getNextType(), activityUnit);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeActivityUnit(activityUnitId);
        return;
    }


    /**
     *  Gets an <code>ActivityUnitList</code> corresponding to the given
     *  activity unit genus <code>Type</code> which does not include
     *  activity units of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known activity units or an error results. Otherwise,
     *  the returned list may contain only those activity units that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  activityUnitGenusType an activity unit genus type 
     *  @return the returned <code>ActivityUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByGenusType(org.osid.type.Type activityUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.activityunit.ArrayActivityUnitList(this.activityUnitsByGenus.get(activityUnitGenusType)));
    }


    /**
     *  Gets an <code>ActivityUnitList</code> containing the given
     *  activity unit record <code>Type</code>. In plenary mode, the
     *  returned list contains all known activity units or an error
     *  results. Otherwise, the returned list may contain only those
     *  activity units that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  activityUnitRecordType an activity unit record type 
     *  @return the returned <code>activityUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByRecordType(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.activityunit.ArrayActivityUnitList(this.activityUnitsByRecord.get(activityUnitRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.activityUnitsByGenus.clear();
        this.activityUnitsByRecord.clear();

        super.close();

        return;
    }
}
