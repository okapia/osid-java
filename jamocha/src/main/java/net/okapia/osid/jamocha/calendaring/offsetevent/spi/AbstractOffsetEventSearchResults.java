//
// AbstractOffsetEventSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractOffsetEventSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.OffsetEventSearchResults {

    private org.osid.calendaring.OffsetEventList offsetEvents;
    private final org.osid.calendaring.OffsetEventQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.records.OffsetEventSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractOffsetEventSearchResults.
     *
     *  @param offsetEvents the result set
     *  @param offsetEventQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>offsetEvents</code>
     *          or <code>offsetEventQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractOffsetEventSearchResults(org.osid.calendaring.OffsetEventList offsetEvents,
                                            org.osid.calendaring.OffsetEventQueryInspector offsetEventQueryInspector) {
        nullarg(offsetEvents, "offset events");
        nullarg(offsetEventQueryInspector, "offset event query inspectpr");

        this.offsetEvents = offsetEvents;
        this.inspector = offsetEventQueryInspector;

        return;
    }


    /**
     *  Gets the offset event list resulting from a search.
     *
     *  @return an offset event list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEvents() {
        if (this.offsetEvents == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.OffsetEventList offsetEvents = this.offsetEvents;
        this.offsetEvents = null;
	return (offsetEvents);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.OffsetEventQueryInspector getOffsetEventQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  offset event search record <code> Type. </code> This method must
     *  be used to retrieve an offsetEvent implementing the requested
     *  record.
     *
     *  @param offsetEventSearchRecordType an offsetEvent search 
     *         record type 
     *  @return the offset event search
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(offsetEventSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventSearchResultsRecord getOffsetEventSearchResultsRecord(org.osid.type.Type offsetEventSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.records.OffsetEventSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(offsetEventSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(offsetEventSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record offset event search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addOffsetEventRecord(org.osid.calendaring.records.OffsetEventSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "offset event record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
