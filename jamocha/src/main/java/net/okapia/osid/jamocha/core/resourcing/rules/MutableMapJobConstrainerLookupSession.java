//
// MutableMapJobConstrainerLookupSession
//
//    Implements a JobConstrainer lookup service backed by a collection of
//    jobConstrainers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a JobConstrainer lookup service backed by a collection of
 *  job constrainers. The job constrainers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of job constrainers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapJobConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapJobConstrainerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerLookupSession {


    /**
     *  Constructs a new {@code MutableMapJobConstrainerLookupSession}
     *  with no job constrainers.
     *
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumentException {@code foundry} is
     *          {@code null}
     */

      public MutableMapJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJobConstrainerLookupSession} with a
     *  single jobConstrainer.
     *
     *  @param foundry the foundry  
     *  @param jobConstrainer a job constrainer
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainer} is {@code null}
     */

    public MutableMapJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                           org.osid.resourcing.rules.JobConstrainer jobConstrainer) {
        this(foundry);
        putJobConstrainer(jobConstrainer);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJobConstrainerLookupSession}
     *  using an array of job constrainers.
     *
     *  @param foundry the foundry
     *  @param jobConstrainers an array of job constrainers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainers} is {@code null}
     */

    public MutableMapJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                           org.osid.resourcing.rules.JobConstrainer[] jobConstrainers) {
        this(foundry);
        putJobConstrainers(jobConstrainers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJobConstrainerLookupSession}
     *  using a collection of job constrainers.
     *
     *  @param foundry the foundry
     *  @param jobConstrainers a collection of job constrainers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainers} is {@code null}
     */

    public MutableMapJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                           java.util.Collection<? extends org.osid.resourcing.rules.JobConstrainer> jobConstrainers) {

        this(foundry);
        putJobConstrainers(jobConstrainers);
        return;
    }

    
    /**
     *  Makes a {@code JobConstrainer} available in this session.
     *
     *  @param jobConstrainer a job constrainer
     *  @throws org.osid.NullArgumentException {@code jobConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putJobConstrainer(org.osid.resourcing.rules.JobConstrainer jobConstrainer) {
        super.putJobConstrainer(jobConstrainer);
        return;
    }


    /**
     *  Makes an array of job constrainers available in this session.
     *
     *  @param jobConstrainers an array of job constrainers
     *  @throws org.osid.NullArgumentException {@code jobConstrainers{@code 
     *          is {@code null}
     */

    @Override
    public void putJobConstrainers(org.osid.resourcing.rules.JobConstrainer[] jobConstrainers) {
        super.putJobConstrainers(jobConstrainers);
        return;
    }


    /**
     *  Makes collection of job constrainers available in this session.
     *
     *  @param jobConstrainers a collection of job constrainers
     *  @throws org.osid.NullArgumentException {@code jobConstrainers{@code  is
     *          {@code null}
     */

    @Override
    public void putJobConstrainers(java.util.Collection<? extends org.osid.resourcing.rules.JobConstrainer> jobConstrainers) {
        super.putJobConstrainers(jobConstrainers);
        return;
    }


    /**
     *  Removes a JobConstrainer from this session.
     *
     *  @param jobConstrainerId the {@code Id} of the job constrainer
     *  @throws org.osid.NullArgumentException {@code jobConstrainerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeJobConstrainer(org.osid.id.Id jobConstrainerId) {
        super.removeJobConstrainer(jobConstrainerId);
        return;
    }    
}
