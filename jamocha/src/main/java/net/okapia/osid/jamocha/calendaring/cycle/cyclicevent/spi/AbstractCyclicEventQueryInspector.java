//
// AbstractCyclicEventQueryInspector.java
//
//     A template for making a CyclicEventQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.cyclicevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for cyclic events.
 */

public abstract class AbstractCyclicEventQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.calendaring.cycle.CyclicEventQueryInspector {

    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicEventQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given cyclic event query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a cyclic event implementing the requested record.
     *
     *  @param cyclicEventRecordType a cyclic event record type
     *  @return the cyclic event query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicEventRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicEventQueryInspectorRecord getCyclicEventQueryInspectorRecord(org.osid.type.Type cyclicEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.cycle.records.CyclicEventQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(cyclicEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this cyclic event query. 
     *
     *  @param cyclicEventQueryInspectorRecord cyclic event query inspector
     *         record
     *  @param cyclicEventRecordType cyclicEvent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCyclicEventQueryInspectorRecord(org.osid.calendaring.cycle.records.CyclicEventQueryInspectorRecord cyclicEventQueryInspectorRecord, 
                                                   org.osid.type.Type cyclicEventRecordType) {

        addRecordType(cyclicEventRecordType);
        nullarg(cyclicEventRecordType, "cyclic event record type");
        this.records.add(cyclicEventQueryInspectorRecord);        
        return;
    }
}
