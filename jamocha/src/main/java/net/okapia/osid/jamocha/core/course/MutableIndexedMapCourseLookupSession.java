//
// MutableIndexedMapCourseLookupSession
//
//    Implements a Course lookup service backed by a collection of
//    courses indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements a Course lookup service backed by a collection of
 *  courses. The courses are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some courses may be compatible
 *  with more types than are indicated through these course
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of courses can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCourseLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractIndexedMapCourseLookupSession
    implements org.osid.course.CourseLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCourseLookupSession} with no courses.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

      public MutableIndexedMapCourseLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCourseLookupSession} with a
     *  single course.
     *  
     *  @param courseCatalog the course catalog
     *  @param  course a single course
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code course} is {@code null}
     */

    public MutableIndexedMapCourseLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.Course course) {
        this(courseCatalog);
        putCourse(course);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCourseLookupSession} using an
     *  array of courses.
     *
     *  @param courseCatalog the course catalog
     *  @param  courses an array of courses
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courses} is {@code null}
     */

    public MutableIndexedMapCourseLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.Course[] courses) {
        this(courseCatalog);
        putCourses(courses);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCourseLookupSession} using a
     *  collection of courses.
     *
     *  @param courseCatalog the course catalog
     *  @param  courses a collection of courses
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courses} is {@code null}
     */

    public MutableIndexedMapCourseLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.Course> courses) {

        this(courseCatalog);
        putCourses(courses);
        return;
    }
    

    /**
     *  Makes a {@code Course} available in this session.
     *
     *  @param  course a course
     *  @throws org.osid.NullArgumentException {@code course{@code  is
     *          {@code null}
     */

    @Override
    public void putCourse(org.osid.course.Course course) {
        super.putCourse(course);
        return;
    }


    /**
     *  Makes an array of courses available in this session.
     *
     *  @param  courses an array of courses
     *  @throws org.osid.NullArgumentException {@code courses{@code 
     *          is {@code null}
     */

    @Override
    public void putCourses(org.osid.course.Course[] courses) {
        super.putCourses(courses);
        return;
    }


    /**
     *  Makes collection of courses available in this session.
     *
     *  @param  courses a collection of courses
     *  @throws org.osid.NullArgumentException {@code course{@code  is
     *          {@code null}
     */

    @Override
    public void putCourses(java.util.Collection<? extends org.osid.course.Course> courses) {
        super.putCourses(courses);
        return;
    }


    /**
     *  Removes a Course from this session.
     *
     *  @param courseId the {@code Id} of the course
     *  @throws org.osid.NullArgumentException {@code courseId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCourse(org.osid.id.Id courseId) {
        super.removeCourse(courseId);
        return;
    }    
}
