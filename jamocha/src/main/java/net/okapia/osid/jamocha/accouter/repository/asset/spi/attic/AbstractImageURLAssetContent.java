//
// AbstractImageURLAssetContent
//
//     AssetContent for an image that reads it from a URL.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.accouter.repository.asset.spi;

import net.okapia.osid.primordium.type.BasicType;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  AssetContent for an image. The image is accessed through a path
 *  resource and currently only JPEG, PNG and GIF are supported.
 *
 *  The following AssetContentRecords are supported:
 *  <pre>
 *	urn:inet:osid.org:type:repository/AssetContent/Image/1
 *  </pre>
 */

public abstract class AbstractImageURLAssetContent
    extends AbstractURLAssetContent
    implements org.osid.repository.AssetContent,
	       org.osid.repository.AssetContentRecord {

    private String path;
    private String format;
    private long width  = -1;
    private long height = -1;
    private long size   = -1;
    private boolean rasterized;

    public static final org.osid.type.Type GENUS_TYPE                = BasicType.valueOf("genera:repository/AssetContent/image@okapia.net");
    public static final org.osid.type.Type IMAGE_RECORD_TYPE         = BasicType.valueOf("urn:inet:osid.org:types:record:repository/AssetContent/Image");

    /*
    private static final org.osid.impl.Property FORMAT_PROPERTY      = new org.osid.impl.Property("Image Format", "format", "The format of the image data.");    
    private static final org.osid.impl.Property RASTERIZED_PROPERTY  = new org.osid.impl.Property("Is Rasterized", "raasterized", "Whether this image is rasterized or the image represents a vector shape.");    
    private static final org.osid.impl.Property WIDTH_PROPERTY       = new org.osid.impl.Property("Image Width", "width", "The width of this image in pixels.");    
    private static final org.osid.impl.Property HEIGHT_PROPERTY      = new org.osid.impl.Property("Image Height", "height", "The height of this image in pixels.");    
    */

    private boolean populated = false;


    /**
     *  Constructs a new
     *  <code>AbstractImageURLAssetContent</code>. The Id will be the
     *  URL.
     *
     *  @param asset the Asset to which this content belongs
     *  @param url url or pathname to the file or location
     *  @throws org.osid.InvalidArgumentException <code>url</code> is invalid
     *  @throws org.osid.NotFoundException <code>url</code> not found
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     */

    protected AbstractImageURLAssetContent(org.osid.repository.Asset asset, String url)
	throws org.osid.NotFoundException,
	       org.osid.OperationFailedException {

	super(asset, url);
	setDisplayName(Plain.valueOf("Image"));
	setDescription(Plain.valueOf("Image content at " + url + "."));
	setGenusType(GENUS_TYPE);
	return;
    }


    /**
     *  Constructs a new
     *  <code>AbstractImageURLAssetContent</code>. The Id will be the
     *  URL.
     *
     *  @param asset the Asset to which this content belongs
     *  @param url url or pathname to the file or location
     *  @throws org.osid.InvalidArgumentException <code>url</code> is invalid
     *  @throws org.osid.NotFoundException <code>url</code> not found
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     */

    protected AbstractImageURLAssetContent(org.osid.repository.Asset asset, java.net.URL url)
	throws org.osid.NotFoundException,
	       org.osid.OperationFailedException {

	super(asset, url);
	setDisplayName(Plain.valueOf("Image"));
	setDescription(Plain.valueOf("Image content at " + url + "."));
	setGenusType(GENUS_TYPE);
	return;
    }


    protected void processFile(java.net.URLConnection connection)
	throws org.osid.OperationFailedException {

	javax.imageio.ImageIO.setUseCache(false);

	try {
	    javax.imageio.ImageReader reader = getImageReader(new java.io.BufferedInputStream(connection.getInputStream()));

	    if (reader.getNumImages(true) < 1) {
		throw new org.osid.OperationFailedException("no image found in " + getURL());
	    }
	    
	    this.format = reader.getFormatName().toUpperCase();

	    /* assuming there is only one image available through this reader */	    
	    this.width  = reader.getWidth(0);
	    this.height = reader.getHeight(0);

	    /* should deal with postscript & pdf */
	    if (this.format.equals("SVG")) {
		this.rasterized = false;
	    } else {
		this.rasterized = true;
	    }
	} catch (Exception e) {
	    throw new org.osid.OperationFailedException("cannot get image parameters for " + getURL(), e);
	}

	addAssetContentRecord(this, IMAGE_RECORD_TYPE);
	addProperties(makeImageProperties(), IMAGE_RECORD_TYPE);

	return;
    }


    /*
     *  image record methods
     */


    /**
     *  Tests if the given type is implemented by this record. Other types
     *  than that directly indicated by <code> getType() </code> may be
     *  supported through an inheritance scheme where the given type specifies
     *  a record that is a parent interface of the interface specified by
     *  <code> getType(). </code>
     *
     *  @param  recordType a type
     *  @return <code> true </code> if the given record <code> Type </code> is
     *          implemented by this record, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is
     *          <code> null </code>
     */

    public boolean implementsRecordType(org.osid.type.Type recordType) {
	return (hasRecordType(recordType));
    }


    /**
     *  Gets the <code> AssetContent </code> from which this record 
     *  originated. 
     *
     *  @return the asset content 
     */

    public org.osid.repository.AssetContent getAssetContent() {
	return (this);
    }


    /**
     *  Determines if this is as rasterized image. A rasterized
     *  image has a width and a height as opposed to a scalable
     *  vector image.
     *
     *  @return <code>true<code> if this image is rasterized,
     *          <code>false</code> otherwise
     */

    public boolean isRasterized() {
	return (this.rasterized);
    }


    /**
     *  Gets the width of this rasterized image in pixels.
     *
     *  @return the width of this rasterized image
     *  @throws org.osid.IllegalStateException image not rasterized
     */

    public long getWidth() {
	if (!isRasterized()) {
	    throw new org.osid.IllegalStateException("vector image");
	}

	return (this.width);
    }


    /**
     *  Gets the height of this rasterized image in pixels.
     *
     *  @return the height of this rasterized image
     *  @throws org.osid.IllegalStateException image not rasterized
     */

    public long getHeight() {
	if (!isRasterized()) {
	    throw new org.osid.IllegalStateException("vector image");
	}

	return (this.height);
    }


    /**
     *  Assemble properties for the image record.
     *
     *  @return a collection of properties
     */

    protected java.util.Collection<org.osid.Property> makeImageProperties() {
	java.util.ArrayList<org.osid.Property> list = new java.util.ArrayList<org.osid.Property>();
	
	list.add(new org.osid.impl.Property(FORMAT_PROPERTY, getFormatString(this.format)));
	
	if (isRasterized()) {
	    list.add(new org.osid.impl.Property(RASTERIZED_PROPERTY, "true"));
	} else {
	    list.add(new org.osid.impl.Property(RASTERIZED_PROPERTY, "false"));
	}
	
	list.add(new org.osid.impl.Property(WIDTH_PROPERTY, String.valueOf(getWidth()) + " pixels"));
	list.add(new org.osid.impl.Property(HEIGHT_PROPERTY, String.valueOf(getHeight()) + " pixels"));

	return (list);
    }


    /**
     *  Gets a format string for the fomrat property.
     *
     *  @param format
     *  @return formatted string
     */

    protected String getFormatString(String format) {
	if (format.equalsIgnoreCase("GIF")) {
	    return ("Graphics Interchange Format (GIF)");
	} else if (format.equalsIgnoreCase("JPG") || format.equalsIgnoreCase("JPEG")) {
	    return ("Joint Photographics Expert Group (JPEG)");
	} else if (format.equalsIgnoreCase("PNG")) {
	    return ("Portable Network Graphics (PNG)");
	} else if (format.equalsIgnoreCase("SVG")) {
	    return ("Scalable Vector Graphics Graphics (SVG)");
	} else if (format.equalsIgnoreCase("TIF") || format.equalsIgnoreCase("TIFF")) {
	    return ("Tagged Image File Format (TIFF)");
	} else {
	    return (this.format);
	}
    }
    

    /**
     *  Gets a image reader.
     *
     *  @param stream an input stream
     *  @return the image reader
     *  @throws org.osid.NullArgumentException <code>stream</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException could not get one
     */

    protected static javax.imageio.ImageReader getImageReader(java.io.InputStream stream)
	throws org.osid.OperationFailedException {

	if (stream == null) {
	    throw new org.osid.NullArgumentException("stream is null");
	}

	try {
	    javax.imageio.stream.ImageInputStream iis = javax.imageio.ImageIO.createImageInputStream(stream);
	    java.util.Iterator<javax.imageio.ImageReader> readers = javax.imageio.ImageIO.getImageReaders(iis);
	    
	    if (!readers.hasNext()) {
		throw new org.osid.OperationFailedException("no image reader available");
	    }
	    
	    /* 
	     * I don't know what to do if multiple readers are available.
	     */
	    
	    javax.imageio.ImageReader reader = readers.next();
	    reader.setInput(iis);
	    return (reader);
	} catch (Exception e) {
	    throw new org.osid.OperationFailedException("cannot read image" , e);
	}
    }
}

