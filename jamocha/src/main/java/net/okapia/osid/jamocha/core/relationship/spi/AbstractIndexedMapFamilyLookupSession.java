//
// AbstractIndexedMapFamilyLookupSession.java
//
//    A simple framework for providing a Family lookup service
//    backed by a fixed collection of families with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Family lookup service backed by a
 *  fixed collection of families. The families are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some families may be compatible
 *  with more types than are indicated through these family
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Families</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapFamilyLookupSession
    extends AbstractMapFamilyLookupSession
    implements org.osid.relationship.FamilyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.relationship.Family> familiesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.relationship.Family>());
    private final MultiMap<org.osid.type.Type, org.osid.relationship.Family> familiesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.relationship.Family>());


    /**
     *  Makes a <code>Family</code> available in this session.
     *
     *  @param  family a family
     *  @throws org.osid.NullArgumentException <code>family<code> is
     *          <code>null</code>
     */

    @Override
    protected void putFamily(org.osid.relationship.Family family) {
        super.putFamily(family);

        this.familiesByGenus.put(family.getGenusType(), family);
        
        try (org.osid.type.TypeList types = family.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.familiesByRecord.put(types.getNextType(), family);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a family from this session.
     *
     *  @param familyId the <code>Id</code> of the family
     *  @throws org.osid.NullArgumentException <code>familyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeFamily(org.osid.id.Id familyId) {
        org.osid.relationship.Family family;
        try {
            family = getFamily(familyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.familiesByGenus.remove(family.getGenusType());

        try (org.osid.type.TypeList types = family.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.familiesByRecord.remove(types.getNextType(), family);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeFamily(familyId);
        return;
    }


    /**
     *  Gets a <code>FamilyList</code> corresponding to the given
     *  family genus <code>Type</code> which does not include
     *  families of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known families or an error results. Otherwise,
     *  the returned list may contain only those families that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  familyGenusType a family genus type 
     *  @return the returned <code>Family</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>familyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByGenusType(org.osid.type.Type familyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.relationship.family.ArrayFamilyList(this.familiesByGenus.get(familyGenusType)));
    }


    /**
     *  Gets a <code>FamilyList</code> containing the given
     *  family record <code>Type</code>. In plenary mode, the
     *  returned list contains all known families or an error
     *  results. Otherwise, the returned list may contain only those
     *  families that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  familyRecordType a family record type 
     *  @return the returned <code>family</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>familyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByRecordType(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.relationship.family.ArrayFamilyList(this.familiesByRecord.get(familyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.familiesByGenus.clear();
        this.familiesByRecord.clear();

        super.close();

        return;
    }
}
