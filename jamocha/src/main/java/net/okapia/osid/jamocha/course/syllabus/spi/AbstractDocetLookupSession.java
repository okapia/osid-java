//
// AbstractDocetLookupSession.java
//
//    A starter implementation framework for providing a Docet
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Docet
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getDocets(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractDocetLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.syllabus.DocetLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Docet</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDocets() {
        return (true);
    }


    /**
     *  A complete view of the <code>Docet</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDocetView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Docet</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDocetView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include docets in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only docets whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveDocetView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All docets of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveDocetView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Docet</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Docet</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Docet</code> and
     *  retained for compatibility.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param  docetId <code>Id</code> of the
     *          <code>Docet</code>
     *  @return the docet
     *  @throws org.osid.NotFoundException <code>docetId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>docetId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Docet getDocet(org.osid.id.Id docetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.syllabus.DocetList docets = getDocets()) {
            while (docets.hasNext()) {
                org.osid.course.syllabus.Docet docet = docets.getNextDocet();
                if (docet.getId().equals(docetId)) {
                    return (docet);
                }
            }
        } 

        throw new org.osid.NotFoundException(docetId + " not found");
    }


    /**
     *  Gets a <code>DocetList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  docets specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Docets</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, docets are returned that are currently effective.
     *  In any effective mode, effective docets and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getDocets()</code>.
     *
     *  @param  docetIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Docet</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>docetIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByIds(org.osid.id.IdList docetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.syllabus.Docet> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = docetIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getDocet(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("docet " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.syllabus.docet.LinkedDocetList(ret));
    }


    /**
     *  Gets a <code>DocetList</code> corresponding to the given
     *  docet genus <code>Type</code> which does not include
     *  docets of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently effective.
     *  In any effective mode, effective docets and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getDocets()</code>.
     *
     *  @param  docetGenusType a docet genus type 
     *  @return the returned <code>Docet</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>docetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByGenusType(org.osid.type.Type docetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.DocetGenusFilterList(getDocets(), docetGenusType));
    }


    /**
     *  Gets a <code>DocetList</code> corresponding to the given
     *  docet genus <code>Type</code> and include any additional
     *  docets with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDocets()</code>.
     *
     *  @param  docetGenusType a docet genus type 
     *  @return the returned <code>Docet</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>docetGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByParentGenusType(org.osid.type.Type docetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDocetsByGenusType(docetGenusType));
    }


    /**
     *  Gets a <code>DocetList</code> containing the given
     *  docet record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDocets()</code>.
     *
     *  @param  docetRecordType a docet record type 
     *  @return the returned <code>Docet</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByRecordType(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.DocetRecordFilterList(getDocets(), docetRecordType));
    }


    /**
     *  Gets a <code>DocetList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *  
     *  In active mode, docets are returned that are currently
     *  active. In any status mode, active and inactive docets
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Docet</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.TemporalDocetFilterList(getDocets(), from, to));
    }
        

    /**
     *  Gets a list of docets corresponding to a module
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  moduleId the <code>Id</code> of the module
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>moduleId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.syllabus.DocetList getDocetsForModule(org.osid.id.Id moduleId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.DocetFilterList(new ModuleFilter(moduleId), getDocets()));
    }


    /**
     *  Gets a list of docets corresponding to a module
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  moduleId the <code>Id</code> of the module
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>moduleId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModuleOnDate(org.osid.id.Id moduleId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.TemporalDocetFilterList(getDocetsForModule(moduleId), from, to));
    }


    /**
     *  Gets a list of docets corresponding to a activity unit
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  activityUnitId the <code>Id</code> of the activity unit
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>activityUnitId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.syllabus.DocetList getDocetsForActivityUnit(org.osid.id.Id activityUnitId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.DocetFilterList(new ActivityUnitFilter(activityUnitId), getDocets()));
    }


    /**
     *  Gets a list of docets corresponding to a activity unit
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  activityUnitId the <code>Id</code> of the activity unit
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>activityUnitId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForActivityUnitOnDate(org.osid.id.Id activityUnitId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.TemporalDocetFilterList(getDocetsForActivityUnit(activityUnitId), from, to));
    }


    /**
     *  Gets a list of docets corresponding to module and activity
     *  unit <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known docets
     *  or an error results. Otherwise, the returned list may contain
     *  only those docets that are accessible through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  moduleId the <code>Id</code> of the module
     *  @param  activityUnitId the <code>Id</code> of the activity unit
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>moduleId</code>,
     *          <code>activityUnitId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModuleAndActivityUnit(org.osid.id.Id moduleId,
                                                                                org.osid.id.Id activityUnitId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.DocetFilterList(new ActivityUnitFilter(activityUnitId), getDocetsForModule(moduleId)));
    }


    /**
     *  Gets a list of docets corresponding to module and activity
     *  unit <code>Ids</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  activityUnitId the <code>Id</code> of the activity unit
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>moduleId</code>,
     *          <code>activityUnitId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModuleAndActivityUnitOnDate(org.osid.id.Id moduleId,
                                                                                      org.osid.id.Id activityUnitId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.TemporalDocetFilterList(getDocetsForModuleAndActivityUnit(moduleId, activityUnitId), from, to));
    }


    /**
     *  Gets all <code>Docets</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Docets</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.syllabus.DocetList getDocets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the docet list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of docets
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.syllabus.DocetList filterDocetsOnViews(org.osid.course.syllabus.DocetList list)
        throws org.osid.OperationFailedException {

        org.osid.course.syllabus.DocetList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.EffectiveDocetFilterList(ret);
        }

        return (ret);
    }

    public static class ModuleFilter
        implements net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.DocetFilter {
         
        private final org.osid.id.Id moduleId;
         
         
        /**
         *  Constructs a new <code>ModuleFilter</code>.
         *
         *  @param moduleId the module to filter
         *  @throws org.osid.NullArgumentException
         *          <code>moduleId</code> is <code>null</code>
         */
        
        public ModuleFilter(org.osid.id.Id moduleId) {
            nullarg(moduleId, "module Id");
            this.moduleId = moduleId;
            return;
        }

         
        /**
         *  Used by the DocetFilterList to filter the 
         *  docet list based on module.
         *
         *  @param docet the docet
         *  @return <code>true</code> to pass the docet,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.syllabus.Docet docet) {
            return (docet.getModuleId().equals(this.moduleId));
        }
    }


    public static class ActivityUnitFilter
        implements net.okapia.osid.jamocha.inline.filter.course.syllabus.docet.DocetFilter {
         
        private final org.osid.id.Id activityUnitId;
         
         
        /**
         *  Constructs a new <code>ActivityUnitFilter</code>.
         *
         *  @param activityUnitId the activityUnit to filter
         *  @throws org.osid.NullArgumentException
         *          <code>activityUnitId</code> is <code>null</code>
         */
        
        public ActivityUnitFilter(org.osid.id.Id activityUnitId) {
            nullarg(activityUnitId, "activityUnit Id");
            this.activityUnitId = activityUnitId;
            return;
        }

         
        /**
         *  Used by the DocetFilterList to filter the 
         *  docet list based on activityUnit.
         *
         *  @param docet the docet
         *  @return <code>true</code> to pass the docet,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.syllabus.Docet docet) {
            return (docet.getActivityUnitId().equals(this.activityUnitId));
        }
    }
}
