//
// InvariantMapSpeedZoneEnablerLookupSession
//
//    Implements a SpeedZoneEnabler lookup service backed by a fixed collection of
//    speedZoneEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules;


/**
 *  Implements a SpeedZoneEnabler lookup service backed by a fixed
 *  collection of speed zone enablers. The speed zone enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapSpeedZoneEnablerLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.rules.spi.AbstractMapSpeedZoneEnablerLookupSession
    implements org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapSpeedZoneEnablerLookupSession</code> with no
     *  speed zone enablers.
     *  
     *  @param map the map
     *  @throws org.osid.NullArgumnetException {@code map} is
     *          {@code null}
     */

    public InvariantMapSpeedZoneEnablerLookupSession(org.osid.mapping.Map map) {
        setMap(map);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSpeedZoneEnablerLookupSession</code> with a single
     *  speed zone enabler.
     *  
     *  @param map the map
     *  @param speedZoneEnabler a single speed zone enabler
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code speedZoneEnabler} is <code>null</code>
     */

      public InvariantMapSpeedZoneEnablerLookupSession(org.osid.mapping.Map map,
                                               org.osid.mapping.path.rules.SpeedZoneEnabler speedZoneEnabler) {
        this(map);
        putSpeedZoneEnabler(speedZoneEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSpeedZoneEnablerLookupSession</code> using an array
     *  of speed zone enablers.
     *  
     *  @param map the map
     *  @param speedZoneEnablers an array of speed zone enablers
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code speedZoneEnablers} is <code>null</code>
     */

      public InvariantMapSpeedZoneEnablerLookupSession(org.osid.mapping.Map map,
                                               org.osid.mapping.path.rules.SpeedZoneEnabler[] speedZoneEnablers) {
        this(map);
        putSpeedZoneEnablers(speedZoneEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSpeedZoneEnablerLookupSession</code> using a
     *  collection of speed zone enablers.
     *
     *  @param map the map
     *  @param speedZoneEnablers a collection of speed zone enablers
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code speedZoneEnablers} is <code>null</code>
     */

      public InvariantMapSpeedZoneEnablerLookupSession(org.osid.mapping.Map map,
                                               java.util.Collection<? extends org.osid.mapping.path.rules.SpeedZoneEnabler> speedZoneEnablers) {
        this(map);
        putSpeedZoneEnablers(speedZoneEnablers);
        return;
    }
}
