//
// AbstractQueryCatalogueLookupSession.java
//
//    An inline adapter that maps a CatalogueLookupSession to
//    a CatalogueQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CatalogueLookupSession to
 *  a CatalogueQuerySession.
 */

public abstract class AbstractQueryCatalogueLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractCatalogueLookupSession
    implements org.osid.offering.CatalogueLookupSession {


    private final org.osid.offering.CatalogueQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCatalogueLookupSession.
     *
     *  @param querySession the underlying catalogue query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCatalogueLookupSession(org.osid.offering.CatalogueQuerySession querySession) {
        nullarg(querySession, "catalogue query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Catalogue</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCatalogues() {
        return (this.session.canSearchCatalogues());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Catalogue</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Catalogue</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Catalogue</code> and
     *  retained for compatibility.
     *
     *  @param  catalogueId <code>Id</code> of the
     *          <code>Catalogue</code>
     *  @return the catalogue
     *  @throws org.osid.NotFoundException <code>catalogueId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>catalogueId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CatalogueQuery query = getQuery();
        query.matchId(catalogueId, true);
        org.osid.offering.CatalogueList catalogues = this.session.getCataloguesByQuery(query);
        if (catalogues.hasNext()) {
            return (catalogues.getNextCatalogue());
        } 
        
        throw new org.osid.NotFoundException(catalogueId + " not found");
    }


    /**
     *  Gets a <code>CatalogueList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  catalogues specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Catalogues</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  catalogueIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Catalogue</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByIds(org.osid.id.IdList catalogueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CatalogueQuery query = getQuery();

        try (org.osid.id.IdList ids = catalogueIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCataloguesByQuery(query));
    }


    /**
     *  Gets a <code>CatalogueList</code> corresponding to the given
     *  catalogue genus <code>Type</code> which does not include
     *  catalogues of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogueGenusType a catalogue genus type 
     *  @return the returned <code>Catalogue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByGenusType(org.osid.type.Type catalogueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CatalogueQuery query = getQuery();
        query.matchGenusType(catalogueGenusType, true);
        return (this.session.getCataloguesByQuery(query));
    }


    /**
     *  Gets a <code>CatalogueList</code> corresponding to the given
     *  catalogue genus <code>Type</code> and include any additional
     *  catalogues with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogueGenusType a catalogue genus type 
     *  @return the returned <code>Catalogue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByParentGenusType(org.osid.type.Type catalogueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CatalogueQuery query = getQuery();
        query.matchParentGenusType(catalogueGenusType, true);
        return (this.session.getCataloguesByQuery(query));
    }


    /**
     *  Gets a <code>CatalogueList</code> containing the given
     *  catalogue record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogueRecordType a catalogue record type 
     *  @return the returned <code>Catalogue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByRecordType(org.osid.type.Type catalogueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CatalogueQuery query = getQuery();
        query.matchRecordType(catalogueRecordType, true);
        return (this.session.getCataloguesByQuery(query));
    }


    /**
     *  Gets a <code>CatalogueList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known catalogues or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  catalogues that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Catalogue</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CatalogueQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getCataloguesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Catalogues</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Catalogues</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCatalogues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.offering.CatalogueQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCataloguesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.offering.CatalogueQuery getQuery() {
        org.osid.offering.CatalogueQuery query = this.session.getCatalogueQuery();
        
        return (query);
    }
}
