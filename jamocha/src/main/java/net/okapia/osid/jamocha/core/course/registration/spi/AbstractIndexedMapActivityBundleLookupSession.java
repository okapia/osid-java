//
// AbstractIndexedMapActivityBundleLookupSession.java
//
//    A simple framework for providing an ActivityBundle lookup service
//    backed by a fixed collection of activity bundles with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an ActivityBundle lookup service backed by a
 *  fixed collection of activity bundles. The activity bundles are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some activity bundles may be compatible
 *  with more types than are indicated through these activity bundle
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActivityBundles</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapActivityBundleLookupSession
    extends AbstractMapActivityBundleLookupSession
    implements org.osid.course.registration.ActivityBundleLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.registration.ActivityBundle> activityBundlesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.registration.ActivityBundle>());
    private final MultiMap<org.osid.type.Type, org.osid.course.registration.ActivityBundle> activityBundlesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.registration.ActivityBundle>());


    /**
     *  Makes an <code>ActivityBundle</code> available in this session.
     *
     *  @param  activityBundle an activity bundle
     *  @throws org.osid.NullArgumentException <code>activityBundle<code> is
     *          <code>null</code>
     */

    @Override
    protected void putActivityBundle(org.osid.course.registration.ActivityBundle activityBundle) {
        super.putActivityBundle(activityBundle);

        this.activityBundlesByGenus.put(activityBundle.getGenusType(), activityBundle);
        
        try (org.osid.type.TypeList types = activityBundle.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.activityBundlesByRecord.put(types.getNextType(), activityBundle);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an activity bundle from this session.
     *
     *  @param activityBundleId the <code>Id</code> of the activity bundle
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeActivityBundle(org.osid.id.Id activityBundleId) {
        org.osid.course.registration.ActivityBundle activityBundle;
        try {
            activityBundle = getActivityBundle(activityBundleId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.activityBundlesByGenus.remove(activityBundle.getGenusType());

        try (org.osid.type.TypeList types = activityBundle.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.activityBundlesByRecord.remove(types.getNextType(), activityBundle);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeActivityBundle(activityBundleId);
        return;
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  activity bundle genus <code>Type</code> which does not include
     *  activity bundles of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known activity bundles or an error results. Otherwise,
     *  the returned list may contain only those activity bundles that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  activityBundleGenusType an activity bundle genus type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByGenusType(org.osid.type.Type activityBundleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.registration.activitybundle.ArrayActivityBundleList(this.activityBundlesByGenus.get(activityBundleGenusType)));
    }


    /**
     *  Gets an <code>ActivityBundleList</code> containing the given
     *  activity bundle record <code>Type</code>. In plenary mode, the
     *  returned list contains all known activity bundles or an error
     *  results. Otherwise, the returned list may contain only those
     *  activity bundles that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  activityBundleRecordType an activity bundle record type 
     *  @return the returned <code>activityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByRecordType(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.registration.activitybundle.ArrayActivityBundleList(this.activityBundlesByRecord.get(activityBundleRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.activityBundlesByGenus.clear();
        this.activityBundlesByRecord.clear();

        super.close();

        return;
    }
}
