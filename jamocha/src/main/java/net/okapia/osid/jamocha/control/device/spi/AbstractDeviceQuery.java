//
// AbstractDeviceQuery.java
//
//     A template for making a Device Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.device.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for devices.
 */

public abstract class AbstractDeviceQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.control.DeviceQuery {

    private final java.util.Collection<org.osid.control.records.DeviceQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the system <code> Id </code> for this query. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given device query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a device implementing the requested record.
     *
     *  @param deviceRecordType a device record type
     *  @return the device query record
     *  @throws org.osid.NullArgumentException
     *          <code>deviceRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.DeviceQueryRecord getDeviceQueryRecord(org.osid.type.Type deviceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.DeviceQueryRecord record : this.records) {
            if (record.implementsRecordType(deviceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceRecordType + " is not supported");
    }


    /**
     *  Adds a record to this device query. 
     *
     *  @param deviceQueryRecord device query record
     *  @param deviceRecordType device record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDeviceQueryRecord(org.osid.control.records.DeviceQueryRecord deviceQueryRecord, 
                                          org.osid.type.Type deviceRecordType) {

        addRecordType(deviceRecordType);
        nullarg(deviceQueryRecord, "device query record");
        this.records.add(deviceQueryRecord);        
        return;
    }
}
