//
// AbstractReceipt.java
//
//     Defines a Receipt.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.receipt.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Receipt</code>.
 */

public abstract class AbstractReceipt
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.messaging.Receipt {

    private org.osid.messaging.Message message;
    private org.osid.calendaring.DateTime receivedTime;
    private org.osid.authentication.Agent receivingAgent;
    private org.osid.resource.Resource recipient;

    private final java.util.Collection<org.osid.messaging.records.ReceiptRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Message. </code> 
     *
     *  @return the message <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMessageId() {
        return (this.message.getId());
    }


    /**
     *  Gets the <code> Message. </code> 
     *
     *  @return the message 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.messaging.Message getMessage()
        throws org.osid.OperationFailedException {

        return (this.message);
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException
     *          <code>message</code> is <code>null</code>
     */

    protected void setMessage(org.osid.messaging.Message message) {
        nullarg(message, "message");
        this.message = message;
        return;
    }


    /**
     *  Gets the time the message was received. 
     *
     *  @return the time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReceivedTime() {
        return (this.receivedTime);
    }


    /**
     *  Sets the received time.
     *
     *  @param time a received time
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    protected void setReceivedTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "received time");
        this.receivedTime = time;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> that received 
     *  this message at this endpoint. 
     *
     *  @return the receiving agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReceivingAgentId() {
        return (this.receivingAgent.getId());
    }


    /**
     *  Gets the <code> Agent </code> that received this message at this 
     *  endpoint. 
     *
     *  @return the receiving agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getReceivingAgent()
        throws org.osid.OperationFailedException {

        return (this.receivingAgent);
    }


    /**
     *  Sets the receiving agent.
     *
     *  @param agent a receiving agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setReceivingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "receiving agent");
        this.receivingAgent = agent;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Resource </code> that 
     *  received this message at this endpoint. 
     *
     *  @return the recipient <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRecipientId() {
        return (this.recipient.getId());
    }


    /**
     *  Gets the <code> Resource </code> that received this message at this 
     *  endpoint. 
     *
     *  @return the recipient 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getRecipient()
        throws org.osid.OperationFailedException {

        return (this.recipient);
    }


    /**
     *  Sets the recipient.
     *
     *  @param recipient a recipient
     *  @throws org.osid.NullArgumentException
     *          <code>recipient</code> is <code>null</code>
     */

    protected void setRecipient(org.osid.resource.Resource recipient) {
        nullarg(recipient, "recipient");
        this.recipient = recipient;
        return;
    }


    /**
     *  Tests if this receipt supports the given record
     *  <code>Type</code>.
     *
     *  @param  receiptRecordType a receipt record type 
     *  @return <code>true</code> if the receiptRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type receiptRecordType) {
        for (org.osid.messaging.records.ReceiptRecord record : this.records) {
            if (record.implementsRecordType(receiptRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Receipt</code> record <code>Type</code>.
     *
     *  @param  receiptRecordType the receipt record type 
     *  @return the receipt record 
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(receiptRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.ReceiptRecord getReceiptRecord(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.ReceiptRecord record : this.records) {
            if (record.implementsRecordType(receiptRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(receiptRecordType + " is not supported");
    }


    /**
     *  Adds a record to this receipt. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param receiptRecord the receipt record
     *  @param receiptRecordType receipt record type
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecord</code> or
     *          <code>receiptRecordTypereceipt</code> is
     *          <code>null</code>
     */
            
    protected void addReceiptRecord(org.osid.messaging.records.ReceiptRecord receiptRecord, 
                                    org.osid.type.Type receiptRecordType) {

        nullarg(receiptRecord, "reciept record");
        addRecordType(receiptRecordType);
        this.records.add(receiptRecord);
        
        return;
    }
}
