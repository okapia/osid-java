//
// AbstractImmutableBallot.java
//
//     Wraps a mutable Ballot to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.ballot.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Ballot</code> to hide modifiers. This
 *  wrapper provides an immutized Ballot from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying ballot whose state changes are visible.
 */

public abstract class AbstractImmutableBallot
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.voting.Ballot {

    private final org.osid.voting.Ballot ballot;


    /**
     *  Constructs a new <code>AbstractImmutableBallot</code>.
     *
     *  @param ballot the ballot to immutablize
     *  @throws org.osid.NullArgumentException <code>ballot</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBallot(org.osid.voting.Ballot ballot) {
        super(ballot);
        this.ballot = ballot;
        return;
    }


    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        return (this.ballot.isEffective());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.ballot.getStartDate());
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.ballot.getEndDate());
    }


    /**
     *  Tests if races on this ballot can be modified before the election 
     *  ends. 
     *
     *  @return <code> true </code> if votes can be modified, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean canRevote() {
        return (this.ballot.canRevote());
    }


    /**
     *  Gets the ballot record corresponding to the given <code> Ballot 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> ballotRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(ballotRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  ballotRecordType the ballot record type 
     *  @return the ballot record 
     *  @throws org.osid.NullArgumentException <code> ballotRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(ballotRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.records.BallotRecord getBallotRecord(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException {

        return (this.ballot.getBallotRecord(ballotRecordType));
    }
}

