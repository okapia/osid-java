//
// AbstractMapCompositionLookupSession
//
//    A simple framework for providing a Composition lookup service
//    backed by a fixed collection of compositions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Composition lookup service backed by a
 *  fixed collection of compositions. The compositions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Compositions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCompositionLookupSession
    extends net.okapia.osid.jamocha.repository.spi.AbstractCompositionLookupSession
    implements org.osid.repository.CompositionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.repository.Composition> compositions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.repository.Composition>());


    /**
     *  Makes a <code>Composition</code> available in this session.
     *
     *  @param  composition a composition
     *  @throws org.osid.NullArgumentException <code>composition<code>
     *          is <code>null</code>
     */

    protected void putComposition(org.osid.repository.Composition composition) {
        this.compositions.put(composition.getId(), composition);
        return;
    }


    /**
     *  Makes an array of compositions available in this session.
     *
     *  @param  compositions an array of compositions
     *  @throws org.osid.NullArgumentException <code>compositions<code>
     *          is <code>null</code>
     */

    protected void putCompositions(org.osid.repository.Composition[] compositions) {
        putCompositions(java.util.Arrays.asList(compositions));
        return;
    }


    /**
     *  Makes a collection of compositions available in this session.
     *
     *  @param  compositions a collection of compositions
     *  @throws org.osid.NullArgumentException <code>compositions<code>
     *          is <code>null</code>
     */

    protected void putCompositions(java.util.Collection<? extends org.osid.repository.Composition> compositions) {
        for (org.osid.repository.Composition composition : compositions) {
            this.compositions.put(composition.getId(), composition);
        }

        return;
    }


    /**
     *  Removes a Composition from this session.
     *
     *  @param  compositionId the <code>Id</code> of the composition
     *  @throws org.osid.NullArgumentException <code>compositionId<code> is
     *          <code>null</code>
     */

    protected void removeComposition(org.osid.id.Id compositionId) {
        this.compositions.remove(compositionId);
        return;
    }


    /**
     *  Gets the <code>Composition</code> specified by its <code>Id</code>.
     *
     *  @param  compositionId <code>Id</code> of the <code>Composition</code>
     *  @return the composition
     *  @throws org.osid.NotFoundException <code>compositionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>compositionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Composition getComposition(org.osid.id.Id compositionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.repository.Composition composition = this.compositions.get(compositionId);
        if (composition == null) {
            throw new org.osid.NotFoundException("composition not found: " + compositionId);
        }

        return (composition);
    }


    /**
     *  Gets all <code>Compositions</code>. In plenary mode, the returned
     *  list contains all known compositions or an error
     *  results. Otherwise, the returned list may contain only those
     *  compositions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Compositions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.composition.ArrayCompositionList(this.compositions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.compositions.clear();
        super.close();
        return;
    }
}
