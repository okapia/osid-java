//
// InvariantMapPeriodLookupSession
//
//    Implements a Period lookup service backed by a fixed collection of
//    periods.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements a Period lookup service backed by a fixed
 *  collection of periods. The periods are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPeriodLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractMapPeriodLookupSession
    implements org.osid.billing.PeriodLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPeriodLookupSession</code> with no
     *  periods.
     *  
     *  @param business the business
     *  @throws org.osid.NullArgumnetException {@code business} is
     *          {@code null}
     */

    public InvariantMapPeriodLookupSession(org.osid.billing.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPeriodLookupSession</code> with a single
     *  period.
     *  
     *  @param business the business
     *  @param period a single period
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code period} is <code>null</code>
     */

      public InvariantMapPeriodLookupSession(org.osid.billing.Business business,
                                               org.osid.billing.Period period) {
        this(business);
        putPeriod(period);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPeriodLookupSession</code> using an array
     *  of periods.
     *  
     *  @param business the business
     *  @param periods an array of periods
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code periods} is <code>null</code>
     */

      public InvariantMapPeriodLookupSession(org.osid.billing.Business business,
                                               org.osid.billing.Period[] periods) {
        this(business);
        putPeriods(periods);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPeriodLookupSession</code> using a
     *  collection of periods.
     *
     *  @param business the business
     *  @param periods a collection of periods
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code periods} is <code>null</code>
     */

      public InvariantMapPeriodLookupSession(org.osid.billing.Business business,
                                               java.util.Collection<? extends org.osid.billing.Period> periods) {
        this(business);
        putPeriods(periods);
        return;
    }
}
