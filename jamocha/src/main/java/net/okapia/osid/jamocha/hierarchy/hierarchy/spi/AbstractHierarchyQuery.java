//
// AbstractHierarchyQuery.java
//
//     A template for making a Hierarchy Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hierarchy.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for hierarchies.
 */

public abstract class AbstractHierarchyQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.hierarchy.HierarchyQuery {

    private final java.util.Collection<org.osid.hierarchy.records.HierarchyQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches an <code> Id </code> of a node in this hierarchy. Multiple 
     *  nodes can be added to this query which behave as a boolean <code> AND. 
     *  </code> 
     *
     *  @param  id <code> Id </code> to match 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchNodeId(org.osid.id.Id id, boolean match) {
        return;
    }


    /**
     *  Matches hierarchies with any node. 
     *
     *  @param  match <code> true </code> to match hierarchies with any nodes, 
     *          <code> false </code> to match hierarchies with no nodes 
     */

    @OSID @Override
    public void matchAnyNodeId(boolean match) {
        return;
    }


    /**
     *  Clears the node <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearNodeIdTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given hierarchy query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a hierarchy implementing the requested record.
     *
     *  @param hierarchyRecordType a hierarchy record type
     *  @return the hierarchy query record
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(hierarchyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hierarchy.records.HierarchyQueryRecord getHierarchyQueryRecord(org.osid.type.Type hierarchyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hierarchy.records.HierarchyQueryRecord record : this.records) {
            if (record.implementsRecordType(hierarchyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(hierarchyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this hierarchy query. 
     *
     *  @param hierarchyQueryRecord hierarchy query record
     *  @param hierarchyRecordType hierarchy record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addHierarchyQueryRecord(org.osid.hierarchy.records.HierarchyQueryRecord hierarchyQueryRecord, 
                                          org.osid.type.Type hierarchyRecordType) {

        addRecordType(hierarchyRecordType);
        nullarg(hierarchyQueryRecord, "hierarchy query record");
        this.records.add(hierarchyQueryRecord);        
        return;
    }
}
