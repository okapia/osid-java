//
// AbstractOsidIdentifiableQueryInspector.java
//
//     Defines an OsidIdentifiableQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidQueryInspector to extend. 
 */

public abstract class AbstractOsidIdentifiableQueryInspector
    extends AbstractOsidQueryInspector
    implements org.osid.OsidIdentifiableQueryInspector {

    private final java.util.Collection<org.osid.search.terms.IdTerm> idTerms = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code>Id</code> query terms.
     *
     *  @return the <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIdTerms() {
        return (this.idTerms.toArray(new org.osid.search.terms.IdTerm[this.idTerms.size()]));
    }

    
    /**
     *  Adds an Id term.
     *
     *  @param term an Id term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "id term");
        this.idTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of Id terms.
     *
     *  @param terms a collection of id terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "id terms");
        this.idTerms.addAll(terms);
        return;
    }


    /**
     *  Adds an Id term.
     *
     *  @param id the Id
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>id</code>
     *          is <code>null</code>
     */

    protected void addIdTerm(org.osid.id.Id id, boolean match) {
        this.idTerms.add(new net.okapia.osid.primordium.terms.IdTerm(id, match));
        return;
    }
}
