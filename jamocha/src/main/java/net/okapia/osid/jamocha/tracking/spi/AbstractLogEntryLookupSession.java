//
// AbstractLogEntryLookupSession.java
//
//    A starter implementation framework for providing a LogEntry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a LogEntry lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getLogEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractLogEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.tracking.LogEntryLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.tracking.FrontOffice frontOffice = new net.okapia.osid.jamocha.nil.tracking.frontoffice.UnknownFrontOffice();
    

    /**
     *  Gets the <code>FrontOffice/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.frontOffice.getId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.frontOffice);
    }


    /**
     *  Sets the <code>FrontOffice</code>.
     *
     *  @param  frontOffice the front office for this session
     *  @throws org.osid.NullArgumentException <code>frontOffice</code>
     *          is <code>null</code>
     */

    protected void setFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        nullarg(frontOffice, "front office");
        this.frontOffice = frontOffice;
        return;
    }


    /**
     *  Tests if this user can perform <code>LogEntry</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupLogEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>LogEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLogEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>LogEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLogEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include log entries in front offices which are
     *  children of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>LogEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>LogEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>LogEntry</code> and
     *  retained for compatibility.
     *
     *  @param logEntryId <code>Id</code> of the <code>LogEntry</code>
     *  @return the log entry
     *  @throws org.osid.NotFoundException <code>logEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>logEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntry getLogEntry(org.osid.id.Id logEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.tracking.LogEntryList logEntries = getLogEntries()) {
            while (logEntries.hasNext()) {
                org.osid.tracking.LogEntry logEntry = logEntries.getNextLogEntry();
                if (logEntry.getId().equals(logEntryId)) {
                    return (logEntry);
                }
            }
        } 

        throw new org.osid.NotFoundException(logEntryId + " not found");
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  logEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>LogEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getLogEntries()</code>.
     *
     *  @param  logEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByIds(org.osid.id.IdList logEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.tracking.LogEntry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = logEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getLogEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("log entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.tracking.logentry.LinkedLogEntryList(ret));
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  log entry genus <code>Type</code> which does not include log
     *  entries of types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known log
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getLogEntries()</code>.
     *
     *  @param logEntryGenusType a logEntry genus type
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.logentry.LogEntryGenusFilterList(getLogEntries(), logEntryGenusType));
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  log entry genus <code>Type</code> and include any additional
     *  log entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known log
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getLogEntries()</code>.
     *
     *  @param  logEntryGenusType a logEntry genus type 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByParentGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getLogEntriesByGenusType(logEntryGenusType));
    }


    /**
     *  Gets a <code>LogEntryList</code> containing the given log
     *  entry record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known log
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getLogEntries()</code>.
     *
     *  @param  logEntryRecordType a logEntry record type 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByRecordType(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.logentry.LogEntryRecordFilterList(getLogEntries(), logEntryRecordType));
    }


    /**
     *  Gets a list of log entries logged in the date range. In
     *  plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those log entries that are accessible
     *  through this session.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>LogEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>from</code> or <code>
     *          to</code> is <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.logentry.LogEntryFilterList(new DateFilter(from, to), getLogEntries()));
    }


    /**
     *  Gets a <code>LogEntryList</code> in the given
     *  <code>Queue.</code> In plenary mode, the returned list
     *  contains all of the log entries, or an error results if a log
     *  entry connected to the issue is not found or
     *  inaccessible. Otherwise, inaccessible <code>LogEntries</code>
     *  may be omitted from the list.
     *
     *  @param queueId a queue <code>Id</code>
     *  @return the returned <code>LogEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>issueId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesForQueue(org.osid.id.Id queueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.tracking.LogEntry> ret = new java.util.ArrayList<>();

        try (org.osid.tracking.LogEntryList entries = getLogEntries()) {
            while (entries.hasNext()) {
                org.osid.tracking.LogEntry entry = entries.getNextLogEntry();
                org.osid.tracking.Issue issue = entry.getIssue();
                if (issue.getQueueId().equals(queueId)) {
                    ret.add(entry);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.tracking.logentry.LinkedLogEntryList(ret));
    }


    /**
     *  Gets a list of log entries logged in the date range in the
     *  given queue. In plenary mode, the returned list contains all
     *  known availabilities or an error results. Otherwise, the
     *  returned list may contain only those log entries that are
     *  accessible through this session.
     *
     *  @param  queueId a queue <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>LogEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>queueId, from</code> or 
     *          <code>to</code> is <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByDateForQueue(org.osid.id.Id queueId, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.logentry.LogEntryFilterList(new DateFilter(from, to), getLogEntriesForQueue(queueId)));
    }

    
    /**
     *  Gets a <code>LogEntryList</code> connected to the given <code>
     *  Issue.</code> In plenary mode, the returned list contains all
     *  of the log entries, or an error results if a log entry
     *  connected to the issue is not found or
     *  inaccessible. Otherwise, inaccessible <code> LogEntries</code>
     *  may be omitted from the list.
     *
     *  @param issueId an issue <code>Id</code>
     *  @return the returned <code>LogEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>issueId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.logentry.LogEntryFilterList(new IssueFilter(issueId), getLogEntries()));
    }


    /**
     *  Gets a list of log entries logged in the date range in the
     *  given issue. In plenary mode, the returned list contains all
     *  known availabilities or an error results. Otherwise, the
     *  returned list may contain only those log entries that are
     *  accessible through this session.
     *
     *  @param  issueId an issue <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>LogEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>issueId, from</code> or 
     *          <code>to</code> is <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByDateForIssue(org.osid.id.Id issueId, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.logentry.LogEntryFilterList(new DateFilter(from, to), getLogEntriesForIssue(issueId)));
    }

    
    /**
     *  Gets all <code>LogEntries</code>.
     *
     *  In plenary mode, the returned list contains all known log
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>LogEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.tracking.LogEntryList getLogEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the log entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of log entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.tracking.LogEntryList filterLogEntriesOnViews(org.osid.tracking.LogEntryList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
    
    
    public static class IssueFilter
        implements net.okapia.osid.jamocha.inline.filter.tracking.logentry.LogEntryFilter {
         
        private final org.osid.id.Id issueId;
         
         
        /**
         *  Constructs a new <code>IssueFilter</code>.
         *
         *  @param issueId the source node to filter
         *  @throws org.osid.NullArgumentException
         *          <code>issueId</code> is <code>null</code>
         */
        
        public IssueFilter(org.osid.id.Id issueId) {
            nullarg(issueId, "issue Id");
            this.issueId = issueId;
            return;
        }

         
        /**
         *  Used by the LogEntryFilterList to filter the 
         *  logEntry list based on source node.
         *
         *  @param logEntry the log entry
         *  @return <code>true</code> to pass the log entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.tracking.LogEntry logEntry) {
            return (logEntry.getIssueId().equals(this.issueId));
        }
    }    


    public static class DateFilter
        implements net.okapia.osid.jamocha.inline.filter.tracking.logentry.LogEntryFilter {
         
        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;
         
         
        /**
         *  Constructs a new <code>DateFilter</code>.
         *
         *  @param from the start date
         *  @param to the end date
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */
        
        public DateFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to = to;

            return;
        }

         
        /**
         *  Used by the LogEntryFilterList to filter the tracking
         *  event list based on date.
         *
         *  @param logEntry the log entry
         *  @return <code>true</code> to pass the log entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.tracking.LogEntry logEntry) {
            if (logEntry.getDate().isLess(this.from)) {
                return (false);
            }

            if (logEntry.getDate().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }
}
