//
// AbstractFederatingCommentLookupSession.java
//
//     An abstract federating adapter for a CommentLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CommentLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCommentLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.commenting.CommentLookupSession>
    implements org.osid.commenting.CommentLookupSession {

    private boolean parallel = false;
    private org.osid.commenting.Book book = new net.okapia.osid.jamocha.nil.commenting.book.UnknownBook();


    /**
     *  Constructs a new <code>AbstractFederatingCommentLookupSession</code>.
     */

    protected AbstractFederatingCommentLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.commenting.CommentLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Book/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Book Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBookId() {
        return (this.book.getId());
    }


    /**
     *  Gets the <code>Book</code> associated with this 
     *  session.
     *
     *  @return the <code>Book</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.book);
    }


    /**
     *  Sets the <code>Book</code>.
     *
     *  @param  book the book for this session
     *  @throws org.osid.NullArgumentException <code>book</code>
     *          is <code>null</code>
     */

    protected void setBook(org.osid.commenting.Book book) {
        nullarg(book, "book");
        this.book = book;
        return;
    }


    /**
     *  Tests if this user can perform <code>Comment</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupComments() {
        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            if (session.canLookupComments()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Comment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommentView() {
        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            session.useComparativeCommentView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Comment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommentView() {
        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            session.usePlenaryCommentView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include comments in books which are children
     *  of this book in the book hierarchy.
     */

    @OSID @Override
    public void useFederatedBookView() {
        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            session.useFederatedBookView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this book only.
     */

    @OSID @Override
    public void useIsolatedBookView() {
        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            session.useIsolatedBookView();
        }

        return;
    }


    /**
     *  Only comments whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveCommentView() {
        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            session.useEffectiveCommentView();
        }

        return;
    }


    /**
     *  All comments of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCommentView() {
        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            session.useAnyEffectiveCommentView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Comment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Comment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Comment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentId <code>Id</code> of the
     *          <code>Comment</code>
     *  @return the comment
     *  @throws org.osid.NotFoundException <code>commentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Comment getComment(org.osid.id.Id commentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            try {
                return (session.getComment(commentId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(commentId + " not found");
    }


    /**
     *  Gets a <code>CommentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  comments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Comments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, comments are returned that are currently effective.
     *  In any effective mode, effective comments and those currently expired
     *  are returned.
     *
     *  @param  commentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>commentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByIds(org.osid.id.IdList commentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.commenting.comment.MutableCommentList ret = new net.okapia.osid.jamocha.commenting.comment.MutableCommentList();

        try (org.osid.id.IdList ids = commentIds) {
            while (ids.hasNext()) {
                ret.addComment(getComment(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CommentList</code> corresponding to the given
     *  comment genus <code>Type</code> which does not include
     *  comments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently effective.
     *  In any effective mode, effective comments and those currently expired
     *  are returned.
     *
     *  @param  commentGenusType a comment genus type 
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusType(org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByGenusType(commentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CommentList</code> corresponding to the given
     *  comment genus <code>Type</code> and include any additional
     *  comments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentGenusType a comment genus type 
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByParentGenusType(org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByParentGenusType(commentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CommentList</code> containing the given
     *  comment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentRecordType a comment record type 
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByRecordType(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByRecordType(commentRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CommentList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *  
     *  In active mode, comments are returned that are currently
     *  active. In any status mode, active and inactive comments
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Comment</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.commenting.CommentList getCommentsOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>CommentList</code> of the given genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In active mode, comments are returned that are currently
     *  active. In any status mode, active and inactive comments
     *  are returned.
     *
     *  @param commentGenusType a comment genustype
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>commentGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeOnDate(org.osid.type.Type commentGenusType,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByGenusTypeOnDate(commentGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of comments corresponding to a commentor
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the commentor
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.commenting.CommentList getCommentsForCommentor(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsForCommentor(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of comments corresponding to a commentor
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the commentor
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentorOnDate(org.osid.id.Id resourceId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsForCommentorOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of comments of the given genus type corresponding to a
     *  resource <code> Id. </code>
     *
     *  In plenary mode, the returned list contains all known comments or an
     *  error results. Otherwise, the returned list may contain only those
     *  comments that are accessible through this session.
     *
     *  In effective mode, comments are returned that are currently effective.
     *  In any effective mode, effective comments and those currently expired
     *  are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource
     *  @param  commentGenusType the comment genus type
     *  @return the returned <code> CommentList </code>
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or
     *          <code> commentGenusType </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentor(org.osid.id.Id resourceId,
                                                                              org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByGenusTypeForCommentor(resourceId, commentGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all comments of the given genus type corresponding to a
     *  resource <code> Id </code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known comments or an
     *  error results. Otherwise, the returned list may contain only those
     *  comments that are accessible through this session.
     *
     *  In effective mode, comments are returned that are currently effective.
     *  In any effective mode, effective comments and those currently expired
     *  are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource
     *  @param  commentGenusType the comment genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code> CommentList </code>
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          commentGenusType, from </code> or <code> to </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentorOnDate(org.osid.id.Id resourceId,
                                                                                    org.osid.type.Type commentGenusType,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByGenusTypeForCommentorOnDate(resourceId, commentGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of comments corresponding to a reference
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.commenting.CommentList getCommentsForReference(org.osid.id.Id referenceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsForReference(referenceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of comments corresponding to a reference
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForReferenceOnDate(org.osid.id.Id referenceId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsForReferenceOnDate(referenceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of comments of the given genus type corresponding
     *  to a reference <code> Id. </code>
     *
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  referenceId the <code> Id </code> of the reference
     *  @param  commentGenusType the comment genus type
     *  @return the returned <code> CommentList </code>
     *  @throws org.osid.NullArgumentException <code> referenceId </code> or
     *          <code> commentGenusType </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForReference(org.osid.id.Id referenceId,
                                                                              org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByGenusTypeForReference(referenceId, commentGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all comments of the given genus type corresponding to a
     *  reference <code> Id </code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known comments or an
     *  error results. Otherwise, the returned list may contain only those
     *  comments that are accessible through this session.
     *
     *  In effective mode, comments are returned that are currently effective.
     *  In any effective mode, effective comments and those currently expired
     *  are returned.
     *
     *  @param  referenceId a reference <code> Id </code>
     *  @param  commentGenusType the comment genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code> CommentList </code>
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> referenceId,
     *          commentGenusType, from </code> or <code> to </code> is <code>
     *  null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForReferenceOnDate(org.osid.id.Id referenceId,
                                                                                    org.osid.type.Type commentGenusType,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByGenusTypeForReferenceOnDate(referenceId, commentGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of comments corresponding to commentor and reference
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the commentor
     *  @param  referenceId the <code>Id</code> of the reference
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>referenceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentorAndReference(org.osid.id.Id resourceId,
                                                                               org.osid.id.Id referenceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsForCommentorAndReference(resourceId, referenceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of comments corresponding to commentor and reference
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>referenceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentorAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                     org.osid.id.Id referenceId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsForCommentorAndReferenceOnDate(resourceId, referenceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of comments of the given genus type corresponding to a
     *  resource and reference <code> Id. </code>
     *
     *  In plenary mode, the returned list contains all known comments or an
     *  error results. Otherwise, the returned list may contain only those
     *  comments that are accessible through this session.
     *
     *  In effective mode, comments are returned that are currently effective.
     *  In any effective mode, effective comments and those currently expired
     *  are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource
     *  @param  referenceId the <code> Id </code> of the reference
     *  @param  commentGenusType the comment genus type
     *  @return the returned <code> CommentList </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, referenceId
     *          </code> or <code> commentGenusType </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentorAndReference(org.osid.id.Id resourceId,
                                                                                          org.osid.id.Id referenceId,
                                                                                          org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByGenusTypeForCommentorAndReference(resourceId, referenceId, commentGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all comments corresponding to a resource and reference
     *  <code> Id </code> and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known comments or an
     *  error results. Otherwise, the returned list may contain only those
     *  comments that are accessible through this session.
     *
     *  In effective mode, comments are returned that are currently effective.
     *  In any effective mode, effective comments and those currently expired
     *  are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource
     *  @param  referenceId a reference <code> Id </code>
     *  @param  commentGenusType the comment genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code> CommentList </code>
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, referenceId,
     *          commentGenusType, from </code> or <code> to </code> is <code>
     *  null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentorAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                                org.osid.id.Id  referenceId,
                                                                                                org.osid.type.Type commentGenusType,
                                                                                                org.osid.calendaring.DateTime from,
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getCommentsByGenusTypeForCommentorAndReferenceOnDate(resourceId, referenceId, commentGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Comments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Comments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getComments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList ret = getCommentList();

        for (org.osid.commenting.CommentLookupSession session : getSessions()) {
            ret.addCommentList(session.getComments());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.commenting.comment.FederatingCommentList getCommentList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.commenting.comment.ParallelCommentList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.commenting.comment.CompositeCommentList());
        }
    }
}
