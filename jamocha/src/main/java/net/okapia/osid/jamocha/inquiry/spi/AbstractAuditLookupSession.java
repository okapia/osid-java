//
// AbstractAuditLookupSession.java
//
//    A starter implementation framework for providing an Audit
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Audit
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAudits(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAuditLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inquiry.AuditLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.inquiry.Inquest inquest = new net.okapia.osid.jamocha.nil.inquiry.inquest.UnknownInquest();
    

    /**
     *  Gets the <code>Inquest/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Inquest Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.inquest.getId());
    }


    /**
     *  Gets the <code>Inquest</code> associated with this 
     *  session.
     *
     *  @return the <code>Inquest</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.inquest);
    }


    /**
     *  Sets the <code>Inquest</code>.
     *
     *  @param  inquest the inquest for this session
     *  @throws org.osid.NullArgumentException <code>inquest</code>
     *          is <code>null</code>
     */

    protected void setInquest(org.osid.inquiry.Inquest inquest) {
        nullarg(inquest, "inquest");
        this.inquest = inquest;
        return;
    }


    /**
     *  Tests if this user can perform <code>Audit</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAudits() {
        return (true);
    }


    /**
     *  A complete view of the <code>Audit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuditView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Audit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuditView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include audits in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active audits are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuditView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive audits are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuditView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Audit</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Audit</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Audit</code> and
     *  retained for compatibility.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @param  auditId <code>Id</code> of the
     *          <code>Audit</code>
     *  @return the audit
     *  @throws org.osid.NotFoundException <code>auditId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auditId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Audit getAudit(org.osid.id.Id auditId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inquiry.AuditList audits = getAudits()) {
            while (audits.hasNext()) {
                org.osid.inquiry.Audit audit = audits.getNextAudit();
                if (audit.getId().equals(auditId)) {
                    return (audit);
                }
            }
        } 

        throw new org.osid.NotFoundException(auditId + " not found");
    }


    /**
     *  Gets an <code>AuditList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  audits specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Audits</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAudits()</code>.
     *
     *  @param  auditIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Audit</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auditIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByIds(org.osid.id.IdList auditIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inquiry.Audit> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = auditIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAudit(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("audit " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inquiry.audit.LinkedAuditList(ret));
    }


    /**
     *  Gets an <code>AuditList</code> corresponding to the given
     *  audit genus <code>Type</code> which does not include
     *  audits of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  audits or an error results. Otherwise, the returned list
     *  may contain only those audits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAudits()</code>.
     *
     *  @param  auditGenusType an audit genus type 
     *  @return the returned <code>Audit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auditGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByGenusType(org.osid.type.Type auditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.audit.AuditGenusFilterList(getAudits(), auditGenusType));
    }


    /**
     *  Gets an <code>AuditList</code> corresponding to the given
     *  audit genus <code>Type</code> and include any additional
     *  audits with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  audits or an error results. Otherwise, the returned list
     *  may contain only those audits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAudits()</code>.
     *
     *  @param  auditGenusType an audit genus type 
     *  @return the returned <code>Audit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auditGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByParentGenusType(org.osid.type.Type auditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAuditsByGenusType(auditGenusType));
    }


    /**
     *  Gets an <code>AuditList</code> containing the given
     *  audit record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  audits or an error results. Otherwise, the returned list
     *  may contain only those audits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAudits()</code>.
     *
     *  @param  auditRecordType an audit record type 
     *  @return the returned <code>Audit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByRecordType(org.osid.type.Type auditRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.audit.AuditRecordFilterList(getAudits(), auditRecordType));
    }


    /**
     *  Gets an <code>AuditList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known audits or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  audits that are accessible through this session. 
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Audit</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.inquiry.audit.AuditProviderFilterList(getAudits(), resourceId));
    }


    /**
     *  Gets all <code>Audits</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  audits or an error results. Otherwise, the returned list
     *  may contain only those audits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @return a list of <code>Audits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.inquiry.AuditList getAudits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the audit list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of audits
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.inquiry.AuditList filterAuditsOnViews(org.osid.inquiry.AuditList list)
        throws org.osid.OperationFailedException {

        org.osid.inquiry.AuditList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.inquiry.audit.ActiveAuditFilterList(ret);
        }

        return (ret);
    }
}
