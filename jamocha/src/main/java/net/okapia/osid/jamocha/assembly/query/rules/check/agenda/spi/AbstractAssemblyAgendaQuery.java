//
// AbstractAssemblyAgendaQuery.java
//
//     An AgendaQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.rules.check.agenda.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AgendaQuery that stores terms.
 */

public abstract class AbstractAssemblyAgendaQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.rules.check.AgendaQuery,
               org.osid.rules.check.AgendaQueryInspector,
               org.osid.rules.check.AgendaSearchOrder {

    private final java.util.Collection<org.osid.rules.check.records.AgendaQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.rules.check.records.AgendaQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.rules.check.records.AgendaSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAgendaQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAgendaQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the instruction <code> Id </code> for this query. 
     *
     *  @param  qualifierId the instruction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> instructionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInstructionId(org.osid.id.Id qualifierId, boolean match) {
        getAssembler().addIdTerm(getInstructionIdColumn(), qualifierId, match);
        return;
    }


    /**
     *  Clears the instruction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInstructionIdTerms() {
        getAssembler().clearTerms(getInstructionIdColumn());
        return;
    }


    /**
     *  Gets the instruction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstructionIdTerms() {
        return (getAssembler().getIdTerms(getInstructionIdColumn()));
    }


    /**
     *  Gets the InstructionId column name.
     *
     * @return the column name
     */

    protected String getInstructionIdColumn() {
        return ("instruction_id");
    }


    /**
     *  Tests if an <code> InstructionQuery </code> is available. 
     *
     *  @return <code> true </code> if an instruction query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an instruction. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the instruction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQuery getInstructionQuery() {
        throw new org.osid.UnimplementedException("supportsInstructionQuery() is false");
    }


    /**
     *  Matches agendas that have any instruction. 
     *
     *  @param  match <code> true </code> to match agendas with any 
     *          instruction, <code> false </code> to match agendas with no 
     *          instruction 
     */

    @OSID @Override
    public void matchAnyInstruction(boolean match) {
        getAssembler().addIdWildcardTerm(getInstructionColumn(), match);
        return;
    }


    /**
     *  Clears the instruction query terms. 
     */

    @OSID @Override
    public void clearInstructionTerms() {
        getAssembler().clearTerms(getInstructionColumn());
        return;
    }


    /**
     *  Gets the instruction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQueryInspector[] getInstructionTerms() {
        return (new org.osid.rules.check.InstructionQueryInspector[0]);
    }


    /**
     *  Gets the Instruction column name.
     *
     * @return the column name
     */

    protected String getInstructionColumn() {
        return ("instruction");
    }


    /**
     *  Sets the engine <code> Id </code> for this query to match agendas 
     *  assigned to foundries. 
     *
     *  @param  engineId the engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEngineId(org.osid.id.Id engineId, boolean match) {
        getAssembler().addIdTerm(getEngineIdColumn(), engineId, match);
        return;
    }


    /**
     *  Clears the engine <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEngineIdTerms() {
        getAssembler().clearTerms(getEngineIdColumn());
        return;
    }


    /**
     *  Gets the engine <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEngineIdTerms() {
        return (getAssembler().getIdTerms(getEngineIdColumn()));
    }


    /**
     *  Gets the EngineId column name.
     *
     * @return the column name
     */

    protected String getEngineIdColumn() {
        return ("engine_id");
    }


    /**
     *  Tests if a <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuery getEngineQuery() {
        throw new org.osid.UnimplementedException("supportsEngineQuery() is false");
    }


    /**
     *  Clears the engine query terms. 
     */

    @OSID @Override
    public void clearEngineTerms() {
        getAssembler().clearTerms(getEngineColumn());
        return;
    }


    /**
     *  Gets the engine query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.EngineQueryInspector[] getEngineTerms() {
        return (new org.osid.rules.EngineQueryInspector[0]);
    }


    /**
     *  Gets the Engine column name.
     *
     * @return the column name
     */

    protected String getEngineColumn() {
        return ("engine");
    }


    /**
     *  Tests if this agenda supports the given record
     *  <code>Type</code>.
     *
     *  @param  agendaRecordType an agenda record type 
     *  @return <code>true</code> if the agendaRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type agendaRecordType) {
        for (org.osid.rules.check.records.AgendaQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(agendaRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  agendaRecordType the agenda record type 
     *  @return the agenda query record 
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agendaRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.AgendaQueryRecord getAgendaQueryRecord(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.AgendaQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(agendaRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agendaRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  agendaRecordType the agenda record type 
     *  @return the agenda query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agendaRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.AgendaQueryInspectorRecord getAgendaQueryInspectorRecord(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.AgendaQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(agendaRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agendaRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param agendaRecordType the agenda record type
     *  @return the agenda search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agendaRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.AgendaSearchOrderRecord getAgendaSearchOrderRecord(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.AgendaSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(agendaRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agendaRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this agenda. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param agendaQueryRecord the agenda query record
     *  @param agendaQueryInspectorRecord the agenda query inspector
     *         record
     *  @param agendaSearchOrderRecord the agenda search order record
     *  @param agendaRecordType agenda record type
     *  @throws org.osid.NullArgumentException
     *          <code>agendaQueryRecord</code>,
     *          <code>agendaQueryInspectorRecord</code>,
     *          <code>agendaSearchOrderRecord</code> or
     *          <code>agendaRecordTypeagenda</code> is
     *          <code>null</code>
     */
            
    protected void addAgendaRecords(org.osid.rules.check.records.AgendaQueryRecord agendaQueryRecord, 
                                      org.osid.rules.check.records.AgendaQueryInspectorRecord agendaQueryInspectorRecord, 
                                      org.osid.rules.check.records.AgendaSearchOrderRecord agendaSearchOrderRecord, 
                                      org.osid.type.Type agendaRecordType) {

        addRecordType(agendaRecordType);

        nullarg(agendaQueryRecord, "agenda query record");
        nullarg(agendaQueryInspectorRecord, "agenda query inspector record");
        nullarg(agendaSearchOrderRecord, "agenda search odrer record");

        this.queryRecords.add(agendaQueryRecord);
        this.queryInspectorRecords.add(agendaQueryInspectorRecord);
        this.searchOrderRecords.add(agendaSearchOrderRecord);
        
        return;
    }
}
