//
// AbstractOfferingConstrainer.java
//
//     Defines an OfferingConstrainer.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>OfferingConstrainer</code>.
 */

public abstract class AbstractOfferingConstrainer
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainer
    implements org.osid.offering.rules.OfferingConstrainer {

    boolean canOverrideDescription;
    boolean canOverrideTitle;
    boolean canOverrideCode;
    boolean canOverrideTimePeriods;
    boolean canConstrainTimePeriods;
    boolean canOverrideResultOptions;
    boolean canConstrainResultOptions;
    boolean canOverrideSponsors;
    boolean canConstrainSponsors;

    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if the description can be overridden at offering. 
     *
     *  @return <code> true </code> if the description can be overridden, 
     *          <code> false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideDescription() {
        return (this.canOverrideDescription);
    }


    /**
     *  Sets the can override description.
     *
     *  @param canOverrideDescription <code> true </code> if the
     *          description can be overridden, <code> false </code> if
     *          constrained
     */

    protected void setCanOverrideDescription(boolean canOverrideDescription) {
        this.canOverrideDescription = canOverrideDescription;
        return;
    }


    /**
     *  Tests if the title can be overridden at offering. 
     *
     *  @return <code> true </code> if the title can be overridden, <code> 
     *          false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideTitle() {
        return (this.canOverrideTitle);
    }


    /**
     *  Sets the can override title.
     *
     *  @param canOverrideTitle <code> true </code> if the title can
     *          be overridden, <code> false </code> if constrained
     */

    protected void setCanOverrideTitle(boolean canOverrideTitle) {
        this.canOverrideTitle = canOverrideTitle;
        return;
    }


    /**
     *  Tests if the code can be overridden at offering. 
     *
     *  @return <code> true </code> if the code can be overridden, <code> 
     *          false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideCode() {
        return (this.canOverrideCode);
    }


    /**
     *  Sets the can override code.
     *
     *  @param canOverrideCode <code> true </code> if the code can be
     *          overridden, <code> false </code> if constrained
     */

    protected void setCanOverrideCode(boolean canOverrideCode) {
        this.canOverrideCode = canOverrideCode;
        return;
    }


    /**
     *  Tests if the offerings can be made outside the specified time period 
     *  cycles. 
     *
     *  @return <code> true </code> if the time period cycles can be 
     *          overridden, <code> false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideTimePeriods() {
        return (this.canOverrideTimePeriods);
    }


    /**
     *  Sets the can override time periods.
     *
     *  @param canOverrideTimePeriods <code> true </code> if the time
     *          period cycles can be overridden, <code> false </code>
     *          if constrained
     */

    protected void setCanOverrideTimePeriods(boolean canOverrideTimePeriods) {
        this.canOverrideTimePeriods = canOverrideTimePeriods;
        return;
    }


    /**
     *  Tests if offerings in all specified time periods are optional.
     *
     *  @return <code> true </code> if the time period cycles can be
     *          optional, <code> false </code> if constrained
     */

    @OSID @Override
    public boolean canConstrainTimePeriods() {
        return (this.canConstrainTimePeriods);
    }


    /**
     *  Sets the can constrain time periods.
     *
     *  @param canConstrainTimePeriods <code> true </code> if the time
     *          period cycles can be optional, <code> false </code> if
     *          constrained
     */

    protected void setCanConstrainTimePeriods(boolean canConstrainTimePeriods) {
        this.canConstrainTimePeriods = canConstrainTimePeriods;
        return;
    }


    /**
     *  Tests if the result options can be overridden at offering. 
     *
     *  @return <code> true </code> if the result options can be
     *          overridden, <code> false </code> if constrained
     */

    @OSID @Override
    public boolean canOverrideResultOptions() {
        return (this.canOverrideResultOptions);
    }


    /**
     *  Sets the can override result options.
     *
     *  @param canOverrideResultOptions <code> true </code> if the
     *          result options can be overridden, <code> false </code>
     *          if constrained
     */

    protected void setCanOverrideResultOptions(boolean canOverrideResultOptions) {
        this.canOverrideResultOptions = canOverrideResultOptions;
        return;
    }


    /**
     *  Tests if the offering result options can be a subset of the
     *  canonical set.
     *
     *  @return <code> true </code> if the result options can be a subset, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean canConstrainResultOptions() {
        return (this.canConstrainResultOptions);
    }


    /**
     *  Sets the can constrain result options.
     *
     *  @param canConstrainResultOptions <code> true </code> if the
     *          result options can be a subset, <code> false </code>
     *          otherwise
     */

    protected void setCanConstrainResultOptions(boolean canConstrainResultOptions) {
        this.canConstrainResultOptions = canConstrainResultOptions;
        return;
    }


    /**
     *  Tests if the sponsors can be overridden at offering. 
     *
     *  @return <code> true </code> if the sponsors can be overridden, <code> 
     *          false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideSponsors() {
        return (this.canOverrideSponsors);
    }


    /**
     *  Sets the can override sponsors.
     *
     *  @param canOverrideSponsors <code> true </code> if the sponsors
     *          can be overridden, <code> false </code> if constrained
     */

    protected void setCanOverrideSponsors(boolean canOverrideSponsors) {
        this.canOverrideSponsors = canOverrideSponsors;
        return;
    }


    /**
     *  Tests if the offering sponsors can be a subset of the canonical set. 
     *
     *  @return <code> true </code> if the sponsors can be a subset,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean canConstrainSponsors() {
        return (this.canConstrainSponsors);
    }


    /**
     *  Sets the can constrain sponsors.
     *
     *  @param canConstrainSponsors <code> true </code> if the sponsors
     *          can be a subset, <code> false </code> otherwise
     */

    protected void setCanConstrainSponsors(boolean canConstrainSponsors) {
        this.canConstrainSponsors = canConstrainSponsors;
        return;
    }


    /**
     *  Tests if this offeringConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param offeringConstrainerRecordType an offering constrainer
     *         record type
     *  @return <code>true</code> if the offeringConstrainerRecordType
     *          is supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type offeringConstrainerRecordType) {
        for (org.osid.offering.rules.records.OfferingConstrainerRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param offeringConstrainerRecordType the offering constrainer
     *         record type
     *  @return the offering constrainer record 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerRecord getOfferingConstrainerRecord(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offering constrainer. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param offeringConstrainerRecord the offering constrainer
     *         record
     *  @param offeringConstrainerRecordType offering constrainer
     *         record type
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecord</code> or
     *          <code>offeringConstrainerRecordTypeofferingConstrainer</code>
     *          is <code>null</code>
     */
            
    protected void addOfferingConstrainerRecord(org.osid.offering.rules.records.OfferingConstrainerRecord offeringConstrainerRecord, 
                                     org.osid.type.Type offeringConstrainerRecordType) {

        addRecordType(offeringConstrainerRecordType);
        this.records.add(offeringConstrainerRecord);
        
        return;
    }
}
