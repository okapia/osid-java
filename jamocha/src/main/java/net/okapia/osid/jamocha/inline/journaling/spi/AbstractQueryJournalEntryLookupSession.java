//
// AbstractQueryJournalEntryLookupSession.java
//
//    An inline adapter that maps a JournalEntryLookupSession to
//    a JournalEntryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a JournalEntryLookupSession to
 *  a JournalEntryQuerySession.
 */

public abstract class AbstractQueryJournalEntryLookupSession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractJournalEntryLookupSession
    implements org.osid.journaling.JournalEntryLookupSession {


    private final org.osid.journaling.JournalEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryJournalEntryLookupSession.
     *
     *  @param querySession the underlying journal entry query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryJournalEntryLookupSession(org.osid.journaling.JournalEntryQuerySession querySession) {
        nullarg(querySession, "journal entry query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Journal</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Journal Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getJournalId() {
        return (this.session.getJournalId());
    }


    /**
     *  Gets the <code>Journal</code> associated with this 
     *  session.
     *
     *  @return the <code>Journal</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getJournal());
    }


    /**
     *  Tests if this user can perform <code>JournalEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canReadJournal() {
        return (this.session.canSearchJournalEntries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include journal entries in journals which are children
     *  of this journal in the journal hierarchy.
     */

    @OSID @Override
    public void useFederatedJournalView() {
        this.session.useFederatedJournalView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this journal only.
     */

    @OSID @Override
    public void useIsolatedJournalView() {
        this.session.useIsolatedJournalView();
        return;
    }
    
     
    /**
     *  Gets the <code>JournalEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>JournalEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>JournalEntry</code> and
     *  retained for compatibility.
     *
     *  @param  journalEntryId <code>Id</code> of the
     *          <code>JournalEntry</code>
     *  @return the journal entry
     *  @throws org.osid.NotFoundException <code>journalEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>journalEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry getJournalEntry(org.osid.id.Id journalEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchId(journalEntryId, true);
        org.osid.journaling.JournalEntryList journalEntries = this.session.getJournalEntriesByQuery(query);
        if (journalEntries.hasNext()) {
            return (journalEntries.getNextJournalEntry());
        } 
        
        throw new org.osid.NotFoundException(journalEntryId + " not found");
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  journalEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>JournalEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  journalEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByIds(org.osid.id.IdList journalEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();

        try (org.osid.id.IdList ids = journalEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  journal entry genus <code>Type</code> which does not include
     *  journal entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalEntryGenusType a journalEntry genus type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByGenusType(org.osid.type.Type journalEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchGenusType(journalEntryGenusType, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  journal entry genus <code>Type</code> and include any additional
     *  journal entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalEntryGenusType a journalEntry genus type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByParentGenusType(org.osid.type.Type journalEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchParentGenusType(journalEntryGenusType, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a <code>JournalEntryList</code> containing the given
     *  journal entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalEntryRecordType a journalEntry record type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByRecordType(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchRecordType(journalEntryRecordType, true);
        return (this.session.getJournalEntriesByQuery(query));
    }

    
    /**
     *  Gets a list of journal entries corresponding to a branch
     *  <code> Id.  </code> In plenary mode, the returned list
     *  contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  branchId the <code> Id </code> of the branch 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> branchId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForBranch(org.osid.id.Id branchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchBranchId(branchId, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets the journal entry corresponding to a resource <code> Id
     *  </code> and date. The entries returned have a date equal to or
     *  more recent than the requested date. In plenary mode, the
     *  returned list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  branchId the <code> Id </code> of the branch 
     *  @param  date from date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> branchId </code>
     *          or <code> date </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranch(org.osid.id.Id branchId, 
                                                                                 org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchBranchId(branchId, true);
        query.matchEntriesSince(date, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a list of journal entries corresponding to a branch
     *  <code> Id </code> and date range. Entries are returned with
     *  dates that fall between the requested dates inclusive. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results.  Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  branchId the <code> Id </code> of the branch 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> branchId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForBranch(org.osid.id.Id branchId, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchBranchId(branchId, true);
        query.matchTimestamp(from, to, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a list of journal entries corresponding to a source
     *  <code> Id.  </code> A source <code> Id </code> of any version
     *  may be requested. In plenary mode, the returned list contains
     *  all known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  sourceId the <code> Id </code> of the source 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> sourceId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets the journal entry corresponding to a source <code> Id
     *  </code> and date. The entry returned has a date equal to or
     *  more recent than the requested date. The <code> sourceId
     *  </code> may correspond to any version. In plenary mode, the
     *  returned list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  sourceId a source <code> Id </code> 
     *  @param  date from date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> sourceId </code>
     *          or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForSource(org.osid.id.Id sourceId, 
                                                                                 org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        query.matchEntriesSince(date, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a list of journal entries corresponding to a source
     *  <code> Id </code> and date range. Entries are returned with
     *  dates that fall between the requested dates inclusive. The
     *  <code> sourceId </code> may correspond to any version. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  sourceId a source <code> Id </code> 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> sourceId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForSource(org.osid.id.Id sourceId, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        query.matchTimestamp(from, to, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a list of journal entries corresponding to a branch and
     *  source <code> Id. </code> A source <code> Id </code> of any
     *  version may be requested. <code> </code> In plenary mode, the
     *  returned list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  branchId the <code> Id </code> of the branch 
     *  @param  sourceId the <code> Id </code> of the source 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> branchId </code>
     *          or <code> sourceId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForBranchAndSource(org.osid.id.Id branchId, 
                                                                                    org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchBranchId(branchId, true);
        query.matchSourceId(sourceId, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets the journal entry corresponding to a branch and source
     *  <code> Id </code> and date. The entry returned has a date
     *  equal to or more recent than the requested date. The <code>
     *  sourceId </code> may correspond to any version. In plenary
     *  mode, the returned list contains all known journal entries or
     *  an error results. Otherwise, the returned list may contain
     *  only those journal entries that are accessible through this
     *  session.
     *
     *  @param  branchId a branch <code> Id </code> 
     *  @param  sourceId the <code> Id </code> of the source 
     *  @param  date from date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> branchId,
     *          sourceId </code> or <code> date </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranchAndSource(org.osid.id.Id branchId, 
                                                                                          org.osid.id.Id sourceId, 
                                                                                          org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchBranchId(branchId, true);
        query.matchSourceId(sourceId, true);
        query.matchEntriesSince(date, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a list of journal entries corresponding to a branch and
     *  source <code> Id </code> and date range. Entries are returned
     *  with dates that fall between the requested dates
     *  inclusive. The <code> sourceId </code> may correspond to any
     *  version In plenary mode, the returned list contains all known
     *  journal entries or an error results.  Otherwise, the returned
     *  list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  branchId a branch <code> Id </code> 
     *  @param  sourceId the <code> Id </code> of the source 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> branchId,
     *          sourceId, from </code> or <code> to </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForBranchAndSource(org.osid.id.Id branchId, 
                                                                                               org.osid.id.Id sourceId, 
                                                                                               org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchBranchId(branchId, true);
        query.matchSourceId(sourceId, true);
        query.matchTimestamp(from, to, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a list of journal entries corresponding to a resource
     *  <code> Id.  </code> In plenary mode, the returned list
     *  contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets the journal entry corresponding to a resource <code> Id
     *  </code> and date. The entry returned has a date equal to or
     *  more recent than the requested date. In plenary mode, the
     *  returned list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @param  date from date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> or <code> date </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForResource(org.osid.id.Id resourceId, 
                                                                                   org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchEntriesSince(date, true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets a list of journal entries corresponding to a resource <code> Id 
     *  </code> and date range. Entries are returned with dates that fall 
     *  between the requested dates inclusive. In plenary mode, the returned 
     *  list contains all known journal entries or an error results. 
     *  Otherwise, the returned list may contain only those journal entries 
     *  that are accessible through this session. 
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, from </code> 
     *          or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForResource(org.osid.id.Id resourceId, 
                                                                                        org.osid.calendaring.DateTime from, 
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchTimestamp(from, to, true);
        return (this.session.getJournalEntriesByQuery(query));
    }

    
    /**
     *  Gets all <code>JournalEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known journal
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>JournalEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.journaling.JournalEntryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getJournalEntriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.journaling.JournalEntryQuery getQuery() {
        org.osid.journaling.JournalEntryQuery query = this.session.getJournalEntryQuery();
        
        return (query);
    }
}
