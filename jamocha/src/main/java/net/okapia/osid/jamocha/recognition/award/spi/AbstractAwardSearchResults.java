//
// AbstractAwardSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.award.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAwardSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.recognition.AwardSearchResults {

    private org.osid.recognition.AwardList awards;
    private final org.osid.recognition.AwardQueryInspector inspector;
    private final java.util.Collection<org.osid.recognition.records.AwardSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAwardSearchResults.
     *
     *  @param awards the result set
     *  @param awardQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>awards</code>
     *          or <code>awardQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAwardSearchResults(org.osid.recognition.AwardList awards,
                                            org.osid.recognition.AwardQueryInspector awardQueryInspector) {
        nullarg(awards, "awards");
        nullarg(awardQueryInspector, "award query inspectpr");

        this.awards = awards;
        this.inspector = awardQueryInspector;

        return;
    }


    /**
     *  Gets the award list resulting from a search.
     *
     *  @return an award list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwards() {
        if (this.awards == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.recognition.AwardList awards = this.awards;
        this.awards = null;
	return (awards);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.recognition.AwardQueryInspector getAwardQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  award search record <code> Type. </code> This method must
     *  be used to retrieve an award implementing the requested
     *  record.
     *
     *  @param awardSearchRecordType an award search 
     *         record type 
     *  @return the award search
     *  @throws org.osid.NullArgumentException
     *          <code>awardSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(awardSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AwardSearchResultsRecord getAwardSearchResultsRecord(org.osid.type.Type awardSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.recognition.records.AwardSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(awardSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(awardSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record award search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAwardRecord(org.osid.recognition.records.AwardSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "award record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
