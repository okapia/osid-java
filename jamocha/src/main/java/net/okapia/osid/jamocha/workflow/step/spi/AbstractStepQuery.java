//
// AbstractStepQuery.java
//
//     A template for making a Step Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.step.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for steps.
 */

public abstract class AbstractStepQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.workflow.StepQuery {

    private final java.util.Collection<org.osid.workflow.records.StepQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the process <code> Id </code> for this query. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcessId(org.osid.id.Id processId, boolean match) {
        return;
    }


    /**
     *  Clears the process <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProcessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuery getProcessQuery() {
        throw new org.osid.UnimplementedException("supportsProcessQuery() is false");
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearProcessTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches steps that have any resources. 
     *
     *  @param  match <code> true </code> to match steps with any resources, 
     *          <code> false </code> to match steps with no resources 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        return;
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Matches initial steps. 
     *
     *  @param  match <code> true </code> to match initial steps, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public void matchInitial(boolean match) {
        return;
    }


    /**
     *  Clears the initial step terms. 
     */

    @OSID @Override
    public void clearInitialTerms() {
        return;
    }


    /**
     *  Matches terminal steps. 
     *
     *  @param  match <code> true </code> to match terminal steps, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public void matchTerminal(boolean match) {
        return;
    }


    /**
     *  Clears the terminal step terms. 
     */

    @OSID @Override
    public void clearTerminalTerms() {
        return;
    }


    /**
     *  Sets the input state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInputStateId(org.osid.id.Id stateId, boolean match) {
        return;
    }


    /**
     *  Clears the input state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInputStateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for an input state. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getInputStateQuery() {
        throw new org.osid.UnimplementedException("supportsInputStateQuery() is false");
    }


    /**
     *  Matches steps that have any input states. 
     *
     *  @param  match <code> true </code> to match steps with any input 
     *          states, <code> false </code> to match steps with no input 
     *          states 
     */

    @OSID @Override
    public void matchAnyInputState(boolean match) {
        return;
    }


    /**
     *  Clears the input state terms. 
     */

    @OSID @Override
    public void clearInputStateTerms() {
        return;
    }


    /**
     *  Sets the next state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchNextStateId(org.osid.id.Id stateId, boolean match) {
        return;
    }


    /**
     *  Clears the next state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearNextStateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNextStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getNextStateQuery() {
        throw new org.osid.UnimplementedException("supportsNextStateQuery() is false");
    }


    /**
     *  Matches steps that have any next state, 
     *
     *  @param  match <code> true </code> to match steps with any next state, 
     *          <code> false </code> to match steps with no next state 
     */

    @OSID @Override
    public void matchAnyNextState(boolean match) {
        return;
    }


    /**
     *  Clears the state terms. 
     */

    @OSID @Override
    public void clearNextStateTerms() {
        return;
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches steps that have any work. 
     *
     *  @param  match <code> true </code> to match steps with any work, <code> 
     *          false </code> to match steps with no work 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        return;
    }


    /**
     *  Sets the office <code> Id </code> for this query to match steps 
     *  assigned to offices. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given step query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a step implementing the requested record.
     *
     *  @param stepRecordType a step record type
     *  @return the step query record
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.StepQueryRecord getStepQueryRecord(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.StepQueryRecord record : this.records) {
            if (record.implementsRecordType(stepRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step query. 
     *
     *  @param stepQueryRecord step query record
     *  @param stepRecordType step record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepQueryRecord(org.osid.workflow.records.StepQueryRecord stepQueryRecord, 
                                          org.osid.type.Type stepRecordType) {

        addRecordType(stepRecordType);
        nullarg(stepQueryRecord, "step query record");
        this.records.add(stepQueryRecord);        
        return;
    }
}
