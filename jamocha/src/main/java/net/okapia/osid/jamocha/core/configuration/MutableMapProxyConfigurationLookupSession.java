//
// MutableMapProxyConfigurationLookupSession
//
//    Implements a Configuration lookup service backed by a collection of
//    configurations that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration;


/**
 *  Implements a Configuration lookup service backed by a collection of
 *  configurations. The configurations are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of configurations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyConfigurationLookupSession
    extends net.okapia.osid.jamocha.core.configuration.spi.AbstractMapConfigurationLookupSession
    implements org.osid.configuration.ConfigurationLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyConfigurationLookupSession} with no
     *  configurations.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyConfigurationLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyConfigurationLookupSession} with a
     *  single configuration.
     *
     *  @param configuration a configuration
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyConfigurationLookupSession(org.osid.configuration.Configuration configuration, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putConfiguration(configuration);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyConfigurationLookupSession} using an
     *  array of configurations.
     *
     *  @param configurations an array of configurations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configurations} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyConfigurationLookupSession(org.osid.configuration.Configuration[] configurations, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putConfigurations(configurations);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyConfigurationLookupSession} using
     *  a collection of configurations.
     *
     *  @param configurations a collection of configurations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configurations} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyConfigurationLookupSession(java.util.Collection<? extends org.osid.configuration.Configuration> configurations,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putConfigurations(configurations);
        return;
    }

    
    /**
     *  Makes a {@code Configuration} available in this session.
     *
     *  @param configuration an configuration
     *  @throws org.osid.NullArgumentException {@code configuration{@code 
     *          is {@code null}
     */

    @Override
    public void putConfiguration(org.osid.configuration.Configuration configuration) {
        super.putConfiguration(configuration);
        return;
    }


    /**
     *  Makes an array of configurations available in this session.
     *
     *  @param configurations an array of configurations
     *  @throws org.osid.NullArgumentException {@code configurations{@code 
     *          is {@code null}
     */

    @Override
    public void putConfigurations(org.osid.configuration.Configuration[] configurations) {
        super.putConfigurations(configurations);
        return;
    }


    /**
     *  Makes collection of configurations available in this session.
     *
     *  @param configurations
     *  @throws org.osid.NullArgumentException {@code configuration{@code 
     *          is {@code null}
     */

    @Override
    public void putConfigurations(java.util.Collection<? extends org.osid.configuration.Configuration> configurations) {
        super.putConfigurations(configurations);
        return;
    }


    /**
     *  Removes a Configuration from this session.
     *
     *  @param configurationId the {@code Id} of the configuration
     *  @throws org.osid.NullArgumentException {@code configurationId{@code  is
     *          {@code null}
     */

    @Override
    public void removeConfiguration(org.osid.id.Id configurationId) {
        super.removeConfiguration(configurationId);
        return;
    }    
}
