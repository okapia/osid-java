//
// AbstractConvocationSearchOdrer.java
//
//     Defines a ConvocationSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.convocation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ConvocationSearchOrder}.
 */

public abstract class AbstractConvocationSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorSearchOrder
    implements org.osid.recognition.ConvocationSearchOrder {

    private final java.util.Collection<org.osid.recognition.records.ConvocationSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the time period. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimePeriod(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a time period order is available. 
     *
     *  @return <code> true </code> if a time period order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the time period order. 
     *
     *  @return the time period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchOrder getTimePeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimePeriodSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  convocationRecordType a convocation record type 
     *  @return {@code true} if the convocationRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code convocationRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type convocationRecordType) {
        for (org.osid.recognition.records.ConvocationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  convocationRecordType the convocation record type 
     *  @return the convocation search order record
     *  @throws org.osid.NullArgumentException
     *          {@code convocationRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(convocationRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationSearchOrderRecord getConvocationSearchOrderRecord(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConvocationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(convocationRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this convocation. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param convocationRecord the convocation search odrer record
     *  @param convocationRecordType convocation record type
     *  @throws org.osid.NullArgumentException
     *          {@code convocationRecord} or
     *          {@code convocationRecordTypeconvocation} is
     *          {@code null}
     */
            
    protected void addConvocationRecord(org.osid.recognition.records.ConvocationSearchOrderRecord convocationSearchOrderRecord, 
                                     org.osid.type.Type convocationRecordType) {

        addRecordType(convocationRecordType);
        this.records.add(convocationSearchOrderRecord);
        
        return;
    }
}
