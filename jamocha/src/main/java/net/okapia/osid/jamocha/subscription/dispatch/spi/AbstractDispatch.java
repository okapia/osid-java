//
// AbstractDispatch.java
//
//     Defines a Dispatch.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.dispatch.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Dispatch</code>.
 */

public abstract class AbstractDispatch
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernator
    implements org.osid.subscription.Dispatch {

    private final java.util.Collection<org.osid.type.Type> addressGenusTypes = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.subscription.records.DispatchRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the list of address genus types accepted by this dispatch. 
     *
     *  @return a list of address genus types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAddressGenusTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.addressGenusTypes));
    }


    /**
     *  Adds an address genus type.
     *
     *  @param addressGenusType an address genus type
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code>null</code>
     */

    protected void addAddressGenusType(org.osid.type.Type addressGenusType) {
        nullarg(addressGenusType, "address genus type");
        this.addressGenusTypes.add(addressGenusType);
        return;
    }


    /**
     *  Sets all the address genus types.
     *
     *  @param addressGenusTypes a collection of address genus types
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusTypes</code> is <code>null</code>
     */

    protected void setAddressGenusTypes(java.util.Collection<org.osid.type.Type> addressGenusTypes) {
        nullarg(addressGenusTypes, "address genus types");
        this.addressGenusTypes.clear();
        this.addressGenusTypes.addAll(addressGenusTypes);
        return;
    }
 

    /**
     *  Tests if this dispatch supports the given record
     *  <code>Type</code>.
     *
     *  @param  dispatchRecordType a dispatch record type 
     *  @return <code>true</code> if the dispatchRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type dispatchRecordType) {
        for (org.osid.subscription.records.DispatchRecord record : this.records) {
            if (record.implementsRecordType(dispatchRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Dispatch</code> record <code>Type</code>.
     *
     *  @param  dispatchRecordType the dispatch record type 
     *  @return the dispatch record 
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dispatchRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.DispatchRecord getDispatchRecord(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.DispatchRecord record : this.records) {
            if (record.implementsRecordType(dispatchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dispatchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this dispatch. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param dispatchRecord the dispatch record
     *  @param dispatchRecordType dispatch record type
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecord</code> or
     *          <code>dispatchRecordTypedispatch</code> is
     *          <code>null</code>
     */
            
    protected void addDispatchRecord(org.osid.subscription.records.DispatchRecord dispatchRecord, 
                                     org.osid.type.Type dispatchRecordType) {

        nullarg(dispatchRecord, "dispatch record");
        addRecordType(dispatchRecordType);
        this.records.add(dispatchRecord);
        
        return;
    }
}
