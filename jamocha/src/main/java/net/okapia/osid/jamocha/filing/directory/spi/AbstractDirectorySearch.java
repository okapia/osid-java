//
// AbstractDirectorySearch.java
//
//     A template for making a Directory Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.directory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing directory searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDirectorySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.filing.DirectorySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.filing.records.DirectorySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.filing.DirectorySearchOrder directorySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of directories. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  directoryIds list of directories
     *  @throws org.osid.NullArgumentException
     *          <code>directoryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDirectories(org.osid.id.IdList directoryIds) {
        while (directoryIds.hasNext()) {
            try {
                this.ids.add(directoryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDirectories</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of directory Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDirectoryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  directorySearchOrder directory search order 
     *  @throws org.osid.NullArgumentException
     *          <code>directorySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>directorySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDirectoryResults(org.osid.filing.DirectorySearchOrder directorySearchOrder) {
	this.directorySearchOrder = directorySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.filing.DirectorySearchOrder getDirectorySearchOrder() {
	return (this.directorySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given directory search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a directory implementing the requested record.
     *
     *  @param directorySearchRecordType a directory search record
     *         type
     *  @return the directory search record
     *  @throws org.osid.NullArgumentException
     *          <code>directorySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directorySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.DirectorySearchRecord getDirectorySearchRecord(org.osid.type.Type directorySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.filing.records.DirectorySearchRecord record : this.records) {
            if (record.implementsRecordType(directorySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directorySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this directory search. 
     *
     *  @param directorySearchRecord directory search record
     *  @param directorySearchRecordType directory search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDirectorySearchRecord(org.osid.filing.records.DirectorySearchRecord directorySearchRecord, 
                                           org.osid.type.Type directorySearchRecordType) {

        addRecordType(directorySearchRecordType);
        this.records.add(directorySearchRecord);        
        return;
    }
}
