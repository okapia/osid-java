//
// AbstractQueryActivityUnitLookupSession.java
//
//    An inline adapter that maps an ActivityUnitLookupSession to
//    an ActivityUnitQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ActivityUnitLookupSession to
 *  an ActivityUnitQuerySession.
 */

public abstract class AbstractQueryActivityUnitLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractActivityUnitLookupSession
    implements org.osid.course.ActivityUnitLookupSession {

    private boolean activeonly = false;
    private final org.osid.course.ActivityUnitQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryActivityUnitLookupSession.
     *
     *  @param querySession the underlying activity unit query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryActivityUnitLookupSession(org.osid.course.ActivityUnitQuerySession querySession) {
        nullarg(querySession, "activity unit query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>ActivityUnit</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivityUnits() {
        return (this.session.canSearchActivityUnits());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity units in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only active activity units are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveActivityUnitView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive activity units are returned by methods in
     *  this session.
     */
    
    @OSID @Override
    public void useAnyStatusActivityUnitView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */

    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>ActivityUnit</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActivityUnit</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ActivityUnit</code> and
     *  retained for compatibility.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param  activityUnitId <code>Id</code> of the
     *          <code>ActivityUnit</code>
     *  @return the activity unit
     *  @throws org.osid.NotFoundException <code>activityUnitId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityUnitId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnit getActivityUnit(org.osid.id.Id activityUnitId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityUnitQuery query = getQuery();
        query.matchId(activityUnitId, true);
        org.osid.course.ActivityUnitList activityUnits = this.session.getActivityUnitsByQuery(query);
        if (activityUnits.hasNext()) {
            return (activityUnits.getNextActivityUnit());
        } 
        
        throw new org.osid.NotFoundException(activityUnitId + " not found");
    }


    /**
     *  Gets an <code>ActivityUnitList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activityUnits specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ActivityUnits</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param  activityUnitIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActivityUnit</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByIds(org.osid.id.IdList activityUnitIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityUnitQuery query = getQuery();

        try (org.osid.id.IdList ids = activityUnitIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getActivityUnitsByQuery(query));
    }


    /**
     *  Gets an <code>ActivityUnitList</code> corresponding to the
     *  given activity unit genus <code>Type</code> which does not
     *  include activity units of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  units or an error results. Otherwise, the returned list may
     *  contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param  activityUnitGenusType an activityUnit genus type 
     *  @return the returned <code>ActivityUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByGenusType(org.osid.type.Type activityUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityUnitQuery query = getQuery();
        query.matchGenusType(activityUnitGenusType, true);
        return (this.session.getActivityUnitsByQuery(query));
    }


    /**
     *  Gets an <code>ActivityUnitList</code> corresponding to the given
     *  activity unit genus <code>Type</code> and include any additional
     *  activity units with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activity units or an error results. Otherwise, the returned list
     *  may contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param  activityUnitGenusType an activityUnit genus type 
     *  @return the returned <code>ActivityUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByParentGenusType(org.osid.type.Type activityUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityUnitQuery query = getQuery();
        query.matchParentGenusType(activityUnitGenusType, true);
        return (this.session.getActivityUnitsByQuery(query));
    }


    /**
     *  Gets an <code>ActivityUnitList</code> containing the given
     *  activity unit record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activity units or an error results. Otherwise, the returned list
     *  may contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activity units are returned that are currently
     *  effective.  In any effective mode, effective activity units and
     *  those currently expired are returned.
     *
     *  @param  activityUnitRecordType an activityUnit record type 
     *  @return the returned <code>ActivityUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByRecordType(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityUnitQuery query = getQuery();
        query.matchRecordType(activityUnitRecordType, true);
        return (this.session.getActivityUnitsByQuery(query));
    }

    
    /**
     *  Gets an <code> ActivityUnitList </code> for the given
     *  course. 
     *  
     *  In plenary mode, the returned list contains all known activity
     *  units or an error results. Otherwise, the returned list may
     *  contain only those activity units that are accessible through
     *  this session.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param  courseId a course <code> Id </code> 
     *  @return the returned <code> ActivityUnit </code> list 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityUnitQuery query = getQuery();
        query.matchCourseId(courseId, true);
        return (this.session.getActivityUnitsByQuery(query));
    }


    /**
     *  Gets all <code>ActivityUnits</code>. 
     *
     *  In plenary mode, the returned list contains all known activity
     *  units or an error results. Otherwise, the returned list may
     *  contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @return a list of <code>ActivityUnits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.course.ActivityUnitQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getActivityUnitsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.ActivityUnitQuery getQuery() {
        org.osid.course.ActivityUnitQuery query = this.session.getActivityUnitQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
