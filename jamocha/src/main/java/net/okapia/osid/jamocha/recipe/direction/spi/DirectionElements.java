//
// DirectionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.direction.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class DirectionElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the DirectionElement Id.
     *
     *  @return the direction element Id
     */

    public static org.osid.id.Id getDirectionEntityId() {
        return (makeEntityId("osid.recipe.Direction"));
    }


    /**
     *  Gets the RecipeId element Id.
     *
     *  @return the RecipeId element Id
     */

    public static org.osid.id.Id getRecipeId() {
        return (makeElementId("osid.recipe.direction.RecipeId"));
    }


    /**
     *  Gets the Recipe element Id.
     *
     *  @return the Recipe element Id
     */

    public static org.osid.id.Id getRecipe() {
        return (makeElementId("osid.recipe.direction.Recipe"));
    }


    /**
     *  Gets the ProcedureIds element Id.
     *
     *  @return the ProcedureIds element Id
     */

    public static org.osid.id.Id getProcedureIds() {
        return (makeElementId("osid.recipe.direction.ProcedureIds"));
    }


    /**
     *  Gets the Procedures element Id.
     *
     *  @return the Procedures element Id
     */

    public static org.osid.id.Id getProcedures() {
        return (makeElementId("osid.recipe.direction.Procedures"));
    }


    /**
     *  Gets the Ingredients element Id.
     *
     *  @return the Ingredients element Id
     */

    public static org.osid.id.Id getIngredients() {
        return (makeElementId("osid.recipe.direction.Ingredients"));
    }


    /**
     *  Gets the EstimatedDuration element Id.
     *
     *  @return the EstimatedDuration element Id
     */

    public static org.osid.id.Id getEstimatedDuration() {
        return (makeElementId("osid.recipe.direction.EstimatedDuration"));
    }


    /**
     *  Gets the AssetIds element Id.
     *
     *  @return the AssetIds element Id
     */

    public static org.osid.id.Id getAssetIds() {
        return (makeElementId("osid.recipe.direction.AssetIds"));
    }


    /**
     *  Gets the Assets element Id.
     *
     *  @return the Assets element Id
     */

    public static org.osid.id.Id getAssets() {
        return (makeElementId("osid.recipe.direction.Assets"));
    }


    /**
     *  Gets the StockId element Id.
     *
     *  @return the StockId element Id
     */

    public static org.osid.id.Id getStockId() {
        return (makeQueryElementId("osid.recipe.direction.StockId"));
    }


    /**
     *  Gets the Stock element Id.
     *
     *  @return the Stock element Id
     */

    public static org.osid.id.Id getStock() {
        return (makeQueryElementId("osid.recipe.direction.Stock"));
    }


    /**
     *  Gets the CookbookId element Id.
     *
     *  @return the CookbookId element Id
     */

    public static org.osid.id.Id getCookbookId() {
        return (makeQueryElementId("osid.recipe.direction.CookbookId"));
    }


    /**
     *  Gets the Cookbook element Id.
     *
     *  @return the Cookbook element Id
     */

    public static org.osid.id.Id getCookbook() {
        return (makeQueryElementId("osid.recipe.direction.Cookbook"));
    }
}
