//
// AbstractQueryFiscalPeriodLookupSession.java
//
//    A FiscalPeriodQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A FiscalPeriodQuerySession adapter.
 */

public abstract class AbstractAdapterFiscalPeriodQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.financials.FiscalPeriodQuerySession {

    private final org.osid.financials.FiscalPeriodQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterFiscalPeriodQuerySession.
     *
     *  @param session the underlying fiscal period query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterFiscalPeriodQuerySession(org.osid.financials.FiscalPeriodQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeBusiness</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeBusiness Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@codeBusiness</code> associated with this 
     *  session.
     *
     *  @return the {@codeBusiness</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@codeFiscalPeriod</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchFiscalPeriods() {
        return (this.session.canSearchFiscalPeriods());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include fiscal periods in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this business only.
     */
    
    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
      
    /**
     *  Gets a fiscal period query. The returned query will not have an
     *  extension query.
     *
     *  @return the fiscal period query 
     */
      
    @OSID @Override
    public org.osid.financials.FiscalPeriodQuery getFiscalPeriodQuery() {
        return (this.session.getFiscalPeriodQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  fiscalPeriodQuery the fiscal period query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code fiscalPeriodQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code fiscalPeriodQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByQuery(org.osid.financials.FiscalPeriodQuery fiscalPeriodQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getFiscalPeriodsByQuery(fiscalPeriodQuery));
    }
}
