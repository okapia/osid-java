//
// AbstractMapFiscalPeriodLookupSession
//
//    A simple framework for providing a FiscalPeriod lookup service
//    backed by a fixed collection of fiscal periods.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a FiscalPeriod lookup service backed by a
 *  fixed collection of fiscal periods. The fiscal periods are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>FiscalPeriods</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapFiscalPeriodLookupSession
    extends net.okapia.osid.jamocha.financials.spi.AbstractFiscalPeriodLookupSession
    implements org.osid.financials.FiscalPeriodLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.financials.FiscalPeriod> fiscalPeriods = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.financials.FiscalPeriod>());


    /**
     *  Makes a <code>FiscalPeriod</code> available in this session.
     *
     *  @param  fiscalPeriod a fiscal period
     *  @throws org.osid.NullArgumentException <code>fiscalPeriod<code>
     *          is <code>null</code>
     */

    protected void putFiscalPeriod(org.osid.financials.FiscalPeriod fiscalPeriod) {
        this.fiscalPeriods.put(fiscalPeriod.getId(), fiscalPeriod);
        return;
    }


    /**
     *  Makes an array of fiscal periods available in this session.
     *
     *  @param  fiscalPeriods an array of fiscal periods
     *  @throws org.osid.NullArgumentException <code>fiscalPeriods<code>
     *          is <code>null</code>
     */

    protected void putFiscalPeriods(org.osid.financials.FiscalPeriod[] fiscalPeriods) {
        putFiscalPeriods(java.util.Arrays.asList(fiscalPeriods));
        return;
    }


    /**
     *  Makes a collection of fiscal periods available in this session.
     *
     *  @param  fiscalPeriods a collection of fiscal periods
     *  @throws org.osid.NullArgumentException <code>fiscalPeriods<code>
     *          is <code>null</code>
     */

    protected void putFiscalPeriods(java.util.Collection<? extends org.osid.financials.FiscalPeriod> fiscalPeriods) {
        for (org.osid.financials.FiscalPeriod fiscalPeriod : fiscalPeriods) {
            this.fiscalPeriods.put(fiscalPeriod.getId(), fiscalPeriod);
        }

        return;
    }


    /**
     *  Removes a FiscalPeriod from this session.
     *
     *  @param  fiscalPeriodId the <code>Id</code> of the fiscal period
     *  @throws org.osid.NullArgumentException <code>fiscalPeriodId<code> is
     *          <code>null</code>
     */

    protected void removeFiscalPeriod(org.osid.id.Id fiscalPeriodId) {
        this.fiscalPeriods.remove(fiscalPeriodId);
        return;
    }


    /**
     *  Gets the <code>FiscalPeriod</code> specified by its <code>Id</code>.
     *
     *  @param  fiscalPeriodId <code>Id</code> of the <code>FiscalPeriod</code>
     *  @return the fiscalPeriod
     *  @throws org.osid.NotFoundException <code>fiscalPeriodId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>fiscalPeriodId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.financials.FiscalPeriod fiscalPeriod = this.fiscalPeriods.get(fiscalPeriodId);
        if (fiscalPeriod == null) {
            throw new org.osid.NotFoundException("fiscalPeriod not found: " + fiscalPeriodId);
        }

        return (fiscalPeriod);
    }


    /**
     *  Gets all <code>FiscalPeriods</code>. In plenary mode, the returned
     *  list contains all known fiscalPeriods or an error
     *  results. Otherwise, the returned list may contain only those
     *  fiscalPeriods that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>FiscalPeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.fiscalperiod.ArrayFiscalPeriodList(this.fiscalPeriods.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.fiscalPeriods.clear();
        super.close();
        return;
    }
}
