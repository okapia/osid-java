//
// AbstractAppointment.java
//
//     Defines an Appointment.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.appointment.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.percentarg;


/**
 *  Defines an <code>Appointment</code>.
 */

public abstract class AbstractAppointment
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.personnel.Appointment {

    private org.osid.personnel.Person person;
    private org.osid.personnel.Position position;
    private long commitment;
    private org.osid.locale.DisplayText title;
    private org.osid.financials.Currency salary;
    private long salaryBasis;

    private final java.util.Collection<org.osid.personnel.records.AppointmentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the person. 
     *
     *  @return the person <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPersonId() {
        return (this.person.getId());
    }


    /**
     *  Gets the person in this appointment. 
     *
     *  @return the <code> Person </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.personnel.Person getPerson()
        throws org.osid.OperationFailedException {

        return (this.person);
    }


    /**
     *  Sets the person.
     *
     *  @param person a person
     *  @throws org.osid.NullArgumentException <code>person</code> is
     *          <code>null</code>
     */

    protected void setPerson(org.osid.personnel.Person person) {
        nullarg(person, "person");
        this.person = person;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the position in this appointment. 
     *
     *  @return the <code> Position </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPositionId() {
        return (this.position.getId());
    }


    /**
     *  Gets the position in this appointment. 
     *
     *  @return the <code> Position </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.personnel.Position getPosition()
        throws org.osid.OperationFailedException {

        return (this.position);
    }


    /**
     *  Sets the position.
     *
     *  @param position a position
     *  @throws org.osid.NullArgumentException <code>position</code>
     *          is <code>null</code>
     */

    protected void setPosition(org.osid.personnel.Position position) {
        nullarg(position, "position");
        this.position = position;
        return;
    }


    /**
     *  Gets the percentage commitment (0-100). 
     *
     *  @return the percentage commitment 
     */

    @OSID @Override
    public long getCommitment() {
        return (this.commitment);
    }


    /**
     *  Sets the commitment.
     *
     *  @param percentage a commitment (0-100)
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    protected void setCommitment(long percentage) {
        percentarg(percentage, "commitment");
        this.commitment = commitment;
        return;
    }


    /**
     *  Gets the title for this appointment. 
     *
     *  @return the title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.title);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    protected void setTitle(org.osid.locale.DisplayText title) {
        nullarg(title, "title");
        this.title = title;
        return;
    }


    /**
     *  Tests if a salary is available. 
     *
     *  @return <code> true </code> if a salary is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSalary() {
        return (this.salary != null);
    }


    /**
     *  Gets the salary. 
     *
     *  @return the slaary 
     *  @throws org.osid.IllegalStateException <code> hasSalary()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.financials.Currency getSalary() {
        if (!hasSalary()) {
            throw new org.osid.IllegalStateException("hasSalary() is false");
        }

        return (this.salary);
    }


    /**
     *  Sets the salary.
     *
     *  @param salary a salary
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    protected void setSalary(org.osid.financials.Currency salary) {
        nullarg(salary, "salary");
        this.salary = salary;
        return;
    }


    /**
     *  Gets the basis in number of months per year. 
     *
     *  @return the number of months 
     *  @throws org.osid.IllegalStateException <code> hasSalary() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getSalaryBasis() {
        if (!hasSalary()) {
            throw new org.osid.IllegalStateException("hasSalary() is false");
        }

        return (this.salaryBasis);
    }


    /**
     *  Sets the salary basis.
     *
     *  @param basis a salary basis
     *  @throws org.osid.InvalidArgumentException <code>basis</code>
     *          is negative
     */

    protected void setSalaryBasis(long basis) {
        cardinalarg(basis, "salary basis");
        this.salaryBasis = basis;
        return;
    }


    /**
     *  Tests if this appointment supports the given record
     *  <code>Type</code>.
     *
     *  @param  appointmentRecordType an appointment record type 
     *  @return <code>true</code> if the appointmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type appointmentRecordType) {
        for (org.osid.personnel.records.AppointmentRecord record : this.records) {
            if (record.implementsRecordType(appointmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Appointment</code> record <code>Type</code>.
     *
     *  @param  appointmentRecordType the appointment record type 
     *  @return the appointment record 
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(appointmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.AppointmentRecord getAppointmentRecord(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.AppointmentRecord record : this.records) {
            if (record.implementsRecordType(appointmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(appointmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this appointment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param appointmentRecord the appointment record
     *  @param appointmentRecordType appointment record type
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecord</code> or
     *          <code>appointmentRecordTypeappointment</code> is
     *          <code>null</code>
     */
            
    protected void addAppointmentRecord(org.osid.personnel.records.AppointmentRecord appointmentRecord, 
                                        org.osid.type.Type appointmentRecordType) {

        nullarg(appointmentRecord, "appointment record");
        addRecordType(appointmentRecordType);
        this.records.add(appointmentRecord);
        
        return;
    }
}
