//
// AbstractImmutableAnswer.java
//
//     Wraps a mutable Answer to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.answer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Answer</code> to hide modifiers. This
 *  wrapper provides an immutized Answer from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying answer whose state changes are visible.
 */

public abstract class AbstractImmutableAnswer
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.assessment.Answer {

    private final org.osid.assessment.Answer answer;


    /**
     *  Constructs a new <code>AbstractImmutableAnswer</code>.
     *
     *  @param answer the answer to immutablize
     *  @throws org.osid.NullArgumentException <code>answer</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAnswer(org.osid.assessment.Answer answer) {
        super(answer);
        this.answer = answer;
        return;
    }


    /**
     *  Gets the answer record corresponding to the given <code> Item </code> 
     *  record <code> Type. </code> The answer record and this item record are 
     *  identified by the same type. This method is used to retrieve an object 
     *  implementing the requested records. The <code> itemRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(itemRecordType) </code> is <code> true </code> . 
     *
     *  @param  itemRecordType the type of the record to retrieve 
     *  @return the answer record 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(itemRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.records.AnswerRecord getAnswerRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        return (this.answer.getAnswerRecord(itemRecordType));
    }
}

