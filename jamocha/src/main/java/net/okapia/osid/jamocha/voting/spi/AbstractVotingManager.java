//
// AbstractVotingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractVotingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.voting.VotingManager,
               org.osid.voting.VotingProxyManager {

    private final Types voteRecordTypes                    = new TypeRefSet();
    private final Types voteSearchRecordTypes              = new TypeRefSet();

    private final Types voterAllocationRecordTypes         = new TypeRefSet();
    private final Types candidateRecordTypes               = new TypeRefSet();
    private final Types candidateSearchRecordTypes         = new TypeRefSet();

    private final Types raceRecordTypes                    = new TypeRefSet();
    private final Types raceSearchRecordTypes              = new TypeRefSet();

    private final Types ballotRecordTypes                  = new TypeRefSet();
    private final Types ballotSearchRecordTypes            = new TypeRefSet();

    private final Types votingResultsRecordTypes           = new TypeRefSet();
    private final Types pollsRecordTypes                   = new TypeRefSet();
    private final Types pollsSearchRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractVotingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractVotingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if voting is supported. 
     *
     *  @return <code> true </code> if voting is supported <code> , </code> 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoting() {
        return (false);
    }


    /**
     *  Tests if race results is supported. 
     *
     *  @return <code> true </code> if race results is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceResults() {
        return (false);
    }


    /**
     *  Tests if voting allocation lookup is supported. 
     *
     *  @return <code> true </code> if voting allocation lookup is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingAllocationLookup() {
        return (false);
    }


    /**
     *  Tests if voting allocation administration is supported. 
     *
     *  @return <code> true </code> if voting allocation administration is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingAllocationAdmin() {
        return (false);
    }


    /**
     *  Tests if looking up votes is supported. 
     *
     *  @return <code> true </code> if votes lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteLookup() {
        return (false);
    }


    /**
     *  Tests if querying votes is supported. 
     *
     *  @return <code> true </code> if votes query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteQuery() {
        return (false);
    }


    /**
     *  Tests if searching votes is supported. 
     *
     *  @return <code> true </code> if votes search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteSearch() {
        return (false);
    }


    /**
     *  Tests if a votes <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if votes notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of votes and polls is supported. 
     *
     *  @return <code> true </code> if vote polls mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotePolls() {
        return (false);
    }


    /**
     *  Tests if managing mappings of votes and polls is supported. 
     *
     *  @return <code> true </code> if vote polls assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotePollsAssignment() {
        return (false);
    }


    /**
     *  Tests if vote smart polls are available. 
     *
     *  @return <code> true </code> if vote smart polls are supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteSmartPolls() {
        return (false);
    }


    /**
     *  Tests if candidate lookup is supported. 
     *
     *  @return <code> true </code> if candidate lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateLookup() {
        return (false);
    }


    /**
     *  Tests if candidate query is supported. 
     *
     *  @return <code> true </code> if candidate query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateQuery() {
        return (false);
    }


    /**
     *  Tests if candidate search is supported. 
     *
     *  @return <code> true </code> if candidate search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateSearch() {
        return (false);
    }


    /**
     *  Tests if candidate administration is supported. 
     *
     *  @return <code> true </code> if candidate administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateAdmin() {
        return (false);
    }


    /**
     *  Tests if candidate notification is supported. Messages may be sent 
     *  when candidates are created, modified, or deleted. 
     *
     *  @return <code> true </code> if candidate notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of candidate and polls is supported. 
     *
     *  @return <code> true </code> if candidate polls mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidatePolls() {
        return (false);
    }


    /**
     *  Tests if managing mappings of candidate and polls is supported. 
     *
     *  @return <code> true </code> if candidate polls assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidatePollsAssignment() {
        return (false);
    }


    /**
     *  Tests if candidate smart polls are available. 
     *
     *  @return <code> true </code> if candidate smart polls are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateSmartPolls() {
        return (false);
    }


    /**
     *  Tests if looking up races is supported. 
     *
     *  @return <code> true </code> if race lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceLookup() {
        return (false);
    }


    /**
     *  Tests if querying races is supported. 
     *
     *  @return <code> true </code> if race query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceQuery() {
        return (false);
    }


    /**
     *  Tests if searching races is supported. 
     *
     *  @return <code> true </code> if races search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceSearch() {
        return (false);
    }


    /**
     *  Tests if a race <code> a </code> dministrative service is supported. 
     *
     *  @return <code> true </code> if race administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceAdmin() {
        return (false);
    }


    /**
     *  Tests if a race <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if race notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of races and polls is supported. 
     *
     *  @return <code> true </code> if race polls mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRacePolls() {
        return (false);
    }


    /**
     *  Tests if managing mappings of races and polls is supported. 
     *
     *  @return <code> true </code> if race polls assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRacePollsAssignment() {
        return (false);
    }


    /**
     *  Tests if race smart polls are available. 
     *
     *  @return <code> true </code> if race smart polls are supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceSmartPolls() {
        return (false);
    }


    /**
     *  Tests if looking up ballots is supported. 
     *
     *  @return <code> true </code> if ballot lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotLookup() {
        return (false);
    }


    /**
     *  Tests if querying ballots is supported. 
     *
     *  @return <code> true </code> if ballot query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotQuery() {
        return (false);
    }


    /**
     *  Tests if searching ballots is supported. 
     *
     *  @return <code> true </code> if ballot search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotSearch() {
        return (false);
    }


    /**
     *  Tests if a ballot <code> a </code> dministrative service is supported. 
     *
     *  @return <code> true </code> if ballot administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotAdmin() {
        return (false);
    }


    /**
     *  Tests if a ballot notification service is supported. 
     *
     *  @return <code> true </code> if ballot notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of ballots and polls is supported. 
     *
     *  @return <code> true </code> if ballot polls mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotPolls() {
        return (false);
    }


    /**
     *  Tests if managing mappings of ballots and polls is supported. 
     *
     *  @return <code> true </code> if ballot polls assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotPollsAssignment() {
        return (false);
    }


    /**
     *  Tests if ballot smart polls are available. 
     *
     *  @return <code> true </code> if ballot smart polls are supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotSmartPolls() {
        return (false);
    }


    /**
     *  Tests if polls lookup is supported. 
     *
     *  @return <code> true </code> if polls lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsLookup() {
        return (false);
    }


    /**
     *  Tests if polls query is supported. 
     *
     *  @return <code> true </code> if polls query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Tests if polls search is supported. 
     *
     *  @return <code> true </code> if polls search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsSearch() {
        return (false);
    }


    /**
     *  Tests if polls administration is supported. 
     *
     *  @return <code> true </code> if polls administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsAdmin() {
        return (false);
    }


    /**
     *  Tests if polls notification is supported. Messages may be sent when 
     *  <code> Polls </code> objects are created, deleted or updated. 
     *  Notifications for candidates within polls are sent via the candidate 
     *  notification session. 
     *
     *  @return <code> true </code> if polls notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsNotification() {
        return (false);
    }


    /**
     *  Tests if a polls hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a polls hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsHierarchy() {
        return (false);
    }


    /**
     *  Tests if a polls hierarchy design is supported. 
     *
     *  @return <code> true </code> if a polls hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a voting batch service is supported. 
     *
     *  @return <code> true </code> if a voting batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingBatch() {
        return (false);
    }


    /**
     *  Tests if a voting rules service is supported. 
     *
     *  @return <code> true </code> if a voting rules service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Vote </code> record types. 
     *
     *  @return a list containing the supported <code> Vote </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVoteRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.voteRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Vote </code> record type is supported. 
     *
     *  @param  voteRecordType a <code> Type </code> indicating a <code> Vote 
     *          </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> voteRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVoteRecordType(org.osid.type.Type voteRecordType) {
        return (this.voteRecordTypes.contains(voteRecordType));
    }


    /**
     *  Adds support for a vote record type.
     *
     *  @param voteRecordType a vote record type
     *  @throws org.osid.NullArgumentException
     *  <code>voteRecordType</code> is <code>null</code>
     */

    protected void addVoteRecordType(org.osid.type.Type voteRecordType) {
        this.voteRecordTypes.add(voteRecordType);
        return;
    }


    /**
     *  Removes support for a vote record type.
     *
     *  @param voteRecordType a vote record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>voteRecordType</code> is <code>null</code>
     */

    protected void removeVoteRecordType(org.osid.type.Type voteRecordType) {
        this.voteRecordTypes.remove(voteRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Vote </code> search record types. 
     *
     *  @return a list containing the supported <code> Vote </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVoteSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.voteSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Vote </code> search record type is 
     *  supported. 
     *
     *  @param  voteSearchRecordType a <code> Type </code> indicating a <code> 
     *          Vote </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> voteSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVoteSearchRecordType(org.osid.type.Type voteSearchRecordType) {
        return (this.voteSearchRecordTypes.contains(voteSearchRecordType));
    }


    /**
     *  Adds support for a vote search record type.
     *
     *  @param voteSearchRecordType a vote search record type
     *  @throws org.osid.NullArgumentException
     *  <code>voteSearchRecordType</code> is <code>null</code>
     */

    protected void addVoteSearchRecordType(org.osid.type.Type voteSearchRecordType) {
        this.voteSearchRecordTypes.add(voteSearchRecordType);
        return;
    }


    /**
     *  Removes support for a vote search record type.
     *
     *  @param voteSearchRecordType a vote search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>voteSearchRecordType</code> is <code>null</code>
     */

    protected void removeVoteSearchRecordType(org.osid.type.Type voteSearchRecordType) {
        this.voteSearchRecordTypes.remove(voteSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> VoterAllocation </code> record types. 
     *
     *  @return a list containing the supported <code> VoterAllocation </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVoterAllocationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.voterAllocationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> VoterAllocation </code> record type is 
     *  supported. 
     *
     *  @param  voterAllocationRecordType a <code> Type </code> indicating a 
     *          <code> VoterAllocation </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          voterAllocationRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVoterAllocationRecordType(org.osid.type.Type voterAllocationRecordType) {
        return (this.voterAllocationRecordTypes.contains(voterAllocationRecordType));
    }


    /**
     *  Adds support for a voter allocation record type.
     *
     *  @param voterAllocationRecordType a voter allocation record type
     *  @throws org.osid.NullArgumentException
     *  <code>voterAllocationRecordType</code> is <code>null</code>
     */

    protected void addVoterAllocationRecordType(org.osid.type.Type voterAllocationRecordType) {
        this.voterAllocationRecordTypes.add(voterAllocationRecordType);
        return;
    }


    /**
     *  Removes support for a voter allocation record type.
     *
     *  @param voterAllocationRecordType a voter allocation record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>voterAllocationRecordType</code> is <code>null</code>
     */

    protected void removeVoterAllocationRecordType(org.osid.type.Type voterAllocationRecordType) {
        this.voterAllocationRecordTypes.remove(voterAllocationRecordType);
        return;
    }


    /**
     *  Gets all the candidate record types supported. 
     *
     *  @return the list of supported candidate record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCandidateRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.candidateRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given candidate record type is supported. 
     *
     *  @param  candidateRecordType the candidate type 
     *  @return <code> true </code> if the candidate record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> candidateRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCandidateRecordType(org.osid.type.Type candidateRecordType) {
        return (this.candidateRecordTypes.contains(candidateRecordType));
    }


    /**
     *  Adds support for a candidate record type.
     *
     *  @param candidateRecordType a candidate record type
     *  @throws org.osid.NullArgumentException
     *  <code>candidateRecordType</code> is <code>null</code>
     */

    protected void addCandidateRecordType(org.osid.type.Type candidateRecordType) {
        this.candidateRecordTypes.add(candidateRecordType);
        return;
    }


    /**
     *  Removes support for a candidate record type.
     *
     *  @param candidateRecordType a candidate record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>candidateRecordType</code> is <code>null</code>
     */

    protected void removeCandidateRecordType(org.osid.type.Type candidateRecordType) {
        this.candidateRecordTypes.remove(candidateRecordType);
        return;
    }


    /**
     *  Gets all the candidate search record types supported. 
     *
     *  @return the list of supported candidate search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCandidateSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.candidateSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given candidate search type is supported. 
     *
     *  @param  candidateSearchRecordType the candidate search type 
     *  @return <code> true </code> if the candidate search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          candidateSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCandidateSearchRecordType(org.osid.type.Type candidateSearchRecordType) {
        return (this.candidateSearchRecordTypes.contains(candidateSearchRecordType));
    }


    /**
     *  Adds support for a candidate search record type.
     *
     *  @param candidateSearchRecordType a candidate search record type
     *  @throws org.osid.NullArgumentException
     *  <code>candidateSearchRecordType</code> is <code>null</code>
     */

    protected void addCandidateSearchRecordType(org.osid.type.Type candidateSearchRecordType) {
        this.candidateSearchRecordTypes.add(candidateSearchRecordType);
        return;
    }


    /**
     *  Removes support for a candidate search record type.
     *
     *  @param candidateSearchRecordType a candidate search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>candidateSearchRecordType</code> is <code>null</code>
     */

    protected void removeCandidateSearchRecordType(org.osid.type.Type candidateSearchRecordType) {
        this.candidateSearchRecordTypes.remove(candidateSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Race </code> record types. 
     *
     *  @return a list containing the supported <code> Race </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Race </code> record type is supported. 
     *
     *  @param  raceRecordType a <code> Type </code> indicating a <code> Race 
     *          </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> raceRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceRecordType(org.osid.type.Type raceRecordType) {
        return (this.raceRecordTypes.contains(raceRecordType));
    }


    /**
     *  Adds support for a race record type.
     *
     *  @param raceRecordType a race record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceRecordType</code> is <code>null</code>
     */

    protected void addRaceRecordType(org.osid.type.Type raceRecordType) {
        this.raceRecordTypes.add(raceRecordType);
        return;
    }


    /**
     *  Removes support for a race record type.
     *
     *  @param raceRecordType a race record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceRecordType</code> is <code>null</code>
     */

    protected void removeRaceRecordType(org.osid.type.Type raceRecordType) {
        this.raceRecordTypes.remove(raceRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Race </code> search record types. 
     *
     *  @return a list containing the supported <code> Race </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Race </code> search record type is 
     *  supported. 
     *
     *  @param  raceSearchRecordType a <code> Type </code> indicating a <code> 
     *          Race </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> raceSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceSearchRecordType(org.osid.type.Type raceSearchRecordType) {
        return (this.raceSearchRecordTypes.contains(raceSearchRecordType));
    }


    /**
     *  Adds support for a race search record type.
     *
     *  @param raceSearchRecordType a race search record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceSearchRecordType</code> is <code>null</code>
     */

    protected void addRaceSearchRecordType(org.osid.type.Type raceSearchRecordType) {
        this.raceSearchRecordTypes.add(raceSearchRecordType);
        return;
    }


    /**
     *  Removes support for a race search record type.
     *
     *  @param raceSearchRecordType a race search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceSearchRecordType</code> is <code>null</code>
     */

    protected void removeRaceSearchRecordType(org.osid.type.Type raceSearchRecordType) {
        this.raceSearchRecordTypes.remove(raceSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Ballot </code> record types. 
     *
     *  @return a list containing the supported <code> Ballot </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ballotRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Ballot </code> record type is supported. 
     *
     *  @param  ballotRecordType a <code> Type </code> indicating a <code> 
     *          Ballot </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ballotRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBallotRecordType(org.osid.type.Type ballotRecordType) {
        return (this.ballotRecordTypes.contains(ballotRecordType));
    }


    /**
     *  Adds support for a ballot record type.
     *
     *  @param ballotRecordType a ballot record type
     *  @throws org.osid.NullArgumentException
     *  <code>ballotRecordType</code> is <code>null</code>
     */

    protected void addBallotRecordType(org.osid.type.Type ballotRecordType) {
        this.ballotRecordTypes.add(ballotRecordType);
        return;
    }


    /**
     *  Removes support for a ballot record type.
     *
     *  @param ballotRecordType a ballot record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ballotRecordType</code> is <code>null</code>
     */

    protected void removeBallotRecordType(org.osid.type.Type ballotRecordType) {
        this.ballotRecordTypes.remove(ballotRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Ballot </code> search record types. 
     *
     *  @return a list containing the supported <code> Ballot </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ballotSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Ballot </code> search record type is 
     *  supported. 
     *
     *  @param  ballotSearchRecordType a <code> Type </code> indicating a 
     *          <code> Ballot </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ballotSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBallotSearchRecordType(org.osid.type.Type ballotSearchRecordType) {
        return (this.ballotSearchRecordTypes.contains(ballotSearchRecordType));
    }


    /**
     *  Adds support for a ballot search record type.
     *
     *  @param ballotSearchRecordType a ballot search record type
     *  @throws org.osid.NullArgumentException
     *  <code>ballotSearchRecordType</code> is <code>null</code>
     */

    protected void addBallotSearchRecordType(org.osid.type.Type ballotSearchRecordType) {
        this.ballotSearchRecordTypes.add(ballotSearchRecordType);
        return;
    }


    /**
     *  Removes support for a ballot search record type.
     *
     *  @param ballotSearchRecordType a ballot search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ballotSearchRecordType</code> is <code>null</code>
     */

    protected void removeBallotSearchRecordType(org.osid.type.Type ballotSearchRecordType) {
        this.ballotSearchRecordTypes.remove(ballotSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> VotingResults </code> record types. 
     *
     *  @return a list containing the supported <code> VotingResults </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVotingResultsRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.votingResultsRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> VotingResults </code> record type is 
     *  supported. 
     *
     *  @param  votingResultsRecordType a <code> Type </code> indicating a 
     *          <code> VotingResults </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> votingResultsRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVotingResultsRecordType(org.osid.type.Type votingResultsRecordType) {
        return (this.votingResultsRecordTypes.contains(votingResultsRecordType));
    }


    /**
     *  Adds support for a voting results record type.
     *
     *  @param votingResultsRecordType a voting results record type
     *  @throws org.osid.NullArgumentException
     *  <code>votingResultsRecordType</code> is <code>null</code>
     */

    protected void addVotingResultsRecordType(org.osid.type.Type votingResultsRecordType) {
        this.votingResultsRecordTypes.add(votingResultsRecordType);
        return;
    }


    /**
     *  Removes support for a voting results record type.
     *
     *  @param votingResultsRecordType a voting results record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>votingResultsRecordType</code> is <code>null</code>
     */

    protected void removeVotingResultsRecordType(org.osid.type.Type votingResultsRecordType) {
        this.votingResultsRecordTypes.remove(votingResultsRecordType);
        return;
    }


    /**
     *  Gets all the polls record types supported. 
     *
     *  @return the list of supported polls record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPollsRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.pollsRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given polls record type is supported. 
     *
     *  @param  pollsRecordType the polls record type 
     *  @return <code> true </code> if the polls record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pollsRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPollsRecordType(org.osid.type.Type pollsRecordType) {
        return (this.pollsRecordTypes.contains(pollsRecordType));
    }


    /**
     *  Adds support for a polls record type.
     *
     *  @param pollsRecordType a polls record type
     *  @throws org.osid.NullArgumentException
     *  <code>pollsRecordType</code> is <code>null</code>
     */

    protected void addPollsRecordType(org.osid.type.Type pollsRecordType) {
        this.pollsRecordTypes.add(pollsRecordType);
        return;
    }


    /**
     *  Removes support for a polls record type.
     *
     *  @param pollsRecordType a polls record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>pollsRecordType</code> is <code>null</code>
     */

    protected void removePollsRecordType(org.osid.type.Type pollsRecordType) {
        this.pollsRecordTypes.remove(pollsRecordType);
        return;
    }


    /**
     *  Gets all the polls search record types supported. 
     *
     *  @return the list of supported polls search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPollsSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.pollsSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given polls search record type is supported. 
     *
     *  @param  pollsSearchRecordType the polls search record type 
     *  @return <code> true </code> if the polls search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pollsSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPollsSearchRecordType(org.osid.type.Type pollsSearchRecordType) {
        return (this.pollsSearchRecordTypes.contains(pollsSearchRecordType));
    }


    /**
     *  Adds support for a polls search record type.
     *
     *  @param pollsSearchRecordType a polls search record type
     *  @throws org.osid.NullArgumentException
     *  <code>pollsSearchRecordType</code> is <code>null</code>
     */

    protected void addPollsSearchRecordType(org.osid.type.Type pollsSearchRecordType) {
        this.pollsSearchRecordTypes.add(pollsSearchRecordType);
        return;
    }


    /**
     *  Removes support for a polls search record type.
     *
     *  @param pollsSearchRecordType a polls search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>pollsSearchRecordType</code> is <code>null</code>
     */

    protected void removePollsSearchRecordType(org.osid.type.Type pollsSearchRecordType) {
        this.pollsSearchRecordTypes.remove(pollsSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voting 
     *  service. 
     *
     *  @return a <code> VotingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingSession getVotingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVotingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voting 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VotingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingSession getVotingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVotingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voting service 
     *  for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Pools </code> 
     *  @return a <code> VotingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoting() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingSession getVotingSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVotingSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voting service 
     *  for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> VotingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given Id 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoting() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingSession getVotingSessionForPolls(org.osid.id.Id pollsId, 
                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVotingSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race results 
     *  service. 
     *
     *  @return a <code> RaceResultsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceResults() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceResultsSession getRaceResultsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceResultsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race results 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceResultsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceResults() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceResultsSession getRaceResultsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceResultsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race results 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Pools </code> 
     *  @return a <code> RaceResultsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceResults() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceResultsSession getRaceResultsSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceResultsSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race results 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceResultsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given Id 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceResults() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceResultsSession getRaceResultsSessionForPolls(org.osid.id.Id pollsId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceResultsSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation lookup service. 
     *
     *  @return a <code> VoterAllocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationLookupSession getVoterAllocationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoterAllocationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VoterAllocationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationLookupSession getVoterAllocationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoterAllocationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoterAllocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given Id 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationLookupSession getVoterAllocationLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoterAllocationLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> VoterAllocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given Id 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationLookupSession getVoterAllocationLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoterAllocationLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation administrative service. 
     *
     *  @return a <code> VoterAllocationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationAdminSession getVoterAllocationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoterAllocationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VoterAllocationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationAdminSession getVoterAllocationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoterAllocationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation administrative service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoterAllocationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given Id 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationAdminSession getVoterAllocationAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoterAllocationAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the voter 
     *  allocation administrative service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> VoterAllocationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given Id 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterAllocationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoterAllocationAdminSession getVoterAllocationAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoterAllocationAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote lookup 
     *  service. 
     *
     *  @return a <code> VoteLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteLookupSession getVoteLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VoteLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteLookupSession getVoteLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote lookup 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteLookupSession getVoteLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoteLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote lookup 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> VoteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteLookupSession getVoteLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoteLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote query 
     *  service. 
     *
     *  @return a <code> VoteQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteQuerySession getVoteQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoteQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VoteQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteQuerySession getVoteQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoteQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote query 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoteQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteQuerySession getVoteQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoteQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote query 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> VoteQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteQuerySession getVoteQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoteQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote search 
     *  service. 
     *
     *  @return a <code> VoteSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSearchSession getVoteSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoteSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VoteSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSearchSession getVoteSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoteSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote search 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoteSearchSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSearchSession getVoteSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoteSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote search 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> VoteSearchSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVoteSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSearchSession getVoteSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoteSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote 
     *  notification service. 
     *
     *  @param  voteReceiver the notification callback 
     *  @return a <code> VoteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> voteReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoteNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteNotificationSession getVoteNotificationSession(org.osid.voting.VoteReceiver voteReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote 
     *  notification service. 
     *
     *  @param  voteReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> VoteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> voteReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoteNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteNotificationSession getVoteNotificationSession(org.osid.voting.VoteReceiver voteReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote 
     *  notification service for the given polls. 
     *
     *  @param  voteReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VoteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> voteReceiver </code> or 
     *          <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteNotificationSession getVoteNotificationSessionForPolls(org.osid.voting.VoteReceiver voteReceiver, 
                                                                                      org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoteNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the vote 
     *  notification service for the given polls. 
     *
     *  @param  voteReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> VoteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> voteReceiver, pollsId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteNotificationSession getVoteNotificationSessionForPolls(org.osid.voting.VoteReceiver voteReceiver, 
                                                                                      org.osid.id.Id pollsId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoteNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the session for retrieving vote to polls mappings. 
     *
     *  @return a <code> VotePollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVotePolls() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotePollsSession getVotePollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVotePollsSession not implemented");
    }


    /**
     *  Gets the session for retrieving vote to polls mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a Vote <code> PollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVotePolls() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotePollsSession getVotePollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVotePollsSession not implemented");
    }


    /**
     *  Gets the session for assigning votes to polls mappings. 
     *
     *  @return a <code> VotePollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotePollsAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotePollsAssignmentSession getVotePollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVotePollsAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning votes to polls mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VotePollsAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotePollsAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotePollsAssignmentSession getVotePollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVotePollsAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic vote polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return a <code> VoteSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoteSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSmartPollsSession getVoteSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVoteSmartPollsSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic vote polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @param  proxy a proxy 
     *  @return a <code> VoteSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoteSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSmartPollsSession getVoteSmartPollsSession(org.osid.id.Id pollsId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVoteSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the candidate 
     *  lookup service. 
     *
     *  @return <code> a CandidateLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateLookupSession getCandidateLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the candidate 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a CandidateLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateLookupSession getCandidateLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the candidate 
     *  lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateLookupSession getCandidateLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the candidate 
     *  lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @param  proxy a proxy 
     *  @return <code> a CandidateLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateLookupSession getCandidateLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateLookupSessionForPolls not implemented");
    }


    /**
     *  Gets a candidate query session. 
     *
     *  @return <code> a CandidateQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuerySession getCandidateQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateQuerySession not implemented");
    }


    /**
     *  Gets a candidate query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a CandidateQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuerySession getCandidateQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateQuerySession not implemented");
    }


    /**
     *  Gets a candidate query session for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuerySession getCandidateQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateQuerySessionForPolls not implemented");
    }


    /**
     *  Gets a candidate query session for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @param  proxy a proxy 
     *  @return <code> a CandidateQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuerySession getCandidateQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateQuerySessionForPolls not implemented");
    }


    /**
     *  Gets a candidate search session. 
     *
     *  @return <code> a CandidateSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSearchSession getCandidateSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateSearchSession not implemented");
    }


    /**
     *  Gets a candidate search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a CandidateSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSearchSession getCandidateSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateSearchSession not implemented");
    }


    /**
     *  Gets a candidate search session for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSearchSession getCandidateSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateSearchSessionForPolls not implemented");
    }


    /**
     *  Gets a candidate search session for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @param  proxy a proxy 
     *  @return <code> a CandidateSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSearchSession getCandidateSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateSearchSessionForPolls not implemented");
    }


    /**
     *  Gets a candidate administration session for creating, updating and 
     *  deleting candidates. 
     *
     *  @return <code> a CandidateAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateAdminSession getCandidateAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateAdminSession not implemented");
    }


    /**
     *  Gets a candidate administration session for creating, updating and 
     *  deleting candidates. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a CandidateAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateAdminSession getCandidateAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateAdminSession not implemented");
    }


    /**
     *  Gets a candidate administration session for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateAdminSession getCandidateAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateAdminSessionForPolls not implemented");
    }


    /**
     *  Gets a candidate administration session for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @param  proxy a proxy 
     *  @return <code> a CandidateAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateAdminSession getCandidateAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  candidate changes. 
     *
     *  @param  candidateReceiver the notification callback 
     *  @return <code> a CandidateNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> candidateReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateNotificationSession getCandidateNotificationSession(org.osid.voting.CandidateReceiver candidateReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  candidate changes. 
     *
     *  @param  candidateReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a CandidateNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> candidateReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateNotificationSession getCandidateNotificationSession(org.osid.voting.CandidateReceiver candidateReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateNotificationSession not implemented");
    }


    /**
     *  Gets the candidate notification session for the given polls. 
     *
     *  @param  candidateReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return <code> a CandidateNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> candidateReceiver 
     *          </code> or <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateNotificationSession getCandidateNotificationSessionForPolls(org.osid.voting.CandidateReceiver candidateReceiver, 
                                                                                                org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the candidate notification session for the given polls. 
     *
     *  @param  candidateReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @param  proxy a proxy 
     *  @return <code> a CandidateNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> candidateReceiver, 
     *          pollsId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateNotificationSession getCandidateNotificationSessionForPolls(org.osid.voting.CandidateReceiver candidateReceiver, 
                                                                                                org.osid.id.Id pollsId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the session for retrieving candidate to polls mappings. 
     *
     *  @return a <code> CandidatePollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidatePolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidatePollsSession getCandidatePollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidatePollsSession not implemented");
    }


    /**
     *  Gets the session for retrieving candidate to polls mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CandidatePollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidatePolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidatePollsSession getCandidatePollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidatePollsSession not implemented");
    }


    /**
     *  Gets the session for assigning candidate to polls mappings. 
     *
     *  @return a <code> CandidatePollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidatePollsAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidatePollsAssignmentSession getCandidatePollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidatePollsAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning candidate to polls mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CandidatePollsAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidatePollsAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidatePollsAssignmentSession getCandidatePollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidatePollsAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic candidate polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return a <code> CandidateSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSmartPollsSession getCandidateSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getCandidateSmartPollsSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic candidate polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @param  proxy a proxy 
     *  @return a <code> CandidateSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSmartPollsSession getCandidateSmartPollsSession(org.osid.id.Id pollsId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getCandidateSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race lookup 
     *  service. 
     *
     *  @return a <code> RaceLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceLookupSession getRaceLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceLookupSession getRaceLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race lookup 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceLookupSession getRaceLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race lookup 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceLookupSession getRaceLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race query 
     *  service. 
     *
     *  @return a <code> RaceQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuerySession getRaceQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuerySession getRaceQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race query 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuerySession getRaceQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race query 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuerySession getRaceQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race search 
     *  service. 
     *
     *  @return a <code> RaceSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceSearchSession getRaceSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceSearchSession getRaceSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race search 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceSearchSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supporstRaceSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSearchSession getRaceSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race search 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceSearchSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supporstRaceSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteSearchSession getRaceSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  administration service. 
     *
     *  @return a <code> RaceAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceAdminSession getRaceAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceAdminSession getRaceAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceAdminSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceAdminSession getRaceAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceAdminSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceAdminSession getRaceAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  notification service. 
     *
     *  @param  raceReceiver the notification callback 
     *  @return a <code> RaceNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> raceReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceNotificationSession getRaceNotificationSession(org.osid.voting.RaceReceiver raceReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  notification service. 
     *
     *  @param  raceReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RaceNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> raceReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceNotificationSession getRaceNotificationSession(org.osid.voting.RaceReceiver raceReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  notification service for the given polls. 
     *
     *  @param  raceReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> raceReceiver </code> or 
     *          <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceNotificationSession getRaceNotificationSessionForPolls(org.osid.voting.RaceReceiver raceReceiver, 
                                                                                      org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  notification service for the given polls. 
     *
     *  @param  raceReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> raceReceiver, pollsId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceNotificationSession getRaceNotificationSessionForPolls(org.osid.voting.RaceReceiver raceReceiver, 
                                                                                      org.osid.id.Id pollsId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the session for retrieving race to polls mappings. 
     *
     *  @return a <code> RacePollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRacePolls() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RacePollsSession getRacePollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRacePollsSession not implemented");
    }


    /**
     *  Gets the session for retrieving race to polls mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RacePollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRacePolls() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RacePollsSession getRacePollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRacePollsSession not implemented");
    }


    /**
     *  Gets the session for assigning race to polls mappings. 
     *
     *  @return a <code> RacePollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRacePollsAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RacePollsAssignmentSession getRacePollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRacePollsAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning race to polls mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RacePollsAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRacePollsAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RacePollsAssignmentSession getRacePollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRacePollsAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic race polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return a <code> RaceSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceSmartPollsSession getRaceSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getRaceSmartPollsSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic race polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @param  proxy a proxy 
     *  @return a <code> RaceSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceSmartPollsSession getRaceSmartPollsSession(org.osid.id.Id pollsId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getRaceSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot lookup 
     *  service. 
     *
     *  @return a <code> BallotLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotLookupSession getBallotLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotLookupSession getBallotLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot lookup 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotLookupSession getBallotLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot lookup 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotLookupSession getBallotLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot query 
     *  service. 
     *
     *  @return a <code> BallotQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuerySession getBallotQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuerySession getBallotQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot query 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuerySession getBallotQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot query 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuerySession getBallotQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot search 
     *  service. 
     *
     *  @return a <code> RaceSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSearchSession getBallotSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRaceSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSearchSession getBallotSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot search 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotSearchSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supporstBallotSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSearchSession getBallotSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot search 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotSearchSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supporstBallotSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSearchSession getBallotSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  administration service. 
     *
     *  @return a <code> BallotAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotAdminSession getBallotAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotAdminSession getBallotAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotAdminSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotAdminSession getBallotAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotAdminSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotAdminSession getBallotAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  notification service. 
     *
     *  @param  ballotReceiver the notification callback 
     *  @return a <code> BallotNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ballotReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotNotificationSession getBallotNotificationSession(org.osid.voting.BallotReceiver ballotReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  notification service. 
     *
     *  @param  ballotReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BallotNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ballotReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotNotificationSession getBallotNotificationSession(org.osid.voting.BallotReceiver ballotReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  notification service for the given polls. 
     *
     *  @param  ballotReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> ballotReceiver </code> 
     *          or <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotNotificationSession getBallotNotificationSessionForPolls(org.osid.voting.BallotReceiver ballotReceiver, 
                                                                                          org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  notification service for the given polls. 
     *
     *  @param  ballotReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> ballotReceiver, pollsId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotNotificationSession getBallotNotificationSessionForPolls(org.osid.voting.BallotReceiver ballotReceiver, 
                                                                                          org.osid.id.Id pollsId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the session for retrieving ballot to polls mappings. 
     *
     *  @return a <code> BallotPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotPolls() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotPollsSession getBallotPollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotPollsSession not implemented");
    }


    /**
     *  Gets the session for retrieving ballot to polls mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotPollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBallotPolls() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotPollsSession getBallotPollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotPollsSession not implemented");
    }


    /**
     *  Gets the session for assigning ballot to polls mappings. 
     *
     *  @return a <code> BallotPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotPollsAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotPollsAssignmentSession getBallotPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning ballot to polls mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotPollsAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotPollsAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotPollsAssignmentSession getBallotPollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic ballot polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @return a <code> BallotSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSmartPollsSession getBallotSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getBallotSmartPollsSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic ballot polls. 
     *
     *  @param  pollsId the <code> Id </code> of the polls 
     *  @param  proxy a proxy 
     *  @return a <code> BallotSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException <code> pollsId </code> not found 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotSmartPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSmartPollsSession getBallotSmartPollsSession(org.osid.id.Id pollsId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getBallotSmartPollsSession not implemented");
    }


    /**
     *  Gets the polls lookup session. 
     *
     *  @return a <code> PollsLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsLookupSession getPollsLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getPollsLookupSession not implemented");
    }


    /**
     *  Gets the polls lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PollsLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsLookupSession getPollsLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getPollsLookupSession not implemented");
    }


    /**
     *  Gets the polls query session. 
     *
     *  @return a <code> PollsQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuerySession getPollsQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getPollsQuerySession not implemented");
    }


    /**
     *  Gets the polls query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PollsQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuerySession getPollsQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getPollsQuerySession not implemented");
    }


    /**
     *  Gets the polls search session. 
     *
     *  @return a <code> PollsSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsSearchSession getPollsSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getPollsSearchSession not implemented");
    }


    /**
     *  Gets the polls search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PollsSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsSearchSession getPollsSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getPollsSearchSession not implemented");
    }


    /**
     *  Gets the polls administrative session for creating, updating and 
     *  deleteing polls. 
     *
     *  @return a <code> PollsAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsAdminSession getPollsAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getPollsAdminSession not implemented");
    }


    /**
     *  Gets the polls administrative session for creating, updating and 
     *  deleteing polls. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PollsAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPollsAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsAdminSession getPollsAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getPollsAdminSession not implemented");
    }


    /**
     *  Gets the notification session for subscripollsg to changes to a polls. 
     *
     *  @param  pollsReceiver the notification callback 
     *  @return a <code> PollsNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> pollsReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsNotificationSession getPollsNotificationSession(org.osid.voting.PollsReceiver pollsReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getPollsNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for subscripollsg to changes to a polls. 
     *
     *  @param  pollsReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PollsNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> pollsReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsNotificationSession getPollsNotificationSession(org.osid.voting.PollsReceiver pollsReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getPollsNotificationSession not implemented");
    }


    /**
     *  Gets the polls hierarchy traversal session. 
     *
     *  @return <code> a PollsHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsHierarchySession getPollsHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getPollsHierarchySession not implemented");
    }


    /**
     *  Gets the polls hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a PollsHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsHierarchySession getPollsHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getPollsHierarchySession not implemented");
    }


    /**
     *  Gets the polls hierarchy design session. 
     *
     *  @return a <code> PollsHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsHierarchyDesignSession getPollsHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getPollsHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the polls hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PollsHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsHierarchyDesignSession getPollsHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getPollsHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> VotingBatchManager. </code> 
     *
     *  @return a <code> VotingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVotingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.VotingBatchManager getVotingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVotingBatchManager not implemented");
    }


    /**
     *  Gets a <code> VotingBatchProxyManager. </code> 
     *
     *  @return a <code> VotingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVotingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.VotingBatchProxyManager getVotingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVotingBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> VotingRulesManager. </code> 
     *
     *  @return a <code> VotingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVotingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.VotingRulesManager getVotingRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingManager.getVotingRulesManager not implemented");
    }


    /**
     *  Gets a <code> VotingRulesProxyManager. </code> 
     *
     *  @return a <code> VotingRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVotingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.VotingRulesProxyManager getVotingRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.VotingProxyManager.getVotingRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.voteRecordTypes.clear();
        this.voteRecordTypes.clear();

        this.voteSearchRecordTypes.clear();
        this.voteSearchRecordTypes.clear();

        this.voterAllocationRecordTypes.clear();
        this.voterAllocationRecordTypes.clear();

        this.candidateRecordTypes.clear();
        this.candidateRecordTypes.clear();

        this.candidateSearchRecordTypes.clear();
        this.candidateSearchRecordTypes.clear();

        this.raceRecordTypes.clear();
        this.raceRecordTypes.clear();

        this.raceSearchRecordTypes.clear();
        this.raceSearchRecordTypes.clear();

        this.ballotRecordTypes.clear();
        this.ballotRecordTypes.clear();

        this.ballotSearchRecordTypes.clear();
        this.ballotSearchRecordTypes.clear();

        this.votingResultsRecordTypes.clear();
        this.votingResultsRecordTypes.clear();

        this.pollsRecordTypes.clear();
        this.pollsRecordTypes.clear();

        this.pollsSearchRecordTypes.clear();
        this.pollsSearchRecordTypes.clear();

        return;
    }
}
