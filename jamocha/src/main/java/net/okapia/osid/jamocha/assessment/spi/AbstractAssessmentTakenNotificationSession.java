//
// AbstractAssessmentTakenNotificationSession.java
//
//     A template for making AssessmentTakenNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code AssessmentTaken} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code AssessmentTaken} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for assessment taken entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractAssessmentTakenNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.assessment.AssessmentTakenNotificationSession {

    private boolean federated = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();


    /**
     *  Gets the {@code Bank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }

    
    /**
     *  Gets the {@code Bank} associated with this 
     *  session.
     *
     *  @return the {@code Bank} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the {@code Bank}.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException {@code bank}
     *          is {@code null}
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }


    /**
     *  Tests if this user can register for {@code AssessmentTaken}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a
     *  {@code PERMISSION_DENIED}. This is intended as a hint to
     *  an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForAssessmentTakenNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeAssessmentTakenNotification() </code>.
     */

    @OSID @Override
    public void reliableAssessmentTakenNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableAssessmentTakenNotifications() {
        return;
    }


    /**
     *  Acknowledge an assessment taken notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeAssessmentTakenNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for assessments taken in
     *  assessment banks which are children of this assessment bank in
     *  the assessment bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this assessment bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new assessments taken. {@code
     *  AssessmentTakenReceiver.newAssessmentTaken()} is invoked when
     *  an new {@code AssessmentTaken} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewAssessmentsTaken()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new assessments taken for a
     *  resource.  {@code
     *  AssessmentTakenReceiver.newAssessmentTaken()} is invoked when
     *  a new {@code AssessmentTaken} appears in this assessment bank.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentOfferedId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public void registerForNewAssessmentsTakenForTaker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new assessments taken for an
     *  assessment offered. {@code
     *  AssessmentTakenReceiver.newAssessmentTaken()} is invoked when
     *  a new {@code AssessmentTaken} appears in this assessment bank.
     *
     *  @param  assessmentOfferedId the {@code Id} of the {@code 
     *          AssessmentOffered} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *          assessmentOfferedId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public void registerForNewAssessmentsTakenForAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new assessments taken for an assessment. 
     *  {@code AssessmentTakenReceiver.newAssessmentTaken()} is invoked 
     *  when a new {@code AssessmentTaken} appears in this assessment 
     *  bank. 
     *
     *  @param  assessmentId the {@code Id} of the {@code Assessment 
     *         } to monitor 
     *  @throws org.osid.NullArgumentException {@code assessmentId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public void registerForNewAssessmentsTakenForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated
     *  assessments taken. {@code AssessmentTakenReceiver.changedAssessmentTaken()} is
     *  invoked when an assessment taken is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedAssessmentsTaken()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of changed assessments taken for a
     *  resource. {@code
     *  AssessmentTakenReceiver.changedAssessmentTaken()} is invoked
     *  when an {@code AssessmentTaken} is changed in this assessment
     *  bank.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentOfferedId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public void registerForChangedAssessmentsTakenForTaker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of changed assessments taken for an
     *  assessment offered. {@code
     *  AssessmentTakenReceiver.changedAssessmentTaken()} is invoked
     *  when an {@code AssessmentTaken} is changed in this assessment
     *  bank.
     *
     *  @param  assessmentOfferedId the {@code Id} of the {@code 
     *          AssessmentOffered} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentOfferedId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public void registerForChangedAssessmentsTakenForAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of changed assessments taken for an
     *  assessment. {@code
     *  AssessmentTakenReceiver.changedAssessmentTaken() } is invoked
     *  when an {@code AssessmentTaken} is changed in this assessment
     *  bank.
     *
     *  @param assessmentId the {@code Id} of the {@code Assessment}
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code assessmentId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public void registerForChangedAssessmentsTakenForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated assessment
     *  taken. {@code
     *  AssessmentTakenReceiver.changedAssessmentTaken()} is invoked
     *  when the specified assessment taken is changed.
     *
     *  @param assessmentTakenId the {@code Id} of the {@code AssessmentTaken} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code assessmentTakenId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedAssessmentTaken(org.osid.id.Id assessmentTakenId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted assessments
     *  taken. {@code
     *  AssessmentTakenReceiver.deletedAssessmentTaken()} is invoked
     *  when an assessment taken is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedAssessmentsTaken()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }

    
    /**
     *  Register for notifications of deleted assessments taken for a
     *  resource. {@code
     *  AssessmentTakenReceiver.deletedAssessmentTaken()} is invoked
     *  when an {@code AssessmentTaken} is removed from this
     *  assessment bank.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentOfferedId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public void registerForDeletedAssessmentsTakenForTaker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted assessments taken for an
     *  assessment offered. {@code
     *  AssessmentTakenReceiver.deletedAssessmentTaken()} is invoked
     *  when an {@code AssessmentTaken} is removed from this
     *  assessment bank.
     *
     *  @param  assessmentOfferedId the {@code Id} of the {@code 
     *          AssessmentOffered} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentOfferedId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public void registerForDeletedAssessmentsTakenForAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }

    
    /**
     *  Register for notifications of deleted assessments taken for an
     *  assessment. {@code
     *  AssessmentTakenReceiver.deletedAssessmentTaken() } is invoked
     *  when an {@code AssessmentTaken} is removed from this
     *  assessment bank.
     *
     *  @param assessmentId the {@code Id} of the {@code Assessment}
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code assessmentId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public void registerForDeletedAssessmentsTakenForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted assessment
     *  taken. {@code
     *  AssessmentTakenReceiver.deletedAssessmentTaken()} is invoked
     *  when the specified assessment taken is deleted.
     *
     *  @param assessmentTakenId the {@code Id} of the
     *          {@code AssessmentTaken} to monitor
     *  @throws org.osid.NullArgumentException {@code assessmentTakenId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedAssessmentTaken(org.osid.id.Id assessmentTakenId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
