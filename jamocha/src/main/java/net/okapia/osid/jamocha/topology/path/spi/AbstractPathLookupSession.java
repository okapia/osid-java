//
// AbstractPathLookupSession.java
//
//    A starter implementation framework for providing a Path
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Path lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPaths(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPathLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.topology.path.PathLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();
    

    /**
     *  Gets the <code>Graph/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this session.
     *
     *  @return the <code>Graph</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the <code>Graph</code>.
     *
     *  @param  graph the graph for this session
     *  @throws org.osid.NullArgumentException <code>graph</code>
     *          is <code>null</code>
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can perform <code>Path</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPaths() {
        return (true);
    }


    /**
     *  A complete view of the <code>Path</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePathView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Path</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPathView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include paths in graphs which are children of this
     *  graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only paths whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectivePathView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All paths of any effective dates are returned by all methods
     *  in this session.
     */

    @OSID @Override
    public void useAnyEffectivePathView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Path</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Path</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Path</code> and retained for
     *  compatibility.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  pathId <code>Id</code> of the
     *          <code>Path</code>
     *  @return the path
     *  @throws org.osid.NotFoundException <code>pathId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pathId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.Path getPath(org.osid.id.Id pathId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.topology.path.PathList paths = getPaths()) {
            while (paths.hasNext()) {
                org.osid.topology.path.Path path = paths.getNextPath();
                if (path.getId().equals(pathId)) {
                    return (path);
                }
            }
        } 

        throw new org.osid.NotFoundException(pathId + " not found");
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the paths
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Paths</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPaths()</code>.
     *
     *  @param pathIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>pathIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByIds(org.osid.id.IdList pathIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.topology.path.Path> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = pathIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPath(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("path " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.topology.path.path.LinkedPathList(ret));
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given path
     *  genus <code>Type</code> which does not include paths of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPaths()</code>.
     *
     *  @param pathGenusType a path genus type
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathGenusFilterList(getPaths(), pathGenusType));
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given path
     *  genus <code>Type</code> and include any additional paths with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPaths()</code>.
     *
     *  @param pathGenusType a path genus type
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByParentGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPathsByGenusType(pathGenusType));
    }


    /**
     *  Gets a <code>PathList</code> containing the given path record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPaths()</code>.
     *
     *  @param  pathRecordType a path record type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByRecordType(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathRecordFilterList(getPaths(), pathRecordType));
    }


    /**
     *  Gets a <code>PathList</code> effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *  
     *  In active mode, paths are returned that are currently
     *  active. In any status mode, active and inactive paths are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Path</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.path.PathList getPathsOnDate(org.osid.calendaring.DateTime from, 
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.TemporalPathFilterList(getPaths(), from, to));
    }
        

    /**
     *  Gets a <code>PathList</code> effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *  
     *  In active mode, paths are returned that are currently
     *  active. In any status mode, active and inactive paths are
     *  returned.
     *
     *  @param pathGenusType a path genus type
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Path</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeOnDate(org.osid.type.Type pathGenusType,
                                                                     org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathGenusFilterList(getPathsOnDate(from, to), pathGenusType));
    }


    /**
     *  Gets a list of paths corresponding to a starting node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.path.PathList getPathsForStartingNode(org.osid.id.Id startingNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathFilterList(new StartingNodeFilter(startingNodeId), getPaths()));
    }


    /**
     *  Gets a list of paths corresponding to a starting node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param startingNodeId the <code>Id</code> of the starting node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForStartingNodeOnDate(org.osid.id.Id startingNodeId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.TemporalPathFilterList(getPathsForStartingNode(startingNodeId), from, to));
    }


    /**
     *  Gets a list of paths of a genus type corresponding to a
     *  starting node <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @param pathGenusType a path genus type
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingNodeId</code> or
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForStartingNode(org.osid.id.Id startingNodeId,
                                                                              org.osid.type.Type pathGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathGenusFilterList(getPathsForStartingNode(startingNodeId), pathGenusType));
    }


    /**
     *  Gets a list of paths of a genus type corresponding to a
     *  starting node <code>Id</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param startingNodeId the <code>Id</code> of the starting node
     *  @param pathGenusType a path genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingNodeId</code>,
     *          <code>pathGenusType</code> <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForStartingNodeOnDate(org.osid.id.Id startingNodeId,
                                                                                    org.osid.type.Type pathGenusType,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathGenusFilterList(getPathsForStartingNodeOnDate(startingNodeId, from, to), pathGenusType));
    }


    /**
     *  Gets a list of paths corresponding to a ending node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>endingNodeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.path.PathList getPathsForEndingNode(org.osid.id.Id endingNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathFilterList(new EndingNodeFilter(endingNodeId), getPaths()));
    }


    /**
     *  Gets a list of paths corresponding to a ending node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param endingNodeId the <code>Id</code> of the ending node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>endingNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForEndingNodeOnDate(org.osid.id.Id endingNodeId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.TemporalPathFilterList(getPathsForEndingNode(endingNodeId), from, to));
    }


    /**
     *  Gets a list of paths of a genus type corresponding to a
     *  ending node <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @param pathGenusType a path genus type
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>endingNodeId</code> or
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForEndingNode(org.osid.id.Id endingNodeId,
                                                                            org.osid.type.Type pathGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathGenusFilterList(getPathsForEndingNode(endingNodeId), pathGenusType));
    }


    /**
     *  Gets a list of paths of a genus type corresponding to a
     *  ending node <code>Id</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param endingNodeId the <code>Id</code> of the ending node
     *  @param pathGenusType a path genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>endingNodeId</code>,
     *          <code>pathGenusType</code> <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForEndingNodeOnDate(org.osid.id.Id endingNodeId,
                                                                                  org.osid.type.Type pathGenusType,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathGenusFilterList(getPathsForEndingNodeOnDate(endingNodeId, from, to), pathGenusType));
    }


    /**
     *  Gets a list of paths corresponding to starting node and ending
     *  node <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code>,
     *          <code>endingNodeId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForNodes(org.osid.id.Id startingNodeId,
                                                            org.osid.id.Id endingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathFilterList(new EndingNodeFilter(endingNodeId), getPathsForStartingNode(startingNodeId)));
    }


    /**
     *  Gets a list of paths corresponding to starting node and ending
     *  node <code>Ids</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param endingNodeId the <code>Id</code> of the ending node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code>,
     *          <code>endingNodeId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForNodesOnDate(org.osid.id.Id startingNodeId,
                                                                  org.osid.id.Id endingNodeId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.TemporalPathFilterList(getPathsForNodes(startingNodeId, endingNodeId), from, to));
    }


    /**
     *  Gets a list of paths of a genus type corresponding to starting
     *  node and ending node <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @param pathGenusType a path genus type
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingNodeId</code>,
     *          <code>endingNodeId</code>, <code>pathGenusType</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForNodes(org.osid.id.Id startingNodeId,
                                                                       org.osid.id.Id endingNodeId,
                                                                       org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.PathGenusFilterList(getPathsForNodes(startingNodeId, endingNodeId), pathGenusType));
    }


    /**
     *  Gets a list of paths of a genus type corresponding to starting
     *  node and ending node <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param startingNodeId the <code>Id</code> of the starting node
     *  @param endingNodeId the <code>Id</code> of the ending node
     *  @param pathGenusType a path genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingNodeId</code>,
     *          <code>endingNodeId</code>, <code>pathGenusType</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForNodesOnDate(org.osid.id.Id startingNodeId,
                                                                             org.osid.id.Id endingNodeId,
                                                                             org.osid.type.Type pathGenusType,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.TemporalPathFilterList(getPathsByGenusTypeForNodes(startingNodeId, endingNodeId, pathGenusType), from, to));
    }


    /**
     *  Gets a <code>PathList</code> connected to all the given 
     *  <code>Nodes</code>.
     *  
     *  In plenary mode, the returned list contains all of the paths
     *  through the nodes, or an error results if a path connected to
     *  the node is not found or inaccessible. Otherwise, inaccessible
     *  <code> Paths </code> may be omitted from the list.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  nodeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Path</code> list 
     *  @throws org.osid.NullArgumentException <code>nodeIds</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsAlongNodes(org.osid.id.IdList nodeIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.topology.path.Path> ret = new java.util.ArrayList<>();
        java.util.Collection<org.osid.id.Id> nodes = new java.util.HashSet<>();

        try (org.osid.id.IdList ids = nodeIds) {
            while (ids.hasNext()) {
                nodes.add(ids.getNextId());
            }
        }

        try (org.osid.topology.path.PathList paths = getPaths()) {
            while (paths.hasNext()) {
                org.osid.topology.path.Path path = paths.getNextPath();
                java.util.Collection<org.osid.id.Id> pnodes = new java.util.HashSet<>();
                try (org.osid.topology.EdgeList edges = path.getEdges()) {
                    while (edges.hasNext()) {
                        org.osid.topology.Edge edge = edges.getNextEdge();
                        pnodes.add(edge.getSourceNodeId());
                        pnodes.add(edge.getDestinationNodeId());
                    }
                }

                boolean found = true;                
                for (org.osid.id.Id id : nodes) {
                    if (!pnodes.contains(id)) {
                        found = false;
                        break;
                    }
                }

                if (found) {
                    ret.add(path);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.topology.path.path.LinkedPathList(ret));
    }


    /**
     *  Gets a <code>PathList</code> connected to all the given
     *  <code>Nodes</code> and and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all of the paths
     *  through the nodes, or an error results if a path connected to
     *  the node is not found or inaccessible. Otherwise, inaccessible
     *  <code> Paths </code> may be omitted from the list.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  nodeIds the list of <code>Ids</code> to retrieve 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Path</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>nodeIds</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsAlongNodesOnDate(org.osid.id.IdList nodeIds, 
                                                                    org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.path.path.TemporalPathFilterList(getPathsAlongNodes(nodeIds), from, to));
    }

    
    /**
     *  Gets all <code>Paths</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Paths</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.topology.path.PathList getPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the path list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of paths
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.topology.path.PathList filterPathsOnViews(org.osid.topology.path.PathList list)
        throws org.osid.OperationFailedException {

        org.osid.topology.path.PathList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.topology.path.path.EffectivePathFilterList(ret);
        }

        return (ret);
    }


    public static class StartingNodeFilter
        implements net.okapia.osid.jamocha.inline.filter.topology.path.path.PathFilter {
         
        private final org.osid.id.Id startingNodeId;
         
         
        /**
         *  Constructs a new <code>StartingNodeFilter</code>.
         *
         *  @param startingNodeId the starting node to filter
         *  @throws org.osid.NullArgumentException
         *          <code>startingNodeId</code> is <code>null</code>
         */
        
        public StartingNodeFilter(org.osid.id.Id startingNodeId) {
            nullarg(startingNodeId, "starting node Id");
            this.startingNodeId = startingNodeId;
            return;
        }

         
        /**
         *  Used by the PathFilterList to filter the 
         *  path list based on starting node.
         *
         *  @param path the path
         *  @return <code>true</code> to pass the path,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.topology.path.Path path) {
            return (path.getStartingNodeId().equals(this.startingNodeId));
        }
    }


    public static class EndingNodeFilter
        implements net.okapia.osid.jamocha.inline.filter.topology.path.path.PathFilter {
         
        private final org.osid.id.Id endingNodeId;
         
         
        /**
         *  Constructs a new <code>EndingNodeFilter</code>.
         *
         *  @param endingNodeId the ending node to filter
         *  @throws org.osid.NullArgumentException
         *          <code>endingNodeId</code> is <code>null</code>
         */
        
        public EndingNodeFilter(org.osid.id.Id endingNodeId) {
            nullarg(endingNodeId, "ending node Id");
            this.endingNodeId = endingNodeId;
            return;
        }

         
        /**
         *  Used by the PathFilterList to filter the 
         *  path list based on ending node.
         *
         *  @param path the path
         *  @return <code>true</code> to pass the path,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.topology.path.Path path) {
            return (path.getEndingNodeId().equals(this.endingNodeId));
        }
    }
}
