//
// AbstractStepConstrainerEnablerQueryInspector.java
//
//     A template for making a StepConstrainerEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for step constrainer enablers.
 */

public abstract class AbstractStepConstrainerEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.workflow.rules.StepConstrainerEnablerQueryInspector {

    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the step constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledStepConstrainerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the step constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQueryInspector[] getRuledStepConstrainerTerms() {
        return (new org.osid.workflow.rules.StepConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given step constrainer enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a step constrainer enabler implementing the requested record.
     *
     *  @param stepConstrainerEnablerRecordType a step constrainer enabler record type
     *  @return the step constrainer enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerEnablerQueryInspectorRecord getStepConstrainerEnablerQueryInspectorRecord(org.osid.type.Type stepConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(stepConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step constrainer enabler query. 
     *
     *  @param stepConstrainerEnablerQueryInspectorRecord step constrainer enabler query inspector
     *         record
     *  @param stepConstrainerEnablerRecordType stepConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepConstrainerEnablerQueryInspectorRecord(org.osid.workflow.rules.records.StepConstrainerEnablerQueryInspectorRecord stepConstrainerEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type stepConstrainerEnablerRecordType) {

        addRecordType(stepConstrainerEnablerRecordType);
        nullarg(stepConstrainerEnablerRecordType, "step constrainer enabler record type");
        this.records.add(stepConstrainerEnablerQueryInspectorRecord);        
        return;
    }
}
