//
// AbstractAdapterSpeedZoneLookupSession.java
//
//    A SpeedZone lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A SpeedZone lookup session adapter.
 */

public abstract class AbstractAdapterSpeedZoneLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.path.SpeedZoneLookupSession {

    private final org.osid.mapping.path.SpeedZoneLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSpeedZoneLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSpeedZoneLookupSession(org.osid.mapping.path.SpeedZoneLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code SpeedZone} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSpeedZones() {
        return (this.session.canLookupSpeedZones());
    }


    /**
     *  A complete view of the {@code SpeedZone} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSpeedZoneView() {
        this.session.useComparativeSpeedZoneView();
        return;
    }


    /**
     *  A complete view of the {@code SpeedZone} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySpeedZoneView() {
        this.session.usePlenarySpeedZoneView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include speed zones in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only active speed zones are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSpeedZoneView() {
        this.session.useActiveSpeedZoneView();
        return;
    }


    /**
     *  Active and inactive speed zones are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSpeedZoneView() {
        this.session.useAnyStatusSpeedZoneView();
        return;
    }
    
     
    /**
     *  Gets the {@code SpeedZone} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code SpeedZone} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code SpeedZone} and
     *  retained for compatibility.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @param speedZoneId {@code Id} of the {@code SpeedZone}
     *  @return the speed zone
     *  @throws org.osid.NotFoundException {@code speedZoneId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code speedZoneId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZone getSpeedZone(org.osid.id.Id speedZoneId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZone(speedZoneId));
    }


    /**
     *  Gets a {@code SpeedZoneList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  speedZones specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code SpeedZones} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @param  speedZoneIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code SpeedZone} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByIds(org.osid.id.IdList speedZoneIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZonesByIds(speedZoneIds));
    }


    /**
     *  Gets a {@code SpeedZoneList} corresponding to the given
     *  speed zone genus {@code Type} which does not include
     *  speed zones of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  speed zones or an error results. Otherwise, the returned list
     *  may contain only those speed zones that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @param  speedZoneGenusType a speedZone genus type 
     *  @return the returned {@code SpeedZone} list
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByGenusType(org.osid.type.Type speedZoneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZonesByGenusType(speedZoneGenusType));
    }


    /**
     *  Gets a {@code SpeedZoneList} corresponding to the given
     *  speed zone genus {@code Type} and include any additional
     *  speed zones with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  speed zones or an error results. Otherwise, the returned list
     *  may contain only those speed zones that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @param  speedZoneGenusType a speedZone genus type 
     *  @return the returned {@code SpeedZone} list
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByParentGenusType(org.osid.type.Type speedZoneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZonesByParentGenusType(speedZoneGenusType));
    }


    /**
     *  Gets a {@code SpeedZoneList} containing the given
     *  speed zone record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  speed zones or an error results. Otherwise, the returned list
     *  may contain only those speed zones that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @param  speedZoneRecordType a speedZone record type 
     *  @return the returned {@code SpeedZone} list
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByRecordType(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZonesByRecordType(speedZoneRecordType));
    }


    /**
     *  Gets a {@code SpeedZoneList} containing the given path.
     *  
     *  In plenary mode, the returned list contains all known spwed
     *  zones or an error results. Otherwise, the returned list may
     *  contain only those speed zones that are accessible through
     *  this session.
     *  
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @param  pathId a path {@code Id} 
     *  @return the returned {@code SpeedZone} list 
     *  @throws org.osid.NullArgumentException {@code pathId} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZonesForPath(pathId));
    }


    /**
     *  Gets a {@code SpeedZoneList} containing the given path between
     *  the given coordinates inclusive.
     *  
     *  In plenary mode, the returned list contains all known speed
     *  zones or an error results. Otherwise, the returned list may
     *  contain only those speed zones that are accessible through
     *  this session.
     *  
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @param  pathId a path {@code Id} 
     *  @param  coordinate starting coordinate 
     *  @param  distance a distance from coordinate 
     *  @return the returned {@code SpeedZone} list 
     *  @throws org.osid.NullArgumentException {@code pathId,
     *         coordinate}, or {@code distance} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesForPathAtCoordinate(org.osid.id.Id pathId, 
                                                                                org.osid.mapping.Coordinate coordinate, 
                                                                                org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZonesForPathAtCoordinate(pathId, coordinate, distance));
    }

    
    /**
     *  Gets all {@code SpeedZones}. 
     *
     *  In plenary mode, the returned list contains all known
     *  speed zones or an error results. Otherwise, the returned list
     *  may contain only those speed zones that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zones are returned that are currently
     *  active. In any status mode, active and inactive speed zones
     *  are returned.
     *
     *  @return a list of {@code SpeedZones} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZones()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZones());
    }
}
