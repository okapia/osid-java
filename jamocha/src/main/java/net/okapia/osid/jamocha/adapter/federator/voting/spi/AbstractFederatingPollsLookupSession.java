//
// AbstractFederatingPollsLookupSession.java
//
//     An abstract federating adapter for a PollsLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PollsLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPollsLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.voting.PollsLookupSession>
    implements org.osid.voting.PollsLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingPollsLookupSession</code>.
     */

    protected AbstractFederatingPollsLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.voting.PollsLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Polls</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPolls() {
        for (org.osid.voting.PollsLookupSession session : getSessions()) {
            if (session.canLookupPolls()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Polls</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePollsView() {
        for (org.osid.voting.PollsLookupSession session : getSessions()) {
            session.useComparativePollsView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Polls</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPollsView() {
        for (org.osid.voting.PollsLookupSession session : getSessions()) {
            session.usePlenaryPollsView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Polls</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Polls</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Polls</code> and
     *  retained for compatibility.
     *
     *  @param  pollsId <code>Id</code> of the
     *          <code>Polls</code>
     *  @return the polls
     *  @throws org.osid.NotFoundException <code>pollsId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pollsId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.voting.PollsLookupSession session : getSessions()) {
            try {
                return (session.getPolls(pollsId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(pollsId + " not found");
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  pollses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Polls</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  pollsIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>pollsIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByIds(org.osid.id.IdList pollsIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.voting.polls.MutablePollsList ret = new net.okapia.osid.jamocha.voting.polls.MutablePollsList();

        try (org.osid.id.IdList ids = pollsIds) {
            while (ids.hasNext()) {
                ret.addPolls(getPolls(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given
     *  polls genus <code>Type</code> which does not include
     *  pollses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  pollses or an error results. Otherwise, the returned list
     *  may contain only those pollses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.polls.FederatingPollsList ret = getPollsList();

        for (org.osid.voting.PollsLookupSession session : getSessions()) {
            ret.addPollsList(session.getPollsByGenusType(pollsGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given
     *  polls genus <code>Type</code> and include any additional
     *  pollses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  pollses or an error results. Otherwise, the returned list
     *  may contain only those pollses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByParentGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.polls.FederatingPollsList ret = getPollsList();

        for (org.osid.voting.PollsLookupSession session : getSessions()) {
            ret.addPollsList(session.getPollsByParentGenusType(pollsGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PollsList</code> containing the given
     *  polls record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  pollses or an error results. Otherwise, the returned list
     *  may contain only those pollses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pollsRecordType a polls record type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByRecordType(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.polls.FederatingPollsList ret = getPollsList();

        for (org.osid.voting.PollsLookupSession session : getSessions()) {
            ret.addPollsList(session.getPollsByRecordType(pollsRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PollsList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known pollses or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  pollses that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Polls</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.voting.polls.FederatingPollsList ret = getPollsList();

        for (org.osid.voting.PollsLookupSession session : getSessions()) {
            ret.addPollsList(session.getPollsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Polls</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  pollses or an error results. Otherwise, the returned list
     *  may contain only those pollses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Polls</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getAllPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.polls.FederatingPollsList ret = getPollsList();

        for (org.osid.voting.PollsLookupSession session : getSessions()) {
            ret.addPollsList(session.getAllPolls());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.voting.polls.FederatingPollsList getPollsList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.polls.ParallelPollsList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.polls.CompositePollsList());
        }
    }
}
