//
// AbstractGradeSystemTransformLookupSession.java
//
//    A starter implementation framework for providing a GradeSystemTransform
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.transform.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a GradeSystemTransform
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getGradeSystemTransforms(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractGradeSystemTransformLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.grading.transform.GradeSystemTransformLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.grading.Gradebook gradebook = new net.okapia.osid.jamocha.nil.grading.gradebook.UnknownGradebook();
    

    /**
     *  Gets the <code>Gradebook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Gradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.gradebook.getId());
    }


    /**
     *  Gets the <code>Gradebook</code> associated with this 
     *  session.
     *
     *  @return the <code>Gradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.gradebook);
    }


    /**
     *  Sets the <code>Gradebook</code>.
     *
     *  @param  gradebook the gradebook for this session
     *  @throws org.osid.NullArgumentException <code>gradebook</code>
     *          is <code>null</code>
     */

    protected void setGradebook(org.osid.grading.Gradebook gradebook) {
        nullarg(gradebook, "gradebook");
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Tests if this user can perform <code>GradeSystemTransform</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradeSystemTransforms() {
        return (true);
    }


    /**
     *  A complete view of the <code>GradeSystemTransform</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradeSystemTransformView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>GradeSystemTransform</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradeSystemTransformView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade system transforms in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active grade system transforms are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveGradeSystemTransformView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive grade system transforms are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusGradeSystemTransformView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>GradeSystemTransform</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradeSystemTransform</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>GradeSystemTransform</code> and
     *  retained for compatibility.
     *
     *  In active mode, grade system transforms are returned that are currently
     *  active. In any status mode, active and inactive grade system transforms
     *  are returned.
     *
     *  @param  gradeSystemTransformId <code>Id</code> of the
     *          <code>GradeSystemTransform</code>
     *  @return the grade system transform
     *  @throws org.osid.NotFoundException <code>gradeSystemTransformId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransformId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransform getGradeSystemTransform(org.osid.id.Id gradeSystemTransformId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.grading.transform.GradeSystemTransformList gradeSystemTransforms = getGradeSystemTransforms()) {
            while (gradeSystemTransforms.hasNext()) {
                org.osid.grading.transform.GradeSystemTransform gradeSystemTransform = gradeSystemTransforms.getNextGradeSystemTransform();
                if (gradeSystemTransform.getId().equals(gradeSystemTransformId)) {
                    return (gradeSystemTransform);
                }
            }
        } 

        throw new org.osid.NotFoundException(gradeSystemTransformId + " not found");
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeSystemTransforms specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>GradeSystemTransforms</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getGradeSystemTransforms()</code>.
     *
     *  @param  gradeSystemTransformIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByIds(org.osid.id.IdList gradeSystemTransformIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.grading.transform.GradeSystemTransform> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = gradeSystemTransformIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getGradeSystemTransform(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("grade system transform " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.grading.transform.gradesystemtransform.LinkedGradeSystemTransformList(ret));
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> corresponding to
     *  the given grade system transform genus <code>Type</code> which
     *  does not include grade system transforms of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  system transforms or an error results. Otherwise, the returned
     *  list may contain only those grade system transforms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getGradeSystemTransforms()</code>.
     *
     *  @param  gradeSystemTransformGenusType a gradeSystemTransform genus type 
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByGenusType(org.osid.type.Type gradeSystemTransformGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.transform.gradesystemtransform.GradeSystemTransformGenusFilterList(getGradeSystemTransforms(), gradeSystemTransformGenusType));
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> corresponding to
     *  the given grade system transform genus <code>Type</code> and
     *  include any additional grade system transforms with genus
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  system transforms or an error results. Otherwise, the returned
     *  list may contain only those grade system transforms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGradeSystemTransforms()</code>.
     *
     *  @param  gradeSystemTransformGenusType a gradeSystemTransform genus type 
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByParentGenusType(org.osid.type.Type gradeSystemTransformGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getGradeSystemTransformsByGenusType(gradeSystemTransformGenusType));
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> containing the
     *  given grade system transform record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known grade
     *  system transforms or an error results. Otherwise, the returned
     *  list may contain only those grade system transforms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGradeSystemTransforms()</code>.
     *
     *  @param  gradeSystemTransformRecordType a gradeSystemTransform record type 
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByRecordType(org.osid.type.Type gradeSystemTransformRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.transform.gradesystemtransform.GradeSystemTransformRecordFilterList(getGradeSystemTransforms(), gradeSystemTransformRecordType));
    }

    
    /**
     *  Gets the grade system transforms from the source grade
     *  system. In plenary mode, the returned list contains all known
     *  grade system transforms or an error results. Otherwise, the
     *  returned list may contain only those grade system transforms
     *  that are accessible through this session.
     *
     *  @param  sourceGradeSystemId the source grade system 
     *  @return the returned <code>GradeSystemTransform</code> list 
     *  @throws org.osid.NotFoundException
     *          <code>sourceGradeSystemId</code> not found
     *  @throws org.osid.NullArgumentException
     *          <code>sourceGradeSystemId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsBySource(org.osid.id.Id sourceGradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.transform.gradesystemtransform.GradeSystemTransformFilterList(new SourceFilter(sourceGradeSystemId), getGradeSystemTransforms()));
    }        


    /**
     *  Gets a grade system transform by its source and target grade
     *  systems.  In plenary mode, the returned list contains all
     *  known grade system transforms or an error results. Otherwise,
     *  the returned list may contain only those grade system
     *  transforms that are accessible through this session.
     *
     *  @param  sourceGradeSystemId the source grade system 
     *  @param  targetGradeSystemId the target grade system 
     *  @return the returned <code>GradeSystemTransform</code> 
     *  @throws org.osid.NotFoundException transform not found 
     *  @throws org.osid.NullArgumentException
     *          <code>sourceGradeSystemId</code> or
     *          <code>targetGradeSystemId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransform getGradeSystemTransformBySystems(org.osid.id.Id sourceGradeSystemId, 
                                                                                            org.osid.id.Id targetGradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.grading.transform.GradeSystemTransformList gradeSystemTransforms = getGradeSystemTransforms()) {
            while (gradeSystemTransforms.hasNext()) {
                org.osid.grading.transform.GradeSystemTransform gradeSystemTransform = gradeSystemTransforms.getNextGradeSystemTransform();
                if (gradeSystemTransform.getSourceGradeSystemId().equals(sourceGradeSystemId) &&
                    gradeSystemTransform.getTargetGradeSystemId().equals(targetGradeSystemId)) {
                    return (gradeSystemTransform);
                }
            }
        } 

        throw new org.osid.NotFoundException("transform  not found");
    }
    

    /**
     *  Gets all <code>GradeSystemTransforms</code>. 
     *
     *  In plenary mode, the returned list contains all known grade
     *  system transforms or an error results. Otherwise, the returned
     *  list may contain only those grade system transforms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  @return a list of <code>GradeSystemTransforms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransforms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the grade system transform list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of grade system transforms
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.grading.transform.GradeSystemTransformList filterGradeSystemTransformsOnViews(org.osid.grading.transform.GradeSystemTransformList list)
        throws org.osid.OperationFailedException {

        org.osid.grading.transform.GradeSystemTransformList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.grading.transform.gradesystemtransform.ActiveGradeSystemTransformFilterList(ret);
        }

        return (ret);
    }


    public static class SourceFilter
        implements net.okapia.osid.jamocha.inline.filter.grading.transform.gradesystemtransform.GradeSystemTransformFilter {

        private final org.osid.id.Id sourceId;

        
        /**
         *  Constructs a new <code>SourceFilter</code>.
         *
         *  @param sourceId the source grade system to filter
         *  @throws org.osid.NullArgumentException
         *          <code>sourceId</code> is <code>null</code>
         */

        public SourceFilter(org.osid.id.Id sourceId) {
            nullarg(sourceId, "source Id");
            this.sourceId = sourceId;
            return;
        }


        /**
         *  Used by the GradeSystemTransformFilterList to filter the
         *  grade system transform list based on source.
         *
         *  @param transform the grade system transform
         *  @return <code>true</code> to pass the transform,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.grading.transform.GradeSystemTransform transform) {
            return (transform.getSourceGradeSystemId().equals(this.sourceId));
        }
    }    
}
