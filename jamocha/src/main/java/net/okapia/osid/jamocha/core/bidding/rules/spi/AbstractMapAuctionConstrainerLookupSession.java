//
// AbstractMapAuctionConstrainerLookupSession
//
//    A simple framework for providing an AuctionConstrainer lookup service
//    backed by a fixed collection of auction constrainers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AuctionConstrainer lookup service backed by a
 *  fixed collection of auction constrainers. The auction constrainers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuctionConstrainerLookupSession
    extends net.okapia.osid.jamocha.bidding.rules.spi.AbstractAuctionConstrainerLookupSession
    implements org.osid.bidding.rules.AuctionConstrainerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.bidding.rules.AuctionConstrainer> auctionConstrainers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.bidding.rules.AuctionConstrainer>());


    /**
     *  Makes an <code>AuctionConstrainer</code> available in this session.
     *
     *  @param  auctionConstrainer an auction constrainer
     *  @throws org.osid.NullArgumentException <code>auctionConstrainer<code>
     *          is <code>null</code>
     */

    protected void putAuctionConstrainer(org.osid.bidding.rules.AuctionConstrainer auctionConstrainer) {
        this.auctionConstrainers.put(auctionConstrainer.getId(), auctionConstrainer);
        return;
    }


    /**
     *  Makes an array of auction constrainers available in this session.
     *
     *  @param  auctionConstrainers an array of auction constrainers
     *  @throws org.osid.NullArgumentException <code>auctionConstrainers<code>
     *          is <code>null</code>
     */

    protected void putAuctionConstrainers(org.osid.bidding.rules.AuctionConstrainer[] auctionConstrainers) {
        putAuctionConstrainers(java.util.Arrays.asList(auctionConstrainers));
        return;
    }


    /**
     *  Makes a collection of auction constrainers available in this session.
     *
     *  @param  auctionConstrainers a collection of auction constrainers
     *  @throws org.osid.NullArgumentException <code>auctionConstrainers<code>
     *          is <code>null</code>
     */

    protected void putAuctionConstrainers(java.util.Collection<? extends org.osid.bidding.rules.AuctionConstrainer> auctionConstrainers) {
        for (org.osid.bidding.rules.AuctionConstrainer auctionConstrainer : auctionConstrainers) {
            this.auctionConstrainers.put(auctionConstrainer.getId(), auctionConstrainer);
        }

        return;
    }


    /**
     *  Removes an AuctionConstrainer from this session.
     *
     *  @param  auctionConstrainerId the <code>Id</code> of the auction constrainer
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerId<code> is
     *          <code>null</code>
     */

    protected void removeAuctionConstrainer(org.osid.id.Id auctionConstrainerId) {
        this.auctionConstrainers.remove(auctionConstrainerId);
        return;
    }


    /**
     *  Gets the <code>AuctionConstrainer</code> specified by its <code>Id</code>.
     *
     *  @param  auctionConstrainerId <code>Id</code> of the <code>AuctionConstrainer</code>
     *  @return the auctionConstrainer
     *  @throws org.osid.NotFoundException <code>auctionConstrainerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainer getAuctionConstrainer(org.osid.id.Id auctionConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.bidding.rules.AuctionConstrainer auctionConstrainer = this.auctionConstrainers.get(auctionConstrainerId);
        if (auctionConstrainer == null) {
            throw new org.osid.NotFoundException("auctionConstrainer not found: " + auctionConstrainerId);
        }

        return (auctionConstrainer);
    }


    /**
     *  Gets all <code>AuctionConstrainers</code>. In plenary mode, the returned
     *  list contains all known auctionConstrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  auctionConstrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AuctionConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionconstrainer.ArrayAuctionConstrainerList(this.auctionConstrainers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionConstrainers.clear();
        super.close();
        return;
    }
}
