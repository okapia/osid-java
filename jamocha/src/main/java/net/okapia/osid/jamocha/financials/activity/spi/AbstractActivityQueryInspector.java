//
// AbstractActivityQueryInspector.java
//
//     A template for making an ActivityQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for activities.
 */

public abstract class AbstractActivityQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.financials.ActivityQueryInspector {

    private final java.util.Collection<org.osid.financials.records.ActivityQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the organization <code> Id </code> query terms. 
     *
     *  @return the organization <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrganizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the organization query terms. 
     *
     *  @return the organization query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getOrganizationTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the supervisor <code> Id </code> query terms. 
     *
     *  @return the supervisor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupervisorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the supervisor query terms. 
     *
     *  @return the supervisor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSupervisorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the code query terms. 
     *
     *  @return the code query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the summary query terms. 
     *
     *  @return the summary query terms 
     */

    @OSID @Override
    public org.osid.financials.SummaryQueryInspector[] getSummaryTerms() {
        return (new org.osid.financials.SummaryQueryInspector[0]);
    }


    /**
     *  Gets the ancestor activity <code> Id </code> query terms. 
     *
     *  @return the ancestor activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor activity query terms. 
     *
     *  @return the ancestor activity terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getAncestorActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the descendant activity <code> Id </code> query terms. 
     *
     *  @return the descendant activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant activity query terms. 
     *
     *  @return the descendant activity terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getDescendantActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given activity query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an activity implementing the requested record.
     *
     *  @param activityRecordType an activity record type
     *  @return the activity query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.ActivityQueryInspectorRecord getActivityQueryInspectorRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.ActivityQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity query. 
     *
     *  @param activityQueryInspectorRecord activity query inspector
     *         record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityQueryInspectorRecord(org.osid.financials.records.ActivityQueryInspectorRecord activityQueryInspectorRecord, 
                                                   org.osid.type.Type activityRecordType) {

        addRecordType(activityRecordType);
        nullarg(activityRecordType, "activity record type");
        this.records.add(activityQueryInspectorRecord);        
        return;
    }
}
