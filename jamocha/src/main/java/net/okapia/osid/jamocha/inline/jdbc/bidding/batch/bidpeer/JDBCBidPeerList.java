//
// JDBCBidPeerList
//
//     Implements a BidPeerList. This list creates bidpeers from
//     the return of a JDBC query.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.jdbc.bidding.batch.bidpeer;

import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  <p>Implements a BidPeerList. This list creates bidpeers from the
 *  return of a JDBC query. The query is processed in a separate
 *  thread.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more bidpeers to be added.</p>
 */

public final class JDBCBidPeerList
    extends net.okapia.osid.jamocha.bidding.batch.bidpeer.spi.AbstractMutableBidPeerList
    implements org.osid.bidding.batch.BidPeerList,
               Runnable {

    private boolean running = false;
    private BidPeerFetcher fetcher;


    /**
     *  Creates a new <code>JDBCBidPeerList</code>.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator a bidPeer to parse a result row and
     *         generate a BidPeer
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of bidpeers in this list
     *         buffer such that when the number of bidpeers exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until bidpeers are retrieved. The value
     *         of this may depend on the amount of memory a BidPeer
     *         consumes.
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>bidPeer</code>
     *          is <code>null</code>
     */

    public JDBCBidPeerList(String query, java.sql.Connection connection, JDBCBidPeerGenerator generator,
                        boolean closeWhenDone, int bufferSize)
        throws org.osid.OperationFailedException {

        nullarg(query, "query");
        nullarg(connection, "connection");
        nullarg(generator, "generator");

        if (bufferSize <= 0) {
            throw new org.osid.InvalidArgumentException("buffer size too small");
        }

        this.fetcher = new BidPeerFetcher(query, connection, generator, closeWhenDone, bufferSize);
        return;
    }


    /**
     *  Creates a new <code>JDBCBidPeerList</code> and runs it upon
     *  instantiation.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator a BidPeer to parse a result row and
     *         generate a BidPeer
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of bidpeers in this list
     *         buffer such that when the number of bidpeers exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until bidpeers are retrieved. The value
     *         of this may depend on the amount of memory a BidPeer
     *         consumes.
     *  @param run <code>true</code> to start the fetching thread
     *         immediately
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>bidPeer</code>
     *          is <code>null</code>
     */

    public JDBCBidPeerList(String query, java.sql.Connection connection, JDBCBidPeerGenerator generator,
                        boolean closeWhenDone, int bufferSize, boolean run)
        throws org.osid.OperationFailedException {

        this(query, connection, generator, closeWhenDone, bufferSize);
        if (run) {
            run();
        }
        return;
    }


    /**
     *  Starts the JDBC process.
     *
     *  @throws org.osid.IllegalStateException already started
     */

    public void run() {
        if (this.running || hasError()) {
            throw new org.osid.IllegalStateException("already started");
        }

        this.fetcher.start();
        return;
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already
     *          closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.running = false;
        return;
    }


    /**
     *  Tests if the list thread is running to populate elements from
     *  the underlying list.
     *
     *  @return <code>true</code> if the list is running,
     *          <code>false</code> otherwise.
     */

    public boolean isRunning() {
        return (this.running);
    }


    class BidPeerFetcher
        extends Thread {
        
        private String query;
        private java.sql.Connection connection;
        private JDBCBidPeerGenerator generator;
        private boolean closeWhenDone;
        private int bufferSize;

        
        BidPeerFetcher(String query, java.sql.Connection connection, JDBCBidPeerGenerator generator,
                        boolean closeWhenDone, int bufferSize) {

            this.query         = query;
            this.connection    = connection;
            this.generator     = generator;
            this.closeWhenDone = closeWhenDone;
            this.bufferSize    = bufferSize;

            return;
        }
            
        
        public void run() {
            java.sql.Statement statement = null;
            java.sql.ResultSet resultset = null;
            JDBCBidPeerList.this.running = true;

            try {
                statement = this.connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
                                                            java.sql.ResultSet.CONCUR_READ_ONLY);
                resultset = statement.executeQuery(this.query);
                long length = 0;

                while (resultset.next() && JDBCBidPeerList.this.running && !JDBCBidPeerList.this.hasError()) {
                    JDBCBidPeerList.this.addBidPeer(generator.makeBidPeer(resultset));
                    synchronized (JDBCBidPeerList.this) {
                        JDBCBidPeerList.this.notifyAll();
                    }

                    if (++length > this.bufferSize) {
                        length = JDBCBidPeerList.this.available();
                        if (length > this.bufferSize) {
                            synchronized (JDBCBidPeerList.this) {
                                try {
                                    JDBCBidPeerList.this.wait();
                                } catch (InterruptedException ie) {}
                            }
                        }
                    } 
                }
            } catch (Exception e) {
                JDBCBidPeerList.this.error(new org.osid.OperationFailedException("cannot generate bidpeer", e));
                return;
            } finally {
                try {
                    if (this.closeWhenDone) {
                        this.connection.close();
                    }
                 
                    JDBCBidPeerList.this.running = false;

                    if (statement != null) {
                        statement.close();
                    }

                    if (resultset != null) {
                        resultset.close();
                    }
                } catch (Exception e) {}
            }

            JDBCBidPeerList.this.eol();
            return;
        }
    }
}
