//
// AbstractMessageLookupSession.java
//
//    A starter implementation framework for providing a Message
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Message
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getMessages(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractMessageLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.messaging.MessageLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.messaging.Mailbox mailbox = new net.okapia.osid.jamocha.nil.messaging.mailbox.UnknownMailbox();
    

    /**
     *  Gets the <code>Mailbox/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Mailbox Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMailboxId() {
        return (this.mailbox.getId());
    }


    /**
     *  Gets the <code>Mailbox</code> associated with this 
     *  session.
     *
     *  @return the <code>Mailbox</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.mailbox);
    }


    /**
     *  Sets the <code>Mailbox</code>.
     *
     *  @param  mailbox the mailbox for this session
     *  @throws org.osid.NullArgumentException <code>mailbox</code>
     *          is <code>null</code>
     */

    protected void setMailbox(org.osid.messaging.Mailbox mailbox) {
        nullarg(mailbox, "mailbox");
        this.mailbox = mailbox;
        return;
    }


    /**
     *  Tests if this user can perform <code>Message</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupMessages() {
        return (true);
    }


    /**
     *  A complete view of the <code>Message</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMessageView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Message</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMessageView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include messages in mailboxes which are children
     *  of this mailbox in the mailbox hierarchy.
     */

    @OSID @Override
    public void useFederatedMailboxView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this mailbox only.
     */

    @OSID @Override
    public void useIsolatedMailboxView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Message</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Message</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Message</code> and retained for
     *  compatibility.
     *
     *  @param  messageId <code>Id</code> of the
     *          <code>Message</code>
     *  @return the message
     *  @throws org.osid.NotFoundException <code>messageId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>messageId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Message getMessage(org.osid.id.Id messageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.messaging.MessageList messages = getMessages()) {
            while (messages.hasNext()) {
                org.osid.messaging.Message message = messages.getNextMessage();
                if (message.getId().equals(messageId)) {
                    return (message);
                }
            }
        } 

        throw new org.osid.NotFoundException(messageId + " not found");
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  messages specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Messages</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getMessages()</code>.
     *
     *  @param  messageIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>messageIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByIds(org.osid.id.IdList messageIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.messaging.Message> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = messageIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getMessage(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("message " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.messaging.message.LinkedMessageList(ret));
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  message genus <code>Type</code> which does not include
     *  messages of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known messages
     *  or an error results. Otherwise, the returned list may contain
     *  only those messages that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getMessages()</code>.
     *
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusType(org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageGenusFilterList(getMessages(), messageGenusType));
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  message genus <code>Type</code> and include any additional
     *  messages with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known messages
     *  or an error results. Otherwise, the returned list may contain
     *  only those messages that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getMessages()</code>.
     *
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByParentGenusType(org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getMessagesByGenusType(messageGenusType));
    }


    /**
     *  Gets a <code>MessageList</code> containing the given message
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known messages
     *  or an error results. Otherwise, the returned list may contain
     *  only those messages that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getMessages()</code>.
     *
     *  @param  messageRecordType a message record type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByRecordType(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageRecordFilterList(getMessages(), messageRecordType));
    }


    /**
     *  Gets a <code>MessageList</code> sent within the specified
     *  range inclusive. In plenary mode, the returned list contains
     *  all known messages or an error results. Otherwise, the
     *  returned list may contain only those messages that are
     *  accessible through this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTime(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageFilterList(new SentTimeFilter(from, to), getMessages()));
    }


    /**
     *  Gets a <code>MessageList</code> of the given genus type and
     *  sent within the specified range inclusive. In plenary mode,
     *  the returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>messageGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeAndGenusType(org.osid.type.Type messageGenusType, 
                                                                            org.osid.calendaring.DateTime from, 
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageGenusFilterList(getMessagesBySentTime(from, to), messageGenusType));
    }


    /**
     *  Gets a <code>MessageList</code> sent by the specified
     *  sender. In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list may
     *  contain only those messages that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource Id 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesFromSender(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageFilterList(new SenderFilter(resourceId), getMessages()));
    }


    /**
     *  Gets a <code>MessageList</code> of the given genus type and
     *  sender. In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list may
     *  contain only those messages that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>messageGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeFromSender(org.osid.id.Id resourceId, 
                                                                           org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageGenusFilterList(getMessagesFromSender(resourceId), messageGenusType));
    }


    /**
     *  Gets a <code>MessageList</code> sent by the specified sender
     *  and sent time. In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session
     *
     *  @param  resourceId a resource Id 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeFromSender(org.osid.id.Id resourceId, 
                                                                          org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageFilterList(new SentTimeFilter(from, to), getMessagesFromSender(resourceId)));
    }


    /**
     *  Gets a <code>MessageList</code> of the given genus type and
     *  sent within the specified range inclusive. In plenary mode,
     *  the returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>,
     *          <code>messageGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeAndGenusTypeFromSender(org.osid.id.Id resourceId, 
                                                                                      org.osid.type.Type messageGenusType, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageGenusFilterList(getMessagesBySentTimeFromSender(resourceId, from, to), messageGenusType));
    }        


    /**
     *  Gets a <code>MessageList</code> received by the specified
     *  recipient.  In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.messaging.Message> ret = new java.util.ArrayList<>();

        try (org.osid.messaging.MessageList messages = getMessages()) {
            while (messages.hasNext()) {
                org.osid.messaging.Message message = messages.getNextMessage();
                if (message.isSent()) {
                    try (org.osid.id.IdList ids = message.getRecipientIds()) {
                        while (ids.hasNext()) {
                            if (ids.getNextId().equals(resourceId)) {
                                ret.add(message);
                            }
                        }
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.messaging.message.LinkedMessageList(ret));
    }

    
    /**
     *  Gets a <code>MessageList</code> received by the specified
     *  recipient.  In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>messageGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeForRecipient(org.osid.id.Id resourceId, 
                                                                             org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageGenusFilterList(getMessagesForRecipient(resourceId), messageGenusType));
    }


    /**
     *  Gets a <code>MessageList</code> received by the specified
     *  resource and received time. In plenary mode, the returned list
     *  contains all known messages or an error results. Otherwise,
     *  the returned list may contain only those messages that are
     *  accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeForRecipient(org.osid.id.Id resourceId, 
                                                                                org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageFilterList(new ReceivedTimeFilter(from, to), getMessagesForRecipient(resourceId)));
    }
        
    
    /**
     *  Gets a <code>MessageList</code> of the given genus type
     *  received by the specified resource and received time. In
     *  plenary mode, the returned list contains all known messages or
     *  an error results.  Otherwise, the returned list may contain
     *  only those messages that are accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>,
     *          <code>messageGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeAndGenusTypeForRecipient(org.osid.id.Id resourceId, 
                                                                                            org.osid.type.Type messageGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageGenusFilterList(getMessagesByReceivedTimeForRecipient(resourceId, from, to), messageGenusType));
    }


    /**
     *  Gets a <code>MessageList</code> sent by the specified sender
     *  and received by the specified recipient. In plenary mode, the
     *  returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @return the returned <code>Message</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>senderResourceId</code> or
     *          <code>recipientResourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                            org.osid.id.Id recipientResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageFilterList(new SenderFilter(senderResourceId), getMessagesForRecipient(recipientResourceId)));        
    }


    /**
     *  Gets a <code>MessageList </code>of the given genus type sent
     *  by the specified sender and received by the specified
     *  recipient. In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message </code>list 
     *  @throws org.osid.NullArgumentException <code>senderResourceId,
     *          recipientResourceId </code>or <code>messageGenusType
     *          </code> is <code>null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                                       org.osid.id.Id recipientResourceId, 
                                                                                       org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageGenusFilterList(getMessagesFromSenderForRecipient(senderResourceId, recipientResourceId), messageGenusType));
    }


    /**
     *  Gets a <code>MessageList</code>by the specified sender,
     *  recipient, and received time. In plenary mode, the returned
     *  list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Message</code>list 
     *  @throws org.osid.InvalidArgumentException <code>to</code>is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>senderResourceId,
     *          recipientResourceId, from</code>or <code>to</code>is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                                          org.osid.id.Id recipientResourceId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageFilterList(new ReceivedTimeFilter(from, to), getMessagesFromSenderForRecipient(senderResourceId, recipientResourceId)));
    }


    /**
     *  Gets a <code>MessageList</code>of the given genus type and
     *  received by the specified resource and received time. In
     *  plenary mode, the returned list contains all known messages or
     *  an error results.  Otherwise, the returned list may contain
     *  only those messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Message</code>list 
     *  @throws org.osid.InvalidArgumentException <code>to</code>is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>senderResourceId,
     *          recipientResourceId, messageGenusType, from</code>or
     *          <code> to</code>is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeAndGenusTypeFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                                                      org.osid.id.Id recipientResourceId, 
                                                                                                      org.osid.type.Type messageGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.message.MessageGenusFilterList(getMessagesByReceivedTimeFromSenderForRecipient(senderResourceId, recipientResourceId, from, to), messageGenusType));
    }

    
    /**
     *  Gets all <code>Messages</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Messages</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.messaging.MessageList getMessages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the message list for active and effective
     *  views. Should be called by <code>getObjects()</code>if no
     *  filtering is already performed.
     *
     *  @param list the list of messages
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.messaging.MessageList filterMessagesOnViews(org.osid.messaging.MessageList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class SenderFilter
        implements net.okapia.osid.jamocha.inline.filter.messaging.message.MessageFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>SenderFilterList</code>.
         *
         *  @param resourceId the sender to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */

        public SenderFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the MessageFilterList to filter the message list
         *  based on sender.
         *
         *  @param message the message
         *  @return <code>true</code> to pass the message,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.messaging.Message message) {
            if (message.isSent()) {
                return (message.getSenderId().equals(this.resourceId));
            } else {
                return (false);
            }
        }
    }


    public static class SentTimeFilter
        implements net.okapia.osid.jamocha.inline.filter.messaging.message.MessageFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;

        
        /**
         *  Constructs a new <date>SentTimeFilter</date>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <date>from</date>
         *          or <code>to</code> is <date>null</date>
         */

        public SentTimeFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to   = to;

            return;
        }


        /**
         *  Used by the MessageFilterList to filter the log entry
         *  list based on date.
         *
         *  @param message the log entry
         *  @return <date>true</date> to pass the log entry,
         *          <date>false</date> to filter it
         */
        
        @Override
        public boolean pass(org.osid.messaging.Message message) {
            if (!message.isSent()) {
                return (false);
            }

            if (message.getSentTime().isLess(this.from)) {
                return (false);
            }

            if (message.getSentTime().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }        



    public static class ReceivedTimeFilter
        implements net.okapia.osid.jamocha.inline.filter.messaging.message.MessageFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;

        
        /**
         *  Constructs a new <date>ReceivedTimeFilter</date>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <date>from</date>
         *          or <code>to</code> is <date>null</date>
         */

        public ReceivedTimeFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to   = to;

            return;
        }


        /**
         *  Used by the MessageFilterList to filter the log entry
         *  list based on date.
         *
         *  @param message the log entry
         *  @return <date>true</date> to pass the log entry,
         *          <date>false</date> to filter it
         */
        
        @Override
        public boolean pass(org.osid.messaging.Message message) {
            if (!message.isSent()) {
                return (false);
            }

            if (message.getReceivedTime().isLess(this.from)) {
                return (false);
            }

            if (message.getReceivedTime().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }        
}
