//
// MutableMapResourceRelationshipLookupSession
//
//    Implements a ResourceRelationship lookup service backed by a collection of
//    resourceRelationships that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource;


/**
 *  Implements a ResourceRelationship lookup service backed by a collection of
 *  resource relationships. The resource relationships are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of resource relationships can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapResourceRelationshipLookupSession
    extends net.okapia.osid.jamocha.core.resource.spi.AbstractMapResourceRelationshipLookupSession
    implements org.osid.resource.ResourceRelationshipLookupSession {


    /**
     *  Constructs a new {@code MutableMapResourceRelationshipLookupSession}
     *  with no resource relationships.
     *
     *  @param bin the bin
     *  @throws org.osid.NullArgumentException {@code bin} is
     *          {@code null}
     */

      public MutableMapResourceRelationshipLookupSession(org.osid.resource.Bin bin) {
        setBin(bin);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapResourceRelationshipLookupSession} with a
     *  single resourceRelationship.
     *
     *  @param bin the bin  
     *  @param resourceRelationship a resource relationship
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resourceRelationship} is {@code null}
     */

    public MutableMapResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                           org.osid.resource.ResourceRelationship resourceRelationship) {
        this(bin);
        putResourceRelationship(resourceRelationship);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapResourceRelationshipLookupSession}
     *  using an array of resource relationships.
     *
     *  @param bin the bin
     *  @param resourceRelationships an array of resource relationships
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resourceRelationships} is {@code null}
     */

    public MutableMapResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                           org.osid.resource.ResourceRelationship[] resourceRelationships) {
        this(bin);
        putResourceRelationships(resourceRelationships);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapResourceRelationshipLookupSession}
     *  using a collection of resource relationships.
     *
     *  @param bin the bin
     *  @param resourceRelationships a collection of resource relationships
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resourceRelationships} is {@code null}
     */

    public MutableMapResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                           java.util.Collection<? extends org.osid.resource.ResourceRelationship> resourceRelationships) {

        this(bin);
        putResourceRelationships(resourceRelationships);
        return;
    }

    
    /**
     *  Makes a {@code ResourceRelationship} available in this session.
     *
     *  @param resourceRelationship a resource relationship
     *  @throws org.osid.NullArgumentException {@code resourceRelationship{@code  is
     *          {@code null}
     */

    @Override
    public void putResourceRelationship(org.osid.resource.ResourceRelationship resourceRelationship) {
        super.putResourceRelationship(resourceRelationship);
        return;
    }


    /**
     *  Makes an array of resource relationships available in this session.
     *
     *  @param resourceRelationships an array of resource relationships
     *  @throws org.osid.NullArgumentException {@code resourceRelationships{@code 
     *          is {@code null}
     */

    @Override
    public void putResourceRelationships(org.osid.resource.ResourceRelationship[] resourceRelationships) {
        super.putResourceRelationships(resourceRelationships);
        return;
    }


    /**
     *  Makes collection of resource relationships available in this session.
     *
     *  @param resourceRelationships a collection of resource relationships
     *  @throws org.osid.NullArgumentException {@code resourceRelationships{@code  is
     *          {@code null}
     */

    @Override
    public void putResourceRelationships(java.util.Collection<? extends org.osid.resource.ResourceRelationship> resourceRelationships) {
        super.putResourceRelationships(resourceRelationships);
        return;
    }


    /**
     *  Removes a ResourceRelationship from this session.
     *
     *  @param resourceRelationshipId the {@code Id} of the resource relationship
     *  @throws org.osid.NullArgumentException {@code resourceRelationshipId{@code 
     *          is {@code null}
     */

    @Override
    public void removeResourceRelationship(org.osid.id.Id resourceRelationshipId) {
        super.removeResourceRelationship(resourceRelationshipId);
        return;
    }    
}
