//
// MutableIndexedMapProxyAgencyLookupSession
//
//    Implements an Agency lookup service backed by a collection of
//    agencies indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication;


/**
 *  Implements an Agency lookup service backed by a collection of
 *  agencies. The agencies are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some agencies may be compatible
 *  with more types than are indicated through these agency
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of agencies can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyAgencyLookupSession
    extends net.okapia.osid.jamocha.core.authentication.spi.AbstractIndexedMapAgencyLookupSession
    implements org.osid.authentication.AgencyLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAgencyLookupSession} with
     *  no agency.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyAgencyLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAgencyLookupSession} with
     *  a single agency.
     *
     *  @param  agency an agency
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyAgencyLookupSession(org.osid.authentication.Agency agency, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAgency(agency);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAgencyLookupSession} using
     *  an array of agencies.
     *
     *  @param  agencies an array of agencies
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agencies} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyAgencyLookupSession(org.osid.authentication.Agency[] agencies, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAgencies(agencies);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyAgencyLookupSession} using
     *  a collection of agencies.
     *
     *  @param  agencies a collection of agencies
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agencies} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyAgencyLookupSession(java.util.Collection<? extends org.osid.authentication.Agency> agencies,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAgencies(agencies);
        return;
    }

    
    /**
     *  Makes an {@code Agency} available in this session.
     *
     *  @param  agency an agency
     *  @throws org.osid.NullArgumentException {@code agency{@code 
     *          is {@code null}
     */

    @Override
    public void putAgency(org.osid.authentication.Agency agency) {
        super.putAgency(agency);
        return;
    }


    /**
     *  Makes an array of agencies available in this session.
     *
     *  @param  agencies an array of agencies
     *  @throws org.osid.NullArgumentException {@code agencies{@code 
     *          is {@code null}
     */

    @Override
    public void putAgencies(org.osid.authentication.Agency[] agencies) {
        super.putAgencies(agencies);
        return;
    }


    /**
     *  Makes collection of agencies available in this session.
     *
     *  @param  agencies a collection of agencies
     *  @throws org.osid.NullArgumentException {@code agency{@code 
     *          is {@code null}
     */

    @Override
    public void putAgencies(java.util.Collection<? extends org.osid.authentication.Agency> agencies) {
        super.putAgencies(agencies);
        return;
    }


    /**
     *  Removes an Agency from this session.
     *
     *  @param agencyId the {@code Id} of the agency
     *  @throws org.osid.NullArgumentException {@code agencyId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAgency(org.osid.id.Id agencyId) {
        super.removeAgency(agencyId);
        return;
    }    
}
