//
// AbstractSignalSearch.java
//
//     A template for making a Signal Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.signal.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing signal searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractSignalSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.mapping.path.SignalSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.SignalSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.mapping.path.SignalSearchOrder signalSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of signals. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  signalIds list of signals
     *  @throws org.osid.NullArgumentException
     *          <code>signalIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSignals(org.osid.id.IdList signalIds) {
        while (signalIds.hasNext()) {
            try {
                this.ids.add(signalIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSignals</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of signal Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getSignalIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  signalSearchOrder signal search order 
     *  @throws org.osid.NullArgumentException
     *          <code>signalSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>signalSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderSignalResults(org.osid.mapping.path.SignalSearchOrder signalSearchOrder) {
	this.signalSearchOrder = signalSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.mapping.path.SignalSearchOrder getSignalSearchOrder() {
	return (this.signalSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given signal search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a signal implementing the requested record.
     *
     *  @param signalSearchRecordType a signal search record
     *         type
     *  @return the signal search record
     *  @throws org.osid.NullArgumentException
     *          <code>signalSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SignalSearchRecord getSignalSearchRecord(org.osid.type.Type signalSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.mapping.path.records.SignalSearchRecord record : this.records) {
            if (record.implementsRecordType(signalSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this signal search. 
     *
     *  @param signalSearchRecord signal search record
     *  @param signalSearchRecordType signal search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSignalSearchRecord(org.osid.mapping.path.records.SignalSearchRecord signalSearchRecord, 
                                           org.osid.type.Type signalSearchRecordType) {

        addRecordType(signalSearchRecordType);
        this.records.add(signalSearchRecord);        
        return;
    }
}
