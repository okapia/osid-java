//
// AbstractResourcingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractResourcingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.resourcing.ResourcingManager,
               org.osid.resourcing.ResourcingProxyManager {

    private final Types jobRecordTypes                     = new TypeRefSet();
    private final Types jobSearchRecordTypes               = new TypeRefSet();

    private final Types workRecordTypes                    = new TypeRefSet();
    private final Types workSearchRecordTypes              = new TypeRefSet();

    private final Types competencyRecordTypes              = new TypeRefSet();
    private final Types competencySearchRecordTypes        = new TypeRefSet();

    private final Types availabilityRecordTypes            = new TypeRefSet();
    private final Types availabilitySearchRecordTypes      = new TypeRefSet();

    private final Types commissionRecordTypes              = new TypeRefSet();
    private final Types commissionSearchRecordTypes        = new TypeRefSet();

    private final Types effortRecordTypes                  = new TypeRefSet();
    private final Types effortSearchRecordTypes            = new TypeRefSet();

    private final Types foundryRecordTypes                 = new TypeRefSet();
    private final Types foundrySearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractResourcingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractResourcingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any foundry federation is exposed. Federation is exposed when 
     *  a specific foundry may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  foundries appears as a single foundry. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if an availability service is supported for the current agent. 
     *
     *  @return <code> true </code> if my availability is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyWork() {
        return (false);
    }


    /**
     *  Tests if looking up jobs is supported. 
     *
     *  @return <code> true </code> if job lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobLookup() {
        return (false);
    }


    /**
     *  Tests if querying jobs is supported. 
     *
     *  @return <code> true </code> if job query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobQuery() {
        return (false);
    }


    /**
     *  Tests if searching jobs is supported. 
     *
     *  @return <code> true </code> if job search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobSearch() {
        return (false);
    }


    /**
     *  Tests if job administrative service is supported. 
     *
     *  @return <code> true </code> if job administration is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobAdmin() {
        return (false);
    }


    /**
     *  Tests if a job notification service is supported. 
     *
     *  @return <code> true </code> if job notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobNotification() {
        return (false);
    }


    /**
     *  Tests if a job foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobFoundry() {
        return (false);
    }


    /**
     *  Tests if a job foundry service is supported. 
     *
     *  @return <code> true </code> if job to foundry assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if a job smart foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if looking up work is supported. 
     *
     *  @return <code> true </code> if work lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkLookup() {
        return (false);
    }


    /**
     *  Tests if querying work is supported. 
     *
     *  @return <code> true </code> if work query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Tests if searching work is supported. 
     *
     *  @return <code> true </code> if work search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkSearch() {
        return (false);
    }


    /**
     *  Tests if work administrative service is supported. 
     *
     *  @return <code> true </code> if work administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkAdmin() {
        return (false);
    }


    /**
     *  Tests if a work notification service is supported. 
     *
     *  @return <code> true </code> if work notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkNotification() {
        return (false);
    }


    /**
     *  Tests if a work foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a work foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkFoundry() {
        return (false);
    }


    /**
     *  Tests if a work foundry service is supported. 
     *
     *  @return <code> true </code> if work to foundry assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if a work smart foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a work smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if looking up competencies is supported. 
     *
     *  @return <code> true </code> if competency lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyLookup() {
        return (false);
    }


    /**
     *  Tests if querying competencies is supported. 
     *
     *  @return <code> true </code> if competency query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyQuery() {
        return (false);
    }


    /**
     *  Tests if searching competencies is supported. 
     *
     *  @return <code> true </code> if competency search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencySearch() {
        return (false);
    }


    /**
     *  Tests if competency administrative service is supported. 
     *
     *  @return <code> true </code> if competency administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyAdmin() {
        return (false);
    }


    /**
     *  Tests if a competency notification service is supported. 
     *
     *  @return <code> true </code> if competency notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyNotification() {
        return (false);
    }


    /**
     *  Tests if a competency foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a competency foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyFoundry() {
        return (false);
    }


    /**
     *  Tests if a competency foundry service is supported. 
     *
     *  @return <code> true </code> if competency to foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if a competency smart foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a competency smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencySmartFoundry() {
        return (false);
    }


    /**
     *  Tests if looking up availabilities is supported. 
     *
     *  @return <code> true </code> if availability lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityLookup() {
        return (false);
    }


    /**
     *  Tests if querying availabilities is supported. 
     *
     *  @return <code> true </code> if availability query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityQuery() {
        return (false);
    }


    /**
     *  Tests if searching availabilities is supported. 
     *
     *  @return <code> true </code> if availability search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilitySearch() {
        return (false);
    }


    /**
     *  Tests if availability <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if availability administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityAdmin() {
        return (false);
    }


    /**
     *  Tests if an availability <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if availability notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityNotification() {
        return (false);
    }


    /**
     *  Tests if an availability foundry lookup service is supported. 
     *
     *  @return <code> true </code> if an availability foundry lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityFoundry() {
        return (false);
    }


    /**
     *  Tests if an availability foundry assignment service is supported. 
     *
     *  @return <code> true </code> if an availability to foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if an availability smart foundry service is supported. 
     *
     *  @return <code> true </code> if an availability smart foundry service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilitySmartFoundry() {
        return (false);
    }


    /**
     *  Tests if looking up commissions is supported. 
     *
     *  @return <code> true </code> if commission lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionLookup() {
        return (false);
    }


    /**
     *  Tests if querying commissions is supported. 
     *
     *  @return <code> true </code> if commission query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionQuery() {
        return (false);
    }


    /**
     *  Tests if searching commissions is supported. 
     *
     *  @return <code> true </code> if commission search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionSearch() {
        return (false);
    }


    /**
     *  Tests if commission administrative service is supported. 
     *
     *  @return <code> true </code> if commission administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionAdmin() {
        return (false);
    }


    /**
     *  Tests if a commission notification service is supported. 
     *
     *  @return <code> true </code> if commission notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionNotification() {
        return (false);
    }


    /**
     *  Tests if a commission foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a commission foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionFoundry() {
        return (false);
    }


    /**
     *  Tests if a commission foundry service is supported. 
     *
     *  @return <code> true </code> if commission to foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if a commission smart foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a commission smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if looking up efforts is supported. 
     *
     *  @return <code> true </code> if effort lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortLookup() {
        return (false);
    }


    /**
     *  Tests if querying efforts is supported. 
     *
     *  @return <code> true </code> if effort query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortQuery() {
        return (false);
    }


    /**
     *  Tests if searching efforts is supported. 
     *
     *  @return <code> true </code> if effort search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortSearch() {
        return (false);
    }


    /**
     *  Tests if an effort administrative service is supported. 
     *
     *  @return <code> true </code> if effort administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortAdmin() {
        return (false);
    }


    /**
     *  Tests if an effort <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if effort notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortNotification() {
        return (false);
    }


    /**
     *  Tests if an effort foundry lookup service is supported. 
     *
     *  @return <code> true </code> if an effort foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortFoundry() {
        return (false);
    }


    /**
     *  Tests if an effort foundry assignment service is supported. 
     *
     *  @return <code> true </code> if an effort to foundry assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if an effort smart foundry service is supported. 
     *
     *  @return <code> true </code> if an v smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if looking up foundries is supported. 
     *
     *  @return <code> true </code> if foundry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryLookup() {
        return (false);
    }


    /**
     *  Tests if querying foundries is supported. 
     *
     *  @return <code> true </code> if a foundry query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Tests if searching foundries is supported. 
     *
     *  @return <code> true </code> if foundry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundrySearch() {
        return (false);
    }


    /**
     *  Tests if foundry administrative service is supported. 
     *
     *  @return <code> true </code> if foundry administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryAdmin() {
        return (false);
    }


    /**
     *  Tests if a foundry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if foundry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a foundry hierarchy traversal service. 
     *
     *  @return <code> true </code> if foundry hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a foundry hierarchy design service. 
     *
     *  @return <code> true </code> if foundry hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a resourcing batch service. 
     *
     *  @return <code> true </code> if a resourcing batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcingBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a resourcing rules service. 
     *
     *  @return <code> true </code> if a resourcing rules service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcingRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Job </code> record types. 
     *
     *  @return a list containing the supported <code> Job </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Job </code> record type is supported. 
     *
     *  @param  jobRecordType a <code> Type </code> indicating a <code> Job 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> jobRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobRecordType(org.osid.type.Type jobRecordType) {
        return (this.jobRecordTypes.contains(jobRecordType));
    }


    /**
     *  Adds support for a job record type.
     *
     *  @param jobRecordType a job record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobRecordType</code> is <code>null</code>
     */

    protected void addJobRecordType(org.osid.type.Type jobRecordType) {
        this.jobRecordTypes.add(jobRecordType);
        return;
    }


    /**
     *  Removes support for a job record type.
     *
     *  @param jobRecordType a job record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobRecordType</code> is <code>null</code>
     */

    protected void removeJobRecordType(org.osid.type.Type jobRecordType) {
        this.jobRecordTypes.remove(jobRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Job </code> search record types. 
     *
     *  @return a list containing the supported <code> Job </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Job </code> search record type is supported. 
     *
     *  @param  jobSearchRecordType a <code> Type </code> indicating a <code> 
     *          Job </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> jobSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobSearchRecordType(org.osid.type.Type jobSearchRecordType) {
        return (this.jobSearchRecordTypes.contains(jobSearchRecordType));
    }


    /**
     *  Adds support for a job search record type.
     *
     *  @param jobSearchRecordType a job search record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobSearchRecordType</code> is <code>null</code>
     */

    protected void addJobSearchRecordType(org.osid.type.Type jobSearchRecordType) {
        this.jobSearchRecordTypes.add(jobSearchRecordType);
        return;
    }


    /**
     *  Removes support for a job search record type.
     *
     *  @param jobSearchRecordType a job search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobSearchRecordType</code> is <code>null</code>
     */

    protected void removeJobSearchRecordType(org.osid.type.Type jobSearchRecordType) {
        this.jobSearchRecordTypes.remove(jobSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Work </code> record types. 
     *
     *  @return a list containing the supported <code> Work </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.workRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Work </code> record type is supported. 
     *
     *  @param  workRecordType a <code> Type </code> indicating a <code> Work 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkRecordType(org.osid.type.Type workRecordType) {
        return (this.workRecordTypes.contains(workRecordType));
    }


    /**
     *  Adds support for a work record type.
     *
     *  @param workRecordType a work record type
     *  @throws org.osid.NullArgumentException
     *  <code>workRecordType</code> is <code>null</code>
     */

    protected void addWorkRecordType(org.osid.type.Type workRecordType) {
        this.workRecordTypes.add(workRecordType);
        return;
    }


    /**
     *  Removes support for a work record type.
     *
     *  @param workRecordType a work record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>workRecordType</code> is <code>null</code>
     */

    protected void removeWorkRecordType(org.osid.type.Type workRecordType) {
        this.workRecordTypes.remove(workRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Work </code> search record types. 
     *
     *  @return a list containing the supported <code> Work </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.workSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Work </code> search record type is 
     *  supported. 
     *
     *  @param  workSearchRecordType a <code> Type </code> indicating a <code> 
     *          Work </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkSearchRecordType(org.osid.type.Type workSearchRecordType) {
        return (this.workSearchRecordTypes.contains(workSearchRecordType));
    }


    /**
     *  Adds support for a work search record type.
     *
     *  @param workSearchRecordType a work search record type
     *  @throws org.osid.NullArgumentException
     *  <code>workSearchRecordType</code> is <code>null</code>
     */

    protected void addWorkSearchRecordType(org.osid.type.Type workSearchRecordType) {
        this.workSearchRecordTypes.add(workSearchRecordType);
        return;
    }


    /**
     *  Removes support for a work search record type.
     *
     *  @param workSearchRecordType a work search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>workSearchRecordType</code> is <code>null</code>
     */

    protected void removeWorkSearchRecordType(org.osid.type.Type workSearchRecordType) {
        this.workSearchRecordTypes.remove(workSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Competency </code> record types. 
     *
     *  @return a list containing the supported <code> Competency </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompetencyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.competencyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Competency </code> record type is supported. 
     *
     *  @param  competencyRecordType a <code> Type </code> indicating a <code> 
     *          Competency </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> competencyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompetencyRecordType(org.osid.type.Type competencyRecordType) {
        return (this.competencyRecordTypes.contains(competencyRecordType));
    }


    /**
     *  Adds support for a competency record type.
     *
     *  @param competencyRecordType a competency record type
     *  @throws org.osid.NullArgumentException
     *  <code>competencyRecordType</code> is <code>null</code>
     */

    protected void addCompetencyRecordType(org.osid.type.Type competencyRecordType) {
        this.competencyRecordTypes.add(competencyRecordType);
        return;
    }


    /**
     *  Removes support for a competency record type.
     *
     *  @param competencyRecordType a competency record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>competencyRecordType</code> is <code>null</code>
     */

    protected void removeCompetencyRecordType(org.osid.type.Type competencyRecordType) {
        this.competencyRecordTypes.remove(competencyRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Competency </code> search record types. 
     *
     *  @return a list containing the supported <code> Competency </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompetencySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.competencySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Competency </code> search record type is 
     *  supported. 
     *
     *  @param  competencySearchRecordType a <code> Type </code> indicating a 
     *          <code> Competency </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          competencSearchyRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompetencySearchRecordType(org.osid.type.Type competencySearchRecordType) {
        return (this.competencySearchRecordTypes.contains(competencySearchRecordType));
    }


    /**
     *  Adds support for a competency search record type.
     *
     *  @param competencySearchRecordType a competency search record type
     *  @throws org.osid.NullArgumentException
     *  <code>competencySearchRecordType</code> is <code>null</code>
     */

    protected void addCompetencySearchRecordType(org.osid.type.Type competencySearchRecordType) {
        this.competencySearchRecordTypes.add(competencySearchRecordType);
        return;
    }


    /**
     *  Removes support for a competency search record type.
     *
     *  @param competencySearchRecordType a competency search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>competencySearchRecordType</code> is <code>null</code>
     */

    protected void removeCompetencySearchRecordType(org.osid.type.Type competencySearchRecordType) {
        this.competencySearchRecordTypes.remove(competencySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Availability </code> record types. 
     *
     *  @return a list containing the supported <code> Availability </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAvailabilityRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.availabilityRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Availability </code> record type is 
     *  supported. 
     *
     *  @param  availabilityRecordType a <code> Type </code> indicating an 
     *          <code> Availability </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> availabilityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAvailabilityRecordType(org.osid.type.Type availabilityRecordType) {
        return (this.availabilityRecordTypes.contains(availabilityRecordType));
    }


    /**
     *  Adds support for an availability record type.
     *
     *  @param availabilityRecordType an availability record type
     *  @throws org.osid.NullArgumentException
     *  <code>availabilityRecordType</code> is <code>null</code>
     */

    protected void addAvailabilityRecordType(org.osid.type.Type availabilityRecordType) {
        this.availabilityRecordTypes.add(availabilityRecordType);
        return;
    }


    /**
     *  Removes support for an availability record type.
     *
     *  @param availabilityRecordType an availability record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>availabilityRecordType</code> is <code>null</code>
     */

    protected void removeAvailabilityRecordType(org.osid.type.Type availabilityRecordType) {
        this.availabilityRecordTypes.remove(availabilityRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Availability </code> search types. 
     *
     *  @return a list containing the supported <code> Availability </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAvailabilitySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.availabilitySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Availability </code> search type is 
     *  supported. 
     *
     *  @param  availabilitySearchRecordType a <code> Type </code> indicating 
     *          an <code> Availability </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilitySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAvailabilitySearchRecordType(org.osid.type.Type availabilitySearchRecordType) {
        return (this.availabilitySearchRecordTypes.contains(availabilitySearchRecordType));
    }


    /**
     *  Adds support for an availability search record type.
     *
     *  @param availabilitySearchRecordType an availability search record type
     *  @throws org.osid.NullArgumentException
     *  <code>availabilitySearchRecordType</code> is <code>null</code>
     */

    protected void addAvailabilitySearchRecordType(org.osid.type.Type availabilitySearchRecordType) {
        this.availabilitySearchRecordTypes.add(availabilitySearchRecordType);
        return;
    }


    /**
     *  Removes support for an availability search record type.
     *
     *  @param availabilitySearchRecordType an availability search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>availabilitySearchRecordType</code> is <code>null</code>
     */

    protected void removeAvailabilitySearchRecordType(org.osid.type.Type availabilitySearchRecordType) {
        this.availabilitySearchRecordTypes.remove(availabilitySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Commission </code> record types. 
     *
     *  @return a list containing the supported <code> Commission </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommissionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commissionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Commission </code> record type is supported. 
     *
     *  @param  commissionRecordType a <code> Type </code> indicating a <code> 
     *          Commission </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> commissionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommissionRecordType(org.osid.type.Type commissionRecordType) {
        return (this.commissionRecordTypes.contains(commissionRecordType));
    }


    /**
     *  Adds support for a commission record type.
     *
     *  @param commissionRecordType a commission record type
     *  @throws org.osid.NullArgumentException
     *  <code>commissionRecordType</code> is <code>null</code>
     */

    protected void addCommissionRecordType(org.osid.type.Type commissionRecordType) {
        this.commissionRecordTypes.add(commissionRecordType);
        return;
    }


    /**
     *  Removes support for a commission record type.
     *
     *  @param commissionRecordType a commission record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commissionRecordType</code> is <code>null</code>
     */

    protected void removeCommissionRecordType(org.osid.type.Type commissionRecordType) {
        this.commissionRecordTypes.remove(commissionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Commission </code> search record types. 
     *
     *  @return a list containing the supported <code> Commission </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommissionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commissionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Commission </code> search record type is 
     *  supported. 
     *
     *  @param  commissionSearchRecordType a <code> Type </code> indicating a 
     *          <code> Commission </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommissionSearchRecordType(org.osid.type.Type commissionSearchRecordType) {
        return (this.commissionSearchRecordTypes.contains(commissionSearchRecordType));
    }


    /**
     *  Adds support for a commission search record type.
     *
     *  @param commissionSearchRecordType a commission search record type
     *  @throws org.osid.NullArgumentException
     *  <code>commissionSearchRecordType</code> is <code>null</code>
     */

    protected void addCommissionSearchRecordType(org.osid.type.Type commissionSearchRecordType) {
        this.commissionSearchRecordTypes.add(commissionSearchRecordType);
        return;
    }


    /**
     *  Removes support for a commission search record type.
     *
     *  @param commissionSearchRecordType a commission search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commissionSearchRecordType</code> is <code>null</code>
     */

    protected void removeCommissionSearchRecordType(org.osid.type.Type commissionSearchRecordType) {
        this.commissionSearchRecordTypes.remove(commissionSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Effort </code> record types. 
     *
     *  @return a list containing the supported <code> Effort </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEffortRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.effortRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Effort </code> record type is supported. 
     *
     *  @param  effortRecordType a <code> Type </code> indicating an <code> 
     *          Effort </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> effortRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEffortRecordType(org.osid.type.Type effortRecordType) {
        return (this.effortRecordTypes.contains(effortRecordType));
    }


    /**
     *  Adds support for an effort record type.
     *
     *  @param effortRecordType an effort record type
     *  @throws org.osid.NullArgumentException
     *  <code>effortRecordType</code> is <code>null</code>
     */

    protected void addEffortRecordType(org.osid.type.Type effortRecordType) {
        this.effortRecordTypes.add(effortRecordType);
        return;
    }


    /**
     *  Removes support for an effort record type.
     *
     *  @param effortRecordType an effort record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>effortRecordType</code> is <code>null</code>
     */

    protected void removeEffortRecordType(org.osid.type.Type effortRecordType) {
        this.effortRecordTypes.remove(effortRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Effort </code> search types. 
     *
     *  @return a list containing the supported <code> Effort </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEffortSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.effortSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Effort </code> search type is supported. 
     *
     *  @param  effortSearchRecordType a <code> Type </code> indicating an 
     *          <code> Effort </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> effiortSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEffortSearchRecordType(org.osid.type.Type effortSearchRecordType) {
        return (this.effortSearchRecordTypes.contains(effortSearchRecordType));
    }


    /**
     *  Adds support for an effort search record type.
     *
     *  @param effortSearchRecordType an effort search record type
     *  @throws org.osid.NullArgumentException
     *  <code>effortSearchRecordType</code> is <code>null</code>
     */

    protected void addEffortSearchRecordType(org.osid.type.Type effortSearchRecordType) {
        this.effortSearchRecordTypes.add(effortSearchRecordType);
        return;
    }


    /**
     *  Removes support for an effort search record type.
     *
     *  @param effortSearchRecordType an effort search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>effortSearchRecordType</code> is <code>null</code>
     */

    protected void removeEffortSearchRecordType(org.osid.type.Type effortSearchRecordType) {
        this.effortSearchRecordTypes.remove(effortSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Foundry </code> record types. 
     *
     *  @return a list containing the supported <code> Foundry </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFoundryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.foundryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Foundry </code> record type is supported. 
     *
     *  @param  foundryRecordType a <code> Type </code> indicating a <code> 
     *          Foundry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> foundryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFoundryRecordType(org.osid.type.Type foundryRecordType) {
        return (this.foundryRecordTypes.contains(foundryRecordType));
    }


    /**
     *  Adds support for a foundry record type.
     *
     *  @param foundryRecordType a foundry record type
     *  @throws org.osid.NullArgumentException
     *  <code>foundryRecordType</code> is <code>null</code>
     */

    protected void addFoundryRecordType(org.osid.type.Type foundryRecordType) {
        this.foundryRecordTypes.add(foundryRecordType);
        return;
    }


    /**
     *  Removes support for a foundry record type.
     *
     *  @param foundryRecordType a foundry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>foundryRecordType</code> is <code>null</code>
     */

    protected void removeFoundryRecordType(org.osid.type.Type foundryRecordType) {
        this.foundryRecordTypes.remove(foundryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Foundry </code> search record types. 
     *
     *  @return a list containing the supported <code> Foundry </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFoundrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.foundrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Foundry </code> search record type is 
     *  supported. 
     *
     *  @param  foundrySearchRecordType a <code> Type </code> indicating a 
     *          <code> Foundry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> foundrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFoundrySearchRecordType(org.osid.type.Type foundrySearchRecordType) {
        return (this.foundrySearchRecordTypes.contains(foundrySearchRecordType));
    }


    /**
     *  Adds support for a foundry search record type.
     *
     *  @param foundrySearchRecordType a foundry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>foundrySearchRecordType</code> is <code>null</code>
     */

    protected void addFoundrySearchRecordType(org.osid.type.Type foundrySearchRecordType) {
        this.foundrySearchRecordTypes.add(foundrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a foundry search record type.
     *
     *  @param foundrySearchRecordType a foundry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>foundrySearchRecordType</code> is <code>null</code>
     */

    protected void removeFoundrySearchRecordType(org.osid.type.Type foundrySearchRecordType) {
        this.foundrySearchRecordTypes.remove(foundrySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my work 
     *  service. 
     *
     *  @return a <code> MyWorkSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyWork() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.MyWorkSession getMyWorkSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getMyWorkSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my work 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyWorkSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyWork() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.MyWorkSession getMyWorkSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getMyWorkSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my work 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the foundry 
     *  @return a <code> MyWorkSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyWork() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.MyWorkSession getMyWorkSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getMyWorkSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my work 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the foundry 
     *  @param  proxy a proxy 
     *  @return a <code> MyWorkSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyWork() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.MyWorkSession getMyWorkSessionForFoundry(org.osid.id.Id foundryId, 
                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getMyWorkSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job lookup 
     *  service. 
     *
     *  @return a <code> JobLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobLookupSession getJobLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobLookupSession getJobLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job lookup 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobLookupSession getJobLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job lookup 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobLookupSession getJobLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job query 
     *  service. 
     *
     *  @return a <code> JobQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuerySession getJobQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuerySession getJobQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job query 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuerySession getJobQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job query 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuerySession getJobQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job search 
     *  service. 
     *
     *  @return a <code> JobSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSearchSession getJobSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSearchSession getJobSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job search 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobSearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSearchSession getJobSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job search 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobSearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSearchSession getJobSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  administration service. 
     *
     *  @return a <code> JobAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobAdminSession getJobAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobAdminSession getJobAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobAdminSession getJobAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobAdminSession getJobAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  notification service. 
     *
     *  @param  jobReceiver the notification callback 
     *  @return a <code> JobNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> jobReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobNotificationSession getJobNotificationSession(org.osid.resourcing.JobReceiver jobReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  notification service. 
     *
     *  @param  jobReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> JobNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> jobReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobNotificationSession getJobNotificationSession(org.osid.resourcing.JobReceiver jobReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  notification service for the given foundry. 
     *
     *  @param  jobReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> jobReceiver </code> or 
     *          <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobNotificationSession getJobNotificationSessionForFoundry(org.osid.resourcing.JobReceiver jobReceiver, 
                                                                                          org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  notification service for the given foundry. 
     *
     *  @param  jobReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> jobReceiver, foundryId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobNotificationSession getJobNotificationSessionForFoundry(org.osid.resourcing.JobReceiver jobReceiver, 
                                                                                          org.osid.id.Id foundryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job/foundry mappings. 
     *
     *  @return a <code> JobFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobFoundry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobFoundrySession getJobFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job/foundry mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobFoundry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobFoundrySession getJobFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning jobs to 
     *  foundries. 
     *
     *  @return a <code> JobFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobFoundryAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobFoundryAssignmentSession getJobFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning jobs to 
     *  foundries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobFoundryAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobFoundryAssignmentSession getJobFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobSmartFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSmartFoundrySession getJobSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getJobSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobSmartFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSmartFoundrySession getJobSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getJobSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service. 
     *
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkLookupSession getWorkLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkLookupSession getWorkLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkLookupSession getWorkLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkLookupSession getWorkLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service. 
     *
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuerySession getWorkQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuerySession getWorkQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuerySession getWorkQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuerySession getWorkQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service. 
     *
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSearchSession getWorkSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSearchSession getWorkSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSearchSession getWorkSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSearchSession getWorkSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service. 
     *
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkAdminSession getWorkAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkAdminSession getWorkAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkAdminSession getWorkAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkAdminSession getWorkAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service. 
     *
     *  @param  workReceiver the notification callback 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkNotificationSession getWorkNotificationSession(org.osid.resourcing.WorkReceiver workReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service. 
     *
     *  @param  workReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkNotificationSession getWorkNotificationSession(org.osid.resourcing.WorkReceiver workReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service for the given foundry. 
     *
     *  @param  workReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver </code> or 
     *          <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkNotificationSession getWorkNotificationSessionForFoundry(org.osid.resourcing.WorkReceiver workReceiver, 
                                                                                            org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service for the given foundry. 
     *
     *  @param  workReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver, foundryId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkNotificationSession getWorkNotificationSessionForFoundry(org.osid.resourcing.WorkReceiver workReceiver, 
                                                                                            org.osid.id.Id foundryId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup work/foundry mappings. 
     *
     *  @return a <code> WorkFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkFoundry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkFoundrySession getWorkFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup work/foundry mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkFoundry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkFoundrySession getWorkFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning work to 
     *  foundries. 
     *
     *  @return a <code> WorkFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkbFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkFoundryAssignmentSession getWorkFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning work to 
     *  foundries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkbFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkFoundryAssignmentSession getWorkFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage work smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkSmartFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSmartFoundrySession getWorkSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getWorkSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage work smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException no <code> Foundry </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkSmartFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSmartFoundrySession getWorkSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getWorkSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  lookup service. 
     *
     *  @return a <code> CompetencyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyLookupSession getCompetencyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyLookupSession getCompetencyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencyLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyLookupSession getCompetencyLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyLookupSession getCompetencyLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  query service. 
     *
     *  @return a <code> CompetencyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuerySession getCompetencyQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuerySession getCompetencyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencyQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuerySession getCompetencyQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuerySession getCompetencyQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  search service. 
     *
     *  @return a <code> CompetencySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySearchSession getCompetencySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompetencySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySearchSession getCompetencySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  search service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencySearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySearchSession getCompetencySearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencySearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  search service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompetencySearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySearchSession getCompetencySearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencySearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  administration service. 
     *
     *  @return a <code> CompetencyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyAdminSession getCompetencyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyAdminSession getCompetencyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencyAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyAdminSession getCompetencyAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyAdminSession getCompetencyAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  notification service. 
     *
     *  @param  competencyReceiver the notification callback 
     *  @return a <code> CompetencyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> competencyReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyNotificationSession getCompetencyNotificationSession(org.osid.resourcing.CompetencyReceiver competencyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  notification service. 
     *
     *  @param  competencyReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> competencyReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyNotificationSession getCompetencyNotificationSession(org.osid.resourcing.CompetencyReceiver competencyReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  notification service for the given foundry. 
     *
     *  @param  competencyReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencyNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> competencyReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyNotificationSession getCompetencyNotificationSessionForFoundry(org.osid.resourcing.CompetencyReceiver competencyReceiver, 
                                                                                                        org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  notification service for the given foundry. 
     *
     *  @param  competencyReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> competencyReceiver, 
     *          foundryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyNotificationSession getCompetencyNotificationSessionForFoundry(org.osid.resourcing.CompetencyReceiver competencyReceiver, 
                                                                                                        org.osid.id.Id foundryId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup competency/foundry 
     *  mappings. 
     *
     *  @return a <code> CompetencyFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyFoundrySession getCompetencyFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup competency/foundry 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyFoundrySession getCompetencyFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to foundries. 
     *
     *  @return a <code> CompetencyFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyFoundryAssignmentSession getCompetencyFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencyFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyFoundryAssignmentSession getCompetencyFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencyFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage competency smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the foundry 
     *  @return a <code> CompetencySmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencySmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySmartFoundrySession getCompetencySmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCompetencySmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage competency smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the foundry 
     *  @param  proxy a proxy 
     *  @return a <code> CompetencySmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencySmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySmartFoundrySession getCompetencySmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCompetencySmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  lookup service. 
     *
     *  @return an <code> AvailabilityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityLookupSession getAvailabilityLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityLookupSession getAvailabilityLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the foundry 
     *  @return an <code> AvailabilityLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityLookupSession getAvailabilityLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the foundry 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityLookupSession getAvailabilityLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  query service. 
     *
     *  @return an <code> AvailabilityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuerySession getAvailabilityQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuerySession getAvailabilityQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuerySession getAvailabilityQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuerySession getAvailabilityQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  search service. 
     *
     *  @return an <code> AvailabilitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilitySearchSession getAvailabilitySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilitySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilitySearchSession getAvailabilitySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  search service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilitySearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilitySearchSession getAvailabilitySearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilitySearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  search service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilitySearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilitySearchSession getAvailabilitySearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilitySearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  administration service. 
     *
     *  @return an <code> AvailabilityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityAdminSession getAvailabilityAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityAdminSession getAvailabilityAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityAdminSession getAvailabilityAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityAdminSession getAvailabilityAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  notification service. 
     *
     *  @param  availabilityReceiver the notification callback 
     *  @return an <code> AvailabilityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> availabilityReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityNotificationSession getAvailabilityNotificationSession(org.osid.resourcing.AvailabilityReceiver availabilityReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  notification service. 
     *
     *  @param  availabilityReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> availabilityReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityNotificationSession getAvailabilityNotificationSession(org.osid.resourcing.AvailabilityReceiver availabilityReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  notification service for the given foundry. 
     *
     *  @param  availabilityReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> availabilityReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityNotificationSession getAvailabilityNotificationSessionForFoundry(org.osid.resourcing.AvailabilityReceiver availabilityReceiver, 
                                                                                                            org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  notification service for the given foundry. 
     *
     *  @param  availabilityReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> availabilityReceiver, 
     *          </code> <code> foundryId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityNotificationSession getAvailabilityNotificationSessionForFoundry(org.osid.resourcing.AvailabilityReceiver availabilityReceiver, 
                                                                                                            org.osid.id.Id foundryId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup availability/foundry 
     *  mappings. 
     *
     *  @return an <code> AvailabilityFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityFoundrySession getAvailabilityFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup availability/foundry 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityFoundrySession getAvailabilityFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  availabilities to foundries. 
     *
     *  @return an <code> AvailabilityFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityFoundryAssignmentSession getAvailabilityFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilityFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  availabilities to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityFoundryAssignmentSession getAvailabilityFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilityFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilitySmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilitySmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilitySmartFoundrySession getAvailabilitySmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getAvailabilitySmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilitySmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilitySmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilitySmartFoundrySession getAvailabilitySmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getAvailabilitySmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  lookup service. 
     *
     *  @return a <code> CommissionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionLookupSession getCommissionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionLookupSession getCommissionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionLookupSession getCommissionLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionLookupSession getCommissionLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  query service. 
     *
     *  @return a <code> CommissionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuerySession getCommissionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuerySession getCommissionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CCommissionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuerySession getCommissionQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuerySession getCommissionQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  search service. 
     *
     *  @return a <code> CommissionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSearchSession getCommissionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSearchSession getCommissionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  search service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSearchSession getCommissionSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  search service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSearchSession getCommissionSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  administrative service. 
     *
     *  @return a <code> CommissionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionAdminSession getCommissionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionAdminSession getCommissionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  administrative service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionAdminSession getCommissionAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  administrative service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionAdminSession getCommissionAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  notification service. 
     *
     *  @param  commissionReceiver the notification callback 
     *  @return a <code> CommissionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> commissionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionNotificationSession getCommissionNotificationSession(org.osid.resourcing.CommissionReceiver commissionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  notification service. 
     *
     *  @param  commissionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> commissionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionNotificationSession getCommissionNotificationSession(org.osid.resourcing.CommissionReceiver commissionReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  notification service for the given foundry. 
     *
     *  @param  commissionReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> commissionReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionNotificationSession getCommissionNotificationSessionForFoundry(org.osid.resourcing.CommissionReceiver commissionReceiver, 
                                                                                                        org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  notification service for the given foundry. 
     *
     *  @param  commissionReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> commissionReceiver, 
     *          foundryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionNotificationSession getCommissionNotificationSessionForFoundry(org.osid.resourcing.CommissionReceiver commissionReceiver, 
                                                                                                        org.osid.id.Id foundryId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup commission/foundry 
     *  mappings. 
     *
     *  @return a <code> CommissionFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionFoundrySession getCommissionFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup commission/foundry 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionFoundrySession getCommissionFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to foundries. 
     *
     *  @return a <code> CommissionyFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionFoundryAssignmentSession getCommissionFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionyFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionFoundryAssignmentSession getCommissionFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage commission smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionSmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSmartFoundrySession getCommissionSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getCommissionSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage commission smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionSmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSmartFoundrySession getCommissionSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getCommissionSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort lookup 
     *  service. 
     *
     *  @return an <code> EffortLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortLookupSession getEffortLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EffortLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortLookupSession getEffortLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort lookup 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortLookupSession getEffortLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort lookup 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EffortLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortLookupSession getEffortLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort query 
     *  service. 
     *
     *  @return an <code> EffortQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQuerySession getEffortQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EffortQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQuerySession getEffortQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort query 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQuerySession getEffortQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort query 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EffortQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQuerySession getEffortQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort search 
     *  service. 
     *
     *  @return an <code> EffortSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortSearchSession getEffortSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EffortSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortSearchSession getEffortSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort search 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortSearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortSearchSession getEffortSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort search 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EffortSearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortSearchSession getEffortSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  administration service. 
     *
     *  @return an <code> EffortAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortAdminSession getEffortAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EffortAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortAdminSession getEffortAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortAdminSession getEffortAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EffortAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortAdminSession getEffortAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  notification service. 
     *
     *  @param  effortReceiver the notification callback 
     *  @return an <code> EffortNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> effortReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortNotificationSession getEffortNotificationSession(org.osid.resourcing.EffortReceiver effortReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  notification service. 
     *
     *  @param  effortReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> EffortNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> effortReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortNotificationSession getEffortNotificationSession(org.osid.resourcing.EffortReceiver effortReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  notification service for the given foundry. 
     *
     *  @param  effortReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> effortReceiver </code> 
     *          or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortNotificationSession getEffortNotificationSessionForFoundry(org.osid.resourcing.EffortReceiver effortReceiver, 
                                                                                                org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  notification service for the given foundry. 
     *
     *  @param  effortReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EffortNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> effortReceiver, 
     *          foundryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortNotificationSession getEffortNotificationSessionForFoundry(org.osid.resourcing.EffortReceiver effortReceiver, 
                                                                                                org.osid.id.Id foundryId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup effort/foundry mappings. 
     *
     *  @return an <code> EffortFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortFoundry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortFoundrySession getEffortFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup effort/foundry mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EffortFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortFoundry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortFoundrySession getEffortFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning efforts 
     *  to foundries. 
     *
     *  @return an <code> EffortFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortFoundryAssignmentSession getEffortFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning efforts 
     *  to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EffortFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortFoundryAssignmentSession getEffortFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage effort smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortSmartFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortFoundrySession getEffortSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getEffortSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage effort smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EffortSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortSmartFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortFoundrySession getEffortSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getEffortSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry lookup 
     *  service. 
     *
     *  @return a <code> FoundryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryLookupSession getFoundryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getFoundryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FoundryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryLookupSession getFoundryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getFoundryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry query 
     *  service. 
     *
     *  @return a <code> FoundryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuerySession getFoundryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getFoundryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FoundryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuerySession getFoundryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getFoundryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry search 
     *  service. 
     *
     *  @return a <code> FoundrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundrySearchSession getFoundrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getFoundrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FoundrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundrySearchSession getFoundrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getFoundrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  administrative service. 
     *
     *  @return a <code> FoundryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryAdminSession getFoundryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getFoundryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FoundryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryAdminSession getFoundryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getFoundryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  notification service. 
     *
     *  @param  foundryReceiver the notification callback 
     *  @return a <code> FoundryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> foundryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryNotificationSession getFoundryNotificationSession(org.osid.resourcing.FoundryReceiver foundryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getFoundryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  notification service. 
     *
     *  @param  foundryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> FoundryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> foundryReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryNotificationSession getFoundryNotificationSession(org.osid.resourcing.FoundryReceiver foundryReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getFoundryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  hierarchy service. 
     *
     *  @return a <code> FoundryHierarchySession </code> for foundries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryHierarchySession getFoundryHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getFoundryHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FoundryHierarchySession </code> for foundries 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryHierarchySession getFoundryHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getFoundryHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for foundries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryHierarchyDesignSession getFoundryHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getFoundryHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for foundries 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryHierarchyDesignSession getFoundryHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getFoundryHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> ResourcingBatchManager. </code> 
     *
     *  @return a <code> ResourcingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.ResourcingBatchManager getResourcingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getResourcingBatchManager not implemented");
    }


    /**
     *  Gets the <code> ResourcingBatchProxyManager. </code> 
     *
     *  @return a <code> ResourcingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.ResourcingBatchProxyManager getResourcingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getResourcingBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> ResourcingRulesManager. </code> 
     *
     *  @return a <code> ResourcingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.ResourcingRulesManager getResourcingRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingManager.getResourcingRulesManager not implemented");
    }


    /**
     *  Gets the <code> ResourcingRulesProxyManager. </code> 
     *
     *  @return a <code> ResourcingRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.ResourcingRulesProxyManager getResourcingRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.ResourcingProxyManager.getResourcingRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.jobRecordTypes.clear();
        this.jobRecordTypes.clear();

        this.jobSearchRecordTypes.clear();
        this.jobSearchRecordTypes.clear();

        this.workRecordTypes.clear();
        this.workRecordTypes.clear();

        this.workSearchRecordTypes.clear();
        this.workSearchRecordTypes.clear();

        this.competencyRecordTypes.clear();
        this.competencyRecordTypes.clear();

        this.competencySearchRecordTypes.clear();
        this.competencySearchRecordTypes.clear();

        this.availabilityRecordTypes.clear();
        this.availabilityRecordTypes.clear();

        this.availabilitySearchRecordTypes.clear();
        this.availabilitySearchRecordTypes.clear();

        this.commissionRecordTypes.clear();
        this.commissionRecordTypes.clear();

        this.commissionSearchRecordTypes.clear();
        this.commissionSearchRecordTypes.clear();

        this.effortRecordTypes.clear();
        this.effortRecordTypes.clear();

        this.effortSearchRecordTypes.clear();
        this.effortSearchRecordTypes.clear();

        this.foundryRecordTypes.clear();
        this.foundryRecordTypes.clear();

        this.foundrySearchRecordTypes.clear();
        this.foundrySearchRecordTypes.clear();

        return;
    }
}
