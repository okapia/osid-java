//
// AbstractBrokerConstrainerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBrokerConstrainerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.rules.BrokerConstrainerSearchResults {

    private org.osid.provisioning.rules.BrokerConstrainerList brokerConstrainers;
    private final org.osid.provisioning.rules.BrokerConstrainerQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBrokerConstrainerSearchResults.
     *
     *  @param brokerConstrainers the result set
     *  @param brokerConstrainerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>brokerConstrainers</code>
     *          or <code>brokerConstrainerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBrokerConstrainerSearchResults(org.osid.provisioning.rules.BrokerConstrainerList brokerConstrainers,
                                            org.osid.provisioning.rules.BrokerConstrainerQueryInspector brokerConstrainerQueryInspector) {
        nullarg(brokerConstrainers, "broker constrainers");
        nullarg(brokerConstrainerQueryInspector, "broker constrainer query inspectpr");

        this.brokerConstrainers = brokerConstrainers;
        this.inspector = brokerConstrainerQueryInspector;

        return;
    }


    /**
     *  Gets the broker constrainer list resulting from a search.
     *
     *  @return a broker constrainer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainers() {
        if (this.brokerConstrainers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.rules.BrokerConstrainerList brokerConstrainers = this.brokerConstrainers;
        this.brokerConstrainers = null;
	return (brokerConstrainers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.rules.BrokerConstrainerQueryInspector getBrokerConstrainerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  broker constrainer search record <code> Type. </code> This method must
     *  be used to retrieve a brokerConstrainer implementing the requested
     *  record.
     *
     *  @param brokerConstrainerSearchRecordType a brokerConstrainer search 
     *         record type 
     *  @return the broker constrainer search
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(brokerConstrainerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerSearchResultsRecord getBrokerConstrainerSearchResultsRecord(org.osid.type.Type brokerConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.rules.records.BrokerConstrainerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(brokerConstrainerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(brokerConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record broker constrainer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBrokerConstrainerRecord(org.osid.provisioning.rules.records.BrokerConstrainerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "broker constrainer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
