//
// InvariantMapTrustLookupSession
//
//    Implements a Trust lookup service backed by a fixed collection of
//    trusts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.process;


/**
 *  Implements a Trust lookup service backed by a fixed
 *  collection of trusts. The trusts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapTrustLookupSession
    extends net.okapia.osid.jamocha.core.authentication.process.spi.AbstractMapTrustLookupSession
    implements org.osid.authentication.process.TrustLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapTrustLookupSession</code> with no
     *  trusts.
     *  
     *  @param agency the agency
     *  @throws org.osid.NullArgumnetException {@code agency} is
     *          {@code null}
     */

    public InvariantMapTrustLookupSession(org.osid.authentication.Agency agency) {
        setAgency(agency);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTrustLookupSession</code> with a single
     *  trust.
     *  
     *  @param agency the agency
     *  @param trust a single trust
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code trust} is <code>null</code>
     */

      public InvariantMapTrustLookupSession(org.osid.authentication.Agency agency,
                                               org.osid.authentication.process.Trust trust) {
        this(agency);
        putTrust(trust);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTrustLookupSession</code> using an array
     *  of trusts.
     *  
     *  @param agency the agency
     *  @param trusts an array of trusts
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code trusts} is <code>null</code>
     */

      public InvariantMapTrustLookupSession(org.osid.authentication.Agency agency,
                                               org.osid.authentication.process.Trust[] trusts) {
        this(agency);
        putTrusts(trusts);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTrustLookupSession</code> using a
     *  collection of trusts.
     *
     *  @param agency the agency
     *  @param trusts a collection of trusts
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code trusts} is <code>null</code>
     */

      public InvariantMapTrustLookupSession(org.osid.authentication.Agency agency,
                                               java.util.Collection<? extends org.osid.authentication.process.Trust> trusts) {
        this(agency);
        putTrusts(trusts);
        return;
    }
}
