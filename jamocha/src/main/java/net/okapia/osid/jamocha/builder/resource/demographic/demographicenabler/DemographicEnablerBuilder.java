//
// DemographicEnabler.java
//
//     Defines a DemographicEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.demographic.demographicenabler;


/**
 *  Defines a <code>DemographicEnabler</code> builder.
 */

public final class DemographicEnablerBuilder
    extends net.okapia.osid.jamocha.builder.resource.demographic.demographicenabler.spi.AbstractDemographicEnablerBuilder<DemographicEnablerBuilder> {
    

    /**
     *  Constructs a new <code>DemographicEnablerBuilder</code> using a
     *  <code>MutableDemographicEnabler</code>.
     */

    public DemographicEnablerBuilder() {
        super(new MutableDemographicEnabler());
        return;
    }


    /**
     *  Constructs a new <code>DemographicEnablerBuilder</code> using the given
     *  mutable demographicEnabler.
     * 
     *  @param demographicEnabler
     */

    public DemographicEnablerBuilder(DemographicEnablerMiter demographicEnabler) {
        super(demographicEnabler);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return DemographicEnablerBuilder
     */

    @Override
    protected DemographicEnablerBuilder self() {
        return (this);
    }
}       


