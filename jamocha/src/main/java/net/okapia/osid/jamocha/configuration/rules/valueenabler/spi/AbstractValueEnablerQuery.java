//
// AbstractValueEnablerQuery.java
//
//     A template for making a ValueEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.valueenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for value enablers.
 */

public abstract class AbstractValueEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.configuration.rules.ValueEnablerQuery {

    private final java.util.Collection<org.osid.configuration.rules.records.ValueEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the value. 
     *
     *  @param  valueId the value <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> valueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledValueId(org.osid.id.Id valueId, boolean match) {
        return;
    }


    /**
     *  Clears the value <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledValueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ValueQuery </code> is available. 
     *
     *  @return <code> true </code> if a value query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledValueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a value. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the value query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleValueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuery getRuledValueQuery() {
        throw new org.osid.UnimplementedException("supportsRuledValueQuery() is false");
    }


    /**
     *  Matches enablers mapped to any value. 
     *
     *  @param  match <code> true </code> for enablers mapped to any value, 
     *          <code> false </code> to match enablers mapped to no value 
     */

    @OSID @Override
    public void matchAnyRuledValue(boolean match) {
        return;
    }


    /**
     *  Clears the value query terms. 
     */

    @OSID @Override
    public void clearRuledValueTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the configuration. 
     *
     *  @param  configurationId the configuration <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration query terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given value enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a value enabler implementing the requested record.
     *
     *  @param valueEnablerRecordType a value enabler record type
     *  @return the value enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ValueEnablerQueryRecord getValueEnablerQueryRecord(org.osid.type.Type valueEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ValueEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(valueEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this value enabler query. 
     *
     *  @param valueEnablerQueryRecord value enabler query record
     *  @param valueEnablerRecordType valueEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addValueEnablerQueryRecord(org.osid.configuration.rules.records.ValueEnablerQueryRecord valueEnablerQueryRecord, 
                                          org.osid.type.Type valueEnablerRecordType) {

        addRecordType(valueEnablerRecordType);
        nullarg(valueEnablerQueryRecord, "value enabler query record");
        this.records.add(valueEnablerQueryRecord);        
        return;
    }
}
