//
// AbstractJobProcessorEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractJobProcessorEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.rules.JobProcessorEnablerSearchResults {

    private org.osid.resourcing.rules.JobProcessorEnablerList jobProcessorEnablers;
    private final org.osid.resourcing.rules.JobProcessorEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractJobProcessorEnablerSearchResults.
     *
     *  @param jobProcessorEnablers the result set
     *  @param jobProcessorEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnablers</code>
     *          or <code>jobProcessorEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractJobProcessorEnablerSearchResults(org.osid.resourcing.rules.JobProcessorEnablerList jobProcessorEnablers,
                                            org.osid.resourcing.rules.JobProcessorEnablerQueryInspector jobProcessorEnablerQueryInspector) {
        nullarg(jobProcessorEnablers, "job processor enablers");
        nullarg(jobProcessorEnablerQueryInspector, "job processor enabler query inspectpr");

        this.jobProcessorEnablers = jobProcessorEnablers;
        this.inspector = jobProcessorEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the job processor enabler list resulting from a search.
     *
     *  @return a job processor enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablers() {
        if (this.jobProcessorEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.rules.JobProcessorEnablerList jobProcessorEnablers = this.jobProcessorEnablers;
        this.jobProcessorEnablers = null;
	return (jobProcessorEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.rules.JobProcessorEnablerQueryInspector getJobProcessorEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  job processor enabler search record <code> Type. </code> This method must
     *  be used to retrieve a jobProcessorEnabler implementing the requested
     *  record.
     *
     *  @param jobProcessorEnablerSearchRecordType a jobProcessorEnabler search 
     *         record type 
     *  @return the job processor enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(jobProcessorEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorEnablerSearchResultsRecord getJobProcessorEnablerSearchResultsRecord(org.osid.type.Type jobProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.rules.records.JobProcessorEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(jobProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(jobProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record job processor enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addJobProcessorEnablerRecord(org.osid.resourcing.rules.records.JobProcessorEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "job processor enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
