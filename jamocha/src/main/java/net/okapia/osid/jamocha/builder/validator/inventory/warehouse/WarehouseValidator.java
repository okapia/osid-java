//
// WarehouseValidator.java
//
//     Validates a Warehouse.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.inventory.warehouse;


/**
 *  Validates a Warehouse.
 */

public final class WarehouseValidator
    extends net.okapia.osid.jamocha.builder.validator.inventory.warehouse.spi.AbstractWarehouseValidator {


    /**
     *  Constructs a new <code>WarehouseValidator</code>.
     */

    public WarehouseValidator() {
        return;
    }


    /**
     *  Constructs a new <code>WarehouseValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public WarehouseValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Warehouse with a default validation.
     *
     *  @param warehouse a warehouse to validate
     *  @return the warehouse
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.inventory.Warehouse validateWarehouse(org.osid.inventory.Warehouse warehouse) {
        WarehouseValidator validator = new WarehouseValidator();
        validator.validate(warehouse);
        return (warehouse);
    }


    /**
     *  Validates a Warehouse for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param warehouse a warehouse to validate
     *  @return the warehouse
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>warehouse</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.inventory.Warehouse validateWarehouse(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.inventory.Warehouse warehouse) {

        WarehouseValidator validator = new WarehouseValidator(validation);
        validator.validate(warehouse);
        return (warehouse);
    }
}
