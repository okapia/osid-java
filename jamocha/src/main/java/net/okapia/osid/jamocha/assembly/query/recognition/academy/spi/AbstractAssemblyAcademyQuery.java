//
// AbstractAssemblyAcademyQuery.java
//
//     An AcademyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.recognition.academy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AcademyQuery that stores terms.
 */

public abstract class AbstractAssemblyAcademyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.recognition.AcademyQuery,
               org.osid.recognition.AcademyQueryInspector,
               org.osid.recognition.AcademySearchOrder {

    private final java.util.Collection<org.osid.recognition.records.AcademyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recognition.records.AcademyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recognition.records.AcademySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAcademyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAcademyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the conferral <code> Id </code> for this query to match 
     *  conferrals assigned to academies. 
     *
     *  @param  conferralId a conferral <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> conferralId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConferralId(org.osid.id.Id conferralId, boolean match) {
        getAssembler().addIdTerm(getConferralIdColumn(), conferralId, match);
        return;
    }


    /**
     *  Clears the conferral <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConferralIdTerms() {
        getAssembler().clearTerms(getConferralIdColumn());
        return;
    }


    /**
     *  Gets the conferral <code> Id </code> terms. 
     *
     *  @return the conferral <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConferralIdTerms() {
        return (getAssembler().getIdTerms(getConferralIdColumn()));
    }


    /**
     *  Gets the ConferralId column name.
     *
     * @return the column name
     */

    protected String getConferralIdColumn() {
        return ("conferral_id");
    }


    /**
     *  Tests if a conferral query is available. 
     *
     *  @return <code> true </code> if a conferral query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralQuery() {
        return (false);
    }


    /**
     *  Gets the query for an conferral. 
     *
     *  @return the conferral query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuery getConferralQuery() {
        throw new org.osid.UnimplementedException("supportsConferralQuery() is false");
    }


    /**
     *  Matches academies with any conferral. 
     *
     *  @param  match <code> true </code> to match academies with any 
     *          conferral, <code> false </code> to match academies with no 
     *          conferrals 
     */

    @OSID @Override
    public void matchAnyConferral(boolean match) {
        getAssembler().addIdWildcardTerm(getConferralColumn(), match);
        return;
    }


    /**
     *  Clears the conferral terms. 
     */

    @OSID @Override
    public void clearConferralTerms() {
        getAssembler().clearTerms(getConferralColumn());
        return;
    }


    /**
     *  Gets the conferral terms. 
     *
     *  @return the conferral terms 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQueryInspector[] getConferralTerms() {
        return (new org.osid.recognition.ConferralQueryInspector[0]);
    }


    /**
     *  Gets the Conferral column name.
     *
     * @return the column name
     */

    protected String getConferralColumn() {
        return ("conferral");
    }


    /**
     *  Sets the award <code> Id </code> for this query to match conferrals 
     *  assigned to awards. 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        getAssembler().addIdTerm(getAwardIdColumn(), awardId, match);
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        getAssembler().clearTerms(getAwardIdColumn());
        return;
    }


    /**
     *  Gets the award <code> Id </code> terms. 
     *
     *  @return the award <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (getAssembler().getIdTerms(getAwardIdColumn()));
    }


    /**
     *  Gets the AwardId column name.
     *
     * @return the column name
     */

    protected String getAwardIdColumn() {
        return ("award_id");
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Matches academies with any award. 
     *
     *  @param  match <code> true </code> to match academies with any award, 
     *          <code> false </code> to match academies with no awards 
     */

    @OSID @Override
    public void matchAnyAward(boolean match) {
        getAssembler().addIdWildcardTerm(getAwardColumn(), match);
        return;
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        getAssembler().clearTerms(getAwardColumn());
        return;
    }


    /**
     *  Gets the award terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Gets the Award column name.
     *
     * @return the column name
     */

    protected String getAwardColumn() {
        return ("award");
    }


    /**
     *  Sets a convocaton <code> Id. </code> 
     *
     *  @param  convocationId a convocaton <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> convocationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConvocationId(org.osid.id.Id convocationId, boolean match) {
        getAssembler().addIdTerm(getConvocationIdColumn(), convocationId, match);
        return;
    }


    /**
     *  Clears the convocaton <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConvocationIdTerms() {
        getAssembler().clearTerms(getConvocationIdColumn());
        return;
    }


    /**
     *  Gets the convocation <code> Id </code> terms. 
     *
     *  @return the convocation <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConvocationIdTerms() {
        return (getAssembler().getIdTerms(getConvocationIdColumn()));
    }


    /**
     *  Gets the ConvocationId column name.
     *
     * @return the column name
     */

    protected String getConvocationIdColumn() {
        return ("convocation_id");
    }


    /**
     *  Tests if a <code> ConvocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a convocaton query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a convocaton query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the convocaton query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuery getConvocationQuery() {
        throw new org.osid.UnimplementedException("supportsConvocationQuery() is false");
    }


    /**
     *  Matches any convocaton. 
     *
     *  @param  match <code> true </code> to match academies with any 
     *          convocation, <code> false </code> to match academies with no 
     *          convocations 
     */

    @OSID @Override
    public void matchAnyConvocation(boolean match) {
        getAssembler().addIdWildcardTerm(getConvocationColumn(), match);
        return;
    }


    /**
     *  Clears the convocaton terms. 
     */

    @OSID @Override
    public void clearConvocationTerms() {
        getAssembler().clearTerms(getConvocationColumn());
        return;
    }


    /**
     *  Gets the convocation terms. 
     *
     *  @return the convocation terms 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQueryInspector[] getConvocationTerms() {
        return (new org.osid.recognition.ConvocationQueryInspector[0]);
    }


    /**
     *  Gets the Convocation column name.
     *
     * @return the column name
     */

    protected String getConvocationColumn() {
        return ("convocation");
    }


    /**
     *  Sets the academy <code> Id </code> for this query to match academies 
     *  that have the specified academy as an ancestor. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAcademyId(org.osid.id.Id academyId, boolean match) {
        getAssembler().addIdTerm(getAncestorAcademyIdColumn(), academyId, match);
        return;
    }


    /**
     *  Clears the ancestor academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorAcademyIdTerms() {
        getAssembler().clearTerms(getAncestorAcademyIdColumn());
        return;
    }


    /**
     *  Gets the ancestor academy <code> Id </code> terms. 
     *
     *  @return the ancestor academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAcademyIdTerms() {
        return (getAssembler().getIdTerms(getAncestorAcademyIdColumn()));
    }


    /**
     *  Gets the AncestorAcademyId column name.
     *
     * @return the column name
     */

    protected String getAncestorAcademyIdColumn() {
        return ("ancestor_academy_id");
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAcademyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getAncestorAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAcademyQuery() is false");
    }


    /**
     *  Matches academies with any ancestor. 
     *
     *  @param  match <code> true </code> to match academies with any 
     *          ancestor, <code> false </code> to match root academies 
     */

    @OSID @Override
    public void matchAnyAncestorAcademy(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorAcademyColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor academy terms. 
     */

    @OSID @Override
    public void clearAncestorAcademyTerms() {
        getAssembler().clearTerms(getAncestorAcademyColumn());
        return;
    }


    /**
     *  Gets the ancestor academy terms. 
     *
     *  @return the ancestor academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getAncestorAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }


    /**
     *  Gets the AncestorAcademy column name.
     *
     * @return the column name
     */

    protected String getAncestorAcademyColumn() {
        return ("ancestor_academy");
    }


    /**
     *  Sets the academy <code> Id </code> for this query to match academies 
     *  that have the specified academy as a descendant. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAcademyId(org.osid.id.Id academyId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantAcademyIdColumn(), academyId, match);
        return;
    }


    /**
     *  Clears the descendant academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantAcademyIdTerms() {
        getAssembler().clearTerms(getDescendantAcademyIdColumn());
        return;
    }


    /**
     *  Gets the descendant academy <code> Id </code> terms. 
     *
     *  @return the descendant academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAcademyIdTerms() {
        return (getAssembler().getIdTerms(getDescendantAcademyIdColumn()));
    }


    /**
     *  Gets the DescendantAcademyId column name.
     *
     * @return the column name
     */

    protected String getDescendantAcademyIdColumn() {
        return ("descendant_academy_id");
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAcademyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getDescendantAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAcademyQuery() is false");
    }


    /**
     *  Matches academies with any descendant. 
     *
     *  @param  match <code> true </code> to match academies with any 
     *          descendant, <code> false </code> to match leaf academies 
     */

    @OSID @Override
    public void matchAnyDescendantAcademy(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantAcademyColumn(), match);
        return;
    }


    /**
     *  Clears the descendant academy terms. 
     */

    @OSID @Override
    public void clearDescendantAcademyTerms() {
        getAssembler().clearTerms(getDescendantAcademyColumn());
        return;
    }


    /**
     *  Gets the descendant academy terms. 
     *
     *  @return the descendant academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getDescendantAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }


    /**
     *  Gets the DescendantAcademy column name.
     *
     * @return the column name
     */

    protected String getDescendantAcademyColumn() {
        return ("descendant_academy");
    }


    /**
     *  Tests if this academy supports the given record
     *  <code>Type</code>.
     *
     *  @param  academyRecordType an academy record type 
     *  @return <code>true</code> if the academyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type academyRecordType) {
        for (org.osid.recognition.records.AcademyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(academyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  academyRecordType the academy record type 
     *  @return the academy query record 
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(academyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AcademyQueryRecord getAcademyQueryRecord(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AcademyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(academyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(academyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  academyRecordType the academy record type 
     *  @return the academy query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(academyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AcademyQueryInspectorRecord getAcademyQueryInspectorRecord(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AcademyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(academyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(academyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param academyRecordType the academy record type
     *  @return the academy search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(academyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AcademySearchOrderRecord getAcademySearchOrderRecord(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AcademySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(academyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(academyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this academy. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param academyQueryRecord the academy query record
     *  @param academyQueryInspectorRecord the academy query inspector
     *         record
     *  @param academySearchOrderRecord the academy search order record
     *  @param academyRecordType academy record type
     *  @throws org.osid.NullArgumentException
     *          <code>academyQueryRecord</code>,
     *          <code>academyQueryInspectorRecord</code>,
     *          <code>academySearchOrderRecord</code> or
     *          <code>academyRecordTypeacademy</code> is
     *          <code>null</code>
     */
            
    protected void addAcademyRecords(org.osid.recognition.records.AcademyQueryRecord academyQueryRecord, 
                                      org.osid.recognition.records.AcademyQueryInspectorRecord academyQueryInspectorRecord, 
                                      org.osid.recognition.records.AcademySearchOrderRecord academySearchOrderRecord, 
                                      org.osid.type.Type academyRecordType) {

        addRecordType(academyRecordType);

        nullarg(academyQueryRecord, "academy query record");
        nullarg(academyQueryInspectorRecord, "academy query inspector record");
        nullarg(academySearchOrderRecord, "academy search odrer record");

        this.queryRecords.add(academyQueryRecord);
        this.queryInspectorRecords.add(academyQueryInspectorRecord);
        this.searchOrderRecords.add(academySearchOrderRecord);
        
        return;
    }
}
