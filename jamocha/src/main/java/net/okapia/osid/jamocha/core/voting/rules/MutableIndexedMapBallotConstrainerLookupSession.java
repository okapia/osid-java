//
// MutableIndexedMapBallotConstrainerLookupSession
//
//    Implements a BallotConstrainer lookup service backed by a collection of
//    ballotConstrainers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules;


/**
 *  Implements a BallotConstrainer lookup service backed by a collection of
 *  ballot constrainers. The ballot constrainers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some ballot constrainers may be compatible
 *  with more types than are indicated through these ballot constrainer
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of ballot constrainers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapBallotConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.voting.rules.spi.AbstractIndexedMapBallotConstrainerLookupSession
    implements org.osid.voting.rules.BallotConstrainerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapBallotConstrainerLookupSession} with no ballot constrainers.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

      public MutableIndexedMapBallotConstrainerLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBallotConstrainerLookupSession} with a
     *  single ballot constrainer.
     *  
     *  @param polls the polls
     *  @param  ballotConstrainer a single ballotConstrainer
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code ballotConstrainer} is {@code null}
     */

    public MutableIndexedMapBallotConstrainerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.BallotConstrainer ballotConstrainer) {
        this(polls);
        putBallotConstrainer(ballotConstrainer);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBallotConstrainerLookupSession} using an
     *  array of ballot constrainers.
     *
     *  @param polls the polls
     *  @param  ballotConstrainers an array of ballot constrainers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code ballotConstrainers} is {@code null}
     */

    public MutableIndexedMapBallotConstrainerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.BallotConstrainer[] ballotConstrainers) {
        this(polls);
        putBallotConstrainers(ballotConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBallotConstrainerLookupSession} using a
     *  collection of ballot constrainers.
     *
     *  @param polls the polls
     *  @param  ballotConstrainers a collection of ballot constrainers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code ballotConstrainers} is {@code null}
     */

    public MutableIndexedMapBallotConstrainerLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.rules.BallotConstrainer> ballotConstrainers) {

        this(polls);
        putBallotConstrainers(ballotConstrainers);
        return;
    }
    

    /**
     *  Makes a {@code BallotConstrainer} available in this session.
     *
     *  @param  ballotConstrainer a ballot constrainer
     *  @throws org.osid.NullArgumentException {@code ballotConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putBallotConstrainer(org.osid.voting.rules.BallotConstrainer ballotConstrainer) {
        super.putBallotConstrainer(ballotConstrainer);
        return;
    }


    /**
     *  Makes an array of ballot constrainers available in this session.
     *
     *  @param  ballotConstrainers an array of ballot constrainers
     *  @throws org.osid.NullArgumentException {@code ballotConstrainers{@code 
     *          is {@code null}
     */

    @Override
    public void putBallotConstrainers(org.osid.voting.rules.BallotConstrainer[] ballotConstrainers) {
        super.putBallotConstrainers(ballotConstrainers);
        return;
    }


    /**
     *  Makes collection of ballot constrainers available in this session.
     *
     *  @param  ballotConstrainers a collection of ballot constrainers
     *  @throws org.osid.NullArgumentException {@code ballotConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putBallotConstrainers(java.util.Collection<? extends org.osid.voting.rules.BallotConstrainer> ballotConstrainers) {
        super.putBallotConstrainers(ballotConstrainers);
        return;
    }


    /**
     *  Removes a BallotConstrainer from this session.
     *
     *  @param ballotConstrainerId the {@code Id} of the ballot constrainer
     *  @throws org.osid.NullArgumentException {@code ballotConstrainerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBallotConstrainer(org.osid.id.Id ballotConstrainerId) {
        super.removeBallotConstrainer(ballotConstrainerId);
        return;
    }    
}
