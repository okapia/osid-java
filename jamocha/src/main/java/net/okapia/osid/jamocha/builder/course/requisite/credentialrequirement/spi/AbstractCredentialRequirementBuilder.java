//
// AbstractCredentialRequirement.java
//
//     Defines a CredentialRequirement builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.credentialrequirement.spi;


/**
 *  Defines a <code>CredentialRequirement</code> builder.
 */

public abstract class AbstractCredentialRequirementBuilder<T extends AbstractCredentialRequirementBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.requisite.credentialrequirement.CredentialRequirementMiter credentialRequirement;


    /**
     *  Constructs a new <code>AbstractCredentialRequirementBuilder</code>.
     *
     *  @param credentialRequirement the credential requirement to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCredentialRequirementBuilder(net.okapia.osid.jamocha.builder.course.requisite.credentialrequirement.CredentialRequirementMiter credentialRequirement) {
        super(credentialRequirement);
        this.credentialRequirement = credentialRequirement;
        return;
    }


    /**
     *  Builds the credential requirement.
     *
     *  @return the new credential requirement
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.requisite.CredentialRequirement build() {
        (new net.okapia.osid.jamocha.builder.validator.course.requisite.credentialrequirement.CredentialRequirementValidator(getValidations())).validate(this.credentialRequirement);
        return (new net.okapia.osid.jamocha.builder.course.requisite.credentialrequirement.ImmutableCredentialRequirement(this.credentialRequirement));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the credential requirement miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.requisite.credentialrequirement.CredentialRequirementMiter getMiter() {
        return (this.credentialRequirement);
    }


    /**
     *  Adds an alternative requisite.
     *
     *  @param requisite an alternative requisite
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisite</code> is <code>null</code>
     */

    public T altRequisite(org.osid.course.requisite.Requisite requisite) {
        getMiter().addAltRequisite(requisite);
        return (self());
    }


    /**
     *  Sets all the alternative requisites.
     *
     *  @param requisites a collection of alternative requisites
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>eequisites</code> is <code>null</code>
     */

    public T altRequisites(java.util.Collection<org.osid.course.requisite.Requisite> requisites) {
        getMiter().setAltRequisites(requisites);
        return (self());
    }


    /**
     *  Sets the credential.
     *
     *  @param credential a credential
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>credential</code> is <code>null</code>
     */

    public T credential(org.osid.course.program.Credential credential) {
        getMiter().setCredential(credential);
        return (self());
    }


    /**
     *  Sets the timeframe.
     *
     *  @param timeframe a timeframe
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>timeframe</code> is <code>null</code>
     */

    public T timeframe(org.osid.calendaring.Duration timeframe) {
        getMiter().setTimeframe(timeframe);
        return (self());
    }


    /**
     *  Adds a CredentialRequirement record.
     *
     *  @param record a credential requirement record
     *  @param recordType the type of credential requirement record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.requisite.records.CredentialRequirementRecord record, org.osid.type.Type recordType) {
        getMiter().addCredentialRequirementRecord(record, recordType);
        return (self());
    }
}       


