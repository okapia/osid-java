//
// AbstractMapPoolLookupSession
//
//    A simple framework for providing a Pool lookup service
//    backed by a fixed collection of pools.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Pool lookup service backed by a
 *  fixed collection of pools. The pools are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Pools</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPoolLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractPoolLookupSession
    implements org.osid.provisioning.PoolLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.Pool> pools = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.Pool>());


    /**
     *  Makes a <code>Pool</code> available in this session.
     *
     *  @param  pool a pool
     *  @throws org.osid.NullArgumentException <code>pool<code>
     *          is <code>null</code>
     */

    protected void putPool(org.osid.provisioning.Pool pool) {
        this.pools.put(pool.getId(), pool);
        return;
    }


    /**
     *  Makes an array of pools available in this session.
     *
     *  @param  pools an array of pools
     *  @throws org.osid.NullArgumentException <code>pools<code>
     *          is <code>null</code>
     */

    protected void putPools(org.osid.provisioning.Pool[] pools) {
        putPools(java.util.Arrays.asList(pools));
        return;
    }


    /**
     *  Makes a collection of pools available in this session.
     *
     *  @param  pools a collection of pools
     *  @throws org.osid.NullArgumentException <code>pools<code>
     *          is <code>null</code>
     */

    protected void putPools(java.util.Collection<? extends org.osid.provisioning.Pool> pools) {
        for (org.osid.provisioning.Pool pool : pools) {
            this.pools.put(pool.getId(), pool);
        }

        return;
    }


    /**
     *  Removes a Pool from this session.
     *
     *  @param  poolId the <code>Id</code> of the pool
     *  @throws org.osid.NullArgumentException <code>poolId<code> is
     *          <code>null</code>
     */

    protected void removePool(org.osid.id.Id poolId) {
        this.pools.remove(poolId);
        return;
    }


    /**
     *  Gets the <code>Pool</code> specified by its <code>Id</code>.
     *
     *  @param  poolId <code>Id</code> of the <code>Pool</code>
     *  @return the pool
     *  @throws org.osid.NotFoundException <code>poolId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>poolId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Pool getPool(org.osid.id.Id poolId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.Pool pool = this.pools.get(poolId);
        if (pool == null) {
            throw new org.osid.NotFoundException("pool not found: " + poolId);
        }

        return (pool);
    }


    /**
     *  Gets all <code>Pools</code>. In plenary mode, the returned
     *  list contains all known pools or an error
     *  results. Otherwise, the returned list may contain only those
     *  pools that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Pools</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPools()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.pool.ArrayPoolList(this.pools.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.pools.clear();
        super.close();
        return;
    }
}
