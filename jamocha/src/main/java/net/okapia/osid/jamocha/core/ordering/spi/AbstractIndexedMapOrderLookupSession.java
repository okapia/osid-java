//
// AbstractIndexedMapOrderLookupSession.java
//
//    A simple framework for providing an Order lookup service
//    backed by a fixed collection of orders with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Order lookup service backed by a
 *  fixed collection of orders. The orders are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some orders may be compatible
 *  with more types than are indicated through these order
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Orders</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOrderLookupSession
    extends AbstractMapOrderLookupSession
    implements org.osid.ordering.OrderLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.ordering.Order> ordersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.Order>());
    private final MultiMap<org.osid.type.Type, org.osid.ordering.Order> ordersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.Order>());


    /**
     *  Makes an <code>Order</code> available in this session.
     *
     *  @param  order an order
     *  @throws org.osid.NullArgumentException <code>order<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOrder(org.osid.ordering.Order order) {
        super.putOrder(order);

        this.ordersByGenus.put(order.getGenusType(), order);
        
        try (org.osid.type.TypeList types = order.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ordersByRecord.put(types.getNextType(), order);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an order from this session.
     *
     *  @param orderId the <code>Id</code> of the order
     *  @throws org.osid.NullArgumentException <code>orderId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOrder(org.osid.id.Id orderId) {
        org.osid.ordering.Order order;
        try {
            order = getOrder(orderId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.ordersByGenus.remove(order.getGenusType());

        try (org.osid.type.TypeList types = order.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ordersByRecord.remove(types.getNextType(), order);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOrder(orderId);
        return;
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  order genus <code>Type</code> which does not include
     *  orders of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known orders or an error results. Otherwise,
     *  the returned list may contain only those orders that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  orderGenusType an order genus type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByGenusType(org.osid.type.Type orderGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.order.ArrayOrderList(this.ordersByGenus.get(orderGenusType)));
    }


    /**
     *  Gets an <code>OrderList</code> containing the given
     *  order record <code>Type</code>. In plenary mode, the
     *  returned list contains all known orders or an error
     *  results. Otherwise, the returned list may contain only those
     *  orders that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  orderRecordType an order record type 
     *  @return the returned <code>order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByRecordType(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.order.ArrayOrderList(this.ordersByRecord.get(orderRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.ordersByGenus.clear();
        this.ordersByRecord.clear();

        super.close();

        return;
    }
}
