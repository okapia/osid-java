//
// ObjectiveBankElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objectivebank.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ObjectiveBankElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the ObjectiveBankElement Id.
     *
     *  @return the objective bank element Id
     */

    public static org.osid.id.Id getObjectiveBankEntityId() {
        return (makeEntityId("osid.learning.ObjectiveBank"));
    }


    /**
     *  Gets the ObjectiveId element Id.
     *
     *  @return the ObjectiveId element Id
     */

    public static org.osid.id.Id getObjectiveId() {
        return (makeQueryElementId("osid.learning.objectivebank.ObjectiveId"));
    }


    /**
     *  Gets the Objective element Id.
     *
     *  @return the Objective element Id
     */

    public static org.osid.id.Id getObjective() {
        return (makeQueryElementId("osid.learning.objectivebank.Objective"));
    }


    /**
     *  Gets the ActivityId element Id.
     *
     *  @return the ActivityId element Id
     */

    public static org.osid.id.Id getActivityId() {
        return (makeQueryElementId("osid.learning.objectivebank.ActivityId"));
    }


    /**
     *  Gets the Activity element Id.
     *
     *  @return the Activity element Id
     */

    public static org.osid.id.Id getActivity() {
        return (makeQueryElementId("osid.learning.objectivebank.Activity"));
    }


    /**
     *  Gets the AncestorObjectiveBankId element Id.
     *
     *  @return the AncestorObjectiveBankId element Id
     */

    public static org.osid.id.Id getAncestorObjectiveBankId() {
        return (makeQueryElementId("osid.learning.objectivebank.AncestorObjectiveBankId"));
    }


    /**
     *  Gets the AncestorObjectiveBank element Id.
     *
     *  @return the AncestorObjectiveBank element Id
     */

    public static org.osid.id.Id getAncestorObjectiveBank() {
        return (makeQueryElementId("osid.learning.objectivebank.AncestorObjectiveBank"));
    }


    /**
     *  Gets the DescendantObjectiveBankId element Id.
     *
     *  @return the DescendantObjectiveBankId element Id
     */

    public static org.osid.id.Id getDescendantObjectiveBankId() {
        return (makeQueryElementId("osid.learning.objectivebank.DescendantObjectiveBankId"));
    }


    /**
     *  Gets the DescendantObjectiveBank element Id.
     *
     *  @return the DescendantObjectiveBank element Id
     */

    public static org.osid.id.Id getDescendantObjectiveBank() {
        return (makeQueryElementId("osid.learning.objectivebank.DescendantObjectiveBank"));
    }
}
