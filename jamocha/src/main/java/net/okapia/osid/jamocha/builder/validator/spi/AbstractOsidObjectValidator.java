//
// AbstractOsidObjectValidator.java
//
//     Validates OsidObjects.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.spi;


/**
 *  Validates an OsidObject.
 */

public abstract class AbstractOsidObjectValidator
    extends AbstractIdentifiableValidator {

    private final BrowsableValidator validator;


    /**
     *  Constructs a new <code>AbstractOsidObjectValidator</code>.
     */

    protected AbstractOsidObjectValidator() {
        this.validator = new BrowsableValidator();
        return;
    }


    /**
     *  Constructs a new <code>AbstractOsidObjectValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractOsidObjectValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        this.validator = new BrowsableValidator(validation);
        return;
    }

    
    /**
     *  Validates an OsidObject.
     *
     *  @param object the object to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.NullArgumentException <code>object</code> is
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public void validate(org.osid.OsidObject object) {
        super.validate(object);
        this.validator.validate(object);

        test(object.getDisplayName(), "getDisplayName()");
        test(object.getDescription(), "getDescription()");
        test(object.getGenusType(),   "getGenusType()");

        if (!object.isOfGenusType(object.getGenusType())) {
            throw new org.osid.BadLogicException("object not of its own genus");
        }

        return;
    }


    protected class BrowsableValidator
        extends AbstractBrowsableValidator {

        protected BrowsableValidator() {
            return;
        }


        protected BrowsableValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
            super(validation);
            return;
        }

        
        public void validate(org.osid.Browsable browsable) {
            super.validate(browsable);
            return;
        }
    }
}
