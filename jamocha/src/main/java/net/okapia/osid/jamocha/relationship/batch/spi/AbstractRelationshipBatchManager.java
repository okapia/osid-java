//
// AbstractRelationshipBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRelationshipBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.relationship.batch.RelationshipBatchManager,
               org.osid.relationship.batch.RelationshipBatchProxyManager {


    /**
     *  Constructs a new
     *  <code>AbstractRelationshipBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRelationshipBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of relationships is available. 
     *
     *  @return <code> true </code> if a relationship bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of family is available. 
     *
     *  @return <code> true </code> if a family bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  relationship administration service. 
     *
     *  @return a <code> RelationshipBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.RelationshipBatchAdminSession getRelationshipBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.batch.RelationshipBatchManager.getRelationshipBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  relationship administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.RelationshipBatchAdminSession getRelationshipBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.batch.RelationshipBatchProxyManager.getRelationshipBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  relationship administration service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.RelationshipBatchAdminSession getRelationshipBatchAdminSessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.batch.RelationshipBatchManager.getRelationshipBatchAdminSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  relationship administration service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.RelationshipBatchAdminSession getRelationshipBatchAdminSessionForFamily(org.osid.id.Id familyId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.batch.RelationshipBatchProxyManager.getRelationshipBatchAdminSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk family 
     *  administration service. 
     *
     *  @return a <code> FamilyBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.FamilyBatchAdminSession getFamilyBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.batch.RelationshipBatchManager.getFamilyBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk family 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.FamilyBatchAdminSession getFamilyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.batch.RelationshipBatchProxyManager.getFamilyBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
