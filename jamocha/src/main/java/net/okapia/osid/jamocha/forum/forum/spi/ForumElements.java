//
// ForumElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.forum.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ForumElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the ForumElement Id.
     *
     *  @return the forum element Id
     */

    public static org.osid.id.Id getForumEntityId() {
        return (makeEntityId("osid.forum.Forum"));
    }


    /**
     *  Gets the ReplyId element Id.
     *
     *  @return the ReplyId element Id
     */

    public static org.osid.id.Id getReplyId() {
        return (makeQueryElementId("osid.forum.forum.ReplyId"));
    }


    /**
     *  Gets the Reply element Id.
     *
     *  @return the Reply element Id
     */

    public static org.osid.id.Id getReply() {
        return (makeQueryElementId("osid.forum.forum.Reply"));
    }


    /**
     *  Gets the PostId element Id.
     *
     *  @return the PostId element Id
     */

    public static org.osid.id.Id getPostId() {
        return (makeQueryElementId("osid.forum.forum.PostId"));
    }


    /**
     *  Gets the Post element Id.
     *
     *  @return the Post element Id
     */

    public static org.osid.id.Id getPost() {
        return (makeQueryElementId("osid.forum.forum.Post"));
    }


    /**
     *  Gets the AncestorForumId element Id.
     *
     *  @return the AncestorForumId element Id
     */

    public static org.osid.id.Id getAncestorForumId() {
        return (makeQueryElementId("osid.forum.forum.AncestorForumId"));
    }


    /**
     *  Gets the AncestorForum element Id.
     *
     *  @return the AncestorForum element Id
     */

    public static org.osid.id.Id getAncestorForum() {
        return (makeQueryElementId("osid.forum.forum.AncestorForum"));
    }


    /**
     *  Gets the DescendantForumId element Id.
     *
     *  @return the DescendantForumId element Id
     */

    public static org.osid.id.Id getDescendantForumId() {
        return (makeQueryElementId("osid.forum.forum.DescendantForumId"));
    }


    /**
     *  Gets the DescendantForum element Id.
     *
     *  @return the DescendantForum element Id
     */

    public static org.osid.id.Id getDescendantForum() {
        return (makeQueryElementId("osid.forum.forum.DescendantForum"));
    }
}
