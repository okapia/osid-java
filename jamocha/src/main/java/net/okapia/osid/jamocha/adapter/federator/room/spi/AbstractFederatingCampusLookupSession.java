//
// AbstractFederatingCampusLookupSession.java
//
//     An abstract federating adapter for a CampusLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CampusLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCampusLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.room.CampusLookupSession>
    implements org.osid.room.CampusLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingCampusLookupSession</code>.
     */

    protected AbstractFederatingCampusLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.room.CampusLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Campus</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCampuses() {
        for (org.osid.room.CampusLookupSession session : getSessions()) {
            if (session.canLookupCampuses()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Campus</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCampusView() {
        for (org.osid.room.CampusLookupSession session : getSessions()) {
            session.useComparativeCampusView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Campus</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCampusView() {
        for (org.osid.room.CampusLookupSession session : getSessions()) {
            session.usePlenaryCampusView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Campus</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Campus</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Campus</code> and
     *  retained for compatibility.
     *
     *  @param  campusId <code>Id</code> of the
     *          <code>Campus</code>
     *  @return the campus
     *  @throws org.osid.NotFoundException <code>campusId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>campusId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.room.CampusLookupSession session : getSessions()) {
            try {
                return (session.getCampus(campusId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(campusId + " not found");
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  campuses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Campuses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  campusIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>campusIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByIds(org.osid.id.IdList campusIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.room.campus.MutableCampusList ret = new net.okapia.osid.jamocha.room.campus.MutableCampusList();

        try (org.osid.id.IdList ids = campusIds) {
            while (ids.hasNext()) {
                ret.addCampus(getCampus(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  campus genus <code>Type</code> which does not include
     *  campuses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  campusGenusType a campus genus type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByGenusType(org.osid.type.Type campusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.campus.FederatingCampusList ret = getCampusList();

        for (org.osid.room.CampusLookupSession session : getSessions()) {
            ret.addCampusList(session.getCampusesByGenusType(campusGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  campus genus <code>Type</code> and include any additional
     *  campuses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  campusGenusType a campus genus type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByParentGenusType(org.osid.type.Type campusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.campus.FederatingCampusList ret = getCampusList();

        for (org.osid.room.CampusLookupSession session : getSessions()) {
            ret.addCampusList(session.getCampusesByParentGenusType(campusGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CampusList</code> containing the given
     *  campus record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  campusRecordType a campus record type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByRecordType(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.campus.FederatingCampusList ret = getCampusList();

        for (org.osid.room.CampusLookupSession session : getSessions()) {
            ret.addCampusList(session.getCampusesByRecordType(campusRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CampusList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known campuses or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  campuses that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Campus</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.room.campus.FederatingCampusList ret = getCampusList();

        for (org.osid.room.CampusLookupSession session : getSessions()) {
            ret.addCampusList(session.getCampusesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Campuses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Campuses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampuses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.campus.FederatingCampusList ret = getCampusList();

        for (org.osid.room.CampusLookupSession session : getSessions()) {
            ret.addCampusList(session.getCampuses());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.room.campus.FederatingCampusList getCampusList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.room.campus.ParallelCampusList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.room.campus.CompositeCampusList());
        }
    }
}
