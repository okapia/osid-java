//
// AbstractQueryDirectionLookupSession.java
//
//    An inline adapter that maps a DirectionLookupSession to
//    a DirectionQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a DirectionLookupSession to
 *  a DirectionQuerySession.
 */

public abstract class AbstractQueryDirectionLookupSession
    extends net.okapia.osid.jamocha.recipe.spi.AbstractDirectionLookupSession
    implements org.osid.recipe.DirectionLookupSession {

    private final org.osid.recipe.DirectionQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryDirectionLookupSession.
     *
     *  @param querySession the underlying direction query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryDirectionLookupSession(org.osid.recipe.DirectionQuerySession querySession) {
        nullarg(querySession, "direction query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Cookbook</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Cookbook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.session.getCookbookId());
    }


    /**
     *  Gets the <code>Cookbook</code> associated with this 
     *  session.
     *
     *  @return the <code>Cookbook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCookbook());
    }


    /**
     *  Tests if this user can perform <code>Direction</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDirections() {
        return (this.session.canSearchDirections());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include directions in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        this.session.useFederatedCookbookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this cookbook only.
     */

    @OSID @Override
    public void useIsolatedCookbookView() {
        this.session.useIsolatedCookbookView();
        return;
    }
    
     
    /**
     *  Gets the <code>Direction</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Direction</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Direction</code> and
     *  retained for compatibility.
     *
     *  @param  directionId <code>Id</code> of the
     *          <code>Direction</code>
     *  @return the direction
     *  @throws org.osid.NotFoundException <code>directionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>directionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Direction getDirection(org.osid.id.Id directionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.DirectionQuery query = getQuery();
        query.matchId(directionId, true);
        org.osid.recipe.DirectionList directions = this.session.getDirectionsByQuery(query);
        if (directions.hasNext()) {
            return (directions.getNextDirection());
        } 
        
        throw new org.osid.NotFoundException(directionId + " not found");
    }


    /**
     *  Gets a <code>DirectionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  directions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Directions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  directionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Direction</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>directionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByIds(org.osid.id.IdList directionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.DirectionQuery query = getQuery();

        try (org.osid.id.IdList ids = directionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getDirectionsByQuery(query));
    }


    /**
     *  Gets a <code>DirectionList</code> corresponding to the given
     *  direction genus <code>Type</code> which does not include
     *  directions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directionGenusType a direction genus type 
     *  @return the returned <code>Direction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByGenusType(org.osid.type.Type directionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.DirectionQuery query = getQuery();
        query.matchGenusType(directionGenusType, true);
        return (this.session.getDirectionsByQuery(query));
    }


    /**
     *  Gets a <code>DirectionList</code> corresponding to the given
     *  direction genus <code>Type</code> and include any additional
     *  directions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directionGenusType a direction genus type 
     *  @return the returned <code>Direction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByParentGenusType(org.osid.type.Type directionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.DirectionQuery query = getQuery();
        query.matchParentGenusType(directionGenusType, true);
        return (this.session.getDirectionsByQuery(query));
    }


    /**
     *  Gets a <code>DirectionList</code> containing the given
     *  direction record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directionRecordType a direction record type 
     *  @return the returned <code>Direction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByRecordType(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.DirectionQuery query = getQuery();
        query.matchRecordType(directionRecordType, true);
        return (this.session.getDirectionsByQuery(query));
    }

    
    /**
     *  Gets a <code> DirectionList </code> for the given recipe
     *  <code> Id.  </code> In plenary mode, the returned list
     *  contains all known directions or an error results. Otherwise,
     *  the returned list may contain only those directions that are
     *  accessible through this session.
     *
     *  @param  recipeId a recipe <code> Id </code> 
     *  @return the returned <code> Direction </code> list 
     *  @throws org.osid.NullArgumentException <code> recipeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsForRecipe(org.osid.id.Id recipeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.DirectionQuery query = getQuery();
        query.matchRecipeId(recipeId, true);
        return (this.session.getDirectionsByQuery(query));
    }


    /**
     *  Gets all <code>Directions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Directions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirections()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.DirectionQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getDirectionsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.recipe.DirectionQuery getQuery() {
        org.osid.recipe.DirectionQuery query = this.session.getDirectionQuery();
        
        return (query);
    }
}
