//
// AbstractEdge.java
//
//     Defines an Edge builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.topology.edge.spi;


/**
 *  Defines an <code>Edge</code> builder.
 */

public abstract class AbstractEdgeBuilder<T extends AbstractEdgeBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.topology.edge.EdgeMiter edge;


    /**
     *  Constructs a new <code>AbstractEdgeBuilder</code>.
     *
     *  @param edge the edge to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractEdgeBuilder(net.okapia.osid.jamocha.builder.topology.edge.EdgeMiter edge) {
        super(edge);
        this.edge = edge;
        return;
    }


    /**
     *  Builds the edge.
     *
     *  @return the new edge
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.topology.Edge build() {
        (new net.okapia.osid.jamocha.builder.validator.topology.edge.EdgeValidator(getValidations())).validate(this.edge);
        return (new net.okapia.osid.jamocha.builder.topology.edge.ImmutableEdge(this.edge));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the edge miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.topology.edge.EdgeMiter getMiter() {
        return (this.edge);
    }


    /**
     *  Sets the directional flag.
     *
     *  @param directional <code>true</code> if this is a directional
     *         edge, <code>false</code> if non-directional
     */

    public T directional(boolean directional) {
        getMiter().setDirectional(directional);
        return (self());
    }


    /**
     *  Sets the bidirectional flag.
     *
     *  @param biDirectional <code>true</code> if this is a
     *         bidirectional edge, <code>false</code> if a
     *         unidirectional edge
     */

    public T biDirectional(boolean biDirectional) {
        getMiter().setBiDirectional(biDirectional);
        return (self());
    }

    
    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>cost</code> is <code>null</code>
     */

    public T cost(java.math.BigDecimal cost) {
        getMiter().setCost(cost);
        return (self());
    }


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>distance</code> is <code>null</code>
     */

    public T distance(java.math.BigDecimal distance) {
        getMiter().setDistance(distance);
        return (self());
    }


    /**
     *  Sets the source node.
     *
     *  @param sourceNode a source node
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNode</code> is <code>null</code>
     */

    public T sourceNode(org.osid.topology.Node sourceNode) {
        getMiter().setSourceNode(sourceNode);
        return (self());
    }


    /**
     *  Sets the destination node.
     *
     *  @param destinationNode a destination node
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNode</code> is <code>null</code>
     */

    public T destinationNode(org.osid.topology.Node destinationNode) {
        getMiter().setDestinationNode(destinationNode);
        return (self());
    }


    /**
     *  Adds an Edge record.
     *
     *  @param record an edge record
     *  @param recordType the type of edge record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.topology.records.EdgeRecord record, org.osid.type.Type recordType) {
        getMiter().addEdgeRecord(record, recordType);
        return (self());
    }
}       


