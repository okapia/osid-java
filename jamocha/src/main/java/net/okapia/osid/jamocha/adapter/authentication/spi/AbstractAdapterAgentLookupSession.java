//
// AbstractAdapterAgentLookupSession.java
//
//    An Agent lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authentication.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Agent lookup session adapter.
 */

public abstract class AbstractAdapterAgentLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authentication.AgentLookupSession {

    private final org.osid.authentication.AgentLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAgentLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAgentLookupSession(org.osid.authentication.AgentLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Agency/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Agency Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.session.getAgencyId());
    }


    /**
     *  Gets the {@code Agency} associated with this session.
     *
     *  @return the {@code Agency} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAgency());
    }


    /**
     *  Tests if this user can perform {@code Agent} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAgents() {
        return (this.session.canLookupAgents());
    }


    /**
     *  A complete view of the {@code Agent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAgentView() {
        this.session.useComparativeAgentView();
        return;
    }


    /**
     *  A complete view of the {@code Agent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAgentView() {
        this.session.usePlenaryAgentView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agents in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        this.session.useFederatedAgencyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        this.session.useIsolatedAgencyView();
        return;
    }
    
     
    /**
     *  Gets the {@code Agent} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Agent} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Agent} and
     *  retained for compatibility.
     *
     *  @param agentId {@code Id} of the {@code Agent}
     *  @return the agent
     *  @throws org.osid.NotFoundException {@code agentId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code agentId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent(org.osid.id.Id agentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgent(agentId));
    }


    /**
     *  Gets an {@code AgentList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  agents specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Agents} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  agentIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Agent} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code agentIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByIds(org.osid.id.IdList agentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgentsByIds(agentIds));
    }


    /**
     *  Gets an {@code AgentList} corresponding to the given
     *  agent genus {@code Type} which does not include
     *  agents of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agentGenusType an agent genus type 
     *  @return the returned {@code Agent} list
     *  @throws org.osid.NullArgumentException
     *          {@code agentGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByGenusType(org.osid.type.Type agentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgentsByGenusType(agentGenusType));
    }


    /**
     *  Gets an {@code AgentList} corresponding to the given
     *  agent genus {@code Type} and include any additional
     *  agents with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  agentGenusType an agent genus type 
     *  @return the returned {@code Agent} list
     *  @throws org.osid.NullArgumentException
     *          {@code agentGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByParentGenusType(org.osid.type.Type agentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgentsByParentGenusType(agentGenusType));
    }


    /**
     *  Gets an {@code AgentList} containing the given
     *  agent record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agentRecordType an agent record type 
     *  @return the returned {@code Agent} list
     *  @throws org.osid.NullArgumentException
     *          {@code agentRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByRecordType(org.osid.type.Type agentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgentsByRecordType(agentRecordType));
    }


    /**
     *  Gets all {@code Agents}. 
     *
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Agents} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgents());
    }
}
