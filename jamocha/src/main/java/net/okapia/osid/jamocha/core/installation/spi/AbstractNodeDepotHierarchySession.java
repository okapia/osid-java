//
// AbstractNodeDepotHierarchySession.java
//
//     Defines a Depot hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a depot hierarchy session for delivering a hierarchy
 *  of depots using the DepotNode interface.
 */

public abstract class AbstractNodeDepotHierarchySession
    extends net.okapia.osid.jamocha.installation.spi.AbstractDepotHierarchySession
    implements org.osid.installation.DepotHierarchySession {

    private java.util.Collection<org.osid.installation.DepotNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root depot <code> Ids </code> in this hierarchy.
     *
     *  @return the root depot <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootDepotIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.installation.depotnode.DepotNodeToIdList(this.roots));
    }


    /**
     *  Gets the root depots in the depot hierarchy. A node
     *  with no parents is an orphan. While all depot <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root depots 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getRootDepots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.installation.depotnode.DepotNodeToDepotList(new net.okapia.osid.jamocha.installation.depotnode.ArrayDepotNodeList(this.roots)));
    }


    /**
     *  Adds a root depot node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootDepot(org.osid.installation.DepotNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root depot nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootDepots(java.util.Collection<org.osid.installation.DepotNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root depot node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootDepot(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.installation.DepotNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Depot </code> has any parents. 
     *
     *  @param  depotId a depot <code> Id </code> 
     *  @return <code> true </code> if the depot has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentDepots(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getDepotNode(depotId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  depot.
     *
     *  @param  id an <code> Id </code> 
     *  @param  depotId the <code> Id </code> of a depot 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> depotId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> depotId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfDepot(org.osid.id.Id id, org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.installation.DepotNodeList parents = getDepotNode(depotId).getParentDepotNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextDepotNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given depot. 
     *
     *  @param  depotId a depot <code> Id </code> 
     *  @return the parent <code> Ids </code> of the depot 
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentDepotIds(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.installation.depot.DepotToIdList(getParentDepots(depotId)));
    }


    /**
     *  Gets the parents of the given depot. 
     *
     *  @param  depotId the <code> Id </code> to query 
     *  @return the parents of the depot 
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getParentDepots(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.installation.depotnode.DepotNodeToDepotList(getDepotNode(depotId).getParentDepotNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  depot.
     *
     *  @param  id an <code> Id </code> 
     *  @param  depotId the Id of a depot 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> depotId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfDepot(org.osid.id.Id id, org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfDepot(id, depotId)) {
            return (true);
        }

        try (org.osid.installation.DepotList parents = getParentDepots(depotId)) {
            while (parents.hasNext()) {
                if (isAncestorOfDepot(id, parents.getNextDepot().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a depot has any children. 
     *
     *  @param  depotId a depot <code> Id </code> 
     *  @return <code> true </code> if the <code> depotId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildDepots(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDepotNode(depotId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  depot.
     *
     *  @param  id an <code> Id </code> 
     *  @param depotId the <code> Id </code> of a 
     *         depot
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> depotId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> depotId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfDepot(org.osid.id.Id id, org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfDepot(depotId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  depot.
     *
     *  @param  depotId the <code> Id </code> to query 
     *  @return the children of the depot 
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildDepotIds(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.installation.depot.DepotToIdList(getChildDepots(depotId)));
    }


    /**
     *  Gets the children of the given depot. 
     *
     *  @param  depotId the <code> Id </code> to query 
     *  @return the children of the depot 
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getChildDepots(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.installation.depotnode.DepotNodeToDepotList(getDepotNode(depotId).getChildDepotNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  depot.
     *
     *  @param  id an <code> Id </code> 
     *  @param depotId the <code> Id </code> of a 
     *         depot
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> depotId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfDepot(org.osid.id.Id id, org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfDepot(depotId, id)) {
            return (true);
        }

        try (org.osid.installation.DepotList children = getChildDepots(depotId)) {
            while (children.hasNext()) {
                if (isDescendantOfDepot(id, children.getNextDepot().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  depot.
     *
     *  @param  depotId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified depot node 
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getDepotNodeIds(org.osid.id.Id depotId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.installation.depotnode.DepotNodeToNode(getDepotNode(depotId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given depot.
     *
     *  @param  depotId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified depot node 
     *  @throws org.osid.NotFoundException <code> depotId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> depotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotNode getDepotNodes(org.osid.id.Id depotId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDepotNode(depotId));
    }


    /**
     *  Closes this <code>DepotHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a depot node.
     *
     *  @param depotId the id of the depot node
     *  @throws org.osid.NotFoundException <code>depotId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>depotId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.installation.DepotNode getDepotNode(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(depotId, "depot Id");
        for (org.osid.installation.DepotNode depot : this.roots) {
            if (depot.getId().equals(depotId)) {
                return (depot);
            }

            org.osid.installation.DepotNode r = findDepot(depot, depotId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(depotId + " is not found");
    }


    protected org.osid.installation.DepotNode findDepot(org.osid.installation.DepotNode node, 
                                                        org.osid.id.Id depotId) 
	throws org.osid.OperationFailedException {

        try (org.osid.installation.DepotNodeList children = node.getChildDepotNodes()) {
            while (children.hasNext()) {
                org.osid.installation.DepotNode depot = children.getNextDepotNode();
                if (depot.getId().equals(depotId)) {
                    return (depot);
                }
                
                depot = findDepot(depot, depotId);
                if (depot != null) {
                    return (depot);
                }
            }
        }

        return (null);
    }
}
