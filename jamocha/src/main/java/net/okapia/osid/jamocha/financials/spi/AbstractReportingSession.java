//
// AbstractReportingLookupSession.java
//
//    A starter implementation framework for providing a Reporting
//    session.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Summary
 *  lookup service.
 */

public abstract class AbstractReportingSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.financials.ReportingSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Summary</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSummaries() {
        return (true);
    }


    /**
     *  A complete view of the <code>Summary</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSummaryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Summary</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySummaryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include summariess in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Gets a summary of financial transactions for a fiscal year for
     *  the given account. A summary is returned for each fiscal time
     *  period within the fiscal year.
     *
     *  @param  accountId the <code> Id </code> of an <code> Account </code> 
     *  @param  year a fiscal year 
     *  @return the account summaries 
     *  @throws org.osid.NotFoundException no <code> Account </code> found 
     *          with the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.financials.SummaryList getSummaryByYearForAccount(org.osid.id.Id accountId, 
                                                                               long year)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets a summary of financial transactions for a fiscal year for
     *  all root accounts. A summary is returned for each fiscal time
     *  period within the fiscal year.
     *
     *  @param  year a fiscal year 
     *  @return the account summaries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.financials.SummaryList getSummariesByYearForRootAccounts(long year)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets a summary of financial transactions for a fiscal period
     *  for the given account.
     *
     *  @param  accountId the <code> Id </code> of an <code> Account </code> 
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @return the account summaries 
     *  @throws org.osid.NotFoundException no <code> Account </code> or <code> 
     *          fiscalPeriodId </code> found with the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> accountId </code> or 
     *          <code> fiscalPeriodId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.financials.SummaryList getSummaryByFiscalPeriodForAccount(org.osid.id.Id accountId, 
                                                                                       org.osid.id.Id fiscalPeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets a summary of financial transactions for a fiscal period
     *  for all root accounts.
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @return the account summaries 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.financials.SummaryList getSummariesByFiscalPeriodForRootAccounts(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets a summary of financial transactions for a fiscal year for
     *  the given activity. A summary is returned for each fiscal time
     *  period within the fiscal year.
     *
     *  @param  activityId the <code> Id </code> of an <code> Activity </code> 
     *  @param  year a fiscal year 
     *  @return the activity summaries 
     *  @throws org.osid.NotFoundException no <code> Activity </code> found 
     *          with the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.financials.SummaryList getSummaryByYearForActivity(org.osid.id.Id activityId, 
                                                                                long year)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets a summary of financial transactions for a fiscal year for all 
     *  root activities. A summary is returned for each fiscal time period 
     *  within the fiscal year. 
     *
     *  @param  year a fiscal year 
     *  @return the activity summaries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    public abstract org.osid.financials.SummaryList getSummariesByYearForRootActivities(long year)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets a summary of financial transactions for a fiscal period
     *  for the given activity.
     *
     *  @param  activityId the <code> Id </code> of an <code> Activity </code> 
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @return the returned <code> Summary </code> 
     *  @throws org.osid.NotFoundException no <code> Activity </code> or 
     *          <code> fiscalPeriodId </code> found with the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> activityId </code> or 
     *          <code> fiscalPeriodId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.financials.Summary getSummaryByFiscalPeriodForActivity(org.osid.id.Id activityId, 
                                                                                    org.osid.id.Id fiscalPeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets a summary of financial transactions for a fiscal period
     *  for all root activities.
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @return the activity summaries 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public abstract org.osid.financials.SummaryList getSummariesByFiscalPeriodForRootActivities(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;
}
