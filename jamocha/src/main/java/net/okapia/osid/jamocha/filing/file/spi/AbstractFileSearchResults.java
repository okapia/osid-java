//
// AbstractFileSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.file.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractFileSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.filing.FileSearchResults {

    private org.osid.filing.FileList files;
    private final org.osid.filing.FileQueryInspector inspector;
    private final java.util.Collection<org.osid.filing.records.FileSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractFileSearchResults.
     *
     *  @param files the result set
     *  @param fileQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>files</code>
     *          or <code>fileQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractFileSearchResults(org.osid.filing.FileList files,
                                            org.osid.filing.FileQueryInspector fileQueryInspector) {
        nullarg(files, "files");
        nullarg(fileQueryInspector, "file query inspectpr");

        this.files = files;
        this.inspector = fileQueryInspector;

        return;
    }


    /**
     *  Gets the file list resulting from a search.
     *
     *  @return a file list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.filing.FileList getFiles() {
        if (this.files == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.filing.FileList files = this.files;
        this.files = null;
	return (files);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.filing.FileQueryInspector getFileQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  file search record <code> Type. </code> This method must
     *  be used to retrieve a file implementing the requested
     *  record.
     *
     *  @param fileSearchRecordType a file search 
     *         record type 
     *  @return the file search
     *  @throws org.osid.NullArgumentException
     *          <code>fileSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(fileSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.FileSearchResultsRecord getFileSearchResultsRecord(org.osid.type.Type fileSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.filing.records.FileSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(fileSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(fileSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record file search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addFileRecord(org.osid.filing.records.FileSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "file record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
