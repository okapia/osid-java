//
// AbstractStock.java
//
//     Defines a Stock.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.stock.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Stock</code>.
 */

public abstract class AbstractStock
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.inventory.Stock {

    private String sku;
    private final java.util.Collection<org.osid.inventory.Model> models = new java.util.LinkedHashSet<>();
    private org.osid.locale.DisplayText locationDescription;
    private final java.util.Collection<org.osid.mapping.Location> locations = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.inventory.records.StockRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the sku for this class of items. 
     *
     *  @return a stock keeping unit 
     */

    @OSID @Override
    public String getSKU() {
        return (this.sku);
    }


    /**
     *  Sets the sku.
     *
     *  @param sku a stock keeping unit
     *  @throws org.osid.NullArgumentException
     *          <code>sku</code> is <code>null</code>
     */

    protected void setSKU(String sku) {
        nullarg(sku, "stock keeping unit");
        this.sku = sku;
        return;
    }


    /**
     *  Gets the model <code> Ids </code> of the items in this stock. 
     *
     *  @return model <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getModelIds() {
        try {
            org.osid.inventory.ModelList models = getModels();
            return (new net.okapia.osid.jamocha.adapter.converter.inventory.model.ModelToIdList(models));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the models of the items in this stock. 
     *
     *  @return a model list 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModels()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.inventory.model.ArrayModelList(this.models));
    }


    /**
     *  Adds a model.
     *
     *  @param model a model
     *  @throws org.osid.NullArgumentException
     *          <code>model</code> is <code>null</code>
     */

    protected void addModel(org.osid.inventory.Model model) {
        nullarg(model, "model");
        this.models.add(model);
        return;
    }


    /**
     *  Sets all the models.
     *
     *  @param models a collection of models
     *  @throws org.osid.NullArgumentException
     *          <code>models</code> is <code>null</code>
     */

    protected void setModels(java.util.Collection<org.osid.inventory.Model> models) {
        nullarg(models, "models");

        this.models.clear();
        this.models.addAll(models);

        return;
    }


    /**
     *  Gets the location description in which this stock is located. 
     *
     *  @return a location 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.locationDescription);
    }


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    protected void setLocationDescription(org.osid.locale.DisplayText description) {
        nullarg(description, "location description");
        this.locationDescription = description;
        return;
    }


    /**
     *  Gets the location <code> Id </code> to which this stock is located. 
     *
     *  @return a location <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLocationIds() {
        try {
            org.osid.mapping.LocationList locations = getLocations();
            return (new net.okapia.osid.jamocha.adapter.converter.mapping.location.LocationToIdList(locations));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the location to which this stock is located. 
     *
     *  @return a location list 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocations()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.mapping.location.ArrayLocationList(this.locations));
    }


    /**
     *  Adds a location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    protected void addLocation(org.osid.mapping.Location location) {
        nullarg(location, "location");
        this.locations.add(location);
        return;
    }


    /**
     *  Sets all the locations.
     *
     *  @param locations a collection of locations
     *  @throws org.osid.NullArgumentException
     *          <code>locations</code> is <code>null</code>
     */

    protected void setLocations(java.util.Collection<org.osid.mapping.Location> locations) {
        nullarg(locations, "locations");

        this.locations.clear();
        this.locations.addAll(locations);

        return;
    }


    /**
     *  Tests if this stock supports the given record
     *  <code>Type</code>.
     *
     *  @param  stockRecordType a stock record type 
     *  @return <code>true</code> if the stockRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type stockRecordType) {
        for (org.osid.inventory.records.StockRecord record : this.records) {
            if (record.implementsRecordType(stockRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Stock</code> record <code>Type</code>.
     *
     *  @param  stockRecordType the stock record type 
     *  @return the stock record 
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stockRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.StockRecord getStockRecord(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.StockRecord record : this.records) {
            if (record.implementsRecordType(stockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stockRecordType + " is not supported");
    }


    /**
     *  Adds a record to this stock. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param stockRecord the stock record
     *  @param stockRecordType stock record type
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecord</code> or
     *          <code>stockRecordTypestock</code> is
     *          <code>null</code>
     */
            
    protected void addStockRecord(org.osid.inventory.records.StockRecord stockRecord, 
                                  org.osid.type.Type stockRecordType) {

        nullarg(stockRecord, "stock record");
        addRecordType(stockRecordType);
        this.records.add(stockRecord);
        
        return;
    }
}
