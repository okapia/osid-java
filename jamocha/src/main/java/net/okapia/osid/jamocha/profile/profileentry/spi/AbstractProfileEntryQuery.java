//
// AbstractProfileEntryQuery.java
//
//     A template for making a ProfileEntry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profileentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for profile entries.
 */

public abstract class AbstractProfileEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.profile.ProfileEntryQuery {

    private final java.util.Collection<org.osid.profile.records.ProfileEntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches implicit profile entries. 
     *
     *  @param match <code> true </code> ito match implicit profile
     *          entries, <code> false </code> to match implciit
     *          profile entries
     */

    @OSID @Override
    public void matchImplicit(boolean match) {
        return;
    }


    /**
     *  Clears the implicit profile entries query terms. 
     */

    @OSID @Override
    public void clearImplicitTerms() {
        return;
    }


    /**
     *  Adds an <code> Id </code> to match an explicit or implicitly
     *  related profile entries depending on <code>
     *  matchExplicitProfileEntries().  </code> Multiple <code> Ids
     *  </code> can be added to perform a boolean <code> OR </code>
     *  among them.
     *
     *  @param  id <code> Id </code> to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRelatedProfileEntryId(org.osid.id.Id id, boolean match) {
        return;
    }


    /**
     *  Clears the related profile entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRelatedProfileEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelatedProfileEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelatedProfileEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuery getRelatedProfileEntryQuery() {
        throw new org.osid.UnimplementedException("supportsRelatedProfileEntryQuery() is false");
    }


    /**
     *  Clears the related profile entry query terms. 
     */

    @OSID @Override
    public void clearRelatedProfileEntryTerms() {
        return;
    }


    /**
     *  Matches the resource identified by the given <code> Id. </code> 
     *
     *  @param  resourceId the Id of the <code> Resource </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the resource query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> ResourceQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Matches the agent identified by the given <code> Id. </code> 
     *
     *  @param  agentId the Id of the <code> Agent </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the agent query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> AgentQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }


    /**
     *  Matches the profile item identified by the given <code> Id. </code> 
     *
     *  @param  profileItemId the Id of the <code> ProfileItem </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileItemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileItemId(org.osid.id.Id profileItemId, boolean match) {
        return;
    }


    /**
     *  Clears the profile item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProfileItemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileItemQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile item query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemQuery() {
        return (false);
    }


    /**
     *  Gets the profile item query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> FunctinQuery </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuery getProfileItemQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsProfileItemQuery() is false");
    }


    /**
     *  Clears the profile item terms. 
     */

    @OSID @Override
    public void clearProfileItemTerms() {
        return;
    }


    /**
     *  Sets the profile <code> Id </code> for this query. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileId(org.osid.id.Id profileId, boolean match) {
        return;
    }


    /**
     *  Clears the profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a profile. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> supportsProfileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getProfileQuery() {
        throw new org.osid.UnimplementedException("supportsProfileQuery() is false");
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearProfileTerms() {
        return;
    }


    /**
     *  Gets the record corresponding to the given profile entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a profile entry implementing the requested record.
     *
     *  @param profileEntryRecordType a profile entry record type
     *  @return the profile entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntryQueryRecord getProfileEntryQueryRecord(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileEntryQueryRecord record : this.records) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile entry query. 
     *
     *  @param profileEntryQueryRecord profile entry query record
     *  @param profileEntryRecordType profileEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileEntryQueryRecord(org.osid.profile.records.ProfileEntryQueryRecord profileEntryQueryRecord, 
                                          org.osid.type.Type profileEntryRecordType) {

        addRecordType(profileEntryRecordType);
        nullarg(profileEntryQueryRecord, "profile entry query record");
        this.records.add(profileEntryQueryRecord);        
        return;
    }
}
