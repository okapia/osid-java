//
// AbstractAuthorizationEnablerSearch.java
//
//     A template for making an AuthorizationEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.rules.authorizationenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing authorization enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAuthorizationEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.authorization.rules.AuthorizationEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.authorization.rules.records.AuthorizationEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.authorization.rules.AuthorizationEnablerSearchOrder authorizationEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of authorization enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  authorizationEnablerIds list of authorization enablers
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAuthorizationEnablers(org.osid.id.IdList authorizationEnablerIds) {
        while (authorizationEnablerIds.hasNext()) {
            try {
                this.ids.add(authorizationEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAuthorizationEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of authorization enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAuthorizationEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  authorizationEnablerSearchOrder authorization enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>authorizationEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAuthorizationEnablerResults(org.osid.authorization.rules.AuthorizationEnablerSearchOrder authorizationEnablerSearchOrder) {
	this.authorizationEnablerSearchOrder = authorizationEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.authorization.rules.AuthorizationEnablerSearchOrder getAuthorizationEnablerSearchOrder() {
	return (this.authorizationEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given authorization enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an authorization enabler implementing the requested record.
     *
     *  @param authorizationEnablerSearchRecordType an authorization enabler search record
     *         type
     *  @return the authorization enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.rules.records.AuthorizationEnablerSearchRecord getAuthorizationEnablerSearchRecord(org.osid.type.Type authorizationEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.authorization.rules.records.AuthorizationEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(authorizationEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this authorization enabler search. 
     *
     *  @param authorizationEnablerSearchRecord authorization enabler search record
     *  @param authorizationEnablerSearchRecordType authorizationEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuthorizationEnablerSearchRecord(org.osid.authorization.rules.records.AuthorizationEnablerSearchRecord authorizationEnablerSearchRecord, 
                                           org.osid.type.Type authorizationEnablerSearchRecordType) {

        addRecordType(authorizationEnablerSearchRecordType);
        this.records.add(authorizationEnablerSearchRecord);        
        return;
    }
}
