//
// AbstractAuctionConstrainerEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAuctionConstrainerEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.bidding.rules.AuctionConstrainerEnablerSearchResults {

    private org.osid.bidding.rules.AuctionConstrainerEnablerList auctionConstrainerEnablers;
    private final org.osid.bidding.rules.AuctionConstrainerEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAuctionConstrainerEnablerSearchResults.
     *
     *  @param auctionConstrainerEnablers the result set
     *  @param auctionConstrainerEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnablers</code>
     *          or <code>auctionConstrainerEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAuctionConstrainerEnablerSearchResults(org.osid.bidding.rules.AuctionConstrainerEnablerList auctionConstrainerEnablers,
                                            org.osid.bidding.rules.AuctionConstrainerEnablerQueryInspector auctionConstrainerEnablerQueryInspector) {
        nullarg(auctionConstrainerEnablers, "auction constrainer enablers");
        nullarg(auctionConstrainerEnablerQueryInspector, "auction constrainer enabler query inspectpr");

        this.auctionConstrainerEnablers = auctionConstrainerEnablers;
        this.inspector = auctionConstrainerEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the auction constrainer enabler list resulting from a search.
     *
     *  @return an auction constrainer enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablers() {
        if (this.auctionConstrainerEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.bidding.rules.AuctionConstrainerEnablerList auctionConstrainerEnablers = this.auctionConstrainerEnablers;
        this.auctionConstrainerEnablers = null;
	return (auctionConstrainerEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.bidding.rules.AuctionConstrainerEnablerQueryInspector getAuctionConstrainerEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  auction constrainer enabler search record <code> Type. </code> This method must
     *  be used to retrieve an auctionConstrainerEnabler implementing the requested
     *  record.
     *
     *  @param auctionConstrainerEnablerSearchRecordType an auctionConstrainerEnabler search 
     *         record type 
     *  @return the auction constrainer enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(auctionConstrainerEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchResultsRecord getAuctionConstrainerEnablerSearchResultsRecord(org.osid.type.Type auctionConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(auctionConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(auctionConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record auction constrainer enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAuctionConstrainerEnablerRecord(org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "auction constrainer enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
