//
// AbstractAssemblyProfileEntryEnablerQuery.java
//
//     A ProfileEntryEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.profile.rules.profileentryenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProfileEntryEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyProfileEntryEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.profile.rules.ProfileEntryEnablerQuery,
               org.osid.profile.rules.ProfileEntryEnablerQueryInspector,
               org.osid.profile.rules.ProfileEntryEnablerSearchOrder {

    private final java.util.Collection<org.osid.profile.rules.records.ProfileEntryEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.profile.rules.records.ProfileEntryEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.profile.rules.records.ProfileEntryEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProfileEntryEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProfileEntryEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the profile entry. 
     *
     *  @param  profileEntryId the profile entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledProfileEntryId(org.osid.id.Id profileEntryId, 
                                         boolean match) {
        getAssembler().addIdTerm(getRuledProfileEntryIdColumn(), profileEntryId, match);
        return;
    }


    /**
     *  Clears the profile entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledProfileEntryIdTerms() {
        getAssembler().clearTerms(getRuledProfileEntryIdColumn());
        return;
    }


    /**
     *  Gets the ruled profile entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledProfileEntryIdTerms() {
        return (getAssembler().getIdTerms(getRuledProfileEntryIdColumn()));
    }


    /**
     *  Gets the RuledProfileEntryId column name.
     *
     * @return the column name
     */

    protected String getRuledProfileEntryIdColumn() {
        return ("ruled_profile_entry_id");
    }


    /**
     *  Tests if a <code> ProfileEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledProfileEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledProfileEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuery getRuledProfileEntryQuery() {
        throw new org.osid.UnimplementedException("supportsRuledProfileEntryQuery() is false");
    }


    /**
     *  Matches enablers mapped to any profile entry. 
     *
     *  @param  match <code> true </code> for enablers mapped to any profile 
     *          entry, <code> false </code> to match enablers mapped to no 
     *          profile entry 
     */

    @OSID @Override
    public void matchAnyRuledProfileEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledProfileEntryColumn(), match);
        return;
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearRuledProfileEntryTerms() {
        getAssembler().clearTerms(getRuledProfileEntryColumn());
        return;
    }


    /**
     *  Gets the ruled profile entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQueryInspector[] getRuledProfileEntryTerms() {
        return (new org.osid.profile.ProfileEntryQueryInspector[0]);
    }


    /**
     *  Gets the RuledProfileEntry column name.
     *
     * @return the column name
     */

    protected String getRuledProfileEntryColumn() {
        return ("ruled_profile_entry");
    }


    /**
     *  Matches enablers mapped to the profile. 
     *
     *  @param  profileId the profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileId(org.osid.id.Id profileId, boolean match) {
        getAssembler().addIdTerm(getProfileIdColumn(), profileId, match);
        return;
    }


    /**
     *  Clears the profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileIdTerms() {
        getAssembler().clearTerms(getProfileIdColumn());
        return;
    }


    /**
     *  Gets the profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileIdTerms() {
        return (getAssembler().getIdTerms(getProfileIdColumn()));
    }


    /**
     *  Gets the ProfileId column name.
     *
     * @return the column name
     */

    protected String getProfileIdColumn() {
        return ("profile_id");
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> supportsProfileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getProfileQuery() {
        throw new org.osid.UnimplementedException("supportsProfileQuery() is false");
    }


    /**
     *  Clears the profile query terms. 
     */

    @OSID @Override
    public void clearProfileTerms() {
        getAssembler().clearTerms(getProfileColumn());
        return;
    }


    /**
     *  Gets the profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }


    /**
     *  Gets the Profile column name.
     *
     * @return the column name
     */

    protected String getProfileColumn() {
        return ("profile");
    }


    /**
     *  Tests if this profileEntryEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  profileEntryEnablerRecordType a profile entry enabler record type 
     *  @return <code>true</code> if the profileEntryEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type profileEntryEnablerRecordType) {
        for (org.osid.profile.rules.records.ProfileEntryEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(profileEntryEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  profileEntryEnablerRecordType the profile entry enabler record type 
     *  @return the profile entry enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.rules.records.ProfileEntryEnablerQueryRecord getProfileEntryEnablerQueryRecord(org.osid.type.Type profileEntryEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.rules.records.ProfileEntryEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(profileEntryEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  profileEntryEnablerRecordType the profile entry enabler record type 
     *  @return the profile entry enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.rules.records.ProfileEntryEnablerQueryInspectorRecord getProfileEntryEnablerQueryInspectorRecord(org.osid.type.Type profileEntryEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.rules.records.ProfileEntryEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(profileEntryEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param profileEntryEnablerRecordType the profile entry enabler record type
     *  @return the profile entry enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.rules.records.ProfileEntryEnablerSearchOrderRecord getProfileEntryEnablerSearchOrderRecord(org.osid.type.Type profileEntryEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.rules.records.ProfileEntryEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(profileEntryEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this profile entry enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param profileEntryEnablerQueryRecord the profile entry enabler query record
     *  @param profileEntryEnablerQueryInspectorRecord the profile entry enabler query inspector
     *         record
     *  @param profileEntryEnablerSearchOrderRecord the profile entry enabler search order record
     *  @param profileEntryEnablerRecordType profile entry enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerQueryRecord</code>,
     *          <code>profileEntryEnablerQueryInspectorRecord</code>,
     *          <code>profileEntryEnablerSearchOrderRecord</code> or
     *          <code>profileEntryEnablerRecordTypeprofileEntryEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addProfileEntryEnablerRecords(org.osid.profile.rules.records.ProfileEntryEnablerQueryRecord profileEntryEnablerQueryRecord, 
                                      org.osid.profile.rules.records.ProfileEntryEnablerQueryInspectorRecord profileEntryEnablerQueryInspectorRecord, 
                                      org.osid.profile.rules.records.ProfileEntryEnablerSearchOrderRecord profileEntryEnablerSearchOrderRecord, 
                                      org.osid.type.Type profileEntryEnablerRecordType) {

        addRecordType(profileEntryEnablerRecordType);

        nullarg(profileEntryEnablerQueryRecord, "profile entry enabler query record");
        nullarg(profileEntryEnablerQueryInspectorRecord, "profile entry enabler query inspector record");
        nullarg(profileEntryEnablerSearchOrderRecord, "profile entry enabler search odrer record");

        this.queryRecords.add(profileEntryEnablerQueryRecord);
        this.queryInspectorRecords.add(profileEntryEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(profileEntryEnablerSearchOrderRecord);
        
        return;
    }
}
