//
// AbstractAssemblyTriggerQuery.java
//
//     A TriggerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.trigger.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A TriggerQuery that stores terms.
 */

public abstract class AbstractAssemblyTriggerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.control.TriggerQuery,
               org.osid.control.TriggerQueryInspector,
               org.osid.control.TriggerSearchOrder {

    private final java.util.Collection<org.osid.control.records.TriggerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.TriggerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.TriggerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyTriggerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyTriggerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the controller <code> Id </code> for this query. 
     *
     *  @param  controllerId a controller <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchControllerId(org.osid.id.Id controllerId, boolean match) {
        getAssembler().addIdTerm(getControllerIdColumn(), controllerId, match);
        return;
    }


    /**
     *  Clears the controller <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearControllerIdTerms() {
        getAssembler().clearTerms(getControllerIdColumn());
        return;
    }


    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getControllerIdTerms() {
        return (getAssembler().getIdTerms(getControllerIdColumn()));
    }


    /**
     *  Orders the results by controller. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByController(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getControllerColumn(), style);
        return;
    }


    /**
     *  Gets the ControllerId column name.
     *
     * @return the column name
     */

    protected String getControllerIdColumn() {
        return ("controller_id");
    }


    /**
     *  Tests if a <code> ControllerQuery </code> is available. 
     *
     *  @return <code> true </code> if a controller query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a controller. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the controller query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuery getControllerQuery() {
        throw new org.osid.UnimplementedException("supportsControllerQuery() is false");
    }


    /**
     *  Clears the controller query terms. 
     */

    @OSID @Override
    public void clearControllerTerms() {
        getAssembler().clearTerms(getControllerColumn());
        return;
    }


    /**
     *  Gets the controller query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getControllerTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Tests if a controller search order is available. 
     *
     *  @return <code> true </code> if a controller search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the controller search order. 
     *
     *  @return the controller search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsControllerSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchOrder getControllerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsControllerSearchOrder() is false");
    }


    /**
     *  Gets the Controller column name.
     *
     * @return the column name
     */

    protected String getControllerColumn() {
        return ("controller");
    }


    /**
     *  Matches triggers listening for ON events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchTurnedOn(boolean match) {
        getAssembler().addBooleanTerm(getTurnedOnColumn(), match);
        return;
    }


    /**
     *  Clears the ON event query terms. 
     */

    @OSID @Override
    public void clearTurnedOnTerms() {
        getAssembler().clearTerms(getTurnedOnColumn());
        return;
    }


    /**
     *  Gets the ON event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getTurnedOnTerms() {
        return (getAssembler().getBooleanTerms(getTurnedOnColumn()));
    }


    /**
     *  Orders the results by ON event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTurnedOn(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTurnedOnColumn(), style);
        return;
    }


    /**
     *  Gets the TurnedOn column name.
     *
     * @return the column name
     */

    protected String getTurnedOnColumn() {
        return ("turned_on");
    }


    /**
     *  Matches triggers listening for OFF events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchTurnedOff(boolean match) {
        getAssembler().addBooleanTerm(getTurnedOffColumn(), match);
        return;
    }


    /**
     *  Clears the OFF event query terms. 
     */

    @OSID @Override
    public void clearTurnedOffTerms() {
        getAssembler().clearTerms(getTurnedOffColumn());
        return;
    }


    /**
     *  Gets the OFF event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getTurnedOffTerms() {
        return (getAssembler().getBooleanTerms(getTurnedOffColumn()));
    }


    /**
     *  Orders the results by OFF event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTurnedOff(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTurnedOffColumn(), style);
        return;
    }


    /**
     *  Gets the TurnedOff column name.
     *
     * @return the column name
     */

    protected String getTurnedOffColumn() {
        return ("turned_off");
    }


    /**
     *  Matches triggers listening for changed amount events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchChangedVariableAmount(boolean match) {
        getAssembler().addBooleanTerm(getChangedVariableAmountColumn(), match);
        return;
    }


    /**
     *  Clears the changed amount event query terms. 
     */

    @OSID @Override
    public void clearChangedVariableAmountTerms() {
        getAssembler().clearTerms(getChangedVariableAmountColumn());
        return;
    }


    /**
     *  Gets the changed amount event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getChangedVariableAmountTerms() {
        return (getAssembler().getBooleanTerms(getChangedVariableAmountColumn()));
    }


    /**
     *  Orders the results by changed variable amount event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByChangedVariableAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getChangedVariableAmountColumn(), style);
        return;
    }


    /**
     *  Gets the ChangedVariableAmount column name.
     *
     * @return the column name
     */

    protected String getChangedVariableAmountColumn() {
        return ("changed_variable_amount");
    }


    /**
     *  Matches triggers listening for exceeds amount events between the given 
     *  values inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchExceedsVariableAmount(java.math.BigDecimal start, 
                                           java.math.BigDecimal end, 
                                           boolean match) {
        getAssembler().addDecimalRangeTerm(getExceedsVariableAmountColumn(), start, end, match);
        return;
    }


    /**
     *  Matches triggers with any exceeds variable amount. 
     *
     *  @param  match <code> true </code> to match triggers with any exceeds 
     *          amount, <code> false </code> to match triggers with no exceeds 
     *          amount 
     */

    @OSID @Override
    public void matchAnyExceedsVariableAmount(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getExceedsVariableAmountColumn(), match);
        return;
    }


    /**
     *  Clears the exceeds amount event query terms. 
     */

    @OSID @Override
    public void clearExceedsVariableAmountTerms() {
        getAssembler().clearTerms(getExceedsVariableAmountColumn());
        return;
    }


    /**
     *  Gets the exceeds amount event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getExceedsVariableAmountTerms() {
        return (getAssembler().getDecimalRangeTerms(getExceedsVariableAmountColumn()));
    }


    /**
     *  Orders the results by exceeds variable amount event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByExceedsVariableAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getExceedsVariableAmountColumn(), style);
        return;
    }


    /**
     *  Gets the ExceedsVariableAmount column name.
     *
     * @return the column name
     */

    protected String getExceedsVariableAmountColumn() {
        return ("exceeds_variable_amount");
    }


    /**
     *  Matches triggers listening for deceeds amount events between the given 
     *  values inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchDeceedsVariableAmount(java.math.BigDecimal start, 
                                           java.math.BigDecimal end, 
                                           boolean match) {
        getAssembler().addDecimalRangeTerm(getDeceedsVariableAmountColumn(), start, end, match);
        return;
    }


    /**
     *  Matches triggers with any deceeds variable amount. 
     *
     *  @param  match <code> true </code> to match triggers with any deceeds 
     *          amount, <code> false </code> to match triggers with no deceeds 
     *          amount 
     */

    @OSID @Override
    public void matchAnyDeceedsVariableAmount(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getDeceedsVariableAmountColumn(), match);
        return;
    }


    /**
     *  Clears the deceeds amount event query terms. 
     */

    @OSID @Override
    public void clearDeceedsVariableAmountTerms() {
        getAssembler().clearTerms(getDeceedsVariableAmountColumn());
        return;
    }


    /**
     *  Gets the deceeds amount event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDeceedsVariableAmountTerms() {
        return (getAssembler().getDecimalRangeTerms(getDeceedsVariableAmountColumn()));
    }


    /**
     *  Orders the results by deceeds variable amount event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDeceedsVariableAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDeceedsVariableAmountColumn(), style);
        return;
    }


    /**
     *  Gets the DeceedsVariableAmount column name.
     *
     * @return the column name
     */

    protected String getDeceedsVariableAmountColumn() {
        return ("deceeds_variable_amount");
    }


    /**
     *  Matches triggers listening for changed state events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchChangedDiscreetState(boolean match) {
        getAssembler().addBooleanTerm(getChangedDiscreetStateColumn(), match);
        return;
    }


    /**
     *  Clears the changed state event query terms. 
     */

    @OSID @Override
    public void clearChangedDiscreetStateTerms() {
        getAssembler().clearTerms(getChangedDiscreetStateColumn());
        return;
    }


    /**
     *  Gets the changed state event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getChangedDiscreetStateTerms() {
        return (getAssembler().getBooleanTerms(getChangedDiscreetStateColumn()));
    }


    /**
     *  Orders the results by state change event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByChangedDiscreetState(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getChangedDiscreetStateColumn(), style);
        return;
    }


    /**
     *  Gets the ChangedDiscreetState column name.
     *
     * @return the column name
     */

    protected String getChangedDiscreetStateColumn() {
        return ("changed_discreet_state");
    }


    /**
     *  Sets the state <code> Id </code> for this query. 
     *
     *  @param  stateId a state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDiscreetStateId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getDiscreetStateIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears the discreet state Id query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateIdTerms() {
        getAssembler().clearTerms(getDiscreetStateIdColumn());
        return;
    }


    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDiscreetStateIdTerms() {
        return (getAssembler().getIdTerms(getDiscreetStateIdColumn()));
    }


    /**
     *  Orders the results by state listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDiscreetState(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDiscreetStateColumn(), style);
        return;
    }


    /**
     *  Gets the DiscreetStateId column name.
     *
     * @return the column name
     */

    protected String getDiscreetStateIdColumn() {
        return ("discreet_state_id");
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a discreet state query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a discreet state. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the discreet state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDiscreetStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getDiscreetStateQuery() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateQuery() is false");
    }


    /**
     *  Matches triggers with any discreet state. 
     *
     *  @param  match <code> true </code> to match triggers with any discreet 
     *          state, <code> false </code> to match triggers with no discreet 
     *          states 
     */

    @OSID @Override
    public void matchAnyDiscreetState(boolean match) {
        getAssembler().addIdWildcardTerm(getDiscreetStateColumn(), match);
        return;
    }


    /**
     *  Clears the discreet state query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateTerms() {
        getAssembler().clearTerms(getDiscreetStateColumn());
        return;
    }


    /**
     *  Gets the discreet state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getDiscreetStateTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Tests if a state search order is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the discreet state search order. 
     *
     *  @return the state search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsDiscreetStateSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getDiscreetStateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateSearchOrder() is false");
    }


    /**
     *  Gets the DiscreetState column name.
     *
     * @return the column name
     */

    protected String getDiscreetStateColumn() {
        return ("discreet_state");
    }


    /**
     *  Sets the action group <code> Id </code> for this query. 
     *
     *  @param  actionGroupId an action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActionGroupId(org.osid.id.Id actionGroupId, boolean match) {
        getAssembler().addIdTerm(getActionGroupIdColumn(), actionGroupId, match);
        return;
    }


    /**
     *  Clears the action group <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActionGroupIdTerms() {
        getAssembler().clearTerms(getActionGroupIdColumn());
        return;
    }


    /**
     *  Gets the action group <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActionGroupIdTerms() {
        return (getAssembler().getIdTerms(getActionGroupIdColumn()));
    }


    /**
     *  Gets the ActionGroupId column name.
     *
     * @return the column name
     */

    protected String getActionGroupIdColumn() {
        return ("action_group_id");
    }


    /**
     *  Tests if an <code> ActionGroupQuery </code> is available. 
     *
     *  @return <code> true </code> if a action group query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action group. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the action group query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuery getActionGroupQuery() {
        throw new org.osid.UnimplementedException("supportsActionGroupQuery() is false");
    }


    /**
     *  Matches triggers with any action group. 
     *
     *  @param  match <code> true </code> to match triggers with any action 
     *          group,, <code> false </code> to match triggers with no action 
     *          groups 
     */

    @OSID @Override
    public void matchAnyActionGroup(boolean match) {
        getAssembler().addIdWildcardTerm(getActionGroupColumn(), match);
        return;
    }


    /**
     *  Clears the action group query terms. 
     */

    @OSID @Override
    public void clearActionGroupTerms() {
        getAssembler().clearTerms(getActionGroupColumn());
        return;
    }


    /**
     *  Gets the action group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQueryInspector[] getActionGroupTerms() {
        return (new org.osid.control.ActionGroupQueryInspector[0]);
    }


    /**
     *  Gets the ActionGroup column name.
     *
     * @return the column name
     */

    protected String getActionGroupColumn() {
        return ("action_group");
    }


    /**
     *  Sets the scene <code> Id </code> for this query. 
     *
     *  @param  sceneId a scene <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sceneId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSceneId(org.osid.id.Id sceneId, boolean match) {
        getAssembler().addIdTerm(getSceneIdColumn(), sceneId, match);
        return;
    }


    /**
     *  Clears the scene <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSceneIdTerms() {
        getAssembler().clearTerms(getSceneIdColumn());
        return;
    }


    /**
     *  Gets the scene <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSceneIdTerms() {
        return (getAssembler().getIdTerms(getSceneIdColumn()));
    }


    /**
     *  Gets the SceneId column name.
     *
     * @return the column name
     */

    protected String getSceneIdColumn() {
        return ("scene_id");
    }


    /**
     *  Tests if a <code> SceneQuery </code> is available. 
     *
     *  @return <code> true </code> if a scene query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneQuery() {
        return (false);
    }


    /**
     *  Gets the query for a scene. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the scene query 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuery getSceneQuery() {
        throw new org.osid.UnimplementedException("supportsSceneQuery() is false");
    }


    /**
     *  Matches triggers with any scene. 
     *
     *  @param  match <code> true </code> to match triggers with any scene, 
     *          <code> false </code> to match triggers with no scenes 
     */

    @OSID @Override
    public void matchAnyScene(boolean match) {
        getAssembler().addIdWildcardTerm(getSceneColumn(), match);
        return;
    }


    /**
     *  Clears the scene query terms. 
     */

    @OSID @Override
    public void clearSceneTerms() {
        getAssembler().clearTerms(getSceneColumn());
        return;
    }


    /**
     *  Gets the scene query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SceneQueryInspector[] getSceneTerms() {
        return (new org.osid.control.SceneQueryInspector[0]);
    }


    /**
     *  Gets the Scene column name.
     *
     * @return the column name
     */

    protected String getSceneColumn() {
        return ("scene");
    }


    /**
     *  Sets the setting <code> Id </code> for this query. 
     *
     *  @param  settingId a setting <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> settingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSettingId(org.osid.id.Id settingId, boolean match) {
        getAssembler().addIdTerm(getSettingIdColumn(), settingId, match);
        return;
    }


    /**
     *  Clears the setting <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSettingIdTerms() {
        getAssembler().clearTerms(getSettingIdColumn());
        return;
    }


    /**
     *  Gets the setting <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSettingIdTerms() {
        return (getAssembler().getIdTerms(getSettingIdColumn()));
    }


    /**
     *  Gets the SettingId column name.
     *
     * @return the column name
     */

    protected String getSettingIdColumn() {
        return ("setting_id");
    }


    /**
     *  Tests if a <code> SettingQuery </code> is available. 
     *
     *  @return <code> true </code> if a setting query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a setting. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the setting query 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuery getSettingQuery() {
        throw new org.osid.UnimplementedException("supportsSettingQuery() is false");
    }


    /**
     *  Matches triggers with any setting. 
     *
     *  @param  match <code> true </code> to match triggers with any setting, 
     *          <code> false </code> to match triggers with no settings 
     */

    @OSID @Override
    public void matchAnySetting(boolean match) {
        getAssembler().addIdWildcardTerm(getSettingColumn(), match);
        return;
    }


    /**
     *  Clears the setting query terms. 
     */

    @OSID @Override
    public void clearSettingTerms() {
        getAssembler().clearTerms(getSettingColumn());
        return;
    }


    /**
     *  Gets the setting query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SettingQueryInspector[] getSettingTerms() {
        return (new org.osid.control.SettingQueryInspector[0]);
    }


    /**
     *  Gets the Setting column name.
     *
     * @return the column name
     */

    protected String getSettingColumn() {
        return ("setting");
    }


    /**
     *  Sets the system <code> Id </code> for this query to match triggers 
     *  assigned to systems. 
     *
     *  @param  triggerId the trigger <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> triggerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id triggerId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), triggerId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this trigger supports the given record
     *  <code>Type</code>.
     *
     *  @param  triggerRecordType a trigger record type 
     *  @return <code>true</code> if the triggerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type triggerRecordType) {
        for (org.osid.control.records.TriggerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  triggerRecordType the trigger record type 
     *  @return the trigger query record 
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.TriggerQueryRecord getTriggerQueryRecord(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.TriggerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  triggerRecordType the trigger record type 
     *  @return the trigger query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.TriggerQueryInspectorRecord getTriggerQueryInspectorRecord(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.TriggerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param triggerRecordType the trigger record type
     *  @return the trigger search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.TriggerSearchOrderRecord getTriggerSearchOrderRecord(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.TriggerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this trigger. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param triggerQueryRecord the trigger query record
     *  @param triggerQueryInspectorRecord the trigger query inspector
     *         record
     *  @param triggerSearchOrderRecord the trigger search order record
     *  @param triggerRecordType trigger record type
     *  @throws org.osid.NullArgumentException
     *          <code>triggerQueryRecord</code>,
     *          <code>triggerQueryInspectorRecord</code>,
     *          <code>triggerSearchOrderRecord</code> or
     *          <code>triggerRecordTypetrigger</code> is
     *          <code>null</code>
     */
            
    protected void addTriggerRecords(org.osid.control.records.TriggerQueryRecord triggerQueryRecord, 
                                      org.osid.control.records.TriggerQueryInspectorRecord triggerQueryInspectorRecord, 
                                      org.osid.control.records.TriggerSearchOrderRecord triggerSearchOrderRecord, 
                                      org.osid.type.Type triggerRecordType) {

        addRecordType(triggerRecordType);

        nullarg(triggerQueryRecord, "trigger query record");
        nullarg(triggerQueryInspectorRecord, "trigger query inspector record");
        nullarg(triggerSearchOrderRecord, "trigger search odrer record");

        this.queryRecords.add(triggerQueryRecord);
        this.queryInspectorRecords.add(triggerQueryInspectorRecord);
        this.searchOrderRecords.add(triggerSearchOrderRecord);
        
        return;
    }
}
