//
// MutableIndexedMapCourseEntryLookupSession
//
//    Implements a CourseEntry lookup service backed by a collection of
//    courseEntries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements a CourseEntry lookup service backed by a collection of
 *  course entries. The course entries are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some course entries may be compatible
 *  with more types than are indicated through these course entry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of course entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCourseEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractIndexedMapCourseEntryLookupSession
    implements org.osid.course.chronicle.CourseEntryLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCourseEntryLookupSession} with no course entries.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

      public MutableIndexedMapCourseEntryLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCourseEntryLookupSession} with a
     *  single course entry.
     *  
     *  @param courseCatalog the course catalog
     *  @param  courseEntry a single courseEntry
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courseEntry} is {@code null}
     */

    public MutableIndexedMapCourseEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.chronicle.CourseEntry courseEntry) {
        this(courseCatalog);
        putCourseEntry(courseEntry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCourseEntryLookupSession} using an
     *  array of course entries.
     *
     *  @param courseCatalog the course catalog
     *  @param  courseEntries an array of course entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courseEntries} is {@code null}
     */

    public MutableIndexedMapCourseEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.chronicle.CourseEntry[] courseEntries) {
        this(courseCatalog);
        putCourseEntries(courseEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCourseEntryLookupSession} using a
     *  collection of course entries.
     *
     *  @param courseCatalog the course catalog
     *  @param  courseEntries a collection of course entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courseEntries} is {@code null}
     */

    public MutableIndexedMapCourseEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.chronicle.CourseEntry> courseEntries) {

        this(courseCatalog);
        putCourseEntries(courseEntries);
        return;
    }
    

    /**
     *  Makes a {@code CourseEntry} available in this session.
     *
     *  @param  courseEntry a course entry
     *  @throws org.osid.NullArgumentException {@code courseEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putCourseEntry(org.osid.course.chronicle.CourseEntry courseEntry) {
        super.putCourseEntry(courseEntry);
        return;
    }


    /**
     *  Makes an array of course entries available in this session.
     *
     *  @param  courseEntries an array of course entries
     *  @throws org.osid.NullArgumentException {@code courseEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putCourseEntries(org.osid.course.chronicle.CourseEntry[] courseEntries) {
        super.putCourseEntries(courseEntries);
        return;
    }


    /**
     *  Makes collection of course entries available in this session.
     *
     *  @param  courseEntries a collection of course entries
     *  @throws org.osid.NullArgumentException {@code courseEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putCourseEntries(java.util.Collection<? extends org.osid.course.chronicle.CourseEntry> courseEntries) {
        super.putCourseEntries(courseEntries);
        return;
    }


    /**
     *  Removes a CourseEntry from this session.
     *
     *  @param courseEntryId the {@code Id} of the course entry
     *  @throws org.osid.NullArgumentException {@code courseEntryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCourseEntry(org.osid.id.Id courseEntryId) {
        super.removeCourseEntry(courseEntryId);
        return;
    }    
}
