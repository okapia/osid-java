//
// RaceElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.race.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RaceElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the RaceElement Id.
     *
     *  @return the race element Id
     */

    public static org.osid.id.Id getRaceEntityId() {
        return (makeEntityId("osid.voting.Race"));
    }


    /**
     *  Gets the BallotId element Id.
     *
     *  @return the BallotId element Id
     */

    public static org.osid.id.Id getBallotId() {
        return (makeElementId("osid.voting.race.BallotId"));
    }


    /**
     *  Gets the Ballot element Id.
     *
     *  @return the Ballot element Id
     */

    public static org.osid.id.Id getBallot() {
        return (makeElementId("osid.voting.race.Ballot"));
    }


    /**
     *  Gets the CandidateIds element Id.
     *
     *  @return the CandidateIds element Id
     */

    public static org.osid.id.Id getCandidateIds() {
        return (makeElementId("osid.voting.race.CandidateIds"));
    }


    /**
     *  Gets the Candidates element Id.
     *
     *  @return the Candidates element Id
     */

    public static org.osid.id.Id getCandidates() {
        return (makeElementId("osid.voting.race.Candidates"));
    }


    /**
     *  Gets the PollsId element Id.
     *
     *  @return the PollsId element Id
     */

    public static org.osid.id.Id getPollsId() {
        return (makeQueryElementId("osid.voting.race.PollsId"));
    }


    /**
     *  Gets the Polls element Id.
     *
     *  @return the Polls element Id
     */

    public static org.osid.id.Id getPolls() {
        return (makeQueryElementId("osid.voting.race.Polls"));
    }
}
