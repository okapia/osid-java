//
// AbstractMapCompetencyLookupSession
//
//    A simple framework for providing a Competency lookup service
//    backed by a fixed collection of competencies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Competency lookup service backed by a
 *  fixed collection of competencies. The competencies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Competencies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCompetencyLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractCompetencyLookupSession
    implements org.osid.resourcing.CompetencyLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.Competency> competencies = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.Competency>());


    /**
     *  Makes a <code>Competency</code> available in this session.
     *
     *  @param  competency a competency
     *  @throws org.osid.NullArgumentException <code>competency<code>
     *          is <code>null</code>
     */

    protected void putCompetency(org.osid.resourcing.Competency competency) {
        this.competencies.put(competency.getId(), competency);
        return;
    }


    /**
     *  Makes an array of competencies available in this session.
     *
     *  @param  competencies an array of competencies
     *  @throws org.osid.NullArgumentException <code>competencies<code>
     *          is <code>null</code>
     */

    protected void putCompetencies(org.osid.resourcing.Competency[] competencies) {
        putCompetencies(java.util.Arrays.asList(competencies));
        return;
    }


    /**
     *  Makes a collection of competencies available in this session.
     *
     *  @param  competencies a collection of competencies
     *  @throws org.osid.NullArgumentException <code>competencies<code>
     *          is <code>null</code>
     */

    protected void putCompetencies(java.util.Collection<? extends org.osid.resourcing.Competency> competencies) {
        for (org.osid.resourcing.Competency competency : competencies) {
            this.competencies.put(competency.getId(), competency);
        }

        return;
    }


    /**
     *  Removes a Competency from this session.
     *
     *  @param  competencyId the <code>Id</code> of the competency
     *  @throws org.osid.NullArgumentException <code>competencyId<code> is
     *          <code>null</code>
     */

    protected void removeCompetency(org.osid.id.Id competencyId) {
        this.competencies.remove(competencyId);
        return;
    }


    /**
     *  Gets the <code>Competency</code> specified by its <code>Id</code>.
     *
     *  @param  competencyId <code>Id</code> of the <code>Competency</code>
     *  @return the competency
     *  @throws org.osid.NotFoundException <code>competencyId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>competencyId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Competency getCompetency(org.osid.id.Id competencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.Competency competency = this.competencies.get(competencyId);
        if (competency == null) {
            throw new org.osid.NotFoundException("competency not found: " + competencyId);
        }

        return (competency);
    }


    /**
     *  Gets all <code>Competencies</code>. In plenary mode, the returned
     *  list contains all known competencies or an error
     *  results. Otherwise, the returned list may contain only those
     *  competencies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Competencies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.competency.ArrayCompetencyList(this.competencies.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.competencies.clear();
        super.close();
        return;
    }
}
