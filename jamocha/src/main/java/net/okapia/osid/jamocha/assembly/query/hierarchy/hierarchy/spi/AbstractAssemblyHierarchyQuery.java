//
// AbstractAssemblyHierarchyQuery.java
//
//     A HierarchyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.hierarchy.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A HierarchyQuery that stores terms.
 */

public abstract class AbstractAssemblyHierarchyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.hierarchy.HierarchyQuery,
               org.osid.hierarchy.HierarchyQueryInspector,
               org.osid.hierarchy.HierarchySearchOrder {

    private final java.util.Collection<org.osid.hierarchy.records.HierarchyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hierarchy.records.HierarchyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hierarchy.records.HierarchySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyHierarchyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyHierarchyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches an <code> Id </code> of a node in this hierarchy. Multiple 
     *  nodes can be added to this query which behave as a boolean <code> AND. 
     *  </code> 
     *
     *  @param  id <code> Id </code> to match 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchNodeId(org.osid.id.Id id, boolean match) {
        getAssembler().addIdTerm(getNodeIdColumn(), id, match);
        return;
    }


    /**
     *  Matches hierarchies with any node. 
     *
     *  @param  match <code> true </code> to match hierarchies with any nodes, 
     *          <code> false </code> to match hierarchies with no nodes 
     */

    @OSID @Override
    public void matchAnyNodeId(boolean match) {
        getAssembler().addIdWildcardTerm(getNodeIdColumn(), match);
        return;
    }


    /**
     *  Clears the node <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearNodeIdTerms() {
        getAssembler().clearTerms(getNodeIdColumn());
        return;
    }


    /**
     *  Gets the node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNodeIdTerms() {
        return (getAssembler().getIdTerms(getNodeIdColumn()));
    }


    /**
     *  Gets the NodeId column name.
     *
     * @return the column name
     */

    protected String getNodeIdColumn() {
        return ("node_id");
    }


    /**
     *  Tests if this hierarchy supports the given record
     *  <code>Type</code>.
     *
     *  @param  hierarchyRecordType a hierarchy record type 
     *  @return <code>true</code> if the hierarchyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type hierarchyRecordType) {
        for (org.osid.hierarchy.records.HierarchyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(hierarchyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  hierarchyRecordType the hierarchy record type 
     *  @return the hierarchy query record 
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(hierarchyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hierarchy.records.HierarchyQueryRecord getHierarchyQueryRecord(org.osid.type.Type hierarchyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hierarchy.records.HierarchyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(hierarchyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(hierarchyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  hierarchyRecordType the hierarchy record type 
     *  @return the hierarchy query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(hierarchyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hierarchy.records.HierarchyQueryInspectorRecord getHierarchyQueryInspectorRecord(org.osid.type.Type hierarchyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hierarchy.records.HierarchyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(hierarchyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(hierarchyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param hierarchyRecordType the hierarchy record type
     *  @return the hierarchy search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(hierarchyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hierarchy.records.HierarchySearchOrderRecord getHierarchySearchOrderRecord(org.osid.type.Type hierarchyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hierarchy.records.HierarchySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(hierarchyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(hierarchyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this hierarchy. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param hierarchyQueryRecord the hierarchy query record
     *  @param hierarchyQueryInspectorRecord the hierarchy query inspector
     *         record
     *  @param hierarchySearchOrderRecord the hierarchy search order record
     *  @param hierarchyRecordType hierarchy record type
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyQueryRecord</code>,
     *          <code>hierarchyQueryInspectorRecord</code>,
     *          <code>hierarchySearchOrderRecord</code> or
     *          <code>hierarchyRecordTypehierarchy</code> is
     *          <code>null</code>
     */
            
    protected void addHierarchyRecords(org.osid.hierarchy.records.HierarchyQueryRecord hierarchyQueryRecord, 
                                      org.osid.hierarchy.records.HierarchyQueryInspectorRecord hierarchyQueryInspectorRecord, 
                                      org.osid.hierarchy.records.HierarchySearchOrderRecord hierarchySearchOrderRecord, 
                                      org.osid.type.Type hierarchyRecordType) {

        addRecordType(hierarchyRecordType);

        nullarg(hierarchyQueryRecord, "hierarchy query record");
        nullarg(hierarchyQueryInspectorRecord, "hierarchy query inspector record");
        nullarg(hierarchySearchOrderRecord, "hierarchy search odrer record");

        this.queryRecords.add(hierarchyQueryRecord);
        this.queryInspectorRecords.add(hierarchyQueryInspectorRecord);
        this.searchOrderRecords.add(hierarchySearchOrderRecord);
        
        return;
    }
}
