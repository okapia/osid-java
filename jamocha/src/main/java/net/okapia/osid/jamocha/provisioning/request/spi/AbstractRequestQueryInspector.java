//
// AbstractRequestQueryInspector.java
//
//     A template for making a RequestQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.request.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for requests.
 */

public abstract class AbstractRequestQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.provisioning.RequestQueryInspector {

    private final java.util.Collection<org.osid.provisioning.records.RequestQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the request transaction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestTransactionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the request transaction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionQueryInspector[] getRequestTransactionTerms() {
        return (new org.osid.provisioning.RequestTransactionQueryInspector[0]);
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQueueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQueryInspector[] getQueueTerms() {
        return (new org.osid.provisioning.QueueQueryInspector[0]);
    }


    /**
     *  Gets the request date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getRequestDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequesterIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getRequesterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getRequestingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the pool <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPoolIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the pool query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQueryInspector[] getPoolTerms() {
        return (new org.osid.provisioning.PoolQueryInspector[0]);
    }


    /**
     *  Gets the requested provisionable <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestedProvisionableIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the requested provisionable query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQueryInspector[] getRequestedProvisionableTerms() {
        return (new org.osid.provisioning.ProvisionableQueryInspector[0]);
    }


    /**
     *  Gets the exchange provision <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getExchangeProvisionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the exchange provision query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQueryInspector[] getExchangeProvisionTerms() {
        return (new org.osid.provisioning.ProvisionQueryInspector[0]);
    }


    /**
     *  Gets the origin provision <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOriginProvisionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the origin provision query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQueryInspector[] getOriginProvisionTerms() {
        return (new org.osid.provisioning.ProvisionQueryInspector[0]);
    }


    /**
     *  Gets the position query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getPositionTerms() {
        return (new org.osid.search.terms.IntegerRangeTerm[0]);
    }


    /**
     *  Gets the ewa query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getEWATerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given request query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a request implementing the requested record.
     *
     *  @param requestRecordType a request record type
     *  @return the request query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestQueryInspectorRecord getRequestQueryInspectorRecord(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(requestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestRecordType + " is not supported");
    }


    /**
     *  Adds a record to this request query. 
     *
     *  @param requestQueryInspectorRecord request query inspector
     *         record
     *  @param requestRecordType request record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRequestQueryInspectorRecord(org.osid.provisioning.records.RequestQueryInspectorRecord requestQueryInspectorRecord, 
                                                   org.osid.type.Type requestRecordType) {

        addRecordType(requestRecordType);
        nullarg(requestRecordType, "request record type");
        this.records.add(requestQueryInspectorRecord);        
        return;
    }
}
