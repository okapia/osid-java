//
// AbstractProfileQuery.java
//
//     A template for making a Profile Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for profiles.
 */

public abstract class AbstractProfileQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.profile.ProfileQuery {

    private final java.util.Collection<org.osid.profile.records.ProfileQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the profile item <code> Id </code> for this query. 
     *
     *  @param  profileItemId a profile item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileItemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileItemId(org.osid.id.Id profileItemId, boolean match) {
        return;
    }


    /**
     *  Clears the profile item <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileItemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileItemQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile item query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile item. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile item query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuery getProfileItemQuery() {
        throw new org.osid.UnimplementedException("supportsProfileItemQuery() is false");
    }


    /**
     *  Matches profiles that have any profile item. 
     *
     *  @param  match <code> true </code> to match profiles with any item 
     *          mapping, <code> false </code> to match profiles with no item 
     *          mapping 
     */

    @OSID @Override
    public void matchAnyProfileItem(boolean match) {
        return;
    }


    /**
     *  Clears the profile item query terms. 
     */

    @OSID @Override
    public void clearProfileItemTerms() {
        return;
    }


    /**
     *  Sets the profile entry <code> Id </code> for this query. 
     *
     *  @param  profileEntryId a profile entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchProfileEntryId(org.osid.id.Id profileEntryId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the profile entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuery getProfileEntryQuery() {
        throw new org.osid.UnimplementedException("supportsProfileEntryQuery() is false");
    }


    /**
     *  Matches profiles that have any profile entry. 
     *
     *  @param  match <code> true </code> to match profiles with any entry 
     *          mapping, <code> false </code> to match profiles with no entry 
     *          mapping 
     */

    @OSID @Override
    public void matchAnyProfileEntry(boolean match) {
        return;
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearProfileEntryTerms() {
        return;
    }


    /**
     *  Sets the profile <code> Id </code> for this query to match profiles 
     *  that have the specified profile as an ancestor. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorProfileId(org.osid.id.Id profileId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorProfileIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorProfileQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getAncestorProfileQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorProfileQuery() is false");
    }


    /**
     *  Matches profiles with any ancestor. 
     *
     *  @param  match <code> true </code> to match profile with any ancestor, 
     *          <code> false </code> to match root profiles 
     */

    @OSID @Override
    public void matchAnyAncestorProfile(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor profile query terms. 
     */

    @OSID @Override
    public void clearAncestorProfileTerms() {
        return;
    }


    /**
     *  Sets the profile <code> Id </code> for this query to match that have 
     *  the specified profile as a descendant. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantProfileId(org.osid.id.Id profileId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantProfileIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantProfileQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getDescendantProfileQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantProfileQuery() is false");
    }


    /**
     *  Matches profiles with any descendant. 
     *
     *  @param  match <code> true </code> to match profile with any 
     *          descendant, <code> false </code> to match leaf profiles 
     */

    @OSID @Override
    public void matchAnyDescendantProfile(boolean match) {
        return;
    }


    /**
     *  Clears the descendant profile query terms. 
     */

    @OSID @Override
    public void clearDescendantProfileTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given profile query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a profile implementing the requested record.
     *
     *  @param profileRecordType a profile record type
     *  @return the profile query record
     *  @throws org.osid.NullArgumentException
     *          <code>profileRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileQueryRecord getProfileQueryRecord(org.osid.type.Type profileRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileQueryRecord record : this.records) {
            if (record.implementsRecordType(profileRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile query. 
     *
     *  @param profileQueryRecord profile query record
     *  @param profileRecordType profile record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileQueryRecord(org.osid.profile.records.ProfileQueryRecord profileQueryRecord, 
                                          org.osid.type.Type profileRecordType) {

        addRecordType(profileRecordType);
        nullarg(profileQueryRecord, "profile query record");
        this.records.add(profileQueryRecord);        
        return;
    }
}
