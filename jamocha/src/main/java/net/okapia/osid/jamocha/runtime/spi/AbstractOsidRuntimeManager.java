//
// AbstractOSIDManager
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.runtime.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractOsidRuntimeManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.OsidRuntimeManager {


    /**
     *  Constructs a new <code>AbstractOsidRuntimeManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractOsidRuntimeManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if a configuration service is provided within this runtime 
     *  environment. 
     *
     *  @return <code> true </code> if a configuration service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfiguration() {
        return (false);
    }


    /**
     *  Finds, loads and instantiates providers of OSID managers. Providers 
     *  must conform to an OsidManager interface. The interfaces are defined 
     *  in the OSID enumeration. For all OSID requests, an instance of <code> 
     *  OsidManager </code> that implements the <code> OsidManager </code> 
     *  interface is returned. In bindings where permitted, this can be safely 
     *  cast into the requested manager. 
     *
     *  @param  osid represents the OSID 
     *  @param  implClassName the name of the implementation 
     *  @param  version the minimum required OSID specification version 
     *  @return the manager of the service 
     *  @throws org.osid.ConfigurationErrorException an error in configuring 
     *          the implementation 
     *  @throws org.osid.NotFoundException the implementation class was not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> implClassName </code> or 
     *          <code> version </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> implClassName </code> 
     *          does not support the requested OSID 
     */

    @OSID @Override
    public org.osid.OsidManager getManager(org.osid.OSID osid, 
                                           String implClassName, 
                                           org.osid.installation.Version version)
        throws org.osid.ConfigurationErrorException,
               org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.OsidRuntimeManager.getManager not implemented");
    }


    /**
     *  Finds, loads and instantiates providers of OSID managers. Providers 
     *  must conform to an <code> OsidManager </code> interface. The 
     *  interfaces are defined in the OSID enumeration. For all OSID requests, 
     *  an instance of <code> OsidManager </code> that implements the <code> 
     *  OsidManager </code> interface is returned. In bindings where 
     *  permitted, this can be safely cast into the requested manager. 
     *
     *  @param  osid represents the OSID 
     *  @param  implementation the name of the implementation 
     *  @param  version the minimum required OSID specification version 
     *  @return the manager of the service 
     *  @throws org.osid.ConfigurationErrorException an error in configuring 
     *          the implementation 
     *  @throws org.osid.NotFoundException the implementation class was not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> implementation </code> 
     *          or <code> version </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> implementation </code> 
     *          does not support the requested OSID 
     */

    @OSID @Override
    public org.osid.OsidProxyManager getProxyManager(org.osid.OSID osid, 
                                                     String implementation, 
                                                     org.osid.installation.Version version)
        throws org.osid.ConfigurationErrorException,
               org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.OsidRuntimeManager.getProxyManager not implemented");
    }


    /**
     *  Gets the current configuration in the runtime environment. 
     *
     *  @return a configuration 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException an authorization failure 
     *          occured 
     *  @throws org.osid.UnimplementedException a configuration service is not 
     *          supported 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getConfiguration()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.UnimplementedException("org.osid.OsidRuntimeManager.getConfiguration not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
