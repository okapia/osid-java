//
// AbstractOsidSession.java
//
//     Supplies basic information in common throughout the sessions.
//
//
// Tom Coppeto
// OnTapSolutions
// 27 June 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology.
// Copyright (c) 2012 Okapia.  All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This is a abstract class for implementing a basic session. It
 *  manages locale and authentication.
 *
 *  If this session is constructed using a <code>Proxy</code>, then
 *  the <code>Locale<code> and <code>Authentication</code> is
 *  retrieved from the <code>Proxy</code> upon construction. 
 *
 *  The agent identity may be managed in this session in one of
 *  two ways.
 *
 *  <ul>
 *      <li>The agent may be explicitly set using
 *          <code>setAgent()</code>. In this case,
 *          <code>isAuthenticated()</code> always returns
 *          <code>true</code> if an agent is set and returns
 *          <code>false</code> if no agent is set.</code></li>
 *
 *      <li>The agent is retrieved from an explitly set
 *          <code>Authentication</code> interface. In this case,
 *          <code>isAuthenticated()</code> is mapped to
 *          <code>Authentication.isValid()</code>. Explicitly supplied
 *          agents take precedence over this.</li>
 *  </ul>
 *
 *  This session does not support transactions.
 */

public abstract class AbstractOsidSession
    implements org.osid.OsidSession {

    private org.osid.locale.Locale locale;

    private org.osid.authentication.Agent agent;
    private org.osid.authentication.Agent effectiveAgent;
    private org.osid.authentication.process.Authentication authentication;
    private org.osid.proxy.Proxy proxy;

    private java.util.Date startDate = new java.util.Date();
    private java.util.Date startEffectiveDate;
    private java.math.BigDecimal clockRate;
    private org.osid.type.Type formatType;

    private boolean closed = false;


    /**
     *  Constructs an <code>OsidSession</code>.
     */

    protected AbstractOsidSession() {
        return;
    }


    /**
     *  Sets a proxy. Session information will be pulled from the
     *  proxy iff none of the other parameters have been set. Setting
     *  individual items overrides this proxy.
     *
     *  @param proxy the proxy
     *  @throws org.osid.NullArgumentException <code>proxy</code> is
     *          <code>null</code>
     */

    protected synchronized void setSessionProxy(org.osid.proxy.Proxy proxy) {
        nullarg(proxy, "proxy");

        if (this.closed) {
            return;
        }

        this.proxy = proxy;

        if (this.authentication == null) {
            if (this.proxy.hasAuthentication()) {
                this.authentication = proxy.getAuthentication();
            }
        }

        if (this.startEffectiveDate == null) {
            if (proxy.hasEffectiveDate()) {
                this.startDate = new java.util.Date();
            }
        }

        return;
    }


    /**
     *  Gets the locale indicating the localization preferences in
     *  effect for this session. 
     *
     *  Uses a specific locale set, then falls back to the proxy.
     *
     *  @return the locale 
     */

    @OSID @Override
    public synchronized org.osid.locale.Locale getLocale() {
        if (this.locale != null) {
            return (locale);
        }

        org.osid.proxy.Proxy proxy = getProxy();
        if (proxy != null) {
            return (proxy.getLocale());
        }


        return (new net.okapia.osid.jamocha.nil.locale.locale.UnknownLocale());
    }


    /**
     *  Sets the locale. This method overrides a locale supplied
     *  through a proxy.
     *
     *  @param locale the locale for this session
     *  @throws org.osid.IllegalStateException this session has been closed 
     *  @throws org.osid.NullArgumentException <code>locale</code> is
     *          <code>null</code>
     */

    protected synchronized void setLocale(org.osid.locale.Locale locale) {
        nullarg(locale, "locale");

        if (this.closed) {
            return;
        }

        this.locale = locale;
        return;
    }


    /**
     *  Tests if there are valid authentication credentials used by this 
     *  service. 
     *
     *  Valid credentials exist of the agent is explicitly set, or an
     *  Authentication was provided where isValid() is true, or a
     *  proxy was provided that contains an Authentication where
     *  isValid() is true.
     *
     *  @return <code> true </code> if valid authentication
     *          credentials exist, <code> false </code> otherwise
     */

    @OSID @Override
    public synchronized boolean isAuthenticated() {        
        if (this.agent != null) {
            return (true);
        }

        if (this.authentication != null) {
            return (this.authentication.isValid());
        }

        org.osid.proxy.Proxy proxy = getProxy();
        if (proxy != null) {
            if (proxy.hasAuthentication()) {
                return (proxy.getAuthentication().isValid());
            }
        }

        return (false);
    }


    /**
     *  Gets the <code> Id </code> of the agent authenticated to this
     *  session.  
     *
     *  The agent may be explicitly set, or through Authentication
     *  provided where isValid() is true, or through proxy provided
     *  that contains an Authentication where isValid() is true.
     *
     *  @return the authenticated agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException
     *          <code>isAuthenticated()</code> is <code>false</code>
     */

    @OSID @Override
    public synchronized org.osid.id.Id getAuthenticatedAgentId() {
        if (!isAuthenticated()) {
            throw new org.osid.IllegalStateException("isAuthenticated() is false");
        }

        if (this.agent != null) {
            return (this.agent.getId());
        }

        /* isAuthenticated() checks for validity of the authentication */
        if (this.authentication != null) {
            return (this.authentication.getAgentId());
        }

        org.osid.proxy.Proxy proxy = getProxy();
        if (proxy != null) {
            if (proxy.hasAuthentication()) {
                return (proxy.getAuthentication().getAgentId());
            }
        }
        
        throw new org.osid.IllegalStateException("something wrong with authentication state in session");
    }


    /**
     *  Gets the agent authenticated to this session. 
     *
     *  The agent may be explicitly set, or through Authentication
     *  provided where isValid() is true, or through proxy provided
     *  that contains an Authentication where isValid() is true.
     *
     *  @return the authenticated agent 
     *  @throws org.osid.IllegalStateException <code> isAuthenticated() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public synchronized org.osid.authentication.Agent getAuthenticatedAgent()
        throws org.osid.OperationFailedException {

        if (!isAuthenticated()) {
            throw new org.osid.IllegalStateException("isAuthenticated() is false");
        }

        if (this.agent != null) {
            return (this.agent);
        }

        /* isAuthenticated() checks for validity of the authentication */
        if (this.authentication != null) {
            return (this.authentication.getAgent());
        }

        org.osid.proxy.Proxy proxy = getProxy();
        if (proxy != null) {
            if (proxy.hasAuthentication()) {
                return (proxy.getAuthentication().getAgent());
            }
        }
        
        throw new org.osid.IllegalStateException("something wrong with authentication state in session");
    }


    /**
     *  Sets an explicit agent to indicate who is authenticated. This
     *  method overrides an agent set using an authentication or
     *  proxy.
     *
     *  @param agent the authenticated entity
     *  @throws org.osid.IllegalStateException this session has been closed 
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected synchronized void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");

        if (this.closed) {
            return;
        }

        this.agent = agent;
        return;
    }


    /**
     *  Clears the explicitly set agent. Agent methods will fallback
     *  to the Authentication and Proxy.
     */
    
    protected synchronized void clearAgent() {
        this.agent = null;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the effective agent in use by this 
     *  session. If <code> isAuthenticated() </code> is true, then the 
     *  effective agent may be the same as the agent returned by <code> 
     *  getAuthenticatedAgent(). </code> If <code> isAuthenticated() </code> 
     *  is <code> false, </code> then the effective agent may be a default 
     *  agent used for authorization by an unknwon or anonymous user. 
     *
     *  @return the effective agent 
     */

    @OSID @Override
    public org.osid.id.Id getEffectiveAgentId() {
        if (this.effectiveAgent != null) {
            return (this.effectiveAgent.getId());
        }

        org.osid.proxy.Proxy proxy = getProxy();
        if (proxy != null) {
            if (proxy.hasAuthentication()) {
                return (proxy.getEffectiveAgentId());
            }
        }
        
        if (isAuthenticated()) {
            return (getAuthenticatedAgentId());
        }

        return (new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent().getId());
    }


    /**
     *  Gets the effective agent in use by this session.
     *
     *  This method returns the effective agent if explicitly set,
     *  then the Proxy. If none is found and the session is
     *  authenticated, the authenticated agent is used.
     *
     *  @return the effective agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public synchronized org.osid.authentication.Agent getEffectiveAgent()
        throws org.osid.OperationFailedException {

        if (this.effectiveAgent != null) {
            return (this.effectiveAgent);
        }

        org.osid.proxy.Proxy proxy = getProxy();
        if (proxy != null) {
            if (proxy.hasAuthentication()) {
                return (proxy.getEffectiveAgent());
            }
        }
        
        if (isAuthenticated()) {
            return (getAuthenticatedAgent());
        }

        return (new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent());
    }


    /**
     *  Sets an effective agent. This overrides the effective agent in
     *  the authentication or proxy.
     *
     *  @param agent an agent
     *  @throws org.osid.IllegalStateException this session has been closed 
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected synchronized void setEffectiveAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "effective agent");

        if (this.closed) {
            return;
        }

        this.effectiveAgent = agent;
        return;
    }

    
    /**
     *  Gets the proxy if available.
     *
     *  @return the session proxy or <code>null</code> if none was
     *          used for this session
     */

    protected synchronized org.osid.proxy.Proxy getProxy() {
        return (this.proxy);
    }


    /**
     *  Gets the <code>Authentication</code> if available. This method
     *  returns the <code>Authentication</code> from the
     *  <code>proxy</code> or one that was explicitly set.
     *
     *  @return the session authentication or <code>null</code> if
     *          none was used for this session
     */

    protected synchronized org.osid.authentication.process.Authentication getAuthentication() {
        return (this.authentication);
    }


    /**
     *  Sets the <code>Authentication</code>. This method overrides an
     *  <code>Authentication</code> supplied via <code>Proxy</code>.
     *
     *  @param authentication the session authentication
     *  @throws org.osid.NullArgumentException <code>authentication</code>
     *          is <code>null</code>
     */

    protected synchronized void setAuthentication(org.osid.authentication.process.Authentication authentication) {
        nullarg(authentication, "authentication");

        if (this.closed) {
            return;
        }

        this.authentication = authentication;
        return;
    }


    /**
     *  Gets the service date which may be the current date or the
     *  effective date in which this session exists.
     *
     *  @return the service date
     */

    @OSID @Override
    public synchronized java.util.Date getDate() {
        java.util.Date now = new java.util.Date();
        java.math.BigDecimal delta = new java.math.BigDecimal(now.getTime() - this.startDate.getTime());
        delta = delta.multiply(getClockRate());

        org.osid.proxy.Proxy proxy = getProxy();

        long start;
        if (this.startEffectiveDate != null) {
            start = this.startEffectiveDate.getTime();
        } else if ((proxy != null) && proxy.hasEffectiveDate()) {
            start = proxy.getEffectiveDate().getTime();
        } else {
            start = this.startDate.getTime();
        }
        
        return (new java.util.Date(start + delta.longValue()));
    }


    /**
     *  Sets the service date which may be the current date or the
     *  effective date in which this session exists. 
     *
     *  @param date the service date
     *  @throws org.osid.NullArgumentException {@code date} is
     *          {@code null}
     */

    protected synchronized void setDate(java.util.Date date) {
        nullarg(date, "date");

        if (this.closed) {
            return;
        }
        
        this.startDate = new java.util.Date();
        this.startEffectiveDate = date;

        return;
    }


    /**
     *  Gets the rate of the service clock.
     *
     *  @return the clock rate
     */

    @OSID @Override
    public synchronized java.math.BigDecimal getClockRate() {
        if (this.clockRate != null) {
            return (this.clockRate);
        }

        org.osid.proxy.Proxy proxy = getProxy();
        if (proxy != null) {
            if (proxy.hasEffectiveDate()) {
                return (proxy.getEffectiveClockRate());
            }
        }

        return (new java.math.BigDecimal(1));
    }


    /**
     *  Sets the rate of the service clock. This overrides the clock
     *  rate set in a proxy.
     *
     *  @param rate the clock rate
     *  @throws org.osid.NullArgumentException {@code rate} is {@code
     *          null}
     */

    protected synchronized void setClockRate(java.math.BigDecimal rate) {
        nullarg(rate, "clock rate");

        if (this.closed) {
            return;
        }

        this.clockRate = rate;
        return;
    }


    /**
     *  Gets the <code> DisplayText </code> format <code> Type </code> 
     *  preference in effect for this session. 
     *
     *  @return the effective <code> DisplayText </code> format <code> Type 
     *          </code> 
     */

    @OSID @Override
    public synchronized org.osid.type.Type getFormatType() {
        if (this.formatType != null) {
            return (formatType);
        }

        org.osid.proxy.Proxy proxy = getProxy();
        if (proxy != null) {
            if (proxy.hasFormatType()) {
                return (proxy.getFormatType());
            }
        }

        return (net.okapia.osid.primordium.types.text.FormatTypes.PLAIN.getType());
    }

    
    /**
     *  Sets the <code> DisplayText </code> format <code> Type </code>
     *  preference in effect for this session. This overrides a format
     *  type set in a proxy.
     *
     *  @param formatType the effective <code> DisplayText </code>
     *          format <code> Type </code>
     *  @throws org.osid.NullArgumentException <code>formatType</code>
     *          is <code>null</code>
     */

    protected synchronized void setFormatType(org.osid.type.Type formatType) {
        nullarg(formatType, "format type");

        if (this.closed) {
            return;
        }

        this.formatType = formatType;
        return;
    }


    /**
     *  Tests for the availability of transactions. 
     *
     *  @return <code> true </code> if transaction methods are available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransactions() {
        return (false);
    }


    /**
     *  Starts a new transaction for this sesson. Transactions are a
     *  means for an OSID to provide an all-or-nothing set of
     *  operations within a session and may be used to coordinate this
     *  service from an external transaction manager. A session
     *  supports one transaction at a time.  Starting a second
     *  transaction before the previous has been committed or aborted
     *  results in an <code> ILLEGAL_STATE </code> error.
     *
     *  @return a new transaction 
     *  @throws org.osid.IllegalStateException a transaction is already open 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException transactions not supported 
     */

    @OSID @Override
    public org.osid.transaction.Transaction startTransaction()
        throws org.osid.IllegalStateException,
               org.osid.OperationFailedException,
               org.osid.UnsupportedException {

        throw new org.osid.UnimplementedException("a transaction service is not available");
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.proxy = null;
        this.authentication = null;
        this.agent = null;
        this.effectiveAgent = null;
        this.clockRate = new java.math.BigDecimal(0);

        this.closed = true;
        
        return;
    }
}

