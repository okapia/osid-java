//
// AbstractMapProcedureLookupSession
//
//    A simple framework for providing a Procedure lookup service
//    backed by a fixed collection of procedures.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Procedure lookup service backed by a
 *  fixed collection of procedures. The procedures are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Procedures</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProcedureLookupSession
    extends net.okapia.osid.jamocha.recipe.spi.AbstractProcedureLookupSession
    implements org.osid.recipe.ProcedureLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.recipe.Procedure> procedures = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.recipe.Procedure>());


    /**
     *  Makes a <code>Procedure</code> available in this session.
     *
     *  @param  procedure a procedure
     *  @throws org.osid.NullArgumentException <code>procedure<code>
     *          is <code>null</code>
     */

    protected void putProcedure(org.osid.recipe.Procedure procedure) {
        this.procedures.put(procedure.getId(), procedure);
        return;
    }


    /**
     *  Makes an array of procedures available in this session.
     *
     *  @param  procedures an array of procedures
     *  @throws org.osid.NullArgumentException <code>procedures<code>
     *          is <code>null</code>
     */

    protected void putProcedures(org.osid.recipe.Procedure[] procedures) {
        putProcedures(java.util.Arrays.asList(procedures));
        return;
    }


    /**
     *  Makes a collection of procedures available in this session.
     *
     *  @param  procedures a collection of procedures
     *  @throws org.osid.NullArgumentException <code>procedures<code>
     *          is <code>null</code>
     */

    protected void putProcedures(java.util.Collection<? extends org.osid.recipe.Procedure> procedures) {
        for (org.osid.recipe.Procedure procedure : procedures) {
            this.procedures.put(procedure.getId(), procedure);
        }

        return;
    }


    /**
     *  Removes a Procedure from this session.
     *
     *  @param  procedureId the <code>Id</code> of the procedure
     *  @throws org.osid.NullArgumentException <code>procedureId<code> is
     *          <code>null</code>
     */

    protected void removeProcedure(org.osid.id.Id procedureId) {
        this.procedures.remove(procedureId);
        return;
    }


    /**
     *  Gets the <code>Procedure</code> specified by its <code>Id</code>.
     *
     *  @param  procedureId <code>Id</code> of the <code>Procedure</code>
     *  @return the procedure
     *  @throws org.osid.NotFoundException <code>procedureId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>procedureId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Procedure getProcedure(org.osid.id.Id procedureId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.recipe.Procedure procedure = this.procedures.get(procedureId);
        if (procedure == null) {
            throw new org.osid.NotFoundException("procedure not found: " + procedureId);
        }

        return (procedure);
    }


    /**
     *  Gets all <code>Procedures</code>. In plenary mode, the returned
     *  list contains all known procedures or an error
     *  results. Otherwise, the returned list may contain only those
     *  procedures that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Procedures</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProcedures()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.procedure.ArrayProcedureList(this.procedures.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.procedures.clear();
        super.close();
        return;
    }
}
