//
// AbstractJob.java
//
//     Defines a Job builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.job.spi;


/**
 *  Defines a <code>Job</code> builder.
 */

public abstract class AbstractJobBuilder<T extends AbstractJobBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidGovernatorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.resourcing.job.JobMiter job;


    /**
     *  Constructs a new <code>AbstractJobBuilder</code>.
     *
     *  @param job the job to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractJobBuilder(net.okapia.osid.jamocha.builder.resourcing.job.JobMiter job) {
        super(job);
        this.job = job;
        return;
    }


    /**
     *  Builds the job.
     *
     *  @return the new job
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.resourcing.Job build() {
        (new net.okapia.osid.jamocha.builder.validator.resourcing.job.JobValidator(getValidations())).validate(this.job);
        return (new net.okapia.osid.jamocha.builder.resourcing.job.ImmutableJob(this.job));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the job miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.resourcing.job.JobMiter getMiter() {
        return (this.job);
    }


    /**
     *  Adds a competency.
     *
     *  @param competency a competency
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>competency</code>
     *          is <code>null</code>
     */

    public T competency(org.osid.resourcing.Competency competency) {
        getMiter().addCompetency(competency);
        return (self());
    }


    /**
     *  Sets all the competencies.
     *
     *  @param competencies a collection of competencies
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>competencies</code> is <code>null</code>
     */

    public T competencies(java.util.Collection<org.osid.resourcing.Competency> competencies) {
        getMiter().setCompetencies(competencies);
        return (self());
    }


    /**
     *  Adds a Job record.
     *
     *  @param record a job record
     *  @param recordType the type of job record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.resourcing.records.JobRecord record, org.osid.type.Type recordType) {
        getMiter().addJobRecord(record, recordType);
        return (self());
    }
}       


