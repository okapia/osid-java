//
// MutableIndexedMapActionGroupLookupSession
//
//    Implements an ActionGroup lookup service backed by a collection of
//    actionGroups indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements an ActionGroup lookup service backed by a collection of
 *  action groups. The action groups are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some action groups may be compatible
 *  with more types than are indicated through these action group
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of action groups can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapActionGroupLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractIndexedMapActionGroupLookupSession
    implements org.osid.control.ActionGroupLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapActionGroupLookupSession} with no action groups.
     *
     *  @param system the system
     *  @throws org.osid.NullArgumentException {@code system}
     *          is {@code null}
     */

      public MutableIndexedMapActionGroupLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapActionGroupLookupSession} with a
     *  single action group.
     *  
     *  @param system the system
     *  @param  actionGroup an single actionGroup
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code actionGroup} is {@code null}
     */

    public MutableIndexedMapActionGroupLookupSession(org.osid.control.System system,
                                                  org.osid.control.ActionGroup actionGroup) {
        this(system);
        putActionGroup(actionGroup);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapActionGroupLookupSession} using an
     *  array of action groups.
     *
     *  @param system the system
     *  @param  actionGroups an array of action groups
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code actionGroups} is {@code null}
     */

    public MutableIndexedMapActionGroupLookupSession(org.osid.control.System system,
                                                  org.osid.control.ActionGroup[] actionGroups) {
        this(system);
        putActionGroups(actionGroups);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapActionGroupLookupSession} using a
     *  collection of action groups.
     *
     *  @param system the system
     *  @param  actionGroups a collection of action groups
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code actionGroups} is {@code null}
     */

    public MutableIndexedMapActionGroupLookupSession(org.osid.control.System system,
                                                  java.util.Collection<? extends org.osid.control.ActionGroup> actionGroups) {

        this(system);
        putActionGroups(actionGroups);
        return;
    }
    

    /**
     *  Makes an {@code ActionGroup} available in this session.
     *
     *  @param  actionGroup an action group
     *  @throws org.osid.NullArgumentException {@code actionGroup{@code  is
     *          {@code null}
     */

    @Override
    public void putActionGroup(org.osid.control.ActionGroup actionGroup) {
        super.putActionGroup(actionGroup);
        return;
    }


    /**
     *  Makes an array of action groups available in this session.
     *
     *  @param  actionGroups an array of action groups
     *  @throws org.osid.NullArgumentException {@code actionGroups{@code 
     *          is {@code null}
     */

    @Override
    public void putActionGroups(org.osid.control.ActionGroup[] actionGroups) {
        super.putActionGroups(actionGroups);
        return;
    }


    /**
     *  Makes collection of action groups available in this session.
     *
     *  @param  actionGroups a collection of action groups
     *  @throws org.osid.NullArgumentException {@code actionGroup{@code  is
     *          {@code null}
     */

    @Override
    public void putActionGroups(java.util.Collection<? extends org.osid.control.ActionGroup> actionGroups) {
        super.putActionGroups(actionGroups);
        return;
    }


    /**
     *  Removes an ActionGroup from this session.
     *
     *  @param actionGroupId the {@code Id} of the action group
     *  @throws org.osid.NullArgumentException {@code actionGroupId{@code  is
     *          {@code null}
     */

    @Override
    public void removeActionGroup(org.osid.id.Id actionGroupId) {
        super.removeActionGroup(actionGroupId);
        return;
    }    
}
