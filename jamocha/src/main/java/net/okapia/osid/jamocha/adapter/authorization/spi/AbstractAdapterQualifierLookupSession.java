//
// AbstractAdapterQualifierLookupSession.java
//
//    A Qualifier lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authorization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Qualifier lookup session adapter.
 */

public abstract class AbstractAdapterQualifierLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authorization.QualifierLookupSession {

    private final org.osid.authorization.QualifierLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterQualifierLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterQualifierLookupSession(org.osid.authorization.QualifierLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Vault/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Vault Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.session.getVaultId());
    }


    /**
     *  Gets the {@code Vault} associated with this session.
     *
     *  @return the {@code Vault} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getVault());
    }


    /**
     *  Tests if this user can perform {@code Qualifier} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupQualifiers() {
        return (this.session.canLookupQualifiers());
    }


    /**
     *  A complete view of the {@code Qualifier} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQualifierView() {
        this.session.useComparativeQualifierView();
        return;
    }


    /**
     *  A complete view of the {@code Qualifier} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQualifierView() {
        this.session.usePlenaryQualifierView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include qualifiers in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.session.useFederatedVaultView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.session.useIsolatedVaultView();
        return;
    }
    
     
    /**
     *  Gets the {@code Qualifier} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Qualifier} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Qualifier} and
     *  retained for compatibility.
     *
     *  @param qualifierId {@code Id} of the {@code Qualifier}
     *  @return the qualifier
     *  @throws org.osid.NotFoundException {@code qualifierId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code qualifierId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Qualifier getQualifier(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQualifier(qualifierId));
    }


    /**
     *  Gets a {@code QualifierList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  qualifiers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Qualifiers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  qualifierIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Qualifier} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code qualifierIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByIds(org.osid.id.IdList qualifierIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQualifiersByIds(qualifierIds));
    }


    /**
     *  Gets a {@code QualifierList} corresponding to the given
     *  qualifier genus {@code Type} which does not include
     *  qualifiers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  qualifierGenusType a qualifier genus type 
     *  @return the returned {@code Qualifier} list
     *  @throws org.osid.NullArgumentException
     *          {@code qualifierGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByGenusType(org.osid.type.Type qualifierGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQualifiersByGenusType(qualifierGenusType));
    }


    /**
     *  Gets a {@code QualifierList} corresponding to the given
     *  qualifier genus {@code Type} and include any additional
     *  qualifiers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  qualifierGenusType a qualifier genus type 
     *  @return the returned {@code Qualifier} list
     *  @throws org.osid.NullArgumentException
     *          {@code qualifierGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByParentGenusType(org.osid.type.Type qualifierGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQualifiersByParentGenusType(qualifierGenusType));
    }


    /**
     *  Gets a {@code QualifierList} containing the given
     *  qualifier record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  qualifierRecordType a qualifier record type 
     *  @return the returned {@code Qualifier} list
     *  @throws org.osid.NullArgumentException
     *          {@code qualifierRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByRecordType(org.osid.type.Type qualifierRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQualifiersByRecordType(qualifierRecordType));
    }


    /**
     *  Gets all {@code Qualifiers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Qualifiers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQualifiers());
    }
}
