//
// AbstractMapProfileItemLookupSession
//
//    A simple framework for providing a ProfileItem lookup service
//    backed by a fixed collection of profile items.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ProfileItem lookup service backed by a
 *  fixed collection of profile items. The profile items are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProfileItems</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProfileItemLookupSession
    extends net.okapia.osid.jamocha.profile.spi.AbstractProfileItemLookupSession
    implements org.osid.profile.ProfileItemLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.profile.ProfileItem> profileItems = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.profile.ProfileItem>());


    /**
     *  Makes a <code>ProfileItem</code> available in this session.
     *
     *  @param  profileItem a profile item
     *  @throws org.osid.NullArgumentException <code>profileItem<code>
     *          is <code>null</code>
     */

    protected void putProfileItem(org.osid.profile.ProfileItem profileItem) {
        this.profileItems.put(profileItem.getId(), profileItem);
        return;
    }


    /**
     *  Makes an array of profile items available in this session.
     *
     *  @param  profileItems an array of profile items
     *  @throws org.osid.NullArgumentException <code>profileItems<code>
     *          is <code>null</code>
     */

    protected void putProfileItems(org.osid.profile.ProfileItem[] profileItems) {
        putProfileItems(java.util.Arrays.asList(profileItems));
        return;
    }


    /**
     *  Makes a collection of profile items available in this session.
     *
     *  @param  profileItems a collection of profile items
     *  @throws org.osid.NullArgumentException <code>profileItems<code>
     *          is <code>null</code>
     */

    protected void putProfileItems(java.util.Collection<? extends org.osid.profile.ProfileItem> profileItems) {
        for (org.osid.profile.ProfileItem profileItem : profileItems) {
            this.profileItems.put(profileItem.getId(), profileItem);
        }

        return;
    }


    /**
     *  Removes a ProfileItem from this session.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @throws org.osid.NullArgumentException <code>profileItemId<code> is
     *          <code>null</code>
     */

    protected void removeProfileItem(org.osid.id.Id profileItemId) {
        this.profileItems.remove(profileItemId);
        return;
    }


    /**
     *  Gets the <code>ProfileItem</code> specified by its <code>Id</code>.
     *
     *  @param  profileItemId <code>Id</code> of the <code>ProfileItem</code>
     *  @return the profileItem
     *  @throws org.osid.NotFoundException <code>profileItemId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>profileItemId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItem getProfileItem(org.osid.id.Id profileItemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.profile.ProfileItem profileItem = this.profileItems.get(profileItemId);
        if (profileItem == null) {
            throw new org.osid.NotFoundException("profileItem not found: " + profileItemId);
        }

        return (profileItem);
    }


    /**
     *  Gets all <code>ProfileItems</code>. In plenary mode, the returned
     *  list contains all known profileItems or an error
     *  results. Otherwise, the returned list may contain only those
     *  profileItems that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ProfileItems</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.profileitem.ArrayProfileItemList(this.profileItems.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.profileItems.clear();
        super.close();
        return;
    }
}
