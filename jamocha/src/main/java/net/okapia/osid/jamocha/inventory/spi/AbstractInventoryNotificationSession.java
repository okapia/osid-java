//
// AbstractInventoryNotificationSession.java
//
//     A template for making InventoryNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Inventory} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Inventory} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for inventory entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractInventoryNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inventory.InventoryNotificationSession {

    private boolean federated = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();


    /**
     *  Gets the {@code Warehouse/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Warehouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }

    
    /**
     *  Gets the {@code Warehouse} associated with this session.
     *
     *  @return the {@code Warehouse} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the {@code Warehouse}.
     *
     *  @param warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException {@code warehouse}
     *          is {@code null}
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can register for {@code Inventory}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForInventoryNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for inventories in warehouses
     *  which are children of this warehouse in the warehouse
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new inventories. {@code
     *  InventoryReceiver.newInventory()} is invoked when an new
     *  {@code Inventory} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new inventories for the given
     *  stock {@code Id}. {@code InventoryReceiver.newInventory()} is
     *  invoked when a new {@code Inventory} is created.
     *
     *  @param  stockId the {@code Id} of the stock to monitor
     *  @throws org.osid.NullArgumentException {@code stockId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated inventories. {@code
     *  InventoryReceiver.changedInventory()} is invoked when an
     *  inventory is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated inventories for the
     *  given stock {@code Id}. {@code
     *  InventoryReceiver.changedInventory()} is invoked when a {@code
     *  Inventory} in this warehouse is changed.
     *
     *  @param  stockId the {@code Id} of the stock to monitor
     *  @throws org.osid.NullArgumentException {@code stockId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated inventory. {@code
     *  InventoryReceiver.changedInventory()} is invoked when the
     *  specified inventory is changed.
     *
     *  @param inventoryId the {@code Id} of the {@code Inventory} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code inventoryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedInventory(org.osid.id.Id inventoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted inventories. {@code
     *  InventoryReceiver.deletedInventory()} is invoked when an
     *  inventory is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted inventories for the
     *  given stock {@code Id}. {@code
     *  InventoryReceiver.deletedInventory()} is invoked when a {@code
     *  Inventory} is deleted or removed from this warehouse.
     *
     *  @param  stockId the {@code Id} of the stock to monitor
     *  @throws org.osid.NullArgumentException {@code stockId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted inventory. {@code
     *  InventoryReceiver.deletedInventory()} is invoked when the
     *  specified inventory is deleted.
     *
     *  @param inventoryId the {@code Id} of the
     *          {@code Inventory} to monitor
     *  @throws org.osid.NullArgumentException {@code inventoryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedInventory(org.osid.id.Id inventoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
