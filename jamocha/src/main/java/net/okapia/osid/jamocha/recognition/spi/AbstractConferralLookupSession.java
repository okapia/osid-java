//
// AbstractConferralLookupSession.java
//
//    A starter implementation framework for providing a Conferral
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Conferral
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getConferrals(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractConferralLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.recognition.ConferralLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.recognition.Academy academy = new net.okapia.osid.jamocha.nil.recognition.academy.UnknownAcademy();
    

    /**
     *  Gets the <code>Academy/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Academy Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.academy.getId());
    }


    /**
     *  Gets the <code>Academy</code> associated with this session.
     *
     *  @return the <code>Academy</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.academy);
    }


    /**
     *  Sets the <code>Academy</code>.
     *
     *  @param  academy the academy for this session
     *  @throws org.osid.NullArgumentException <code>academy</code>
     *          is <code>null</code>
     */

    protected void setAcademy(org.osid.recognition.Academy academy) {
        nullarg(academy, "academy");
        this.academy = academy;
        return;
    }


    /**
     *  Tests if this user can perform <code>Conferral</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupConferrals() {
        return (true);
    }


    /**
     *  A complete view of the <code>Conferral</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeConferralView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Conferral</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryConferralView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include conferrals in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only conferrals whose effective dates are current are returned
     *  by methods in this session.
     */

    @OSID @Override
    public void useEffectiveConferralView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All conferrals of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveConferralView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Conferral</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Conferral</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Conferral</code> and
     *  retained for compatibility.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralId <code>Id</code> of the
     *          <code>Conferral</code>
     *  @return the conferral
     *  @throws org.osid.NotFoundException <code>conferralId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>conferralId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Conferral getConferral(org.osid.id.Id conferralId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.recognition.ConferralList conferrals = getConferrals()) {
            while (conferrals.hasNext()) {
                org.osid.recognition.Conferral conferral = conferrals.getNextConferral();
                if (conferral.getId().equals(conferralId)) {
                    return (conferral);
                }
            }
        } 

        throw new org.osid.NotFoundException(conferralId + " not found");
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  conferrals specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Conferrals</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getConferrals()</code>.
     *
     *  @param conferralIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>conferralIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByIds(org.osid.id.IdList conferralIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.recognition.Conferral> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = conferralIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getConferral(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("conferral " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.recognition.conferral.LinkedConferralList(ret));
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  conferral genus <code>Type</code> which does not include
     *  conferrals of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getConferrals()</code>.
     *
     *  @param conferralGenusType a conferral genus type
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByGenusType(org.osid.type.Type conferralGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralGenusFilterList(getConferrals(), conferralGenusType));
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  conferral genus <code>Type</code> and include any additional
     *  conferrals with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getConferrals()</code>.
     *
     *  @param conferralGenusType a conferral genus type
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByParentGenusType(org.osid.type.Type conferralGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getConferralsByGenusType(conferralGenusType));
    }


    /**
     *  Gets a <code>ConferralList</code> containing the given
     *  conferral record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getConferrals()</code>.
     *
     *  @param conferralRecordType a conferral record type
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByRecordType(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralRecordFilterList(getConferrals(), conferralRecordType));
    }


    /**
     *  Gets a <code>ConferralList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In active mode, conferrals are returned that are currently
     *  active. In any status mode, active and inactive conferrals are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Conferral</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsOnDate(org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.TemporalConferralFilterList(getConferrals(), from, to));
    }
        

    /**
     *  Gets a list of conferrals corresponding to a recipient
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the recipient
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsForRecipient(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralFilterList(new RecipientFilter(resourceId), getConferrals()));
    }


    /**
     *  Gets a list of conferrals corresponding to a recipient
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.TemporalConferralFilterList(getConferralsForRecipient(resourceId), from, to));
    }


    /**
     *  Gets a list of conferrals corresponding to an award
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param awardId the <code>Id</code> of the award
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsForAward(org.osid.id.Id awardId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralFilterList(new AwardFilter(awardId), getConferrals()));
    }


    /**
     *  Gets a list of conferrals corresponding to an award
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForAwardOnDate(org.osid.id.Id awardId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.TemporalConferralFilterList(getConferralsForAward(awardId), from, to));
    }


    /**
     *  Gets a list of conferrals corresponding to recipient and award
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the recipient
     *  @param  awardId the <code>Id</code> of the award
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>awardId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientAndAward(org.osid.id.Id resourceId,
                                                                        org.osid.id.Id awardId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralFilterList(new AwardFilter(awardId), getConferralsForRecipient(resourceId)));
    }


    /**
     *  Gets a list of conferrals corresponding to recipient and award
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>awardId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientAndAwardOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.id.Id awardId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.TemporalConferralFilterList(getConferralsForRecipientAndAward(resourceId, awardId), from, to));
    }


    /**
     *  Gets a list of conferrals corresponding to a reference
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the reference
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsByReference(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralFilterList(new ReferenceFilter(resourceId), getConferrals()));
    }


    /**
     *  Gets a list of conferrals corresponding to a reference
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByReferenceOnDate(org.osid.id.Id resourceId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.TemporalConferralFilterList(getConferralsByReference(resourceId), from, to));
    }


    /**
     *  Gets a list of conferrals corresponding to a convocation
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the convocation
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsByConvocation(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralFilterList(new ConvocationFilter(resourceId), getConferrals()));
    }


    /**
     *  Gets a list of conferrals corresponding to a convocation
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the convocation
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByConvocationOnDate(org.osid.id.Id resourceId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.conferral.TemporalConferralFilterList(getConferralsByConvocation(resourceId), from, to));
    }
    
    
    /**
     *  Gets all <code>Conferrals</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Conferrals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.recognition.ConferralList getConferrals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the conferral list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of conferrals
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.recognition.ConferralList filterConferralsOnViews(org.osid.recognition.ConferralList list)
        throws org.osid.OperationFailedException {

        org.osid.recognition.ConferralList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.recognition.conferral.EffectiveConferralFilterList(ret);
        }

        return (ret);
    }


    public static class RecipientFilter
        implements net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>RecipientFilter</code>.
         *
         *  @param resourceId the recipient to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public RecipientFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "recipient Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ConferralFilterList to filter the conferral
         *  list based on recipient.
         *
         *  @param conferral the conferral
         *  @return <code>true</code> to pass the conferral,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.recognition.Conferral conferral) {
            return (conferral.getRecipientId().equals(this.resourceId));
        }
    }


    public static class ReferenceFilter
        implements net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ReferenceFilter</code>.
         *
         *  @param resourceId the reference to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ReferenceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "reference Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ConferralFilterList to filter the conferral
         *  list based on reference.
         *
         *  @param conferral the conferral
         *  @return <code>true</code> to pass the conferral,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.recognition.Conferral conferral) {
            if (conferral.hasReference()) {
                return (conferral.getReferenceId().equals(this.resourceId));
            } else {
                return (false);
            }
        }
    }


    public static class ConvocationFilter
        implements net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ConvocationFilter</code>.
         *
         *  @param resourceId the convocation to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ConvocationFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "convocation Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ConferralFilterList to filter the conferral
         *  list based on convocation.
         *
         *  @param conferral the conferral
         *  @return <code>true</code> to pass the conferral,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.recognition.Conferral conferral) {
            if (conferral.hasConvocation()) {
                return (conferral.getConvocationId().equals(this.resourceId));
            } else {
                return (false);
            }
        }
    }


    public static class AwardFilter
        implements net.okapia.osid.jamocha.inline.filter.recognition.conferral.ConferralFilter {
         
        private final org.osid.id.Id awardId;
         
         
        /**
         *  Constructs a new <code>AwardFilter</code>.
         *
         *  @param awardId the award to filter
         *  @throws org.osid.NullArgumentException
         *          <code>awardId</code> is <code>null</code>
         */
        
        public AwardFilter(org.osid.id.Id awardId) {
            nullarg(awardId, "award Id");
            this.awardId = awardId;
            return;
        }

         
        /**
         *  Used by the ConferralFilterList to filter the 
         *  conferral list based on award.
         *
         *  @param conferral the conferral
         *  @return <code>true</code> to pass the conferral,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.recognition.Conferral conferral) {
            return (conferral.getAwardId().equals(this.awardId));
        }
    }
}
