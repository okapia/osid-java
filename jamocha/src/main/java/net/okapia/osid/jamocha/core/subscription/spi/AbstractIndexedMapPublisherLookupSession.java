//
// AbstractIndexedMapPublisherLookupSession.java
//
//    A simple framework for providing a Publisher lookup service
//    backed by a fixed collection of publishers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Publisher lookup service backed by a
 *  fixed collection of publishers. The publishers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some publishers may be compatible
 *  with more types than are indicated through these publisher
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Publishers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPublisherLookupSession
    extends AbstractMapPublisherLookupSession
    implements org.osid.subscription.PublisherLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.subscription.Publisher> publishersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.subscription.Publisher>());
    private final MultiMap<org.osid.type.Type, org.osid.subscription.Publisher> publishersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.subscription.Publisher>());


    /**
     *  Makes a <code>Publisher</code> available in this session.
     *
     *  @param  publisher a publisher
     *  @throws org.osid.NullArgumentException <code>publisher<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPublisher(org.osid.subscription.Publisher publisher) {
        super.putPublisher(publisher);

        this.publishersByGenus.put(publisher.getGenusType(), publisher);
        
        try (org.osid.type.TypeList types = publisher.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.publishersByRecord.put(types.getNextType(), publisher);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a publisher from this session.
     *
     *  @param publisherId the <code>Id</code> of the publisher
     *  @throws org.osid.NullArgumentException <code>publisherId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePublisher(org.osid.id.Id publisherId) {
        org.osid.subscription.Publisher publisher;
        try {
            publisher = getPublisher(publisherId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.publishersByGenus.remove(publisher.getGenusType());

        try (org.osid.type.TypeList types = publisher.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.publishersByRecord.remove(types.getNextType(), publisher);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePublisher(publisherId);
        return;
    }


    /**
     *  Gets a <code>PublisherList</code> corresponding to the given
     *  publisher genus <code>Type</code> which does not include
     *  publishers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known publishers or an error results. Otherwise,
     *  the returned list may contain only those publishers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  publisherGenusType a publisher genus type 
     *  @return the returned <code>Publisher</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>publisherGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByGenusType(org.osid.type.Type publisherGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.publisher.ArrayPublisherList(this.publishersByGenus.get(publisherGenusType)));
    }


    /**
     *  Gets a <code>PublisherList</code> containing the given
     *  publisher record <code>Type</code>. In plenary mode, the
     *  returned list contains all known publishers or an error
     *  results. Otherwise, the returned list may contain only those
     *  publishers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  publisherRecordType a publisher record type 
     *  @return the returned <code>publisher</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>publisherRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByRecordType(org.osid.type.Type publisherRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.publisher.ArrayPublisherList(this.publishersByRecord.get(publisherRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.publishersByGenus.clear();
        this.publishersByRecord.clear();

        super.close();

        return;
    }
}
