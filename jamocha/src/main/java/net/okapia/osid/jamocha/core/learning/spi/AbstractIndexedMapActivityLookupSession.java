//
// AbstractIndexedMapActivityLookupSession.java
//
//    A simple framework for providing an Activity lookup service
//    backed by a fixed collection of activities with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Activity lookup service backed by a
 *  fixed collection of activities. The activities are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some activities may be compatible
 *  with more types than are indicated through these activity
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Activities</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapActivityLookupSession
    extends AbstractMapActivityLookupSession
    implements org.osid.learning.ActivityLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.learning.Activity> activitiesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.learning.Activity>());
    private final MultiMap<org.osid.type.Type, org.osid.learning.Activity> activitiesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.learning.Activity>());


    /**
     *  Makes an <code>Activity</code> available in this session.
     *
     *  @param  activity an activity
     *  @throws org.osid.NullArgumentException <code>activity<code> is
     *          <code>null</code>
     */

    @Override
    protected void putActivity(org.osid.learning.Activity activity) {
        super.putActivity(activity);

        this.activitiesByGenus.put(activity.getGenusType(), activity);
        
        try (org.osid.type.TypeList types = activity.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.activitiesByRecord.put(types.getNextType(), activity);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an activity from this session.
     *
     *  @param activityId the <code>Id</code> of the activity
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeActivity(org.osid.id.Id activityId) {
        org.osid.learning.Activity activity;
        try {
            activity = getActivity(activityId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.activitiesByGenus.remove(activity.getGenusType());

        try (org.osid.type.TypeList types = activity.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.activitiesByRecord.remove(types.getNextType(), activity);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeActivity(activityId);
        return;
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> which does not include
     *  activities of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known activities or an error results. Otherwise,
     *  the returned list may contain only those activities that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.activity.ArrayActivityList(this.activitiesByGenus.get(activityGenusType)));
    }


    /**
     *  Gets an <code>ActivityList</code> containing the given
     *  activity record <code>Type</code>. In plenary mode, the
     *  returned list contains all known activities or an error
     *  results. Otherwise, the returned list may contain only those
     *  activities that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned <code>activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.activity.ArrayActivityList(this.activitiesByRecord.get(activityRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.activitiesByGenus.clear();
        this.activitiesByRecord.clear();

        super.close();

        return;
    }
}
