//
// AbstractJobConstrainerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractJobConstrainerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.rules.JobConstrainerSearchResults {

    private org.osid.resourcing.rules.JobConstrainerList jobConstrainers;
    private final org.osid.resourcing.rules.JobConstrainerQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractJobConstrainerSearchResults.
     *
     *  @param jobConstrainers the result set
     *  @param jobConstrainerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>jobConstrainers</code>
     *          or <code>jobConstrainerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractJobConstrainerSearchResults(org.osid.resourcing.rules.JobConstrainerList jobConstrainers,
                                            org.osid.resourcing.rules.JobConstrainerQueryInspector jobConstrainerQueryInspector) {
        nullarg(jobConstrainers, "job constrainers");
        nullarg(jobConstrainerQueryInspector, "job constrainer query inspectpr");

        this.jobConstrainers = jobConstrainers;
        this.inspector = jobConstrainerQueryInspector;

        return;
    }


    /**
     *  Gets the job constrainer list resulting from a search.
     *
     *  @return a job constrainer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainers() {
        if (this.jobConstrainers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.rules.JobConstrainerList jobConstrainers = this.jobConstrainers;
        this.jobConstrainers = null;
	return (jobConstrainers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.rules.JobConstrainerQueryInspector getJobConstrainerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  job constrainer search record <code> Type. </code> This method must
     *  be used to retrieve a jobConstrainer implementing the requested
     *  record.
     *
     *  @param jobConstrainerSearchRecordType a jobConstrainer search 
     *         record type 
     *  @return the job constrainer search
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(jobConstrainerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerSearchResultsRecord getJobConstrainerSearchResultsRecord(org.osid.type.Type jobConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.rules.records.JobConstrainerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(jobConstrainerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(jobConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record job constrainer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addJobConstrainerRecord(org.osid.resourcing.rules.records.JobConstrainerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "job constrainer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
