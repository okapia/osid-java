//
// AbstractAssemblyLocationQuery.java
//
//     A LocationQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.location.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A LocationQuery that stores terms.
 */

public abstract class AbstractAssemblyLocationQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.mapping.LocationQuery,
               org.osid.mapping.LocationQueryInspector,
               org.osid.mapping.LocationSearchOrder {

    private final java.util.Collection<org.osid.mapping.records.LocationQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.records.LocationQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.records.LocationSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyLocationQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyLocationQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches locations containing the specified <code> Coordinate. </code> 
     *
     *  @param  coordinate a coordinate 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        getAssembler().addCoordinateTerm(getCoordinateColumn(), coordinate, match);
        return;
    }


    /**
     *  Matches locations that have any coordinate assignment. 
     *
     *  @param  match <code> true </code> to match locations with any 
     *          coordinate, <code> false </code> to match locations with no 
     *          coordinates 
     */

    @OSID @Override
    public void matchAnyCoordinate(boolean match) {
        getAssembler().addCoordinateWildcardTerm(getCoordinateColumn(), match);
        return;
    }


    /**
     *  Clears the coordinate query terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        getAssembler().clearTerms(getCoordinateColumn());
        return;
    }


    /**
     *  Gets the coordinate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (getAssembler().getCoordinateTerms(getCoordinateColumn()));
    }


    /**
     *  Gets the Coordinate column name.
     *
     * @return the column name
     */

    protected String getCoordinateColumn() {
        return ("coordinate");
    }


    /**
     *  Matches locations containing the specified <code> SpatialUnit. </code> 
     *
     *  @param  spatialUnit a spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainedSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                          boolean match) {
        getAssembler().addSpatialUnitTerm(getContainedSpatialUnitColumn(), spatialUnit, match);
        return;
    }


    /**
     *  Clears the spatial unit terms. 
     */

    @OSID @Override
    public void clearContainedSpatialUnitTerms() {
        getAssembler().clearTerms(getContainedSpatialUnitColumn());
        return;
    }


    /**
     *  Gets the contained spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getContainedSpatialUnitTerms() {
        return (getAssembler().getSpatialUnitTerms(getContainedSpatialUnitColumn()));
    }


    /**
     *  Gets the ContainedSpatialUnit column name.
     *
     * @return the column name
     */

    protected String getContainedSpatialUnitColumn() {
        return ("contained_spatial_unit");
    }


    /**
     *  Matches locations overlapping with the specified <code> SpatialUnit. 
     *  </code> 
     *
     *  @param  spatialUnit a spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOverlappingSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                            boolean match) {
        getAssembler().addSpatialUnitTerm(getOverlappingSpatialUnitColumn(), spatialUnit, match);
        return;
    }


    /**
     *  Clears the overlapping spatial unit terms. 
     */

    @OSID @Override
    public void clearOverlappingSpatialUnitTerms() {
        getAssembler().clearTerms(getOverlappingSpatialUnitColumn());
        return;
    }


    /**
     *  Gets the overlapping spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getOverlappingSpatialUnitTerms() {
        return (getAssembler().getSpatialUnitTerms(getOverlappingSpatialUnitColumn()));
    }


    /**
     *  Gets the Overlapping SpatialUnit column name.
     *
     *  @return the column name
     */

    protected String getOverlappingSpatialUnitColumn() {
        return ("overlapping_spatial_unit");
    }


    /**
     *  Matches locations that have any spatial unit assignment.
     *
     *  @param  match <code> true </code> to match locations with any
     *          boundary, <code> false </code> to match locations with no
     *          boundaries
     */

    @OSID @Override
    public void matchAnySpatialUnit(boolean match) {
        getAssembler().addSpatialUnitWildcardTerm(getSpatialUnitColumn(), match);
        return;
    }


    /**
     *  Clears the spatial unit query terms.
     */

    @OSID @Override
    public void clearSpatialUnitTerms() {
        getAssembler().clearTerms(getSpatialUnitColumn());
        return;
    }


    /**
     *  Gets the overlapping spatial unit query terms.
     *
     *  @return the query terms
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialUnitTerms() {
        return (getAssembler().getSpatialUnitTerms(getSpatialUnitColumn()));
    }


    /**
     *  Gets the SpatialUnit column name.
     *
     *  @return the column name
     */

    protected String getSpatialUnitColumn() {
        return ("spatial_unit");
    }


    /**
     *  Orders the results by distance.
     *
     *  @param  style search order style
     *  @throws org.osid.NullArgumentException <code> style </code> is <code>
     *          null </code>
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDistanceColumn(), style);
        return;
    }


    /**
     *  Gets the Distance column name.
     *
     *  @return the column name
     */

    protected String getDistanceColumn() {
        return ("distance");
    }

    
    /**
     *  Sets the route <code> Id </code> for this query to match locations 
     *  along the given route. 
     *
     *  @param  routeId the route <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> routeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRouteId(org.osid.id.Id routeId, boolean match) {
        getAssembler().addIdTerm(getRouteIdColumn(), routeId, match);
        return;
    }


    /**
     *  Clears the route <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRouteIdTerms() {
        getAssembler().clearTerms(getRouteIdColumn());
        return;
    }


    /**
     *  Gets the route <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRouteIdTerms() {
        return (getAssembler().getIdTerms(getRouteIdColumn()));
    }


    /**
     *  Gets the Route Id column name.
     *
     *  @return the column name
     */

    protected String getRouteIdColumn() {
        return ("route_id");
    }


    /**
     *  Tests if a <code> RouteQuery </code> is available. 
     *
     *  @return <code> true </code> if a route query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a route. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the route query 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuery getRouteQuery() {
        throw new org.osid.UnimplementedException("supportsRouteQuery() is false");
    }


    /**
     *  Matches locations that are used on any route. 
     *
     *  @param  match <code> true </code> to match locations on any route, 
     *          <code> false </code> to match locations on no routes 
     */

    @OSID @Override
    public void matchAnyRoute(boolean match) {
        getAssembler().addIdWildcardTerm(getRouteColumn(), match);
        return;
    }


    /**
     *  Clears the route query terms. 
     */

    @OSID @Override
    public void clearRouteTerms() {
        getAssembler().clearTerms(getRouteColumn());
        return;
    }


    /**
     *  Gets the route query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQueryInspector[] getRouteTerms() {
        return (new org.osid.mapping.route.RouteQueryInspector[0]);
    }


    /**
     *  Gets the Route column name.
     *
     *  @return the column name
     */

    protected String getRouteColumn() {
        return ("route");
    }


    /**
     *  Sets the path <code> Id </code> for this query to match locations 
     *  along the given path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getPathIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        getAssembler().clearTerms(getPathIdColumn());
        return;
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (getAssembler().getIdTerms(getPathIdColumn()));
    }


    /**
     *  Gets the PathId column name.
     *
     * @return the column name
     */

    protected String getPathIdColumn() {
        return ("path_id");
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Matches locations that exist along any path. 
     *
     *  @param  match <code> true </code> to match locations on any path, 
     *          <code> false </code> to match locations on no path 
     */

    @OSID @Override
    public void matchAnyPath(boolean match) {
        getAssembler().addIdWildcardTerm(getPathColumn(), match);
        return;
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        getAssembler().clearTerms(getPathColumn());
        return;
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the Path column name.
     *
     * @return the column name
     */

    protected String getPathColumn() {
        return ("path");
    }


    /**
     *  Sets the location <code> Id </code> for this query to match locations 
     *  contained within the given location. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingLocationId(org.osid.id.Id locationId, 
                                          boolean match) {
        getAssembler().addIdTerm(getContainingLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the pcontaining location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearContainingLocationIdTerms() {
        getAssembler().clearTerms(getContainingLocationIdColumn());
        return;
    }


    /**
     *  Gets the containing location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingLocationIdTerms() {
        return (getAssembler().getIdTerms(getContainingLocationIdColumn()));
    }


    /**
     *  Gets the Containing Location Id column name.
     * 
     *  @return the column name
     */

    protected String getContainingLocationIdColumn() {
        return ("containing_location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a parent location. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingLocationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getContainingLocationQuery() {
        throw new org.osid.UnimplementedException("supportsContainingLocationQuery() is false");
    }


    /**
     *  Matches locations that have any ancestor. 
     *
     *  @param  match <code> true </code> to match locations with any parent 
     *          location, <code> false </code> to match root locations 
     */

    @OSID @Override
    public void matchAnyContainingLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getContainingLocationColumn(), match);
        return;
    }


    /**
     *  Clears the containing location query terms. 
     */

    @OSID @Override
    public void clearContainingLocationTerms() {
        getAssembler().clearTerms(getContainingLocationColumn());
        return;
    }


    /**
     *  Gets the containing location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getContainingLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the ContainingLocation column name.
     *
     * @return the column name
     */

    protected String getContainingLocationColumn() {
        return ("containing_location");
    }


    /**
     *  Sets the location <code> Id </code> for this query to match locations 
     *  containing the given location. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainedLocationId(org.osid.id.Id locationId, 
                                         boolean match) {
        getAssembler().addIdTerm(getContainedLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the contained location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearContainedLocationIdTerms() {
        getAssembler().clearTerms(getContainedLocationIdColumn());
        return;
    }


    /**
     *  Gets the contained location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainedLocationIdTerms() {
        return (getAssembler().getIdTerms(getContainedLocationIdColumn()));
    }


    /**
     *  Gets the ContainedLocationId column name.
     *
     * @return the column name
     */

    protected String getContainedLocationIdColumn() {
        return ("contained_location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainedLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a contained location. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainedLocationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getContainedLocationQuery() {
        throw new org.osid.UnimplementedException("supportsContainedLocationQuery() is false");
    }


    /**
     *  Matches locations that have any children. 
     *
     *  @param  match <code> true </code> to match locations containing any 
     *          other location, <code> false </code> to match empty locations 
     */

    @OSID @Override
    public void matchAnyContainedLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getContainedLocationColumn(), match);
        return;
    }


    /**
     *  Clears the contained location query terms. 
     */

    @OSID @Override
    public void clearContainedLocationTerms() {
        getAssembler().clearTerms(getContainedLocationColumn());
        return;
    }


    /**
     *  Gets the contained location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getContainedLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the ContainedLocation column name.
     *
     * @return the column name
     */

    protected String getContainedLocationColumn() {
        return ("contained_location");
    }


    /**
     *  Sets the map <code> Id </code> for this query. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        getAssembler().clearTerms(getMapIdColumn());
        return;
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (getAssembler().getIdTerms(getMapIdColumn()));
    }


    /**
     *  Gets the MapId column name.
     *
     * @return the column name
     */

    protected String getMapIdColumn() {
        return ("map_id");
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        getAssembler().clearTerms(getMapColumn());
        return;
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the Map column name.
     *
     * @return the column name
     */

    protected String getMapColumn() {
        return ("map");
    }


    /**
     *  Tests if this location supports the given record
     *  <code>Type</code>.
     *
     *  @param  locationRecordType a location record type 
     *  @return <code>true</code> if the locationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type locationRecordType) {
        for (org.osid.mapping.records.LocationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(locationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  locationRecordType the location record type 
     *  @return the location query record 
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(locationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.LocationQueryRecord getLocationQueryRecord(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.LocationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(locationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(locationRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  locationRecordType the location record type 
     *  @return the location query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(locationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.LocationQueryInspectorRecord getLocationQueryInspectorRecord(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.LocationQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(locationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(locationRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param locationRecordType the location record type
     *  @return the location search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(locationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.LocationSearchOrderRecord getLocationSearchOrderRecord(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.LocationSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(locationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(locationRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this location. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param locationQueryRecord the location query record
     *  @param locationQueryInspectorRecord the location query inspector
     *         record
     *  @param locationSearchOrderRecord the location search order record
     *  @param locationRecordType location record type
     *  @throws org.osid.NullArgumentException
     *          <code>locationQueryRecord</code>,
     *          <code>locationQueryInspectorRecord</code>,
     *          <code>locationSearchOrderRecord</code> or
     *          <code>locationRecordTypelocation</code> is
     *          <code>null</code>
     */
            
    protected void addLocationRecords(org.osid.mapping.records.LocationQueryRecord locationQueryRecord, 
                                      org.osid.mapping.records.LocationQueryInspectorRecord locationQueryInspectorRecord, 
                                      org.osid.mapping.records.LocationSearchOrderRecord locationSearchOrderRecord, 
                                      org.osid.type.Type locationRecordType) {

        addRecordType(locationRecordType);

        nullarg(locationQueryRecord, "location query record");
        nullarg(locationQueryInspectorRecord, "location query inspector record");
        nullarg(locationSearchOrderRecord, "location search odrer record");

        this.queryRecords.add(locationQueryRecord);
        this.queryInspectorRecords.add(locationQueryInspectorRecord);
        this.searchOrderRecords.add(locationSearchOrderRecord);
        
        return;
    }
}
