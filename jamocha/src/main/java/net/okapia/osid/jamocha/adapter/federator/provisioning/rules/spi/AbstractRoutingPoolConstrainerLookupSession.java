//
// AbstractRoutingPoolConstrainerLookupSession
//
//     A routing federating adapter for a PoolConstrainerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A federating adapter for a PoolConstrainerLookupSession. Sessions are
 *  added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 *
 *  The routing for Ids and Types are manually set by subclasses of
 *  this session that add child sessions for federation.
 *
 *  Routing for Ids is based on the existince of an Id embedded in the
 *  authority of a pool constrainer Id. The embedded Id is specified when
 *  adding child sessions. Optionally, a list of types for genus and
 *  record may also be mapped to a child session. A service Id for
 *  routing must be unique across all federated sessions while a Type
 *  may map to more than one session.
 *
 *  Lookup methods consult the mapping tables for Ids and Types, and
 *  will fallback to searching all sessions if the pool constrainer is not
 *  found and fallback is true. In the case of record and genus Type
 *  lookups, a fallback will search all child sessions regardless if
 *  any are found.
 */

public abstract class AbstractRoutingPoolConstrainerLookupSession
    extends AbstractFederatingPoolConstrainerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.PoolConstrainerLookupSession> sessionsById = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.PoolConstrainerLookupSession>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.PoolConstrainerLookupSession> sessionsByType = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.PoolConstrainerLookupSession>());
    private final boolean idFallback;
    private final boolean typeFallback;

    
    /**
     *  Constructs a new
     *  <code>AbstractRoutingPoolConstrainerLookupSession</code> with
     *  fallbacks for both Types and Ids.
     */

    protected AbstractRoutingPoolConstrainerLookupSession() {
        this.idFallback   = true;
        this.typeFallback = true;
        selectAll();
        return;
    }


    /**
     *  Constructs a new
     *  <code>AbstractRoutingPoolConstrainerLookupSession</code>.
     *
     *  @param idFallback <code>true</code> to fall back to searching
     *         all sessions if an Id is not in routing table
     *  @param typeFallback <code>true</code> to fall back to
     *         searching all sessions if a Type is not in routing
     *         table
     */

    protected AbstractRoutingPoolConstrainerLookupSession(boolean idFallback, boolean typeFallback) {
        this.idFallback   = idFallback;
        this.typeFallback = typeFallback;
        selectAll();
        return;
    }


    /**
     *  Maps a session to a service Id. Use <code>addSession()</code>
     *  to add a session to this federation.
     *
     *  @param session a session to map
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void mapSession(org.osid.provisioning.rules.PoolConstrainerLookupSession session, org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.put(serviceId, session);
        return;
    }


    /**
     *  Maps a session to a record or genus Type. Use
     *  <code>addSession()</code> to add a session to this federation.
     *
     *  @param session a session to add
     *  @param type
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>type</code> is <code>null</code>
     */

    protected void mapSession(org.osid.provisioning.rules.PoolConstrainerLookupSession session, org.osid.type.Type type) {
        nullarg(type, "type");
        this.sessionsByType.put(type, session);
        return;
    }


    /**
     *  Unmaps a session from a serviceId.
     *
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException 
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.remove(serviceId);
        return;
    }


    /**
     *  Unmaps a session from both the Id and Type indices.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.provisioning.rules.PoolConstrainerLookupSession session) {
        nullarg(session, "session");

        for (org.osid.id.Id id : this.sessionsById.keySet()) {
            if (session.equals(this.sessionsById.get(id))) {
                this.sessionsById.remove(id);
            }
        }

        this.sessionsByType.removeValue(session);

        return;
    }


    /**
     *  Removes a session from this federation.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void removeSession(org.osid.provisioning.rules.PoolConstrainerLookupSession session) {
        unmapSession(session);
        super.removeSession(session);
        return;
    }


    /**
     *  Gets the <code>PoolConstrainer</code> specified by its <code>Id</code>. 
     *
     *  @param  poolConstrainerId <code>Id</code> of the
     *          <code>PoolConstrainer</code>
     *  @return the pool constrainer
     *  @throws org.osid.NotFoundException <code>poolConstrainerId</code> not 
     *          found in any session
     *  @throws org.osid.NullArgumentException <code>poolConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainer getPoolConstrainer(org.osid.id.Id poolConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.PoolConstrainerLookupSession session = this.sessionsById.get(poolConstrainerId.getAuthority());
        if (session != null) {
            return (session.getPoolConstrainer(poolConstrainerId));
        }
        
        if (this.idFallback) {
            return (super.getPoolConstrainer(poolConstrainerId));
        }

        throw new org.osid.NotFoundException(poolConstrainerId + " not found");
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the given
     *  poolConstrainer genus <code>Type</code> which does not include
     *  pool constrainers of types derived from the specified
     *  <code>Type</code>.
     *  
     *
     *  @param  poolConstrainerGenusType a pool constrainer genus type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.FederatingPoolConstrainerList ret = getPoolConstrainerList();
        java.util.Collection<org.osid.provisioning.rules.PoolConstrainerLookupSession> sessions = this.sessionsByType.get(poolConstrainerGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getPoolConstrainersByGenusType(poolConstrainerGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.provisioning.rules.poolconstrainer.EmptyPoolConstrainerList());
            }
        }


        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : sessions) {
            ret.addPoolConstrainerList(session.getPoolConstrainersByGenusType(poolConstrainerGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the given
     *  pool constrainer genus <code>Type</code> and include any additional
     *  pool constrainers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  @param  poolConstrainerGenusType a pool constrainer genus type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByParentGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.FederatingPoolConstrainerList ret = getPoolConstrainerList();
        java.util.Collection<org.osid.provisioning.rules.PoolConstrainerLookupSession> sessions = this.sessionsByType.get(poolConstrainerGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getPoolConstrainersByParentGenusType(poolConstrainerGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.provisioning.rules.poolconstrainer.EmptyPoolConstrainerList());
            }
        }


        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : sessions) {
            ret.addPoolConstrainerList(session.getPoolConstrainersByParentGenusType(poolConstrainerGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> containing the given
     *  pool constrainer record <code>Type</code>.
     *
     *  @param  poolConstrainerRecordType a pool constrainer record type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByRecordType(org.osid.type.Type poolConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.FederatingPoolConstrainerList ret = getPoolConstrainerList();
        java.util.Collection<org.osid.provisioning.rules.PoolConstrainerLookupSession> sessions = this.sessionsByType.get(poolConstrainerRecordType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getPoolConstrainersByRecordType(poolConstrainerRecordType));
            } else {
                return (new net.okapia.osid.jamocha.nil.provisioning.rules.poolconstrainer.EmptyPoolConstrainerList());
            }
        }

        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : sessions) {
            ret.addPoolConstrainerList(session.getPoolConstrainersByRecordType(poolConstrainerRecordType));
        }
        
        ret.noMore();
        return (ret);
    }
}
