//
// AbstractIndexedMapStepProcessorLookupSession.java
//
//    A simple framework for providing a StepProcessor lookup service
//    backed by a fixed collection of step processors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a StepProcessor lookup service backed by a
 *  fixed collection of step processors. The step processors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some step processors may be compatible
 *  with more types than are indicated through these step processor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>StepProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapStepProcessorLookupSession
    extends AbstractMapStepProcessorLookupSession
    implements org.osid.workflow.rules.StepProcessorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.StepProcessor> stepProcessorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.StepProcessor>());
    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.StepProcessor> stepProcessorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.StepProcessor>());


    /**
     *  Makes a <code>StepProcessor</code> available in this session.
     *
     *  @param  stepProcessor a step processor
     *  @throws org.osid.NullArgumentException <code>stepProcessor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putStepProcessor(org.osid.workflow.rules.StepProcessor stepProcessor) {
        super.putStepProcessor(stepProcessor);

        this.stepProcessorsByGenus.put(stepProcessor.getGenusType(), stepProcessor);
        
        try (org.osid.type.TypeList types = stepProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepProcessorsByRecord.put(types.getNextType(), stepProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a step processor from this session.
     *
     *  @param stepProcessorId the <code>Id</code> of the step processor
     *  @throws org.osid.NullArgumentException <code>stepProcessorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeStepProcessor(org.osid.id.Id stepProcessorId) {
        org.osid.workflow.rules.StepProcessor stepProcessor;
        try {
            stepProcessor = getStepProcessor(stepProcessorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.stepProcessorsByGenus.remove(stepProcessor.getGenusType());

        try (org.osid.type.TypeList types = stepProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepProcessorsByRecord.remove(types.getNextType(), stepProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeStepProcessor(stepProcessorId);
        return;
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the given
     *  step processor genus <code>Type</code> which does not include
     *  step processors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known step processors or an error results. Otherwise,
     *  the returned list may contain only those step processors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  stepProcessorGenusType a step processor genus type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByGenusType(org.osid.type.Type stepProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepprocessor.ArrayStepProcessorList(this.stepProcessorsByGenus.get(stepProcessorGenusType)));
    }


    /**
     *  Gets a <code>StepProcessorList</code> containing the given
     *  step processor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known step processors or an error
     *  results. Otherwise, the returned list may contain only those
     *  step processors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  stepProcessorRecordType a step processor record type 
     *  @return the returned <code>stepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByRecordType(org.osid.type.Type stepProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepprocessor.ArrayStepProcessorList(this.stepProcessorsByRecord.get(stepProcessorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stepProcessorsByGenus.clear();
        this.stepProcessorsByRecord.clear();

        super.close();

        return;
    }
}
