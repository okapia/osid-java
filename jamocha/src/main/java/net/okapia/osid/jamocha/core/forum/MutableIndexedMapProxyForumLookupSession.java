//
// MutableIndexedMapProxyForumLookupSession
//
//    Implements a Forum lookup service backed by a collection of
//    forums indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.forum;


/**
 *  Implements a Forum lookup service backed by a collection of
 *  forums. The forums are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some forums may be compatible
 *  with more types than are indicated through these forum
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of forums can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyForumLookupSession
    extends net.okapia.osid.jamocha.core.forum.spi.AbstractIndexedMapForumLookupSession
    implements org.osid.forum.ForumLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyForumLookupSession} with
     *  no forum.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyForumLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyForumLookupSession} with
     *  a single forum.
     *
     *  @param  forum an forum
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forum} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyForumLookupSession(org.osid.forum.Forum forum, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putForum(forum);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyForumLookupSession} using
     *  an array of forums.
     *
     *  @param  forums an array of forums
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forums} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyForumLookupSession(org.osid.forum.Forum[] forums, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putForums(forums);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyForumLookupSession} using
     *  a collection of forums.
     *
     *  @param  forums a collection of forums
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forums} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyForumLookupSession(java.util.Collection<? extends org.osid.forum.Forum> forums,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putForums(forums);
        return;
    }

    
    /**
     *  Makes a {@code Forum} available in this session.
     *
     *  @param  forum a forum
     *  @throws org.osid.NullArgumentException {@code forum{@code 
     *          is {@code null}
     */

    @Override
    public void putForum(org.osid.forum.Forum forum) {
        super.putForum(forum);
        return;
    }


    /**
     *  Makes an array of forums available in this session.
     *
     *  @param  forums an array of forums
     *  @throws org.osid.NullArgumentException {@code forums{@code 
     *          is {@code null}
     */

    @Override
    public void putForums(org.osid.forum.Forum[] forums) {
        super.putForums(forums);
        return;
    }


    /**
     *  Makes collection of forums available in this session.
     *
     *  @param  forums a collection of forums
     *  @throws org.osid.NullArgumentException {@code forum{@code 
     *          is {@code null}
     */

    @Override
    public void putForums(java.util.Collection<? extends org.osid.forum.Forum> forums) {
        super.putForums(forums);
        return;
    }


    /**
     *  Removes a Forum from this session.
     *
     *  @param forumId the {@code Id} of the forum
     *  @throws org.osid.NullArgumentException {@code forumId{@code  is
     *          {@code null}
     */

    @Override
    public void removeForum(org.osid.id.Id forumId) {
        super.removeForum(forumId);
        return;
    }    
}
