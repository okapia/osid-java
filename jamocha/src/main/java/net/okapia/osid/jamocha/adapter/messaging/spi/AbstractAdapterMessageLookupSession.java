//
// AbstractAdapterMessageLookupSession.java
//
//    A Message lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.messaging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Message lookup session adapter.
 */

public abstract class AbstractAdapterMessageLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.messaging.MessageLookupSession {

    private final org.osid.messaging.MessageLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterMessageLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterMessageLookupSession(org.osid.messaging.MessageLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Mailbox/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Mailbox Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMailboxId() {
        return (this.session.getMailboxId());
    }


    /**
     *  Gets the {@code Mailbox} associated with this session.
     *
     *  @return the {@code Mailbox} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMailbox());
    }


    /**
     *  Tests if this user can perform {@code Message} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupMessages() {
        return (this.session.canLookupMessages());
    }


    /**
     *  A complete view of the {@code Message} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMessageView() {
        this.session.useComparativeMessageView();
        return;
    }


    /**
     *  A complete view of the {@code Message} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMessageView() {
        this.session.usePlenaryMessageView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include messages in mailboxes which are children
     *  of this mailbox in the mailbox hierarchy.
     */

    @OSID @Override
    public void useFederatedMailboxView() {
        this.session.useFederatedMailboxView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this mailbox only.
     */

    @OSID @Override
    public void useIsolatedMailboxView() {
        this.session.useIsolatedMailboxView();
        return;
    }
    
     
    /**
     *  Gets the {@code Message} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Message} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Message} and
     *  retained for compatibility.
     *
     *  @param messageId {@code Id} of the {@code Message}
     *  @return the message
     *  @throws org.osid.NotFoundException {@code messageId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code messageId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Message getMessage(org.osid.id.Id messageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessage(messageId));
    }


    /**
     *  Gets a {@code MessageList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  messages specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Messages} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  messageIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Message} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code messageIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByIds(org.osid.id.IdList messageIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByIds(messageIds));
    }


    /**
     *  Gets a {@code MessageList} corresponding to the given
     *  message genus {@code Type} which does not include
     *  messages of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  messageGenusType a message genus type 
     *  @return the returned {@code Message} list
     *  @throws org.osid.NullArgumentException
     *          {@code messageGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusType(org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByGenusType(messageGenusType));
    }


    /**
     *  Gets a {@code MessageList} corresponding to the given
     *  message genus {@code Type} and include any additional
     *  messages with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  messageGenusType a message genus type 
     *  @return the returned {@code Message} list
     *  @throws org.osid.NullArgumentException
     *          {@code messageGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByParentGenusType(org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByParentGenusType(messageGenusType));
    }


    /**
     *  Gets a {@code MessageList} containing the given
     *  message record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  messageRecordType a message record type 
     *  @return the returned {@code Message} list
     *  @throws org.osid.NullArgumentException
     *          {@code messageRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByRecordType(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByRecordType(messageRecordType));
    }


    /**
     *  Gets a {@code MessageList} sent within the specified range
     *  inclusive. In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code
     *          to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTime(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMessagesBySentTime(from, to));
    }


    /**
     *  Gets a {@code MessageList} of the given genus type and sent
     *  within the specified range inclusive. In plenary mode, the
     *  returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code
     *          messageGenusType, from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeAndGenusType(org.osid.type.Type messageGenusType, 
                                                                            org.osid.calendaring.DateTime from, 
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesBySentTimeAndGenusType(messageGenusType, from, to));
    }

    
    /**
     *  Gets a {@code MessageList} sent by the specified sender. In
     *  plenary mode, the returned list contains all known messages or
     *  an error results. Otherwise, the returned list may contain
     *  only those messages that are accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesFromSender(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMessagesFromSender(resourceId));
    }


    /**
     *  Gets a {@code MessageList} of the given genus type and sender.
     *  In plenary mode, the returned list contains all known messages
     *  or an error results. Otherwise, the returned list may contain
     *  only those messages that are accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code messageGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeFromSender(org.osid.id.Id resourceId, 
                                                                           org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMessagesByGenusTypeFromSender(resourceId, messageGenusType));
    }

    
    /**
     *  Gets a {@code MessageList} sent by the specified sender and
     *  sent time. In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session
     *
     *  @param  resourceId a resource Id 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId, from} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeFromSender(org.osid.id.Id resourceId, 
                                                                          org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesBySentTimeFromSender(resourceId, from, to));
    }


    /**
     *  Gets a {@code MessageList} of the given genus type and sent
     *  within the specified range inclusive. In plenary mode, the
     *  returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          messageGenusType, from} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeAndGenusTypeFromSender(org.osid.id.Id resourceId, 
                                                                                      org.osid.type.Type messageGenusType, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesBySentTimeAndGenusTypeFromSender(resourceId, messageGenusType, from, to));
    }


    /**
     *  Gets a {@code MessageList} received by the specified
     *  recipient.  In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMessagesForRecipient(resourceId));
    }


    /**
     *  Gets a {@code MessageList} received by the specified
     *  recipient.  In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code messageGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeForRecipient(org.osid.id.Id resourceId, 
                                                                             org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByGenusTypeForRecipient(resourceId, messageGenusType));
    }


    /**
     *  Gets a {@code MessageList} received by the specified resource
     *  and received time. In plenary mode, the returned list contains
     *  all known messages or an error results. Otherwise, the
     *  returned list may contain only those messages that are
     *  accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId, from} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeForRecipient(org.osid.id.Id resourceId, 
                                                                                org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByReceivedTimeForRecipient(resourceId, from, to));
    }


    /**
     *  Gets a {@code MessageList} of the given genus type received by
     *  the specified resource and received time. In plenary mode, the
     *  returned list contains all known messages or an error results.
     *  Otherwise, the returned list may contain only those messages
     *  that are accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          messageGenusType, from} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeAndGenusTypeForRecipient(org.osid.id.Id resourceId, 
                                                                                            org.osid.type.Type messageGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByReceivedTimeAndGenusTypeForRecipient(resourceId, messageGenusType, from, to));
    }


    /**
     *  Gets a {@code MessageList} sent by the specified sender and
     *  received by the specified recipient. In plenary mode, the
     *  returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.NullArgumentException {@code senderResourceId} 
     *          or {@code recipientResourceId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                            org.osid.id.Id recipientResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesFromSenderForRecipient(senderResourceId, recipientResourceId));
    }


    /**
     *  Gets a {@code MessageList} of the given genus type sent by the
     *  specified sender and received by the specified recipient. In
     *  plenary mode, the returned list contains all known messages or
     *  an error results. Otherwise, the returned list may contain
     *  only those messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.NullArgumentException {@code senderResourceId, 
     *          recipientResourceId} or {@code messageGenusType} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                                       org.osid.id.Id recipientResourceId, 
                                                                                       org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByGenusTypeFromSenderForRecipient(senderResourceId, recipientResourceId, messageGenusType));
    }

    
    /**
     *  Gets a {@code MessageList} by the specified sender, recipient,
     *  and received time. In plenary mode, the returned list contains
     *  all known messages or an error results. Otherwise, the
     *  returned list may contain only those messages that are
     *  accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code senderResourceId, 
     *          recipientResourceId, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                                          org.osid.id.Id recipientResourceId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByReceivedTimeFromSenderForRecipient(senderResourceId, recipientResourceId, from, to));
    }


    /**
     *  Gets a {@code MessageList} of the given genus type and
     *  received by the specified resource and received time. In
     *  plenary mode, the returned list contains all known messages or
     *  an error results.  Otherwise, the returned list may contain
     *  only those messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Message} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code senderResourceId, 
     *          recipientResourceId, messageGenusType, from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeAndGenusTypeFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                                                      org.osid.id.Id recipientResourceId, 
                                                                                                      org.osid.type.Type messageGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessagesByReceivedTimeAndGenusTypeFromSenderForRecipient(senderResourceId, recipientResourceId, messageGenusType, from, to));
    }


    /**
     *  Gets all {@code Messages}. 
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Messages} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMessages());
    }
}
