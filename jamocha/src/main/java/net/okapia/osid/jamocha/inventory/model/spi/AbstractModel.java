//
// AbstractModel.java
//
//     Defines a Model.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.model.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Model</code>.
 */

public abstract class AbstractModel
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.inventory.Model {

    private org.osid.resource.Resource manufacturer;
    private String archetype;
    private String number;

    private final java.util.Collection<org.osid.inventory.records.ModelRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the manufacturer <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getManufacturerId() {
        return (this.manufacturer.getId());
    }


    /**
     *  Gets the manufacturer (e.g. Cisco). 
     *
     *  @return a resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getManufacturer()
        throws org.osid.OperationFailedException {

        return (this.manufacturer);
    }


    /**
     *  Sets the manufacturer.
     *
     *  @param manufacturer a manufacturer
     *  @throws org.osid.NullArgumentException
     *          <code>manufacturer</code> is <code>null</code>
     */

    protected void setManufacturer(org.osid.resource.Resource manufacturer) {
        nullarg(manufacturer, "manufacturer");
        this.manufacturer = manufacturer;
        return;
    }


    /**
     *  Gets the model name component (e.g. ASR 9010) 
     *
     *  @return the model 
     */

    @OSID @Override
    public String getArchetype() {
        return (this.archetype);
    }


    /**
     *  Sets the archetype.
     *
     *  @param archetype an archetype
     *  @throws org.osid.NullArgumentException
     *          <code>archetype</code> is <code>null</code>
     */

    protected void setArchetype(String archetype) {
        nullarg(archetype, "archetype");
        this.archetype = archetype;
        return;
    }


    /**
     *  Gets the model number (e.g. ASR-9010-AC-V2). 
     *
     *  @return the model number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.number);
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    protected void setNumber(String number) {
        nullarg(number, "number");
        this.number = number;
        return;
    }


    /**
     *  Tests if this model supports the given record
     *  <code>Type</code>.
     *
     *  @param  modelRecordType a model record type 
     *  @return <code>true</code> if the modelRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type modelRecordType) {
        for (org.osid.inventory.records.ModelRecord record : this.records) {
            if (record.implementsRecordType(modelRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Model</code>
     *  record <code>Type</code>.
     *
     *  @param  modelRecordType the model record type 
     *  @return the model record 
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(modelRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ModelRecord getModelRecord(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ModelRecord record : this.records) {
            if (record.implementsRecordType(modelRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(modelRecordType + " is not supported");
    }


    /**
     *  Adds a record to this model. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param modelRecord the model record
     *  @param modelRecordType model record type
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecord</code> or
     *          <code>modelRecordTypemodel</code> is
     *          <code>null</code>
     */
            
    protected void addModelRecord(org.osid.inventory.records.ModelRecord modelRecord, 
                                  org.osid.type.Type modelRecordType) {

        nullarg(modelRecord, "model record");
        addRecordType(modelRecordType);
        this.records.add(modelRecord);
        
        return;
    }
}
